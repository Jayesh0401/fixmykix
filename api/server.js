var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var session = require('express-session');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, './../fixmykix')));
app.use(session({ secret: 'user._id',resave: false, saveUnintialized: true }));


app.get('*', function(req, res, next) {
  res.sendFile('index.html', { root: path.join(__dirname, './../fixmykix') });
});

var port = process.env.PORT || 8000; // set our port

io.on('connect',function(socket) {
  console.log('a user is connected');
  socket.on('disconnect',function() {
    console.log('a user is disconnected');
  });
  count= 0;
  socket.on('message', (message) => {
    count++;
    io.emit('messages',message);
    // io.emit(message);
  });
});
app.set('socketio', io);
http.listen(port,function() {
  console.log('server starts on port : ', port);
});
// START THE SERVER
// =============================================================================
// http.listen(port);
// console.log('server starts on port ' + port);

module.exports = app;