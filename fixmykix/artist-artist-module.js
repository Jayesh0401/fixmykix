(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["artist-artist-module"],{

/***/ "./node_modules/@nebular/eva-icons/fesm5/index.js":
/*!********************************************************!*\
  !*** ./node_modules/@nebular/eva-icons/fesm5/index.js ***!
  \********************************************************/
/*! exports provided: NbEvaSvgIcon, NbEvaIconsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NbEvaSvgIcon", function() { return NbEvaSvgIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NbEvaIconsModule", function() { return NbEvaIconsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm5/index.js");
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! eva-icons */ "./node_modules/eva-icons/eva.js");
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(eva_icons__WEBPACK_IMPORTED_MODULE_2__);




/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var NbEvaSvgIcon = /** @class */ (function (_super) {
    __extends(NbEvaSvgIcon, _super);
    function NbEvaSvgIcon(name, content) {
        var _this = _super.call(this, name, '') || this;
        _this.name = name;
        _this.content = content;
        return _this;
    }
    NbEvaSvgIcon.prototype.getContent = function (options) {
        return this.content.toSvg(__assign({ width: '100%', height: '100%', fill: 'currentColor' }, options));
    };
    return NbEvaSvgIcon;
}(_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbSvgIcon"]));
var NbEvaIconsModule = /** @class */ (function () {
    function NbEvaIconsModule(iconLibrary) {
        this.NAME = 'eva';
        iconLibrary.registerSvgPack(this.NAME, this.createIcons());
        iconLibrary.setDefaultPack(this.NAME);
    }
    NbEvaIconsModule.prototype.createIcons = function () {
        return Object
            .entries(eva_icons__WEBPACK_IMPORTED_MODULE_2__["icons"])
            .map(function (_a) {
            var name = _a[0], icon = _a[1];
            return [name, new NbEvaSvgIcon(name, icon)];
        })
            .reduce(function (newIcons, _a) {
            var name = _a[0], icon = _a[1];
            newIcons[name] = icon;
            return newIcons;
        }, {});
    };
    NbEvaIconsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({}),
        __metadata("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_1__["NbIconLibraries"]])
    ], NbEvaIconsModule);
    return NbEvaIconsModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * Generated bundle index. Do not edit.
 */




/***/ }),

/***/ "./node_modules/eva-icons/eva.js":
/*!***************************************!*\
  !*** ./node_modules/eva-icons/eva.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory();
	else {}
})(typeof self !== "undefined" ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./package/src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/classnames/dedupe.js":
/*!*******************************************!*\
  !*** ./node_modules/classnames/dedupe.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2017 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var classNames = (function () {
		// don't inherit from Object so we can skip hasOwnProperty check later
		// http://stackoverflow.com/questions/15518328/creating-js-object-with-object-createnull#answer-21079232
		function StorageObject() {}
		StorageObject.prototype = Object.create(null);

		function _parseArray (resultSet, array) {
			var length = array.length;

			for (var i = 0; i < length; ++i) {
				_parse(resultSet, array[i]);
			}
		}

		var hasOwn = {}.hasOwnProperty;

		function _parseNumber (resultSet, num) {
			resultSet[num] = true;
		}

		function _parseObject (resultSet, object) {
			for (var k in object) {
				if (hasOwn.call(object, k)) {
					// set value to false instead of deleting it to avoid changing object structure
					// https://www.smashingmagazine.com/2012/11/writing-fast-memory-efficient-javascript/#de-referencing-misconceptions
					resultSet[k] = !!object[k];
				}
			}
		}

		var SPACE = /\s+/;
		function _parseString (resultSet, str) {
			var array = str.split(SPACE);
			var length = array.length;

			for (var i = 0; i < length; ++i) {
				resultSet[array[i]] = true;
			}
		}

		function _parse (resultSet, arg) {
			if (!arg) return;
			var argType = typeof arg;

			// 'foo bar'
			if (argType === 'string') {
				_parseString(resultSet, arg);

			// ['foo', 'bar', ...]
			} else if (Array.isArray(arg)) {
				_parseArray(resultSet, arg);

			// { 'foo': true, ... }
			} else if (argType === 'object') {
				_parseObject(resultSet, arg);

			// '130'
			} else if (argType === 'number') {
				_parseNumber(resultSet, arg);
			}
		}

		function _classNames () {
			// don't leak arguments
			// https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#32-leaking-arguments
			var len = arguments.length;
			var args = Array(len);
			for (var i = 0; i < len; i++) {
				args[i] = arguments[i];
			}

			var classSet = new StorageObject();
			_parseArray(classSet, args);

			var list = [];

			for (var k in classSet) {
				if (classSet[k]) {
					list.push(k)
				}
			}

			return list.join(' ');
		}

		return _classNames;
	})();

	if (typeof module !== 'undefined' && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
}());


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./package/src/animation.scss":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/sass-loader/lib/loader.js!./package/src/animation.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n.eva-animation {\n  animation-duration: 1s;\n  animation-fill-mode: both; }\n\n.eva-infinite {\n  animation-iteration-count: infinite; }\n\n.eva-icon-shake {\n  animation-name: eva-shake; }\n\n.eva-icon-zoom {\n  animation-name: eva-zoomIn; }\n\n.eva-icon-pulse {\n  animation-name: eva-pulse; }\n\n.eva-icon-flip {\n  animation-name: eva-flipInY; }\n\n.eva-hover {\n  display: inline-block; }\n\n.eva-hover:hover .eva-icon-hover-shake, .eva-parent-hover:hover .eva-icon-hover-shake {\n  animation-name: eva-shake; }\n\n.eva-hover:hover .eva-icon-hover-zoom, .eva-parent-hover:hover .eva-icon-hover-zoom {\n  animation-name: eva-zoomIn; }\n\n.eva-hover:hover .eva-icon-hover-pulse, .eva-parent-hover:hover .eva-icon-hover-pulse {\n  animation-name: eva-pulse; }\n\n.eva-hover:hover .eva-icon-hover-flip, .eva-parent-hover:hover .eva-icon-hover-flip {\n  animation-name: eva-flipInY; }\n\n@keyframes eva-flipInY {\n  from {\n    transform: perspective(400px) rotate3d(0, 1, 0, 90deg);\n    animation-timing-function: ease-in;\n    opacity: 0; }\n  40% {\n    transform: perspective(400px) rotate3d(0, 1, 0, -20deg);\n    animation-timing-function: ease-in; }\n  60% {\n    transform: perspective(400px) rotate3d(0, 1, 0, 10deg);\n    opacity: 1; }\n  80% {\n    transform: perspective(400px) rotate3d(0, 1, 0, -5deg); }\n  to {\n    transform: perspective(400px); } }\n\n@keyframes eva-shake {\n  from,\n  to {\n    transform: translate3d(0, 0, 0); }\n  10%,\n  30%,\n  50%,\n  70%,\n  90% {\n    transform: translate3d(-3px, 0, 0); }\n  20%,\n  40%,\n  60%,\n  80% {\n    transform: translate3d(3px, 0, 0); } }\n\n@keyframes eva-pulse {\n  from {\n    transform: scale3d(1, 1, 1); }\n  50% {\n    transform: scale3d(1.2, 1.2, 1.2); }\n  to {\n    transform: scale3d(1, 1, 1); } }\n\n@keyframes eva-zoomIn {\n  from {\n    opacity: 1;\n    transform: scale3d(0.5, 0.5, 0.5); }\n  50% {\n    opacity: 1; } }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/isomorphic-style-loader/insertCss.js":
/*!***********************************************************!*\
  !*** ./node_modules/isomorphic-style-loader/insertCss.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*! Isomorphic Style Loader | MIT License | https://github.com/kriasoft/isomorphic-style-loader */



var inserted = {};

function b64EncodeUnicode(str) {
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
    return String.fromCharCode("0x" + p1);
  }));
}

function removeCss(ids) {
  ids.forEach(function (id) {
    if (--inserted[id] <= 0) {
      var elem = document.getElementById(id);

      if (elem) {
        elem.parentNode.removeChild(elem);
      }
    }
  });
}

function insertCss(styles, _temp) {
  var _ref = _temp === void 0 ? {} : _temp,
      _ref$replace = _ref.replace,
      replace = _ref$replace === void 0 ? false : _ref$replace,
      _ref$prepend = _ref.prepend,
      prepend = _ref$prepend === void 0 ? false : _ref$prepend,
      _ref$prefix = _ref.prefix,
      prefix = _ref$prefix === void 0 ? 's' : _ref$prefix;

  var ids = [];

  for (var i = 0; i < styles.length; i++) {
    var _styles$i = styles[i],
        moduleId = _styles$i[0],
        css = _styles$i[1],
        media = _styles$i[2],
        sourceMap = _styles$i[3];
    var id = "" + prefix + moduleId + "-" + i;
    ids.push(id);

    if (inserted[id]) {
      if (!replace) {
        inserted[id]++;
        continue;
      }
    }

    inserted[id] = 1;
    var elem = document.getElementById(id);
    var create = false;

    if (!elem) {
      create = true;
      elem = document.createElement('style');
      elem.setAttribute('type', 'text/css');
      elem.id = id;

      if (media) {
        elem.setAttribute('media', media);
      }
    }

    var cssText = css;

    if (sourceMap && typeof btoa === 'function') {
      cssText += "\n/*# sourceMappingURL=data:application/json;base64," + b64EncodeUnicode(JSON.stringify(sourceMap)) + "*/";
      cssText += "\n/*# sourceURL=" + sourceMap.file + "?" + id + "*/";
    }

    if ('textContent' in elem) {
      elem.textContent = cssText;
    } else {
      elem.styleSheet.cssText = cssText;
    }

    if (create) {
      if (prepend) {
        document.head.insertBefore(elem, document.head.childNodes[0]);
      } else {
        document.head.appendChild(elem);
      }
    }
  }

  return removeCss.bind(null, ids);
}

module.exports = insertCss;
//# sourceMappingURL=insertCss.js.map


/***/ }),

/***/ "./package-build/eva-icons.json":
/*!**************************************!*\
  !*** ./package-build/eva-icons.json ***!
  \**************************************/
/*! exports provided: activity, alert-circle, alert-triangle, archive, arrow-back, arrow-circle-down, arrow-circle-left, arrow-circle-right, arrow-circle-up, arrow-down, arrow-downward, arrow-forward, arrow-ios-back, arrow-ios-downward, arrow-ios-forward, arrow-ios-upward, arrow-left, arrow-right, arrow-up, arrow-upward, arrowhead-down, arrowhead-left, arrowhead-right, arrowhead-up, at, attach-2, attach, award, backspace, bar-chart-2, bar-chart, battery, behance, bell-off, bell, bluetooth, book-open, book, bookmark, briefcase, browser, brush, bulb, calendar, camera, car, cast, charging, checkmark-circle-2, checkmark-circle, checkmark-square-2, checkmark-square, checkmark, chevron-down, chevron-left, chevron-right, chevron-up, clipboard, clock, close-circle, close-square, close, cloud-download, cloud-upload, code-download, code, collapse, color-palette, color-picker, compass, copy, corner-down-left, corner-down-right, corner-left-down, corner-left-up, corner-right-down, corner-right-up, corner-up-left, corner-up-right, credit-card, crop, cube, diagonal-arrow-left-down, diagonal-arrow-left-up, diagonal-arrow-right-down, diagonal-arrow-right-up, done-all, download, droplet-off, droplet, edit-2, edit, email, expand, external-link, eye-off-2, eye-off, eye, facebook, file-add, file-remove, file-text, file, film, flag, flash-off, flash, flip-2, flip, folder-add, folder-remove, folder, funnel, gift, github, globe-2, globe-3, globe, google, grid, hard-drive, hash, headphones, heart, home, image-2, image, inbox, info, keypad, layers, layout, link-2, link, linkedin, list, lock, log-in, log-out, map, maximize, menu-2, menu-arrow, menu, message-circle, message-square, mic-off, mic, minimize, minus-circle, minus-square, minus, monitor, moon, more-horizontal, more-vertical, move, music, navigation-2, navigation, npm, options-2, options, pantone, paper-plane, pause-circle, people, percent, person-add, person-delete, person-done, person-remove, person, phone-call, phone-missed, phone-off, phone, pie-chart-2, pie-chart, pin, play-circle, plus-circle, plus-square, plus, power, pricetags, printer, question-mark-circle, question-mark, radio-button-off, radio-button-on, radio, recording, refresh, repeat, rewind-left, rewind-right, save, scissors, search, settings-2, settings, shake, share, shield-off, shield, shopping-bag, shopping-cart, shuffle-2, shuffle, skip-back, skip-forward, slash, smartphone, smiling-face, speaker, square, star, stop-circle, sun, swap, sync, text, thermometer-minus, thermometer-plus, thermometer, toggle-left, toggle-right, trash-2, trash, trending-down, trending-up, tv, twitter, umbrella, undo, unlock, upload, video-off, video, volume-down, volume-mute, volume-off, volume-up, wifi-off, wifi, activity-outline, alert-circle-outline, alert-triangle-outline, archive-outline, arrow-back-outline, arrow-circle-down-outline, arrow-circle-left-outline, arrow-circle-right-outline, arrow-circle-up-outline, arrow-down-outline, arrow-downward-outline, arrow-forward-outline, arrow-ios-back-outline, arrow-ios-downward-outline, arrow-ios-forward-outline, arrow-ios-upward-outline, arrow-left-outline, arrow-right-outline, arrow-up-outline, arrow-upward-outline, arrowhead-down-outline, arrowhead-left-outline, arrowhead-right-outline, arrowhead-up-outline, at-outline, attach-2-outline, attach-outline, award-outline, backspace-outline, bar-chart-2-outline, bar-chart-outline, battery-outline, behance-outline, bell-off-outline, bell-outline, bluetooth-outline, book-open-outline, book-outline, bookmark-outline, briefcase-outline, browser-outline, brush-outline, bulb-outline, calendar-outline, camera-outline, car-outline, cast-outline, charging-outline, checkmark-circle-2-outline, checkmark-circle-outline, checkmark-outline, checkmark-square-2-outline, checkmark-square-outline, chevron-down-outline, chevron-left-outline, chevron-right-outline, chevron-up-outline, clipboard-outline, clock-outline, close-circle-outline, close-outline, close-square-outline, cloud-download-outline, cloud-upload-outline, code-download-outline, code-outline, collapse-outline, color-palette-outline, color-picker-outline, compass-outline, copy-outline, corner-down-left-outline, corner-down-right-outline, corner-left-down-outline, corner-left-up-outline, corner-right-down-outline, corner-right-up-outline, corner-up-left-outline, corner-up-right-outline, credit-card-outline, crop-outline, cube-outline, diagonal-arrow-left-down-outline, diagonal-arrow-left-up-outline, diagonal-arrow-right-down-outline, diagonal-arrow-right-up-outline, done-all-outline, download-outline, droplet-off-outline, droplet-outline, edit-2-outline, edit-outline, email-outline, expand-outline, external-link-outline, eye-off-2-outline, eye-off-outline, eye-outline, facebook-outline, file-add-outline, file-outline, file-remove-outline, file-text-outline, film-outline, flag-outline, flash-off-outline, flash-outline, flip-2-outline, flip-outline, folder-add-outline, folder-outline, folder-remove-outline, funnel-outline, gift-outline, github-outline, globe-2-outline, globe-outline, google-outline, grid-outline, hard-drive-outline, hash-outline, headphones-outline, heart-outline, home-outline, image-outline, inbox-outline, info-outline, keypad-outline, layers-outline, layout-outline, link-2-outline, link-outline, linkedin-outline, list-outline, loader-outline, lock-outline, log-in-outline, log-out-outline, map-outline, maximize-outline, menu-2-outline, menu-arrow-outline, menu-outline, message-circle-outline, message-square-outline, mic-off-outline, mic-outline, minimize-outline, minus-circle-outline, minus-outline, minus-square-outline, monitor-outline, moon-outline, more-horizontal-outline, more-vertical-outline, move-outline, music-outline, navigation-2-outline, navigation-outline, npm-outline, options-2-outline, options-outline, pantone-outline, paper-plane-outline, pause-circle-outline, people-outline, percent-outline, person-add-outline, person-delete-outline, person-done-outline, person-outline, person-remove-outline, phone-call-outline, phone-missed-outline, phone-off-outline, phone-outline, pie-chart-outline, pin-outline, play-circle-outline, plus-circle-outline, plus-outline, plus-square-outline, power-outline, pricetags-outline, printer-outline, question-mark-circle-outline, question-mark-outline, radio-button-off-outline, radio-button-on-outline, radio-outline, recording-outline, refresh-outline, repeat-outline, rewind-left-outline, rewind-right-outline, save-outline, scissors-outline, search-outline, settings-2-outline, settings-outline, shake-outline, share-outline, shield-off-outline, shield-outline, shopping-bag-outline, shopping-cart-outline, shuffle-2-outline, shuffle-outline, skip-back-outline, skip-forward-outline, slash-outline, smartphone-outline, smiling-face-outline, speaker-outline, square-outline, star-outline, stop-circle-outline, sun-outline, swap-outline, sync-outline, text-outline, thermometer-minus-outline, thermometer-outline, thermometer-plus-outline, toggle-left-outline, toggle-right-outline, trash-2-outline, trash-outline, trending-down-outline, trending-up-outline, tv-outline, twitter-outline, umbrella-outline, undo-outline, unlock-outline, upload-outline, video-off-outline, video-outline, volume-down-outline, volume-mute-outline, volume-off-outline, volume-up-outline, wifi-off-outline, wifi-outline, default */
/***/ (function(module) {

module.exports = {"activity":"<g data-name=\"Layer 2\"><g data-name=\"activity\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M14.33 20h-.21a2 2 0 0 1-1.76-1.58L9.68 6l-2.76 6.4A1 1 0 0 1 6 13H3a1 1 0 0 1 0-2h2.34l2.51-5.79a2 2 0 0 1 3.79.38L14.32 18l2.76-6.38A1 1 0 0 1 18 11h3a1 1 0 0 1 0 2h-2.34l-2.51 5.79A2 2 0 0 1 14.33 20z\"/></g></g>","alert-circle":"<g data-name=\"Layer 2\"><g data-name=\"alert-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 15a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm1-4a1 1 0 0 1-2 0V8a1 1 0 0 1 2 0z\"/></g></g>","alert-triangle":"<g data-name=\"Layer 2\"><g data-name=\"alert-triangle\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M22.56 16.3L14.89 3.58a3.43 3.43 0 0 0-5.78 0L1.44 16.3a3 3 0 0 0-.05 3A3.37 3.37 0 0 0 4.33 21h15.34a3.37 3.37 0 0 0 2.94-1.66 3 3 0 0 0-.05-3.04zM12 17a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm1-4a1 1 0 0 1-2 0V9a1 1 0 0 1 2 0z\"/></g></g>","archive":"<g data-name=\"Layer 2\"><g data-name=\"archive\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-2 5.22V18a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8.22A3 3 0 0 0 18 3zm-3 10.13a.87.87 0 0 1-.87.87H9.87a.87.87 0 0 1-.87-.87v-.26a.87.87 0 0 1 .87-.87h4.26a.87.87 0 0 1 .87.87zM18 7H6a1 1 0 0 1 0-2h12a1 1 0 0 1 0 2z\"/></g></g>","arrow-back":"<g data-name=\"Layer 2\"><g data-name=\"arrow-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M19 11H7.14l3.63-4.36a1 1 0 1 0-1.54-1.28l-5 6a1.19 1.19 0 0 0-.09.15c0 .05 0 .08-.07.13A1 1 0 0 0 4 12a1 1 0 0 0 .07.36c0 .05 0 .08.07.13a1.19 1.19 0 0 0 .09.15l5 6A1 1 0 0 0 10 19a1 1 0 0 0 .64-.23 1 1 0 0 0 .13-1.41L7.14 13H19a1 1 0 0 0 0-2z\"/></g></g>","arrow-circle-down":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm3.69 11.86l-3 2.86a.49.49 0 0 1-.15.1.54.54 0 0 1-.16.1.94.94 0 0 1-.76 0 1 1 0 0 1-.33-.21l-3-3a1 1 0 0 1 1.42-1.42l1.29 1.3V8a1 1 0 0 1 2 0v5.66l1.31-1.25a1 1 0 0 1 1.38 1.45z\"/></g></g>","arrow-circle-left":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M22 12a10 10 0 1 0-10 10 10 10 0 0 0 10-10zm-11.86 3.69l-2.86-3a.49.49 0 0 1-.1-.15.54.54 0 0 1-.1-.16.94.94 0 0 1 0-.76 1 1 0 0 1 .21-.33l3-3a1 1 0 0 1 1.42 1.42L10.41 11H16a1 1 0 0 1 0 2h-5.66l1.25 1.31a1 1 0 0 1-1.45 1.38z\"/></g></g>","arrow-circle-right":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M2 12A10 10 0 1 0 12 2 10 10 0 0 0 2 12zm11.86-3.69l2.86 3a.49.49 0 0 1 .1.15.54.54 0 0 1 .1.16.94.94 0 0 1 0 .76 1 1 0 0 1-.21.33l-3 3a1 1 0 0 1-1.42-1.42l1.3-1.29H8a1 1 0 0 1 0-2h5.66l-1.25-1.31a1 1 0 0 1 1.45-1.38z\"/></g></g>","arrow-circle-up":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 22A10 10 0 1 0 2 12a10 10 0 0 0 10 10zM8.31 10.14l3-2.86a.49.49 0 0 1 .15-.1.54.54 0 0 1 .16-.1.94.94 0 0 1 .76 0 1 1 0 0 1 .33.21l3 3a1 1 0 0 1-1.42 1.42L13 10.41V16a1 1 0 0 1-2 0v-5.66l-1.31 1.25a1 1 0 0 1-1.38-1.45z\"/></g></g>","arrow-down":"<g data-name=\"Layer 2\"><g data-name=\"arrow-downward\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M12 17a1.72 1.72 0 0 1-1.33-.64l-4.21-5.1a2.1 2.1 0 0 1-.26-2.21A1.76 1.76 0 0 1 7.79 8h8.42a1.76 1.76 0 0 1 1.59 1.05 2.1 2.1 0 0 1-.26 2.21l-4.21 5.1A1.72 1.72 0 0 1 12 17z\"/></g></g>","arrow-downward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.77 13.36a1 1 0 0 0-1.41-.13L13 16.86V5a1 1 0 0 0-2 0v11.86l-4.36-3.63a1 1 0 1 0-1.28 1.54l6 5 .15.09.13.07a1 1 0 0 0 .72 0l.13-.07.15-.09 6-5a1 1 0 0 0 .13-1.41z\"/></g></g>","arrow-forward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-forward\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M5 13h11.86l-3.63 4.36a1 1 0 0 0 1.54 1.28l5-6a1.19 1.19 0 0 0 .09-.15c0-.05.05-.08.07-.13A1 1 0 0 0 20 12a1 1 0 0 0-.07-.36c0-.05-.05-.08-.07-.13a1.19 1.19 0 0 0-.09-.15l-5-6A1 1 0 0 0 14 5a1 1 0 0 0-.64.23 1 1 0 0 0-.13 1.41L16.86 11H5a1 1 0 0 0 0 2z\"/></g></g>","arrow-ios-back":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.83 19a1 1 0 0 1-.78-.37l-4.83-6a1 1 0 0 1 0-1.27l5-6a1 1 0 0 1 1.54 1.28L10.29 12l4.32 5.36a1 1 0 0 1-.78 1.64z\"/></g></g>","arrow-ios-downward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-downward\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z\"/></g></g>","arrow-ios-forward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-forward\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z\"/></g></g>","arrow-ios-upward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-upward\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 15a1 1 0 0 1-.64-.23L12 10.29l-5.37 4.32a1 1 0 0 1-1.41-.15 1 1 0 0 1 .15-1.41l6-4.83a1 1 0 0 1 1.27 0l6 5a1 1 0 0 1 .13 1.41A1 1 0 0 1 18 15z\"/></g></g>","arrow-left":"<g data-name=\"Layer 2\"><g data-name=\"arrow-left\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.54 18a2.06 2.06 0 0 1-1.3-.46l-5.1-4.21a1.7 1.7 0 0 1 0-2.66l5.1-4.21a2.1 2.1 0 0 1 2.21-.26 1.76 1.76 0 0 1 1.05 1.59v8.42a1.76 1.76 0 0 1-1.05 1.59 2.23 2.23 0 0 1-.91.2z\"/></g></g>","arrow-right":"<g data-name=\"Layer 2\"><g data-name=\"arrow-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M10.46 18a2.23 2.23 0 0 1-.91-.2 1.76 1.76 0 0 1-1.05-1.59V7.79A1.76 1.76 0 0 1 9.55 6.2a2.1 2.1 0 0 1 2.21.26l5.1 4.21a1.7 1.7 0 0 1 0 2.66l-5.1 4.21a2.06 2.06 0 0 1-1.3.46z\"/></g></g>","arrow-up":"<g data-name=\"Layer 2\"><g data-name=\"arrow-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M16.21 16H7.79a1.76 1.76 0 0 1-1.59-1 2.1 2.1 0 0 1 .26-2.21l4.21-5.1a1.76 1.76 0 0 1 2.66 0l4.21 5.1A2.1 2.1 0 0 1 17.8 15a1.76 1.76 0 0 1-1.59 1z\"/></g></g>","arrow-upward":"<g data-name=\"Layer 2\"><g data-name=\"arrow-upward\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M5.23 10.64a1 1 0 0 0 1.41.13L11 7.14V19a1 1 0 0 0 2 0V7.14l4.36 3.63a1 1 0 1 0 1.28-1.54l-6-5-.15-.09-.13-.07a1 1 0 0 0-.72 0l-.13.07-.15.09-6 5a1 1 0 0 0-.13 1.41z\"/></g></g>","arrowhead-down":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.37 12.39L12 16.71l-5.36-4.48a1 1 0 1 0-1.28 1.54l6 5a1 1 0 0 0 1.27 0l6-4.83a1 1 0 0 0 .15-1.41 1 1 0 0 0-1.41-.14z\"/><path d=\"M11.36 11.77a1 1 0 0 0 1.27 0l6-4.83a1 1 0 0 0 .15-1.41 1 1 0 0 0-1.41-.15L12 9.71 6.64 5.23a1 1 0 0 0-1.28 1.54z\"/></g></g>","arrowhead-left":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M11.64 5.23a1 1 0 0 0-1.41.13l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63L7.29 12l4.48-5.37a1 1 0 0 0-.13-1.4z\"/><path d=\"M14.29 12l4.48-5.37a1 1 0 0 0-1.54-1.28l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63z\"/></g></g>","arrowhead-right":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M18.78 11.37l-4.78-6a1 1 0 0 0-1.41-.15 1 1 0 0 0-.15 1.41L16.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 13 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/><path d=\"M7 5.37a1 1 0 0 0-1.61 1.26L9.71 12l-4.48 5.36a1 1 0 0 0 .13 1.41A1 1 0 0 0 6 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 0-1.27z\"/></g></g>","arrowhead-up":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M6.63 11.61L12 7.29l5.37 4.48A1 1 0 0 0 18 12a1 1 0 0 0 .77-.36 1 1 0 0 0-.13-1.41l-6-5a1 1 0 0 0-1.27 0l-6 4.83a1 1 0 0 0-.15 1.41 1 1 0 0 0 1.41.14z\"/><path d=\"M12.64 12.23a1 1 0 0 0-1.27 0l-6 4.83a1 1 0 0 0-.15 1.41 1 1 0 0 0 1.41.15L12 14.29l5.37 4.48A1 1 0 0 0 18 19a1 1 0 0 0 .77-.36 1 1 0 0 0-.13-1.41z\"/></g></g>","at":"<g data-name=\"Layer 2\"><g data-name=\"at\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13 2a10 10 0 0 0-5 19.1 10.15 10.15 0 0 0 4 .9 10 10 0 0 0 6.08-2.06 1 1 0 0 0 .19-1.4 1 1 0 0 0-1.41-.19A8 8 0 1 1 12.77 4 8.17 8.17 0 0 1 20 12.22v.68a1.71 1.71 0 0 1-1.78 1.7 1.82 1.82 0 0 1-1.62-1.88V8.4a1 1 0 0 0-1-1 1 1 0 0 0-1 .87 5 5 0 0 0-3.44-1.36A5.09 5.09 0 1 0 15.31 15a3.6 3.6 0 0 0 5.55.61A3.67 3.67 0 0 0 22 12.9v-.68A10.2 10.2 0 0 0 13 2zm-1.82 13.09A3.09 3.09 0 1 1 14.27 12a3.1 3.1 0 0 1-3.09 3.09z\"/></g></g>","attach-2":"<g data-name=\"Layer 2\"><g data-name=\"attach-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a5.86 5.86 0 0 1-6-5.7V6.13A4.24 4.24 0 0 1 10.33 2a4.24 4.24 0 0 1 4.34 4.13v10.18a2.67 2.67 0 0 1-5.33 0V6.92a1 1 0 0 1 1-1 1 1 0 0 1 1 1v9.39a.67.67 0 0 0 1.33 0V6.13A2.25 2.25 0 0 0 10.33 4 2.25 2.25 0 0 0 8 6.13V16.3a3.86 3.86 0 0 0 4 3.7 3.86 3.86 0 0 0 4-3.7V6.13a1 1 0 1 1 2 0V16.3a5.86 5.86 0 0 1-6 5.7z\"/></g></g>","attach":"<g data-name=\"Layer 2\"><g data-name=\"attach\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.29 21a6.23 6.23 0 0 1-4.43-1.88 6 6 0 0 1-.22-8.49L12 3.2A4.11 4.11 0 0 1 15 2a4.48 4.48 0 0 1 3.19 1.35 4.36 4.36 0 0 1 .15 6.13l-7.4 7.43a2.54 2.54 0 0 1-1.81.75 2.72 2.72 0 0 1-1.95-.82 2.68 2.68 0 0 1-.08-3.77l6.83-6.86a1 1 0 0 1 1.37 1.41l-6.83 6.86a.68.68 0 0 0 .08.95.78.78 0 0 0 .53.23.56.56 0 0 0 .4-.16l7.39-7.43a2.36 2.36 0 0 0-.15-3.31 2.38 2.38 0 0 0-3.27-.15L6.06 12a4 4 0 0 0 .22 5.67 4.22 4.22 0 0 0 3 1.29 3.67 3.67 0 0 0 2.61-1.06l7.39-7.43a1 1 0 1 1 1.42 1.41l-7.39 7.43A5.65 5.65 0 0 1 9.29 21z\"/></g></g>","award":"<g data-name=\"Layer 2\"><g data-name=\"award\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 20.75l-2.31-9A5.94 5.94 0 0 0 18 8 6 6 0 0 0 6 8a5.94 5.94 0 0 0 1.34 3.77L5 20.75a1 1 0 0 0 1.48 1.11l5.33-3.13 5.68 3.14A.91.91 0 0 0 18 22a1 1 0 0 0 1-1.25zM12 4a4 4 0 1 1-4 4 4 4 0 0 1 4-4z\"/></g></g>","backspace":"<g data-name=\"Layer 2\"><g data-name=\"backspace\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.14 4h-9.77a3 3 0 0 0-2 .78l-.1.11-6 7.48a1 1 0 0 0 .11 1.37l6 5.48a3 3 0 0 0 2 .78h9.77A1.84 1.84 0 0 0 22 18.18V5.82A1.84 1.84 0 0 0 20.14 4zm-3.43 9.29a1 1 0 0 1 0 1.42 1 1 0 0 1-1.42 0L14 13.41l-1.29 1.3a1 1 0 0 1-1.42 0 1 1 0 0 1 0-1.42l1.3-1.29-1.3-1.29a1 1 0 0 1 1.42-1.42l1.29 1.3 1.29-1.3a1 1 0 0 1 1.42 1.42L15.41 12z\"/></g></g>","bar-chart-2":"<g data-name=\"Layer 2\"><g data-name=\"bar-chart-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M12 8a1 1 0 0 0-1 1v11a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/><path d=\"M19 4a1 1 0 0 0-1 1v15a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/><path d=\"M5 12a1 1 0 0 0-1 1v7a1 1 0 0 0 2 0v-7a1 1 0 0 0-1-1z\"/></g></g>","bar-chart":"<g data-name=\"Layer 2\"><g data-name=\"bar-chart\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M12 4a1 1 0 0 0-1 1v15a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/><path d=\"M19 12a1 1 0 0 0-1 1v7a1 1 0 0 0 2 0v-7a1 1 0 0 0-1-1z\"/><path d=\"M5 8a1 1 0 0 0-1 1v11a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/></g></g>","battery":"<g data-name=\"Layer 2\"><g data-name=\"battery\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15.83 6H4.17A2.31 2.31 0 0 0 2 8.43v7.14A2.31 2.31 0 0 0 4.17 18h11.66A2.31 2.31 0 0 0 18 15.57V8.43A2.31 2.31 0 0 0 15.83 6z\"/><path d=\"M21 9a1 1 0 0 0-1 1v4a1 1 0 0 0 2 0v-4a1 1 0 0 0-1-1z\"/></g></g>","behance":"<g data-name=\"Layer 2\"><g data-name=\"behance\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.76 11.19a1 1 0 0 0-1 1.09h2.06a1 1 0 0 0-1.06-1.09z\"/><path d=\"M9.49 12.3H8.26v1.94h1c1 0 1.44-.33 1.44-1s-.46-.94-1.21-.94z\"/><path d=\"M10.36 10.52c0-.53-.35-.85-.95-.85H8.26v1.74h.85c.89 0 1.25-.32 1.25-.89z\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zM9.7 15.2H7V8.7h2.7c1.17 0 1.94.61 1.94 1.6a1.4 1.4 0 0 1-1.12 1.43A1.52 1.52 0 0 1 12 13.37c0 1.16-1 1.83-2.3 1.83zm3.55-6h3v.5h-3zM17 13.05h-3.3v.14a1.07 1.07 0 0 0 1.09 1.19.9.9 0 0 0 1-.63H17a2 2 0 0 1-2.17 1.55 2.15 2.15 0 0 1-2.36-2.3v-.44a2.11 2.11 0 0 1 2.28-2.25A2.12 2.12 0 0 1 17 12.58z\"/></g></g>","bell-off":"<g data-name=\"Layer 2\"><g data-name=\"bell-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15.88 18.71l-.59-.59L14 16.78l-.07-.07L6.58 9.4 5.31 8.14a5.68 5.68 0 0 0 0 .59v4.67l-1.8 1.81A1.64 1.64 0 0 0 4.64 18H8v.34A3.84 3.84 0 0 0 12 22a3.88 3.88 0 0 0 4-3.22zM14 18.34A1.88 1.88 0 0 1 12 20a1.88 1.88 0 0 1-2-1.66V18h4z\"/><path d=\"M7.13 4.3l1.46 1.46 9.53 9.53 2 2 .31.3a1.58 1.58 0 0 0 .45-.6 1.62 1.62 0 0 0-.35-1.78l-1.8-1.81V8.94a6.86 6.86 0 0 0-5.83-6.88 6.71 6.71 0 0 0-5.32 1.61 6.88 6.88 0 0 0-.58.54z\"/><path d=\"M20.71 19.29L19.41 18l-2-2-9.52-9.53L6.42 5 4.71 3.29a1 1 0 0 0-1.42 1.42L5.53 7l1.75 1.7 7.31 7.3.07.07L16 17.41l.59.59 2.7 2.71a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","bell":"<g data-name=\"Layer 2\"><g data-name=\"bell\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.52 15.21l-1.8-1.81V8.94a6.86 6.86 0 0 0-5.82-6.88 6.74 6.74 0 0 0-7.62 6.67v4.67l-1.8 1.81A1.64 1.64 0 0 0 4.64 18H8v.34A3.84 3.84 0 0 0 12 22a3.84 3.84 0 0 0 4-3.66V18h3.36a1.64 1.64 0 0 0 1.16-2.79zM14 18.34A1.88 1.88 0 0 1 12 20a1.88 1.88 0 0 1-2-1.66V18h4z\"/></g></g>","bluetooth":"<g data-name=\"Layer 2\"><g data-name=\"bluetooth\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.63 12l4-3.79a1.14 1.14 0 0 0-.13-1.77l-4.67-3.23a1.17 1.17 0 0 0-1.21-.08 1.15 1.15 0 0 0-.62 1v6.2l-3.19-4a1 1 0 0 0-1.56 1.3L9.72 12l-3.5 4.43a1 1 0 0 0 .16 1.4A1 1 0 0 0 7 18a1 1 0 0 0 .78-.38L11 13.56v6.29A1.16 1.16 0 0 0 12.16 21a1.16 1.16 0 0 0 .67-.21l4.64-3.18a1.17 1.17 0 0 0 .49-.85 1.15 1.15 0 0 0-.34-.91zM13 5.76l2.5 1.73L13 9.85zm0 12.49v-4.07l2.47 2.38z\"/></g></g>","book-open":"<g data-name=\"Layer 2\"><g data-name=\"book-open\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M21 4.34a1.24 1.24 0 0 0-1.08-.23L13 5.89v14.27l7.56-1.94A1.25 1.25 0 0 0 21.5 17V5.32a1.25 1.25 0 0 0-.5-.98z\"/><path d=\"M11 5.89L4.06 4.11A1.27 1.27 0 0 0 3 4.34a1.25 1.25 0 0 0-.48 1V17a1.25 1.25 0 0 0 .94 1.21L11 20.16z\"/></g></g>","book":"<g data-name=\"Layer 2\"><g data-name=\"book\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 3H7a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM7 19a1 1 0 0 1 0-2h11v2z\"/></g></g>","bookmark":"<g data-name=\"Layer 2\"><g data-name=\"bookmark\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M6 21a1 1 0 0 1-.49-.13A1 1 0 0 1 5 20V5.33A2.28 2.28 0 0 1 7.2 3h9.6A2.28 2.28 0 0 1 19 5.33V20a1 1 0 0 1-.5.86 1 1 0 0 1-1 0l-5.67-3.21-5.33 3.2A1 1 0 0 1 6 21z\"/></g></g>","briefcase":"<g data-name=\"Layer 2\"><g data-name=\"briefcase\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M7 21h10V7h-1V5.5A2.5 2.5 0 0 0 13.5 3h-3A2.5 2.5 0 0 0 8 5.5V7H7zm3-15.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5V7h-4z\"/><path d=\"M19 7v14a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3z\"/><path d=\"M5 7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3z\"/></g></g>","browser":"<g data-name=\"Layer 2\"><g data-name=\"browser\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm-6 3a1 1 0 1 1-1 1 1 1 0 0 1 1-1zM8 6a1 1 0 1 1-1 1 1 1 0 0 1 1-1zm11 12a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1v-7h14z\"/></g></g>","brush":"<g data-name=\"Layer 2\"><g data-name=\"brush\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M7.12 12.55a4 4 0 0 0-3.07 3.86v3.11a.47.47 0 0 0 .48.48l3.24-.06a3.78 3.78 0 0 0 3.44-2.2 3.65 3.65 0 0 0-4.09-5.19z\"/><path d=\"M19.26 4.46a2.14 2.14 0 0 0-2.88.21L10 11.08a.47.47 0 0 0 0 .66L12.25 14a.47.47 0 0 0 .66 0l6.49-6.47a2.06 2.06 0 0 0 .6-1.47 2 2 0 0 0-.74-1.6z\"/></g></g>","bulb":"<g data-name=\"Layer 2\"><g data-name=\"bulb\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 7a5 5 0 0 0-3 9v4a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-4a5 5 0 0 0-3-9z\"/><path d=\"M12 6a1 1 0 0 0 1-1V3a1 1 0 0 0-2 0v2a1 1 0 0 0 1 1z\"/><path d=\"M21 11h-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M5 11H3a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M7.66 6.42L6.22 5a1 1 0 0 0-1.39 1.47l1.44 1.39a1 1 0 0 0 .73.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.06-1.41z\"/><path d=\"M19.19 5.05a1 1 0 0 0-1.41 0l-1.44 1.37a1 1 0 0 0 0 1.41 1 1 0 0 0 .72.31 1 1 0 0 0 .69-.28l1.44-1.39a1 1 0 0 0 0-1.42z\"/></g></g>","calendar":"<g data-name=\"Layer 2\"><g data-name=\"calendar\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 4h-1V3a1 1 0 0 0-2 0v1H9V3a1 1 0 0 0-2 0v1H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zM8 17a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm8 0h-4a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2zm3-6H5V7a1 1 0 0 1 1-1h1v1a1 1 0 0 0 2 0V6h6v1a1 1 0 0 0 2 0V6h1a1 1 0 0 1 1 1z\"/></g></g>","camera":"<g data-name=\"Layer 2\"><g data-name=\"camera\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"14\" r=\"1.5\"/><path d=\"M19 7h-3V5.5A2.5 2.5 0 0 0 13.5 3h-3A2.5 2.5 0 0 0 8 5.5V7H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-9-1.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5V7h-4zm2 12a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5z\"/></g></g>","car":"<g data-name=\"Layer 2\"><g data-name=\"car\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.6 11.22L17 7.52V5a1.91 1.91 0 0 0-1.81-2H3.79A1.91 1.91 0 0 0 2 5v10a2 2 0 0 0 1.2 1.88 3 3 0 1 0 5.6.12h6.36a3 3 0 1 0 5.64 0h.2a1 1 0 0 0 1-1v-4a1 1 0 0 0-.4-.78zM20 12.48V15h-3v-4.92zM7 18a1 1 0 1 1-1-1 1 1 0 0 1 1 1zm12 0a1 1 0 1 1-1-1 1 1 0 0 1 1 1z\"/></g></g>","cast":"<g data-name=\"Layer 2\"><g data-name=\"cast\"><polyline points=\"24 24 0 24 0 0\" opacity=\"0\"/><path d=\"M18.4 3H5.6A2.7 2.7 0 0 0 3 5.78V7a1 1 0 0 0 2 0V5.78A.72.72 0 0 1 5.6 5h12.8a.72.72 0 0 1 .6.78v12.44a.72.72 0 0 1-.6.78H17a1 1 0 0 0 0 2h1.4a2.7 2.7 0 0 0 2.6-2.78V5.78A2.7 2.7 0 0 0 18.4 3z\"/><path d=\"M3.86 14A1 1 0 0 0 3 15.17a1 1 0 0 0 1.14.83 2.49 2.49 0 0 1 2.12.72 2.52 2.52 0 0 1 .51 2.84 1 1 0 0 0 .48 1.33 1.06 1.06 0 0 0 .42.09 1 1 0 0 0 .91-.58A4.52 4.52 0 0 0 3.86 14z\"/><path d=\"M3.86 10.08a1 1 0 0 0 .28 2 6 6 0 0 1 5.09 1.71 6 6 0 0 1 1.53 5.95 1 1 0 0 0 .68 1.26.9.9 0 0 0 .28 0 1 1 0 0 0 1-.72 8 8 0 0 0-8.82-10.2z\"/><circle cx=\"4\" cy=\"19\" r=\"1\"/></g></g>","charging":"<g data-name=\"Layer 2\"><g data-name=\"charging\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M11.28 13H7a1 1 0 0 1-.86-.5 1 1 0 0 1 0-1L9.28 6H4.17A2.31 2.31 0 0 0 2 8.43v7.14A2.31 2.31 0 0 0 4.17 18h4.25z\"/><path d=\"M15.83 6h-4.25l-2.86 5H13a1 1 0 0 1 .86.5 1 1 0 0 1 0 1L10.72 18h5.11A2.31 2.31 0 0 0 18 15.57V8.43A2.31 2.31 0 0 0 15.83 6z\"/><path d=\"M21 9a1 1 0 0 0-1 1v4a1 1 0 0 0 2 0v-4a1 1 0 0 0-1-1z\"/></g></g>","checkmark-circle-2":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-circle-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm4.3 7.61l-4.57 6a1 1 0 0 1-.79.39 1 1 0 0 1-.79-.38l-2.44-3.11a1 1 0 0 1 1.58-1.23l1.63 2.08 3.78-5a1 1 0 1 1 1.6 1.22z\"/></g></g>","checkmark-circle":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.71 11.29a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 16a1 1 0 0 0 .72-.34l7-8a1 1 0 0 0-1.5-1.32L12 13.54z\"/><path d=\"M21 11a1 1 0 0 0-1 1 8 8 0 0 1-8 8A8 8 0 0 1 6.33 6.36 7.93 7.93 0 0 1 12 4a8.79 8.79 0 0 1 1.9.22 1 1 0 1 0 .47-1.94A10.54 10.54 0 0 0 12 2a10 10 0 0 0-7 17.09A9.93 9.93 0 0 0 12 22a10 10 0 0 0 10-10 1 1 0 0 0-1-1z\"/></g></g>","checkmark-square-2":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-square-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm-1.7 6.61l-4.57 6a1 1 0 0 1-.79.39 1 1 0 0 1-.79-.38l-2.44-3.11a1 1 0 0 1 1.58-1.23l1.63 2.08 3.78-5a1 1 0 1 1 1.6 1.22z\"/></g></g>","checkmark-square":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 11.83a1 1 0 0 0-1 1v5.57a.6.6 0 0 1-.6.6H5.6a.6.6 0 0 1-.6-.6V5.6a.6.6 0 0 1 .6-.6h9.57a1 1 0 1 0 0-2H5.6A2.61 2.61 0 0 0 3 5.6v12.8A2.61 2.61 0 0 0 5.6 21h12.8a2.61 2.61 0 0 0 2.6-2.6v-5.57a1 1 0 0 0-1-1z\"/><path d=\"M10.72 11a1 1 0 0 0-1.44 1.38l2.22 2.33a1 1 0 0 0 .72.31 1 1 0 0 0 .72-.3l6.78-7a1 1 0 1 0-1.44-1.4l-6.05 6.26z\"/></g></g>","checkmark":"<g data-name=\"Layer 2\"><g data-name=\"checkmark\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.86 18a1 1 0 0 1-.73-.32l-4.86-5.17a1 1 0 1 1 1.46-1.37l4.12 4.39 8.41-9.2a1 1 0 1 1 1.48 1.34l-9.14 10a1 1 0 0 1-.73.33z\"/></g></g>","chevron-down":"<g data-name=\"Layer 2\"><g data-name=\"chevron-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 15.5a1 1 0 0 1-.71-.29l-4-4a1 1 0 1 1 1.42-1.42L12 13.1l3.3-3.18a1 1 0 1 1 1.38 1.44l-4 3.86a1 1 0 0 1-.68.28z\"/></g></g>","chevron-left":"<g data-name=\"Layer 2\"><g data-name=\"chevron-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.36 17a1 1 0 0 1-.72-.31l-3.86-4a1 1 0 0 1 0-1.4l4-4a1 1 0 1 1 1.42 1.42L10.9 12l3.18 3.3a1 1 0 0 1 0 1.41 1 1 0 0 1-.72.29z\"/></g></g>","chevron-right":"<g data-name=\"Layer 2\"><g data-name=\"chevron-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M10.5 17a1 1 0 0 1-.71-.29 1 1 0 0 1 0-1.42L13.1 12 9.92 8.69a1 1 0 0 1 0-1.41 1 1 0 0 1 1.42 0l3.86 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-.7.32z\"/></g></g>","chevron-up":"<g data-name=\"Layer 2\"><g data-name=\"chevron-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M16 14.5a1 1 0 0 1-.71-.29L12 10.9l-3.3 3.18a1 1 0 0 1-1.41 0 1 1 0 0 1 0-1.42l4-3.86a1 1 0 0 1 1.4 0l4 4a1 1 0 0 1 0 1.42 1 1 0 0 1-.69.28z\"/></g></g>","clipboard":"<g data-name=\"Layer 2\"><g data-name=\"clipboard\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 4v3a2 2 0 0 1-2 2H8a2 2 0 0 1-2-2V4a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3z\"/><rect x=\"7\" y=\"2\" width=\"10\" height=\"6\" rx=\"1\" ry=\"1\"/></g></g>","clock":"<g data-name=\"Layer 2\"><g data-name=\"clock\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm4 11h-4a1 1 0 0 1-1-1V8a1 1 0 0 1 2 0v3h3a1 1 0 0 1 0 2z\"/></g></g>","close-circle":"<g data-name=\"Layer 2\"><g data-name=\"close-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm2.71 11.29a1 1 0 0 1 0 1.42 1 1 0 0 1-1.42 0L12 13.41l-1.29 1.3a1 1 0 0 1-1.42 0 1 1 0 0 1 0-1.42l1.3-1.29-1.3-1.29a1 1 0 0 1 1.42-1.42l1.29 1.3 1.29-1.3a1 1 0 0 1 1.42 1.42L13.41 12z\"/></g></g>","close-square":"<g data-name=\"Layer 2\"><g data-name=\"close-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm-3.29 10.29a1 1 0 0 1 0 1.42 1 1 0 0 1-1.42 0L12 13.41l-1.29 1.3a1 1 0 0 1-1.42 0 1 1 0 0 1 0-1.42l1.3-1.29-1.3-1.29a1 1 0 0 1 1.42-1.42l1.29 1.3 1.29-1.3a1 1 0 0 1 1.42 1.42L13.41 12z\"/></g></g>","close":"<g data-name=\"Layer 2\"><g data-name=\"close\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M13.41 12l4.3-4.29a1 1 0 1 0-1.42-1.42L12 10.59l-4.29-4.3a1 1 0 0 0-1.42 1.42l4.3 4.29-4.3 4.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l4.29-4.3 4.29 4.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","cloud-download":"<g data-name=\"Layer 2\"><g data-name=\"cloud-download\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.9 11c0-.11-.06-.22-.09-.33a4.17 4.17 0 0 0-.18-.57c-.05-.12-.12-.24-.18-.37s-.15-.3-.24-.44S21 9.08 21 9s-.2-.25-.31-.37-.21-.2-.32-.3L20 8l-.36-.24a3.68 3.68 0 0 0-.44-.23l-.39-.18a4.13 4.13 0 0 0-.5-.15 3 3 0 0 0-.41-.09h-.18A6 6 0 0 0 6.33 7h-.18a3 3 0 0 0-.41.09 4.13 4.13 0 0 0-.5.15l-.39.18a3.68 3.68 0 0 0-.44.23L4.05 8l-.37.31c-.11.1-.22.19-.32.3s-.21.25-.31.37-.18.23-.26.36-.16.29-.24.44-.13.25-.18.37a4.17 4.17 0 0 0-.18.57c0 .11-.07.22-.09.33A5.23 5.23 0 0 0 2 12a5.5 5.5 0 0 0 .09.91c0 .1.05.19.07.29a5.58 5.58 0 0 0 .18.58l.12.29a5 5 0 0 0 .3.56l.14.22a.56.56 0 0 0 .05.08L3 15a5 5 0 0 0 4 2 2 2 0 0 1 .59-1.41A2 2 0 0 1 9 15a1.92 1.92 0 0 1 1 .27V12a2 2 0 0 1 4 0v3.37a2 2 0 0 1 1-.27 2.05 2.05 0 0 1 1.44.61A2 2 0 0 1 17 17a5 5 0 0 0 4-2l.05-.05a.56.56 0 0 0 .05-.08l.14-.22a5 5 0 0 0 .3-.56l.12-.29a5.58 5.58 0 0 0 .18-.58c0-.1.05-.19.07-.29A5.5 5.5 0 0 0 22 12a5.23 5.23 0 0 0-.1-1z\"/><path d=\"M14.31 16.38L13 17.64V12a1 1 0 0 0-2 0v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 21a1 1 0 0 0 .69-.28l3-2.9a1 1 0 1 0-1.38-1.44z\"/><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.9 11c0-.11-.06-.22-.09-.33a4.17 4.17 0 0 0-.18-.57c-.05-.12-.12-.24-.18-.37s-.15-.3-.24-.44S21 9.08 21 9s-.2-.25-.31-.37-.21-.2-.32-.3L20 8l-.36-.24a3.68 3.68 0 0 0-.44-.23l-.39-.18a4.13 4.13 0 0 0-.5-.15 3 3 0 0 0-.41-.09h-.18A6 6 0 0 0 6.33 7h-.18a3 3 0 0 0-.41.09 4.13 4.13 0 0 0-.5.15l-.39.18a3.68 3.68 0 0 0-.44.23L4.05 8l-.37.31c-.11.1-.22.19-.32.3s-.21.25-.31.37-.18.23-.26.36-.16.29-.24.44-.13.25-.18.37a4.17 4.17 0 0 0-.18.57c0 .11-.07.22-.09.33A5.23 5.23 0 0 0 2 12a5.5 5.5 0 0 0 .09.91c0 .1.05.19.07.29a5.58 5.58 0 0 0 .18.58l.12.29a5 5 0 0 0 .3.56l.14.22a.56.56 0 0 0 .05.08L3 15a5 5 0 0 0 4 2 2 2 0 0 1 .59-1.41A2 2 0 0 1 9 15a1.92 1.92 0 0 1 1 .27V12a2 2 0 0 1 4 0v3.37a2 2 0 0 1 1-.27 2.05 2.05 0 0 1 1.44.61A2 2 0 0 1 17 17a5 5 0 0 0 4-2l.05-.05a.56.56 0 0 0 .05-.08l.14-.22a5 5 0 0 0 .3-.56l.12-.29a5.58 5.58 0 0 0 .18-.58c0-.1.05-.19.07-.29A5.5 5.5 0 0 0 22 12a5.23 5.23 0 0 0-.1-1z\"/><path d=\"M14.31 16.38L13 17.64V12a1 1 0 0 0-2 0v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 21a1 1 0 0 0 .69-.28l3-2.9a1 1 0 1 0-1.38-1.44z\"/></g></g>","cloud-upload":"<g data-name=\"Layer 2\"><g data-name=\"cloud-upload\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.9 12c0-.11-.06-.22-.09-.33a4.17 4.17 0 0 0-.18-.57c-.05-.12-.12-.24-.18-.37s-.15-.3-.24-.44S21 10.08 21 10s-.2-.25-.31-.37-.21-.2-.32-.3L20 9l-.36-.24a3.68 3.68 0 0 0-.44-.23l-.39-.18a4.13 4.13 0 0 0-.5-.15 3 3 0 0 0-.41-.09L17.67 8A6 6 0 0 0 6.33 8l-.18.05a3 3 0 0 0-.41.09 4.13 4.13 0 0 0-.5.15l-.39.18a3.68 3.68 0 0 0-.44.23l-.36.3-.37.31c-.11.1-.22.19-.32.3s-.21.25-.31.37-.18.23-.26.36-.16.29-.24.44-.13.25-.18.37a4.17 4.17 0 0 0-.18.57c0 .11-.07.22-.09.33A5.23 5.23 0 0 0 2 13a5.5 5.5 0 0 0 .09.91c0 .1.05.19.07.29a5.58 5.58 0 0 0 .18.58l.12.29a5 5 0 0 0 .3.56l.14.22a.56.56 0 0 0 .05.08L3 16a5 5 0 0 0 4 2h3v-1.37a2 2 0 0 1-1 .27 2.05 2.05 0 0 1-1.44-.61 2 2 0 0 1 .05-2.83l3-2.9A2 2 0 0 1 12 10a2 2 0 0 1 1.41.59l3 3a2 2 0 0 1 0 2.82A2 2 0 0 1 15 17a1.92 1.92 0 0 1-1-.27V18h3a5 5 0 0 0 4-2l.05-.05a.56.56 0 0 0 .05-.08l.14-.22a5 5 0 0 0 .3-.56l.12-.29a5.58 5.58 0 0 0 .18-.58c0-.1.05-.19.07-.29A5.5 5.5 0 0 0 22 13a5.23 5.23 0 0 0-.1-1z\"/><path d=\"M12.71 11.29a1 1 0 0 0-1.4 0l-3 2.9a1 1 0 1 0 1.38 1.44L11 14.36V20a1 1 0 0 0 2 0v-5.59l1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","code-download":"<g data-name=\"Layer 2\"><g data-name=\"code-download\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M4.29 12l4.48-5.36a1 1 0 1 0-1.54-1.28l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63z\"/><path d=\"M21.78 11.37l-4.78-6a1 1 0 0 0-1.56 1.26L19.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 16 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/><path d=\"M15.72 11.41a1 1 0 0 0-1.41 0L13 12.64V8a1 1 0 0 0-2 0v4.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 16a1 1 0 0 0 .69-.28l3-2.9a1 1 0 0 0 .03-1.41z\"/></g></g>","code":"<g data-name=\"Layer 2\"><g data-name=\"code\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M8.64 5.23a1 1 0 0 0-1.41.13l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63L4.29 12l4.48-5.36a1 1 0 0 0-.13-1.41z\"/><path d=\"M21.78 11.37l-4.78-6a1 1 0 0 0-1.41-.15 1 1 0 0 0-.15 1.41L19.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 16 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/></g></g>","collapse":"<g data-name=\"Layer 2\"><g data-name=\"collapse\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 9h-2.58l3.29-3.29a1 1 0 1 0-1.42-1.42L15 7.57V5a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h5a1 1 0 0 0 0-2z\"/><path d=\"M10 13H5a1 1 0 0 0 0 2h2.57l-3.28 3.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L9 16.42V19a1 1 0 0 0 1 1 1 1 0 0 0 1-1v-5a1 1 0 0 0-1-1z\"/></g></g>","color-palette":"<g data-name=\"Layer 2\"><g data-name=\"color-palette\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.54 5.08A10.61 10.61 0 0 0 11.91 2a10 10 0 0 0-.05 20 2.58 2.58 0 0 0 2.53-1.89 2.52 2.52 0 0 0-.57-2.28.5.5 0 0 1 .37-.83h1.65A6.15 6.15 0 0 0 22 11.33a8.48 8.48 0 0 0-2.46-6.25zm-12.7 9.66a1.5 1.5 0 1 1 .4-2.08 1.49 1.49 0 0 1-.4 2.08zM8.3 9.25a1.5 1.5 0 1 1-.55-2 1.5 1.5 0 0 1 .55 2zM11 7a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 11 7zm5.75.8a1.5 1.5 0 1 1 .55-2 1.5 1.5 0 0 1-.55 2z\"/></g></g>","color-picker":"<g data-name=\"Layer 2\"><g data-name=\"color-picker\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.4 7.34L16.66 4.6A1.92 1.92 0 0 0 14 4.53l-2 2-1.29-1.24a1 1 0 0 0-1.42 1.42L10.53 8 5 13.53a2 2 0 0 0-.57 1.21L4 18.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 20h.09l4.17-.38a2 2 0 0 0 1.21-.57l5.58-5.58 1.24 1.24a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42l-1.24-1.24 2-2a1.92 1.92 0 0 0-.07-2.71zm-13 7.6L12 9.36l2.69 2.7-2.79 2.79\"/></g></g>","compass":"<g data-name=\"Layer 2\"><g data-name=\"compass\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><polygon points=\"10.8 13.21 12.49 12.53 13.2 10.79 11.51 11.47 10.8 13.21\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm3.93 7.42l-1.75 4.26a1 1 0 0 1-.55.55l-4.21 1.7A1 1 0 0 1 9 16a1 1 0 0 1-.71-.31h-.05a1 1 0 0 1-.18-1l1.75-4.26a1 1 0 0 1 .55-.55l4.21-1.7a1 1 0 0 1 1.1.25 1 1 0 0 1 .26.99z\"/></g></g>","copy":"<g data-name=\"Layer 2\"><g data-name=\"copy\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 9h-3V5.67A2.68 2.68 0 0 0 12.33 3H5.67A2.68 2.68 0 0 0 3 5.67v6.66A2.68 2.68 0 0 0 5.67 15H9v3a3 3 0 0 0 3 3h6a3 3 0 0 0 3-3v-6a3 3 0 0 0-3-3zm-9 3v1H5.67a.67.67 0 0 1-.67-.67V5.67A.67.67 0 0 1 5.67 5h6.66a.67.67 0 0 1 .67.67V9h-1a3 3 0 0 0-3 3z\"/></g></g>","corner-down-left":"<g data-name=\"Layer 2\"><g data-name=\"corner-down-left\"><rect x=\".05\" y=\".05\" width=\"24\" height=\"24\" transform=\"rotate(-89.76 12.05 12.05)\" opacity=\"0\"/><path d=\"M20 6a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 1-.29.71A1 1 0 0 1 17 12H8.08l2.69-3.39a1 1 0 0 0-1.52-1.17l-4 5a1 1 0 0 0 0 1.25l4 5a1 1 0 0 0 .78.37 1 1 0 0 0 .62-.22 1 1 0 0 0 .15-1.41l-2.66-3.36h8.92a3 3 0 0 0 3-3z\"/></g></g>","corner-down-right":"<g data-name=\"Layer 2\"><g data-name=\"corner-down-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19.78 12.38l-4-5a1 1 0 0 0-1.56 1.24l2.7 3.38H8a1 1 0 0 1-1-1V6a1 1 0 0 0-2 0v5a3 3 0 0 0 3 3h8.92l-2.7 3.38a1 1 0 0 0 .16 1.4A1 1 0 0 0 15 19a1 1 0 0 0 .78-.38l4-5a1 1 0 0 0 0-1.24z\"/></g></g>","corner-left-down":"<g data-name=\"Layer 2\"><g data-name=\"corner-left-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 5h-5a3 3 0 0 0-3 3v8.92l-3.38-2.7a1 1 0 0 0-1.24 1.56l5 4a1 1 0 0 0 1.24 0l5-4a1 1 0 1 0-1.24-1.56L12 16.92V8a1 1 0 0 1 1-1h5a1 1 0 0 0 0-2z\"/></g></g>","corner-left-up":"<g data-name=\"Layer 2\"><g data-name=\"corner-left-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 17h-5a1 1 0 0 1-1-1V7.08l3.38 2.7A1 1 0 0 0 16 10a1 1 0 0 0 .78-.38 1 1 0 0 0-.16-1.4l-5-4a1 1 0 0 0-1.24 0l-5 4a1 1 0 0 0 1.24 1.56L10 7.08V16a3 3 0 0 0 3 3h5a1 1 0 0 0 0-2z\"/></g></g>","corner-right-down":"<g data-name=\"Layer 2\"><g data-name=\"corner-right-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.78 14.38a1 1 0 0 0-1.4-.16L14 16.92V8a3 3 0 0 0-3-3H6a1 1 0 0 0 0 2h5a1 1 0 0 1 1 1v8.92l-3.38-2.7a1 1 0 0 0-1.24 1.56l5 4a1 1 0 0 0 1.24 0l5-4a1 1 0 0 0 .16-1.4z\"/></g></g>","corner-right-up":"<g data-name=\"Layer 2\"><g data-name=\"corner-right-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.62 8.22l-5-4a1 1 0 0 0-1.24 0l-5 4a1 1 0 0 0 1.24 1.56L12 7.08V16a1 1 0 0 1-1 1H6a1 1 0 0 0 0 2h5a3 3 0 0 0 3-3V7.08l3.38 2.7A1 1 0 0 0 18 10a1 1 0 0 0 .78-.38 1 1 0 0 0-.16-1.4z\"/></g></g>","corner-up-left":"<g data-name=\"Layer 2\"><g data-name=\"corner-up-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M16 10H7.08l2.7-3.38a1 1 0 1 0-1.56-1.24l-4 5a1 1 0 0 0 0 1.24l4 5A1 1 0 0 0 9 17a1 1 0 0 0 .62-.22 1 1 0 0 0 .16-1.4L7.08 12H16a1 1 0 0 1 1 1v5a1 1 0 0 0 2 0v-5a3 3 0 0 0-3-3z\"/></g></g>","corner-up-right":"<g data-name=\"Layer 2\"><g data-name=\"corner-up-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19.78 10.38l-4-5a1 1 0 0 0-1.56 1.24l2.7 3.38H8a3 3 0 0 0-3 3v5a1 1 0 0 0 2 0v-5a1 1 0 0 1 1-1h8.92l-2.7 3.38a1 1 0 0 0 .16 1.4A1 1 0 0 0 15 17a1 1 0 0 0 .78-.38l4-5a1 1 0 0 0 0-1.24z\"/></g></g>","credit-card":"<g data-name=\"Layer 2\"><g data-name=\"credit-card\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 5H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V8a3 3 0 0 0-3-3zm-8 10H7a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2zm6 0h-2a1 1 0 0 1 0-2h2a1 1 0 0 1 0 2zm3-6H4V8a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1z\"/></g></g>","crop":"<g data-name=\"Layer 2\"><g data-name=\"crop\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 16h-3V8.56A2.56 2.56 0 0 0 15.44 6H8V3a1 1 0 0 0-2 0v3H3a1 1 0 0 0 0 2h3v7.44A2.56 2.56 0 0 0 8.56 18H16v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2zM8.56 16a.56.56 0 0 1-.56-.56V8h7.44a.56.56 0 0 1 .56.56V16z\"/></g></g>","cube":"<g data-name=\"Layer 2\"><g data-name=\"cube\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M11.25 11.83L3 8.36v7.73a1.69 1.69 0 0 0 1 1.52L11.19 21h.06z\"/><path d=\"M12 10.5l8.51-3.57a1.62 1.62 0 0 0-.51-.38l-7.2-3.37a1.87 1.87 0 0 0-1.6 0L4 6.55a1.62 1.62 0 0 0-.51.38z\"/><path d=\"M12.75 11.83V21h.05l7.2-3.39a1.69 1.69 0 0 0 1-1.51V8.36z\"/></g></g>","diagonal-arrow-left-down":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-left-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.71 6.29a1 1 0 0 0-1.42 0L8 14.59V9a1 1 0 0 0-2 0v8a1 1 0 0 0 1 1h8a1 1 0 0 0 0-2H9.41l8.3-8.29a1 1 0 0 0 0-1.42z\"/></g></g>","diagonal-arrow-left-up":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-left-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17.71 16.29L9.42 8H15a1 1 0 0 0 0-2H7.05a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1H7a1 1 0 0 0 1-1V9.45l8.26 8.26a1 1 0 0 0 1.42 0 1 1 0 0 0 .03-1.42z\"/></g></g>","diagonal-arrow-right-down":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-right-down\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M17 8a1 1 0 0 0-1 1v5.59l-8.29-8.3a1 1 0 0 0-1.42 1.42l8.3 8.29H9a1 1 0 0 0 0 2h8a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1z\"/></g></g>","diagonal-arrow-right-up":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-right-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 7.05a1 1 0 0 0-1-1L9 6a1 1 0 0 0 0 2h5.56l-8.27 8.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L16 9.42V15a1 1 0 0 0 1 1 1 1 0 0 0 1-1z\"/></g></g>","done-all":"<g data-name=\"Layer 2\"><g data-name=\"done-all\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16.62 6.21a1 1 0 0 0-1.41.17l-7 9-3.43-4.18a1 1 0 1 0-1.56 1.25l4.17 5.18a1 1 0 0 0 .78.37 1 1 0 0 0 .83-.38l7.83-10a1 1 0 0 0-.21-1.41z\"/><path d=\"M21.62 6.21a1 1 0 0 0-1.41.17l-7 9-.61-.75-1.26 1.62 1.1 1.37a1 1 0 0 0 .78.37 1 1 0 0 0 .78-.38l7.83-10a1 1 0 0 0-.21-1.4z\"/><path d=\"M8.71 13.06L10 11.44l-.2-.24a1 1 0 0 0-1.43-.2 1 1 0 0 0-.15 1.41z\"/></g></g>","download":"<g data-name=\"Layer 2\"><g data-name=\"download\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"4\" y=\"18\" width=\"16\" height=\"2\" rx=\"1\" ry=\"1\"/><rect x=\"3\" y=\"17\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 5 18)\"/><rect x=\"17\" y=\"17\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 19 18)\"/><path d=\"M12 15a1 1 0 0 1-.58-.18l-4-2.82a1 1 0 0 1-.24-1.39 1 1 0 0 1 1.4-.24L12 12.76l3.4-2.56a1 1 0 0 1 1.2 1.6l-4 3a1 1 0 0 1-.6.2z\"/><path d=\"M12 13a1 1 0 0 1-1-1V4a1 1 0 0 1 2 0v8a1 1 0 0 1-1 1z\"/></g></g>","droplet-off":"<g data-name=\"Layer 2\"><g data-name=\"droplet-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 16.14A7.73 7.73 0 0 0 17.34 8l-4.56-4.69a1 1 0 0 0-.71-.31 1 1 0 0 0-.72.3L8.74 5.92z\"/><path d=\"M6 8.82a7.73 7.73 0 0 0 .64 9.9A7.44 7.44 0 0 0 11.92 21a7.34 7.34 0 0 0 4.64-1.6z\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","droplet":"<g data-name=\"Layer 2\"><g data-name=\"droplet\"><rect x=\".1\" y=\".1\" width=\"24\" height=\"24\" transform=\"rotate(.48 11.987 11.887)\" opacity=\"0\"/><path d=\"M12 21.1a7.4 7.4 0 0 1-5.28-2.28 7.73 7.73 0 0 1 .1-10.77l4.64-4.65a.94.94 0 0 1 .71-.3 1 1 0 0 1 .71.31l4.56 4.72a7.73 7.73 0 0 1-.09 10.77A7.33 7.33 0 0 1 12 21.1z\"/></g></g>","edit-2":"<g data-name=\"Layer 2\"><g data-name=\"edit-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 20H5a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2z\"/><path d=\"M5 18h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71L16.66 2.6A2 2 0 0 0 14 2.53l-9 9a2 2 0 0 0-.57 1.21L4 16.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 18zM15.27 4L18 6.73l-2 1.95L13.32 6z\"/></g></g>","edit":"<g data-name=\"Layer 2\"><g data-name=\"edit\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.4 7.34L16.66 4.6A2 2 0 0 0 14 4.53l-9 9a2 2 0 0 0-.57 1.21L4 18.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 20h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71zM16 10.68L13.32 8l1.95-2L18 8.73z\"/></g></g>","email":"<g data-name=\"Layer 2\"><g data-name=\"email\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 4H5a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zm0 2l-6.5 4.47a1 1 0 0 1-1 0L5 6z\"/></g></g>","expand":"<g data-name=\"Layer 2\"><g data-name=\"expand\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20 5a1 1 0 0 0-1-1h-5a1 1 0 0 0 0 2h2.57l-3.28 3.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L18 7.42V10a1 1 0 0 0 1 1 1 1 0 0 0 1-1z\"/><path d=\"M10.71 13.29a1 1 0 0 0-1.42 0L6 16.57V14a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h5a1 1 0 0 0 0-2H7.42l3.29-3.29a1 1 0 0 0 0-1.42z\"/></g></g>","external-link":"<g data-name=\"Layer 2\"><g data-name=\"external-link\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 11a1 1 0 0 0-1 1v6a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h6a1 1 0 0 0 0-2H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-6a1 1 0 0 0-1-1z\"/><path d=\"M16 5h1.58l-6.29 6.28a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L19 6.42V8a1 1 0 0 0 1 1 1 1 0 0 0 1-1V4a1 1 0 0 0-1-1h-4a1 1 0 0 0 0 2z\"/></g></g>","eye-off-2":"<g data-name=\"Layer 2\"><g data-name=\"eye-off-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.81 13.39A8.93 8.93 0 0 0 21 7.62a1 1 0 1 0-2-.24 7.07 7.07 0 0 1-14 0 1 1 0 1 0-2 .24 8.93 8.93 0 0 0 3.18 5.77l-2.3 2.32a1 1 0 0 0 1.41 1.41l2.61-2.6a9.06 9.06 0 0 0 3.1.92V19a1 1 0 0 0 2 0v-3.56a9.06 9.06 0 0 0 3.1-.92l2.61 2.6a1 1 0 0 0 1.41-1.41z\"/></g></g>","eye-off":"<g data-name=\"Layer 2\"><g data-name=\"eye-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"1.5\"/><path d=\"M15.29 18.12L14 16.78l-.07-.07-1.27-1.27a4.07 4.07 0 0 1-.61.06A3.5 3.5 0 0 1 8.5 12a4.07 4.07 0 0 1 .06-.61l-2-2L5 7.87a15.89 15.89 0 0 0-2.87 3.63 1 1 0 0 0 0 1c.63 1.09 4 6.5 9.89 6.5h.25a9.48 9.48 0 0 0 3.23-.67z\"/><path d=\"M8.59 5.76l2.8 2.8A4.07 4.07 0 0 1 12 8.5a3.5 3.5 0 0 1 3.5 3.5 4.07 4.07 0 0 1-.06.61l2.68 2.68.84.84a15.89 15.89 0 0 0 2.91-3.63 1 1 0 0 0 0-1c-.64-1.11-4.16-6.68-10.14-6.5a9.48 9.48 0 0 0-3.23.67z\"/><path d=\"M20.71 19.29L19.41 18l-2-2-9.52-9.53L6.42 5 4.71 3.29a1 1 0 0 0-1.42 1.42L5.53 7l1.75 1.7 7.31 7.3.07.07L16 17.41l.59.59 2.7 2.71a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","eye":"<g data-name=\"Layer 2\"><g data-name=\"eye\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"1.5\"/><path d=\"M21.87 11.5c-.64-1.11-4.16-6.68-10.14-6.5-5.53.14-8.73 5-9.6 6.5a1 1 0 0 0 0 1c.63 1.09 4 6.5 9.89 6.5h.25c5.53-.14 8.74-5 9.6-6.5a1 1 0 0 0 0-1zm-9.87 4a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5z\"/></g></g>","facebook":"<g data-name=\"Layer 2\"><g data-name=\"facebook\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M17 3.5a.5.5 0 0 0-.5-.5H14a4.77 4.77 0 0 0-5 4.5v2.7H6.5a.5.5 0 0 0-.5.5v2.6a.5.5 0 0 0 .5.5H9v6.7a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-6.7h2.62a.5.5 0 0 0 .49-.37l.72-2.6a.5.5 0 0 0-.48-.63H13V7.5a1 1 0 0 1 1-.9h2.5a.5.5 0 0 0 .5-.5z\"/></g></g>","file-add":"<g data-name=\"Layer 2\"><g data-name=\"file-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 7.33l-4.44-5a1 1 0 0 0-.74-.33h-8A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V8a1 1 0 0 0-.26-.67zM14 15h-1v1a1 1 0 0 1-2 0v-1h-1a1 1 0 0 1 0-2h1v-1a1 1 0 0 1 2 0v1h1a1 1 0 0 1 0 2zm.71-7a.79.79 0 0 1-.71-.85V4l3.74 4z\"/></g></g>","file-remove":"<g data-name=\"Layer 2\"><g data-name=\"file-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 7.33l-4.44-5a1 1 0 0 0-.74-.33h-8A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V8a1 1 0 0 0-.26-.67zM14 15h-4a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2zm.71-7a.79.79 0 0 1-.71-.85V4l3.74 4z\"/></g></g>","file-text":"<g data-name=\"Layer 2\"><g data-name=\"file-text\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 7.33l-4.44-5a1 1 0 0 0-.74-.33h-8A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V8a1 1 0 0 0-.26-.67zM9 12h3a1 1 0 0 1 0 2H9a1 1 0 0 1 0-2zm6 6H9a1 1 0 0 1 0-2h6a1 1 0 0 1 0 2zm-.29-10a.79.79 0 0 1-.71-.85V4l3.74 4z\"/></g></g>","file":"<g data-name=\"Layer 2\"><g data-name=\"file\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 7.33l-4.44-5a1 1 0 0 0-.74-.33h-8A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V8a1 1 0 0 0-.26-.67zM14 4l3.74 4h-3a.79.79 0 0 1-.74-.85z\"/></g></g>","film":"<g data-name=\"Layer 2\"><g data-name=\"film\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.26 3H5.74A2.74 2.74 0 0 0 3 5.74v12.52A2.74 2.74 0 0 0 5.74 21h12.52A2.74 2.74 0 0 0 21 18.26V5.74A2.74 2.74 0 0 0 18.26 3zM7 11H5V9h2zm-2 2h2v2H5zm14-2h-2V9h2zm-2 2h2v2h-2zm2-7.26V7h-2V5h1.26a.74.74 0 0 1 .74.74zM5.74 5H7v2H5V5.74A.74.74 0 0 1 5.74 5zM5 18.26V17h2v2H5.74a.74.74 0 0 1-.74-.74zm14 0a.74.74 0 0 1-.74.74H17v-2h2z\"/></g></g>","flag":"<g data-name=\"Layer 2\"><g data-name=\"flag\"><polyline points=\"24 24 0 24 0 0\" opacity=\"0\"/><path d=\"M19.27 4.68a1.79 1.79 0 0 0-1.6-.25 7.53 7.53 0 0 1-2.17.28 8.54 8.54 0 0 1-3.13-.78A10.15 10.15 0 0 0 8.5 3c-2.89 0-4 1-4.2 1.14a1 1 0 0 0-.3.72V20a1 1 0 0 0 2 0v-4.3a6.28 6.28 0 0 1 2.5-.41 8.54 8.54 0 0 1 3.13.78 10.15 10.15 0 0 0 3.87.93 7.66 7.66 0 0 0 3.5-.7 1.74 1.74 0 0 0 1-1.55V6.11a1.77 1.77 0 0 0-.73-1.43z\"/></g></g>","flash-off":"<g data-name=\"Layer 2\"><g data-name=\"flash-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.33 14.5l2.5-3.74A1 1 0 0 0 19 9.2h-5.89l.77-7.09a1 1 0 0 0-.65-1 1 1 0 0 0-1.17.38L8.94 6.11z\"/><path d=\"M6.67 9.5l-2.5 3.74A1 1 0 0 0 5 14.8h5.89l-.77 7.09a1 1 0 0 0 .65 1.05 1 1 0 0 0 .34.06 1 1 0 0 0 .83-.44l3.12-4.67z\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","flash":"<g data-name=\"Layer 2\"><g data-name=\"flash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M11.11 23a1 1 0 0 1-.34-.06 1 1 0 0 1-.65-1.05l.77-7.09H5a1 1 0 0 1-.83-1.56l7.89-11.8a1 1 0 0 1 1.17-.38 1 1 0 0 1 .65 1l-.77 7.14H19a1 1 0 0 1 .83 1.56l-7.89 11.8a1 1 0 0 1-.83.44z\"/></g></g>","flip-2":"<g data-name=\"Layer 2\"><g data-name=\"flip-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M6.09 19h12l-1.3 1.29a1 1 0 0 0 1.42 1.42l3-3a1 1 0 0 0 0-1.42l-3-3a1 1 0 0 0-1.42 0 1 1 0 0 0 0 1.42l1.3 1.29h-12a1.56 1.56 0 0 1-1.59-1.53V13a1 1 0 0 0-2 0v2.47A3.56 3.56 0 0 0 6.09 19z\"/><path d=\"M5.79 9.71a1 1 0 1 0 1.42-1.42L5.91 7h12a1.56 1.56 0 0 1 1.59 1.53V11a1 1 0 0 0 2 0V8.53A3.56 3.56 0 0 0 17.91 5h-12l1.3-1.29a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0l-3 3a1 1 0 0 0 0 1.42z\"/></g></g>","flip":"<g data-name=\"Layer 2\"><g data-name=\"flip-in\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M5 6.09v12l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3a1 1 0 0 0 1.42 0l3-3a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0L7 18.09v-12A1.56 1.56 0 0 1 8.53 4.5H11a1 1 0 0 0 0-2H8.53A3.56 3.56 0 0 0 5 6.09z\"/><path d=\"M14.29 5.79a1 1 0 0 0 1.42 1.42L17 5.91v12a1.56 1.56 0 0 1-1.53 1.59H13a1 1 0 0 0 0 2h2.47A3.56 3.56 0 0 0 19 17.91v-12l1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42l-3-3a1 1 0 0 0-1.42 0z\"/></g></g>","folder-add":"<g data-name=\"Layer 2\"><g data-name=\"folder-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.5 7.05h-7L9.87 3.87a1 1 0 0 0-.77-.37H4.5A2.47 2.47 0 0 0 2 5.93v12.14a2.47 2.47 0 0 0 2.5 2.43h15a2.47 2.47 0 0 0 2.5-2.43V9.48a2.47 2.47 0 0 0-2.5-2.43zM14 15h-1v1a1 1 0 0 1-2 0v-1h-1a1 1 0 0 1 0-2h1v-1a1 1 0 0 1 2 0v1h1a1 1 0 0 1 0 2z\"/></g></g>","folder-remove":"<g data-name=\"Layer 2\"><g data-name=\"folder-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.5 7.05h-7L9.87 3.87a1 1 0 0 0-.77-.37H4.5A2.47 2.47 0 0 0 2 5.93v12.14a2.47 2.47 0 0 0 2.5 2.43h15a2.47 2.47 0 0 0 2.5-2.43V9.48a2.47 2.47 0 0 0-2.5-2.43zM14 15h-4a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2z\"/></g></g>","folder":"<g data-name=\"Layer 2\"><g data-name=\"folder\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.5 20.5h-15A2.47 2.47 0 0 1 2 18.07V5.93A2.47 2.47 0 0 1 4.5 3.5h4.6a1 1 0 0 1 .77.37l2.6 3.18h7A2.47 2.47 0 0 1 22 9.48v8.59a2.47 2.47 0 0 1-2.5 2.43z\"/></g></g>","funnel":"<g data-name=\"Layer 2\"><g data-name=\"funnel\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.9 22a1 1 0 0 1-.6-.2l-4-3.05a1 1 0 0 1-.39-.8v-3.27l-4.8-9.22A1 1 0 0 1 5 4h14a1 1 0 0 1 .86.49 1 1 0 0 1 0 1l-5 9.21V21a1 1 0 0 1-.55.9 1 1 0 0 1-.41.1z\"/></g></g>","gift":"<g data-name=\"Layer 2\"><g data-name=\"gift\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M4.64 15.27v4.82a.92.92 0 0 0 .92.91h5.62v-5.73z\"/><path d=\"M12.82 21h5.62a.92.92 0 0 0 .92-.91v-4.82h-6.54z\"/><path d=\"M20.1 7.09h-1.84a2.82 2.82 0 0 0 .29-1.23A2.87 2.87 0 0 0 15.68 3 4.21 4.21 0 0 0 12 5.57 4.21 4.21 0 0 0 8.32 3a2.87 2.87 0 0 0-2.87 2.86 2.82 2.82 0 0 0 .29 1.23H3.9c-.5 0-.9.59-.9 1.31v3.93c0 .72.4 1.31.9 1.31h7.28V7.09h1.64v6.55h7.28c.5 0 .9-.59.9-1.31V8.4c0-.72-.4-1.31-.9-1.31zm-11.78 0a1.23 1.23 0 1 1 0-2.45c1.4 0 2.19 1.44 2.58 2.45zm7.36 0H13.1c.39-1 1.18-2.45 2.58-2.45a1.23 1.23 0 1 1 0 2.45z\"/></g></g>","github":"<g data-name=\"Layer 2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 1A10.89 10.89 0 0 0 1 11.77 10.79 10.79 0 0 0 8.52 22c.55.1.75-.23.75-.52v-1.83c-3.06.65-3.71-1.44-3.71-1.44a2.86 2.86 0 0 0-1.22-1.58c-1-.66.08-.65.08-.65a2.31 2.31 0 0 1 1.68 1.11 2.37 2.37 0 0 0 3.2.89 2.33 2.33 0 0 1 .7-1.44c-2.44-.27-5-1.19-5-5.32a4.15 4.15 0 0 1 1.11-2.91 3.78 3.78 0 0 1 .11-2.84s.93-.29 3 1.1a10.68 10.68 0 0 1 5.5 0c2.1-1.39 3-1.1 3-1.1a3.78 3.78 0 0 1 .11 2.84A4.15 4.15 0 0 1 19 11.2c0 4.14-2.58 5.05-5 5.32a2.5 2.5 0 0 1 .75 2v2.95c0 .35.2.63.75.52A10.8 10.8 0 0 0 23 11.77 10.89 10.89 0 0 0 12 1\" data-name=\"github\"/></g>","globe-2":"<g data-name=\"Layer 2\"><g data-name=\"globe-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 2a8.19 8.19 0 0 1 1.79.21 2.61 2.61 0 0 1-.78 1c-.22.17-.46.31-.7.46a4.56 4.56 0 0 0-1.85 1.67 6.49 6.49 0 0 0-.62 3.3c0 1.36 0 2.16-.95 2.87-1.37 1.07-3.46.47-4.76-.07A8.33 8.33 0 0 1 4 12a8 8 0 0 1 8-8zm4.89 14.32a6.79 6.79 0 0 0-.63-1.14c-.11-.16-.22-.32-.32-.49-.39-.68-.25-1 .38-2l.1-.17a4.77 4.77 0 0 0 .58-2.43 5.42 5.42 0 0 1 .09-1c.16-.73 1.71-.93 2.67-1a7.94 7.94 0 0 1-2.86 8.28z\"/></g></g>","globe-3":"<g data-name=\"Layer 2\"><g data-name=\"globe-3\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zM5 15.8a8.42 8.42 0 0 0 2 .27 5 5 0 0 0 3.14-1c1.71-1.34 1.71-3.06 1.71-4.44a4.76 4.76 0 0 1 .37-2.34 2.86 2.86 0 0 1 1.12-.91 9.75 9.75 0 0 0 .92-.61 4.55 4.55 0 0 0 1.4-1.87A8 8 0 0 1 19 8.12c-1.43.2-3.46.67-3.86 2.53A7 7 0 0 0 15 12a2.93 2.93 0 0 1-.29 1.47l-.1.17c-.65 1.08-1.38 2.31-.39 4 .12.21.25.41.38.61a2.29 2.29 0 0 1 .52 1.08A7.89 7.89 0 0 1 12 20a8 8 0 0 1-7-4.2z\"/></g></g>","globe":"<g data-name=\"Layer 2\"><g data-name=\"globe\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M22 12A10 10 0 0 0 12 2a10 10 0 0 0 0 20 10 10 0 0 0 10-10zm-2.07-1H17a12.91 12.91 0 0 0-2.33-6.54A8 8 0 0 1 19.93 11zM9.08 13H15a11.44 11.44 0 0 1-3 6.61A11 11 0 0 1 9.08 13zm0-2A11.4 11.4 0 0 1 12 4.4a11.19 11.19 0 0 1 3 6.6zm.36-6.57A13.18 13.18 0 0 0 7.07 11h-3a8 8 0 0 1 5.37-6.57zM4.07 13h3a12.86 12.86 0 0 0 2.35 6.56A8 8 0 0 1 4.07 13zm10.55 6.55A13.14 13.14 0 0 0 17 13h2.95a8 8 0 0 1-5.33 6.55z\"/></g></g>","google":"<g data-name=\"Layer 2\"><g data-name=\"google\"><polyline points=\"0 0 24 0 24 24 0 24\" opacity=\"0\"/><path d=\"M17.5 14a5.51 5.51 0 0 1-4.5 3.93 6.15 6.15 0 0 1-7-5.45A6 6 0 0 1 12 6a6.12 6.12 0 0 1 2.27.44.5.5 0 0 0 .64-.21l1.44-2.65a.52.52 0 0 0-.23-.7A10 10 0 0 0 2 12.29 10.12 10.12 0 0 0 11.57 22 10 10 0 0 0 22 12.52v-2a.51.51 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h5\"/></g></g>","grid":"<g data-name=\"Layer 2\"><g data-name=\"grid\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9 3H5a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2z\"/><path d=\"M19 3h-4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2z\"/><path d=\"M9 13H5a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-4a2 2 0 0 0-2-2z\"/><path d=\"M19 13h-4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-4a2 2 0 0 0-2-2z\"/></g></g>","hard-drive":"<g data-name=\"Layer 2\"><g data-name=\"hard-drive\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.79 11.34l-3.34-6.68A3 3 0 0 0 14.76 3H9.24a3 3 0 0 0-2.69 1.66l-3.34 6.68a2 2 0 0 0-.21.9V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-5.76a2 2 0 0 0-.21-.9zM8 17a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm8 0h-4a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2zM5.62 11l2.72-5.45a1 1 0 0 1 .9-.55h5.52a1 1 0 0 1 .9.55L18.38 11z\"/></g></g>","hash":"<g data-name=\"Layer 2\"><g data-name=\"hash\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20 14h-4.3l.73-4H20a1 1 0 0 0 0-2h-3.21l.69-3.81A1 1 0 0 0 16.64 3a1 1 0 0 0-1.22.82L14.67 8h-3.88l.69-3.81A1 1 0 0 0 10.64 3a1 1 0 0 0-1.22.82L8.67 8H4a1 1 0 0 0 0 2h4.3l-.73 4H4a1 1 0 0 0 0 2h3.21l-.69 3.81A1 1 0 0 0 7.36 21a1 1 0 0 0 1.22-.82L9.33 16h3.88l-.69 3.81a1 1 0 0 0 .84 1.19 1 1 0 0 0 1.22-.82l.75-4.18H20a1 1 0 0 0 0-2zM9.7 14l.73-4h3.87l-.73 4z\"/></g></g>","headphones":"<g data-name=\"Layer 2\"><g data-name=\"headphones\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2A10.2 10.2 0 0 0 2 12.37V17a4 4 0 1 0 4-4 3.91 3.91 0 0 0-2 .56v-1.19A8.2 8.2 0 0 1 12 4a8.2 8.2 0 0 1 8 8.37v1.19a3.91 3.91 0 0 0-2-.56 4 4 0 1 0 4 4v-4.63A10.2 10.2 0 0 0 12 2z\"/></g></g>","heart":"<g data-name=\"Layer 2\"><g data-name=\"heart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 21a1 1 0 0 1-.71-.29l-7.77-7.78a5.26 5.26 0 0 1 0-7.4 5.24 5.24 0 0 1 7.4 0L12 6.61l1.08-1.08a5.24 5.24 0 0 1 7.4 0 5.26 5.26 0 0 1 0 7.4l-7.77 7.78A1 1 0 0 1 12 21z\"/></g></g>","home":"<g data-name=\"Layer 2\"><g data-name=\"home\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"10\" y=\"14\" width=\"4\" height=\"7\"/><path d=\"M20.42 10.18L12.71 2.3a1 1 0 0 0-1.42 0l-7.71 7.89A2 2 0 0 0 3 11.62V20a2 2 0 0 0 1.89 2H8v-9a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v9h3.11A2 2 0 0 0 21 20v-8.38a2.07 2.07 0 0 0-.58-1.44z\"/></g></g>","image-2":"<g data-name=\"Layer 2\"><g data-name=\"image-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zM8 7a1.5 1.5 0 1 1-1.5 1.5A1.5 1.5 0 0 1 8 7zm11 10.83A1.09 1.09 0 0 1 18 19H6l7.57-6.82a.69.69 0 0 1 .93 0l4.5 4.48z\"/></g></g>","image":"<g data-name=\"Layer 2\"><g data-name=\"image\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zM6 5h12a1 1 0 0 1 1 1v8.36l-3.2-2.73a2.77 2.77 0 0 0-3.52 0L5 17.7V6a1 1 0 0 1 1-1z\"/><circle cx=\"8\" cy=\"8.5\" r=\"1.5\"/></g></g>","inbox":"<g data-name=\"Layer 2\"><g data-name=\"inbox\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.79 11.34l-3.34-6.68A3 3 0 0 0 14.76 3H9.24a3 3 0 0 0-2.69 1.66l-3.34 6.68a2 2 0 0 0-.21.9V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-5.76a2 2 0 0 0-.21-.9zM8.34 5.55a1 1 0 0 1 .9-.55h5.52a1 1 0 0 1 .9.55L18.38 11H16a1 1 0 0 0-1 1v2a1 1 0 0 1-1 1h-4a1 1 0 0 1-1-1v-2a1 1 0 0 0-1-1H5.62z\"/></g></g>","info":"<g data-name=\"Layer 2\"><g data-name=\"info\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm1 14a1 1 0 0 1-2 0v-5a1 1 0 0 1 2 0zm-1-7a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","keypad":"<g data-name=\"Layer 2\"><g data-name=\"keypad\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M5 2a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M12 2a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M19 8a3 3 0 1 0-3-3 3 3 0 0 0 3 3z\"/><path d=\"M5 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M12 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M19 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M5 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M12 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/><path d=\"M19 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3z\"/></g></g>","layers":"<g data-name=\"Layer 2\"><g data-name=\"layers\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M3.24 7.29l8.52 4.63a.51.51 0 0 0 .48 0l8.52-4.63a.44.44 0 0 0-.05-.81L12.19 3a.5.5 0 0 0-.38 0L3.29 6.48a.44.44 0 0 0-.05.81z\"/><path d=\"M20.71 10.66l-1.83-.78-6.64 3.61a.51.51 0 0 1-.48 0L5.12 9.88l-1.83.78a.48.48 0 0 0 0 .85l8.52 4.9a.46.46 0 0 0 .48 0l8.52-4.9a.48.48 0 0 0-.1-.85z\"/><path d=\"M20.71 15.1l-1.56-.68-6.91 3.76a.51.51 0 0 1-.48 0l-6.91-3.76-1.56.68a.49.49 0 0 0 0 .87l8.52 5a.51.51 0 0 0 .48 0l8.52-5a.49.49 0 0 0-.1-.87z\"/></g></g>","layout":"<g data-name=\"Layer 2\"><g data-name=\"layout\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 8V6a3 3 0 0 0-3-3H6a3 3 0 0 0-3 3v2z\"/><path d=\"M3 10v8a3 3 0 0 0 3 3h5V10z\"/><path d=\"M13 10v11h5a3 3 0 0 0 3-3v-8z\"/></g></g>","link-2":"<g data-name=\"Layer 2\"><g data-name=\"link-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.29 9.29l-4 4a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l4-4a1 1 0 0 0-1.42-1.42z\"/><path d=\"M12.28 17.4L11 18.67a4.2 4.2 0 0 1-5.58.4 4 4 0 0 1-.27-5.93l1.42-1.43a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0l-1.27 1.28a6.15 6.15 0 0 0-.67 8.07 6.06 6.06 0 0 0 9.07.6l1.42-1.42a1 1 0 0 0-1.42-1.42z\"/><path d=\"M19.66 3.22a6.18 6.18 0 0 0-8.13.68L10.45 5a1.09 1.09 0 0 0-.17 1.61 1 1 0 0 0 1.42 0L13 5.3a4.17 4.17 0 0 1 5.57-.4 4 4 0 0 1 .27 5.95l-1.42 1.43a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l1.42-1.42a6.06 6.06 0 0 0-.6-9.06z\"/></g></g>","link":"<g data-name=\"Layer 2\"><g data-name=\"link\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8 12a1 1 0 0 0 1 1h6a1 1 0 0 0 0-2H9a1 1 0 0 0-1 1z\"/><path d=\"M9 16H7.21A4.13 4.13 0 0 1 3 12.37 4 4 0 0 1 7 8h2a1 1 0 0 0 0-2H7.21a6.15 6.15 0 0 0-6.16 5.21A6 6 0 0 0 7 18h2a1 1 0 0 0 0-2z\"/><path d=\"M23 11.24A6.16 6.16 0 0 0 16.76 6h-1.51C14.44 6 14 6.45 14 7a1 1 0 0 0 1 1h1.79A4.13 4.13 0 0 1 21 11.63 4 4 0 0 1 17 16h-2a1 1 0 0 0 0 2h2a6 6 0 0 0 6-6.76z\"/></g></g>","linkedin":"<g data-name=\"Layer 2\"><g data-name=\"linkedin\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M15.15 8.4a5.83 5.83 0 0 0-5.85 5.82v5.88a.9.9 0 0 0 .9.9h2.1a.9.9 0 0 0 .9-.9v-5.88a1.94 1.94 0 0 1 2.15-1.93 2 2 0 0 1 1.75 2v5.81a.9.9 0 0 0 .9.9h2.1a.9.9 0 0 0 .9-.9v-5.88a5.83 5.83 0 0 0-5.85-5.82z\"/><rect x=\"3\" y=\"9.3\" width=\"4.5\" height=\"11.7\" rx=\".9\" ry=\".9\"/><circle cx=\"5.25\" cy=\"5.25\" r=\"2.25\"/></g></g>","list":"<g data-name=\"Layer 2\"><g data-name=\"list\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><circle cx=\"4\" cy=\"7\" r=\"1\"/><circle cx=\"4\" cy=\"12\" r=\"1\"/><circle cx=\"4\" cy=\"17\" r=\"1\"/><rect x=\"7\" y=\"11\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"7\" y=\"16\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"7\" y=\"6\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/></g></g>","lock":"<g data-name=\"Layer 2\"><g data-name=\"lock\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"15\" r=\"1\"/><path d=\"M17 8h-1V6.11a4 4 0 1 0-8 0V8H7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-7-1.89A2.06 2.06 0 0 1 12 4a2.06 2.06 0 0 1 2 2.11V8h-4zM12 18a3 3 0 1 1 3-3 3 3 0 0 1-3 3z\"/></g></g>","log-in":"<g data-name=\"Layer 2\"><g data-name=\"log-in\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19 4h-2a1 1 0 0 0 0 2h1v12h-1a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z\"/><path d=\"M11.8 7.4a1 1 0 0 0-1.6 1.2L12 11H4a1 1 0 0 0 0 2h8.09l-1.72 2.44a1 1 0 0 0 .24 1.4 1 1 0 0 0 .58.18 1 1 0 0 0 .81-.42l2.82-4a1 1 0 0 0 0-1.18z\"/></g></g>","log-out":"<g data-name=\"Layer 2\"><g data-name=\"log-out\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M7 6a1 1 0 0 0 0-2H5a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h2a1 1 0 0 0 0-2H6V6z\"/><path d=\"M20.82 11.42l-2.82-4a1 1 0 0 0-1.39-.24 1 1 0 0 0-.24 1.4L18.09 11H10a1 1 0 0 0 0 2h8l-1.8 2.4a1 1 0 0 0 .2 1.4 1 1 0 0 0 .6.2 1 1 0 0 0 .8-.4l3-4a1 1 0 0 0 .02-1.18z\"/></g></g>","map":"<g data-name=\"Layer 2\"><g data-name=\"map\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.41 5.89l-4-1.8H15.59L12 5.7 8.41 4.09h-.05L8.24 4h-.6l-4 1.8a1 1 0 0 0-.64 1V19a1 1 0 0 0 .46.84A1 1 0 0 0 4 20a1 1 0 0 0 .41-.09L8 18.3l3.59 1.61h.05a.85.85 0 0 0 .72 0h.05L16 18.3l3.59 1.61A1 1 0 0 0 20 20a1 1 0 0 0 .54-.16A1 1 0 0 0 21 19V6.8a1 1 0 0 0-.59-.91zM9 6.55l2 .89v10l-2-.89zm10 10.9l-2-.89v-10l2 .89z\"/></g></g>","maximize":"<g data-name=\"Layer 2\"><g data-name=\"maximize\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM13 12h-1v1a1 1 0 0 1-2 0v-1H9a1 1 0 0 1 0-2h1V9a1 1 0 0 1 2 0v1h1a1 1 0 0 1 0 2z\"/></g></g>","menu-2":"<g data-name=\"Layer 2\"><g data-name=\"menu-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><circle cx=\"4\" cy=\"12\" r=\"1\"/><rect x=\"7\" y=\"11\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"3\" y=\"16\" width=\"18\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"3\" y=\"6\" width=\"18\" height=\"2\" rx=\".94\" ry=\".94\"/></g></g>","menu-arrow":"<g data-name=\"Layer 2\"><g data-name=\"menu-arrow\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.05 11H5.91l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3a1 1 0 0 0 0 1.42l3 3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L5.91 13h14.14a1 1 0 0 0 .95-.95V12a1 1 0 0 0-.95-1z\"/><rect x=\"3\" y=\"17\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"5\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/></g></g>","menu":"<g data-name=\"Layer 2\"><g data-name=\"menu\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><rect x=\"3\" y=\"11\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"16\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"6\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/></g></g>","message-circle":"<g data-name=\"Layer 2\"><g data-name=\"message-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.07 4.93a10 10 0 0 0-16.28 11 1.06 1.06 0 0 1 .09.64L2 20.8a1 1 0 0 0 .27.91A1 1 0 0 0 3 22h.2l4.28-.86a1.26 1.26 0 0 1 .64.09 10 10 0 0 0 11-16.28zM8 13a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm4 0a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm4 0a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","message-square":"<g data-name=\"Layer 2\"><g data-name=\"message-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 3H5a3 3 0 0 0-3 3v15a1 1 0 0 0 .51.87A1 1 0 0 0 3 22a1 1 0 0 0 .51-.14L8 19.14a1 1 0 0 1 .55-.14H19a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zM8 12a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm4 0a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm4 0a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","mic-off":"<g data-name=\"Layer 2\"><g data-name=\"mic-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15.58 12.75A4 4 0 0 0 16 11V6a4 4 0 0 0-7.92-.75\"/><path d=\"M19 11a1 1 0 0 0-2 0 4.86 4.86 0 0 1-.69 2.48L17.78 15A7 7 0 0 0 19 11z\"/><path d=\"M12 15h.16L8 10.83V11a4 4 0 0 0 4 4z\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M15 20h-2v-2.08a7 7 0 0 0 1.65-.44l-1.6-1.6A4.57 4.57 0 0 1 12 16a5 5 0 0 1-5-5 1 1 0 0 0-2 0 7 7 0 0 0 6 6.92V20H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2z\"/></g></g>","mic":"<g data-name=\"Layer 2\"><g data-name=\"mic\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 15a4 4 0 0 0 4-4V6a4 4 0 0 0-8 0v5a4 4 0 0 0 4 4z\"/><path d=\"M19 11a1 1 0 0 0-2 0 5 5 0 0 1-10 0 1 1 0 0 0-2 0 7 7 0 0 0 6 6.92V20H8.89a.89.89 0 0 0-.89.89v.22a.89.89 0 0 0 .89.89h6.22a.89.89 0 0 0 .89-.89v-.22a.89.89 0 0 0-.89-.89H13v-2.08A7 7 0 0 0 19 11z\"/></g></g>","minimize":"<g data-name=\"Layer 2\"><g data-name=\"minimize\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM13 12H9a1 1 0 0 1 0-2h4a1 1 0 0 1 0 2z\"/></g></g>","minus-circle":"<g data-name=\"Layer 2\"><g data-name=\"minus-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm3 11H9a1 1 0 0 1 0-2h6a1 1 0 0 1 0 2z\"/></g></g>","minus-square":"<g data-name=\"Layer 2\"><g data-name=\"minus-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm-3 10H9a1 1 0 0 1 0-2h6a1 1 0 0 1 0 2z\"/></g></g>","minus":"<g data-name=\"Layer 2\"><g data-name=\"minus\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 13H5a1 1 0 0 1 0-2h14a1 1 0 0 1 0 2z\"/></g></g>","monitor":"<g data-name=\"Layer 2\"><g data-name=\"monitor\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 3H5a3 3 0 0 0-3 3v5h20V6a3 3 0 0 0-3-3z\"/><path d=\"M2 14a3 3 0 0 0 3 3h6v2H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2h-4v-2h6a3 3 0 0 0 3-3v-1H2z\"/></g></g>","moon":"<g data-name=\"Layer 2\"><g data-name=\"moon\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12.3 22h-.1a10.31 10.31 0 0 1-7.34-3.15 10.46 10.46 0 0 1-.26-14 10.13 10.13 0 0 1 4-2.74 1 1 0 0 1 1.06.22 1 1 0 0 1 .24 1 8.4 8.4 0 0 0 1.94 8.81 8.47 8.47 0 0 0 8.83 1.94 1 1 0 0 1 1.27 1.29A10.16 10.16 0 0 1 19.6 19a10.28 10.28 0 0 1-7.3 3z\"/></g></g>","more-horizontal":"<g data-name=\"Layer 2\"><g data-name=\"more-horizotnal\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"2\"/><circle cx=\"19\" cy=\"12\" r=\"2\"/><circle cx=\"5\" cy=\"12\" r=\"2\"/></g></g>","more-vertical":"<g data-name=\"Layer 2\"><g data-name=\"more-vertical\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"2\"/><circle cx=\"12\" cy=\"5\" r=\"2\"/><circle cx=\"12\" cy=\"19\" r=\"2\"/></g></g>","move":"<g data-name=\"Layer 2\"><g data-name=\"move\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M21.71 11.31l-3-3a1 1 0 0 0-1.42 1.42L18.58 11H13V5.41l1.29 1.3A1 1 0 0 0 15 7a1 1 0 0 0 .71-.29 1 1 0 0 0 0-1.42l-3-3A1 1 0 0 0 12 2a1 1 0 0 0-.7.29l-3 3a1 1 0 0 0 1.41 1.42L11 5.42V11H5.41l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3A1 1 0 0 0 2 12a1 1 0 0 0 .29.71l3 3A1 1 0 0 0 6 16a1 1 0 0 0 .71-.29 1 1 0 0 0 0-1.42L5.42 13H11v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 22a1 1 0 0 0 .7-.29l3-3a1 1 0 0 0-1.42-1.42L13 18.58V13h5.59l-1.3 1.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 16a1 1 0 0 0 .71-.29l3-3A1 1 0 0 0 22 12a1 1 0 0 0-.29-.69z\"/></g></g>","music":"<g data-name=\"Layer 2\"><g data-name=\"music\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 15V4a1 1 0 0 0-.38-.78 1 1 0 0 0-.84-.2l-9 2A1 1 0 0 0 8 6v8.34a3.49 3.49 0 1 0 2 3.18 4.36 4.36 0 0 0 0-.52V6.8l7-1.55v7.09a3.49 3.49 0 1 0 2 3.17 4.57 4.57 0 0 0 0-.51z\"/></g></g>","navigation-2":"<g data-name=\"Layer 2\"><g data-name=\"navigation-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.67 22h-.06a1 1 0 0 1-.92-.8l-1.54-7.57a1 1 0 0 0-.78-.78L2.8 11.31a1 1 0 0 1-.12-1.93l16-5.33A1 1 0 0 1 20 5.32l-5.33 16a1 1 0 0 1-1 .68z\"/></g></g>","navigation":"<g data-name=\"Layer 2\"><g data-name=\"navigation\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 20a.94.94 0 0 1-.55-.17l-6.9-4.56a1 1 0 0 0-1.1 0l-6.9 4.56a1 1 0 0 1-1.44-1.28l8-16a1 1 0 0 1 1.78 0l8 16a1 1 0 0 1-.23 1.2A1 1 0 0 1 20 20z\"/></g></g>","npm":"<g data-name=\"Layer 2\"><g data-name=\"npm\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h7V11h4v10h1a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3z\"/></g></g>","options-2":"<g data-name=\"Layer 2\"><g data-name=\"options-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M19 9a3 3 0 0 0-2.82 2H3a1 1 0 0 0 0 2h13.18A3 3 0 1 0 19 9z\"/><path d=\"M3 7h1.18a3 3 0 0 0 5.64 0H21a1 1 0 0 0 0-2H9.82a3 3 0 0 0-5.64 0H3a1 1 0 0 0 0 2z\"/><path d=\"M21 17h-7.18a3 3 0 0 0-5.64 0H3a1 1 0 0 0 0 2h5.18a3 3 0 0 0 5.64 0H21a1 1 0 0 0 0-2z\"/></g></g>","options":"<g data-name=\"Layer 2\"><g data-name=\"options\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M7 14.18V3a1 1 0 0 0-2 0v11.18a3 3 0 0 0 0 5.64V21a1 1 0 0 0 2 0v-1.18a3 3 0 0 0 0-5.64z\"/><path d=\"M21 13a3 3 0 0 0-2-2.82V3a1 1 0 0 0-2 0v7.18a3 3 0 0 0 0 5.64V21a1 1 0 0 0 2 0v-5.18A3 3 0 0 0 21 13z\"/><path d=\"M15 5a3 3 0 1 0-4 2.82V21a1 1 0 0 0 2 0V7.82A3 3 0 0 0 15 5z\"/></g></g>","pantone":"<g data-name=\"Layer 2\"><g data-name=\"pantone\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 13.18h-2.7l-1.86 2L11.88 19l-1.41 1.52L10 21h10a1 1 0 0 0 1-1v-5.82a1 1 0 0 0-1-1z\"/><path d=\"M18.19 9.3l-4.14-3.86a.89.89 0 0 0-.71-.26 1 1 0 0 0-.7.31l-.82.89v10.71a5.23 5.23 0 0 1-.06.57l6.48-6.95a1 1 0 0 0-.05-1.41z\"/><path d=\"M10.82 4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v13.09a3.91 3.91 0 0 0 7.82 0zm-2 13.09a1.91 1.91 0 0 1-3.82 0V15h3.82zm0-4.09H5v-3h3.82zm0-5H5V5h3.82z\"/></g></g>","paper-plane":"<g data-name=\"Layer 2\"><g data-name=\"paper-plane\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 4a1.31 1.31 0 0 0-.06-.27v-.09a1 1 0 0 0-.2-.3 1 1 0 0 0-.29-.19h-.09a.86.86 0 0 0-.31-.15H20a1 1 0 0 0-.3 0l-18 6a1 1 0 0 0 0 1.9l8.53 2.84 2.84 8.53a1 1 0 0 0 1.9 0l6-18A1 1 0 0 0 21 4zm-4.7 2.29l-5.57 5.57L5.16 10zM14 18.84l-1.86-5.57 5.57-5.57z\"/></g></g>","pause-circle":"<g data-name=\"Layer 2\"><g data-name=\"pause-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm-2 13a1 1 0 0 1-2 0V9a1 1 0 0 1 2 0zm6 0a1 1 0 0 1-2 0V9a1 1 0 0 1 2 0z\"/></g></g>","people":"<g data-name=\"Layer 2\"><g data-name=\"people\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M17 13a3 3 0 1 0-3-3 3 3 0 0 0 3 3z\"/><path d=\"M21 20a1 1 0 0 0 1-1 5 5 0 0 0-8.06-3.95A7 7 0 0 0 2 20a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1\"/></g></g>","percent":"<g data-name=\"Layer 2\"><g data-name=\"percent\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8 11a3.5 3.5 0 1 0-3.5-3.5A3.5 3.5 0 0 0 8 11zm0-5a1.5 1.5 0 1 1-1.5 1.5A1.5 1.5 0 0 1 8 6z\"/><path d=\"M16 14a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 16 14zm0 5a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 16 19z\"/><path d=\"M19.74 4.26a.89.89 0 0 0-1.26 0L4.26 18.48a.91.91 0 0 0-.26.63.89.89 0 0 0 1.52.63L19.74 5.52a.89.89 0 0 0 0-1.26z\"/></g></g>","person-add":"<g data-name=\"Layer 2\"><g data-name=\"person-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-1V5a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0V8h1a1 1 0 0 0 0-2z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1\"/></g></g>","person-delete":"<g data-name=\"Layer 2\"><g data-name=\"person-delete\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.47 7.5l.73-.73a1 1 0 0 0-1.47-1.47L19 6l-.73-.73a1 1 0 0 0-1.47 1.5l.73.73-.73.73a1 1 0 0 0 1.47 1.47L19 9l.73.73a1 1 0 0 0 1.47-1.5z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1z\"/></g></g>","person-done":"<g data-name=\"Layer 2\"><g data-name=\"person-done\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.66 4.25a1 1 0 0 0-1.41.09l-1.87 2.15-.63-.71a1 1 0 0 0-1.5 1.33l1.39 1.56a1 1 0 0 0 .75.33 1 1 0 0 0 .74-.34l2.61-3a1 1 0 0 0-.08-1.41z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1\"/></g></g>","person-remove":"<g data-name=\"Layer 2\"><g data-name=\"person-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M16 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1\"/></g></g>","person":"<g data-name=\"Layer 2\"><g data-name=\"person\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4z\"/><path d=\"M18 21a1 1 0 0 0 1-1 7 7 0 0 0-14 0 1 1 0 0 0 1 1z\"/></g></g>","phone-call":"<g data-name=\"Layer 2\"><g data-name=\"phone-call\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13 8a3 3 0 0 1 3 3 1 1 0 0 0 2 0 5 5 0 0 0-5-5 1 1 0 0 0 0 2z\"/><path d=\"M13 4a7 7 0 0 1 7 7 1 1 0 0 0 2 0 9 9 0 0 0-9-9 1 1 0 0 0 0 2z\"/><path d=\"M21.75 15.91a1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a9.91 9.91 0 0 1-4.87-4.89C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6 15.42 15.42 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76 4.34 4.34 0 0 0-.19-.73z\"/></g></g>","phone-missed":"<g data-name=\"Layer 2\"><g data-name=\"phone-missed\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.94 16.64a4.34 4.34 0 0 0-.19-.73 1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a10 10 0 0 1-4.88-4.89C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6 15.42 15.42 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76z\"/><path d=\"M15.8 8.7a1.05 1.05 0 0 0 1.47 0L18 8l.73.73a1 1 0 0 0 1.47-1.5l-.73-.73.73-.73a1 1 0 0 0-1.47-1.47L18 5l-.73-.73a1 1 0 0 0-1.47 1.5l.73.73-.73.73a1.05 1.05 0 0 0 0 1.47z\"/></g></g>","phone-off":"<g data-name=\"Layer 2\"><g data-name=\"phone-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.27 12.06a10.37 10.37 0 0 1-.8-1.42C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6a15.33 15.33 0 0 0 3.27 9.46z\"/><path d=\"M21.94 16.64a4.34 4.34 0 0 0-.19-.73 1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a10.88 10.88 0 0 1-1.41-.8l-4 4A15.33 15.33 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76z\"/><path d=\"M19.74 4.26a.89.89 0 0 0-1.26 0L4.26 18.48a.91.91 0 0 0-.26.63.89.89 0 0 0 1.52.63L19.74 5.52a.89.89 0 0 0 0-1.26z\"/></g></g>","phone":"<g data-name=\"Layer 2\"><g data-name=\"phone\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.4 22A15.42 15.42 0 0 1 2 6.6 4.6 4.6 0 0 1 6.6 2a3.94 3.94 0 0 1 .77.07 3.79 3.79 0 0 1 .72.18 1 1 0 0 1 .65.75l1.37 6a1 1 0 0 1-.26.92c-.13.14-.14.15-1.37.79a9.91 9.91 0 0 0 4.87 4.89c.65-1.24.66-1.25.8-1.38a1 1 0 0 1 .92-.26l6 1.37a1 1 0 0 1 .72.65 4.34 4.34 0 0 1 .19.73 4.77 4.77 0 0 1 .06.76A4.6 4.6 0 0 1 17.4 22z\"/></g></g>","pie-chart-2":"<g data-name=\"Layer 2\"><g data-name=\"pie-chart-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.5 10.33h6.67A.83.83 0 0 0 22 9.5 7.5 7.5 0 0 0 14.5 2a.83.83 0 0 0-.83.83V9.5a.83.83 0 0 0 .83.83zm.83-6.6a5.83 5.83 0 0 1 4.94 4.94h-4.94z\"/><path d=\"M21.08 12h-8.15a.91.91 0 0 1-.91-.91V2.92A.92.92 0 0 0 11 2a10 10 0 1 0 11 11 .92.92 0 0 0-.92-1z\"/></g></g>","pie-chart":"<g data-name=\"Layer 2\"><g data-name=\"pie-chart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.5 10.33h6.67A.83.83 0 0 0 22 9.5 7.5 7.5 0 0 0 14.5 2a.83.83 0 0 0-.83.83V9.5a.83.83 0 0 0 .83.83z\"/><path d=\"M21.08 12h-8.15a.91.91 0 0 1-.91-.91V2.92A.92.92 0 0 0 11 2a10 10 0 1 0 11 11 .92.92 0 0 0-.92-1z\"/></g></g>","pin":"<g data-name=\"Layer 2\"><g data-name=\"pin\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"9.5\" r=\"1.5\"/><path d=\"M12 2a8 8 0 0 0-8 7.92c0 5.48 7.05 11.58 7.35 11.84a1 1 0 0 0 1.3 0C13 21.5 20 15.4 20 9.92A8 8 0 0 0 12 2zm0 11a3.5 3.5 0 1 1 3.5-3.5A3.5 3.5 0 0 1 12 13z\"/></g></g>","play-circle":"<g data-name=\"Layer 2\"><g data-name=\"play-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><polygon points=\"11.5 14.6 14.31 12 11.5 9.4 11.5 14.6\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm4 11.18l-3.64 3.37a1.74 1.74 0 0 1-1.16.45 1.68 1.68 0 0 1-.69-.15 1.6 1.6 0 0 1-1-1.48V8.63a1.6 1.6 0 0 1 1-1.48 1.7 1.7 0 0 1 1.85.3L16 10.82a1.6 1.6 0 0 1 0 2.36z\"/></g></g>","plus-circle":"<g data-name=\"Layer 2\"><g data-name=\"plus-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm3 11h-2v2a1 1 0 0 1-2 0v-2H9a1 1 0 0 1 0-2h2V9a1 1 0 0 1 2 0v2h2a1 1 0 0 1 0 2z\"/></g></g>","plus-square":"<g data-name=\"Layer 2\"><g data-name=\"plus-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm-3 10h-2v2a1 1 0 0 1-2 0v-2H9a1 1 0 0 1 0-2h2V9a1 1 0 0 1 2 0v2h2a1 1 0 0 1 0 2z\"/></g></g>","plus":"<g data-name=\"Layer 2\"><g data-name=\"plus\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 11h-6V5a1 1 0 0 0-2 0v6H5a1 1 0 0 0 0 2h6v6a1 1 0 0 0 2 0v-6h6a1 1 0 0 0 0-2z\"/></g></g>","power":"<g data-name=\"Layer 2\"><g data-name=\"power\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 13a1 1 0 0 0 1-1V2a1 1 0 0 0-2 0v10a1 1 0 0 0 1 1z\"/><path d=\"M16.59 3.11a1 1 0 0 0-.92 1.78 8 8 0 1 1-7.34 0 1 1 0 1 0-.92-1.78 10 10 0 1 0 9.18 0z\"/></g></g>","pricetags":"<g data-name=\"Layer 2\"><g data-name=\"pricetags\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.47 11.58l-6.42-6.41a1 1 0 0 0-.61-.29L5.09 4a1 1 0 0 0-.8.29 1 1 0 0 0-.29.8l.88 9.35a1 1 0 0 0 .29.61l6.41 6.42a1.84 1.84 0 0 0 1.29.53 1.82 1.82 0 0 0 1.28-.53l7.32-7.32a1.82 1.82 0 0 0 0-2.57zm-9.91 0a1.5 1.5 0 1 1 0-2.12 1.49 1.49 0 0 1 0 2.1z\"/></g></g>","printer":"<g data-name=\"Layer 2\"><g data-name=\"printer\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.36 7H18V5a1.92 1.92 0 0 0-1.83-2H7.83A1.92 1.92 0 0 0 6 5v2H4.64A2.66 2.66 0 0 0 2 9.67v6.66A2.66 2.66 0 0 0 4.64 19h.86a2 2 0 0 0 2 2h9a2 2 0 0 0 2-2h.86A2.66 2.66 0 0 0 22 16.33V9.67A2.66 2.66 0 0 0 19.36 7zM8 5h8v2H8zm-.5 14v-4h9v4z\"/></g></g>","question-mark-circle":"<g data-name=\"Layer 2\"><g data-name=\"menu-arrow-circle\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 16a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm1-5.16V14a1 1 0 0 1-2 0v-2a1 1 0 0 1 1-1 1.5 1.5 0 1 0-1.5-1.5 1 1 0 0 1-2 0 3.5 3.5 0 1 1 4.5 3.34z\"/></g></g>","question-mark":"<g data-name=\"Layer 2\"><g data-name=\"menu-arrow\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M17 9A5 5 0 0 0 7 9a1 1 0 0 0 2 0 3 3 0 1 1 3 3 1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-1.1A5 5 0 0 0 17 9z\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/></g></g>","radio-button-off":"<g data-name=\"Layer 2\"><g data-name=\"radio-button-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a10 10 0 1 1 10-10 10 10 0 0 1-10 10zm0-18a8 8 0 1 0 8 8 8 8 0 0 0-8-8z\"/></g></g>","radio-button-on":"<g data-name=\"Layer 2\"><g data-name=\"radio-button-on\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M12 7a5 5 0 1 0 5 5 5 5 0 0 0-5-5z\"/></g></g>","radio":"<g data-name=\"Layer 2\"><g data-name=\"radio\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 8a3 3 0 0 0-1 5.83 1 1 0 0 0 0 .17v6a1 1 0 0 0 2 0v-6a1 1 0 0 0 0-.17A3 3 0 0 0 12 8z\"/><path d=\"M3.5 11a6.87 6.87 0 0 1 2.64-5.23 1 1 0 1 0-1.28-1.54A8.84 8.84 0 0 0 1.5 11a8.84 8.84 0 0 0 3.36 6.77 1 1 0 1 0 1.28-1.54A6.87 6.87 0 0 1 3.5 11z\"/><path d=\"M16.64 6.24a1 1 0 0 0-1.28 1.52A4.28 4.28 0 0 1 17 11a4.28 4.28 0 0 1-1.64 3.24A1 1 0 0 0 16 16a1 1 0 0 0 .64-.24A6.2 6.2 0 0 0 19 11a6.2 6.2 0 0 0-2.36-4.76z\"/><path d=\"M8.76 6.36a1 1 0 0 0-1.4-.12A6.2 6.2 0 0 0 5 11a6.2 6.2 0 0 0 2.36 4.76 1 1 0 0 0 1.4-.12 1 1 0 0 0-.12-1.4A4.28 4.28 0 0 1 7 11a4.28 4.28 0 0 1 1.64-3.24 1 1 0 0 0 .12-1.4z\"/><path d=\"M19.14 4.23a1 1 0 1 0-1.28 1.54A6.87 6.87 0 0 1 20.5 11a6.87 6.87 0 0 1-2.64 5.23 1 1 0 0 0 1.28 1.54A8.84 8.84 0 0 0 22.5 11a8.84 8.84 0 0 0-3.36-6.77z\"/></g></g>","recording":"<g data-name=\"Layer 2\"><g data-name=\"recording\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 8a4 4 0 0 0-4 4 3.91 3.91 0 0 0 .56 2H9.44a3.91 3.91 0 0 0 .56-2 4 4 0 1 0-4 4h12a4 4 0 0 0 0-8z\"/></g></g>","refresh":"<g data-name=\"Layer 2\"><g data-name=\"refresh\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.3 13.43a1 1 0 0 0-1.25.65A7.14 7.14 0 0 1 12.18 19 7.1 7.1 0 0 1 5 12a7.1 7.1 0 0 1 7.18-7 7.26 7.26 0 0 1 4.65 1.67l-2.17-.36a1 1 0 0 0-1.15.83 1 1 0 0 0 .83 1.15l4.24.7h.17a1 1 0 0 0 .34-.06.33.33 0 0 0 .1-.06.78.78 0 0 0 .2-.11l.09-.11c0-.05.09-.09.13-.15s0-.1.05-.14a1.34 1.34 0 0 0 .07-.18l.75-4a1 1 0 0 0-2-.38l-.27 1.45A9.21 9.21 0 0 0 12.18 3 9.1 9.1 0 0 0 3 12a9.1 9.1 0 0 0 9.18 9A9.12 9.12 0 0 0 21 14.68a1 1 0 0 0-.7-1.25z\"/></g></g>","repeat":"<g data-name=\"Layer 2\"><g data-name=\"repeat\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.91 5h-12l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3a1 1 0 0 0 0 1.42l3 3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L5.91 7h12a1.56 1.56 0 0 1 1.59 1.53V11a1 1 0 0 0 2 0V8.53A3.56 3.56 0 0 0 17.91 5z\"/><path d=\"M18.21 14.29a1 1 0 0 0-1.42 1.42l1.3 1.29h-12a1.56 1.56 0 0 1-1.59-1.53V13a1 1 0 0 0-2 0v2.47A3.56 3.56 0 0 0 6.09 19h12l-1.3 1.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l3-3a1 1 0 0 0 0-1.42z\"/></g></g>","rewind-left":"<g data-name=\"Layer 2\"><g data-name=\"rewind-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.45 6.2a2.1 2.1 0 0 0-2.21.26l-4.74 3.92V7.79a1.76 1.76 0 0 0-1.05-1.59 2.1 2.1 0 0 0-2.21.26l-5.1 4.21a1.7 1.7 0 0 0 0 2.66l5.1 4.21a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59v-2.59l4.74 3.92a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59V7.79a1.76 1.76 0 0 0-1.05-1.59z\"/></g></g>","rewind-right":"<g data-name=\"Layer 2\"><g data-name=\"rewind-right\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.86 10.67l-5.1-4.21a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1.05 1.59v2.59L7.76 6.46a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1 1.59v8.42a1.76 1.76 0 0 0 1 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l4.74-3.92v2.59a1.76 1.76 0 0 0 1.05 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l5.1-4.21a1.7 1.7 0 0 0 0-2.66z\"/></g></g>","save":"<g data-name=\"Layer 2\"><g data-name=\"save\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"10\" y=\"17\" width=\"4\" height=\"4\"/><path d=\"M20.12 8.71l-4.83-4.83A3 3 0 0 0 13.17 3H10v6h5a1 1 0 0 1 0 2H9a1 1 0 0 1-1-1V3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h2v-4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v4h2a3 3 0 0 0 3-3v-7.17a3 3 0 0 0-.88-2.12z\"/></g></g>","scissors":"<g data-name=\"Layer 2\"><g data-name=\"scissors\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.21 5.71a1 1 0 1 0-1.42-1.42l-6.28 6.31-3.3-3.31A3 3 0 0 0 9.5 6a3 3 0 1 0-3 3 3 3 0 0 0 1.29-.3L11.1 12l-3.29 3.3A3 3 0 0 0 6.5 15a3 3 0 1 0 3 3 3 3 0 0 0-.29-1.26zM6.5 7a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm0 12a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M15.21 13.29a1 1 0 0 0-1.42 1.42l5 5a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","search":"<g data-name=\"Layer 2\"><g data-name=\"search\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM5 11a6 6 0 1 1 6 6 6 6 0 0 1-6-6z\"/></g></g>","settings-2":"<g data-name=\"Layer 2\"><g data-name=\"settings-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"1.5\"/><path d=\"M20.32 9.37h-1.09c-.14 0-.24-.11-.3-.26a.34.34 0 0 1 0-.37l.81-.74a1.63 1.63 0 0 0 .5-1.18 1.67 1.67 0 0 0-.5-1.19L18.4 4.26a1.67 1.67 0 0 0-2.37 0l-.77.74a.38.38 0 0 1-.41 0 .34.34 0 0 1-.22-.29V3.68A1.68 1.68 0 0 0 13 2h-1.94a1.69 1.69 0 0 0-1.69 1.68v1.09c0 .14-.11.24-.26.3a.34.34 0 0 1-.37 0L8 4.26a1.72 1.72 0 0 0-1.19-.5 1.65 1.65 0 0 0-1.18.5L4.26 5.6a1.67 1.67 0 0 0 0 2.4l.74.74a.38.38 0 0 1 0 .41.34.34 0 0 1-.29.22H3.68A1.68 1.68 0 0 0 2 11.05v1.89a1.69 1.69 0 0 0 1.68 1.69h1.09c.14 0 .24.11.3.26a.34.34 0 0 1 0 .37l-.81.74a1.72 1.72 0 0 0-.5 1.19 1.66 1.66 0 0 0 .5 1.19l1.34 1.36a1.67 1.67 0 0 0 2.37 0l.77-.74a.38.38 0 0 1 .41 0 .34.34 0 0 1 .22.29v1.09A1.68 1.68 0 0 0 11.05 22h1.89a1.69 1.69 0 0 0 1.69-1.68v-1.09c0-.14.11-.24.26-.3a.34.34 0 0 1 .37 0l.76.77a1.72 1.72 0 0 0 1.19.5 1.65 1.65 0 0 0 1.18-.5l1.34-1.34a1.67 1.67 0 0 0 0-2.37l-.73-.73a.34.34 0 0 1 0-.37.34.34 0 0 1 .29-.22h1.09A1.68 1.68 0 0 0 22 13v-1.94a1.69 1.69 0 0 0-1.68-1.69zM12 15.5a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5z\"/></g></g>","settings":"<g data-name=\"Layer 2\"><g data-name=\"settings\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"1.5\"/><path d=\"M21.89 10.32L21.1 7.8a2.26 2.26 0 0 0-2.88-1.51l-.34.11a1.74 1.74 0 0 1-1.59-.26l-.11-.08a1.76 1.76 0 0 1-.69-1.43v-.28a2.37 2.37 0 0 0-.68-1.68 2.26 2.26 0 0 0-1.6-.67h-2.55a2.32 2.32 0 0 0-2.29 2.33v.24a1.94 1.94 0 0 1-.73 1.51l-.13.1a1.93 1.93 0 0 1-1.78.29 2.14 2.14 0 0 0-1.68.12 2.18 2.18 0 0 0-1.12 1.33l-.82 2.6a2.34 2.34 0 0 0 1.48 2.94h.16a1.83 1.83 0 0 1 1.12 1.22l.06.16a2.06 2.06 0 0 1-.23 1.86 2.37 2.37 0 0 0 .49 3.3l2.07 1.57a2.25 2.25 0 0 0 1.35.43A2 2 0 0 0 9 22a2.25 2.25 0 0 0 1.47-1l.23-.33a1.8 1.8 0 0 1 1.43-.77 1.75 1.75 0 0 1 1.5.78l.12.17a2.24 2.24 0 0 0 3.22.53L19 19.86a2.38 2.38 0 0 0 .5-3.23l-.26-.38A2 2 0 0 1 19 14.6a1.89 1.89 0 0 1 1.21-1.28l.2-.07a2.36 2.36 0 0 0 1.48-2.93zM12 15.5a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5z\"/></g></g>","shake":"<g data-name=\"Layer 2\"><g data-name=\"shake\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M5.5 18a1 1 0 0 1-.64-.24A8.81 8.81 0 0 1 1.5 11a8.81 8.81 0 0 1 3.36-6.76 1 1 0 1 1 1.28 1.52A6.9 6.9 0 0 0 3.5 11a6.9 6.9 0 0 0 2.64 5.24 1 1 0 0 1 .13 1.4 1 1 0 0 1-.77.36z\"/><path d=\"M12 7a4.09 4.09 0 0 1 1 .14V3a1 1 0 0 0-2 0v4.14A4.09 4.09 0 0 1 12 7z\"/><path d=\"M12 15a4.09 4.09 0 0 1-1-.14V20a1 1 0 0 0 2 0v-5.14a4.09 4.09 0 0 1-1 .14z\"/><path d=\"M16 16a1 1 0 0 1-.77-.36 1 1 0 0 1 .13-1.4A4.28 4.28 0 0 0 17 11a4.28 4.28 0 0 0-1.64-3.24 1 1 0 1 1 1.28-1.52A6.2 6.2 0 0 1 19 11a6.2 6.2 0 0 1-2.36 4.76A1 1 0 0 1 16 16z\"/><path d=\"M8 16a1 1 0 0 1-.64-.24A6.2 6.2 0 0 1 5 11a6.2 6.2 0 0 1 2.36-4.76 1 1 0 1 1 1.28 1.52A4.28 4.28 0 0 0 7 11a4.28 4.28 0 0 0 1.64 3.24 1 1 0 0 1 .13 1.4A1 1 0 0 1 8 16z\"/><path d=\"M18.5 18a1 1 0 0 1-.77-.36 1 1 0 0 1 .13-1.4A6.9 6.9 0 0 0 20.5 11a6.9 6.9 0 0 0-2.64-5.24 1 1 0 1 1 1.28-1.52A8.81 8.81 0 0 1 22.5 11a8.81 8.81 0 0 1-3.36 6.76 1 1 0 0 1-.64.24z\"/><path d=\"M12 12a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm0-1zm0 0zm0 0zm0 0zm0 0zm0 0zm0 0z\"/></g></g>","share":"<g data-name=\"Layer 2\"><g data-name=\"share\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 15a3 3 0 0 0-2.1.86L8 12.34V12v-.33l7.9-3.53A3 3 0 1 0 15 6v.34L7.1 9.86a3 3 0 1 0 0 4.28l7.9 3.53V18a3 3 0 1 0 3-3z\"/></g></g>","shield-off":"<g data-name=\"Layer 2\"><g data-name=\"shield-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M3.73 6.56A2 2 0 0 0 3 8.09v.14a15.17 15.17 0 0 0 7.72 13.2l.3.17a2 2 0 0 0 2 0l.3-.17a15.22 15.22 0 0 0 3-2.27z\"/><path d=\"M18.84 16A15.08 15.08 0 0 0 21 8.23v-.14a2 2 0 0 0-1-1.75L13 2.4a2 2 0 0 0-2 0L7.32 4.49z\"/><path d=\"M4.71 3.29a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","shield":"<g data-name=\"Layer 2\"><g data-name=\"shield\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 21.85a2 2 0 0 1-1-.25l-.3-.17A15.17 15.17 0 0 1 3 8.23v-.14a2 2 0 0 1 1-1.75l7-3.94a2 2 0 0 1 2 0l7 3.94a2 2 0 0 1 1 1.75v.14a15.17 15.17 0 0 1-7.72 13.2l-.3.17a2 2 0 0 1-.98.25z\"/></g></g>","shopping-bag":"<g data-name=\"Layer 2\"><g data-name=\"shopping-bag\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.12 6.71l-2.83-2.83A3 3 0 0 0 15.17 3H8.83a3 3 0 0 0-2.12.88L3.88 6.71A3 3 0 0 0 3 8.83V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V8.83a3 3 0 0 0-.88-2.12zM12 16a4 4 0 0 1-4-4 1 1 0 0 1 2 0 2 2 0 0 0 4 0 1 1 0 0 1 2 0 4 4 0 0 1-4 4zM6.41 7l1.71-1.71A1.05 1.05 0 0 1 8.83 5h6.34a1.05 1.05 0 0 1 .71.29L17.59 7z\"/></g></g>","shopping-cart":"<g data-name=\"Layer 2\"><g data-name=\"shopping-cart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.08 7a2 2 0 0 0-1.7-1H6.58L6 3.74A1 1 0 0 0 5 3H3a1 1 0 0 0 0 2h1.24L7 15.26A1 1 0 0 0 8 16h9a1 1 0 0 0 .89-.55l3.28-6.56A2 2 0 0 0 21.08 7z\"/><circle cx=\"7.5\" cy=\"19.5\" r=\"1.5\"/><circle cx=\"17.5\" cy=\"19.5\" r=\"1.5\"/></g></g>","shuffle-2":"<g data-name=\"Layer 2\"><g data-name=\"shuffle-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.71 14.29a1 1 0 0 0-1.42 1.42l.29.29H16a4 4 0 0 1 0-8h1.59l-.3.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 10a1 1 0 0 0 .71-.29l2-2A1 1 0 0 0 21 7a1 1 0 0 0-.29-.71l-2-2a1 1 0 0 0-1.42 1.42l.29.29H16a6 6 0 0 0-5 2.69A6 6 0 0 0 6 6H4a1 1 0 0 0 0 2h2a4 4 0 0 1 0 8H4a1 1 0 0 0 0 2h2a6 6 0 0 0 5-2.69A6 6 0 0 0 16 18h1.59l-.3.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 20a1 1 0 0 0 .71-.29l2-2A1 1 0 0 0 21 17a1 1 0 0 0-.29-.71z\"/></g></g>","shuffle":"<g data-name=\"Layer 2\"><g data-name=\"shuffle\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 9.31a1 1 0 0 0 1 1 1 1 0 0 0 1-1V5a1 1 0 0 0-1-1h-4.3a1 1 0 0 0-1 1 1 1 0 0 0 1 1h1.89L12 10.59 6.16 4.76a1 1 0 0 0-1.41 1.41L10.58 12l-6.29 6.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L18 7.42z\"/><path d=\"M19 13.68a1 1 0 0 0-1 1v1.91l-2.78-2.79a1 1 0 0 0-1.42 1.42L16.57 18h-1.88a1 1 0 0 0 0 2H19a1 1 0 0 0 1-1.11v-4.21a1 1 0 0 0-1-1z\"/></g></g>","skip-back":"<g data-name=\"Layer 2\"><g data-name=\"skip-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M16.45 6.2a2.1 2.1 0 0 0-2.21.26l-5.1 4.21-.14.15V7a1 1 0 0 0-2 0v10a1 1 0 0 0 2 0v-3.82l.14.15 5.1 4.21a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59V7.79a1.76 1.76 0 0 0-1.05-1.59z\"/></g></g>","skip-forward":"<g data-name=\"Layer 2\"><g data-name=\"skip-forward\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16 6a1 1 0 0 0-1 1v3.82l-.14-.15-5.1-4.21a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1 1.59v8.42a1.76 1.76 0 0 0 1 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l5.1-4.21.14-.15V17a1 1 0 0 0 2 0V7a1 1 0 0 0-1-1z\"/></g></g>","slash":"<g data-name=\"Layer 2\"><g data-name=\"slash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm8 10a7.92 7.92 0 0 1-1.69 4.9L7.1 5.69A7.92 7.92 0 0 1 12 4a8 8 0 0 1 8 8zM4 12a7.92 7.92 0 0 1 1.69-4.9L16.9 18.31A7.92 7.92 0 0 1 12 20a8 8 0 0 1-8-8z\"/></g></g>","smartphone":"<g data-name=\"Layer 2\"><g data-name=\"smartphone\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17 2H7a3 3 0 0 0-3 3v14a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3zm-5 16a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 12 18zm2.5-10h-5a1 1 0 0 1 0-2h5a1 1 0 0 1 0 2z\"/></g></g>","smiling-face":"<defs><style/></defs><g id=\"Layer_2\" data-name=\"Layer 2\"><g id=\"smiling-face\"><g id=\"smiling-face\" data-name=\"smiling-face\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm0 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16zm5 9a5 5 0 0 1-10 0z\" id=\"&#x1F3A8;-Icon-&#x421;olor\"/></g></g></g>","speaker":"<g data-name=\"Layer 2\"><g data-name=\"speaker\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><circle cx=\"12\" cy=\"15.5\" r=\"1.5\"/><circle cx=\"12\" cy=\"8\" r=\"1\"/><path d=\"M17 2H7a3 3 0 0 0-3 3v14a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3zm-5 3a3 3 0 1 1-3 3 3 3 0 0 1 3-3zm0 14a3.5 3.5 0 1 1 3.5-3.5A3.5 3.5 0 0 1 12 19z\"/></g></g>","square":"<g data-name=\"Layer 2\"><g data-name=\"square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 21H6a3 3 0 0 1-3-3V6a3 3 0 0 1 3-3h12a3 3 0 0 1 3 3v12a3 3 0 0 1-3 3zM6 5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V6a1 1 0 0 0-1-1z\"/></g></g>","star":"<g data-name=\"Layer 2\"><g data-name=\"star\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18z\"/></g></g>","stop-circle":"<g data-name=\"Layer 2\"><g data-name=\"stop-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm4 12.75A1.25 1.25 0 0 1 14.75 16h-5.5A1.25 1.25 0 0 1 8 14.75v-5.5A1.25 1.25 0 0 1 9.25 8h5.5A1.25 1.25 0 0 1 16 9.25z\"/><rect x=\"10\" y=\"10\" width=\"4\" height=\"4\"/></g></g>","sun":"<g data-name=\"Layer 2\"><g data-name=\"sun\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 6a1 1 0 0 0 1-1V3a1 1 0 0 0-2 0v2a1 1 0 0 0 1 1z\"/><path d=\"M21 11h-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M6 12a1 1 0 0 0-1-1H3a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1z\"/><path d=\"M6.22 5a1 1 0 0 0-1.39 1.47l1.44 1.39a1 1 0 0 0 .73.28 1 1 0 0 0 .72-.31 1 1 0 0 0 0-1.41z\"/><path d=\"M17 8.14a1 1 0 0 0 .69-.28l1.44-1.39A1 1 0 0 0 17.78 5l-1.44 1.42a1 1 0 0 0 0 1.41 1 1 0 0 0 .66.31z\"/><path d=\"M12 18a1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-2a1 1 0 0 0-1-1z\"/><path d=\"M17.73 16.14a1 1 0 0 0-1.39 1.44L17.78 19a1 1 0 0 0 .69.28 1 1 0 0 0 .72-.3 1 1 0 0 0 0-1.42z\"/><path d=\"M6.27 16.14l-1.44 1.39a1 1 0 0 0 0 1.42 1 1 0 0 0 .72.3 1 1 0 0 0 .67-.25l1.44-1.39a1 1 0 0 0-1.39-1.44z\"/><path d=\"M12 8a4 4 0 1 0 4 4 4 4 0 0 0-4-4z\"/></g></g>","swap":"<g data-name=\"Layer 2\"><g data-name=\"swap\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M4 9h13l-1.6 1.2a1 1 0 0 0-.2 1.4 1 1 0 0 0 .8.4 1 1 0 0 0 .6-.2l4-3a1 1 0 0 0 0-1.59l-3.86-3a1 1 0 0 0-1.23 1.58L17.08 7H4a1 1 0 0 0 0 2z\"/><path d=\"M20 16H7l1.6-1.2a1 1 0 0 0-1.2-1.6l-4 3a1 1 0 0 0 0 1.59l3.86 3a1 1 0 0 0 .61.21 1 1 0 0 0 .79-.39 1 1 0 0 0-.17-1.4L6.92 18H20a1 1 0 0 0 0-2z\"/></g></g>","sync":"<g data-name=\"Layer 2\"><g data-name=\"sync\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.66 10.37a.62.62 0 0 0 .07-.19l.75-4a1 1 0 0 0-2-.36l-.37 2a9.22 9.22 0 0 0-16.58.84 1 1 0 0 0 .55 1.3 1 1 0 0 0 1.31-.55A7.08 7.08 0 0 1 12.07 5a7.17 7.17 0 0 1 6.24 3.58l-1.65-.27a1 1 0 1 0-.32 2l4.25.71h.16a.93.93 0 0 0 .34-.06.33.33 0 0 0 .1-.06.78.78 0 0 0 .2-.11l.08-.1a1.07 1.07 0 0 0 .14-.16.58.58 0 0 0 .05-.16z\"/><path d=\"M19.88 14.07a1 1 0 0 0-1.31.56A7.08 7.08 0 0 1 11.93 19a7.17 7.17 0 0 1-6.24-3.58l1.65.27h.16a1 1 0 0 0 .16-2L3.41 13a.91.91 0 0 0-.33 0H3a1.15 1.15 0 0 0-.32.14 1 1 0 0 0-.18.18l-.09.1a.84.84 0 0 0-.07.19.44.44 0 0 0-.07.17l-.75 4a1 1 0 0 0 .8 1.22h.18a1 1 0 0 0 1-.82l.37-2a9.22 9.22 0 0 0 16.58-.83 1 1 0 0 0-.57-1.28z\"/></g></g>","text":"<g data-name=\"Layer 2\"><g data-name=\"text\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 4H4a1 1 0 0 0-1 1v3a1 1 0 0 0 2 0V6h6v13H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2h-2V6h6v2a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/></g></g>","thermometer-minus":"<g data-name=\"Layer 2\"><g data-name=\"thermometer-minus\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\"/><path d=\"M14 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm1-12.46V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 14 4a1 1 0 0 0-1 1v4.54z\"/></g></g>","thermometer-plus":"<g data-name=\"Layer 2\"><g data-name=\"thermometer-plus\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 5 6)\"/><path d=\"M14 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm1-12.46V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 14 4a1 1 0 0 0-1 1v4.54z\"/></g></g>","thermometer":"<g data-name=\"Layer 2\"><g data-name=\"thermometer\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm1-12.46V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 12 4a1 1 0 0 0-1 1v4.54z\"/></g></g>","toggle-left":"<g data-name=\"Layer 2\"><g data-name=\"toggle-left\"><rect x=\".02\" y=\".02\" width=\"23.97\" height=\"23.97\" transform=\"rotate(179.92 12.002 11.998)\" opacity=\"0\"/><path d=\"M15 5H9a7 7 0 0 0 0 14h6a7 7 0 0 0 0-14zM9 15a3 3 0 1 1 3-3 3 3 0 0 1-3 3z\"/><path d=\"M9 11a1 1 0 1 0 0 2 1 1 0 0 0 0-2z\"/></g></g>","toggle-right":"<g data-name=\"Layer 2\"><g data-name=\"toggle-right\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"15\" cy=\"12\" r=\"1\"/><path d=\"M15 5H9a7 7 0 0 0 0 14h6a7 7 0 0 0 0-14zm0 10a3 3 0 1 1 3-3 3 3 0 0 1-3 3z\"/></g></g>","trash-2":"<g data-name=\"Layer 2\"><g data-name=\"trash-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-5V4.33A2.42 2.42 0 0 0 13.5 2h-3A2.42 2.42 0 0 0 8 4.33V6H3a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2zM10 16a1 1 0 0 1-2 0v-4a1 1 0 0 1 2 0zm0-11.67c0-.16.21-.33.5-.33h3c.29 0 .5.17.5.33V6h-4zM16 16a1 1 0 0 1-2 0v-4a1 1 0 0 1 2 0z\"/></g></g>","trash":"<g data-name=\"Layer 2\"><g data-name=\"trash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-5V4.33A2.42 2.42 0 0 0 13.5 2h-3A2.42 2.42 0 0 0 8 4.33V6H3a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2zM10 4.33c0-.16.21-.33.5-.33h3c.29 0 .5.17.5.33V6h-4z\"/></g></g>","trending-down":"<g data-name=\"Layer 2\"><g data-name=\"trending-down\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M21 12a1 1 0 0 0-2 0v2.3l-4.24-5a1 1 0 0 0-1.27-.21L9.22 11.7 4.77 6.36a1 1 0 1 0-1.54 1.28l5 6a1 1 0 0 0 1.28.22l4.28-2.57 4 4.71H15a1 1 0 0 0 0 2h5a1.1 1.1 0 0 0 .36-.07l.14-.08a1.19 1.19 0 0 0 .15-.09.75.75 0 0 0 .14-.17 1.1 1.1 0 0 0 .09-.14.64.64 0 0 0 .05-.17A.78.78 0 0 0 21 17z\"/></g></g>","trending-up":"<g data-name=\"Layer 2\"><g data-name=\"trending-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M21 7a.78.78 0 0 0 0-.21.64.64 0 0 0-.05-.17 1.1 1.1 0 0 0-.09-.14.75.75 0 0 0-.14-.17l-.12-.07a.69.69 0 0 0-.19-.1h-.2A.7.7 0 0 0 20 6h-5a1 1 0 0 0 0 2h2.83l-4 4.71-4.32-2.57a1 1 0 0 0-1.28.22l-5 6a1 1 0 0 0 .13 1.41A1 1 0 0 0 4 18a1 1 0 0 0 .77-.36l4.45-5.34 4.27 2.56a1 1 0 0 0 1.27-.21L19 9.7V12a1 1 0 0 0 2 0V7z\"/></g></g>","tv":"<g data-name=\"Layer 2\"><g data-name=\"tv\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 6h-3.59l2.3-2.29a1 1 0 1 0-1.42-1.42L12 5.59l-3.29-3.3a1 1 0 1 0-1.42 1.42L9.59 6H6a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V9a3 3 0 0 0-3-3zm1 13a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1v-7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/></g></g>","twitter":"<g data-name=\"Layer 2\"><g data-name=\"twitter\"><polyline points=\"0 0 24 0 24 24 0 24\" opacity=\"0\"/><path d=\"M8.08 20A11.07 11.07 0 0 0 19.52 9 8.09 8.09 0 0 0 21 6.16a.44.44 0 0 0-.62-.51 1.88 1.88 0 0 1-2.16-.38 3.89 3.89 0 0 0-5.58-.17A4.13 4.13 0 0 0 11.49 9C8.14 9.2 5.84 7.61 4 5.43a.43.43 0 0 0-.75.24 9.68 9.68 0 0 0 4.6 10.05A6.73 6.73 0 0 1 3.38 18a.45.45 0 0 0-.14.84A11 11 0 0 0 8.08 20\"/></g></g>","umbrella":"<g data-name=\"Layer 2\"><g data-name=\"umbrella\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2A10 10 0 0 0 2 12a1 1 0 0 0 1 1h8v6a3 3 0 0 0 6 0 1 1 0 0 0-2 0 1 1 0 0 1-2 0v-6h8a1 1 0 0 0 1-1A10 10 0 0 0 12 2z\"/></g></g>","undo":"<g data-name=\"Layer 2\"><g data-name=\"undo\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M20.22 21a1 1 0 0 1-1-.76 8.91 8.91 0 0 0-7.8-6.69v1.12a1.78 1.78 0 0 1-1.09 1.64A2 2 0 0 1 8.18 16l-5.06-4.41a1.76 1.76 0 0 1 0-2.68l5.06-4.42a2 2 0 0 1 2.18-.3 1.78 1.78 0 0 1 1.09 1.64V7A10.89 10.89 0 0 1 21.5 17.75a10.29 10.29 0 0 1-.31 2.49 1 1 0 0 1-1 .76z\"/></g></g>","unlock":"<g data-name=\"Layer 2\"><g data-name=\"unlock\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"15\" r=\"1\"/><path d=\"M17 8h-7V6a2 2 0 0 1 4 0 1 1 0 0 0 2 0 4 4 0 0 0-8 0v2H7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-5 10a3 3 0 1 1 3-3 3 3 0 0 1-3 3z\"/></g></g>","upload":"<g data-name=\"Layer 2\"><g data-name=\"upload\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><rect x=\"4\" y=\"4\" width=\"16\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(180 12 5)\"/><rect x=\"17\" y=\"5\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(90 19 6)\"/><rect x=\"3\" y=\"5\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(90 5 6)\"/><path d=\"M8 14a1 1 0 0 1-.8-.4 1 1 0 0 1 .2-1.4l4-3a1 1 0 0 1 1.18 0l4 2.82a1 1 0 0 1 .24 1.39 1 1 0 0 1-1.4.24L12 11.24 8.6 13.8a1 1 0 0 1-.6.2z\"/><path d=\"M12 21a1 1 0 0 1-1-1v-8a1 1 0 0 1 2 0v8a1 1 0 0 1-1 1z\"/></g></g>","video-off":"<g data-name=\"Layer 2\"><g data-name=\"video-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.22 17.05L4.88 7.71 3.12 6 3 5.8A3 3 0 0 0 2 8v8a3 3 0 0 0 3 3h9a2.94 2.94 0 0 0 1.66-.51z\"/><path d=\"M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H7.83l1.29 1.29 6.59 6.59 2 2 2 2a1.73 1.73 0 0 0 .6.11 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63a1.6 1.6 0 0 0-1-1.48z\"/><path d=\"M17 15.59l-2-2L8.41 7l-2-2-1.7-1.71a1 1 0 0 0-1.42 1.42l.54.53L5.59 7l9.34 9.34 1.46 1.46 2.9 2.91a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","video":"<g data-name=\"Layer 2\"><g data-name=\"video\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h9a3 3 0 0 0 3-3v-1.45l2.16 2a1.74 1.74 0 0 0 1.16.45 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63A1.6 1.6 0 0 0 21 7.15z\"/></g></g>","volume-down":"<g data-name=\"Layer 2\"><g data-name=\"volume-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.78 8.37a1 1 0 1 0-1.56 1.26 4 4 0 0 1 0 4.74A1 1 0 0 0 20 16a1 1 0 0 0 .78-.37 6 6 0 0 0 0-7.26z\"/><path d=\"M16.47 3.12a1 1 0 0 0-1 0L9 7.57H4a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4A1.06 1.06 0 0 0 16 21a1 1 0 0 0 1-1V4a1 1 0 0 0-.53-.88z\"/></g></g>","volume-mute":"<g data-name=\"Layer 2\"><g data-name=\"volume-mute\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17 21a1.06 1.06 0 0 1-.57-.17L10 16.43H5a1 1 0 0 1-1-1V8.57a1 1 0 0 1 1-1h5l6.41-4.4A1 1 0 0 1 18 4v16a1 1 0 0 1-1 1z\"/></g></g>","volume-off":"<g data-name=\"Layer 2\"><g data-name=\"volume-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16.91 14.08l1.44 1.44a6 6 0 0 0-.07-7.15 1 1 0 1 0-1.56 1.26 4 4 0 0 1 .19 4.45z\"/><path d=\"M21 12a6.51 6.51 0 0 1-1.78 4.39l1.42 1.42A8.53 8.53 0 0 0 23 12a8.75 8.75 0 0 0-3.36-6.77 1 1 0 1 0-1.28 1.54A6.8 6.8 0 0 1 21 12z\"/><path d=\"M15 12.17V4a1 1 0 0 0-1.57-.83L9 6.2z\"/><path d=\"M4.74 7.57H2a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4A1.06 1.06 0 0 0 14 21a1 1 0 0 0 1-1v-2.17z\"/><path d=\"M4.71 3.29a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","volume-up":"<g data-name=\"Layer 2\"><g data-name=\"volume-up\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.28 8.37a1 1 0 1 0-1.56 1.26 4 4 0 0 1 0 4.74A1 1 0 0 0 17.5 16a1 1 0 0 0 .78-.37 6 6 0 0 0 0-7.26z\"/><path d=\"M19.64 5.23a1 1 0 1 0-1.28 1.54A6.8 6.8 0 0 1 21 12a6.8 6.8 0 0 1-2.64 5.23 1 1 0 0 0-.13 1.41A1 1 0 0 0 19 19a1 1 0 0 0 .64-.23A8.75 8.75 0 0 0 23 12a8.75 8.75 0 0 0-3.36-6.77z\"/><path d=\"M14.47 3.12a1 1 0 0 0-1 0L7 7.57H2a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4A1.06 1.06 0 0 0 14 21a1 1 0 0 0 1-1V4a1 1 0 0 0-.53-.88z\"/></g></g>","wifi-off":"<g data-name=\"Layer 2\"><g data-name=\"wifi-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/><path d=\"M12.44 11l-1.9-1.89-2.46-2.44-1.55-1.55-1.82-1.83a1 1 0 0 0-1.42 1.42l1.38 1.37 1.46 1.46 2.23 2.24 1.55 1.54 2.74 2.74 2.79 2.8 3.85 3.85a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M21.72 7.93A13.93 13.93 0 0 0 12 4a14.1 14.1 0 0 0-4.44.73l1.62 1.62a11.89 11.89 0 0 1 11.16 3 1 1 0 0 0 .69.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.03-1.39z\"/><path d=\"M3.82 6.65a14.32 14.32 0 0 0-1.54 1.28 1 1 0 0 0 1.38 1.44 13.09 13.09 0 0 1 1.6-1.29z\"/><path d=\"M17 13.14a1 1 0 0 0 .71.3 1 1 0 0 0 .72-1.69A9 9 0 0 0 12 9h-.16l2.35 2.35A7 7 0 0 1 17 13.14z\"/><path d=\"M7.43 10.26a8.8 8.8 0 0 0-1.9 1.49A1 1 0 0 0 7 13.14a7.3 7.3 0 0 1 2-1.41z\"/><path d=\"M8.53 15.4a1 1 0 1 0 1.39 1.44 3.06 3.06 0 0 1 3.84-.25l-2.52-2.52a5 5 0 0 0-2.71 1.33z\"/></g></g>","wifi":"<g data-name=\"Layer 2\"><g data-name=\"wifi\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/><path d=\"M12 14a5 5 0 0 0-3.47 1.4 1 1 0 1 0 1.39 1.44 3.08 3.08 0 0 1 4.16 0 1 1 0 1 0 1.39-1.44A5 5 0 0 0 12 14z\"/><path d=\"M12 9a9 9 0 0 0-6.47 2.75A1 1 0 0 0 7 13.14a7 7 0 0 1 10.08 0 1 1 0 0 0 .71.3 1 1 0 0 0 .72-1.69A9 9 0 0 0 12 9z\"/><path d=\"M21.72 7.93a14 14 0 0 0-19.44 0 1 1 0 0 0 1.38 1.44 12 12 0 0 1 16.68 0 1 1 0 0 0 .69.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.03-1.41z\"/></g></g>","activity-outline":"<g data-name=\"Layer 2\"><g data-name=\"activity\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M14.33 20h-.21a2 2 0 0 1-1.76-1.58L9.68 6l-2.76 6.4A1 1 0 0 1 6 13H3a1 1 0 0 1 0-2h2.34l2.51-5.79a2 2 0 0 1 3.79.38L14.32 18l2.76-6.38A1 1 0 0 1 18 11h3a1 1 0 0 1 0 2h-2.34l-2.51 5.79A2 2 0 0 1 14.33 20z\"/></g></g>","alert-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"alert-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><circle cx=\"12\" cy=\"16\" r=\"1\"/><path d=\"M12 7a1 1 0 0 0-1 1v5a1 1 0 0 0 2 0V8a1 1 0 0 0-1-1z\"/></g></g>","alert-triangle-outline":"<g data-name=\"Layer 2\"><g data-name=\"alert-triangle\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M22.56 16.3L14.89 3.58a3.43 3.43 0 0 0-5.78 0L1.44 16.3a3 3 0 0 0-.05 3A3.37 3.37 0 0 0 4.33 21h15.34a3.37 3.37 0 0 0 2.94-1.66 3 3 0 0 0-.05-3.04zm-1.7 2.05a1.31 1.31 0 0 1-1.19.65H4.33a1.31 1.31 0 0 1-1.19-.65 1 1 0 0 1 0-1l7.68-12.73a1.48 1.48 0 0 1 2.36 0l7.67 12.72a1 1 0 0 1 .01 1.01z\"/><circle cx=\"12\" cy=\"16\" r=\"1\"/><path d=\"M12 8a1 1 0 0 0-1 1v4a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/></g></g>","archive-outline":"<g data-name=\"Layer 2\"><g data-name=\"archive\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M21 6a3 3 0 0 0-3-3H6a3 3 0 0 0-2 5.22V18a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8.22A3 3 0 0 0 21 6zM6 5h12a1 1 0 0 1 0 2H6a1 1 0 0 1 0-2zm12 13a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V9h12z\"/><rect x=\"9\" y=\"12\" width=\"6\" height=\"2\" rx=\".87\" ry=\".87\"/></g></g>","arrow-back-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M19 11H7.14l3.63-4.36a1 1 0 1 0-1.54-1.28l-5 6a1.19 1.19 0 0 0-.09.15c0 .05 0 .08-.07.13A1 1 0 0 0 4 12a1 1 0 0 0 .07.36c0 .05 0 .08.07.13a1.19 1.19 0 0 0 .09.15l5 6A1 1 0 0 0 10 19a1 1 0 0 0 .64-.23 1 1 0 0 0 .13-1.41L7.14 13H19a1 1 0 0 0 0-2z\"/></g></g>","arrow-circle-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.31 12.41L13 13.66V8a1 1 0 0 0-2 0v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3a1 1 0 0 0 .33.21.94.94 0 0 0 .76 0 .54.54 0 0 0 .16-.1.49.49 0 0 0 .15-.1l3-2.86a1 1 0 0 0-1.38-1.45z\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/></g></g>","arrow-circle-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-left\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16 11h-5.66l1.25-1.31a1 1 0 0 0-1.45-1.38l-2.86 3a1 1 0 0 0-.09.13.72.72 0 0 0-.11.19.88.88 0 0 0-.06.28L7 12a1 1 0 0 0 .08.38 1 1 0 0 0 .21.32l3 3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L10.41 13H16a1 1 0 0 0 0-2z\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/></g></g>","arrow-circle-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M17 12v-.09a.88.88 0 0 0-.06-.28.72.72 0 0 0-.11-.19 1 1 0 0 0-.09-.13l-2.86-3a1 1 0 0 0-1.45 1.38L13.66 11H8a1 1 0 0 0 0 2h5.59l-1.3 1.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l3-3a1 1 0 0 0 .21-.32A1 1 0 0 0 17 12z\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/></g></g>","arrow-circle-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-circle-up\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12.71 7.29a1 1 0 0 0-.32-.21A1 1 0 0 0 12 7h-.1a.82.82 0 0 0-.27.06.72.72 0 0 0-.19.11 1 1 0 0 0-.13.09l-3 2.86a1 1 0 0 0 1.38 1.45L11 10.34V16a1 1 0 0 0 2 0v-5.59l1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/></g></g>","arrow-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-down\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M12 17a1.72 1.72 0 0 1-1.33-.64l-4.21-5.1a2.1 2.1 0 0 1-.26-2.21A1.76 1.76 0 0 1 7.79 8h8.42a1.76 1.76 0 0 1 1.59 1.05 2.1 2.1 0 0 1-.26 2.21l-4.21 5.1A1.72 1.72 0 0 1 12 17zm-3.91-7L12 14.82 16 10z\"/></g></g>","arrow-downward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-downward\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.77 13.36a1 1 0 0 0-1.41-.13L13 16.86V5a1 1 0 0 0-2 0v11.86l-4.36-3.63a1 1 0 1 0-1.28 1.54l6 5 .15.09.13.07a1 1 0 0 0 .72 0l.13-.07.15-.09 6-5a1 1 0 0 0 .13-1.41z\"/></g></g>","arrow-forward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-forward\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M5 13h11.86l-3.63 4.36a1 1 0 0 0 1.54 1.28l5-6a1.19 1.19 0 0 0 .09-.15c0-.05.05-.08.07-.13A1 1 0 0 0 20 12a1 1 0 0 0-.07-.36c0-.05-.05-.08-.07-.13a1.19 1.19 0 0 0-.09-.15l-5-6A1 1 0 0 0 14 5a1 1 0 0 0-.64.23 1 1 0 0 0-.13 1.41L16.86 11H5a1 1 0 0 0 0 2z\"/></g></g>","arrow-ios-back-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.83 19a1 1 0 0 1-.78-.37l-4.83-6a1 1 0 0 1 0-1.27l5-6a1 1 0 0 1 1.54 1.28L10.29 12l4.32 5.36a1 1 0 0 1-.78 1.64z\"/></g></g>","arrow-ios-downward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-downward\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 16a1 1 0 0 1-.64-.23l-6-5a1 1 0 1 1 1.28-1.54L12 13.71l5.36-4.32a1 1 0 0 1 1.41.15 1 1 0 0 1-.14 1.46l-6 4.83A1 1 0 0 1 12 16z\"/></g></g>","arrow-ios-forward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-forward\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M10 19a1 1 0 0 1-.64-.23 1 1 0 0 1-.13-1.41L13.71 12 9.39 6.63a1 1 0 0 1 .15-1.41 1 1 0 0 1 1.46.15l4.83 6a1 1 0 0 1 0 1.27l-5 6A1 1 0 0 1 10 19z\"/></g></g>","arrow-ios-upward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-ios-upward\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 15a1 1 0 0 1-.64-.23L12 10.29l-5.37 4.32a1 1 0 0 1-1.41-.15 1 1 0 0 1 .15-1.41l6-4.83a1 1 0 0 1 1.27 0l6 5a1 1 0 0 1 .13 1.41A1 1 0 0 1 18 15z\"/></g></g>","arrow-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-left\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.54 18a2.06 2.06 0 0 1-1.3-.46l-5.1-4.21a1.7 1.7 0 0 1 0-2.66l5.1-4.21a2.1 2.1 0 0 1 2.21-.26 1.76 1.76 0 0 1 1.05 1.59v8.42a1.76 1.76 0 0 1-1.05 1.59 2.23 2.23 0 0 1-.91.2zm-4.86-6l4.82 4V8.09z\"/></g></g>","arrow-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M10.46 18a2.23 2.23 0 0 1-.91-.2 1.76 1.76 0 0 1-1.05-1.59V7.79A1.76 1.76 0 0 1 9.55 6.2a2.1 2.1 0 0 1 2.21.26l5.1 4.21a1.7 1.7 0 0 1 0 2.66l-5.1 4.21a2.06 2.06 0 0 1-1.3.46zm0-10v7.9l4.86-3.9z\"/></g></g>","arrow-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M16.21 16H7.79a1.76 1.76 0 0 1-1.59-1 2.1 2.1 0 0 1 .26-2.21l4.21-5.1a1.76 1.76 0 0 1 2.66 0l4.21 5.1A2.1 2.1 0 0 1 17.8 15a1.76 1.76 0 0 1-1.59 1zM8 14h7.9L12 9.18z\"/></g></g>","arrow-upward-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrow-upward\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M5.23 10.64a1 1 0 0 0 1.41.13L11 7.14V19a1 1 0 0 0 2 0V7.14l4.36 3.63a1 1 0 1 0 1.28-1.54l-6-5-.15-.09-.13-.07a1 1 0 0 0-.72 0l-.13.07-.15.09-6 5a1 1 0 0 0-.13 1.41z\"/></g></g>","arrowhead-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.37 12.39L12 16.71l-5.36-4.48a1 1 0 1 0-1.28 1.54l6 5a1 1 0 0 0 1.27 0l6-4.83a1 1 0 0 0 .15-1.41 1 1 0 0 0-1.41-.14z\"/><path d=\"M11.36 11.77a1 1 0 0 0 1.27 0l6-4.83a1 1 0 0 0 .15-1.41 1 1 0 0 0-1.41-.15L12 9.71 6.64 5.23a1 1 0 0 0-1.28 1.54z\"/></g></g>","arrowhead-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M11.64 5.23a1 1 0 0 0-1.41.13l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63L7.29 12l4.48-5.37a1 1 0 0 0-.13-1.4z\"/><path d=\"M14.29 12l4.48-5.37a1 1 0 0 0-1.54-1.28l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63z\"/></g></g>","arrowhead-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M18.78 11.37l-4.78-6a1 1 0 0 0-1.41-.15 1 1 0 0 0-.15 1.41L16.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 13 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/><path d=\"M7 5.37a1 1 0 0 0-1.61 1.26L9.71 12l-4.48 5.36a1 1 0 0 0 .13 1.41A1 1 0 0 0 6 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 0-1.27z\"/></g></g>","arrowhead-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"arrowhead-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M6.63 11.61L12 7.29l5.37 4.48A1 1 0 0 0 18 12a1 1 0 0 0 .77-.36 1 1 0 0 0-.13-1.41l-6-5a1 1 0 0 0-1.27 0l-6 4.83a1 1 0 0 0-.15 1.41 1 1 0 0 0 1.41.14z\"/><path d=\"M12.64 12.23a1 1 0 0 0-1.27 0l-6 4.83a1 1 0 0 0-.15 1.41 1 1 0 0 0 1.41.15L12 14.29l5.37 4.48A1 1 0 0 0 18 19a1 1 0 0 0 .77-.36 1 1 0 0 0-.13-1.41z\"/></g></g>","at-outline":"<g data-name=\"Layer 2\"><g data-name=\"at\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13 2a10 10 0 0 0-5 19.1 10.15 10.15 0 0 0 4 .9 10 10 0 0 0 6.08-2.06 1 1 0 0 0 .19-1.4 1 1 0 0 0-1.41-.19A8 8 0 1 1 12.77 4 8.17 8.17 0 0 1 20 12.22v.68a1.71 1.71 0 0 1-1.78 1.7 1.82 1.82 0 0 1-1.62-1.88V8.4a1 1 0 0 0-1-1 1 1 0 0 0-1 .87 5 5 0 0 0-3.44-1.36A5.09 5.09 0 1 0 15.31 15a3.6 3.6 0 0 0 5.55.61A3.67 3.67 0 0 0 22 12.9v-.68A10.2 10.2 0 0 0 13 2zm-1.82 13.09A3.09 3.09 0 1 1 14.27 12a3.1 3.1 0 0 1-3.09 3.09z\"/></g></g>","attach-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"attach-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a5.86 5.86 0 0 1-6-5.7V6.13A4.24 4.24 0 0 1 10.33 2a4.24 4.24 0 0 1 4.34 4.13v10.18a2.67 2.67 0 0 1-5.33 0V6.92a1 1 0 0 1 1-1 1 1 0 0 1 1 1v9.39a.67.67 0 0 0 1.33 0V6.13A2.25 2.25 0 0 0 10.33 4 2.25 2.25 0 0 0 8 6.13V16.3a3.86 3.86 0 0 0 4 3.7 3.86 3.86 0 0 0 4-3.7V6.13a1 1 0 1 1 2 0V16.3a5.86 5.86 0 0 1-6 5.7z\"/></g></g>","attach-outline":"<g data-name=\"Layer 2\"><g data-name=\"attach\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.29 21a6.23 6.23 0 0 1-4.43-1.88 6 6 0 0 1-.22-8.49L12 3.2A4.11 4.11 0 0 1 15 2a4.48 4.48 0 0 1 3.19 1.35 4.36 4.36 0 0 1 .15 6.13l-7.4 7.43a2.54 2.54 0 0 1-1.81.75 2.72 2.72 0 0 1-1.95-.82 2.68 2.68 0 0 1-.08-3.77l6.83-6.86a1 1 0 0 1 1.37 1.41l-6.83 6.86a.68.68 0 0 0 .08.95.78.78 0 0 0 .53.23.56.56 0 0 0 .4-.16l7.39-7.43a2.36 2.36 0 0 0-.15-3.31 2.38 2.38 0 0 0-3.27-.15L6.06 12a4 4 0 0 0 .22 5.67 4.22 4.22 0 0 0 3 1.29 3.67 3.67 0 0 0 2.61-1.06l7.39-7.43a1 1 0 1 1 1.42 1.41l-7.39 7.43A5.65 5.65 0 0 1 9.29 21z\"/></g></g>","award-outline":"<g data-name=\"Layer 2\"><g data-name=\"award\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 20.75l-2.31-9A5.94 5.94 0 0 0 18 8 6 6 0 0 0 6 8a5.94 5.94 0 0 0 1.34 3.77L5 20.75a1 1 0 0 0 1.48 1.11l5.33-3.13 5.68 3.14A.91.91 0 0 0 18 22a1 1 0 0 0 1-1.25zM12 4a4 4 0 1 1-4 4 4 4 0 0 1 4-4zm.31 12.71a1 1 0 0 0-1 0l-3.75 2.2L9 13.21a5.94 5.94 0 0 0 5.92 0L16.45 19z\"/></g></g>","backspace-outline":"<g data-name=\"Layer 2\"><g data-name=\"backspace\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.14 4h-9.77a3 3 0 0 0-2 .78l-.1.11-6 7.48a1 1 0 0 0 .11 1.37l6 5.48a3 3 0 0 0 2 .78h9.77A1.84 1.84 0 0 0 22 18.18V5.82A1.84 1.84 0 0 0 20.14 4zM20 18h-9.63a1 1 0 0 1-.67-.26l-5.33-4.85 5.38-6.67a1 1 0 0 1 .62-.22H20z\"/><path d=\"M11.29 14.71a1 1 0 0 0 1.42 0l1.29-1.3 1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L15.41 12l1.3-1.29a1 1 0 0 0-1.42-1.42L14 10.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l1.3 1.29-1.3 1.29a1 1 0 0 0 0 1.42z\"/></g></g>","bar-chart-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"bar-chart-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M12 8a1 1 0 0 0-1 1v11a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/><path d=\"M19 4a1 1 0 0 0-1 1v15a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/><path d=\"M5 12a1 1 0 0 0-1 1v7a1 1 0 0 0 2 0v-7a1 1 0 0 0-1-1z\"/></g></g>","bar-chart-outline":"<g data-name=\"Layer 2\"><g data-name=\"bar-chart\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M12 4a1 1 0 0 0-1 1v15a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/><path d=\"M19 12a1 1 0 0 0-1 1v7a1 1 0 0 0 2 0v-7a1 1 0 0 0-1-1z\"/><path d=\"M5 8a1 1 0 0 0-1 1v11a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/></g></g>","battery-outline":"<g data-name=\"Layer 2\"><g data-name=\"battery\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15.83 6H4.17A2.31 2.31 0 0 0 2 8.43v7.14A2.31 2.31 0 0 0 4.17 18h11.66A2.31 2.31 0 0 0 18 15.57V8.43A2.31 2.31 0 0 0 15.83 6zm.17 9.57a.52.52 0 0 1-.17.43H4.18a.5.5 0 0 1-.18-.43V8.43A.53.53 0 0 1 4.17 8h11.65a.5.5 0 0 1 .18.43z\"/><path d=\"M21 9a1 1 0 0 0-1 1v4a1 1 0 0 0 2 0v-4a1 1 0 0 0-1-1z\"/></g></g>","behance-outline":"<g data-name=\"Layer 2\"><g data-name=\"behance\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M10.52 11.78a1.4 1.4 0 0 0 1.12-1.43c0-1-.77-1.6-1.94-1.6H7v6.5h2.7c1.3-.05 2.3-.72 2.3-1.88a1.52 1.52 0 0 0-1.48-1.59zM8.26 9.67h1.15c.6 0 .95.32.95.85s-.38.89-1.25.89h-.85zm1 4.57h-1V12.3h1.23c.75 0 1.17.38 1.17 1s-.42.94-1.44.94z\"/><path d=\"M14.75 10.3a2.11 2.11 0 0 0-2.28 2.25V13a2.15 2.15 0 0 0 2.34 2.31A2 2 0 0 0 17 13.75h-1.21a.9.9 0 0 1-1 .63 1.07 1.07 0 0 1-1.09-1.19v-.14H17v-.47a2.12 2.12 0 0 0-2.25-2.28zm1 2h-2.02a1 1 0 0 1 1-1.09 1 1 0 0 1 1 1.09z\"/><rect x=\"13.25\" y=\"9.2\" width=\"3\" height=\".5\"/></g></g>","bell-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"bell-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8.9 5.17A4.67 4.67 0 0 1 12.64 4a4.86 4.86 0 0 1 4.08 4.9v4.5a1.92 1.92 0 0 0 .1.59l3.6 3.6a1.58 1.58 0 0 0 .45-.6 1.62 1.62 0 0 0-.35-1.78l-1.8-1.81V8.94a6.86 6.86 0 0 0-5.82-6.88 6.71 6.71 0 0 0-5.32 1.61 6.88 6.88 0 0 0-.58.54l1.47 1.43a4.79 4.79 0 0 1 .43-.47z\"/><path d=\"M14 16.86l-.83-.86H5.51l1.18-1.18a2 2 0 0 0 .59-1.42v-3.29l-2-2a5.68 5.68 0 0 0 0 .59v4.7l-1.8 1.81A1.63 1.63 0 0 0 4.64 18H8v.34A3.84 3.84 0 0 0 12 22a3.88 3.88 0 0 0 4-3.22l-.83-.78zM12 20a1.88 1.88 0 0 1-2-1.66V18h4v.34A1.88 1.88 0 0 1 12 20z\"/><path d=\"M20.71 19.29L19.41 18l-2-2-9.52-9.53L6.42 5 4.71 3.29a1 1 0 0 0-1.42 1.42L5.53 7l1.75 1.7 7.31 7.3.07.07L16 17.41l.59.59 2.7 2.71a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","bell-outline":"<g data-name=\"Layer 2\"><g data-name=\"bell\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.52 15.21l-1.8-1.81V8.94a6.86 6.86 0 0 0-5.82-6.88 6.74 6.74 0 0 0-7.62 6.67v4.67l-1.8 1.81A1.64 1.64 0 0 0 4.64 18H8v.34A3.84 3.84 0 0 0 12 22a3.84 3.84 0 0 0 4-3.66V18h3.36a1.64 1.64 0 0 0 1.16-2.79zM14 18.34A1.88 1.88 0 0 1 12 20a1.88 1.88 0 0 1-2-1.66V18h4zM5.51 16l1.18-1.18a2 2 0 0 0 .59-1.42V8.73A4.73 4.73 0 0 1 8.9 5.17 4.67 4.67 0 0 1 12.64 4a4.86 4.86 0 0 1 4.08 4.9v4.5a2 2 0 0 0 .58 1.42L18.49 16z\"/></g></g>","bluetooth-outline":"<g data-name=\"Layer 2\"><g data-name=\"bluetooth\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.63 12l4-3.79a1.14 1.14 0 0 0-.13-1.77l-4.67-3.23a1.17 1.17 0 0 0-1.21-.08 1.15 1.15 0 0 0-.62 1v6.2l-3.19-4a1 1 0 0 0-1.56 1.3L9.72 12l-3.5 4.43a1 1 0 0 0 .16 1.4A1 1 0 0 0 7 18a1 1 0 0 0 .78-.38L11 13.56v6.29A1.16 1.16 0 0 0 12.16 21a1.16 1.16 0 0 0 .67-.21l4.64-3.18a1.17 1.17 0 0 0 .49-.85 1.15 1.15 0 0 0-.34-.91zM13 5.76l2.5 1.73L13 9.85zm0 12.49v-4.07l2.47 2.38z\"/></g></g>","book-open-outline":"<g data-name=\"Layer 2\"><g data-name=\"book-open\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.62 4.22a1 1 0 0 0-.84-.2L12 5.77 4.22 4A1 1 0 0 0 3 5v12.2a1 1 0 0 0 .78 1l8 1.8h.44l8-1.8a1 1 0 0 0 .78-1V5a1 1 0 0 0-.38-.78zM5 6.25l6 1.35v10.15L5 16.4zM19 16.4l-6 1.35V7.6l6-1.35z\"/></g></g>","book-outline":"<g data-name=\"Layer 2\"><g data-name=\"book\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 3H7a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM7 5h11v10H7a3 3 0 0 0-1 .18V6a1 1 0 0 1 1-1zm0 14a1 1 0 0 1 0-2h11v2z\"/></g></g>","bookmark-outline":"<g data-name=\"Layer 2\"><g data-name=\"bookmark\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M6.09 21.06a1 1 0 0 1-1-1L4.94 5.4a2.26 2.26 0 0 1 2.18-2.35L16.71 3a2.27 2.27 0 0 1 2.23 2.31l.14 14.66a1 1 0 0 1-.49.87 1 1 0 0 1-1 0l-5.7-3.16-5.29 3.23a1.2 1.2 0 0 1-.51.15zm5.76-5.55a1.11 1.11 0 0 1 .5.12l4.71 2.61-.12-12.95c0-.2-.13-.34-.21-.33l-9.6.09c-.08 0-.19.13-.19.33l.12 12.9 4.28-2.63a1.06 1.06 0 0 1 .51-.14z\"/></g></g>","briefcase-outline":"<g data-name=\"Layer 2\"><g data-name=\"briefcase\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 7h-3V5.5A2.5 2.5 0 0 0 13.5 3h-3A2.5 2.5 0 0 0 8 5.5V7H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-4 2v10H9V9zm-5-3.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5V7h-4zM4 18v-8a1 1 0 0 1 1-1h2v10H5a1 1 0 0 1-1-1zm16 0a1 1 0 0 1-1 1h-2V9h2a1 1 0 0 1 1 1z\"/></g></g>","browser-outline":"<g data-name=\"Layer 2\"><g data-name=\"browser\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1v-7h14zM5 9V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3z\"/><circle cx=\"8\" cy=\"7.03\" r=\"1\"/><circle cx=\"12\" cy=\"7.03\" r=\"1\"/></g></g>","brush-outline":"<g data-name=\"Layer 2\"><g data-name=\"brush\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 6.83a2.76 2.76 0 0 0-.82-2 2.89 2.89 0 0 0-4 0l-6.6 6.6h-.22a4.42 4.42 0 0 0-4.3 4.31L4 19a1 1 0 0 0 .29.73A1.05 1.05 0 0 0 5 20l3.26-.06a4.42 4.42 0 0 0 4.31-4.3v-.23l6.61-6.6A2.74 2.74 0 0 0 20 6.83zM8.25 17.94L6 18v-2.23a2.4 2.4 0 0 1 2.4-2.36 2.15 2.15 0 0 1 2.15 2.19 2.4 2.4 0 0 1-2.3 2.34zm9.52-10.55l-5.87 5.87a4.55 4.55 0 0 0-.52-.64 3.94 3.94 0 0 0-.64-.52l5.87-5.86a.84.84 0 0 1 1.16 0 .81.81 0 0 1 .23.59.79.79 0 0 1-.23.56z\"/></g></g>","bulb-outline":"<g data-name=\"Layer 2\"><g data-name=\"bulb\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 7a5 5 0 0 0-3 9v4a2 2 0 0 0 2 2h2a2 2 0 0 0 2-2v-4a5 5 0 0 0-3-9zm1.5 7.59a1 1 0 0 0-.5.87V20h-2v-4.54a1 1 0 0 0-.5-.87A3 3 0 0 1 9 12a3 3 0 0 1 6 0 3 3 0 0 1-1.5 2.59z\"/><path d=\"M12 6a1 1 0 0 0 1-1V3a1 1 0 0 0-2 0v2a1 1 0 0 0 1 1z\"/><path d=\"M21 11h-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M5 11H3a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M7.66 6.42L6.22 5a1 1 0 0 0-1.39 1.47l1.44 1.39a1 1 0 0 0 .73.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.06-1.41z\"/><path d=\"M19.19 5.05a1 1 0 0 0-1.41 0l-1.44 1.37a1 1 0 0 0 0 1.41 1 1 0 0 0 .72.31 1 1 0 0 0 .69-.28l1.44-1.39a1 1 0 0 0 0-1.42z\"/></g></g>","calendar-outline":"<g data-name=\"Layer 2\"><g data-name=\"calendar\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 4h-1V3a1 1 0 0 0-2 0v1H9V3a1 1 0 0 0-2 0v1H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zM6 6h1v1a1 1 0 0 0 2 0V6h6v1a1 1 0 0 0 2 0V6h1a1 1 0 0 1 1 1v4H5V7a1 1 0 0 1 1-1zm12 14H6a1 1 0 0 1-1-1v-6h14v6a1 1 0 0 1-1 1z\"/><circle cx=\"8\" cy=\"16\" r=\"1\"/><path d=\"M16 15h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/></g></g>","camera-outline":"<g data-name=\"Layer 2\"><g data-name=\"camera\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 7h-3V5.5A2.5 2.5 0 0 0 13.5 3h-3A2.5 2.5 0 0 0 8 5.5V7H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-9-1.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5V7h-4zM20 18a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-8a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1z\"/><path d=\"M12 10.5a3.5 3.5 0 1 0 3.5 3.5 3.5 3.5 0 0 0-3.5-3.5zm0 5a1.5 1.5 0 1 1 1.5-1.5 1.5 1.5 0 0 1-1.5 1.5z\"/></g></g>","car-outline":"<g data-name=\"Layer 2\"><g data-name=\"car\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.6 11.22L17 7.52V5a1.91 1.91 0 0 0-1.81-2H3.79A1.91 1.91 0 0 0 2 5v10a2 2 0 0 0 1.2 1.88 3 3 0 1 0 5.6.12h6.36a3 3 0 1 0 5.64 0h.2a1 1 0 0 0 1-1v-4a1 1 0 0 0-.4-.78zM20 12.48V15h-3v-4.92zM7 18a1 1 0 1 1-1-1 1 1 0 0 1 1 1zm5-3H4V5h11v10zm7 3a1 1 0 1 1-1-1 1 1 0 0 1 1 1z\"/></g></g>","cast-outline":"<g data-name=\"Layer 2\"><g data-name=\"cast\"><polyline points=\"24 24 0 24 0 0\" opacity=\"0\"/><path d=\"M18.4 3H5.6A2.7 2.7 0 0 0 3 5.78V7a1 1 0 0 0 2 0V5.78A.72.72 0 0 1 5.6 5h12.8a.72.72 0 0 1 .6.78v12.44a.72.72 0 0 1-.6.78H17a1 1 0 0 0 0 2h1.4a2.7 2.7 0 0 0 2.6-2.78V5.78A2.7 2.7 0 0 0 18.4 3z\"/><path d=\"M3.86 14A1 1 0 0 0 3 15.17a1 1 0 0 0 1.14.83 2.49 2.49 0 0 1 2.12.72 2.52 2.52 0 0 1 .51 2.84 1 1 0 0 0 .48 1.33 1.06 1.06 0 0 0 .42.09 1 1 0 0 0 .91-.58A4.52 4.52 0 0 0 3.86 14z\"/><path d=\"M3.86 10.08a1 1 0 0 0 .28 2 6 6 0 0 1 5.09 1.71 6 6 0 0 1 1.53 5.95 1 1 0 0 0 .68 1.26.9.9 0 0 0 .28 0 1 1 0 0 0 1-.72 8 8 0 0 0-8.82-10.2z\"/><circle cx=\"4\" cy=\"19\" r=\"1\"/></g></g>","charging-outline":"<g data-name=\"Layer 2\"><g data-name=\"charging\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 9a1 1 0 0 0-1 1v4a1 1 0 0 0 2 0v-4a1 1 0 0 0-1-1z\"/><path d=\"M15.83 6h-3.1l-1.14 2h4.23a.5.5 0 0 1 .18.43v7.14a.52.52 0 0 1-.17.43H13l-1.15 2h4A2.31 2.31 0 0 0 18 15.57V8.43A2.31 2.31 0 0 0 15.83 6z\"/><path d=\"M4 15.57V8.43A.53.53 0 0 1 4.17 8H7l1.13-2h-4A2.31 2.31 0 0 0 2 8.43v7.14A2.31 2.31 0 0 0 4.17 18h3.1l1.14-2H4.18a.5.5 0 0 1-.18-.43z\"/><path d=\"M9 20a1 1 0 0 1-.87-1.5l3.15-5.5H7a1 1 0 0 1-.86-.5 1 1 0 0 1 0-1l4-7a1 1 0 0 1 1.74 1L8.72 11H13a1 1 0 0 1 .86.5 1 1 0 0 1 0 1l-4 7A1 1 0 0 1 9 20z\"/></g></g>","checkmark-circle-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-circle-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M14.7 8.39l-3.78 5-1.63-2.11a1 1 0 0 0-1.58 1.23l2.43 3.11a1 1 0 0 0 .79.38 1 1 0 0 0 .79-.39l4.57-6a1 1 0 1 0-1.6-1.22z\"/></g></g>","checkmark-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.71 11.29a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 16a1 1 0 0 0 .72-.34l7-8a1 1 0 0 0-1.5-1.32L12 13.54z\"/><path d=\"M21 11a1 1 0 0 0-1 1 8 8 0 0 1-8 8A8 8 0 0 1 6.33 6.36 7.93 7.93 0 0 1 12 4a8.79 8.79 0 0 1 1.9.22 1 1 0 1 0 .47-1.94A10.54 10.54 0 0 0 12 2a10 10 0 0 0-7 17.09A9.93 9.93 0 0 0 12 22a10 10 0 0 0 10-10 1 1 0 0 0-1-1z\"/></g></g>","checkmark-outline":"<g data-name=\"Layer 2\"><g data-name=\"checkmark\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9.86 18a1 1 0 0 1-.73-.32l-4.86-5.17a1 1 0 1 1 1.46-1.37l4.12 4.39 8.41-9.2a1 1 0 1 1 1.48 1.34l-9.14 10a1 1 0 0 1-.73.33z\"/></g></g>","checkmark-square-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-square-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/><path d=\"M14.7 8.39l-3.78 5-1.63-2.11a1 1 0 0 0-1.58 1.23l2.43 3.11a1 1 0 0 0 .79.38 1 1 0 0 0 .79-.39l4.57-6a1 1 0 1 0-1.6-1.22z\"/></g></g>","checkmark-square-outline":"<g data-name=\"Layer 2\"><g data-name=\"checkmark-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 11.83a1 1 0 0 0-1 1v5.57a.6.6 0 0 1-.6.6H5.6a.6.6 0 0 1-.6-.6V5.6a.6.6 0 0 1 .6-.6h9.57a1 1 0 1 0 0-2H5.6A2.61 2.61 0 0 0 3 5.6v12.8A2.61 2.61 0 0 0 5.6 21h12.8a2.61 2.61 0 0 0 2.6-2.6v-5.57a1 1 0 0 0-1-1z\"/><path d=\"M10.72 11a1 1 0 0 0-1.44 1.38l2.22 2.33a1 1 0 0 0 .72.31 1 1 0 0 0 .72-.3l6.78-7a1 1 0 1 0-1.44-1.4l-6.05 6.26z\"/></g></g>","chevron-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"chevron-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 15.5a1 1 0 0 1-.71-.29l-4-4a1 1 0 1 1 1.42-1.42L12 13.1l3.3-3.18a1 1 0 1 1 1.38 1.44l-4 3.86a1 1 0 0 1-.68.28z\"/></g></g>","chevron-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"chevron-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M13.36 17a1 1 0 0 1-.72-.31l-3.86-4a1 1 0 0 1 0-1.4l4-4a1 1 0 1 1 1.42 1.42L10.9 12l3.18 3.3a1 1 0 0 1 0 1.41 1 1 0 0 1-.72.29z\"/></g></g>","chevron-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"chevron-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M10.5 17a1 1 0 0 1-.71-.29 1 1 0 0 1 0-1.42L13.1 12 9.92 8.69a1 1 0 0 1 0-1.41 1 1 0 0 1 1.42 0l3.86 4a1 1 0 0 1 0 1.4l-4 4a1 1 0 0 1-.7.32z\"/></g></g>","chevron-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"chevron-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M16 14.5a1 1 0 0 1-.71-.29L12 10.9l-3.3 3.18a1 1 0 0 1-1.41 0 1 1 0 0 1 0-1.42l4-3.86a1 1 0 0 1 1.4 0l4 4a1 1 0 0 1 0 1.42 1 1 0 0 1-.69.28z\"/></g></g>","clipboard-outline":"<g data-name=\"Layer 2\"><g data-name=\"clipboard\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 5V4a2 2 0 0 0-2-2H8a2 2 0 0 0-2 2v1a3 3 0 0 0-3 3v11a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V8a3 3 0 0 0-3-3zM8 4h8v4H8V4zm11 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1v1a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V7a1 1 0 0 1 1 1z\"/></g></g>","clock-outline":"<g data-name=\"Layer 2\"><g data-name=\"clock\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M16 11h-3V8a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1h4a1 1 0 0 0 0-2z\"/></g></g>","close-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"close-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M14.71 9.29a1 1 0 0 0-1.42 0L12 10.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l1.3 1.29-1.3 1.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l1.29-1.3 1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L13.41 12l1.3-1.29a1 1 0 0 0 0-1.42z\"/></g></g>","close-outline":"<g data-name=\"Layer 2\"><g data-name=\"close\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M13.41 12l4.3-4.29a1 1 0 1 0-1.42-1.42L12 10.59l-4.29-4.3a1 1 0 0 0-1.42 1.42l4.3 4.29-4.3 4.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l4.29-4.3 4.29 4.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","close-square-outline":"<g data-name=\"Layer 2\"><g data-name=\"close-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/><path d=\"M14.71 9.29a1 1 0 0 0-1.42 0L12 10.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l1.3 1.29-1.3 1.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l1.29-1.3 1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L13.41 12l1.3-1.29a1 1 0 0 0 0-1.42z\"/></g></g>","cloud-download-outline":"<g data-name=\"Layer 2\"><g data-name=\"cloud-download\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14.31 16.38L13 17.64V12a1 1 0 0 0-2 0v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 21a1 1 0 0 0 .69-.28l3-2.9a1 1 0 1 0-1.38-1.44z\"/><path d=\"M17.67 7A6 6 0 0 0 6.33 7a5 5 0 0 0-3.08 8.27A1 1 0 1 0 4.75 14 3 3 0 0 1 7 9h.1a1 1 0 0 0 1-.8 4 4 0 0 1 7.84 0 1 1 0 0 0 1 .8H17a3 3 0 0 1 2.25 5 1 1 0 0 0 .09 1.42 1 1 0 0 0 .66.25 1 1 0 0 0 .75-.34A5 5 0 0 0 17.67 7z\"/></g></g>","cloud-upload-outline":"<g data-name=\"Layer 2\"><g data-name=\"cloud-upload\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12.71 11.29a1 1 0 0 0-1.4 0l-3 2.9a1 1 0 1 0 1.38 1.44L11 14.36V20a1 1 0 0 0 2 0v-5.59l1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M17.67 7A6 6 0 0 0 6.33 7a5 5 0 0 0-3.08 8.27A1 1 0 1 0 4.75 14 3 3 0 0 1 7 9h.1a1 1 0 0 0 1-.8 4 4 0 0 1 7.84 0 1 1 0 0 0 1 .8H17a3 3 0 0 1 2.25 5 1 1 0 0 0 .09 1.42 1 1 0 0 0 .66.25 1 1 0 0 0 .75-.34A5 5 0 0 0 17.67 7z\"/></g></g>","code-download-outline":"<g data-name=\"Layer 2\"><g data-name=\"code-download\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M4.29 12l4.48-5.36a1 1 0 1 0-1.54-1.28l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63z\"/><path d=\"M21.78 11.37l-4.78-6a1 1 0 0 0-1.56 1.26L19.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 16 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/><path d=\"M15.72 11.41a1 1 0 0 0-1.41 0L13 12.64V8a1 1 0 0 0-2 0v4.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 16a1 1 0 0 0 .69-.28l3-2.9a1 1 0 0 0 .03-1.41z\"/></g></g>","code-outline":"<g data-name=\"Layer 2\"><g data-name=\"code\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M8.64 5.23a1 1 0 0 0-1.41.13l-5 6a1 1 0 0 0 0 1.27l4.83 6a1 1 0 0 0 .78.37 1 1 0 0 0 .78-1.63L4.29 12l4.48-5.36a1 1 0 0 0-.13-1.41z\"/><path d=\"M21.78 11.37l-4.78-6a1 1 0 0 0-1.41-.15 1 1 0 0 0-.15 1.41L19.71 12l-4.48 5.37a1 1 0 0 0 .13 1.41A1 1 0 0 0 16 19a1 1 0 0 0 .77-.36l5-6a1 1 0 0 0 .01-1.27z\"/></g></g>","collapse-outline":"<g data-name=\"Layer 2\"><g data-name=\"collapse\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 9h-2.58l3.29-3.29a1 1 0 1 0-1.42-1.42L15 7.57V5a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h5a1 1 0 0 0 0-2z\"/><path d=\"M10 13H5a1 1 0 0 0 0 2h2.57l-3.28 3.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L9 16.42V19a1 1 0 0 0 1 1 1 1 0 0 0 1-1v-5a1 1 0 0 0-1-1z\"/></g></g>","color-palette-outline":"<g data-name=\"Layer 2\"><g data-name=\"color-palette\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.54 5.08A10.61 10.61 0 0 0 11.91 2a10 10 0 0 0-.05 20 2.58 2.58 0 0 0 2.53-1.89 2.52 2.52 0 0 0-.57-2.28.5.5 0 0 1 .37-.83h1.65A6.15 6.15 0 0 0 22 11.33a8.48 8.48 0 0 0-2.46-6.25zM15.88 15h-1.65a2.49 2.49 0 0 0-1.87 4.15.49.49 0 0 1 .12.49c-.05.21-.28.34-.59.36a8 8 0 0 1-7.82-9.11A8.1 8.1 0 0 1 11.92 4H12a8.47 8.47 0 0 1 6.1 2.48 6.5 6.5 0 0 1 1.9 4.77A4.17 4.17 0 0 1 15.88 15z\"/><circle cx=\"12\" cy=\"6.5\" r=\"1.5\"/><path d=\"M15.25 7.2a1.5 1.5 0 1 0 2.05.55 1.5 1.5 0 0 0-2.05-.55z\"/><path d=\"M8.75 7.2a1.5 1.5 0 1 0 .55 2.05 1.5 1.5 0 0 0-.55-2.05z\"/><path d=\"M6.16 11.26a1.5 1.5 0 1 0 2.08.4 1.49 1.49 0 0 0-2.08-.4z\"/></g></g>","color-picker-outline":"<g data-name=\"Layer 2\"><g data-name=\"color-picker\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.4 7.34L16.66 4.6A1.92 1.92 0 0 0 14 4.53l-2 2-1.29-1.24a1 1 0 0 0-1.42 1.42L10.53 8 5 13.53a2 2 0 0 0-.57 1.21L4 18.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 20h.09l4.17-.38a2 2 0 0 0 1.21-.57l5.58-5.58 1.24 1.24a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42l-1.24-1.24 2-2a1.92 1.92 0 0 0-.07-2.71zM9.08 17.62l-3 .28.27-3L12 9.36l2.69 2.7zm7-7L13.36 8l1.91-2L18 8.73z\"/></g></g>","compass-outline":"<g data-name=\"Layer 2\"><g data-name=\"compass\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M15.68 8.32a1 1 0 0 0-1.1-.25l-4.21 1.7a1 1 0 0 0-.55.55l-1.75 4.26a1 1 0 0 0 .18 1h.05A1 1 0 0 0 9 16a1 1 0 0 0 .38-.07l4.21-1.7a1 1 0 0 0 .55-.55l1.75-4.26a1 1 0 0 0-.21-1.1zm-4.88 4.89l.71-1.74 1.69-.68-.71 1.74z\"/></g></g>","copy-outline":"<g data-name=\"Layer 2\"><g data-name=\"copy\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 21h-6a3 3 0 0 1-3-3v-6a3 3 0 0 1 3-3h6a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3zm-6-10a1 1 0 0 0-1 1v6a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-6a1 1 0 0 0-1-1z\"/><path d=\"M9.73 15H5.67A2.68 2.68 0 0 1 3 12.33V5.67A2.68 2.68 0 0 1 5.67 3h6.66A2.68 2.68 0 0 1 15 5.67V9.4h-2V5.67a.67.67 0 0 0-.67-.67H5.67a.67.67 0 0 0-.67.67v6.66a.67.67 0 0 0 .67.67h4.06z\"/></g></g>","corner-down-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-down-left\"><rect x=\".05\" y=\".05\" width=\"24\" height=\"24\" transform=\"rotate(-89.76 12.05 12.05)\" opacity=\"0\"/><path d=\"M20 6a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 1-.29.71A1 1 0 0 1 17 12H8.08l2.69-3.39a1 1 0 0 0-1.52-1.17l-4 5a1 1 0 0 0 0 1.25l4 5a1 1 0 0 0 .78.37 1 1 0 0 0 .62-.22 1 1 0 0 0 .15-1.41l-2.66-3.36h8.92a3 3 0 0 0 3-3z\"/></g></g>","corner-down-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-down-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19.78 12.38l-4-5a1 1 0 0 0-1.56 1.24l2.7 3.38H8a1 1 0 0 1-1-1V6a1 1 0 0 0-2 0v5a3 3 0 0 0 3 3h8.92l-2.7 3.38a1 1 0 0 0 .16 1.4A1 1 0 0 0 15 19a1 1 0 0 0 .78-.38l4-5a1 1 0 0 0 0-1.24z\"/></g></g>","corner-left-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-left-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 5h-5a3 3 0 0 0-3 3v8.92l-3.38-2.7a1 1 0 0 0-1.24 1.56l5 4a1 1 0 0 0 1.24 0l5-4a1 1 0 1 0-1.24-1.56L12 16.92V8a1 1 0 0 1 1-1h5a1 1 0 0 0 0-2z\"/></g></g>","corner-left-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-left-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 17h-5a1 1 0 0 1-1-1V7.08l3.38 2.7A1 1 0 0 0 16 10a1 1 0 0 0 .78-.38 1 1 0 0 0-.16-1.4l-5-4a1 1 0 0 0-1.24 0l-5 4a1 1 0 0 0 1.24 1.56L10 7.08V16a3 3 0 0 0 3 3h5a1 1 0 0 0 0-2z\"/></g></g>","corner-right-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-right-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.78 14.38a1 1 0 0 0-1.4-.16L14 16.92V8a3 3 0 0 0-3-3H6a1 1 0 0 0 0 2h5a1 1 0 0 1 1 1v8.92l-3.38-2.7a1 1 0 0 0-1.24 1.56l5 4a1 1 0 0 0 1.24 0l5-4a1 1 0 0 0 .16-1.4z\"/></g></g>","corner-right-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-right-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.62 8.22l-5-4a1 1 0 0 0-1.24 0l-5 4a1 1 0 0 0 1.24 1.56L12 7.08V16a1 1 0 0 1-1 1H6a1 1 0 0 0 0 2h5a3 3 0 0 0 3-3V7.08l3.38 2.7A1 1 0 0 0 18 10a1 1 0 0 0 .78-.38 1 1 0 0 0-.16-1.4z\"/></g></g>","corner-up-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-up-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M16 10H7.08l2.7-3.38a1 1 0 1 0-1.56-1.24l-4 5a1 1 0 0 0 0 1.24l4 5A1 1 0 0 0 9 17a1 1 0 0 0 .62-.22 1 1 0 0 0 .16-1.4L7.08 12H16a1 1 0 0 1 1 1v5a1 1 0 0 0 2 0v-5a3 3 0 0 0-3-3z\"/></g></g>","corner-up-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"corner-up-right\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19.78 10.38l-4-5a1 1 0 0 0-1.56 1.24l2.7 3.38H8a3 3 0 0 0-3 3v5a1 1 0 0 0 2 0v-5a1 1 0 0 1 1-1h8.92l-2.7 3.38a1 1 0 0 0 .16 1.4A1 1 0 0 0 15 17a1 1 0 0 0 .78-.38l4-5a1 1 0 0 0 0-1.24z\"/></g></g>","credit-card-outline":"<g data-name=\"Layer 2\"><g data-name=\"credit-card\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 5H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V8a3 3 0 0 0-3-3zM4 8a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v1H4zm16 8a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-5h16z\"/><path d=\"M7 15h4a1 1 0 0 0 0-2H7a1 1 0 0 0 0 2z\"/><path d=\"M15 15h2a1 1 0 0 0 0-2h-2a1 1 0 0 0 0 2z\"/></g></g>","crop-outline":"<g data-name=\"Layer 2\"><g data-name=\"crop\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 16h-3V8.56A2.56 2.56 0 0 0 15.44 6H8V3a1 1 0 0 0-2 0v3H3a1 1 0 0 0 0 2h3v7.44A2.56 2.56 0 0 0 8.56 18H16v3a1 1 0 0 0 2 0v-3h3a1 1 0 0 0 0-2zM8.56 16a.56.56 0 0 1-.56-.56V8h7.44a.56.56 0 0 1 .56.56V16z\"/></g></g>","cube-outline":"<g data-name=\"Layer 2\"><g data-name=\"cube\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.66 7.26c0-.07-.1-.14-.15-.21l-.09-.1a2.5 2.5 0 0 0-.86-.68l-6.4-3a2.7 2.7 0 0 0-2.26 0l-6.4 3a2.6 2.6 0 0 0-.86.68L3.52 7a1 1 0 0 0-.15.2A2.39 2.39 0 0 0 3 8.46v7.06a2.49 2.49 0 0 0 1.46 2.26l6.4 3a2.7 2.7 0 0 0 2.27 0l6.4-3A2.49 2.49 0 0 0 21 15.54V8.46a2.39 2.39 0 0 0-.34-1.2zm-8.95-2.2a.73.73 0 0 1 .58 0l5.33 2.48L12 10.15 6.38 7.54zM5.3 16a.47.47 0 0 1-.3-.43V9.1l6 2.79v6.72zm13.39 0L13 18.61v-6.72l6-2.79v6.44a.48.48 0 0 1-.31.46z\"/></g></g>","diagonal-arrow-left-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-left-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.71 6.29a1 1 0 0 0-1.42 0L8 14.59V9a1 1 0 0 0-2 0v8a1 1 0 0 0 1 1h8a1 1 0 0 0 0-2H9.41l8.3-8.29a1 1 0 0 0 0-1.42z\"/></g></g>","diagonal-arrow-left-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-left-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17.71 16.29L9.42 8H15a1 1 0 0 0 0-2H7.05a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1H7a1 1 0 0 0 1-1V9.45l8.26 8.26a1 1 0 0 0 1.42 0 1 1 0 0 0 .03-1.42z\"/></g></g>","diagonal-arrow-right-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-right-down\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M17 8a1 1 0 0 0-1 1v5.59l-8.29-8.3a1 1 0 0 0-1.42 1.42l8.3 8.29H9a1 1 0 0 0 0 2h8a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1z\"/></g></g>","diagonal-arrow-right-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"diagonal-arrow-right-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 7.05a1 1 0 0 0-1-1L9 6a1 1 0 0 0 0 2h5.56l-8.27 8.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L16 9.42V15a1 1 0 0 0 1 1 1 1 0 0 0 1-1z\"/></g></g>","done-all-outline":"<g data-name=\"Layer 2\"><g data-name=\"done-all\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16.62 6.21a1 1 0 0 0-1.41.17l-7 9-3.43-4.18a1 1 0 1 0-1.56 1.25l4.17 5.18a1 1 0 0 0 .78.37 1 1 0 0 0 .83-.38l7.83-10a1 1 0 0 0-.21-1.41z\"/><path d=\"M21.62 6.21a1 1 0 0 0-1.41.17l-7 9-.61-.75-1.26 1.62 1.1 1.37a1 1 0 0 0 .78.37 1 1 0 0 0 .78-.38l7.83-10a1 1 0 0 0-.21-1.4z\"/><path d=\"M8.71 13.06L10 11.44l-.2-.24a1 1 0 0 0-1.43-.2 1 1 0 0 0-.15 1.41z\"/></g></g>","download-outline":"<g data-name=\"Layer 2\"><g data-name=\"download\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"4\" y=\"18\" width=\"16\" height=\"2\" rx=\"1\" ry=\"1\"/><rect x=\"3\" y=\"17\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 5 18)\"/><rect x=\"17\" y=\"17\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 19 18)\"/><path d=\"M12 15a1 1 0 0 1-.58-.18l-4-2.82a1 1 0 0 1-.24-1.39 1 1 0 0 1 1.4-.24L12 12.76l3.4-2.56a1 1 0 0 1 1.2 1.6l-4 3a1 1 0 0 1-.6.2z\"/><path d=\"M12 13a1 1 0 0 1-1-1V4a1 1 0 0 1 2 0v8a1 1 0 0 1-1 1z\"/></g></g>","droplet-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"droplet-off-outline\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 19a5.4 5.4 0 0 1-3.88-1.64 5.73 5.73 0 0 1-.69-7.11L6 8.82a7.74 7.74 0 0 0 .7 9.94A7.37 7.37 0 0 0 12 21a7.36 7.36 0 0 0 4.58-1.59L15.15 18A5.43 5.43 0 0 1 12 19z\"/><path d=\"M12 5.43l3.88 4a5.71 5.71 0 0 1 1.49 5.15L19 16.15A7.72 7.72 0 0 0 17.31 8l-4.6-4.7A1 1 0 0 0 12 3a1 1 0 0 0-.72.3L8.73 5.9l1.42 1.42z\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","droplet-outline":"<g data-name=\"Layer 2\"><g data-name=\"droplet-outline\"><rect x=\".1\" y=\".1\" width=\"24\" height=\"24\" transform=\"rotate(.48 11.987 11.887)\" opacity=\"0\"/><path d=\"M12 21.1a7.4 7.4 0 0 1-5.28-2.28 7.73 7.73 0 0 1 .1-10.77l4.64-4.65a.94.94 0 0 1 .71-.3 1 1 0 0 1 .71.31l4.56 4.72a7.73 7.73 0 0 1-.09 10.77A7.33 7.33 0 0 1 12 21.1zm.13-15.57L8.24 9.45a5.74 5.74 0 0 0-.07 8A5.43 5.43 0 0 0 12 19.1a5.42 5.42 0 0 0 3.9-1.61 5.72 5.72 0 0 0 .06-8z\"/></g></g>","edit-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"edit-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 20H5a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2z\"/><path d=\"M5 18h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71L16.66 2.6A2 2 0 0 0 14 2.53l-9 9a2 2 0 0 0-.57 1.21L4 16.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 18zM15.27 4L18 6.73l-2 1.95L13.32 6zm-8.9 8.91L12 7.32l2.7 2.7-5.6 5.6-3 .28z\"/></g></g>","edit-outline":"<g data-name=\"Layer 2\"><g data-name=\"edit\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.4 7.34L16.66 4.6A2 2 0 0 0 14 4.53l-9 9a2 2 0 0 0-.57 1.21L4 18.91a1 1 0 0 0 .29.8A1 1 0 0 0 5 20h.09l4.17-.38a2 2 0 0 0 1.21-.57l9-9a1.92 1.92 0 0 0-.07-2.71zM9.08 17.62l-3 .28.27-3L12 9.32l2.7 2.7zM16 10.68L13.32 8l1.95-2L18 8.73z\"/></g></g>","email-outline":"<g data-name=\"Layer 2\"><g data-name=\"email\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 4H5a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3V7a3 3 0 0 0-3-3zm-.67 2L12 10.75 5.67 6zM19 18H5a1 1 0 0 1-1-1V7.25l7.4 5.55a1 1 0 0 0 .6.2 1 1 0 0 0 .6-.2L20 7.25V17a1 1 0 0 1-1 1z\"/></g></g>","expand-outline":"<g data-name=\"Layer 2\"><g data-name=\"expand\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20 5a1 1 0 0 0-1-1h-5a1 1 0 0 0 0 2h2.57l-3.28 3.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L18 7.42V10a1 1 0 0 0 1 1 1 1 0 0 0 1-1z\"/><path d=\"M10.71 13.29a1 1 0 0 0-1.42 0L6 16.57V14a1 1 0 0 0-1-1 1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h5a1 1 0 0 0 0-2H7.42l3.29-3.29a1 1 0 0 0 0-1.42z\"/></g></g>","external-link-outline":"<g data-name=\"Layer 2\"><g data-name=\"external-link\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 11a1 1 0 0 0-1 1v6a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h6a1 1 0 0 0 0-2H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-6a1 1 0 0 0-1-1z\"/><path d=\"M16 5h1.58l-6.29 6.28a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L19 6.42V8a1 1 0 0 0 1 1 1 1 0 0 0 1-1V4a1 1 0 0 0-1-1h-4a1 1 0 0 0 0 2z\"/></g></g>","eye-off-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"eye-off-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.81 13.39A8.93 8.93 0 0 0 21 7.62a1 1 0 1 0-2-.24 7.07 7.07 0 0 1-14 0 1 1 0 1 0-2 .24 8.93 8.93 0 0 0 3.18 5.77l-2.3 2.32a1 1 0 0 0 1.41 1.41l2.61-2.6a9.06 9.06 0 0 0 3.1.92V19a1 1 0 0 0 2 0v-3.56a9.06 9.06 0 0 0 3.1-.92l2.61 2.6a1 1 0 0 0 1.41-1.41z\"/></g></g>","eye-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"eye-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M4.71 3.29a1 1 0 0 0-1.42 1.42l5.63 5.63a3.5 3.5 0 0 0 4.74 4.74l5.63 5.63a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM12 13.5a1.5 1.5 0 0 1-1.5-1.5v-.07l1.56 1.56z\"/><path d=\"M12.22 17c-4.3.1-7.12-3.59-8-5a13.7 13.7 0 0 1 2.24-2.72L5 7.87a15.89 15.89 0 0 0-2.87 3.63 1 1 0 0 0 0 1c.63 1.09 4 6.5 9.89 6.5h.25a9.48 9.48 0 0 0 3.23-.67l-1.58-1.58a7.74 7.74 0 0 1-1.7.25z\"/><path d=\"M21.87 11.5c-.64-1.11-4.17-6.68-10.14-6.5a9.48 9.48 0 0 0-3.23.67l1.58 1.58a7.74 7.74 0 0 1 1.7-.25c4.29-.11 7.11 3.59 8 5a13.7 13.7 0 0 1-2.29 2.72L19 16.13a15.89 15.89 0 0 0 2.91-3.63 1 1 0 0 0-.04-1z\"/></g></g>","eye-outline":"<g data-name=\"Layer 2\"><g data-name=\"eye\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.87 11.5c-.64-1.11-4.16-6.68-10.14-6.5-5.53.14-8.73 5-9.6 6.5a1 1 0 0 0 0 1c.63 1.09 4 6.5 9.89 6.5h.25c5.53-.14 8.74-5 9.6-6.5a1 1 0 0 0 0-1zM12.22 17c-4.31.1-7.12-3.59-8-5 1-1.61 3.61-4.9 7.61-5 4.29-.11 7.11 3.59 8 5-1.03 1.61-3.61 4.9-7.61 5z\"/><path d=\"M12 8.5a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 12 8.5zm0 5a1.5 1.5 0 1 1 1.5-1.5 1.5 1.5 0 0 1-1.5 1.5z\"/></g></g>","facebook-outline":"<g data-name=\"Layer 2\"><g data-name=\"facebook\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M13 22H9a1 1 0 0 1-1-1v-6.2H6a1 1 0 0 1-1-1v-3.6a1 1 0 0 1 1-1h2V7.5A5.77 5.77 0 0 1 14 2h3a1 1 0 0 1 1 1v3.6a1 1 0 0 1-1 1h-3v1.6h3a1 1 0 0 1 .8.39 1 1 0 0 1 .16.88l-1 3.6a1 1 0 0 1-1 .73H14V21a1 1 0 0 1-1 1zm-3-2h2v-6.2a1 1 0 0 1 1-1h2.24l.44-1.6H13a1 1 0 0 1-1-1V7.5a2 2 0 0 1 2-1.9h2V4h-2a3.78 3.78 0 0 0-4 3.5v2.7a1 1 0 0 1-1 1H7v1.6h2a1 1 0 0 1 1 1z\"/></g></g>","file-add-outline":"<g data-name=\"Layer 2\"><g data-name=\"file-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 8.33l-5.44-6a1 1 0 0 0-.74-.33h-7A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V9a1 1 0 0 0-.26-.67zM14 5l2.74 3h-2a.79.79 0 0 1-.74-.85zm3.44 15H6.56a.53.53 0 0 1-.56-.5v-15a.53.53 0 0 1 .56-.5H12v3.15A2.79 2.79 0 0 0 14.71 10H18v9.5a.53.53 0 0 1-.56.5z\"/><path d=\"M14 13h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2z\"/></g></g>","file-outline":"<g data-name=\"Layer 2\"><g data-name=\"file\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 8.33l-5.44-6a1 1 0 0 0-.74-.33h-7A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V9a1 1 0 0 0-.26-.67zM17.65 9h-3.94a.79.79 0 0 1-.71-.85V4h.11zm-.21 11H6.56a.53.53 0 0 1-.56-.5v-15a.53.53 0 0 1 .56-.5H11v4.15A2.79 2.79 0 0 0 13.71 11H18v8.5a.53.53 0 0 1-.56.5z\"/></g></g>","file-remove-outline":"<g data-name=\"Layer 2\"><g data-name=\"file-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 8.33l-5.44-6a1 1 0 0 0-.74-.33h-7A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V9a1 1 0 0 0-.26-.67zM14 5l2.74 3h-2a.79.79 0 0 1-.74-.85zm3.44 15H6.56a.53.53 0 0 1-.56-.5v-15a.53.53 0 0 1 .56-.5H12v3.15A2.79 2.79 0 0 0 14.71 10H18v9.5a.53.53 0 0 1-.56.5z\"/><path d=\"M14 13h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/></g></g>","file-text-outline":"<g data-name=\"Layer 2\"><g data-name=\"file-text\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15 16H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2z\"/><path d=\"M9 14h3a1 1 0 0 0 0-2H9a1 1 0 0 0 0 2z\"/><path d=\"M19.74 8.33l-5.44-6a1 1 0 0 0-.74-.33h-7A2.53 2.53 0 0 0 4 4.5v15A2.53 2.53 0 0 0 6.56 22h10.88A2.53 2.53 0 0 0 20 19.5V9a1 1 0 0 0-.26-.67zM14 5l2.74 3h-2a.79.79 0 0 1-.74-.85zm3.44 15H6.56a.53.53 0 0 1-.56-.5v-15a.53.53 0 0 1 .56-.5H12v3.15A2.79 2.79 0 0 0 14.71 10H18v9.5a.53.53 0 0 1-.56.5z\"/></g></g>","film-outline":"<g data-name=\"Layer 2\"><g data-name=\"film\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.26 3H5.74A2.74 2.74 0 0 0 3 5.74v12.52A2.74 2.74 0 0 0 5.74 21h12.52A2.74 2.74 0 0 0 21 18.26V5.74A2.74 2.74 0 0 0 18.26 3zM7 11H5V9h2zm-2 2h2v2H5zm4-8h6v14H9zm10 6h-2V9h2zm-2 2h2v2h-2zm2-7.26V7h-2V5h1.26a.74.74 0 0 1 .74.74zM5.74 5H7v2H5V5.74A.74.74 0 0 1 5.74 5zM5 18.26V17h2v2H5.74a.74.74 0 0 1-.74-.74zm14 0a.74.74 0 0 1-.74.74H17v-2h2z\"/></g></g>","flag-outline":"<g data-name=\"Layer 2\"><g data-name=\"flag\"><polyline points=\"24 24 0 24 0 0\" opacity=\"0\"/><path d=\"M19.27 4.68a1.79 1.79 0 0 0-1.6-.25 7.53 7.53 0 0 1-2.17.28 8.54 8.54 0 0 1-3.13-.78A10.15 10.15 0 0 0 8.5 3c-2.89 0-4 1-4.2 1.14a1 1 0 0 0-.3.72V20a1 1 0 0 0 2 0v-4.3a6.28 6.28 0 0 1 2.5-.41 8.54 8.54 0 0 1 3.13.78 10.15 10.15 0 0 0 3.87.93 7.66 7.66 0 0 0 3.5-.7 1.74 1.74 0 0 0 1-1.55V6.11a1.77 1.77 0 0 0-.73-1.43zM18 14.59a6.32 6.32 0 0 1-2.5.41 8.36 8.36 0 0 1-3.13-.79 10.34 10.34 0 0 0-3.87-.92 9.51 9.51 0 0 0-2.5.29V5.42A6.13 6.13 0 0 1 8.5 5a8.36 8.36 0 0 1 3.13.79 10.34 10.34 0 0 0 3.87.92 9.41 9.41 0 0 0 2.5-.3z\"/></g></g>","flash-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"flash-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M12.54 18.06l.27-2.42L10 12.8H6.87l1.24-1.86L6.67 9.5l-2.5 3.74A1 1 0 0 0 5 14.8h5.89l-.77 7.09a1 1 0 0 0 .65 1.05 1 1 0 0 0 .34.06 1 1 0 0 0 .83-.44l3.12-4.67-1.44-1.44z\"/><path d=\"M11.46 5.94l-.27 2.42L14 11.2h3.1l-1.24 1.86 1.44 1.44 2.5-3.74A1 1 0 0 0 19 9.2h-5.89l.77-7.09a1 1 0 0 0-.65-1 1 1 0 0 0-1.17.38L8.94 6.11l1.44 1.44z\"/></g></g>","flash-outline":"<g data-name=\"Layer 2\"><g data-name=\"flash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M11.11 23a1 1 0 0 1-.34-.06 1 1 0 0 1-.65-1.05l.77-7.09H5a1 1 0 0 1-.83-1.56l7.89-11.8a1 1 0 0 1 1.17-.38 1 1 0 0 1 .65 1l-.77 7.14H19a1 1 0 0 1 .83 1.56l-7.89 11.8a1 1 0 0 1-.83.44zM6.87 12.8H12a1 1 0 0 1 .74.33 1 1 0 0 1 .25.78l-.45 4.15 4.59-6.86H12a1 1 0 0 1-1-1.11l.45-4.15z\"/></g></g>","flip-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"flip-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M6.09 19h12l-1.3 1.29a1 1 0 0 0 1.42 1.42l3-3a1 1 0 0 0 0-1.42l-3-3a1 1 0 0 0-1.42 0 1 1 0 0 0 0 1.42l1.3 1.29h-12a1.56 1.56 0 0 1-1.59-1.53V13a1 1 0 0 0-2 0v2.47A3.56 3.56 0 0 0 6.09 19z\"/><path d=\"M5.79 9.71a1 1 0 1 0 1.42-1.42L5.91 7h12a1.56 1.56 0 0 1 1.59 1.53V11a1 1 0 0 0 2 0V8.53A3.56 3.56 0 0 0 17.91 5h-12l1.3-1.29a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0l-3 3a1 1 0 0 0 0 1.42z\"/></g></g>","flip-outline":"<g data-name=\"Layer 2\"><g data-name=\"flip-in\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M5 6.09v12l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3a1 1 0 0 0 1.42 0l3-3a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0L7 18.09v-12A1.56 1.56 0 0 1 8.53 4.5H11a1 1 0 0 0 0-2H8.53A3.56 3.56 0 0 0 5 6.09z\"/><path d=\"M14.29 5.79a1 1 0 0 0 1.42 1.42L17 5.91v12a1.56 1.56 0 0 1-1.53 1.59H13a1 1 0 0 0 0 2h2.47A3.56 3.56 0 0 0 19 17.91v-12l1.29 1.3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42l-3-3a1 1 0 0 0-1.42 0z\"/></g></g>","folder-add-outline":"<g data-name=\"Layer 2\"><g data-name=\"folder-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14 13h-1v-1a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2z\"/><path d=\"M19.5 7.05h-7L9.87 3.87a1 1 0 0 0-.77-.37H4.5A2.47 2.47 0 0 0 2 5.93v12.14a2.47 2.47 0 0 0 2.5 2.43h15a2.47 2.47 0 0 0 2.5-2.43V9.48a2.47 2.47 0 0 0-2.5-2.43zm.5 11a.46.46 0 0 1-.5.43h-15a.46.46 0 0 1-.5-.43V5.93a.46.46 0 0 1 .5-.43h4.13l2.6 3.18a1 1 0 0 0 .77.37h7.5a.46.46 0 0 1 .5.43z\"/></g></g>","folder-outline":"<g data-name=\"Layer 2\"><g data-name=\"folder\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.5 20.5h-15A2.47 2.47 0 0 1 2 18.07V5.93A2.47 2.47 0 0 1 4.5 3.5h4.6a1 1 0 0 1 .77.37l2.6 3.18h7A2.47 2.47 0 0 1 22 9.48v8.59a2.47 2.47 0 0 1-2.5 2.43zM4 13.76v4.31a.46.46 0 0 0 .5.43h15a.46.46 0 0 0 .5-.43V9.48a.46.46 0 0 0-.5-.43H12a1 1 0 0 1-.77-.37L8.63 5.5H4.5a.46.46 0 0 0-.5.43z\"/></g></g>","folder-remove-outline":"<g data-name=\"Layer 2\"><g data-name=\"folder-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M14 13h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/><path d=\"M19.5 7.05h-7L9.87 3.87a1 1 0 0 0-.77-.37H4.5A2.47 2.47 0 0 0 2 5.93v12.14a2.47 2.47 0 0 0 2.5 2.43h15a2.47 2.47 0 0 0 2.5-2.43V9.48a2.47 2.47 0 0 0-2.5-2.43zm.5 11a.46.46 0 0 1-.5.43h-15a.46.46 0 0 1-.5-.43V5.93a.46.46 0 0 1 .5-.43h4.13l2.6 3.18a1 1 0 0 0 .77.37h7.5a.46.46 0 0 1 .5.43z\"/></g></g>","funnel-outline":"<g data-name=\"Layer 2\"><g data-name=\"funnel\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.9 22a1 1 0 0 1-.6-.2l-4-3.05a1 1 0 0 1-.39-.8v-3.27l-4.8-9.22A1 1 0 0 1 5 4h14a1 1 0 0 1 .86.49 1 1 0 0 1 0 1l-5 9.21V21a1 1 0 0 1-.55.9 1 1 0 0 1-.41.1zm-3-4.54l2 1.53v-4.55A1 1 0 0 1 13 14l4.3-8H6.64l4.13 8a1 1 0 0 1 .11.46z\"/></g></g>","gift-outline":"<g data-name=\"Layer 2\"><g data-name=\"gift\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19.2 7h-.39A3 3 0 0 0 19 6a3.08 3.08 0 0 0-3.14-3A4.46 4.46 0 0 0 12 5.4 4.46 4.46 0 0 0 8.14 3 3.08 3.08 0 0 0 5 6a3 3 0 0 0 .19 1H4.8A2 2 0 0 0 3 9.2v3.6A2.08 2.08 0 0 0 4.5 15v4.37A1.75 1.75 0 0 0 6.31 21h11.38a1.75 1.75 0 0 0 1.81-1.67V15a2.08 2.08 0 0 0 1.5-2.2V9.2A2 2 0 0 0 19.2 7zM19 9.2v3.6a.56.56 0 0 1 0 .2h-6V9h6a.56.56 0 0 1 0 .2zM15.86 5A1.08 1.08 0 0 1 17 6a1.08 1.08 0 0 1-1.14 1H13.4a2.93 2.93 0 0 1 2.46-2zM7 6a1.08 1.08 0 0 1 1.14-1 2.93 2.93 0 0 1 2.45 2H8.14A1.08 1.08 0 0 1 7 6zM5 9.2A.56.56 0 0 1 5 9h6v4H5a.56.56 0 0 1 0-.2zM6.5 15H11v4H6.5zm6.5 4v-4h4.5v4z\"/></g></g>","github-outline":"<g data-name=\"Layer 2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16.24 22a1 1 0 0 1-1-1v-2.6a2.15 2.15 0 0 0-.54-1.66 1 1 0 0 1 .61-1.67C17.75 14.78 20 14 20 9.77a4 4 0 0 0-.67-2.22 2.75 2.75 0 0 1-.41-2.06 3.71 3.71 0 0 0 0-1.41 7.65 7.65 0 0 0-2.09 1.09 1 1 0 0 1-.84.15 10.15 10.15 0 0 0-5.52 0 1 1 0 0 1-.84-.15 7.4 7.4 0 0 0-2.11-1.09 3.52 3.52 0 0 0 0 1.41 2.84 2.84 0 0 1-.43 2.08 4.07 4.07 0 0 0-.67 2.23c0 3.89 1.88 4.93 4.7 5.29a1 1 0 0 1 .82.66 1 1 0 0 1-.21 1 2.06 2.06 0 0 0-.55 1.56V21a1 1 0 0 1-2 0v-.57a6 6 0 0 1-5.27-2.09 3.9 3.9 0 0 0-1.16-.88 1 1 0 1 1 .5-1.94 4.93 4.93 0 0 1 2 1.36c1 1 2 1.88 3.9 1.52a3.89 3.89 0 0 1 .23-1.58c-2.06-.52-5-2-5-7a6 6 0 0 1 1-3.33.85.85 0 0 0 .13-.62 5.69 5.69 0 0 1 .33-3.21 1 1 0 0 1 .63-.57c.34-.1 1.56-.3 3.87 1.2a12.16 12.16 0 0 1 5.69 0c2.31-1.5 3.53-1.31 3.86-1.2a1 1 0 0 1 .63.57 5.71 5.71 0 0 1 .33 3.22.75.75 0 0 0 .11.57 6 6 0 0 1 1 3.34c0 5.07-2.92 6.54-5 7a4.28 4.28 0 0 1 .22 1.67V21a1 1 0 0 1-.94 1z\"/></g>","globe-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"globe-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 2a8.19 8.19 0 0 1 1.79.21 2.61 2.61 0 0 1-.78 1c-.22.17-.46.31-.7.46a4.56 4.56 0 0 0-1.85 1.67 6.49 6.49 0 0 0-.62 3.3c0 1.36 0 2.16-.95 2.87-1.37 1.07-3.46.47-4.76-.07A8.33 8.33 0 0 1 4 12a8 8 0 0 1 8-8zM5 15.8a8.42 8.42 0 0 0 2 .27 5 5 0 0 0 3.14-1c1.71-1.34 1.71-3.06 1.71-4.44a4.76 4.76 0 0 1 .37-2.34 2.86 2.86 0 0 1 1.12-.91 9.75 9.75 0 0 0 .92-.61 4.55 4.55 0 0 0 1.4-1.87A8 8 0 0 1 19 8.12c-1.43.2-3.46.67-3.86 2.53A7 7 0 0 0 15 12a2.93 2.93 0 0 1-.29 1.47l-.1.17c-.65 1.08-1.38 2.31-.39 4 .12.21.25.41.38.61a2.29 2.29 0 0 1 .52 1.08A7.89 7.89 0 0 1 12 20a8 8 0 0 1-7-4.2zm11.93 2.52a6.79 6.79 0 0 0-.63-1.14c-.11-.16-.22-.32-.32-.49-.39-.68-.25-1 .38-2l.1-.17a4.77 4.77 0 0 0 .54-2.43 5.42 5.42 0 0 1 .09-1c.16-.73 1.71-.93 2.67-1a7.94 7.94 0 0 1-2.86 8.28z\"/></g></g>","globe-outline":"<g data-name=\"Layer 2\"><g data-name=\"globe\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M22 12A10 10 0 0 0 12 2a10 10 0 0 0 0 20 10 10 0 0 0 10-10zm-2.07-1H17a12.91 12.91 0 0 0-2.33-6.54A8 8 0 0 1 19.93 11zM9.08 13H15a11.44 11.44 0 0 1-3 6.61A11 11 0 0 1 9.08 13zm0-2A11.4 11.4 0 0 1 12 4.4a11.19 11.19 0 0 1 3 6.6zm.36-6.57A13.18 13.18 0 0 0 7.07 11h-3a8 8 0 0 1 5.37-6.57zM4.07 13h3a12.86 12.86 0 0 0 2.35 6.56A8 8 0 0 1 4.07 13zm10.55 6.55A13.14 13.14 0 0 0 17 13h2.95a8 8 0 0 1-5.33 6.55z\"/></g></g>","google-outline":"<g data-name=\"Layer 2\"><g data-name=\"google\"><polyline points=\"0 0 24 0 24 24 0 24\" opacity=\"0\"/><path d=\"M12 22h-.43A10.16 10.16 0 0 1 2 12.29a10 10 0 0 1 14.12-9.41 1.48 1.48 0 0 1 .77.86 1.47 1.47 0 0 1-.1 1.16L15.5 7.28a1.44 1.44 0 0 1-1.83.64A4.5 4.5 0 0 0 8.77 9a4.41 4.41 0 0 0-1.16 3.34 4.36 4.36 0 0 0 1.66 3 4.52 4.52 0 0 0 3.45 1 3.89 3.89 0 0 0 2.63-1.57h-2.9A1.45 1.45 0 0 1 11 13.33v-2.68a1.45 1.45 0 0 1 1.45-1.45h8.1A1.46 1.46 0 0 1 22 10.64v1.88A10 10 0 0 1 12 22zm0-18a8 8 0 0 0-8 8.24A8.12 8.12 0 0 0 11.65 20 8 8 0 0 0 20 12.42V11.2h-7v1.58h5.31l-.41 1.3a6 6 0 0 1-4.9 4.25A6.58 6.58 0 0 1 8 17a6.33 6.33 0 0 1-.72-9.3A6.52 6.52 0 0 1 14 5.91l.77-1.43A7.9 7.9 0 0 0 12 4z\"/></g></g>","grid-outline":"<g data-name=\"Layer 2\"><g data-name=\"grid\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9 3H5a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2zM5 9V5h4v4z\"/><path d=\"M19 3h-4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2zm-4 6V5h4v4z\"/><path d=\"M9 13H5a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-4a2 2 0 0 0-2-2zm-4 6v-4h4v4z\"/><path d=\"M19 13h-4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-4a2 2 0 0 0-2-2zm-4 6v-4h4v4z\"/></g></g>","hard-drive-outline":"<g data-name=\"Layer 2\"><g data-name=\"hard-drive\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.79 11.34l-3.34-6.68A3 3 0 0 0 14.76 3H9.24a3 3 0 0 0-2.69 1.66l-3.34 6.68a2 2 0 0 0-.21.9V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-5.76a2 2 0 0 0-.21-.9zM8.34 5.55a1 1 0 0 1 .9-.55h5.52a1 1 0 0 1 .9.55L18.38 11H5.62zM18 19H6a1 1 0 0 1-1-1v-5h14v5a1 1 0 0 1-1 1z\"/><path d=\"M16 15h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/><circle cx=\"8\" cy=\"16\" r=\"1\"/></g></g>","hash-outline":"<g data-name=\"Layer 2\"><g data-name=\"hash\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20 14h-4.3l.73-4H20a1 1 0 0 0 0-2h-3.21l.69-3.81A1 1 0 0 0 16.64 3a1 1 0 0 0-1.22.82L14.67 8h-3.88l.69-3.81A1 1 0 0 0 10.64 3a1 1 0 0 0-1.22.82L8.67 8H4a1 1 0 0 0 0 2h4.3l-.73 4H4a1 1 0 0 0 0 2h3.21l-.69 3.81A1 1 0 0 0 7.36 21a1 1 0 0 0 1.22-.82L9.33 16h3.88l-.69 3.81a1 1 0 0 0 .84 1.19 1 1 0 0 0 1.22-.82l.75-4.18H20a1 1 0 0 0 0-2zM9.7 14l.73-4h3.87l-.73 4z\"/></g></g>","headphones-outline":"<g data-name=\"Layer 2\"><g data-name=\"headphones\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2A10.2 10.2 0 0 0 2 12.37V17a4 4 0 1 0 4-4 3.91 3.91 0 0 0-2 .56v-1.19A8.2 8.2 0 0 1 12 4a8.2 8.2 0 0 1 8 8.37v1.19a3.91 3.91 0 0 0-2-.56 4 4 0 1 0 4 4v-4.63A10.2 10.2 0 0 0 12 2zM6 15a2 2 0 1 1-2 2 2 2 0 0 1 2-2zm12 4a2 2 0 1 1 2-2 2 2 0 0 1-2 2z\"/></g></g>","heart-outline":"<g data-name=\"Layer 2\"><g data-name=\"heart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 21a1 1 0 0 1-.71-.29l-7.77-7.78a5.26 5.26 0 0 1 0-7.4 5.24 5.24 0 0 1 7.4 0L12 6.61l1.08-1.08a5.24 5.24 0 0 1 7.4 0 5.26 5.26 0 0 1 0 7.4l-7.77 7.78A1 1 0 0 1 12 21zM7.22 6a3.2 3.2 0 0 0-2.28.94 3.24 3.24 0 0 0 0 4.57L12 18.58l7.06-7.07a3.24 3.24 0 0 0 0-4.57 3.32 3.32 0 0 0-4.56 0l-1.79 1.8a1 1 0 0 1-1.42 0L9.5 6.94A3.2 3.2 0 0 0 7.22 6z\"/></g></g>","home-outline":"<g data-name=\"Layer 2\"><g data-name=\"home\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.42 10.18L12.71 2.3a1 1 0 0 0-1.42 0l-7.71 7.89A2 2 0 0 0 3 11.62V20a2 2 0 0 0 1.89 2h14.22A2 2 0 0 0 21 20v-8.38a2.07 2.07 0 0 0-.58-1.44zM10 20v-6h4v6zm9 0h-3v-7a1 1 0 0 0-1-1H9a1 1 0 0 0-1 1v7H5v-8.42l7-7.15 7 7.19z\"/></g></g>","image-outline":"<g data-name=\"Layer 2\"><g data-name=\"image\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zM6 5h12a1 1 0 0 1 1 1v8.36l-3.2-2.73a2.77 2.77 0 0 0-3.52 0L5 17.7V6a1 1 0 0 1 1-1zm12 14H6.56l7-5.84a.78.78 0 0 1 .93 0L19 17v1a1 1 0 0 1-1 1z\"/><circle cx=\"8\" cy=\"8.5\" r=\"1.5\"/></g></g>","inbox-outline":"<g data-name=\"Layer 2\"><g data-name=\"inbox\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.79 11.34l-3.34-6.68A3 3 0 0 0 14.76 3H9.24a3 3 0 0 0-2.69 1.66l-3.34 6.68a2 2 0 0 0-.21.9V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-5.76a2 2 0 0 0-.21-.9zM8.34 5.55a1 1 0 0 1 .9-.55h5.52a1 1 0 0 1 .9.55L18.38 11H16a1 1 0 0 0-1 1v3H9v-3a1 1 0 0 0-1-1H5.62zM18 19H6a1 1 0 0 1-1-1v-5h2v3a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-3h2v5a1 1 0 0 1-1 1z\"/></g></g>","info-outline":"<g data-name=\"Layer 2\"><g data-name=\"info\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><circle cx=\"12\" cy=\"8\" r=\"1\"/><path d=\"M12 10a1 1 0 0 0-1 1v5a1 1 0 0 0 2 0v-5a1 1 0 0 0-1-1z\"/></g></g>","keypad-outline":"<g data-name=\"Layer 2\"><g data-name=\"keypad\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M5 2a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M12 2a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M19 8a3 3 0 1 0-3-3 3 3 0 0 0 3 3zm0-4a1 1 0 1 1-1 1 1 1 0 0 1 1-1z\"/><path d=\"M5 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M12 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M19 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M5 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M12 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M19 16a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","layers-outline":"<g data-name=\"Layer 2\"><g data-name=\"layers\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M21 11.35a1 1 0 0 0-.61-.86l-2.15-.92 2.26-1.3a1 1 0 0 0 .5-.92 1 1 0 0 0-.61-.86l-8-3.41a1 1 0 0 0-.78 0l-8 3.41a1 1 0 0 0-.61.86 1 1 0 0 0 .5.92l2.26 1.3-2.15.92a1 1 0 0 0-.61.86 1 1 0 0 0 .5.92l2.26 1.3-2.15.92a1 1 0 0 0-.61.86 1 1 0 0 0 .5.92l8 4.6a1 1 0 0 0 1 0l8-4.6a1 1 0 0 0 .5-.92 1 1 0 0 0-.61-.86l-2.15-.92 2.26-1.3a1 1 0 0 0 .5-.92zm-9-6.26l5.76 2.45L12 10.85 6.24 7.54zm-.5 7.78a1 1 0 0 0 1 0l3.57-2 1.69.72L12 14.85l-5.76-3.31 1.69-.72zm6.26 2.67L12 18.85l-5.76-3.31 1.69-.72 3.57 2.05a1 1 0 0 0 1 0l3.57-2.05z\"/></g></g>","layout-outline":"<g data-name=\"Layer 2\"><g data-name=\"layout\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zM6 5h12a1 1 0 0 1 1 1v2H5V6a1 1 0 0 1 1-1zM5 18v-8h6v9H6a1 1 0 0 1-1-1zm13 1h-5v-9h6v8a1 1 0 0 1-1 1z\"/></g></g>","link-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"link-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.29 9.29l-4 4a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l4-4a1 1 0 0 0-1.42-1.42z\"/><path d=\"M12.28 17.4L11 18.67a4.2 4.2 0 0 1-5.58.4 4 4 0 0 1-.27-5.93l1.42-1.43a1 1 0 0 0 0-1.42 1 1 0 0 0-1.42 0l-1.27 1.28a6.15 6.15 0 0 0-.67 8.07 6.06 6.06 0 0 0 9.07.6l1.42-1.42a1 1 0 0 0-1.42-1.42z\"/><path d=\"M19.66 3.22a6.18 6.18 0 0 0-8.13.68L10.45 5a1.09 1.09 0 0 0-.17 1.61 1 1 0 0 0 1.42 0L13 5.3a4.17 4.17 0 0 1 5.57-.4 4 4 0 0 1 .27 5.95l-1.42 1.43a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l1.42-1.42a6.06 6.06 0 0 0-.6-9.06z\"/></g></g>","link-outline":"<g data-name=\"Layer 2\"><g data-name=\"link\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8 12a1 1 0 0 0 1 1h6a1 1 0 0 0 0-2H9a1 1 0 0 0-1 1z\"/><path d=\"M9 16H7.21A4.13 4.13 0 0 1 3 12.37 4 4 0 0 1 7 8h2a1 1 0 0 0 0-2H7.21a6.15 6.15 0 0 0-6.16 5.21A6 6 0 0 0 7 18h2a1 1 0 0 0 0-2z\"/><path d=\"M23 11.24A6.16 6.16 0 0 0 16.76 6h-1.51C14.44 6 14 6.45 14 7a1 1 0 0 0 1 1h1.79A4.13 4.13 0 0 1 21 11.63 4 4 0 0 1 17 16h-2a1 1 0 0 0 0 2h2a6 6 0 0 0 6-6.76z\"/></g></g>","linkedin-outline":"<g data-name=\"Layer 2\"><g data-name=\"linkedin\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20 22h-1.67a2 2 0 0 1-2-2v-5.37a.92.92 0 0 0-.69-.93.84.84 0 0 0-.67.19.85.85 0 0 0-.3.65V20a2 2 0 0 1-2 2H11a2 2 0 0 1-2-2v-5.46a6.5 6.5 0 1 1 13 0V20a2 2 0 0 1-2 2zm-4.5-10.31a3.73 3.73 0 0 1 .47 0 2.91 2.91 0 0 1 2.36 2.9V20H20v-5.46a4.5 4.5 0 1 0-9 0V20h1.67v-5.46a2.85 2.85 0 0 1 2.83-2.85z\"/><path d=\"M6 22H4a2 2 0 0 1-2-2V10a2 2 0 0 1 2-2h2a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2zM4 10v10h2V10z\"/><path d=\"M5 7a3 3 0 1 1 3-3 3 3 0 0 1-3 3zm0-4a1 1 0 1 0 1 1 1 1 0 0 0-1-1z\"/></g></g>","list-outline":"<g data-name=\"Layer 2\"><g data-name=\"list\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><circle cx=\"4\" cy=\"7\" r=\"1\"/><circle cx=\"4\" cy=\"12\" r=\"1\"/><circle cx=\"4\" cy=\"17\" r=\"1\"/><rect x=\"7\" y=\"11\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"7\" y=\"16\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"7\" y=\"6\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/></g></g>","loader-outline":"<g data-name=\"Layer 2\"><g data-name=\"loader\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a1 1 0 0 0-1 1v2a1 1 0 0 0 2 0V3a1 1 0 0 0-1-1z\"/><path d=\"M21 11h-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M6 12a1 1 0 0 0-1-1H3a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1z\"/><path d=\"M6.22 5a1 1 0 0 0-1.39 1.47l1.44 1.39a1 1 0 0 0 .73.28 1 1 0 0 0 .72-.31 1 1 0 0 0 0-1.41z\"/><path d=\"M17 8.14a1 1 0 0 0 .69-.28l1.44-1.39A1 1 0 0 0 17.78 5l-1.44 1.42a1 1 0 0 0 0 1.41 1 1 0 0 0 .66.31z\"/><path d=\"M12 18a1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-2a1 1 0 0 0-1-1z\"/><path d=\"M17.73 16.14a1 1 0 0 0-1.39 1.44L17.78 19a1 1 0 0 0 .69.28 1 1 0 0 0 .72-.3 1 1 0 0 0 0-1.42z\"/><path d=\"M6.27 16.14l-1.44 1.39a1 1 0 0 0 0 1.42 1 1 0 0 0 .72.3 1 1 0 0 0 .67-.25l1.44-1.39a1 1 0 0 0-1.39-1.44z\"/></g></g>","lock-outline":"<g data-name=\"Layer 2\"><g data-name=\"lock\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17 8h-1V6.11a4 4 0 1 0-8 0V8H7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm-7-1.89A2.06 2.06 0 0 1 12 4a2.06 2.06 0 0 1 2 2.11V8h-4zM18 19a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1v-8a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1z\"/><path d=\"M12 12a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","log-in-outline":"<g data-name=\"Layer 2\"><g data-name=\"log-in\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M19 4h-2a1 1 0 0 0 0 2h1v12h-1a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1z\"/><path d=\"M11.8 7.4a1 1 0 0 0-1.6 1.2L12 11H4a1 1 0 0 0 0 2h8.09l-1.72 2.44a1 1 0 0 0 .24 1.4 1 1 0 0 0 .58.18 1 1 0 0 0 .81-.42l2.82-4a1 1 0 0 0 0-1.18z\"/></g></g>","log-out-outline":"<g data-name=\"Layer 2\"><g data-name=\"log-out\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M7 6a1 1 0 0 0 0-2H5a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h2a1 1 0 0 0 0-2H6V6z\"/><path d=\"M20.82 11.42l-2.82-4a1 1 0 0 0-1.39-.24 1 1 0 0 0-.24 1.4L18.09 11H10a1 1 0 0 0 0 2h8l-1.8 2.4a1 1 0 0 0 .2 1.4 1 1 0 0 0 .6.2 1 1 0 0 0 .8-.4l3-4a1 1 0 0 0 .02-1.18z\"/></g></g>","map-outline":"<g data-name=\"Layer 2\"><g data-name=\"map\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.41 5.89l-4-1.8H15.59L12 5.7 8.41 4.09h-.05L8.24 4h-.6l-4 1.8a1 1 0 0 0-.64 1V19a1 1 0 0 0 .46.84A1 1 0 0 0 4 20a1 1 0 0 0 .41-.09L8 18.3l3.59 1.61h.05a.85.85 0 0 0 .72 0h.05L16 18.3l3.59 1.61A1 1 0 0 0 20 20a1 1 0 0 0 .54-.16A1 1 0 0 0 21 19V6.8a1 1 0 0 0-.59-.91zM5 7.44l2-.89v10l-2 .89zm4-.89l2 .89v10l-2-.89zm4 .89l2-.89v10l-2 .89zm6 10l-2-.89v-10l2 .89z\"/></g></g>","maximize-outline":"<g data-name=\"Layer 2\"><g data-name=\"maximize\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM5 11a6 6 0 1 1 6 6 6 6 0 0 1-6-6z\"/><path d=\"M13 10h-1V9a1 1 0 0 0-2 0v1H9a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0v-1h1a1 1 0 0 0 0-2z\"/></g></g>","menu-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"menu-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><circle cx=\"4\" cy=\"12\" r=\"1\"/><rect x=\"7\" y=\"11\" width=\"14\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"3\" y=\"16\" width=\"18\" height=\"2\" rx=\".94\" ry=\".94\"/><rect x=\"3\" y=\"6\" width=\"18\" height=\"2\" rx=\".94\" ry=\".94\"/></g></g>","menu-arrow-outline":"<g data-name=\"Layer 2\"><g data-name=\"menu-arrow\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M20.05 11H5.91l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3a1 1 0 0 0 0 1.42l3 3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L5.91 13h14.14a1 1 0 0 0 .95-.95V12a1 1 0 0 0-.95-1z\"/><rect x=\"3\" y=\"17\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"5\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/></g></g>","menu-outline":"<g data-name=\"Layer 2\"><g data-name=\"menu\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><rect x=\"3\" y=\"11\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"16\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/><rect x=\"3\" y=\"6\" width=\"18\" height=\"2\" rx=\".95\" ry=\".95\"/></g></g>","message-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"message-circle\"><circle cx=\"12\" cy=\"12\" r=\"1\"/><circle cx=\"16\" cy=\"12\" r=\"1\"/><circle cx=\"8\" cy=\"12\" r=\"1\"/><path d=\"M19.07 4.93a10 10 0 0 0-16.28 11 1.06 1.06 0 0 1 .09.64L2 20.8a1 1 0 0 0 .27.91A1 1 0 0 0 3 22h.2l4.28-.86a1.26 1.26 0 0 1 .64.09 10 10 0 0 0 11-16.28zm.83 8.36a8 8 0 0 1-11 6.08 3.26 3.26 0 0 0-1.25-.26 3.43 3.43 0 0 0-.56.05l-2.82.57.57-2.82a3.09 3.09 0 0 0-.21-1.81 8 8 0 0 1 6.08-11 8 8 0 0 1 9.19 9.19z\"/><rect width=\"24\" height=\"24\" opacity=\"0\"/></g></g>","message-square-outline":"<g data-name=\"Layer 2\"><g data-name=\"message-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"11\" r=\"1\"/><circle cx=\"16\" cy=\"11\" r=\"1\"/><circle cx=\"8\" cy=\"11\" r=\"1\"/><path d=\"M19 3H5a3 3 0 0 0-3 3v15a1 1 0 0 0 .51.87A1 1 0 0 0 3 22a1 1 0 0 0 .51-.14L8 19.14a1 1 0 0 1 .55-.14H19a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 13a1 1 0 0 1-1 1H8.55a3 3 0 0 0-1.55.43l-3 1.8V6a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1z\"/></g></g>","mic-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"mic-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M10 6a2 2 0 0 1 4 0v5a1 1 0 0 1 0 .16l1.6 1.59A4 4 0 0 0 16 11V6a4 4 0 0 0-7.92-.75L10 7.17z\"/><path d=\"M19 11a1 1 0 0 0-2 0 4.86 4.86 0 0 1-.69 2.48L17.78 15A7 7 0 0 0 19 11z\"/><path d=\"M12 15h.16L8 10.83V11a4 4 0 0 0 4 4z\"/><path d=\"M20.71 19.29l-16-16a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M15 20h-2v-2.08a7 7 0 0 0 1.65-.44l-1.6-1.6A4.57 4.57 0 0 1 12 16a5 5 0 0 1-5-5 1 1 0 0 0-2 0 7 7 0 0 0 6 6.92V20H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2z\"/></g></g>","mic-outline":"<g data-name=\"Layer 2\"><g data-name=\"mic\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 15a4 4 0 0 0 4-4V6a4 4 0 0 0-8 0v5a4 4 0 0 0 4 4zm-2-9a2 2 0 0 1 4 0v5a2 2 0 0 1-4 0z\"/><path d=\"M19 11a1 1 0 0 0-2 0 5 5 0 0 1-10 0 1 1 0 0 0-2 0 7 7 0 0 0 6 6.92V20H8.89a.89.89 0 0 0-.89.89v.22a.89.89 0 0 0 .89.89h6.22a.89.89 0 0 0 .89-.89v-.22a.89.89 0 0 0-.89-.89H13v-2.08A7 7 0 0 0 19 11z\"/></g></g>","minimize-outline":"<g data-name=\"Layer 2\"><g data-name=\"minimize\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM5 11a6 6 0 1 1 6 6 6 6 0 0 1-6-6z\"/><path d=\"M13 10H9a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/></g></g>","minus-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"minus-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M15 11H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2z\"/></g></g>","minus-outline":"<g data-name=\"Layer 2\"><g data-name=\"minus\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 13H5a1 1 0 0 1 0-2h14a1 1 0 0 1 0 2z\"/></g></g>","minus-square-outline":"<g data-name=\"Layer 2\"><g data-name=\"minus-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/><path d=\"M15 11H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2z\"/></g></g>","monitor-outline":"<g data-name=\"Layer 2\"><g data-name=\"monitor\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 3H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h6v2H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2h-4v-2h6a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 11a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1z\"/></g></g>","moon-outline":"<g data-name=\"Layer 2\"><g data-name=\"moon\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12.3 22h-.1a10.31 10.31 0 0 1-7.34-3.15 10.46 10.46 0 0 1-.26-14 10.13 10.13 0 0 1 4-2.74 1 1 0 0 1 1.06.22 1 1 0 0 1 .24 1 8.4 8.4 0 0 0 1.94 8.81 8.47 8.47 0 0 0 8.83 1.94 1 1 0 0 1 1.27 1.29A10.16 10.16 0 0 1 19.6 19a10.28 10.28 0 0 1-7.3 3zM7.46 4.92a7.93 7.93 0 0 0-1.37 1.22 8.44 8.44 0 0 0 .2 11.32A8.29 8.29 0 0 0 12.22 20h.08a8.34 8.34 0 0 0 6.78-3.49A10.37 10.37 0 0 1 7.46 4.92z\"/></g></g>","more-horizontal-outline":"<g data-name=\"Layer 2\"><g data-name=\"more-horizotnal\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"2\"/><circle cx=\"19\" cy=\"12\" r=\"2\"/><circle cx=\"5\" cy=\"12\" r=\"2\"/></g></g>","more-vertical-outline":"<g data-name=\"Layer 2\"><g data-name=\"more-vertical\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><circle cx=\"12\" cy=\"12\" r=\"2\"/><circle cx=\"12\" cy=\"5\" r=\"2\"/><circle cx=\"12\" cy=\"19\" r=\"2\"/></g></g>","move-outline":"<g data-name=\"Layer 2\"><g data-name=\"move\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M21.71 11.31l-3-3a1 1 0 0 0-1.42 1.42L18.58 11H13V5.41l1.29 1.3A1 1 0 0 0 15 7a1 1 0 0 0 .71-.29 1 1 0 0 0 0-1.42l-3-3A1 1 0 0 0 12 2a1 1 0 0 0-.7.29l-3 3a1 1 0 0 0 1.41 1.42L11 5.42V11H5.41l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3A1 1 0 0 0 2 12a1 1 0 0 0 .29.71l3 3A1 1 0 0 0 6 16a1 1 0 0 0 .71-.29 1 1 0 0 0 0-1.42L5.42 13H11v5.59l-1.29-1.3a1 1 0 0 0-1.42 1.42l3 3A1 1 0 0 0 12 22a1 1 0 0 0 .7-.29l3-3a1 1 0 0 0-1.42-1.42L13 18.58V13h5.59l-1.3 1.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 16a1 1 0 0 0 .71-.29l3-3A1 1 0 0 0 22 12a1 1 0 0 0-.29-.69z\"/></g></g>","music-outline":"<g data-name=\"Layer 2\"><g data-name=\"music\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19 15V4a1 1 0 0 0-.38-.78 1 1 0 0 0-.84-.2l-9 2A1 1 0 0 0 8 6v8.34a3.49 3.49 0 1 0 2 3.18 4.36 4.36 0 0 0 0-.52V6.8l7-1.55v7.09a3.49 3.49 0 1 0 2 3.17 4.57 4.57 0 0 0 0-.51zM6.54 19A1.49 1.49 0 1 1 8 17.21a1.53 1.53 0 0 1 0 .3A1.49 1.49 0 0 1 6.54 19zm9-2A1.5 1.5 0 1 1 17 15.21a1.53 1.53 0 0 1 0 .3A1.5 1.5 0 0 1 15.51 17z\"/></g></g>","navigation-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"navigation-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13.67 22h-.06a1 1 0 0 1-.92-.8L11 13l-8.2-1.69a1 1 0 0 1-.12-1.93l16-5.33A1 1 0 0 1 20 5.32l-5.33 16a1 1 0 0 1-1 .68zm-6.8-11.9l5.19 1.06a1 1 0 0 1 .79.78l1.05 5.19 3.52-10.55z\"/></g></g>","navigation-outline":"<g data-name=\"Layer 2\"><g data-name=\"navigation\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 20a.94.94 0 0 1-.55-.17L12 14.9l-7.45 4.93a1 1 0 0 1-1.44-1.28l8-16a1 1 0 0 1 1.78 0l8 16a1 1 0 0 1-.23 1.2A1 1 0 0 1 20 20zm-8-7.3a1 1 0 0 1 .55.17l4.88 3.23L12 5.24 6.57 16.1l4.88-3.23a1 1 0 0 1 .55-.17z\"/></g></g>","npm-outline":"<g data-name=\"Layer 2\"><g data-name=\"npm\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 21H6a3 3 0 0 1-3-3V6a3 3 0 0 1 3-3h12a3 3 0 0 1 3 3v12a3 3 0 0 1-3 3zM6 5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V6a1 1 0 0 0-1-1z\"/><rect x=\"12\" y=\"9\" width=\"4\" height=\"10\"/></g></g>","options-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"options-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M19 9a3 3 0 0 0-2.82 2H3a1 1 0 0 0 0 2h13.18A3 3 0 1 0 19 9zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M3 7h1.18a3 3 0 0 0 5.64 0H21a1 1 0 0 0 0-2H9.82a3 3 0 0 0-5.64 0H3a1 1 0 0 0 0 2zm4-2a1 1 0 1 1-1 1 1 1 0 0 1 1-1z\"/><path d=\"M21 17h-7.18a3 3 0 0 0-5.64 0H3a1 1 0 0 0 0 2h5.18a3 3 0 0 0 5.64 0H21a1 1 0 0 0 0-2zm-10 2a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","options-outline":"<g data-name=\"Layer 2\"><g data-name=\"options\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M7 14.18V3a1 1 0 0 0-2 0v11.18a3 3 0 0 0 0 5.64V21a1 1 0 0 0 2 0v-1.18a3 3 0 0 0 0-5.64zM6 18a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M21 13a3 3 0 0 0-2-2.82V3a1 1 0 0 0-2 0v7.18a3 3 0 0 0 0 5.64V21a1 1 0 0 0 2 0v-5.18A3 3 0 0 0 21 13zm-3 1a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M15 5a3 3 0 1 0-4 2.82V21a1 1 0 0 0 2 0V7.82A3 3 0 0 0 15 5zm-3 1a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","pantone-outline":"<g data-name=\"Layer 2\"><g data-name=\"pantone\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 13.18h-4.06l2.3-2.47a1 1 0 0 0 0-1.41l-4.19-3.86a.93.93 0 0 0-.71-.26 1 1 0 0 0-.7.31l-1.82 2V4a1 1 0 0 0-1-1H4a1 1 0 0 0-1 1v13.09A3.91 3.91 0 0 0 6.91 21H20a1 1 0 0 0 1-1v-5.82a1 1 0 0 0-1-1zm-6.58-5.59l2.67 2.49-5.27 5.66v-5.36zM8.82 10v3H5v-3zm0-5v3H5V5zM5 17.09V15h3.82v2.09a1.91 1.91 0 0 1-3.82 0zM19 19h-8.49l3.56-3.82H19z\"/></g></g>","paper-plane-outline":"<g data-name=\"Layer 2\"><g data-name=\"paper-plane\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 4a1.31 1.31 0 0 0-.06-.27v-.09a1 1 0 0 0-.2-.3 1 1 0 0 0-.29-.19h-.09a.86.86 0 0 0-.31-.15H20a1 1 0 0 0-.3 0l-18 6a1 1 0 0 0 0 1.9l8.53 2.84 2.84 8.53a1 1 0 0 0 1.9 0l6-18A1 1 0 0 0 21 4zm-4.7 2.29l-5.57 5.57L5.16 10zM14 18.84l-1.86-5.57 5.57-5.57z\"/></g></g>","pause-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"pause-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M15 8a1 1 0 0 0-1 1v6a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/><path d=\"M9 8a1 1 0 0 0-1 1v6a1 1 0 0 0 2 0V9a1 1 0 0 0-1-1z\"/></g></g>","people-outline":"<g data-name=\"Layer 2\"><g data-name=\"people\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M9 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M17 13a3 3 0 1 0-3-3 3 3 0 0 0 3 3zm0-4a1 1 0 1 1-1 1 1 1 0 0 1 1-1z\"/><path d=\"M17 14a5 5 0 0 0-3.06 1.05A7 7 0 0 0 2 20a1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 6.9 6.9 0 0 0-.86-3.35A3 3 0 0 1 20 19a1 1 0 0 0 2 0 5 5 0 0 0-5-5z\"/></g></g>","percent-outline":"<g data-name=\"Layer 2\"><g data-name=\"percent\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8 11a3.5 3.5 0 1 0-3.5-3.5A3.5 3.5 0 0 0 8 11zm0-5a1.5 1.5 0 1 1-1.5 1.5A1.5 1.5 0 0 1 8 6z\"/><path d=\"M16 14a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 16 14zm0 5a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 16 19z\"/><path d=\"M19.74 4.26a.89.89 0 0 0-1.26 0L4.26 18.48a.91.91 0 0 0-.26.63.89.89 0 0 0 1.52.63L19.74 5.52a.89.89 0 0 0 0-1.26z\"/></g></g>","person-add-outline":"<g data-name=\"Layer 2\"><g data-name=\"person-add\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-1V5a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0V8h1a1 1 0 0 0 0-2z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M10 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z\"/></g></g>","person-delete-outline":"<g data-name=\"Layer 2\"><g data-name=\"person-delete\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.47 7.5l.73-.73a1 1 0 0 0-1.47-1.47L19 6l-.73-.73a1 1 0 0 0-1.47 1.5l.73.73-.73.73a1 1 0 0 0 1.47 1.47L19 9l.73.73a1 1 0 0 0 1.47-1.5z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M10 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z\"/></g></g>","person-done-outline":"<g data-name=\"Layer 2\"><g data-name=\"person-done\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.66 4.25a1 1 0 0 0-1.41.09l-1.87 2.15-.63-.71a1 1 0 0 0-1.5 1.33l1.39 1.56a1 1 0 0 0 .75.33 1 1 0 0 0 .74-.34l2.61-3a1 1 0 0 0-.08-1.41z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M10 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z\"/></g></g>","person-outline":"<g data-name=\"Layer 2\"><g data-name=\"person\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M12 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z\"/></g></g>","person-remove-outline":"<g data-name=\"Layer 2\"><g data-name=\"person-remove\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-4a1 1 0 0 0 0 2h4a1 1 0 0 0 0-2z\"/><path d=\"M10 11a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0-6a2 2 0 1 1-2 2 2 2 0 0 1 2-2z\"/><path d=\"M10 13a7 7 0 0 0-7 7 1 1 0 0 0 2 0 5 5 0 0 1 10 0 1 1 0 0 0 2 0 7 7 0 0 0-7-7z\"/></g></g>","phone-call-outline":"<g data-name=\"Layer 2\"><g data-name=\"phone-call\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13 8a3 3 0 0 1 3 3 1 1 0 0 0 2 0 5 5 0 0 0-5-5 1 1 0 0 0 0 2z\"/><path d=\"M13 4a7 7 0 0 1 7 7 1 1 0 0 0 2 0 9 9 0 0 0-9-9 1 1 0 0 0 0 2z\"/><path d=\"M21.75 15.91a1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a9.91 9.91 0 0 1-4.87-4.89C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6 15.42 15.42 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76 4.34 4.34 0 0 0-.19-.73zM17.4 20A13.41 13.41 0 0 1 4 6.6 2.61 2.61 0 0 1 6.6 4h.33L8 8.64l-.54.28c-.86.45-1.54.81-1.18 1.59a11.85 11.85 0 0 0 7.18 7.21c.84.34 1.17-.29 1.62-1.16l.29-.55L20 17.07v.33a2.61 2.61 0 0 1-2.6 2.6z\"/></g></g>","phone-missed-outline":"<g data-name=\"Layer 2\"><g data-name=\"phone-missed\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.94 16.64a4.34 4.34 0 0 0-.19-.73 1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a10 10 0 0 1-4.88-4.89C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6 15.42 15.42 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76zM17.4 20A13.41 13.41 0 0 1 4 6.6 2.61 2.61 0 0 1 6.6 4h.33L8 8.64l-.55.29c-.87.45-1.5.78-1.17 1.58a11.85 11.85 0 0 0 7.18 7.21c.84.34 1.17-.29 1.62-1.16l.29-.55L20 17.07v.33a2.61 2.61 0 0 1-2.6 2.6z\"/><path d=\"M15.8 8.7a1.05 1.05 0 0 0 1.47 0L18 8l.73.73a1 1 0 0 0 1.47-1.5l-.73-.73.73-.73a1 1 0 0 0-1.47-1.47L18 5l-.73-.73a1 1 0 0 0-1.47 1.5l.73.73-.73.73a1.05 1.05 0 0 0 0 1.47z\"/></g></g>","phone-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"phone-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.74 4.26a.89.89 0 0 0-1.26 0L4.26 18.48a.91.91 0 0 0-.26.63.89.89 0 0 0 1.52.63L19.74 5.52a.89.89 0 0 0 0-1.26z\"/><path d=\"M6.7 14.63A13.29 13.29 0 0 1 4 6.6 2.61 2.61 0 0 1 6.6 4h.33L8 8.64l-.55.29c-.87.45-1.5.78-1.17 1.58a11.57 11.57 0 0 0 1.57 3l1.43-1.42a10.37 10.37 0 0 1-.8-1.42C9.71 10 9.72 10 9.85 9.85a1 1 0 0 0 .26-.92L8.74 3a1 1 0 0 0-.65-.72 3.79 3.79 0 0 0-.72-.18A3.94 3.94 0 0 0 6.6 2 4.6 4.6 0 0 0 2 6.6a15.33 15.33 0 0 0 3.27 9.46z\"/><path d=\"M21.94 16.64a4.34 4.34 0 0 0-.19-.73 1 1 0 0 0-.72-.65l-6-1.37a1 1 0 0 0-.92.26c-.14.13-.15.14-.8 1.38a10.88 10.88 0 0 1-1.41-.8l-1.43 1.43a11.52 11.52 0 0 0 2.94 1.56c.84.34 1.17-.29 1.62-1.16l.29-.55L20 17.07v.33a2.61 2.61 0 0 1-2.6 2.6 13.29 13.29 0 0 1-8-2.7l-1.46 1.43A15.33 15.33 0 0 0 17.4 22a4.6 4.6 0 0 0 4.6-4.6 4.77 4.77 0 0 0-.06-.76z\"/></g></g>","phone-outline":"<g data-name=\"Layer 2\"><g data-name=\"phone\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.4 22A15.42 15.42 0 0 1 2 6.6 4.6 4.6 0 0 1 6.6 2a3.94 3.94 0 0 1 .77.07 3.79 3.79 0 0 1 .72.18 1 1 0 0 1 .65.75l1.37 6a1 1 0 0 1-.26.92c-.13.14-.14.15-1.37.79a9.91 9.91 0 0 0 4.87 4.89c.65-1.24.66-1.25.8-1.38a1 1 0 0 1 .92-.26l6 1.37a1 1 0 0 1 .72.65 4.34 4.34 0 0 1 .19.73 4.77 4.77 0 0 1 .06.76A4.6 4.6 0 0 1 17.4 22zM6.6 4A2.61 2.61 0 0 0 4 6.6 13.41 13.41 0 0 0 17.4 20a2.61 2.61 0 0 0 2.6-2.6v-.33L15.36 16l-.29.55c-.45.87-.78 1.5-1.62 1.16a11.85 11.85 0 0 1-7.18-7.21c-.36-.78.32-1.14 1.18-1.59L8 8.64 6.93 4z\"/></g></g>","pie-chart-outline":"<g data-name=\"Layer 2\"><g data-name=\"pie-chart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M13 2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1 9 9 0 0 0-9-9zm1 8V4.07A7 7 0 0 1 19.93 10z\"/><path d=\"M20.82 14.06a1 1 0 0 0-1.28.61A8 8 0 1 1 9.33 4.46a1 1 0 0 0-.66-1.89 10 10 0 1 0 12.76 12.76 1 1 0 0 0-.61-1.27z\"/></g></g>","pin-outline":"<g data-name=\"Layer 2\"><g data-name=\"pin\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a8 8 0 0 0-8 7.92c0 5.48 7.05 11.58 7.35 11.84a1 1 0 0 0 1.3 0C13 21.5 20 15.4 20 9.92A8 8 0 0 0 12 2zm0 17.65c-1.67-1.59-6-6-6-9.73a6 6 0 0 1 12 0c0 3.7-4.33 8.14-6 9.73z\"/><path d=\"M12 6a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 12 6zm0 5a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 12 11z\"/></g></g>","play-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"play-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M12.34 7.45a1.7 1.7 0 0 0-1.85-.3 1.6 1.6 0 0 0-1 1.48v6.74a1.6 1.6 0 0 0 1 1.48 1.68 1.68 0 0 0 .69.15 1.74 1.74 0 0 0 1.16-.45L16 13.18a1.6 1.6 0 0 0 0-2.36zm-.84 7.15V9.4l2.81 2.6z\"/></g></g>","plus-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"plus-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M15 11h-2V9a1 1 0 0 0-2 0v2H9a1 1 0 0 0 0 2h2v2a1 1 0 0 0 2 0v-2h2a1 1 0 0 0 0-2z\"/></g></g>","plus-outline":"<g data-name=\"Layer 2\"><g data-name=\"plus\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M19 11h-6V5a1 1 0 0 0-2 0v6H5a1 1 0 0 0 0 2h6v6a1 1 0 0 0 2 0v-6h6a1 1 0 0 0 0-2z\"/></g></g>","plus-square-outline":"<g data-name=\"Layer 2\"><g data-name=\"plus-square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V6a3 3 0 0 0-3-3zm1 15a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/><path d=\"M15 11h-2V9a1 1 0 0 0-2 0v2H9a1 1 0 0 0 0 2h2v2a1 1 0 0 0 2 0v-2h2a1 1 0 0 0 0-2z\"/></g></g>","power-outline":"<g data-name=\"Layer 2\"><g data-name=\"power\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 13a1 1 0 0 0 1-1V2a1 1 0 0 0-2 0v10a1 1 0 0 0 1 1z\"/><path d=\"M16.59 3.11a1 1 0 0 0-.92 1.78 8 8 0 1 1-7.34 0 1 1 0 1 0-.92-1.78 10 10 0 1 0 9.18 0z\"/></g></g>","pricetags-outline":"<g data-name=\"Layer 2\"><g data-name=\"pricetags\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12.87 22a1.84 1.84 0 0 1-1.29-.53l-6.41-6.42a1 1 0 0 1-.29-.61L4 5.09a1 1 0 0 1 .29-.8 1 1 0 0 1 .8-.29l9.35.88a1 1 0 0 1 .61.29l6.42 6.41a1.82 1.82 0 0 1 0 2.57l-7.32 7.32a1.82 1.82 0 0 1-1.28.53zm-6-8.11l6 6 7.05-7.05-6-6-7.81-.73z\"/><circle cx=\"10.5\" cy=\"10.5\" r=\"1.5\"/></g></g>","printer-outline":"<g data-name=\"Layer 2\"><g data-name=\"printer\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M19.36 7H18V5a1.92 1.92 0 0 0-1.83-2H7.83A1.92 1.92 0 0 0 6 5v2H4.64A2.66 2.66 0 0 0 2 9.67v6.66A2.66 2.66 0 0 0 4.64 19h.86a2 2 0 0 0 2 2h9a2 2 0 0 0 2-2h.86A2.66 2.66 0 0 0 22 16.33V9.67A2.66 2.66 0 0 0 19.36 7zM8 5h8v2H8zm-.5 14v-4h9v4zM20 16.33a.66.66 0 0 1-.64.67h-.86v-2a2 2 0 0 0-2-2h-9a2 2 0 0 0-2 2v2h-.86a.66.66 0 0 1-.64-.67V9.67A.66.66 0 0 1 4.64 9h14.72a.66.66 0 0 1 .64.67z\"/></g></g>","question-mark-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"menu-arrow-circle\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M12 6a3.5 3.5 0 0 0-3.5 3.5 1 1 0 0 0 2 0A1.5 1.5 0 1 1 12 11a1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-1.16A3.49 3.49 0 0 0 12 6z\"/><circle cx=\"12\" cy=\"17\" r=\"1\"/></g></g>","question-mark-outline":"<g data-name=\"Layer 2\"><g data-name=\"question-mark\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M17 9A5 5 0 0 0 7 9a1 1 0 0 0 2 0 3 3 0 1 1 3 3 1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-1.1A5 5 0 0 0 17 9z\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/></g></g>","radio-button-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"radio-button-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a10 10 0 1 1 10-10 10 10 0 0 1-10 10zm0-18a8 8 0 1 0 8 8 8 8 0 0 0-8-8z\"/></g></g>","radio-button-on-outline":"<g data-name=\"Layer 2\"><g data-name=\"radio-button-on\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M12 7a5 5 0 1 0 5 5 5 5 0 0 0-5-5zm0 8a3 3 0 1 1 3-3 3 3 0 0 1-3 3z\"/></g></g>","radio-outline":"<g data-name=\"Layer 2\"><g data-name=\"radio\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 8a3 3 0 0 0-1 5.83 1 1 0 0 0 0 .17v6a1 1 0 0 0 2 0v-6a1 1 0 0 0 0-.17A3 3 0 0 0 12 8zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M3.5 11a6.87 6.87 0 0 1 2.64-5.23 1 1 0 1 0-1.28-1.54A8.84 8.84 0 0 0 1.5 11a8.84 8.84 0 0 0 3.36 6.77 1 1 0 1 0 1.28-1.54A6.87 6.87 0 0 1 3.5 11z\"/><path d=\"M16.64 6.24a1 1 0 0 0-1.28 1.52A4.28 4.28 0 0 1 17 11a4.28 4.28 0 0 1-1.64 3.24A1 1 0 0 0 16 16a1 1 0 0 0 .64-.24A6.2 6.2 0 0 0 19 11a6.2 6.2 0 0 0-2.36-4.76z\"/><path d=\"M8.76 6.36a1 1 0 0 0-1.4-.12A6.2 6.2 0 0 0 5 11a6.2 6.2 0 0 0 2.36 4.76 1 1 0 0 0 1.4-.12 1 1 0 0 0-.12-1.4A4.28 4.28 0 0 1 7 11a4.28 4.28 0 0 1 1.64-3.24 1 1 0 0 0 .12-1.4z\"/><path d=\"M19.14 4.23a1 1 0 1 0-1.28 1.54A6.87 6.87 0 0 1 20.5 11a6.87 6.87 0 0 1-2.64 5.23 1 1 0 0 0 1.28 1.54A8.84 8.84 0 0 0 22.5 11a8.84 8.84 0 0 0-3.36-6.77z\"/></g></g>","recording-outline":"<g data-name=\"Layer 2\"><g data-name=\"recording\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 8a4 4 0 0 0-4 4 3.91 3.91 0 0 0 .56 2H9.44a3.91 3.91 0 0 0 .56-2 4 4 0 1 0-4 4h12a4 4 0 0 0 0-8zM4 12a2 2 0 1 1 2 2 2 2 0 0 1-2-2zm14 2a2 2 0 1 1 2-2 2 2 0 0 1-2 2z\"/></g></g>","refresh-outline":"<g data-name=\"Layer 2\"><g data-name=\"refresh\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.3 13.43a1 1 0 0 0-1.25.65A7.14 7.14 0 0 1 12.18 19 7.1 7.1 0 0 1 5 12a7.1 7.1 0 0 1 7.18-7 7.26 7.26 0 0 1 4.65 1.67l-2.17-.36a1 1 0 0 0-1.15.83 1 1 0 0 0 .83 1.15l4.24.7h.17a1 1 0 0 0 .34-.06.33.33 0 0 0 .1-.06.78.78 0 0 0 .2-.11l.09-.11c0-.05.09-.09.13-.15s0-.1.05-.14a1.34 1.34 0 0 0 .07-.18l.75-4a1 1 0 0 0-2-.38l-.27 1.45A9.21 9.21 0 0 0 12.18 3 9.1 9.1 0 0 0 3 12a9.1 9.1 0 0 0 9.18 9A9.12 9.12 0 0 0 21 14.68a1 1 0 0 0-.7-1.25z\"/></g></g>","repeat-outline":"<g data-name=\"Layer 2\"><g data-name=\"repeat\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17.91 5h-12l1.3-1.29a1 1 0 0 0-1.42-1.42l-3 3a1 1 0 0 0 0 1.42l3 3a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L5.91 7h12a1.56 1.56 0 0 1 1.59 1.53V11a1 1 0 0 0 2 0V8.53A3.56 3.56 0 0 0 17.91 5z\"/><path d=\"M18.21 14.29a1 1 0 0 0-1.42 1.42l1.3 1.29h-12a1.56 1.56 0 0 1-1.59-1.53V13a1 1 0 0 0-2 0v2.47A3.56 3.56 0 0 0 6.09 19h12l-1.3 1.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l3-3a1 1 0 0 0 0-1.42z\"/></g></g>","rewind-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"rewind-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.45 6.2a2.1 2.1 0 0 0-2.21.26l-4.74 3.92V7.79a1.76 1.76 0 0 0-1.05-1.59 2.1 2.1 0 0 0-2.21.26l-5.1 4.21a1.7 1.7 0 0 0 0 2.66l5.1 4.21a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59v-2.59l4.74 3.92a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59V7.79a1.76 1.76 0 0 0-1.05-1.59zM9.5 16l-4.82-4L9.5 8.09zm8 0l-4.82-4 4.82-3.91z\"/></g></g>","rewind-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"rewind-right\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.86 10.67l-5.1-4.21a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1.05 1.59v2.59L7.76 6.46a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1 1.59v8.42a1.76 1.76 0 0 0 1 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l4.74-3.92v2.59a1.76 1.76 0 0 0 1.05 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l5.1-4.21a1.7 1.7 0 0 0 0-2.66zM6.5 15.91V8l4.82 4zm8 0V8l4.82 4z\"/></g></g>","save-outline":"<g data-name=\"Layer 2\"><g data-name=\"save\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.12 8.71l-4.83-4.83A3 3 0 0 0 13.17 3H6a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3v-7.17a3 3 0 0 0-.88-2.12zM10 19v-2h4v2zm9-1a1 1 0 0 1-1 1h-2v-3a1 1 0 0 0-1-1H9a1 1 0 0 0-1 1v3H6a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h2v5a1 1 0 0 0 1 1h4a1 1 0 0 0 0-2h-3V5h3.17a1.05 1.05 0 0 1 .71.29l4.83 4.83a1 1 0 0 1 .29.71z\"/></g></g>","scissors-outline":"<g data-name=\"Layer 2\"><g data-name=\"scissors\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.21 5.71a1 1 0 1 0-1.42-1.42l-6.28 6.31-3.3-3.31A3 3 0 0 0 9.5 6a3 3 0 1 0-3 3 3 3 0 0 0 1.29-.3L11.1 12l-3.29 3.3A3 3 0 0 0 6.5 15a3 3 0 1 0 3 3 3 3 0 0 0-.29-1.26zM6.5 7a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm0 12a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/><path d=\"M15.21 13.29a1 1 0 0 0-1.42 1.42l5 5a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/></g></g>","search-outline":"<g data-name=\"Layer 2\"><g data-name=\"search\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.71 19.29l-3.4-3.39A7.92 7.92 0 0 0 19 11a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l3.39 3.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM5 11a6 6 0 1 1 6 6 6 6 0 0 1-6-6z\"/></g></g>","settings-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"settings-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12.94 22h-1.89a1.68 1.68 0 0 1-1.68-1.68v-1.09a.34.34 0 0 0-.22-.29.38.38 0 0 0-.41 0l-.74.8a1.67 1.67 0 0 1-2.37 0L4.26 18.4a1.66 1.66 0 0 1-.5-1.19 1.72 1.72 0 0 1 .5-1.21l.74-.74a.34.34 0 0 0 0-.37c-.06-.15-.16-.26-.3-.26H3.68A1.69 1.69 0 0 1 2 12.94v-1.89a1.68 1.68 0 0 1 1.68-1.68h1.09a.34.34 0 0 0 .29-.22.38.38 0 0 0 0-.41L4.26 8a1.67 1.67 0 0 1 0-2.37L5.6 4.26a1.65 1.65 0 0 1 1.18-.5 1.72 1.72 0 0 1 1.22.5l.74.74a.34.34 0 0 0 .37 0c.15-.06.26-.16.26-.3V3.68A1.69 1.69 0 0 1 11.06 2H13a1.68 1.68 0 0 1 1.68 1.68v1.09a.34.34 0 0 0 .22.29.38.38 0 0 0 .41 0l.69-.8a1.67 1.67 0 0 1 2.37 0l1.37 1.34a1.67 1.67 0 0 1 .5 1.19 1.63 1.63 0 0 1-.5 1.21l-.74.74a.34.34 0 0 0 0 .37c.06.15.16.26.3.26h1.09A1.69 1.69 0 0 1 22 11.06V13a1.68 1.68 0 0 1-1.68 1.68h-1.09a.34.34 0 0 0-.29.22.34.34 0 0 0 0 .37l.77.77a1.67 1.67 0 0 1 0 2.37l-1.31 1.33a1.65 1.65 0 0 1-1.18.5 1.72 1.72 0 0 1-1.19-.5l-.77-.74a.34.34 0 0 0-.37 0c-.15.06-.26.16-.26.3v1.09A1.69 1.69 0 0 1 12.94 22zm-1.57-2h1.26v-.77a2.33 2.33 0 0 1 1.46-2.14 2.36 2.36 0 0 1 2.59.47l.54.54.88-.88-.54-.55a2.34 2.34 0 0 1-.48-2.56 2.33 2.33 0 0 1 2.14-1.45H20v-1.29h-.77a2.33 2.33 0 0 1-2.14-1.46 2.36 2.36 0 0 1 .47-2.59l.54-.54-.88-.88-.55.54a2.39 2.39 0 0 1-4-1.67V4h-1.3v.77a2.33 2.33 0 0 1-1.46 2.14 2.36 2.36 0 0 1-2.59-.47l-.54-.54-.88.88.54.55a2.39 2.39 0 0 1-1.67 4H4v1.26h.77a2.33 2.33 0 0 1 2.14 1.46 2.36 2.36 0 0 1-.47 2.59l-.54.54.88.88.55-.54a2.39 2.39 0 0 1 4 1.67z\" data-name=\"&lt;Group&gt;\"/><path d=\"M12 15.5a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5zm0-5a1.5 1.5 0 1 0 1.5 1.5 1.5 1.5 0 0 0-1.5-1.5z\"/></g></g>","settings-outline":"<g data-name=\"Layer 2\"><g data-name=\"settings\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M8.61 22a2.25 2.25 0 0 1-1.35-.46L5.19 20a2.37 2.37 0 0 1-.49-3.22 2.06 2.06 0 0 0 .23-1.86l-.06-.16a1.83 1.83 0 0 0-1.12-1.22h-.16a2.34 2.34 0 0 1-1.48-2.94L2.93 8a2.18 2.18 0 0 1 1.12-1.41 2.14 2.14 0 0 1 1.68-.12 1.93 1.93 0 0 0 1.78-.29l.13-.1a1.94 1.94 0 0 0 .73-1.51v-.24A2.32 2.32 0 0 1 10.66 2h2.55a2.26 2.26 0 0 1 1.6.67 2.37 2.37 0 0 1 .68 1.68v.28a1.76 1.76 0 0 0 .69 1.43l.11.08a1.74 1.74 0 0 0 1.59.26l.34-.11A2.26 2.26 0 0 1 21.1 7.8l.79 2.52a2.36 2.36 0 0 1-1.46 2.93l-.2.07A1.89 1.89 0 0 0 19 14.6a2 2 0 0 0 .25 1.65l.26.38a2.38 2.38 0 0 1-.5 3.23L17 21.41a2.24 2.24 0 0 1-3.22-.53l-.12-.17a1.75 1.75 0 0 0-1.5-.78 1.8 1.8 0 0 0-1.43.77l-.23.33A2.25 2.25 0 0 1 9 22a2 2 0 0 1-.39 0zM4.4 11.62a3.83 3.83 0 0 1 2.38 2.5v.12a4 4 0 0 1-.46 3.62.38.38 0 0 0 0 .51L8.47 20a.25.25 0 0 0 .37-.07l.23-.33a3.77 3.77 0 0 1 6.2 0l.12.18a.3.3 0 0 0 .18.12.25.25 0 0 0 .19-.05l2.06-1.56a.36.36 0 0 0 .07-.49l-.26-.38A4 4 0 0 1 17.1 14a3.92 3.92 0 0 1 2.49-2.61l.2-.07a.34.34 0 0 0 .19-.44l-.78-2.49a.35.35 0 0 0-.2-.19.21.21 0 0 0-.19 0l-.34.11a3.74 3.74 0 0 1-3.43-.57L15 7.65a3.76 3.76 0 0 1-1.49-3v-.31a.37.37 0 0 0-.1-.26.31.31 0 0 0-.21-.08h-2.54a.31.31 0 0 0-.29.33v.25a3.9 3.9 0 0 1-1.52 3.09l-.13.1a3.91 3.91 0 0 1-3.63.59.22.22 0 0 0-.14 0 .28.28 0 0 0-.12.15L4 11.12a.36.36 0 0 0 .22.45z\" data-name=\"&lt;Group&gt;\"/><path d=\"M12 15.5a3.5 3.5 0 1 1 3.5-3.5 3.5 3.5 0 0 1-3.5 3.5zm0-5a1.5 1.5 0 1 0 1.5 1.5 1.5 1.5 0 0 0-1.5-1.5z\"/></g></g>","shake-outline":"<g data-name=\"Layer 2\"><g data-name=\"shake\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M5.5 18a1 1 0 0 1-.64-.24A8.81 8.81 0 0 1 1.5 11a8.81 8.81 0 0 1 3.36-6.76 1 1 0 1 1 1.28 1.52A6.9 6.9 0 0 0 3.5 11a6.9 6.9 0 0 0 2.64 5.24 1 1 0 0 1 .13 1.4 1 1 0 0 1-.77.36z\"/><path d=\"M12 7a4.09 4.09 0 0 1 1 .14V3a1 1 0 0 0-2 0v4.14A4.09 4.09 0 0 1 12 7z\"/><path d=\"M12 15a4.09 4.09 0 0 1-1-.14V20a1 1 0 0 0 2 0v-5.14a4.09 4.09 0 0 1-1 .14z\"/><path d=\"M16 16a1 1 0 0 1-.77-.36 1 1 0 0 1 .13-1.4A4.28 4.28 0 0 0 17 11a4.28 4.28 0 0 0-1.64-3.24 1 1 0 1 1 1.28-1.52A6.2 6.2 0 0 1 19 11a6.2 6.2 0 0 1-2.36 4.76A1 1 0 0 1 16 16z\"/><path d=\"M8 16a1 1 0 0 1-.64-.24A6.2 6.2 0 0 1 5 11a6.2 6.2 0 0 1 2.36-4.76 1 1 0 1 1 1.28 1.52A4.28 4.28 0 0 0 7 11a4.28 4.28 0 0 0 1.64 3.24 1 1 0 0 1 .13 1.4A1 1 0 0 1 8 16z\"/><path d=\"M18.5 18a1 1 0 0 1-.77-.36 1 1 0 0 1 .13-1.4A6.9 6.9 0 0 0 20.5 11a6.9 6.9 0 0 0-2.64-5.24 1 1 0 1 1 1.28-1.52A8.81 8.81 0 0 1 22.5 11a8.81 8.81 0 0 1-3.36 6.76 1 1 0 0 1-.64.24z\"/><path d=\"M12 12a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm0-1zm0 0zm0 0zm0 0zm0 0zm0 0zm0 0z\"/></g></g>","share-outline":"<g data-name=\"Layer 2\"><g data-name=\"share\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 15a3 3 0 0 0-2.1.86L8 12.34V12v-.33l7.9-3.53A3 3 0 1 0 15 6v.34L7.1 9.86a3 3 0 1 0 0 4.28l7.9 3.53V18a3 3 0 1 0 3-3zm0-10a1 1 0 1 1-1 1 1 1 0 0 1 1-1zM5 13a1 1 0 1 1 1-1 1 1 0 0 1-1 1zm13 6a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","shield-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"shield-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M4.71 3.29a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M12.3 19.68l-.3.17-.3-.17A13.15 13.15 0 0 1 5 8.23v-.14L5.16 8 3.73 6.56A2 2 0 0 0 3 8.09v.14a15.17 15.17 0 0 0 7.72 13.2l.3.17a2 2 0 0 0 2 0l.3-.17a15.22 15.22 0 0 0 3-2.27l-1.42-1.42a12.56 12.56 0 0 1-2.6 1.94z\"/><path d=\"M20 6.34L13 2.4a2 2 0 0 0-2 0L7.32 4.49 8.78 6 12 4.15l7 3.94v.14a13 13 0 0 1-1.63 6.31L18.84 16A15.08 15.08 0 0 0 21 8.23v-.14a2 2 0 0 0-1-1.75z\"/></g></g>","shield-outline":"<g data-name=\"Layer 2\"><g data-name=\"shield\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 21.85a2 2 0 0 1-1-.25l-.3-.17A15.17 15.17 0 0 1 3 8.23v-.14a2 2 0 0 1 1-1.75l7-3.94a2 2 0 0 1 2 0l7 3.94a2 2 0 0 1 1 1.75v.14a15.17 15.17 0 0 1-7.72 13.2l-.3.17a2 2 0 0 1-.98.25zm0-17.7L5 8.09v.14a13.15 13.15 0 0 0 6.7 11.45l.3.17.3-.17A13.15 13.15 0 0 0 19 8.23v-.14z\"/></g></g>","shopping-bag-outline":"<g data-name=\"Layer 2\"><g data-name=\"shopping-bag\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.12 6.71l-2.83-2.83A3 3 0 0 0 15.17 3H8.83a3 3 0 0 0-2.12.88L3.88 6.71A3 3 0 0 0 3 8.83V18a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V8.83a3 3 0 0 0-.88-2.12zm-12-1.42A1.05 1.05 0 0 1 8.83 5h6.34a1.05 1.05 0 0 1 .71.29L17.59 7H6.41zM18 19H6a1 1 0 0 1-1-1V9h14v9a1 1 0 0 1-1 1z\"/><path d=\"M15 11a1 1 0 0 0-1 1 2 2 0 0 1-4 0 1 1 0 0 0-2 0 4 4 0 0 0 8 0 1 1 0 0 0-1-1z\"/></g></g>","shopping-cart-outline":"<g data-name=\"Layer 2\"><g data-name=\"shopping-cart\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.08 7a2 2 0 0 0-1.7-1H6.58L6 3.74A1 1 0 0 0 5 3H3a1 1 0 0 0 0 2h1.24L7 15.26A1 1 0 0 0 8 16h9a1 1 0 0 0 .89-.55l3.28-6.56A2 2 0 0 0 21.08 7zm-4.7 7H8.76L7.13 8h12.25z\"/><circle cx=\"7.5\" cy=\"19.5\" r=\"1.5\"/><circle cx=\"17.5\" cy=\"19.5\" r=\"1.5\"/></g></g>","shuffle-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"shuffle-2\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18.71 14.29a1 1 0 0 0-1.42 1.42l.29.29H16a4 4 0 0 1 0-8h1.59l-.3.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 10a1 1 0 0 0 .71-.29l2-2A1 1 0 0 0 21 7a1 1 0 0 0-.29-.71l-2-2a1 1 0 0 0-1.42 1.42l.29.29H16a6 6 0 0 0-5 2.69A6 6 0 0 0 6 6H4a1 1 0 0 0 0 2h2a4 4 0 0 1 0 8H4a1 1 0 0 0 0 2h2a6 6 0 0 0 5-2.69A6 6 0 0 0 16 18h1.59l-.3.29a1 1 0 0 0 0 1.42A1 1 0 0 0 18 20a1 1 0 0 0 .71-.29l2-2A1 1 0 0 0 21 17a1 1 0 0 0-.29-.71z\"/></g></g>","shuffle-outline":"<g data-name=\"Layer 2\"><g data-name=\"shuffle\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M18 9.31a1 1 0 0 0 1 1 1 1 0 0 0 1-1V5a1 1 0 0 0-1-1h-4.3a1 1 0 0 0-1 1 1 1 0 0 0 1 1h1.89L12 10.59 6.16 4.76a1 1 0 0 0-1.41 1.41L10.58 12l-6.29 6.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0L18 7.42z\"/><path d=\"M19 13.68a1 1 0 0 0-1 1v1.91l-2.78-2.79a1 1 0 0 0-1.42 1.42L16.57 18h-1.88a1 1 0 0 0 0 2H19a1 1 0 0 0 1-1.11v-4.21a1 1 0 0 0-1-1z\"/></g></g>","skip-back-outline":"<g data-name=\"Layer 2\"><g data-name=\"skip-back\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M16.45 6.2a2.1 2.1 0 0 0-2.21.26l-5.1 4.21-.14.15V7a1 1 0 0 0-2 0v10a1 1 0 0 0 2 0v-3.82l.14.15 5.1 4.21a2.06 2.06 0 0 0 1.3.46 2.23 2.23 0 0 0 .91-.2 1.76 1.76 0 0 0 1.05-1.59V7.79a1.76 1.76 0 0 0-1.05-1.59zM15.5 16l-4.82-4 4.82-3.91z\"/></g></g>","skip-forward-outline":"<g data-name=\"Layer 2\"><g data-name=\"skip-forward\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M16 6a1 1 0 0 0-1 1v3.82l-.14-.15-5.1-4.21a2.1 2.1 0 0 0-2.21-.26 1.76 1.76 0 0 0-1 1.59v8.42a1.76 1.76 0 0 0 1 1.59 2.23 2.23 0 0 0 .91.2 2.06 2.06 0 0 0 1.3-.46l5.1-4.21.14-.15V17a1 1 0 0 0 2 0V7a1 1 0 0 0-1-1zm-7.5 9.91V8l4.82 4z\"/></g></g>","slash-outline":"<g data-name=\"Layer 2\"><g data-name=\"slash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm8 10a7.92 7.92 0 0 1-1.69 4.9L7.1 5.69A7.92 7.92 0 0 1 12 4a8 8 0 0 1 8 8zM4 12a7.92 7.92 0 0 1 1.69-4.9L16.9 18.31A7.92 7.92 0 0 1 12 20a8 8 0 0 1-8-8z\"/></g></g>","smartphone-outline":"<g data-name=\"Layer 2\"><g data-name=\"smartphone\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17 2H7a3 3 0 0 0-3 3v14a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3zm1 17a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V5a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1z\"/><circle cx=\"12\" cy=\"16.5\" r=\"1.5\"/><path d=\"M14.5 6h-5a1 1 0 0 0 0 2h5a1 1 0 0 0 0-2z\"/></g></g>","smiling-face-outline":"<defs><style/></defs><g id=\"Layer_2\" data-name=\"Layer 2\"><g id=\"smiling-face\"><g id=\"smiling-face\" data-name=\"smiling-face\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm0 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16zm5 9a5 5 0 0 1-10 0z\" id=\"&#x1F3A8;-Icon-&#x421;olor\"/></g></g></g>","speaker-outline":"<g data-name=\"Layer 2\"><g data-name=\"speaker\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M12 11a3 3 0 1 0-3-3 3 3 0 0 0 3 3zm0-4a1 1 0 1 1-1 1 1 1 0 0 1 1-1z\"/><path d=\"M12 12a3.5 3.5 0 1 0 3.5 3.5A3.5 3.5 0 0 0 12 12zm0 5a1.5 1.5 0 1 1 1.5-1.5A1.5 1.5 0 0 1 12 17z\"/><path d=\"M17 2H7a3 3 0 0 0-3 3v14a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V5a3 3 0 0 0-3-3zm1 17a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V5a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1z\"/></g></g>","square-outline":"<g data-name=\"Layer 2\"><g data-name=\"square\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 21H6a3 3 0 0 1-3-3V6a3 3 0 0 1 3-3h12a3 3 0 0 1 3 3v12a3 3 0 0 1-3 3zM6 5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V6a1 1 0 0 0-1-1z\"/></g></g>","star-outline":"<g data-name=\"Layer 2\"><g data-name=\"star\"><rect width=\"24\" height=\"24\" transform=\"rotate(90 12 12)\" opacity=\"0\"/><path d=\"M17.56 21a1 1 0 0 1-.46-.11L12 18.22l-5.1 2.67a1 1 0 0 1-1.45-1.06l1-5.63-4.12-4a1 1 0 0 1-.25-1 1 1 0 0 1 .81-.68l5.7-.83 2.51-5.13a1 1 0 0 1 1.8 0l2.54 5.12 5.7.83a1 1 0 0 1 .81.68 1 1 0 0 1-.25 1l-4.12 4 1 5.63a1 1 0 0 1-.4 1 1 1 0 0 1-.62.18zM12 16.1a.92.92 0 0 1 .46.11l3.77 2-.72-4.21a1 1 0 0 1 .29-.89l3-2.93-4.2-.62a1 1 0 0 1-.71-.56L12 5.25 10.11 9a1 1 0 0 1-.75.54l-4.2.62 3 2.93a1 1 0 0 1 .29.89l-.72 4.16 3.77-2a.92.92 0 0 1 .5-.04z\"/></g></g>","stop-circle-outline":"<g data-name=\"Layer 2\"><g data-name=\"stop-circle\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8z\"/><path d=\"M14.75 8h-5.5A1.25 1.25 0 0 0 8 9.25v5.5A1.25 1.25 0 0 0 9.25 16h5.5A1.25 1.25 0 0 0 16 14.75v-5.5A1.25 1.25 0 0 0 14.75 8zM14 14h-4v-4h4z\"/></g></g>","sun-outline":"<g data-name=\"Layer 2\"><g data-name=\"sun\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M12 6a1 1 0 0 0 1-1V3a1 1 0 0 0-2 0v2a1 1 0 0 0 1 1z\"/><path d=\"M21 11h-2a1 1 0 0 0 0 2h2a1 1 0 0 0 0-2z\"/><path d=\"M6 12a1 1 0 0 0-1-1H3a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1z\"/><path d=\"M6.22 5a1 1 0 0 0-1.39 1.47l1.44 1.39a1 1 0 0 0 .73.28 1 1 0 0 0 .72-.31 1 1 0 0 0 0-1.41z\"/><path d=\"M17 8.14a1 1 0 0 0 .69-.28l1.44-1.39A1 1 0 0 0 17.78 5l-1.44 1.42a1 1 0 0 0 0 1.41 1 1 0 0 0 .66.31z\"/><path d=\"M12 18a1 1 0 0 0-1 1v2a1 1 0 0 0 2 0v-2a1 1 0 0 0-1-1z\"/><path d=\"M17.73 16.14a1 1 0 0 0-1.39 1.44L17.78 19a1 1 0 0 0 .69.28 1 1 0 0 0 .72-.3 1 1 0 0 0 0-1.42z\"/><path d=\"M6.27 16.14l-1.44 1.39a1 1 0 0 0 0 1.42 1 1 0 0 0 .72.3 1 1 0 0 0 .67-.25l1.44-1.39a1 1 0 0 0-1.39-1.44z\"/><path d=\"M12 8a4 4 0 1 0 4 4 4 4 0 0 0-4-4zm0 6a2 2 0 1 1 2-2 2 2 0 0 1-2 2z\"/></g></g>","swap-outline":"<g data-name=\"Layer 2\"><g data-name=\"swap\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M4 9h13l-1.6 1.2a1 1 0 0 0-.2 1.4 1 1 0 0 0 .8.4 1 1 0 0 0 .6-.2l4-3a1 1 0 0 0 0-1.59l-3.86-3a1 1 0 0 0-1.23 1.58L17.08 7H4a1 1 0 0 0 0 2z\"/><path d=\"M20 16H7l1.6-1.2a1 1 0 0 0-1.2-1.6l-4 3a1 1 0 0 0 0 1.59l3.86 3a1 1 0 0 0 .61.21 1 1 0 0 0 .79-.39 1 1 0 0 0-.17-1.4L6.92 18H20a1 1 0 0 0 0-2z\"/></g></g>","sync-outline":"<g data-name=\"Layer 2\"><g data-name=\"sync\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21.66 10.37a.62.62 0 0 0 .07-.19l.75-4a1 1 0 0 0-2-.36l-.37 2a9.22 9.22 0 0 0-16.58.84 1 1 0 0 0 .55 1.3 1 1 0 0 0 1.31-.55A7.08 7.08 0 0 1 12.07 5a7.17 7.17 0 0 1 6.24 3.58l-1.65-.27a1 1 0 1 0-.32 2l4.25.71h.16a.93.93 0 0 0 .34-.06.33.33 0 0 0 .1-.06.78.78 0 0 0 .2-.11l.08-.1a1.07 1.07 0 0 0 .14-.16.58.58 0 0 0 .05-.16z\"/><path d=\"M19.88 14.07a1 1 0 0 0-1.31.56A7.08 7.08 0 0 1 11.93 19a7.17 7.17 0 0 1-6.24-3.58l1.65.27h.16a1 1 0 0 0 .16-2L3.41 13a.91.91 0 0 0-.33 0H3a1.15 1.15 0 0 0-.32.14 1 1 0 0 0-.18.18l-.09.1a.84.84 0 0 0-.07.19.44.44 0 0 0-.07.17l-.75 4a1 1 0 0 0 .8 1.22h.18a1 1 0 0 0 1-.82l.37-2a9.22 9.22 0 0 0 16.58-.83 1 1 0 0 0-.57-1.28z\"/></g></g>","text-outline":"<g data-name=\"Layer 2\"><g data-name=\"text\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20 4H4a1 1 0 0 0-1 1v3a1 1 0 0 0 2 0V6h6v13H9a1 1 0 0 0 0 2h6a1 1 0 0 0 0-2h-2V6h6v2a1 1 0 0 0 2 0V5a1 1 0 0 0-1-1z\"/></g></g>","thermometer-minus-outline":"<g data-name=\"Layer 2\"><g data-name=\"thermometer-minus\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\"/><path d=\"M14 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm0-18a1 1 0 0 0-1 1v8.54a1 1 0 0 1-.5.87A3 3 0 0 0 11 17a3 3 0 0 0 6 0 3 3 0 0 0-1.5-2.59 1 1 0 0 1-.5-.87V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 14 4z\"/></g></g>","thermometer-outline":"<g data-name=\"Layer 2\"><g data-name=\"thermometer\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm0-18a1 1 0 0 0-1 1v8.54a1 1 0 0 1-.5.87A3 3 0 0 0 9 17a3 3 0 0 0 6 0 3 3 0 0 0-1.5-2.59 1 1 0 0 1-.5-.87V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 12 4z\"/></g></g>","thermometer-plus-outline":"<g data-name=\"Layer 2\"><g data-name=\"thermometer-plus\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\"/><rect x=\"2\" y=\"5\" width=\"6\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(-90 5 6)\"/><path d=\"M14 22a5 5 0 0 1-3-9V5a3 3 0 0 1 3-3 3 3 0 0 1 3 3v8a5 5 0 0 1-3 9zm0-18a1 1 0 0 0-1 1v8.54a1 1 0 0 1-.5.87A3 3 0 0 0 11 17a3 3 0 0 0 6 0 3 3 0 0 0-1.5-2.59 1 1 0 0 1-.5-.87V5a.93.93 0 0 0-.29-.69A1 1 0 0 0 14 4z\"/></g></g>","toggle-left-outline":"<g data-name=\"Layer 2\"><g data-name=\"toggle-left\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><path d=\"M15 5H9a7 7 0 0 0 0 14h6a7 7 0 0 0 0-14zm0 12H9A5 5 0 0 1 9 7h6a5 5 0 0 1 0 10z\"/><path d=\"M9 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","toggle-right-outline":"<g data-name=\"Layer 2\"><g data-name=\"toggle-right\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M15 5H9a7 7 0 0 0 0 14h6a7 7 0 0 0 0-14zm0 12H9A5 5 0 0 1 9 7h6a5 5 0 0 1 0 10z\"/><path d=\"M15 9a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","trash-2-outline":"<g data-name=\"Layer 2\"><g data-name=\"trash-2\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-5V4.33A2.42 2.42 0 0 0 13.5 2h-3A2.42 2.42 0 0 0 8 4.33V6H3a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2zM10 4.33c0-.16.21-.33.5-.33h3c.29 0 .5.17.5.33V6h-4zM18 19a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V8h12z\"/><path d=\"M9 17a1 1 0 0 0 1-1v-4a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1z\"/><path d=\"M15 17a1 1 0 0 0 1-1v-4a1 1 0 0 0-2 0v4a1 1 0 0 0 1 1z\"/></g></g>","trash-outline":"<g data-name=\"Layer 2\"><g data-name=\"trash\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 6h-5V4.33A2.42 2.42 0 0 0 13.5 2h-3A2.42 2.42 0 0 0 8 4.33V6H3a1 1 0 0 0 0 2h1v11a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3V8h1a1 1 0 0 0 0-2zM10 4.33c0-.16.21-.33.5-.33h3c.29 0 .5.17.5.33V6h-4zM18 19a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V8h12z\"/></g></g>","trending-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"trending-down\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M21 12a1 1 0 0 0-2 0v2.3l-4.24-5a1 1 0 0 0-1.27-.21L9.22 11.7 4.77 6.36a1 1 0 1 0-1.54 1.28l5 6a1 1 0 0 0 1.28.22l4.28-2.57 4 4.71H15a1 1 0 0 0 0 2h5a1.1 1.1 0 0 0 .36-.07l.14-.08a1.19 1.19 0 0 0 .15-.09.75.75 0 0 0 .14-.17 1.1 1.1 0 0 0 .09-.14.64.64 0 0 0 .05-.17A.78.78 0 0 0 21 17z\"/></g></g>","trending-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"trending-up\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M21 7a.78.78 0 0 0 0-.21.64.64 0 0 0-.05-.17 1.1 1.1 0 0 0-.09-.14.75.75 0 0 0-.14-.17l-.12-.07a.69.69 0 0 0-.19-.1h-.2A.7.7 0 0 0 20 6h-5a1 1 0 0 0 0 2h2.83l-4 4.71-4.32-2.57a1 1 0 0 0-1.28.22l-5 6a1 1 0 0 0 .13 1.41A1 1 0 0 0 4 18a1 1 0 0 0 .77-.36l4.45-5.34 4.27 2.56a1 1 0 0 0 1.27-.21L19 9.7V12a1 1 0 0 0 2 0V7z\"/></g></g>","tv-outline":"<g data-name=\"Layer 2\"><g data-name=\"tv\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18 6h-3.59l2.3-2.29a1 1 0 1 0-1.42-1.42L12 5.59l-3.29-3.3a1 1 0 1 0-1.42 1.42L9.59 6H6a3 3 0 0 0-3 3v10a3 3 0 0 0 3 3h12a3 3 0 0 0 3-3V9a3 3 0 0 0-3-3zm1 13a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1z\"/></g></g>","twitter-outline":"<g data-name=\"Layer 2\"><g data-name=\"twitter\"><polyline points=\"0 0 24 0 24 24 0 24\" opacity=\"0\"/><path d=\"M8.51 20h-.08a10.87 10.87 0 0 1-4.65-1.09A1.38 1.38 0 0 1 3 17.47a1.41 1.41 0 0 1 1.16-1.18 6.63 6.63 0 0 0 2.54-.89 9.49 9.49 0 0 1-3.51-9.07 1.41 1.41 0 0 1 1-1.15 1.35 1.35 0 0 1 1.43.41 7.09 7.09 0 0 0 4.88 2.75 4.5 4.5 0 0 1 1.41-3.1 4.47 4.47 0 0 1 6.37.19.7.7 0 0 0 .78.1A1.39 1.39 0 0 1 21 7.13a6.66 6.66 0 0 1-1.28 2.6A10.79 10.79 0 0 1 8.51 20zm0-2h.08a8.79 8.79 0 0 0 9.09-8.59 1.32 1.32 0 0 1 .37-.85 5.19 5.19 0 0 0 .62-1 2.56 2.56 0 0 1-1.91-.85A2.45 2.45 0 0 0 15 6a2.5 2.5 0 0 0-1.79.69 2.53 2.53 0 0 0-.72 2.42l.26 1.14-1.17.08a8.3 8.3 0 0 1-6.54-2.4 7.12 7.12 0 0 0 3.73 6.46l.95.54-.63.9a5.62 5.62 0 0 1-2.68 1.92A8.34 8.34 0 0 0 8.5 18zM19 6.65z\"/></g></g>","umbrella-outline":"<g data-name=\"Layer 2\"><g data-name=\"umbrella\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M12 2A10 10 0 0 0 2 12a1 1 0 0 0 1 1h8v6a3 3 0 0 0 6 0 1 1 0 0 0-2 0 1 1 0 0 1-2 0v-6h8a1 1 0 0 0 1-1A10 10 0 0 0 12 2zm-7.94 9a8 8 0 0 1 15.88 0z\"/></g></g>","undo-outline":"<g data-name=\"Layer 2\"><g data-name=\"undo\"><rect width=\"24\" height=\"24\" transform=\"rotate(-90 12 12)\" opacity=\"0\"/><path d=\"M20.22 21a1 1 0 0 1-1-.76 8.91 8.91 0 0 0-7.8-6.69v1.12a1.78 1.78 0 0 1-1.09 1.64A2 2 0 0 1 8.18 16l-5.06-4.41a1.76 1.76 0 0 1 0-2.68l5.06-4.42a2 2 0 0 1 2.18-.3 1.78 1.78 0 0 1 1.09 1.64V7A10.89 10.89 0 0 1 21.5 17.75a10.29 10.29 0 0 1-.31 2.49 1 1 0 0 1-1 .76zm-9.77-9.5a11.07 11.07 0 0 1 8.81 4.26A9 9 0 0 0 10.45 9a1 1 0 0 1-1-1V6.08l-4.82 4.17 4.82 4.21v-2a1 1 0 0 1 1-.96z\"/></g></g>","unlock-outline":"<g data-name=\"Layer 2\"><g data-name=\"unlock\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17 8h-7V6a2 2 0 0 1 4 0 1 1 0 0 0 2 0 4 4 0 0 0-8 0v2H7a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h10a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3zm1 11a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1v-8a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1z\"/><path d=\"M12 12a3 3 0 1 0 3 3 3 3 0 0 0-3-3zm0 4a1 1 0 1 1 1-1 1 1 0 0 1-1 1z\"/></g></g>","upload-outline":"<g data-name=\"Layer 2\"><g data-name=\"upload\"><rect width=\"24\" height=\"24\" transform=\"rotate(180 12 12)\" opacity=\"0\"/><rect x=\"4\" y=\"4\" width=\"16\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(180 12 5)\"/><rect x=\"17\" y=\"5\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(90 19 6)\"/><rect x=\"3\" y=\"5\" width=\"4\" height=\"2\" rx=\"1\" ry=\"1\" transform=\"rotate(90 5 6)\"/><path d=\"M8 14a1 1 0 0 1-.8-.4 1 1 0 0 1 .2-1.4l4-3a1 1 0 0 1 1.18 0l4 2.82a1 1 0 0 1 .24 1.39 1 1 0 0 1-1.4.24L12 11.24 8.6 13.8a1 1 0 0 1-.6.2z\"/><path d=\"M12 21a1 1 0 0 1-1-1v-8a1 1 0 0 1 2 0v8a1 1 0 0 1-1 1z\"/></g></g>","video-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"video-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17 15.59l-2-2L8.41 7l-2-2-1.7-1.71a1 1 0 0 0-1.42 1.42l.54.53L5.59 7l9.34 9.34 1.46 1.46 2.9 2.91a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M14 17H5a1 1 0 0 1-1-1V8a1 1 0 0 1 .4-.78L3 5.8A3 3 0 0 0 2 8v8a3 3 0 0 0 3 3h9a2.94 2.94 0 0 0 1.66-.51L14.14 17a.7.7 0 0 1-.14 0z\"/><path d=\"M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H7.83l2 2H14a1 1 0 0 1 1 1v4.17l4.72 4.72a1.73 1.73 0 0 0 .6.11 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63A1.6 1.6 0 0 0 21 7.15zm-1 7.45L17.19 12 20 9.4z\"/></g></g>","video-outline":"<g data-name=\"Layer 2\"><g data-name=\"video\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M21 7.15a1.7 1.7 0 0 0-1.85.3l-2.15 2V8a3 3 0 0 0-3-3H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h9a3 3 0 0 0 3-3v-1.45l2.16 2a1.74 1.74 0 0 0 1.16.45 1.68 1.68 0 0 0 .69-.15 1.6 1.6 0 0 0 1-1.48V8.63A1.6 1.6 0 0 0 21 7.15zM15 16a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h9a1 1 0 0 1 1 1zm5-1.4L17.19 12 20 9.4z\"/></g></g>","volume-down-outline":"<g data-name=\"Layer 2\"><g data-name=\"volume-down\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M20.78 8.37a1 1 0 1 0-1.56 1.26 4 4 0 0 1 0 4.74A1 1 0 0 0 20 16a1 1 0 0 0 .78-.37 6 6 0 0 0 0-7.26z\"/><path d=\"M16.47 3.12a1 1 0 0 0-1 0L9 7.57H4a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4A1.06 1.06 0 0 0 16 21a1 1 0 0 0 1-1V4a1 1 0 0 0-.53-.88zM15 18.1l-5.1-3.5a1 1 0 0 0-.57-.17H5V9.57h4.33a1 1 0 0 0 .57-.17L15 5.9z\"/></g></g>","volume-mute-outline":"<g data-name=\"Layer 2\"><g data-name=\"volume-mute\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M17 21a1.06 1.06 0 0 1-.57-.17L10 16.43H5a1 1 0 0 1-1-1V8.57a1 1 0 0 1 1-1h5l6.41-4.4A1 1 0 0 1 18 4v16a1 1 0 0 1-1 1zM6 14.43h4.33a1 1 0 0 1 .57.17l5.1 3.5V5.9l-5.1 3.5a1 1 0 0 1-.57.17H6z\"/></g></g>","volume-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"volume-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M4.71 3.29a1 1 0 0 0-1.42 1.42l16 16a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M16.91 14.08l1.44 1.44a6 6 0 0 0-.07-7.15 1 1 0 1 0-1.56 1.26 4 4 0 0 1 .19 4.45z\"/><path d=\"M21 12a6.51 6.51 0 0 1-1.78 4.39l1.42 1.42A8.53 8.53 0 0 0 23 12a8.75 8.75 0 0 0-3.36-6.77 1 1 0 1 0-1.28 1.54A6.8 6.8 0 0 1 21 12z\"/><path d=\"M13.5 18.1l-5.1-3.5a1 1 0 0 0-.57-.17H3.5V9.57h3.24l-2-2H2.5a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4a1.06 1.06 0 0 0 .57.17 1 1 0 0 0 1-1v-1.67l-2-2z\"/><path d=\"M13.5 5.9v4.77l2 2V4a1 1 0 0 0-1.57-.83L9.23 6.4l1.44 1.44z\"/></g></g>","volume-up-outline":"<g data-name=\"Layer 2\"><g data-name=\"volume-up\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><path d=\"M18.28 8.37a1 1 0 1 0-1.56 1.26 4 4 0 0 1 0 4.74A1 1 0 0 0 17.5 16a1 1 0 0 0 .78-.37 6 6 0 0 0 0-7.26z\"/><path d=\"M19.64 5.23a1 1 0 1 0-1.28 1.54A6.8 6.8 0 0 1 21 12a6.8 6.8 0 0 1-2.64 5.23 1 1 0 0 0-.13 1.41A1 1 0 0 0 19 19a1 1 0 0 0 .64-.23A8.75 8.75 0 0 0 23 12a8.75 8.75 0 0 0-3.36-6.77z\"/><path d=\"M15 3.12a1 1 0 0 0-1 0L7.52 7.57h-5a1 1 0 0 0-1 1v6.86a1 1 0 0 0 1 1h5l6.41 4.4a1.06 1.06 0 0 0 .57.17 1 1 0 0 0 1-1V4a1 1 0 0 0-.5-.88zm-1.47 15L8.4 14.6a1 1 0 0 0-.57-.17H3.5V9.57h4.33a1 1 0 0 0 .57-.17l5.1-3.5z\"/></g></g>","wifi-off-outline":"<g data-name=\"Layer 2\"><g data-name=\"wifi-off\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/><path d=\"M12.44 11l-1.9-1.89-2.46-2.44-1.55-1.55-1.82-1.83a1 1 0 0 0-1.42 1.42l1.38 1.37 1.46 1.46 2.23 2.24 1.55 1.54 2.74 2.74 2.79 2.8 3.85 3.85a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42z\"/><path d=\"M21.72 7.93A13.93 13.93 0 0 0 12 4a14.1 14.1 0 0 0-4.44.73l1.62 1.62a11.89 11.89 0 0 1 11.16 3 1 1 0 0 0 .69.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.03-1.39z\"/><path d=\"M3.82 6.65a14.32 14.32 0 0 0-1.54 1.28 1 1 0 0 0 1.38 1.44 13.09 13.09 0 0 1 1.6-1.29z\"/><path d=\"M17 13.14a1 1 0 0 0 .71.3 1 1 0 0 0 .72-1.69A9 9 0 0 0 12 9h-.16l2.35 2.35A7 7 0 0 1 17 13.14z\"/><path d=\"M7.43 10.26a8.8 8.8 0 0 0-1.9 1.49A1 1 0 0 0 7 13.14a7.3 7.3 0 0 1 2-1.41z\"/><path d=\"M8.53 15.4a1 1 0 1 0 1.39 1.44 3.06 3.06 0 0 1 3.84-.25l-2.52-2.52a5 5 0 0 0-2.71 1.33z\"/></g></g>","wifi-outline":"<g data-name=\"Layer 2\"><g data-name=\"wifi\"><rect width=\"24\" height=\"24\" opacity=\"0\"/><circle cx=\"12\" cy=\"19\" r=\"1\"/><path d=\"M12 14a5 5 0 0 0-3.47 1.4 1 1 0 1 0 1.39 1.44 3.08 3.08 0 0 1 4.16 0 1 1 0 1 0 1.39-1.44A5 5 0 0 0 12 14z\"/><path d=\"M12 9a9 9 0 0 0-6.47 2.75A1 1 0 0 0 7 13.14a7 7 0 0 1 10.08 0 1 1 0 0 0 .71.3 1 1 0 0 0 .72-1.69A9 9 0 0 0 12 9z\"/><path d=\"M21.72 7.93a14 14 0 0 0-19.44 0 1 1 0 0 0 1.38 1.44 12 12 0 0 1 16.68 0 1 1 0 0 0 .69.28 1 1 0 0 0 .72-.31 1 1 0 0 0-.03-1.41z\"/></g></g>"};

/***/ }),

/***/ "./package/src/animation.scss":
/*!************************************!*\
  !*** ./package/src/animation.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


    var refs = 0;
    var css = __webpack_require__(/*! !../../node_modules/css-loader!../../node_modules/sass-loader/lib/loader.js!./animation.scss */ "./node_modules/css-loader/index.js!./node_modules/sass-loader/lib/loader.js!./package/src/animation.scss");
    var insertCss = __webpack_require__(/*! ../../node_modules/isomorphic-style-loader/insertCss.js */ "./node_modules/isomorphic-style-loader/insertCss.js");
    var content = typeof css === 'string' ? [[module.i, css, '']] : css;

    exports = module.exports = css.locals || {};
    exports._getContent = function() { return content; };
    exports._getCss = function() { return '' + css; };
    exports._insertCss = function(options) { return insertCss(content, options) };

    // Hot Module Replacement
    // https://webpack.github.io/docs/hot-module-replacement
    // Only activated in browser context
    if (false) { var removeCss; }
  

/***/ }),

/***/ "./package/src/default-attrs.json":
/*!****************************************!*\
  !*** ./package/src/default-attrs.json ***!
  \****************************************/
/*! exports provided: xmlns, width, height, viewBox, default */
/***/ (function(module) {

module.exports = {"xmlns":"http://www.w3.org/2000/svg","width":24,"height":24,"viewBox":"0 0 24 24"};

/***/ }),

/***/ "./package/src/icon.js":
/*!*****************************!*\
  !*** ./package/src/icon.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _default_attrs_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./default-attrs.json */ "./package/src/default-attrs.json");
var _default_attrs_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./default-attrs.json */ "./package/src/default-attrs.json", 1);
function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */


var defaultAnimationOptions = {
  hover: true
};

var isString = function isString(value) {
  return typeof value === 'string' || value instanceof String;
};

var Icon =
/*#__PURE__*/
function () {
  function Icon(name, contents) {
    _classCallCheck(this, Icon);

    this.name = name;
    this.contents = contents;
    this.attrs = _objectSpread({}, _default_attrs_json__WEBPACK_IMPORTED_MODULE_1__, {
      class: "eva eva-".concat(name)
    });
  }

  _createClass(Icon, [{
    key: "toSvg",
    value: function toSvg() {
      var attrs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var animation = attrs.animation,
          remAttrs = _objectWithoutProperties(attrs, ["animation"]);

      var animationOptions = getAnimationOptions(animation);
      var animationClasses = animationOptions ? animationOptions.class : '';

      var combinedAttrs = _objectSpread({}, this.attrs, remAttrs, {
        class: classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default()(this.attrs.class, attrs.class, animationClasses)
      });

      var svg = "<svg ".concat(attrsToString(combinedAttrs), ">").concat(this.contents, "</svg>");
      return !!animationOptions ? animationOptions.hover ? "<i class=\"eva-hover\">".concat(svg, "</i>") : svg : svg;
    }
  }, {
    key: "toString",
    value: function toString() {
      return this.contents;
    }
  }]);

  return Icon;
}();

function getAnimationOptions(animation) {
  if (!animation) {
    return null;
  }

  if (animation.hover) {
    animation.hover = isString(animation.hover) ? JSON.parse(animation.hover) : animation.hover;
  }

  var mergedAnimationOptions = _objectSpread({}, defaultAnimationOptions, animation);

  var animationType = mergedAnimationOptions.hover ? "eva-icon-hover-".concat(mergedAnimationOptions.type) : "eva-icon-".concat(mergedAnimationOptions.type);
  mergedAnimationOptions.class = classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default()({
    'eva-animation': true,
    'eva-infinite': isString(animation.infinite) ? JSON.parse(animation.infinite) : animation.infinite
  }, animationType);
  return mergedAnimationOptions;
}

function attrsToString(attrs) {
  return Object.keys(attrs).map(function (key) {
    return "".concat(key, "=\"").concat(attrs[key], "\"");
  }).join(' ');
}

/* harmony default export */ __webpack_exports__["default"] = (Icon);

/***/ }),

/***/ "./package/src/icons.js":
/*!******************************!*\
  !*** ./package/src/icons.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./icon */ "./package/src/icon.js");
/* harmony import */ var _package_build_eva_icons_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../package-build/eva-icons.json */ "./package-build/eva-icons.json");
var _package_build_eva_icons_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../package-build/eva-icons.json */ "./package-build/eva-icons.json", 1);
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */


/* harmony default export */ __webpack_exports__["default"] = (Object.keys(_package_build_eva_icons_json__WEBPACK_IMPORTED_MODULE_1__).map(function (key) {
  return new _icon__WEBPACK_IMPORTED_MODULE_0__["default"](key, _package_build_eva_icons_json__WEBPACK_IMPORTED_MODULE_1__[key]);
}).reduce(function (object, icon) {
  object[icon.name] = icon;
  return object;
}, {}));

/***/ }),

/***/ "./package/src/index.js":
/*!******************************!*\
  !*** ./package/src/index.js ***!
  \******************************/
/*! exports provided: icons, replace */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _icons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./icons */ "./package/src/icons.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "icons", function() { return _icons__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _replace__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./replace */ "./package/src/replace.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "replace", function() { return _replace__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _animation_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./animation.scss */ "./package/src/animation.scss");
/* harmony import */ var _animation_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_animation_scss__WEBPACK_IMPORTED_MODULE_2__);
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */




if (typeof window !== 'undefined') {
  _animation_scss__WEBPACK_IMPORTED_MODULE_2___default.a._insertCss();
}



/***/ }),

/***/ "./package/src/replace.js":
/*!********************************!*\
  !*** ./package/src/replace.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames/dedupe */ "./node_modules/classnames/dedupe.js");
/* harmony import */ var classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames_dedupe__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./icons */ "./package/src/icons.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */


var animationKeys = {
  'data-eva-animation': 'type',
  'data-eva-hover': 'hover',
  'data-eva-infinite': 'infinite'
};
var dataAttributesKeys = {
  'data-eva': 'name',
  'data-eva-width': 'width',
  'data-eva-height': 'height',
  'data-eva-fill': 'fill'
};

function replace() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  if (typeof document === 'undefined') {
    throw new Error('`eva.replace()` only works in a browser environment.');
  }

  var elementsToReplace = document.querySelectorAll('[data-eva]');
  Array.from(elementsToReplace).forEach(function (element) {
    return replaceElement(element, options);
  });
}

function replaceElement(element) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var _getAttrs = getAttrs(element),
      name = _getAttrs.name,
      elementAttrs = _objectWithoutProperties(_getAttrs, ["name"]);

  var svgString = _icons__WEBPACK_IMPORTED_MODULE_1__["default"][name].toSvg(_objectSpread({}, options, elementAttrs, {
    animation: getAnimationObject(options.animation, elementAttrs.animation)
  }, {
    class: classnames_dedupe__WEBPACK_IMPORTED_MODULE_0___default()(options.class, elementAttrs.class)
  }));
  var svgDocument = new DOMParser().parseFromString(svgString, 'text/html');
  var svgElement = svgDocument.querySelector('.eva-hover') || svgDocument.querySelector('svg');
  element.parentNode.replaceChild(svgElement, element);
}

function getAttrs(element) {
  return Array.from(element.attributes).reduce(function (attrs, attr) {
    if (!!animationKeys[attr.name]) {
      attrs['animation'] = _objectSpread({}, attrs['animation'], _defineProperty({}, animationKeys[attr.name], attr.value));
    } else {
      attrs = _objectSpread({}, attrs, getAttr(attr));
    }

    return attrs;
  }, {});
}

function getAttr(attr) {
  if (!!dataAttributesKeys[attr.name]) {
    return _defineProperty({}, dataAttributesKeys[attr.name], attr.value);
  }

  return _defineProperty({}, attr.name, attr.value);
}

function getAnimationObject(optionsAnimation, elementAttrsAnimation) {
  if (optionsAnimation || elementAttrsAnimation) {
    return _objectSpread({}, optionsAnimation, elementAttrsAnimation);
  }

  return null;
}

/* harmony default export */ __webpack_exports__["default"] = (replace);

/***/ })

/******/ });
});
//# sourceMappingURL=eva.js.map

/***/ }),

/***/ "./node_modules/material-dynamic-table/fesm5/material-dynamic-table.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/material-dynamic-table/fesm5/material-dynamic-table.js ***!
  \*****************************************************************************/
/*! exports provided: CellDirective, CellService, ColumnConfig, ColumnFilter, ColumnFilterService, DynamicTableComponent, DynamicTableModule, ɵa, ɵb, ɵc */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CellDirective", function() { return CellDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CellService", function() { return CellService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColumnConfig", function() { return ColumnConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColumnFilter", function() { return ColumnFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ColumnFilterService", function() { return ColumnFilterService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DynamicTableComponent", function() { return DynamicTableComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DynamicTableModule", function() { return DynamicTableModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return TableCellComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return TextCellComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return DateCellComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/esm5/sort.es5.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/esm5/table.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");










/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ColumnFilter = /** @class */ (function () {
    function ColumnFilter() {
    }
    return ColumnFilter;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ColumnFilterService = /** @class */ (function () {
    function ColumnFilterService() {
        this.registeredFilters = {};
    }
    /**
     * @param {?} type
     * @param {?} component
     * @return {?}
     */
    ColumnFilterService.prototype.registerFilter = /**
     * @param {?} type
     * @param {?} component
     * @return {?}
     */
    function (type, component) {
        this.registeredFilters[type] = component;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    ColumnFilterService.prototype.getFilter = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        /** @type {?} */
        var component = this.registeredFilters[type];
        return component;
    };
    return ColumnFilterService;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DynamicTableComponent = /** @class */ (function () {
    function DynamicTableComponent(columnFilterService, dialog) {
        this.columnFilterService = columnFilterService;
        this.dialog = dialog;
        this.pageSize = 20;
        this.pageSizeOptions = [20, 50, 100];
        this.showFilters = true;
        this.stickyHeader = false;
        this.rowClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.appliedFilters = {};
    }
    /**
     * @return {?}
     */
    DynamicTableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.dataSource == null) {
            throw Error('DynamicTable must be provided with data source.');
        }
        if (this.columns == null) {
            throw Error('DynamicTable must be provided with column definitions.');
        }
        if (this.paginator === undefined) {
            this.paginator = this.internalPaginator;
        }
        this.columns.forEach((/**
         * @param {?} column
         * @param {?} index
         * @return {?}
         */
        function (column, index) { return column.name = _this.prepareColumnName(column.name, index); }));
        this.displayedColumns = this.columns.map((/**
         * @param {?} column
         * @param {?} index
         * @return {?}
         */
        function (column, index) { return column.name; }));
        /** @type {?} */
        var dataSource = (/** @type {?} */ (this.dataSource));
        dataSource.sort = this.sort;
        dataSource.paginator = this.paginator;
    };
    /**
     * @return {?}
     */
    DynamicTableComponent.prototype.isUsingInternalPaginator = /**
     * @return {?}
     */
    function () {
        return this.paginator === this.internalPaginator;
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DynamicTableComponent.prototype.canFilter = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        /** @type {?} */
        var filter = this.columnFilterService.getFilter(column.type);
        return filter != null;
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DynamicTableComponent.prototype.isFiltered = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        return this.appliedFilters[column.name];
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DynamicTableComponent.prototype.getFilterDescription = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        /** @type {?} */
        var filter = this.appliedFilters[column.name];
        if (!filter || !filter.getDescription) {
            return null;
        }
        return filter.getDescription();
    };
    /**
     * @param {?} name
     * @param {?} columnNumber
     * @return {?}
     */
    DynamicTableComponent.prototype.prepareColumnName = /**
     * @param {?} name
     * @param {?} columnNumber
     * @return {?}
     */
    function (name, columnNumber) {
        return name || 'col' + columnNumber;
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DynamicTableComponent.prototype.filter = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        var _this = this;
        /** @type {?} */
        var filter = this.columnFilterService.getFilter(column.type);
        if (filter) {
            /** @type {?} */
            var dialogConfig = new _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogConfig"]();
            /** @type {?} */
            var columnFilter = new ColumnFilter();
            columnFilter.column = column;
            if (this.appliedFilters[column.name]) {
                columnFilter.filter = Object.create(this.appliedFilters[column.name]);
            }
            dialogConfig.data = columnFilter;
            /** @type {?} */
            var dialogRef = this.dialog.open(filter, dialogConfig);
            dialogRef.afterClosed().subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                if (result) {
                    _this.appliedFilters[column.name] = result;
                }
                else if (result === '') {
                    delete _this.appliedFilters[column.name];
                }
                if (result || result === '') {
                    _this.updateDataSource();
                }
            }));
        }
    };
    /**
     * @return {?}
     */
    DynamicTableComponent.prototype.clearFilters = /**
     * @return {?}
     */
    function () {
        this.appliedFilters = {};
        this.updateDataSource();
    };
    /**
     * @protected
     * @return {?}
     */
    DynamicTableComponent.prototype.updateDataSource = /**
     * @protected
     * @return {?}
     */
    function () {
        /** @type {?} */
        var dataSource = (/** @type {?} */ (this.dataSource));
        dataSource.filters = this.getFilters();
    };
    /**
     * @return {?}
     */
    DynamicTableComponent.prototype.getFilters = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var filters = this.appliedFilters;
        /** @type {?} */
        var filterArray = Object.keys(filters).map((/**
         * @param {?} key
         * @return {?}
         */
        function (key) { return filters[key]; }));
        return filterArray;
    };
    /**
     * @param {?} columnName
     * @return {?}
     */
    DynamicTableComponent.prototype.getFilter = /**
     * @param {?} columnName
     * @return {?}
     */
    function (columnName) {
        /** @type {?} */
        var filterColumn = this.getColumnByName(columnName);
        if (!filterColumn) {
            throw Error("Column with name '" + columnName + "' does not exist.");
        }
        return this.appliedFilters[filterColumn.name];
    };
    /**
     * @param {?} columnName
     * @param {?} filter
     * @return {?}
     */
    DynamicTableComponent.prototype.setFilter = /**
     * @param {?} columnName
     * @param {?} filter
     * @return {?}
     */
    function (columnName, filter) {
        /** @type {?} */
        var filterColumn = this.getColumnByName(columnName);
        if (!filterColumn) {
            throw Error("Cannot set filter for a column. Column with name '" + columnName + "' does not exist.");
        }
        this.appliedFilters[filterColumn.name] = filter;
        this.updateDataSource();
    };
    /**
     * @private
     * @param {?} columnName
     * @return {?}
     */
    DynamicTableComponent.prototype.getColumnByName = /**
     * @private
     * @param {?} columnName
     * @return {?}
     */
    function (columnName) {
        return this.columns.find((/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            return (c.name ? c.name.toLowerCase() : c.name) === (columnName ? columnName.toLowerCase() : columnName);
        }));
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableComponent.prototype.onRowClick = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        this.rowClick.next(row);
    };
    DynamicTableComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'mdt-dynamic-table',
                    template: "<table mat-table [dataSource]=\"dataSource\"\r\n       matSort matSortDirection=\"asc\">\r\n\r\n  <ng-container *ngFor=\"let column of columns; let i = index\" matColumnDef=\"{{ column.name }}\"\r\n                [sticky]=\"column.sticky === 'start'\" [stickyEnd]=\"column.sticky === 'end'\">\r\n    <th mat-header-cell *matHeaderCellDef mat-sort-header=\"{{ column.name }}\" [disabled]=\"column.sort === false\">\r\n      {{ column.displayName }}\r\n      <button class=\"mat-sort-header-button\" *ngIf=\"showFilters && canFilter(column)\"\r\n              (click)=\"filter(column); $event.stopPropagation();\"\r\n              matTooltip=\"{{ getFilterDescription(column) }}\" matTooltipDisabled=\"{{ !getFilterDescription(column) }}\">\r\n        <mat-icon color=\"{{ isFiltered(column) ? 'primary' : 'default' }}\">filter_list</mat-icon>\r\n      </button>\r\n    </th>\r\n    <td mat-cell *matCellDef=\"let row\"><mdt-table-cell [row]=\"row\" [column]=\"column\"></mdt-table-cell></td>\r\n  </ng-container>\r\n\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: [stickyHeader]\"></tr>\r\n  <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\" (click)=\"onRowClick(row)\"></tr>\r\n</table>\r\n<mat-paginator [hidden]=\"!isUsingInternalPaginator()\" [pageSize]=\"pageSize\"\r\n               [pageSizeOptions]=\"pageSizeOptions\">\r\n</mat-paginator>",
                    styles: ["table{width:100%}th .mat-icon.mat-default{opacity:.54}th .mat-icon.mat-default:hover{opacity:1}[hidden]{display:none}"]
                }] }
    ];
    /** @nocollapse */
    DynamicTableComponent.ctorParameters = function () { return [
        { type: ColumnFilterService },
        { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"] }
    ]; };
    DynamicTableComponent.propDecorators = {
        columns: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        dataSource: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        pageSize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        pageSizeOptions: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        showFilters: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        stickyHeader: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        paginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        rowClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSort"], { static: true },] }],
        internalPaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], { static: true },] }]
    };
    return DynamicTableComponent;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CellDirective = /** @class */ (function () {
    function CellDirective(viewContainerRef) {
        this.viewContainerRef = viewContainerRef;
    }
    CellDirective.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                    selector: '[mdtCellHost]',
                },] }
    ];
    /** @nocollapse */
    CellDirective.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] }
    ]; };
    return CellDirective;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ColumnConfig = /** @class */ (function () {
    function ColumnConfig() {
    }
    return ColumnConfig;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TextCellComponent = /** @class */ (function () {
    function TextCellComponent() {
    }
    TextCellComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'mdt-text-cell',
                    template: '{{ row[column.name] }}'
                }] }
    ];
    TextCellComponent.propDecorators = {
        column: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        row: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
    };
    return TextCellComponent;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CellService = /** @class */ (function () {
    function CellService() {
        this.registeredCells = {};
    }
    /**
     * @param {?} type
     * @param {?} component
     * @return {?}
     */
    CellService.prototype.registerCell = /**
     * @param {?} type
     * @param {?} component
     * @return {?}
     */
    function (type, component) {
        this.registeredCells[type] = component;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    CellService.prototype.getCell = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        /** @type {?} */
        var component = this.registeredCells[type];
        if (component == null) {
            return TextCellComponent;
        }
        return component;
    };
    return CellService;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TableCellComponent = /** @class */ (function () {
    function TableCellComponent(cellService, componentFactoryResolver) {
        this.cellService = cellService;
        this.componentFactoryResolver = componentFactoryResolver;
    }
    /**
     * @return {?}
     */
    TableCellComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.initCell();
    };
    /**
     * @return {?}
     */
    TableCellComponent.prototype.initCell = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var cellComponent = this.cellService.getCell(this.column.type);
        /** @type {?} */
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(cellComponent);
        /** @type {?} */
        var viewContainerRef = this.cellHost.viewContainerRef;
        viewContainerRef.clear();
        /** @type {?} */
        var componentRef = viewContainerRef.createComponent(componentFactory);
        /** @type {?} */
        var cell = (/** @type {?} */ (componentRef.instance));
        cell.row = this.row;
        cell.column = this.column;
    };
    TableCellComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'mdt-table-cell',
                    template: '<ng-template mdtCellHost></ng-template>'
                }] }
    ];
    /** @nocollapse */
    TableCellComponent.ctorParameters = function () { return [
        { type: CellService },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"] }
    ]; };
    TableCellComponent.propDecorators = {
        cellHost: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: [CellDirective, { static: true },] }],
        row: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        column: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
    };
    return TableCellComponent;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DateCellComponent = /** @class */ (function () {
    function DateCellComponent() {
        this.dateFormat = 'short';
    }
    /**
     * @return {?}
     */
    DateCellComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.column.options) {
            if (this.column.options.dateFormat) {
                this.dateFormat = this.column.options.dateFormat;
            }
        }
    };
    DateCellComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'mdt-date-cell',
                    template: '{{ row[column.name] | date:dateFormat }}'
                }] }
    ];
    DateCellComponent.propDecorators = {
        column: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        row: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }]
    };
    return DateCellComponent;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var DynamicTableModule = /** @class */ (function () {
    function DynamicTableModule(cellService) {
        this.cellService = cellService;
        cellService.registerCell('string', TextCellComponent);
        cellService.registerCell('date', DateCellComponent);
    }
    DynamicTableModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [
                        _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"],
                        _angular_material_table__WEBPACK_IMPORTED_MODULE_6__["MatTableModule"],
                        _angular_material_sort__WEBPACK_IMPORTED_MODULE_1__["MatSortModule"],
                        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                        _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"],
                        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_8__["MatTooltipModule"]
                    ],
                    declarations: [
                        DynamicTableComponent,
                        TableCellComponent,
                        CellDirective,
                        TextCellComponent,
                        DateCellComponent
                    ],
                    exports: [DynamicTableComponent],
                    entryComponents: [
                        TextCellComponent,
                        DateCellComponent
                    ],
                    providers: [
                        CellService,
                        ColumnFilterService
                    ]
                },] }
    ];
    /** @nocollapse */
    DynamicTableModule.ctorParameters = function () { return [
        { type: CellService }
    ]; };
    return DynamicTableModule;
}());
if (false) {}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */


//# sourceMappingURL=material-dynamic-table.js.map


/***/ }),

/***/ "./node_modules/ng2-material-dropdown/fesm5/ng2-material-dropdown.js":
/*!***************************************************************************!*\
  !*** ./node_modules/ng2-material-dropdown/fesm5/ng2-material-dropdown.js ***!
  \***************************************************************************/
/*! exports provided: DropdownStateService, Ng2Dropdown, Ng2DropdownButton, Ng2DropdownMenu, Ng2DropdownModule, Ng2MenuItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownStateService", function() { return DropdownStateService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2Dropdown", function() { return Ng2Dropdown; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2DropdownButton", function() { return Ng2DropdownButton; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2DropdownMenu", function() { return Ng2DropdownMenu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2DropdownModule", function() { return Ng2DropdownModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2MenuItem", function() { return Ng2MenuItem; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");





var Ng2DropdownButton = /** @class */ (function () {
    function Ng2DropdownButton(element) {
        this.element = element;
        this.onMenuToggled = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.showCaret = true;
    }
    /**
     * @name toggleMenu
     * @desc emits event to toggle menu
     */
    Ng2DropdownButton.prototype.toggleMenu = function () {
        this.onMenuToggled.emit(true);
    };
    /**
     * @name getPosition
     * @desc returns position of the button
     */
    Ng2DropdownButton.prototype.getPosition = function () {
        return this.element.nativeElement.getBoundingClientRect();
    };
    Ng2DropdownButton.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], Ng2DropdownButton.prototype, "onMenuToggled", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownButton.prototype, "showCaret", void 0);
    Ng2DropdownButton = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ng2-dropdown-button',
            template: "<button class='ng2-dropdown-button' type=\"button\" (click)=\"toggleMenu()\" tabindex=\"0s\">\n    <span class=\"ng2-dropdown-button__label\">\n        <ng-content></ng-content>\n    </span>\n\n    <span class=\"ng2-dropdown-button__caret\" *ngIf=\"showCaret\">\n        <svg enable-background=\"new 0 0 32 32\" height=\"16px\" id=\"\u0421\u043B\u043E\u0439_1\" version=\"1.1\" viewBox=\"0 0 32 32\" width=\"16px\" xml:space=\"preserve\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><path d=\"M24.285,11.284L16,19.571l-8.285-8.288c-0.395-0.395-1.034-0.395-1.429,0  c-0.394,0.395-0.394,1.035,0,1.43l8.999,9.002l0,0l0,0c0.394,0.395,1.034,0.395,1.428,0l8.999-9.002  c0.394-0.395,0.394-1.036,0-1.431C25.319,10.889,24.679,10.889,24.285,11.284z\" fill=\"#121313\" id=\"Expand_More\"/><g/><g/><g/><g/><g/><g/></svg>\n    </span>\n</button>\n",
            styles: [".ng2-dropdown-button{font-family:Roboto,\"Helvetica Neue\",Helvetica,Arial;background:#fff;padding:.45rem .25rem;font-size:14px;letter-spacing:.08rem;color:#444;outline:0;cursor:pointer;font-weight:400;border:none;border-bottom:1px solid #efefef;text-align:left;min-width:100px;width:100%;display:flex;flex-direction:row;max-width:150px}.ng2-dropdown-button:hover{color:#222}.ng2-dropdown-button:active,.ng2-dropdown-button:focus{color:#222;border-bottom:2px solid #2196f3}.ng2-dropdown-button__label{flex:1 1 95%}.ng2-dropdown-button__caret{width:12px;height:12px;display:flex;flex:1 1 6%}:host-context(.ng2-dropdown-button--icon) .ng2-dropdown-button{border:none;min-width:40px;width:40px;border-radius:100%;transition:.2s;text-align:center;height:40px;padding:.5em}:host-context(.ng2-dropdown-button--icon) .ng2-dropdown-button:active{background:rgba(0,0,0,.2)}"]
        })
    ], Ng2DropdownButton);
    return Ng2DropdownButton;
}());

var _a;
var KEYS = {
    BACKSPACE: 9,
    PREV: 38,
    NEXT: 40,
    ENTER: 13,
    ESCAPE: 27
};
/**
 * @name onSwitchNext
 * @param index
 * @param items
 * @param state
 */
var onSwitchNext = function (index, items, state) {
    if (index < items.length - 1) {
        state.select(items[index + 1], true);
    }
};
var ɵ0 = onSwitchNext;
/**
 * @name onSwitchPrev
 * @param index
 * @param items
 * @param state
 */
var onSwitchPrev = function (index, items, state) {
    if (index > 0) {
        state.select(items[index - 1], true);
    }
};
var ɵ1 = onSwitchPrev;
/**
 * @name onBackspace
 * @param index
 * @param items
 * @param state
 */
var onBackspace = function (index, items, state) {
    if (index < items.length - 1) {
        state.select(items[index + 1], true);
    }
    else {
        state.select(items[0], true);
    }
};
var ɵ2 = onBackspace;
function onEscape() {
    this.hide();
}
;
/**
 * @name onItemClicked
 * @param index
 * @param items
 * @param state
 */
var onItemClicked = function (index, items, state) {
    return state.selectedItem ? state.selectedItem.click() : undefined;
};
var ɵ3 = onItemClicked;
var ACTIONS = (_a = {},
    _a[KEYS.BACKSPACE] = onBackspace,
    _a[KEYS.PREV] = onSwitchPrev,
    _a[KEYS.NEXT] = onSwitchNext,
    _a[KEYS.ENTER] = onItemClicked,
    _a[KEYS.ESCAPE] = onEscape,
    _a);
function arrowKeysHandler(event) {
    if ([38, 40].indexOf(event.keyCode) > -1) {
        event.preventDefault();
    }
}

var Ng2DropdownState = /** @class */ (function () {
    function Ng2DropdownState() {
        this.onItemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onItemClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onItemDestroyed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    Object.defineProperty(Ng2DropdownState.prototype, "selectedItem", {
        /**
         * @name selectedItem
         * @desc getter for _selectedItem
         */
        get: function () {
            return this._selectedItem;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name selects a menu item and emits event
     * @param item
     */
    Ng2DropdownState.prototype.select = function (item, dispatchEvent) {
        if (dispatchEvent === void 0) { dispatchEvent = true; }
        this._selectedItem = item;
        if (!dispatchEvent || !item) {
            return;
        }
        item.focus();
        this.onItemSelected.emit(item);
    };
    /**
     * @name unselect
     * @desc sets _selectedItem as undefined
     */
    Ng2DropdownState.prototype.unselect = function () {
        this._selectedItem = undefined;
    };
    return Ng2DropdownState;
}());

var DropdownStateService = /** @class */ (function () {
    function DropdownStateService() {
        this.menuState = {
            isVisible: false,
            toString: function () {
                return this.isVisible === true ? 'visible' : 'hidden';
            }
        };
        this.dropdownState = new Ng2DropdownState();
    }
    DropdownStateService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], DropdownStateService);
    return DropdownStateService;
}());

var Ng2MenuItem = /** @class */ (function () {
    function Ng2MenuItem(state, element) {
        this.state = state;
        this.element = element;
        /**
         * @preventClose
         * @desc if true, clicking on the item won't close the dropdown
         */
        this.preventClose = false;
    }
    Ng2MenuItem.prototype.ngOnDestroy = function () {
        this.state.dropdownState.onItemDestroyed.emit(this);
    };
    Object.defineProperty(Ng2MenuItem.prototype, "isSelected", {
        /**
         * @name isSelected
         * @desc returns current selected item
         */
        get: function () {
            return this === this.state.dropdownState.selectedItem;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name click
     * @desc emits select event
     */
    Ng2MenuItem.prototype.select = function ($event) {
        this.state.dropdownState.select(this, true);
        if ($event) {
            $event.stopPropagation();
            $event.preventDefault();
        }
    };
    /**
     * @name click
     * @desc emits click event
     */
    Ng2MenuItem.prototype.click = function () {
        this.state.dropdownState.onItemClicked.emit(this);
    };
    /**
     * @name focus
     */
    Ng2MenuItem.prototype.focus = function () {
        this.element.nativeElement.children[0].focus();
    };
    Ng2MenuItem.ctorParameters = function () { return [
        { type: DropdownStateService },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2MenuItem.prototype, "preventClose", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2MenuItem.prototype, "value", void 0);
    Ng2MenuItem = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ng2-menu-item',
            template: "\n        <div\n            class=\"ng2-menu-item\"\n            role=\"button\"\n            tabindex=\"0\"\n            [class.ng2-menu-item--selected]=\"isSelected\"\n            (keydown.enter)=\"click()\"\n            (click)=\"click()\"\n            (mouseover)=\"select()\"\n        >\n            <ng-content></ng-content>\n        </div>\n    ",
            styles: [".ng2-menu-item{font-family:Roboto,\"Helvetica Neue\",Helvetica,Arial;background:#fff;color:rgba(0,0,0,.87);cursor:pointer;font-size:.9em;text-transform:none;font-weight:400;letter-spacing:.03em;height:48px;line-height:48px;padding:.3em 1.25rem;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;transition:background .25s}.ng2-menu-item--selected{background:rgba(158,158,158,.2);outline:0}.ng2-menu-item:focus{outline:0}.ng2-menu-item:active{background:rgba(158,158,158,.4)}:host(ng2-menu-item) ::ng-deep [ng2-menu-item-icon]{vertical-align:middle;font-size:28px;width:1.5em;height:30px;color:rgba(0,0,0,.44)}"]
        })
    ], Ng2MenuItem);
    return Ng2MenuItem;
}());

var Ng2DropdownMenu = /** @class */ (function () {
    function Ng2DropdownMenu(dropdownState, element, renderer) {
        this.dropdownState = dropdownState;
        this.element = element;
        this.renderer = renderer;
        /**
         * @name width
         */
        this.width = 4;
        /**
         * @description if set to true, the first element of the dropdown will be automatically focused
         * @name focusFirstElement
         */
        this.focusFirstElement = true;
        /**
         * @name appendToBody
         */
        this.appendToBody = true;
        /**
         * @name zIndex
         */
        this.zIndex = 1000;
        this.listeners = {
            arrowHandler: undefined,
            handleKeypress: undefined
        };
    }
    /**
     * @name show
     * @shows menu and selects first item
     */
    Ng2DropdownMenu.prototype.show = function (position, dynamic) {
        if (dynamic === void 0) { dynamic = true; }
        var dc = typeof document !== 'undefined' ? document : undefined;
        var wd = typeof window !== 'undefined' ? window : undefined;
        if (!this.dropdownState.menuState.isVisible) {
            // setting handlers
            this.listeners.handleKeypress = this.renderer.listen(dc.body, 'keydown', this.handleKeypress.bind(this));
            this.listeners.arrowHandler = this.renderer.listen(wd, 'keydown', arrowKeysHandler);
        }
        // update state
        this.dropdownState.menuState.isVisible = true;
        if (position) {
            this.updatePosition(position, dynamic);
        }
    };
    /**
     * @name hide
     * @desc hides menu
     */
    Ng2DropdownMenu.prototype.hide = function () {
        this.dropdownState.menuState.isVisible = false;
        // reset selected item state
        this.dropdownState.dropdownState.unselect();
        // call function to unlisten
        this.listeners.arrowHandler && this.listeners.arrowHandler();
        this.listeners.handleKeypress && this.listeners.handleKeypress();
    };
    /**
     * @name updatePosition
     * @desc updates the menu position every time it is toggled
     * @param position {ClientRect}
     * @param dynamic {boolean}
     */
    Ng2DropdownMenu.prototype.updatePosition = function (position, dynamic) {
        this.position = position;
        this.updateOnChange(dynamic);
    };
    /**
     * @name handleKeypress
     * @desc executes functions on keyPress based on the key pressed
     * @param $event
     */
    Ng2DropdownMenu.prototype.handleKeypress = function ($event) {
        var key = $event.keyCode;
        var items = this.items.toArray();
        var index = items.indexOf(this.dropdownState.dropdownState.selectedItem);
        if (!ACTIONS.hasOwnProperty(key)) {
            return;
        }
        ACTIONS[key].call(this, index, items, this.dropdownState.dropdownState);
    };
    /**
     * @name getMenuElement
     */
    Ng2DropdownMenu.prototype.getMenuElement = function () {
        return this.element.nativeElement.children[0];
    };
    /**
     * @name calcPositionOffset
     * @param position
     */
    Ng2DropdownMenu.prototype.calcPositionOffset = function (position) {
        var wd = typeof window !== 'undefined' ? window : undefined;
        var dc = typeof document !== 'undefined' ? document : undefined;
        if (!wd || !dc || !position) {
            return;
        }
        var element = this.getMenuElement();
        var supportPageOffset = wd.pageXOffset !== undefined;
        var isCSS1Compat = (dc.compatMode || '') === 'CSS1Compat';
        var x = supportPageOffset
            ? wd.pageXOffset
            : isCSS1Compat
                ? dc.documentElement.scrollLeft
                : dc.body.scrollLeft;
        var y = supportPageOffset
            ? wd.pageYOffset
            : isCSS1Compat
                ? dc.documentElement.scrollTop
                : dc.body.scrollTop;
        var _a = this.applyOffset(position.top + (this.appendToBody ? y - 15 : 0) + "px", position.left + x - 5 + "px"), top = _a.top, left = _a.left;
        var clientWidth = element.clientWidth;
        var clientHeight = element.clientHeight;
        var marginFromBottom = parseInt(top) + clientHeight + (this.appendToBody ? 0 : y - 15);
        var marginFromRight = parseInt(left) + clientWidth;
        var windowScrollHeight = wd.innerHeight + wd.scrollY;
        var windowScrollWidth = wd.innerWidth + wd.scrollX;
        if (marginFromBottom >= windowScrollHeight) {
            top = parseInt(top.replace('px', '')) - clientHeight + "px";
        }
        if (marginFromRight >= windowScrollWidth) {
            var marginRight = marginFromRight - windowScrollWidth + 30;
            left = parseInt(left.replace('px', '')) - marginRight + "px";
        }
        return { top: top, left: left };
    };
    Ng2DropdownMenu.prototype.applyOffset = function (top, left) {
        if (!this.offset) {
            return { top: top, left: left };
        }
        var offset = this.offset.split(' ');
        if (!offset[1]) {
            offset[1] = '0';
        }
        top = parseInt(top.replace('px', '')) + parseInt(offset[0]) + "px";
        left = parseInt(left.replace('px', '')) + parseInt(offset[1]) + "px";
        return { top: top, left: left };
    };
    Ng2DropdownMenu.prototype.ngOnInit = function () {
        var dc = typeof document !== 'undefined' ? document : undefined;
        if (this.appendToBody) {
            // append menu element to the body
            dc.body.appendChild(this.element.nativeElement);
        }
    };
    Ng2DropdownMenu.prototype.updateOnChange = function (dynamic) {
        if (dynamic === void 0) { dynamic = true; }
        var element = this.getMenuElement();
        var position = this.calcPositionOffset(this.position);
        if (position) {
            this.renderer.setStyle(element, 'top', position.top.toString());
            this.renderer.setStyle(element, 'left', position.left.toString());
        }
        // select first item unless user disabled this option
        if (this.focusFirstElement &&
            this.items.first &&
            !this.dropdownState.dropdownState.selectedItem) {
            this.dropdownState.dropdownState.select(this.items.first, false);
        }
    };
    Ng2DropdownMenu.prototype.ngOnDestroy = function () {
        var elem = this.element.nativeElement;
        elem.parentNode.removeChild(elem);
        if (this.listeners.handleKeypress) {
            this.listeners.handleKeypress();
        }
    };
    Ng2DropdownMenu.ctorParameters = function () { return [
        { type: DropdownStateService },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownMenu.prototype, "width", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownMenu.prototype, "focusFirstElement", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownMenu.prototype, "offset", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownMenu.prototype, "appendToBody", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2DropdownMenu.prototype, "zIndex", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"])(Ng2MenuItem, { descendants: true })
    ], Ng2DropdownMenu.prototype, "items", void 0);
    Ng2DropdownMenu = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ng2-dropdown-menu',
            template: "\n        <!-- MENU -->\n        <div\n            class=\"ng2-dropdown-menu ng2-dropdown-menu---width--{{ width }}\"\n            [class.ng2-dropdown-menu--inside-element]=\"!appendToBody\"\n            [class.ng2-dropdown-menu--open]=\"dropdownState.menuState.isVisible\"\n            [style.z-index]=\"zIndex\"\n            [@fade]=\"dropdownState.menuState.toString()\"\n        >\n            <div\n                class=\"ng2-dropdown-menu__options-container\"\n                [@opacity]=\"dropdownState.menuState.toString()\"\n            >\n                <ng-content></ng-content>\n            </div>\n        </div>\n\n        <!-- BACKDROP -->\n        <div\n            class=\"ng2-dropdown-backdrop\"\n            *ngIf=\"dropdownState.menuState.isVisible\"\n            (click)=\"hide()\"\n        ></div>\n    ",
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('fade', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["state"])('visible', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, height: '*', width: '*' })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["state"])('hidden', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, overflow: 'hidden', height: 0, width: 0 })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('hidden => visible', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('250ms ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, height: '*', width: '*' }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('visible => hidden', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('350ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, width: 0, height: 0 }))
                    ])
                ]),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('opacity', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('hidden => visible', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('450ms ease-in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["keyframes"])([
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, offset: 0 }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, offset: 1 })
                        ]))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('visible => hidden', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('250ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["keyframes"])([
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, offset: 0 }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0.5, offset: 0.3 }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, offset: 1 })
                        ]))
                    ])
                ])
            ],
            styles: [":host{display:block}.ng2-dropdown-menu{overflow-y:auto;box-shadow:0 1px 2px 0 rgba(0,0,0,.3);position:absolute;padding:.5em 0;background:#fff;border-radius:1px;max-height:400px;width:260px;min-height:0;display:block}.ng2-dropdown-menu.ng2-dropdown-menu--inside-element{position:fixed}.ng2-dropdown-menu.ng2-dropdown-menu--width--2{width:200px}.ng2-dropdown-menu.ng2-dropdown-menu--width--4{width:260px}.ng2-dropdown-menu.ng2-dropdown-menu--width--6{width:320px}.ng2-dropdown-backdrop{position:fixed;top:0;left:0;width:100%;height:100%;z-index:1;overflow:hidden}:host ::ng-deep .ng2-menu-divider{height:1px;min-height:1px;max-height:1px;width:100%;display:block;background:#f9f9f9}"]
        })
    ], Ng2DropdownMenu);
    return Ng2DropdownMenu;
}());

var Ng2Dropdown = /** @class */ (function () {
    function Ng2Dropdown(state) {
        this.state = state;
        this.dynamicUpdate = true;
        // outputs
        this.onItemClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onItemSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onShow = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onHide = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    Ng2Dropdown.prototype.ngOnInit = function () {
        var _this = this;
        this.state.dropdownState.onItemClicked.subscribe(function (item) {
            _this.onItemClicked.emit(item);
            if (item.preventClose) {
                return;
            }
            _this.hide.call(_this);
        });
        if (this.button) {
            this.button.onMenuToggled.subscribe(function () {
                _this.toggleMenu();
            });
        }
        this.state.dropdownState.onItemSelected.subscribe(function (item) {
            _this.onItemSelected.emit(item);
        });
        this.state.dropdownState.onItemDestroyed.subscribe(function (item) {
            var newSelectedItem;
            var items = _this.menu.items.toArray();
            if (item !== _this.state.dropdownState.selectedItem) {
                return;
            }
            if (_this.menu.focusFirstElement) {
                newSelectedItem =
                    item === items[0] && items.length > 1
                        ? items[1]
                        : items[0];
            }
            _this.state.dropdownState.select(newSelectedItem);
        });
    };
    /**
     * @name toggleMenu
     * @desc toggles menu visibility
     */
    Ng2Dropdown.prototype.toggleMenu = function (position) {
        if (position === void 0) { position = this.button.getPosition(); }
        this.state.menuState.isVisible ? this.hide() : this.show(position);
    };
    /**
     * - hides dropdown
     * @name hide
     */
    Ng2Dropdown.prototype.hide = function () {
        this.menu.hide();
        this.onHide.emit(this);
    };
    /**
     * - shows dropdown
     * @name show
     * @param position
     */
    Ng2Dropdown.prototype.show = function (position) {
        if (position === void 0) { position = this.button.getPosition(); }
        this.menu.show(position, this.dynamicUpdate);
        this.onShow.emit(this);
    };
    /**
     * @name scrollListener
     */
    Ng2Dropdown.prototype.scrollListener = function () {
        if (this.button && this.dynamicUpdate) {
            this.menu.updatePosition(this.button.getPosition(), true);
        }
    };
    Ng2Dropdown.ctorParameters = function () { return [
        { type: DropdownStateService }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])(Ng2DropdownButton, { static: true })
    ], Ng2Dropdown.prototype, "button", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])(Ng2DropdownMenu, { static: true })
    ], Ng2Dropdown.prototype, "menu", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], Ng2Dropdown.prototype, "dynamicUpdate", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], Ng2Dropdown.prototype, "onItemClicked", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], Ng2Dropdown.prototype, "onItemSelected", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], Ng2Dropdown.prototype, "onShow", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], Ng2Dropdown.prototype, "onHide", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll')
    ], Ng2Dropdown.prototype, "scrollListener", null);
    Ng2Dropdown = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ng2-dropdown',
            template: "\n        <div class=\"ng2-dropdown-container\">\n            <ng-content select=\"ng2-dropdown-button\"></ng-content>\n            <ng-content select=\"ng2-dropdown-menu\"></ng-content>\n        </div>\n    ",
            providers: [DropdownStateService]
        })
    ], Ng2Dropdown);
    return Ng2Dropdown;
}());

var Ng2DropdownModule = /** @class */ (function () {
    function Ng2DropdownModule() {
    }
    Ng2DropdownModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            exports: [
                Ng2MenuItem,
                Ng2DropdownButton,
                Ng2DropdownMenu,
                Ng2Dropdown
            ],
            declarations: [
                Ng2Dropdown,
                Ng2MenuItem,
                Ng2DropdownButton,
                Ng2DropdownMenu,
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]
            ]
        })
    ], Ng2DropdownModule);
    return Ng2DropdownModule;
}());

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=ng2-material-dropdown.js.map


/***/ }),

/***/ "./node_modules/ngx-chips/fesm5/ngx-chips.js":
/*!***************************************************!*\
  !*** ./node_modules/ngx-chips/fesm5/ngx-chips.js ***!
  \***************************************************/
/*! exports provided: DeleteIconComponent, TagComponent, TagInputComponent, TagInputDropdown, TagInputForm, TagInputModule, TagRipple, ɵa, ɵb, ɵc, ɵd */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteIconComponent", function() { return DeleteIconComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagComponent", function() { return TagComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagInputComponent", function() { return TagInputComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagInputDropdown", function() { return TagInputDropdown; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagInputForm", function() { return TagInputForm; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagInputModule", function() { return TagInputModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TagRipple", function() { return TagRipple; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return TagInputAccessor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return animations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return DragProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵd", function() { return HighlightPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ng2_material_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-material-dropdown */ "./node_modules/ng2-material-dropdown/fesm5/ng2-material-dropdown.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");








var escape = function (s) { return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'); };
var ɵ0 = escape;
var HighlightPipe = /** @class */ (function () {
    function HighlightPipe() {
    }
    /**
     * @name transform
     * @param value {string}
     * @param arg {string}
     */
    HighlightPipe.prototype.transform = function (value, arg) {
        if (!arg.trim()) {
            return value;
        }
        try {
            var regex = new RegExp("(" + escape(arg) + ")", 'i');
            return value.replace(regex, '<b>$1</b>');
        }
        catch (e) {
            return value;
        }
    };
    HighlightPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'highlight'
        })
    ], HighlightPipe);
    return HighlightPipe;
}());

/*
** constants and default values for <tag-input>
 */
var PLACEHOLDER = '+ Tag';
var SECONDARY_PLACEHOLDER = 'Enter a new tag';
var KEYDOWN = 'keydown';
var KEYUP = 'keyup';
var FOCUS = 'focus';
var MAX_ITEMS_WARNING = 'The number of items specified was greater than the property max-items.';
var ACTIONS_KEYS = {
    DELETE: 'DELETE',
    SWITCH_PREV: 'SWITCH_PREV',
    SWITCH_NEXT: 'SWITCH_NEXT',
    TAB: 'TAB'
};
var KEY_PRESS_ACTIONS = {
    8: ACTIONS_KEYS.DELETE,
    46: ACTIONS_KEYS.DELETE,
    37: ACTIONS_KEYS.SWITCH_PREV,
    39: ACTIONS_KEYS.SWITCH_NEXT,
    9: ACTIONS_KEYS.TAB
};
var DRAG_AND_DROP_KEY = 'Text';
var NEXT = 'NEXT';
var PREV = 'PREV';

var DragProvider = /** @class */ (function () {
    function DragProvider() {
        this.state = {
            dragging: false,
            dropping: false,
            index: undefined
        };
    }
    /**
     * @name setDraggedItem
     * @param event
     * @param tag
     */
    DragProvider.prototype.setDraggedItem = function (event, tag) {
        if (event && event.dataTransfer) {
            event.dataTransfer.setData(DRAG_AND_DROP_KEY, JSON.stringify(tag));
        }
    };
    /**
     * @name getDraggedItem
     * @param event
     */
    DragProvider.prototype.getDraggedItem = function (event) {
        if (event && event.dataTransfer) {
            var data = event.dataTransfer.getData(DRAG_AND_DROP_KEY);
            try {
                return JSON.parse(data);
            }
            catch (_a) {
                return;
            }
        }
    };
    /**
     * @name setSender
     * @param sender
     */
    DragProvider.prototype.setSender = function (sender) {
        this.sender = sender;
    };
    /**
     * @name setReceiver
     * @param receiver
     */
    DragProvider.prototype.setReceiver = function (receiver) {
        this.receiver = receiver;
    };
    /**
     * @name onTagDropped
     * @param tag
     * @param indexDragged
     * @param indexDropped
     */
    DragProvider.prototype.onTagDropped = function (tag, indexDragged, indexDropped) {
        this.onDragEnd();
        this.sender.onRemoveRequested(tag, indexDragged);
        this.receiver.onAddingRequested(false, tag, indexDropped);
    };
    /**
     * @name setState
     * @param state
     */
    DragProvider.prototype.setState = function (state) {
        this.state = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, this.state), state);
    };
    /**
     * @name getState
     * @param key
     */
    DragProvider.prototype.getState = function (key) {
        return key ? this.state[key] : this.state;
    };
    /**
     * @name onDragEnd
     */
    DragProvider.prototype.onDragEnd = function () {
        this.setState({
            dragging: false,
            dropping: false,
            index: undefined
        });
    };
    DragProvider = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], DragProvider);
    return DragProvider;
}());

var defaults = {
    tagInput: {
        separatorKeys: [],
        separatorKeyCodes: [],
        maxItems: Infinity,
        placeholder: PLACEHOLDER,
        secondaryPlaceholder: SECONDARY_PLACEHOLDER,
        validators: [],
        asyncValidators: [],
        onlyFromAutocomplete: false,
        errorMessages: {},
        theme: '',
        onTextChangeDebounce: 250,
        inputId: null,
        inputClass: '',
        clearOnBlur: false,
        hideForm: false,
        addOnBlur: false,
        addOnPaste: false,
        pasteSplitPattern: ',',
        blinkIfDupe: true,
        removable: true,
        editable: false,
        allowDupes: false,
        modelAsStrings: false,
        trimTags: true,
        ripple: true,
        tabIndex: '',
        disable: false,
        dragZone: '',
        onRemoving: undefined,
        onAdding: undefined,
        displayBy: 'display',
        identifyBy: 'value',
        animationDuration: {
            enter: '250ms',
            leave: '150ms'
        }
    },
    dropdown: {
        displayBy: 'display',
        identifyBy: 'value',
        appendToBody: true,
        offset: '50 0',
        focusFirstElement: false,
        showDropdownIfEmpty: false,
        minimumTextLength: 1,
        limitItemsTo: Infinity,
        keepOpen: true,
        dynamicUpdate: true,
        zIndex: 1000,
        matchingFn: matchingFn
    }
};
/**
 * @name matchingFn
 * @param this
 * @param value
 * @param target
 */
function matchingFn(value, target) {
    var targetValue = target[this.displayBy].toString();
    return targetValue && targetValue
        .toLowerCase()
        .indexOf(value.toLowerCase()) >= 0;
}

var OptionsProvider = /** @class */ (function () {
    function OptionsProvider() {
    }
    OptionsProvider.prototype.setOptions = function (options) {
        OptionsProvider.defaults.tagInput = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, defaults.tagInput), options.tagInput);
        OptionsProvider.defaults.dropdown = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, defaults.dropdown), options.dropdown);
    };
    OptionsProvider.defaults = defaults;
    return OptionsProvider;
}());

var TagModelClass = /** @class */ (function () {
    function TagModelClass() {
    }
    return TagModelClass;
}());
function isObject(obj) {
    return obj === Object(obj);
}
var TagInputAccessor = /** @class */ (function () {
    function TagInputAccessor() {
        this._items = [];
        /**
         * @name displayBy
         */
        this.displayBy = OptionsProvider.defaults.tagInput.displayBy;
        /**
         * @name identifyBy
         */
        this.identifyBy = OptionsProvider.defaults.tagInput.identifyBy;
    }
    Object.defineProperty(TagInputAccessor.prototype, "items", {
        get: function () {
            return this._items;
        },
        set: function (items) {
            this._items = items;
            this._onChangeCallback(this._items);
        },
        enumerable: true,
        configurable: true
    });
    TagInputAccessor.prototype.onTouched = function () {
        this._onTouchedCallback();
    };
    TagInputAccessor.prototype.writeValue = function (items) {
        this._items = items || [];
    };
    TagInputAccessor.prototype.registerOnChange = function (fn) {
        this._onChangeCallback = fn;
    };
    TagInputAccessor.prototype.registerOnTouched = function (fn) {
        this._onTouchedCallback = fn;
    };
    /**
     * @name getItemValue
     * @param item
     * @param fromDropdown
     */
    TagInputAccessor.prototype.getItemValue = function (item, fromDropdown) {
        if (fromDropdown === void 0) { fromDropdown = false; }
        var property = fromDropdown && this.dropdown ? this.dropdown.identifyBy : this.identifyBy;
        return isObject(item) ? item[property] : item;
    };
    /**
     * @name getItemDisplay
     * @param item
     * @param fromDropdown
     */
    TagInputAccessor.prototype.getItemDisplay = function (item, fromDropdown) {
        if (fromDropdown === void 0) { fromDropdown = false; }
        var property = fromDropdown && this.dropdown ? this.dropdown.displayBy : this.displayBy;
        return isObject(item) ? item[property] : item;
    };
    /**
     * @name getItemsWithout
     * @param index
     */
    TagInputAccessor.prototype.getItemsWithout = function (index) {
        return this.items.filter(function (item, position) { return position !== index; });
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputAccessor.prototype, "displayBy", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputAccessor.prototype, "identifyBy", void 0);
    TagInputAccessor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])()
    ], TagInputAccessor);
    return TagInputAccessor;
}());

/**
 * @name listen
 * @param listenerType
 * @param action
 * @param condition
 */
function listen(listenerType, action, condition) {
    if (condition === void 0) { condition = true; }
    // if the event provided does not exist, throw an error
    if (!this.listeners.hasOwnProperty(listenerType)) {
        throw new Error('The event entered may be wrong');
    }
    // if a condition is present and is false, exit early
    if (!condition) {
        return;
    }
    // fire listener
    this.listeners[listenerType].push(action);
}

var TagInputForm = /** @class */ (function () {
    function TagInputForm() {
        /**
         * @name onSubmit
         */
        this.onSubmit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onBlur
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onFocus
         */
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onKeyup
         */
        this.onKeyup = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onKeydown
         */
        this.onKeydown = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name inputTextChange
         */
        this.inputTextChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name validators
         */
        this.validators = [];
        /**
         * @name asyncValidators
         * @desc array of AsyncValidator that are used to validate the tag before it gets appended to the list
         */
        this.asyncValidators = [];
        /**
         * @name tabindex
         * @desc pass through the specified tabindex to the input
         */
        this.tabindex = '';
        /**
         * @name disabled
         */
        this.disabled = false;
        this.item = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]({ value: '', disabled: this.disabled });
    }
    Object.defineProperty(TagInputForm.prototype, "inputText", {
        /**
         * @name inputText
         */
        get: function () {
            return this.item.value;
        },
        /**
         * @name inputText
         * @param text {string}
         */
        set: function (text) {
            this.item.setValue(text);
            this.inputTextChange.emit(text);
        },
        enumerable: true,
        configurable: true
    });
    TagInputForm.prototype.ngOnInit = function () {
        this.item.setValidators(this.validators);
        this.item.setAsyncValidators(this.asyncValidators);
        // creating form
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            item: this.item
        });
    };
    TagInputForm.prototype.ngOnChanges = function (changes) {
        if (changes.disabled && !changes.disabled.firstChange) {
            if (changes.disabled.currentValue) {
                this.form.controls['item'].disable();
            }
            else {
                this.form.controls['item'].enable();
            }
        }
    };
    Object.defineProperty(TagInputForm.prototype, "value", {
        /**
         * @name value
         */
        get: function () {
            return this.form.get('item');
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name isInputFocused
     */
    TagInputForm.prototype.isInputFocused = function () {
        var doc = typeof document !== 'undefined' ? document : undefined;
        return doc ? doc.activeElement === this.input.nativeElement : false;
    };
    /**
     * @name getErrorMessages
     * @param messages
     */
    TagInputForm.prototype.getErrorMessages = function (messages) {
        var _this = this;
        return Object.keys(messages)
            .filter(function (err) { return _this.value.hasError(err); })
            .map(function (err) { return messages[err]; });
    };
    /**
     * @name hasErrors
     */
    TagInputForm.prototype.hasErrors = function () {
        var _a = this.form, dirty = _a.dirty, value = _a.value, valid = _a.valid;
        return dirty && value.item && !valid;
    };
    /**
     * @name focus
     */
    TagInputForm.prototype.focus = function () {
        this.input.nativeElement.focus();
    };
    /**
     * @name blur
     */
    TagInputForm.prototype.blur = function () {
        this.input.nativeElement.blur();
    };
    /**
     * @name getElementPosition
     */
    TagInputForm.prototype.getElementPosition = function () {
        return this.input.nativeElement.getBoundingClientRect();
    };
    /**
     * - removes input from the component
     * @name destroy
     */
    TagInputForm.prototype.destroy = function () {
        var input = this.input.nativeElement;
        input.parentElement.removeChild(input);
    };
    /**
     * @name onKeyDown
     * @param $event
     */
    TagInputForm.prototype.onKeyDown = function ($event) {
        this.inputText = this.value.value;
        if ($event.key === 'Enter') {
            this.submit($event);
        }
        else {
            return this.onKeydown.emit($event);
        }
    };
    /**
     * @name onKeyUp
     * @param $event
     */
    TagInputForm.prototype.onKeyUp = function ($event) {
        this.inputText = this.value.value;
        return this.onKeyup.emit($event);
    };
    /**
     * @name submit
     */
    TagInputForm.prototype.submit = function ($event) {
        $event.preventDefault();
        this.onSubmit.emit($event);
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "onSubmit", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "onBlur", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "onFocus", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "onKeyup", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "onKeydown", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputForm.prototype, "inputTextChange", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "placeholder", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "validators", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "asyncValidators", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "inputId", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "inputClass", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "tabindex", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "disabled", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('input')
    ], TagInputForm.prototype, "input", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputForm.prototype, "inputText", null);
    TagInputForm = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tag-input-form',
            template: "<!-- form -->\n<form (ngSubmit)=\"submit($event)\" [formGroup]=\"form\">\n    <input #input\n\n           type=\"text\"\n           class=\"ng2-tag-input__text-input\"\n           autocomplete=\"off\"\n           tabindex=\"{{ disabled ? -1 : tabindex ? tabindex : 0 }}\"\n           minlength=\"1\"\n           formControlName=\"item\"\n\n           [ngClass]=\"inputClass\"\n           [attr.id]=\"inputId\"\n           [attr.placeholder]=\"placeholder\"\n           [attr.aria-label]=\"placeholder\"\n           [attr.tabindex]=\"tabindex\"\n           [attr.disabled]=\"disabled ? disabled : null\"\n\n           (focus)=\"onFocus.emit($event)\"\n           (blur)=\"onBlur.emit($event)\"\n           (keydown)=\"onKeyDown($event)\"\n           (keyup)=\"onKeyUp($event)\"\n    />\n</form>\n",
            styles: [".dark tag:focus{box-shadow:0 0 0 1px #323232}.ng2-tag-input.bootstrap3-info{background-color:#fff;display:inline-block;color:#555;vertical-align:middle;max-width:100%;height:42px;line-height:44px}.ng2-tag-input.bootstrap3-info input{border:none;box-shadow:none;outline:0;background-color:transparent;padding:0 6px;margin:0;width:auto;max-width:inherit}.ng2-tag-input.bootstrap3-info .form-control input::-moz-placeholder{color:#777;opacity:1}.ng2-tag-input.bootstrap3-info .form-control input:-ms-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info .form-control input::-webkit-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info input:focus{border:none;box-shadow:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--focused{box-shadow:inset 0 1px 1px rgba(0,0,0,.4);border:1px solid #ccc}.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;transition:.25s;padding:.25rem 0;min-height:32px;cursor:text;border-bottom:2px solid #efefef}.ng2-tag-input:focus{outline:0}.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #2196f3}.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #f44336}.ng2-tag-input.ng2-tag-input--loading{border:none}.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.ng2-tag-input form{margin:.1em 0}.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.minimal.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:1px solid transparent}.minimal.ng2-tag-input:focus{outline:0}.minimal.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.minimal.ng2-tag-input.ng2-tag-input--loading{border:none}.minimal.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.minimal.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.dark.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #444}.dark.ng2-tag-input:focus{outline:0}.dark.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.dark.ng2-tag-input.ng2-tag-input--loading{border:none}.dark.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.dark.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #efefef}.bootstrap.ng2-tag-input:focus{outline:0}.bootstrap.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #0275d8}.bootstrap.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #d9534f}.bootstrap.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap3-info.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;padding:4px;cursor:text;box-shadow:inset 0 1px 1px rgba(0,0,0,.075);border-radius:4px}.bootstrap3-info.ng2-tag-input:focus{outline:0}.bootstrap3-info.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap3-info.ng2-tag-input.ng2-tag-input--invalid{box-shadow:inset 0 1px 1px #d9534f;border-bottom:1px solid #d9534f}.bootstrap3-info.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap3-info.ng2-tag-input form{margin:.1em 0}.bootstrap3-info.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.error-message{font-size:.8em;color:#f44336;margin:.5em 0 0}.bootstrap .error-message{color:#d9534f}.ng2-tag-input__text-input{display:inline;vertical-align:middle;border:none;padding:0 .5rem;height:38px;font-size:1em;font-family:Roboto,\"Helvetica Neue\",sans-serif}.ng2-tag-input__text-input:focus{outline:0}.ng2-tag-input__text-input[disabled=true]{opacity:.5;background:#fff}"]
        })
    ], TagInputForm);
    return TagInputForm;
}());

var TagRipple = /** @class */ (function () {
    function TagRipple() {
        this.state = 'none';
    }
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagRipple.prototype, "state", void 0);
    TagRipple = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tag-ripple',
            template: "\n        <div class=\"tag-ripple\" [@ink]=\"state\"></div>\n    ",
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('ink', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["state"])('none', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ width: 0, opacity: 0 })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])('none => clicked', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])(300, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["keyframes"])([
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, offset: 0, width: '30%', borderRadius: '100%' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, offset: 0.5, width: '50%' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0.5, offset: 1, width: '100%', borderRadius: '16px' })
                        ]))
                    ])
                ])
            ],
            styles: ["\n        :host {\n            width: 100%;\n            height: 100%;\n            left: 0;\n            overflow: hidden;\n            position: absolute;\n        }\n\n        .tag-ripple {\n            background: rgba(0, 0, 0, 0.1);\n            top: 50%;\n            left: 50%;\n            height: 100%;\n            transform: translate(-50%, -50%);\n            position: absolute;\n        }\n    "]
        })
    ], TagRipple);
    return TagRipple;
}());

// mocking navigator
var navigator = typeof window !== 'undefined' ? window.navigator : {
    userAgent: 'Chrome',
    vendor: 'Google Inc'
};
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
var TagComponent = /** @class */ (function () {
    function TagComponent(element, renderer, cdRef) {
        this.element = element;
        this.renderer = renderer;
        this.cdRef = cdRef;
        /**
         * @name disabled
         */
        this.disabled = false;
        /**
         * @name onSelect
         */
        this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onRemove
         */
        this.onRemove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onBlur
         */
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onKeyDown
         */
        this.onKeyDown = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onTagEdited
         */
        this.onTagEdited = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name editing
         */
        this.editing = false;
        /**
         * @name rippleState
         */
        this.rippleState = 'none';
    }
    Object.defineProperty(TagComponent.prototype, "readonly", {
        /**
         * @name readonly {boolean}
         */
        get: function () {
            return typeof this.model !== 'string' && this.model.readonly === true;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name select
     */
    TagComponent.prototype.select = function ($event) {
        if (this.readonly || this.disabled) {
            return;
        }
        if ($event) {
            $event.stopPropagation();
        }
        this.focus();
        this.onSelect.emit(this.model);
    };
    /**
     * @name remove
     */
    TagComponent.prototype.remove = function ($event) {
        $event.stopPropagation();
        this.onRemove.emit(this);
    };
    /**
     * @name focus
     */
    TagComponent.prototype.focus = function () {
        this.element.nativeElement.focus();
    };
    TagComponent.prototype.move = function () {
        this.moving = true;
    };
    /**
     * @name keydown
     * @param event
     */
    TagComponent.prototype.keydown = function (event) {
        if (this.editing) {
            if (event.keyCode === 13) {
                return this.disableEditMode(event);
            }
        }
        else {
            this.onKeyDown.emit({ event: event, model: this.model });
        }
    };
    /**
     * @name blink
     */
    TagComponent.prototype.blink = function () {
        var classList = this.element.nativeElement.classList;
        classList.add('blink');
        setTimeout(function () { return classList.remove('blink'); }, 50);
    };
    /**
     * @name toggleEditMode
     */
    TagComponent.prototype.toggleEditMode = function () {
        if (this.editable) {
            return this.editing ? undefined : this.activateEditMode();
        }
    };
    /**
     * @name onBlurred
     * @param event
     */
    TagComponent.prototype.onBlurred = function (event) {
        var _a;
        // Checks if it is editable first before handeling the onBlurred event in order to prevent
        // a bug in IE where tags are still editable with onlyFromAutocomplete set to true
        if (!this.editable) {
            return;
        }
        this.disableEditMode();
        var value = event.target.innerText;
        var result = typeof this.model === 'string'
            ? value
            : Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, this.model), (_a = {}, _a[this.displayBy] = value, _a));
        this.onBlur.emit(result);
    };
    /**
     * @name getDisplayValue
     * @param item
     */
    TagComponent.prototype.getDisplayValue = function (item) {
        return typeof item === 'string' ? item : item[this.displayBy];
    };
    Object.defineProperty(TagComponent.prototype, "isRippleVisible", {
        /**
         * @desc returns whether the ripple is visible or not
         * only works in Chrome
         * @name isRippleVisible
         */
        get: function () {
            return !this.readonly && !this.editing && isChrome && this.hasRipple;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name disableEditMode
     * @param $event
     */
    TagComponent.prototype.disableEditMode = function ($event) {
        var classList = this.element.nativeElement.classList;
        var input = this.getContentEditableText();
        this.editing = false;
        classList.remove('tag--editing');
        if (!input) {
            this.setContentEditableText(this.model);
            return;
        }
        this.storeNewValue(input);
        this.cdRef.detectChanges();
        if ($event) {
            $event.preventDefault();
        }
    };
    /**
     * @name isDeleteIconVisible
     */
    TagComponent.prototype.isDeleteIconVisible = function () {
        return (!this.readonly && !this.disabled && this.removable && !this.editing);
    };
    /**
     * @name getContentEditableText
     */
    TagComponent.prototype.getContentEditableText = function () {
        var input = this.getContentEditable();
        return input ? input.innerText.trim() : '';
    };
    /**
     * @name setContentEditableText
     * @param model
     */
    TagComponent.prototype.setContentEditableText = function (model) {
        var input = this.getContentEditable();
        var value = this.getDisplayValue(model);
        input.innerText = value;
    };
    /**
     * @name
     */
    TagComponent.prototype.activateEditMode = function () {
        var classList = this.element.nativeElement.classList;
        classList.add('tag--editing');
        this.editing = true;
    };
    /**
     * @name storeNewValue
     * @param input
     */
    TagComponent.prototype.storeNewValue = function (input) {
        var _a;
        var _this = this;
        var exists = function (tag) {
            return typeof tag === 'string'
                ? tag === input
                : tag[_this.displayBy] === input;
        };
        var hasId = function () {
            return _this.model[_this.identifyBy] !== _this.model[_this.displayBy];
        };
        // if the value changed, replace the value in the model
        if (exists(this.model)) {
            return;
        }
        var model = typeof this.model === 'string'
            ? input
            : (_a = {
                    index: this.index
                },
                _a[this.identifyBy] = hasId()
                    ? this.model[this.identifyBy]
                    : input,
                _a[this.displayBy] = input,
                _a);
        if (this.canAddTag(model)) {
            this.onTagEdited.emit({ tag: model, index: this.index });
        }
        else {
            this.setContentEditableText(this.model);
        }
    };
    /**
     * @name getContentEditable
     */
    TagComponent.prototype.getContentEditable = function () {
        return this.element.nativeElement.querySelector('[contenteditable]');
    };
    TagComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "model", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "removable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "editable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "template", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "displayBy", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "identifyBy", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "index", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "hasRipple", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "disabled", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagComponent.prototype, "canAddTag", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagComponent.prototype, "onSelect", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagComponent.prototype, "onRemove", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagComponent.prototype, "onBlur", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagComponent.prototype, "onKeyDown", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagComponent.prototype, "onTagEdited", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.moving')
    ], TagComponent.prototype, "moving", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(TagRipple)
    ], TagComponent.prototype, "ripple", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('keydown', ['$event'])
    ], TagComponent.prototype, "keydown", null);
    TagComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tag',
            template: "<div (click)=\"select($event)\"\n     (dblclick)=\"toggleEditMode()\"\n     (mousedown)=\"rippleState='clicked'\"\n     (mouseup)=\"rippleState='none'\"\n     [ngSwitch]=\"!!template\"\n     [class.disabled]=\"disabled\"\n     [attr.tabindex]=\"-1\"\n     [attr.aria-label]=\"getDisplayValue(model)\">\n\n    <div *ngSwitchCase=\"true\" [attr.contenteditable]=\"editing\">\n        <!-- CUSTOM TEMPLATE -->\n        <ng-template\n            [ngTemplateOutletContext]=\"{ item: model, index: index }\"\n            [ngTemplateOutlet]=\"template\">\n        </ng-template>\n    </div>\n\n    <div *ngSwitchCase=\"false\" class=\"tag-wrapper\">\n        <!-- TAG NAME -->\n        <div [attr.contenteditable]=\"editing\"\n             [attr.title]=\"getDisplayValue(model)\"\n             class=\"tag__text inline\"\n             spellcheck=\"false\"\n             (keydown.enter)=\"disableEditMode($event)\"\n             (keydown.escape)=\"disableEditMode($event)\"\n             (click)=\"editing ? $event.stopPropagation() : undefined\"\n             (blur)=\"onBlurred($event)\">\n            {{ getDisplayValue(model) }}\n        </div>\n\n        <!-- 'X' BUTTON -->\n        <delete-icon\n            aria-label=\"Remove tag\"\n            role=\"button\"\n            (click)=\"remove($event)\"\n            *ngIf=\"isDeleteIconVisible()\">\n        </delete-icon>\n    </div>\n</div>\n\n<tag-ripple [state]=\"rippleState\"\n            [attr.tabindex]=\"-1\"\n            *ngIf=\"isRippleVisible\">\n</tag-ripple>\n",
            styles: [":host,:host>div,:host>div:focus{outline:0;overflow:hidden;transition:opacity 1s;z-index:1}:host{max-width:400px}:host.blink{-webkit-animation:.3s ease-in-out forwards blink;animation:.3s ease-in-out forwards blink}@-webkit-keyframes blink{0%{opacity:.3}}@keyframes blink{0%{opacity:.3}}:host .disabled{cursor:not-allowed}:host [contenteditable=true]{outline:0}.tag-wrapper{flex-direction:row;display:flex}.tag__text{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}"]
        })
    ], TagComponent);
    return TagComponent;
}());

/**
 * @name animations
 */
var animations = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('animation', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["state"])('in', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({
            opacity: 1
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["state"])('out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({
            opacity: 0
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])(':enter', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('{{ enter }}', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["keyframes"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0, offset: 0, transform: 'translate(0px, 20px)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0.3, offset: 0.3, transform: 'translate(0px, -10px)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0.5, offset: 0.5, transform: 'translate(0px, 0px)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0.75, offset: 0.75, transform: 'translate(0px, 5px)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, offset: 1, transform: 'translate(0px, 0px)' })
            ]))
        ]),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])(':leave', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('{{ leave }}', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["keyframes"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, transform: 'translateX(0)', offset: 0 }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, transform: 'translateX(-15px)', offset: 0.7 }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0, transform: 'translateX(100%)', offset: 1.0 })
            ]))
        ])
    ])
];

var TagInputDropdown = /** @class */ (function () {
    function TagInputDropdown(injector) {
        var _this = this;
        this.injector = injector;
        /**
         * @name offset
         */
        this.offset = defaults.dropdown.offset;
        /**
         * @name focusFirstElement
         */
        this.focusFirstElement = defaults.dropdown.focusFirstElement;
        /**
         * - show autocomplete dropdown if the value of input is empty
         * @name showDropdownIfEmpty
         */
        this.showDropdownIfEmpty = defaults.dropdown.showDropdownIfEmpty;
        /**
         * - desc minimum text length in order to display the autocomplete dropdown
         * @name minimumTextLength
         */
        this.minimumTextLength = defaults.dropdown.minimumTextLength;
        /**
         * - number of items to display in the autocomplete dropdown
         * @name limitItemsTo
         */
        this.limitItemsTo = defaults.dropdown.limitItemsTo;
        /**
         * @name displayBy
         */
        this.displayBy = defaults.dropdown.displayBy;
        /**
         * @name identifyBy
         */
        this.identifyBy = defaults.dropdown.identifyBy;
        /**
         * @description a function a developer can use to implement custom matching for the autocomplete
         * @name matchingFn
         */
        this.matchingFn = defaults.dropdown.matchingFn;
        /**
         * @name appendToBody
         */
        this.appendToBody = defaults.dropdown.appendToBody;
        /**
         * @name keepOpen
         * @description option to leave dropdown open when adding a new item
         */
        this.keepOpen = defaults.dropdown.keepOpen;
        /**
         * @name dynamicUpdate
         */
        this.dynamicUpdate = defaults.dropdown.dynamicUpdate;
        /**
         * @name zIndex
         */
        this.zIndex = defaults.dropdown.zIndex;
        /**
         * list of items that match the current value of the input (for autocomplete)
         * @name items
         */
        this.items = [];
        /**
         * @name tagInput
         */
        this.tagInput = this.injector.get(TagInputComponent);
        /**
         * @name _autocompleteItems
         */
        this._autocompleteItems = [];
        /**
         *
         * @name show
         */
        this.show = function () {
            var maxItemsReached = _this.tagInput.items.length === _this.tagInput.maxItems;
            var value = _this.getFormValue();
            var hasMinimumText = value.trim().length >= _this.minimumTextLength;
            var position = _this.calculatePosition();
            var items = _this.getMatchingItems(value);
            var hasItems = items.length > 0;
            var isHidden = _this.isVisible === false;
            var showDropdownIfEmpty = _this.showDropdownIfEmpty && hasItems && !value;
            var isDisabled = _this.tagInput.disable;
            var shouldShow = isHidden && ((hasItems && hasMinimumText) || showDropdownIfEmpty);
            var shouldHide = _this.isVisible && !hasItems;
            if (_this.autocompleteObservable && hasMinimumText) {
                return _this.getItemsFromObservable(value);
            }
            if ((!_this.showDropdownIfEmpty && !value) ||
                maxItemsReached ||
                isDisabled) {
                return _this.dropdown.hide();
            }
            _this.setItems(items);
            if (shouldShow) {
                _this.dropdown.show(position);
            }
            else if (shouldHide) {
                _this.hide();
            }
        };
        /**
         * @name requestAdding
         * @param item {Ng2MenuItem}
         */
        this.requestAdding = function (item) { return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, function () {
            var tag;
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"])(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tag = this.createTagModel(item);
                        return [4 /*yield*/, this.tagInput.onAddingRequested(true, tag).catch(function () { })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        /**
         * @name resetItems
         */
        this.resetItems = function () {
            _this.items = [];
        };
        /**
         * @name getItemsFromObservable
         * @param text
         */
        this.getItemsFromObservable = function (text) {
            _this.setLoadingState(true);
            var subscribeFn = function (data) {
                // hide loading animation
                _this.setLoadingState(false)
                    // add items
                    .populateItems(data);
                _this.setItems(_this.getMatchingItems(text));
                if (_this.items.length) {
                    _this.dropdown.show(_this.calculatePosition());
                }
                else {
                    _this.dropdown.hide();
                }
            };
            _this.autocompleteObservable(text)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                .subscribe(subscribeFn, function () { return _this.setLoadingState(false); });
        };
    }
    Object.defineProperty(TagInputDropdown.prototype, "autocompleteItems", {
        /**
         * @name autocompleteItems
         * @desc array of items that will populate the autocomplete
         */
        get: function () {
            var _this = this;
            var items = this._autocompleteItems;
            if (!items) {
                return [];
            }
            return items.map(function (item) {
                var _a;
                return typeof item === 'string'
                    ? (_a = {},
                        _a[_this.displayBy] = item,
                        _a[_this.identifyBy] = item,
                        _a) : item;
            });
        },
        /**
         * @name autocompleteItems
         * @param items
         */
        set: function (items) {
            this._autocompleteItems = items;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name ngAfterviewInit
     */
    TagInputDropdown.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.onItemClicked().subscribe(function (item) {
            _this.requestAdding(item);
        });
        // reset itemsMatching array when the dropdown is hidden
        this.onHide().subscribe(this.resetItems);
        var DEBOUNCE_TIME = 200;
        var KEEP_OPEN = this.keepOpen;
        this.tagInput.onTextChange
            .asObservable()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["debounceTime"])(DEBOUNCE_TIME), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (value) {
            if (KEEP_OPEN === false) {
                return value.length > 0;
            }
            return true;
        }))
            .subscribe(this.show);
    };
    /**
     * @name updatePosition
     */
    TagInputDropdown.prototype.updatePosition = function () {
        var position = this.tagInput.inputForm.getElementPosition();
        this.dropdown.menu.updatePosition(position, this.dynamicUpdate);
    };
    Object.defineProperty(TagInputDropdown.prototype, "isVisible", {
        /**
         * @name isVisible
         */
        get: function () {
            return this.dropdown.menu.dropdownState.menuState.isVisible;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name onHide
     */
    TagInputDropdown.prototype.onHide = function () {
        return this.dropdown.onHide;
    };
    /**
     * @name onItemClicked
     */
    TagInputDropdown.prototype.onItemClicked = function () {
        return this.dropdown.onItemClicked;
    };
    Object.defineProperty(TagInputDropdown.prototype, "selectedItem", {
        /**
         * @name selectedItem
         */
        get: function () {
            return this.dropdown.menu.dropdownState.dropdownState.selectedItem;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TagInputDropdown.prototype, "state", {
        /**
         * @name state
         */
        get: function () {
            return this.dropdown.menu.dropdownState;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name hide
     */
    TagInputDropdown.prototype.hide = function () {
        this.resetItems();
        this.dropdown.hide();
    };
    /**
     * @name scrollListener
     */
    TagInputDropdown.prototype.scrollListener = function () {
        if (!this.isVisible || !this.dynamicUpdate) {
            return;
        }
        this.updatePosition();
    };
    /**
     * @name onWindowBlur
     */
    TagInputDropdown.prototype.onWindowBlur = function () {
        this.dropdown.hide();
    };
    /**
     * @name getFormValue
     */
    TagInputDropdown.prototype.getFormValue = function () {
        var formValue = this.tagInput.formValue;
        return formValue ? formValue.toString().trim() : '';
    };
    /**
     * @name calculatePosition
     */
    TagInputDropdown.prototype.calculatePosition = function () {
        return this.tagInput.inputForm.getElementPosition();
    };
    /**
     * @name createTagModel
     * @param item
     */
    TagInputDropdown.prototype.createTagModel = function (item) {
        var _a;
        var display = typeof item.value === 'string' ? item.value : item.value[this.displayBy];
        var value = typeof item.value === 'string' ? item.value : item.value[this.identifyBy];
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, item.value), (_a = {}, _a[this.tagInput.displayBy] = display, _a[this.tagInput.identifyBy] = value, _a));
    };
    /**
     *
     * @param value {string}
     */
    TagInputDropdown.prototype.getMatchingItems = function (value) {
        var _this = this;
        if (!value && !this.showDropdownIfEmpty) {
            return [];
        }
        var dupesAllowed = this.tagInput.allowDupes;
        return this.autocompleteItems.filter(function (item) {
            var hasValue = dupesAllowed
                ? false
                : _this.tagInput.tags.some(function (tag) {
                    var identifyBy = _this.tagInput.identifyBy;
                    var model = typeof tag.model === 'string' ? tag.model : tag.model[identifyBy];
                    return model === item[_this.identifyBy];
                });
            return _this.matchingFn(value, item) && hasValue === false;
        });
    };
    /**
     * @name setItems
     */
    TagInputDropdown.prototype.setItems = function (items) {
        this.items = items.slice(0, this.limitItemsTo || items.length);
    };
    /**
     * @name populateItems
     * @param data
     */
    TagInputDropdown.prototype.populateItems = function (data) {
        var _this = this;
        this.autocompleteItems = data.map(function (item) {
            var _a;
            return typeof item === 'string'
                ? (_a = {},
                    _a[_this.displayBy] = item,
                    _a[_this.identifyBy] = item,
                    _a) : item;
        });
        return this;
    };
    /**
     * @name setLoadingState
     * @param state
     */
    TagInputDropdown.prototype.setLoadingState = function (state) {
        this.tagInput.isLoading = state;
        return this;
    };
    TagInputDropdown.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"] }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_material_dropdown__WEBPACK_IMPORTED_MODULE_4__["Ng2Dropdown"])
    ], TagInputDropdown.prototype, "dropdown", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"])
    ], TagInputDropdown.prototype, "templates", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "offset", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "focusFirstElement", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "showDropdownIfEmpty", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "autocompleteObservable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "minimumTextLength", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "limitItemsTo", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "displayBy", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "identifyBy", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "matchingFn", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "appendToBody", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "keepOpen", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "dynamicUpdate", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "zIndex", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputDropdown.prototype, "autocompleteItems", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:scroll')
    ], TagInputDropdown.prototype, "scrollListener", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:blur')
    ], TagInputDropdown.prototype, "onWindowBlur", null);
    TagInputDropdown = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tag-input-dropdown',
            template: "<ng2-dropdown [dynamicUpdate]=\"dynamicUpdate\">\n    <ng2-dropdown-menu [focusFirstElement]=\"focusFirstElement\"\n                       [zIndex]=\"zIndex\"\n                       [appendToBody]=\"appendToBody\"\n                       [offset]=\"offset\">\n        <ng2-menu-item *ngFor=\"let item of items; let index = index; let last = last\"\n                       [value]=\"item\"\n                       [ngSwitch]=\"!!templates.length\">\n\n            <span *ngSwitchCase=\"false\"\n                  [innerHTML]=\"item[displayBy] | highlight : tagInput.inputForm.value.value\">\n            </span>\n\n            <ng-template *ngSwitchDefault\n                      [ngTemplateOutlet]=\"templates.first\"\n                      [ngTemplateOutletContext]=\"{ item: item, index: index, last: last }\">\n            </ng-template>\n        </ng2-menu-item>\n    </ng2-dropdown-menu>\n</ng2-dropdown>\n"
        })
    ], TagInputDropdown);
    return TagInputDropdown;
}());

// angular universal hacks
/* tslint:disable-next-line */
var DragEvent = typeof window !== 'undefined' && window.DragEvent;
var CUSTOM_ACCESSOR = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return TagInputComponent; }),
    multi: true
};
var TagInputComponent = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(TagInputComponent, _super);
    function TagInputComponent(renderer, dragProvider) {
        var _a;
        var _this = _super.call(this) || this;
        _this.renderer = renderer;
        _this.dragProvider = dragProvider;
        /**
         * @name separatorKeys
         * @desc keyboard keys with which a user can separate items
         */
        _this.separatorKeys = defaults.tagInput.separatorKeys;
        /**
         * @name separatorKeyCodes
         * @desc keyboard key codes with which a user can separate items
         */
        _this.separatorKeyCodes = defaults.tagInput.separatorKeyCodes;
        /**
         * @name placeholder
         * @desc the placeholder of the input text
         */
        _this.placeholder = defaults.tagInput.placeholder;
        /**
         * @name secondaryPlaceholder
         * @desc placeholder to appear when the input is empty
         */
        _this.secondaryPlaceholder = defaults.tagInput.secondaryPlaceholder;
        /**
         * @name maxItems
         * @desc maximum number of items that can be added
         */
        _this.maxItems = defaults.tagInput.maxItems;
        /**
         * @name validators
         * @desc array of Validators that are used to validate the tag before it gets appended to the list
         */
        _this.validators = defaults.tagInput.validators;
        /**
         * @name asyncValidators
         * @desc array of AsyncValidator that are used to validate the tag before it gets appended to the list
         */
        _this.asyncValidators = defaults.tagInput.asyncValidators;
        /**
        * - if set to true, it will only possible to add items from the autocomplete
        * @name onlyFromAutocomplete
        */
        _this.onlyFromAutocomplete = defaults.tagInput.onlyFromAutocomplete;
        /**
         * @name errorMessages
         */
        _this.errorMessages = defaults.tagInput.errorMessages;
        /**
         * @name theme
         */
        _this.theme = defaults.tagInput.theme;
        /**
         * @name onTextChangeDebounce
         */
        _this.onTextChangeDebounce = defaults.tagInput.onTextChangeDebounce;
        /**
         * - custom id assigned to the input
         * @name id
         */
        _this.inputId = defaults.tagInput.inputId;
        /**
         * - custom class assigned to the input
         */
        _this.inputClass = defaults.tagInput.inputClass;
        /**
         * - option to clear text input when the form is blurred
         * @name clearOnBlur
         */
        _this.clearOnBlur = defaults.tagInput.clearOnBlur;
        /**
         * - hideForm
         * @name clearOnBlur
         */
        _this.hideForm = defaults.tagInput.hideForm;
        /**
         * @name addOnBlur
         */
        _this.addOnBlur = defaults.tagInput.addOnBlur;
        /**
         * @name addOnPaste
         */
        _this.addOnPaste = defaults.tagInput.addOnPaste;
        /**
         * - pattern used with the native method split() to separate patterns in the string pasted
         * @name pasteSplitPattern
         */
        _this.pasteSplitPattern = defaults.tagInput.pasteSplitPattern;
        /**
         * @name blinkIfDupe
         */
        _this.blinkIfDupe = defaults.tagInput.blinkIfDupe;
        /**
         * @name removable
         */
        _this.removable = defaults.tagInput.removable;
        /**
         * @name editable
         */
        _this.editable = defaults.tagInput.editable;
        /**
         * @name allowDupes
         */
        _this.allowDupes = defaults.tagInput.allowDupes;
        /**
         * @description if set to true, the newly added tags will be added as strings, and not objects
         * @name modelAsStrings
         */
        _this.modelAsStrings = defaults.tagInput.modelAsStrings;
        /**
         * @name trimTags
         */
        _this.trimTags = defaults.tagInput.trimTags;
        /**
         * @name ripple
         */
        _this.ripple = defaults.tagInput.ripple;
        /**
         * @name tabindex
         * @desc pass through the specified tabindex to the input
         */
        _this.tabindex = defaults.tagInput.tabIndex;
        /**
         * @name disable
         */
        _this.disable = defaults.tagInput.disable;
        /**
         * @name dragZone
         */
        _this.dragZone = defaults.tagInput.dragZone;
        /**
         * @name onRemoving
         */
        _this.onRemoving = defaults.tagInput.onRemoving;
        /**
         * @name onAdding
         */
        _this.onAdding = defaults.tagInput.onAdding;
        /**
         * @name animationDuration
         */
        _this.animationDuration = defaults.tagInput.animationDuration;
        /**
         * @name onAdd
         * @desc event emitted when adding a new item
         */
        _this.onAdd = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onRemove
         * @desc event emitted when removing an existing item
         */
        _this.onRemove = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onSelect
         * @desc event emitted when selecting an item
         */
        _this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onFocus
         * @desc event emitted when the input is focused
         */
        _this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onFocus
         * @desc event emitted when the input is blurred
         */
        _this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name onTextChange
         * @desc event emitted when the input value changes
         */
        _this.onTextChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * - output triggered when text is pasted in the form
         * @name onPaste
         */
        _this.onPaste = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * - output triggered when tag entered is not valid
         * @name onValidationError
         */
        _this.onValidationError = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * - output triggered when tag is edited
         * @name onTagEdited
         */
        _this.onTagEdited = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @name isLoading
         */
        _this.isLoading = false;
        /**
         * @name listeners
         * @desc array of events that get fired using @fireEvents
         */
        _this.listeners = (_a = {},
            _a[KEYDOWN] = [],
            _a[KEYUP] = [],
            _a);
        /**
         * @description emitter for the 2-way data binding inputText value
         * @name inputTextChange
         */
        _this.inputTextChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /**
         * @description private variable to bind get/set
         * @name inputTextValue
         */
        _this.inputTextValue = '';
        _this.errors = [];
        /**
         * @name appendTag
         * @param tag {TagModel}
         */
        _this.appendTag = function (tag, index) {
            if (index === void 0) { index = _this.items.length; }
            var items = _this.items;
            var model = _this.modelAsStrings ? tag[_this.identifyBy] : tag;
            _this.items = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"])(items.slice(0, index), [
                model
            ], items.slice(index, items.length));
        };
        /**
         * @name createTag
         * @param model
         */
        _this.createTag = function (model) {
            var _a;
            var trim = function (val, key) {
                return typeof val === 'string' ? val.trim() : val[key];
            };
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])(Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, typeof model !== 'string' ? model : {}), (_a = {}, _a[_this.displayBy] = _this.trimTags ? trim(model, _this.displayBy) : model, _a[_this.identifyBy] = _this.trimTags ? trim(model, _this.identifyBy) : model, _a));
        };
        /**
         *
         * @param tag
         * @param isFromAutocomplete
         */
        _this.isTagValid = function (tag, fromAutocomplete) {
            if (fromAutocomplete === void 0) { fromAutocomplete = false; }
            var selectedItem = _this.dropdown ? _this.dropdown.selectedItem : undefined;
            var value = _this.getItemDisplay(tag).trim();
            if (selectedItem && !fromAutocomplete || !value) {
                return false;
            }
            var dupe = _this.findDupe(tag, fromAutocomplete);
            // if so, give a visual cue and return false
            if (!_this.allowDupes && dupe && _this.blinkIfDupe) {
                var model = _this.tags.find(function (item) {
                    return _this.getItemValue(item.model) === _this.getItemValue(dupe);
                });
                if (model) {
                    model.blink();
                }
            }
            var isFromAutocomplete = fromAutocomplete && _this.onlyFromAutocomplete;
            var assertions = [
                // 1. there must be no dupe OR dupes are allowed
                !dupe || _this.allowDupes,
                // 2. check max items has not been reached
                !_this.maxItemsReached,
                // 3. check item comes from autocomplete or onlyFromAutocomplete is false
                ((isFromAutocomplete) || !_this.onlyFromAutocomplete)
            ];
            return assertions.filter(Boolean).length === assertions.length;
        };
        /**
         * @name onPasteCallback
         * @param data
         */
        _this.onPasteCallback = function (data) { return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, function () {
            var getText, text, requests, resetInput;
            var _this = this;
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"])(this, function (_a) {
                getText = function () {
                    var isIE = Boolean(window.clipboardData);
                    var clipboardData = isIE ? (window.clipboardData) : data.clipboardData;
                    var type = isIE ? 'Text' : 'text/plain';
                    return clipboardData === null ? '' : clipboardData.getData(type) || '';
                };
                text = getText();
                requests = text
                    .split(this.pasteSplitPattern)
                    .map(function (item) {
                    var tag = _this.createTag(item);
                    _this.setInputValue(tag[_this.displayBy]);
                    return _this.onAddingRequested(false, tag);
                });
                resetInput = function () { return setTimeout(function () { return _this.setInputValue(''); }, 50); };
                Promise.all(requests).then(function () {
                    _this.onPaste.emit(text);
                    resetInput();
                })
                    .catch(resetInput);
                return [2 /*return*/];
            });
        }); };
        return _this;
    }
    Object.defineProperty(TagInputComponent.prototype, "inputText", {
        /**
         * @name inputText
         */
        get: function () {
            return this.inputTextValue;
        },
        /**
         * @name inputText
         * @param text
         */
        set: function (text) {
            this.inputTextValue = text;
            this.inputTextChange.emit(text);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TagInputComponent.prototype, "tabindexAttr", {
        /**
         * @desc removes the tab index if it is set - it will be passed through to the input
         * @name tabindexAttr
         */
        get: function () {
            return this.tabindex !== '' ? '-1' : '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name ngAfterViewInit
     */
    TagInputComponent.prototype.ngAfterViewInit = function () {
        // set up listeners
        var _this = this;
        this.setUpKeypressListeners();
        this.setupSeparatorKeysListener();
        this.setUpInputKeydownListeners();
        if (this.onTextChange.observers.length) {
            this.setUpTextChangeSubscriber();
        }
        // if clear on blur is set to true, subscribe to the event and clear the text's form
        if (this.clearOnBlur || this.addOnBlur) {
            this.setUpOnBlurSubscriber();
        }
        // if addOnPaste is set to true, register the handler and add items
        if (this.addOnPaste) {
            this.setUpOnPasteListener();
        }
        var statusChanges$ = this.inputForm.form.statusChanges;
        statusChanges$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (status) { return status !== 'PENDING'; })).subscribe(function () {
            _this.errors = _this.inputForm.getErrorMessages(_this.errorMessages);
        });
        this.isProgressBarVisible$ = statusChanges$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (status) {
            return status === 'PENDING' || _this.isLoading;
        }));
        // if hideForm is set to true, remove the input
        if (this.hideForm) {
            this.inputForm.destroy();
        }
    };
    /**
     * @name ngOnInit
     */
    TagInputComponent.prototype.ngOnInit = function () {
        // if the number of items specified in the model is > of the value of maxItems
        // degrade gracefully and let the max number of items to be the number of items in the model
        // though, warn the user.
        var hasReachedMaxItems = this.maxItems !== undefined &&
            this.items &&
            this.items.length > this.maxItems;
        if (hasReachedMaxItems) {
            this.maxItems = this.items.length;
            console.warn(MAX_ITEMS_WARNING);
        }
        // Setting editable to false to fix problem with tags in IE still being editable when
        // onlyFromAutocomplete is true
        this.editable = this.onlyFromAutocomplete ? false : this.editable;
        this.setAnimationMetadata();
    };
    /**
     * @name onRemoveRequested
     * @param tag
     * @param index
     */
    TagInputComponent.prototype.onRemoveRequested = function (tag, index) {
        var _this = this;
        return new Promise(function (resolve) {
            var subscribeFn = function (model) {
                _this.removeItem(model, index);
                resolve(tag);
            };
            _this.onRemoving ?
                _this.onRemoving(tag)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                    .subscribe(subscribeFn) : subscribeFn(tag);
        });
    };
    /**
     * @name onAddingRequested
     * @param fromAutocomplete {boolean}
     * @param tag {TagModel}
     * @param index? {number}
     * @param giveupFocus? {boolean}
     */
    TagInputComponent.prototype.onAddingRequested = function (fromAutocomplete, tag, index, giveupFocus) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var subscribeFn = function (model) {
                return _this
                    .addItem(fromAutocomplete, model, index, giveupFocus)
                    .then(resolve)
                    .catch(reject);
            };
            return _this.onAdding ?
                _this.onAdding(tag)
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                    .subscribe(subscribeFn, reject) : subscribeFn(tag);
        });
    };
    /**
     * @name selectItem
     * @desc selects item passed as parameter as the selected tag
     * @param item
     * @param emit
     */
    TagInputComponent.prototype.selectItem = function (item, emit) {
        if (emit === void 0) { emit = true; }
        var isReadonly = item && typeof item !== 'string' && item.readonly;
        if (isReadonly || this.selectedTag === item) {
            return;
        }
        this.selectedTag = item;
        if (emit) {
            this.onSelect.emit(item);
        }
    };
    /**
     * @name fireEvents
     * @desc goes through the list of the events for a given eventName, and fires each of them
     * @param eventName
     * @param $event
     */
    TagInputComponent.prototype.fireEvents = function (eventName, $event) {
        var _this = this;
        this.listeners[eventName].forEach(function (listener) { return listener.call(_this, $event); });
    };
    /**
     * @name handleKeydown
     * @desc handles action when the user hits a keyboard key
     * @param data
     */
    TagInputComponent.prototype.handleKeydown = function (data) {
        var event = data.event;
        var key = event.keyCode || event.which;
        var shiftKey = event.shiftKey || false;
        switch (KEY_PRESS_ACTIONS[key]) {
            case ACTIONS_KEYS.DELETE:
                if (this.selectedTag && this.removable) {
                    var index = this.items.indexOf(this.selectedTag);
                    this.onRemoveRequested(this.selectedTag, index);
                }
                break;
            case ACTIONS_KEYS.SWITCH_PREV:
                this.moveToTag(data.model, PREV);
                break;
            case ACTIONS_KEYS.SWITCH_NEXT:
                this.moveToTag(data.model, NEXT);
                break;
            case ACTIONS_KEYS.TAB:
                if (shiftKey) {
                    if (this.isFirstTag(data.model)) {
                        return;
                    }
                    this.moveToTag(data.model, PREV);
                }
                else {
                    if (this.isLastTag(data.model) && (this.disable || this.maxItemsReached)) {
                        return;
                    }
                    this.moveToTag(data.model, NEXT);
                }
                break;
            default:
                return;
        }
        // prevent default behaviour
        event.preventDefault();
    };
    TagInputComponent.prototype.onFormSubmit = function () {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function () {
            var _a;
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"])(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.onAddingRequested(false, this.formValue)];
                    case 1:
                        _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _a = _b.sent();
                        return [2 /*return*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @name setInputValue
     * @param value
     */
    TagInputComponent.prototype.setInputValue = function (value, emitEvent) {
        if (emitEvent === void 0) { emitEvent = true; }
        var control = this.getControl();
        // update form value with the transformed item
        control.setValue(value, { emitEvent: emitEvent });
    };
    /**
     * @name getControl
     */
    TagInputComponent.prototype.getControl = function () {
        return this.inputForm.value;
    };
    /**
     * @name focus
     * @param applyFocus
     * @param displayAutocomplete
     */
    TagInputComponent.prototype.focus = function (applyFocus, displayAutocomplete) {
        if (applyFocus === void 0) { applyFocus = false; }
        if (displayAutocomplete === void 0) { displayAutocomplete = false; }
        if (this.dragProvider.getState('dragging')) {
            return;
        }
        this.selectItem(undefined, false);
        if (applyFocus) {
            this.inputForm.focus();
            this.onFocus.emit(this.formValue);
        }
    };
    /**
     * @name blur
     */
    TagInputComponent.prototype.blur = function () {
        this.onTouched();
        this.onBlur.emit(this.formValue);
    };
    /**
     * @name hasErrors
     */
    TagInputComponent.prototype.hasErrors = function () {
        return !!this.inputForm && this.inputForm.hasErrors();
    };
    /**
     * @name isInputFocused
     */
    TagInputComponent.prototype.isInputFocused = function () {
        return !!this.inputForm && this.inputForm.isInputFocused();
    };
    /**
     * - this is the one way I found to tell if the template has been passed and it is not
     * the template for the menu item
     * @name hasCustomTemplate
     */
    TagInputComponent.prototype.hasCustomTemplate = function () {
        var template = this.templates ? this.templates.first : undefined;
        var menuTemplate = this.dropdown && this.dropdown.templates ?
            this.dropdown.templates.first : undefined;
        return Boolean(template && template !== menuTemplate);
    };
    Object.defineProperty(TagInputComponent.prototype, "maxItemsReached", {
        /**
         * @name maxItemsReached
         */
        get: function () {
            return this.maxItems !== undefined &&
                this.items.length >= this.maxItems;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TagInputComponent.prototype, "formValue", {
        /**
         * @name formValue
         */
        get: function () {
            var form = this.inputForm.value;
            return form ? form.value : '';
        },
        enumerable: true,
        configurable: true
    });
    /**3
     * @name onDragStarted
     * @param event
     * @param index
     */
    TagInputComponent.prototype.onDragStarted = function (event, tag, index) {
        event.stopPropagation();
        var item = { zone: this.dragZone, tag: tag, index: index };
        this.dragProvider.setSender(this);
        this.dragProvider.setDraggedItem(event, item);
        this.dragProvider.setState({ dragging: true, index: index });
    };
    /**
     * @name onDragOver
     * @param event
     */
    TagInputComponent.prototype.onDragOver = function (event, index) {
        this.dragProvider.setState({ dropping: true });
        this.dragProvider.setReceiver(this);
        event.preventDefault();
    };
    /**
     * @name onTagDropped
     * @param event
     * @param index
     */
    TagInputComponent.prototype.onTagDropped = function (event, index) {
        var item = this.dragProvider.getDraggedItem(event);
        if (!item || item.zone !== this.dragZone) {
            return;
        }
        this.dragProvider.onTagDropped(item.tag, item.index, index);
        event.preventDefault();
        event.stopPropagation();
    };
    /**
     * @name isDropping
     */
    TagInputComponent.prototype.isDropping = function () {
        var isReceiver = this.dragProvider.receiver === this;
        var isDropping = this.dragProvider.getState('dropping');
        return Boolean(isReceiver && isDropping);
    };
    /**
     * @name onTagBlurred
     * @param changedElement {TagModel}
     * @param index {number}
     */
    TagInputComponent.prototype.onTagBlurred = function (changedElement, index) {
        this.items[index] = changedElement;
        this.blur();
    };
    /**
     * @name trackBy
     * @param items
     */
    TagInputComponent.prototype.trackBy = function (index, item) {
        return item[this.identifyBy];
    };
    /**
     * @name updateEditedTag
     * @param tag
     */
    TagInputComponent.prototype.updateEditedTag = function (_a) {
        var tag = _a.tag, index = _a.index;
        this.onTagEdited.emit(tag);
    };
    /**
     * @name moveToTag
     * @param item
     * @param direction
     */
    TagInputComponent.prototype.moveToTag = function (item, direction) {
        var isLast = this.isLastTag(item);
        var isFirst = this.isFirstTag(item);
        var stopSwitch = (direction === NEXT && isLast) ||
            (direction === PREV && isFirst);
        if (stopSwitch) {
            this.focus(true);
            return;
        }
        var offset = direction === NEXT ? 1 : -1;
        var index = this.getTagIndex(item) + offset;
        var tag = this.getTagAtIndex(index);
        return tag.select.call(tag);
    };
    /**
     * @name isFirstTag
     * @param item {TagModel}
     */
    TagInputComponent.prototype.isFirstTag = function (item) {
        return this.tags.first.model === item;
    };
    /**
     * @name isLastTag
     * @param item {TagModel}
     */
    TagInputComponent.prototype.isLastTag = function (item) {
        return this.tags.last.model === item;
    };
    /**
     * @name getTagIndex
     * @param item
     */
    TagInputComponent.prototype.getTagIndex = function (item) {
        var tags = this.tags.toArray();
        return tags.findIndex(function (tag) { return tag.model === item; });
    };
    /**
     * @name getTagAtIndex
     * @param index
     */
    TagInputComponent.prototype.getTagAtIndex = function (index) {
        var tags = this.tags.toArray();
        return tags[index];
    };
    /**
     * @name removeItem
     * @desc removes an item from the array of the model
     * @param tag {TagModel}
     * @param index {number}
     */
    TagInputComponent.prototype.removeItem = function (tag, index) {
        this.items = this.getItemsWithout(index);
        // if the removed tag was selected, set it as undefined
        if (this.selectedTag === tag) {
            this.selectItem(undefined, false);
        }
        // focus input
        this.focus(true, false);
        // emit remove event
        this.onRemove.emit(tag);
    };
    /**
     * @name addItem
     * @desc adds the current text model to the items array
     * @param fromAutocomplete {boolean}
     * @param item {TagModel}
     * @param index? {number}
     * @param giveupFocus? {boolean}
     */
    TagInputComponent.prototype.addItem = function (fromAutocomplete, item, index, giveupFocus) {
        var _this = this;
        if (fromAutocomplete === void 0) { fromAutocomplete = false; }
        var display = this.getItemDisplay(item);
        var tag = this.createTag(item);
        if (fromAutocomplete) {
            this.setInputValue(this.getItemValue(item, true));
        }
        return new Promise(function (resolve, reject) {
            /**
             * @name reset
             */
            var reset = function () {
                // reset control and focus input
                _this.setInputValue('');
                if (giveupFocus) {
                    _this.focus(false, false);
                }
                else {
                    // focus input
                    _this.focus(true, false);
                }
                resolve(display);
            };
            var appendItem = function () {
                _this.appendTag(tag, index);
                // emit event
                _this.onAdd.emit(tag);
                if (!_this.dropdown) {
                    return;
                }
                _this.dropdown.hide();
                if (_this.dropdown.showDropdownIfEmpty) {
                    _this.dropdown.show();
                }
            };
            var status = _this.inputForm.form.status;
            var isTagValid = _this.isTagValid(tag, fromAutocomplete);
            var onValidationError = function () {
                _this.onValidationError.emit(tag);
                return reject();
            };
            if (status === 'VALID' && isTagValid) {
                appendItem();
                return reset();
            }
            if (status === 'INVALID' || !isTagValid) {
                reset();
                return onValidationError();
            }
            if (status === 'PENDING') {
                var statusUpdate$ = _this.inputForm.form.statusChanges;
                return statusUpdate$
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (statusUpdate) { return statusUpdate !== 'PENDING'; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                    .subscribe(function (statusUpdate) {
                    if (statusUpdate === 'VALID' && isTagValid) {
                        appendItem();
                        return reset();
                    }
                    else {
                        reset();
                        return onValidationError();
                    }
                });
            }
        });
    };
    /**
     * @name setupSeparatorKeysListener
     */
    TagInputComponent.prototype.setupSeparatorKeysListener = function () {
        var _this = this;
        var useSeparatorKeys = this.separatorKeyCodes.length > 0 || this.separatorKeys.length > 0;
        var listener = function ($event) {
            var hasKeyCode = _this.separatorKeyCodes.indexOf($event.keyCode) >= 0;
            var hasKey = _this.separatorKeys.indexOf($event.key) >= 0;
            // the keyCode of keydown event is 229 when IME is processing the key event.
            var isIMEProcessing = $event.keyCode === 229;
            if (hasKeyCode || (hasKey && !isIMEProcessing)) {
                $event.preventDefault();
                _this.onAddingRequested(false, _this.formValue)
                    .catch(function () { });
            }
        };
        listen.call(this, KEYDOWN, listener, useSeparatorKeys);
    };
    /**
     * @name setUpKeypressListeners
     */
    TagInputComponent.prototype.setUpKeypressListeners = function () {
        var _this = this;
        var listener = function ($event) {
            var isCorrectKey = $event.keyCode === 37 || $event.keyCode === 8;
            if (isCorrectKey &&
                !_this.formValue &&
                _this.items.length) {
                _this.tags.last.select.call(_this.tags.last);
            }
        };
        // setting up the keypress listeners
        listen.call(this, KEYDOWN, listener);
    };
    /**
     * @name setUpKeydownListeners
     */
    TagInputComponent.prototype.setUpInputKeydownListeners = function () {
        var _this = this;
        this.inputForm.onKeydown.subscribe(function (event) {
            if (event.key === 'Backspace' && _this.formValue.trim() === '') {
                event.preventDefault();
            }
        });
    };
    /**
     * @name setUpOnPasteListener
     */
    TagInputComponent.prototype.setUpOnPasteListener = function () {
        var _this = this;
        var input = this.inputForm.input.nativeElement;
        // attach listener to input
        this.renderer.listen(input, 'paste', function (event) {
            _this.onPasteCallback(event);
            event.preventDefault();
            return true;
        });
    };
    /**
     * @name setUpTextChangeSubscriber
     */
    TagInputComponent.prototype.setUpTextChangeSubscriber = function () {
        var _this = this;
        this.inputForm.form
            .valueChanges
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["debounceTime"])(this.onTextChangeDebounce))
            .subscribe(function (value) {
            _this.onTextChange.emit(value.item);
        });
    };
    /**
     * @name setUpOnBlurSubscriber
     */
    TagInputComponent.prototype.setUpOnBlurSubscriber = function () {
        var _this = this;
        var filterFn = function () {
            var isVisible = _this.dropdown && _this.dropdown.isVisible;
            return !isVisible && !!_this.formValue;
        };
        this.inputForm
            .onBlur
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["debounceTime"])(100), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(filterFn))
            .subscribe(function () {
            var reset = function () { return _this.setInputValue(''); };
            if (_this.addOnBlur) {
                return _this
                    .onAddingRequested(false, _this.formValue, undefined, true)
                    .then(reset)
                    .catch(reset);
            }
            reset();
        });
    };
    /**
     * @name findDupe
     * @param tag
     * @param isFromAutocomplete
     */
    TagInputComponent.prototype.findDupe = function (tag, isFromAutocomplete) {
        var _this = this;
        var identifyBy = isFromAutocomplete ? this.dropdown.identifyBy : this.identifyBy;
        var id = tag[identifyBy];
        return this.items.find(function (item) { return _this.getItemValue(item) === id; });
    };
    /**
     * @name setAnimationMetadata
     */
    TagInputComponent.prototype.setAnimationMetadata = function () {
        this.animationMetadata = {
            value: 'in',
            params: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"])({}, this.animationDuration)
        };
    };
    TagInputComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"] },
        { type: DragProvider }
    ]; };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "separatorKeys", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "separatorKeyCodes", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "placeholder", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "secondaryPlaceholder", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "maxItems", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "validators", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "asyncValidators", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "onlyFromAutocomplete", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "errorMessages", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "theme", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "onTextChangeDebounce", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "inputId", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "inputClass", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "clearOnBlur", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "hideForm", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "addOnBlur", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "addOnPaste", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "pasteSplitPattern", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "blinkIfDupe", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "removable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "editable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "allowDupes", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "modelAsStrings", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "trimTags", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "inputText", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "ripple", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "tabindex", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "disable", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "dragZone", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "onRemoving", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "onAdding", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], TagInputComponent.prototype, "animationDuration", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onAdd", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onRemove", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onSelect", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onFocus", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onBlur", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onTextChange", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onPaste", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onValidationError", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "onTagEdited", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])(TagInputDropdown)
    ], TagInputComponent.prototype, "dropdown", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"], { descendants: false })
    ], TagInputComponent.prototype, "templates", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(TagInputForm)
    ], TagInputComponent.prototype, "inputForm", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChildren"])(TagComponent)
    ], TagInputComponent.prototype, "tags", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], TagInputComponent.prototype, "inputTextChange", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('attr.tabindex')
    ], TagInputComponent.prototype, "tabindexAttr", null);
    TagInputComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'tag-input',
            providers: [CUSTOM_ACCESSOR],
            template: "<div\n    [ngClass]=\"theme\"\n    class=\"ng2-tag-input\"\n    (click)=\"focus(true, false)\"\n    [attr.tabindex]=\"-1\"\n    (drop)=\"dragZone ? onTagDropped($event, undefined) : undefined\"\n    (dragenter)=\"dragZone ? onDragOver($event) : undefined\"\n    (dragover)=\"dragZone ? onDragOver($event) : undefined\"\n    (dragend)=\"dragZone ? dragProvider.onDragEnd() : undefined\"\n    [class.ng2-tag-input--dropping]=\"isDropping()\"\n    [class.ng2-tag-input--disabled]=\"disable\"\n    [class.ng2-tag-input--loading]=\"isLoading\"\n    [class.ng2-tag-input--invalid]=\"hasErrors()\"\n    [class.ng2-tag-input--focused]=\"isInputFocused()\"\n>\n\n    <!-- TAGS -->\n    <div class=\"ng2-tags-container\">\n        <tag\n            *ngFor=\"let item of items; let i = index; trackBy: trackBy\"\n            (onSelect)=\"selectItem(item)\"\n            (onRemove)=\"onRemoveRequested(item, i)\"\n            (onKeyDown)=\"handleKeydown($event)\"\n            (onTagEdited)=\"updateEditedTag($event)\"\n            (onBlur)=\"onTagBlurred($event, i)\"\n            draggable=\"{{ editable }}\"\n            (dragstart)=\"dragZone ? onDragStarted($event, item, i) : undefined\"\n            (drop)=\"dragZone ? onTagDropped($event, i) : undefined\"\n            (dragenter)=\"dragZone ? onDragOver($event) : undefined\"\n            (dragover)=\"dragZone ? onDragOver($event, i) : undefined\"\n            (dragleave)=\"dragZone ? dragProvider.onDragEnd() : undefined\"\n            [canAddTag]=\"isTagValid\"\n            [attr.tabindex]=\"0\"\n            [disabled]=\"disable\"\n            [@animation]=\"animationMetadata\"\n            [hasRipple]=\"ripple\"\n            [index]=\"i\"\n            [removable]=\"removable\"\n            [editable]=\"editable\"\n            [displayBy]=\"displayBy\"\n            [identifyBy]=\"identifyBy\"\n            [template]=\"!!hasCustomTemplate() ? templates.first : undefined\"\n            [draggable]=\"dragZone\"\n            [model]=\"item\"\n        >\n        </tag>\n\n        <tag-input-form\n            (onSubmit)=\"onFormSubmit()\"\n            (onBlur)=\"blur()\"\n            (click)=\"dropdown ? dropdown.show() : undefined\"\n            (onKeydown)=\"fireEvents('keydown', $event)\"\n            (onKeyup)=\"fireEvents('keyup', $event)\"\n            [inputText]=\"inputText\"\n            [disabled]=\"disable\"\n            [validators]=\"validators\"\n            [asyncValidators]=\"asyncValidators\"\n            [hidden]=\"maxItemsReached\"\n            [placeholder]=\"items.length ? placeholder : secondaryPlaceholder\"\n            [inputClass]=\"inputClass\"\n            [inputId]=\"inputId\"\n            [tabindex]=\"tabindex\"\n        >\n        </tag-input-form>\n    </div>\n\n    <div\n        class=\"progress-bar\"\n        *ngIf=\"isProgressBarVisible$ | async\"\n    ></div>\n</div>\n\n<!-- ERRORS -->\n<div\n    *ngIf=\"hasErrors()\"\n    [ngClass]=\"theme\"\n    class=\"error-messages\"\n>\n    <p\n        *ngFor=\"let error of errors\"\n        class=\"error-message\"\n    >\n        <span>{{ error }}</span>\n    </p>\n</div>\n<ng-content></ng-content>\n",
            animations: animations,
            styles: [".dark tag:focus{box-shadow:0 0 0 1px #323232}.ng2-tag-input.bootstrap3-info{background-color:#fff;display:inline-block;color:#555;vertical-align:middle;max-width:100%;height:42px;line-height:44px}.ng2-tag-input.bootstrap3-info input{border:none;box-shadow:none;outline:0;background-color:transparent;padding:0 6px;margin:0;width:auto;max-width:inherit}.ng2-tag-input.bootstrap3-info .form-control input::-moz-placeholder{color:#777;opacity:1}.ng2-tag-input.bootstrap3-info .form-control input:-ms-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info .form-control input::-webkit-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info input:focus{border:none;box-shadow:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--focused{box-shadow:inset 0 1px 1px rgba(0,0,0,.4);border:1px solid #ccc}.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;transition:.25s;padding:.25rem 0;min-height:32px;cursor:text;border-bottom:2px solid #efefef}.ng2-tag-input:focus{outline:0}.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #2196f3}.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #f44336}.ng2-tag-input.ng2-tag-input--loading{border:none}.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.ng2-tag-input form{margin:.1em 0}.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.minimal.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:1px solid transparent}.minimal.ng2-tag-input:focus{outline:0}.minimal.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.minimal.ng2-tag-input.ng2-tag-input--loading{border:none}.minimal.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.minimal.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.dark.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #444}.dark.ng2-tag-input:focus{outline:0}.dark.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.dark.ng2-tag-input.ng2-tag-input--loading{border:none}.dark.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.dark.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #efefef}.bootstrap.ng2-tag-input:focus{outline:0}.bootstrap.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #0275d8}.bootstrap.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #d9534f}.bootstrap.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap3-info.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;padding:4px;cursor:text;box-shadow:inset 0 1px 1px rgba(0,0,0,.075);border-radius:4px}.bootstrap3-info.ng2-tag-input:focus{outline:0}.bootstrap3-info.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap3-info.ng2-tag-input.ng2-tag-input--invalid{box-shadow:inset 0 1px 1px #d9534f;border-bottom:1px solid #d9534f}.bootstrap3-info.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap3-info.ng2-tag-input form{margin:.1em 0}.bootstrap3-info.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.error-message{font-size:.8em;color:#f44336;margin:.5em 0 0}.bootstrap .error-message{color:#d9534f}.progress-bar,.progress-bar:before{height:2px;width:100%;margin:0}.progress-bar{background-color:#2196f3;display:flex;position:absolute;bottom:0}.progress-bar:before{background-color:#82c4f8;content:\"\";-webkit-animation:2s cubic-bezier(.4,0,.2,1) infinite running-progress;animation:2s cubic-bezier(.4,0,.2,1) infinite running-progress}@-webkit-keyframes running-progress{0%{margin-left:0;margin-right:100%}50%{margin-left:25%;margin-right:0}100%{margin-left:100%;margin-right:0}}@keyframes running-progress{0%{margin-left:0;margin-right:100%}50%{margin-left:25%;margin-right:0}100%{margin-left:100%;margin-right:0}}tag{display:flex;flex-direction:row;flex-wrap:wrap;font-family:Roboto,\"Helvetica Neue\",sans-serif;font-weight:400;font-size:1em;letter-spacing:.05rem;color:#444;border-radius:16px;transition:.3s;margin:.1rem .3rem .1rem 0;padding:.08rem .45rem;height:32px;line-height:34px;background:#efefef;-webkit-user-select:none;-moz-user-select:none;user-select:none;overflow:hidden;outline:0;cursor:pointer;position:relative}tag:not(.readonly):not(.tag--editing):focus{background:#2196f3;color:#fff;box-shadow:0 2px 3px 1px #d4d1d1}tag:not(.readonly):not(.tag--editing):active{background:#0d8aee;color:#fff;box-shadow:0 2px 3px 1px #d4d1d1}tag:not(:focus):not(.tag--editing):not(:active):not(.readonly):hover{background:#e2e2e2;color:initial;box-shadow:0 2px 3px 1px #d4d1d1}tag.readonly{cursor:default}tag.readonly:focus,tag:focus{outline:0}tag.tag--editing{background-color:#fff;border:1px solid #ccc;cursor:text}.minimal tag{display:flex;flex-direction:row;flex-wrap:wrap;border-radius:0;background:#f9f9f9;-webkit-user-select:none;-moz-user-select:none;user-select:none;overflow:hidden;outline:0;cursor:pointer;position:relative}.minimal tag:not(.readonly):not(.tag--editing):active,.minimal tag:not(.readonly):not(.tag--editing):focus{background:#d0d0d0;color:initial}.minimal tag:not(:focus):not(.tag--editing):not(:active):not(.readonly):hover{background:#ececec}.minimal tag.readonly{cursor:default}.minimal tag.readonly:focus,.minimal tag:focus{outline:0}.minimal tag.tag--editing{cursor:text}.dark tag{display:flex;flex-direction:row;flex-wrap:wrap;color:#f9f9f9;border-radius:3px;background:#444;-webkit-user-select:none;-moz-user-select:none;user-select:none;overflow:hidden;outline:0;cursor:pointer;position:relative}.dark tag:not(.readonly):not(.tag--editing):focus{background:#efefef;color:#444}.dark tag:not(:focus):not(.tag--editing):not(:active):not(.readonly):hover{background:#2b2b2b;color:#f9f9f9}.dark tag.readonly{cursor:default}.dark tag.readonly:focus,.dark tag:focus{outline:0}.dark tag.tag--editing{cursor:text}.bootstrap tag{display:flex;flex-direction:row;flex-wrap:wrap;color:#f9f9f9;border-radius:.25rem;background:#0275d8;-webkit-user-select:none;-moz-user-select:none;user-select:none;overflow:hidden;outline:0;cursor:pointer;position:relative}.bootstrap tag:not(.readonly):not(.tag--editing):active,.bootstrap tag:not(.readonly):not(.tag--editing):focus{background:#025aa5}.bootstrap tag:not(:focus):not(.tag--editing):not(:active):not(.readonly):hover{background:#0267bf;color:#f9f9f9}.bootstrap tag.readonly{cursor:default}.bootstrap tag.readonly:focus,.bootstrap tag:focus{outline:0}.bootstrap tag.tag--editing{cursor:text}.bootstrap3-info tag{display:flex;flex-direction:row;flex-wrap:wrap;font-family:inherit;font-weight:400;font-size:95%;color:#fff;border-radius:.25em;background:#5bc0de;-webkit-user-select:none;-moz-user-select:none;user-select:none;overflow:hidden;outline:0;cursor:pointer;position:relative;padding:.25em .6em;text-align:center;white-space:nowrap}.bootstrap3-info tag:not(.readonly):not(.tag--editing):active,.bootstrap3-info tag:not(.readonly):not(.tag--editing):focus{background:#28a1c5}.bootstrap3-info tag:not(:focus):not(.tag--editing):not(:active):not(.readonly):hover{background:#46b8da;color:#fff}.bootstrap3-info tag.readonly{cursor:default}.bootstrap3-info tag.readonly:focus,.bootstrap3-info tag:focus{outline:0}.bootstrap3-info tag.tag--editing{cursor:text}:host{display:block}"]
        })
    ], TagInputComponent);
    return TagInputComponent;
}(TagInputAccessor));

var DeleteIconComponent = /** @class */ (function () {
    function DeleteIconComponent() {
    }
    DeleteIconComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'delete-icon',
            template: "<span>\n    <svg\n        height=\"16px\"\n        viewBox=\"0 0 32 32\"\n        width=\"16px\"\n    >\n        <path\n            d=\"M17.459,16.014l8.239-8.194c0.395-0.391,0.395-1.024,0-1.414c-0.394-0.391-1.034-0.391-1.428,0  l-8.232,8.187L7.73,6.284c-0.394-0.395-1.034-0.395-1.428,0c-0.394,0.396-0.394,1.037,0,1.432l8.302,8.303l-8.332,8.286  c-0.394,0.391-0.394,1.024,0,1.414c0.394,0.391,1.034,0.391,1.428,0l8.325-8.279l8.275,8.276c0.394,0.395,1.034,0.395,1.428,0  c0.394-0.396,0.394-1.037,0-1.432L17.459,16.014z\"\n            fill=\"#121313\"\n        />\n    </svg>\n</span>",
            styles: [".dark tag:focus{box-shadow:0 0 0 1px #323232}.ng2-tag-input.bootstrap3-info{background-color:#fff;display:inline-block;color:#555;vertical-align:middle;max-width:100%;height:42px;line-height:44px}.ng2-tag-input.bootstrap3-info input{border:none;box-shadow:none;outline:0;background-color:transparent;padding:0 6px;margin:0;width:auto;max-width:inherit}.ng2-tag-input.bootstrap3-info .form-control input::-moz-placeholder{color:#777;opacity:1}.ng2-tag-input.bootstrap3-info .form-control input:-ms-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info .form-control input::-webkit-input-placeholder{color:#777}.ng2-tag-input.bootstrap3-info input:focus{border:none;box-shadow:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--focused{box-shadow:inset 0 1px 1px rgba(0,0,0,.4);border:1px solid #ccc}.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;transition:.25s;padding:.25rem 0;min-height:32px;cursor:text;border-bottom:2px solid #efefef}.ng2-tag-input:focus{outline:0}.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #2196f3}.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #f44336}.ng2-tag-input.ng2-tag-input--loading{border:none}.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.ng2-tag-input form{margin:.1em 0}.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.minimal.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:1px solid transparent}.minimal.ng2-tag-input:focus{outline:0}.minimal.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.minimal.ng2-tag-input.ng2-tag-input--loading{border:none}.minimal.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.minimal.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.dark.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #444}.dark.ng2-tag-input:focus{outline:0}.dark.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.dark.ng2-tag-input.ng2-tag-input--loading{border:none}.dark.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.dark.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;cursor:text;border-bottom:2px solid #efefef}.bootstrap.ng2-tag-input:focus{outline:0}.bootstrap.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap.ng2-tag-input.ng2-tag-input--focused{border-bottom:2px solid #0275d8}.bootstrap.ng2-tag-input.ng2-tag-input--invalid{border-bottom:2px solid #d9534f}.bootstrap.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.bootstrap3-info.ng2-tag-input{display:block;flex-direction:row;flex-wrap:wrap;position:relative;padding:4px;cursor:text;box-shadow:inset 0 1px 1px rgba(0,0,0,.075);border-radius:4px}.bootstrap3-info.ng2-tag-input:focus{outline:0}.bootstrap3-info.ng2-tag-input.ng2-tag-input--dropping{opacity:.7}.bootstrap3-info.ng2-tag-input.ng2-tag-input--invalid{box-shadow:inset 0 1px 1px #d9534f;border-bottom:1px solid #d9534f}.bootstrap3-info.ng2-tag-input.ng2-tag-input--loading{border:none}.bootstrap3-info.ng2-tag-input.ng2-tag-input--disabled{opacity:.5;cursor:not-allowed}.bootstrap3-info.ng2-tag-input form{margin:.1em 0}.bootstrap3-info.ng2-tag-input .ng2-tags-container{flex-wrap:wrap;display:flex}.error-message{font-size:.8em;color:#f44336;margin:.5em 0 0}.bootstrap .error-message{color:#d9534f}:host(delete-icon){width:20px;height:16px;transition:.15s;display:inline-block;text-align:right}:host(delete-icon) path{fill:#444}:host(delete-icon) svg{vertical-align:bottom;height:34px}:host(delete-icon):hover{transform:scale(1.5) translateY(-3px)}:host-context(.dark){text-align:right}:host-context(.dark) path{fill:#fff}:host-context(.dark) svg{vertical-align:bottom;height:34px}:host-context(.minimal){text-align:right}:host-context(.minimal) path{fill:#444}:host-context(.minimal) svg{vertical-align:bottom;height:34px}:host-context(.bootstrap){text-align:right}:host-context(.bootstrap) path{fill:#fff}:host-context(.bootstrap) svg{vertical-align:bottom;height:34px}:host-context(tag:active) path,:host-context(tag:focus) path{fill:#fff}:host-context(.darktag:active) path,:host-context(.darktag:focus) path{fill:#000}:host-context(.minimaltag:active) path,:host-context(.minimaltag:focus) path{fill:#000}:host-context(.bootstraptag:active) path,:host-context(.bootstraptag:focus) path{fill:#fff}:host-context(.bootstrap3-info){height:inherit}:host-context(.bootstrap3-info) path{fill:#fff}"]
        })
    ], DeleteIconComponent);
    return DeleteIconComponent;
}());

var optionsProvider = new OptionsProvider();
var TagInputModule = /** @class */ (function () {
    function TagInputModule() {
    }
    /**
     * @name withDefaults
     * @param options {Options}
     */
    TagInputModule.withDefaults = function (options) {
        optionsProvider.setOptions(options);
    };
    TagInputModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                ng2_material_dropdown__WEBPACK_IMPORTED_MODULE_4__["Ng2DropdownModule"]
            ],
            declarations: [
                TagInputComponent,
                DeleteIconComponent,
                TagInputForm,
                TagComponent,
                HighlightPipe,
                TagInputDropdown,
                TagRipple
            ],
            exports: [
                TagInputComponent,
                DeleteIconComponent,
                TagInputForm,
                TagComponent,
                HighlightPipe,
                TagInputDropdown,
                TagRipple
            ],
            providers: [
                DragProvider,
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["COMPOSITION_BUFFER_MODE"], useValue: false },
            ]
        })
    ], TagInputModule);
    return TagInputModule;
}());

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=ngx-chips.js.map


/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.html ***!
  \*******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cast-page\">\r\n\t<div class=\"about-bg\"><span>Add Service</span></div>\r\n</div>\r\n<div class=\"login_form\">\r\n\t<div class=\"login_form_set\">\r\n\t\t<div class=\"login-logo\"><h2>Welcome to <span>FixMyKix</span> Custom Services</h2></div>\r\n\t\t<form>\r\n\t\t\t<div *ngIf=\"categories?.length > 0\" class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Select Option</label>\r\n\t\t\t\t<label class=\"label-check\" *ngFor=\"let category of  categories; let i = index;\" for=\"{{category.type}}\"><input type=\"radio\" id=\"{{category.id}}\" name=\"cate01\" value=\"{{category.id}}\" (click)=\"selectedCategory(category);\"> {{category.name}}</label>\r\n\t\t\t</div>\r\n\t\t\t<div *ngIf=\"subCategories?.length > 0\" class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Option Category</label>\r\n\t\t\t\t<label *ngFor=\"let subCategory of  subCategories; let i = index;\" class=\"label-check\" for=\"{{subCategory.id}}\"><input type=\"radio\" id=\"{{subCategory.id}}\" name=\"cate02\" (click)=\"selectedSubCategory(subCategory);\"> {{subCategory.name}}</label>\r\n\t\t\t</div>\r\n\t\t\t<div *ngIf=\"subCategoriesOpt?.length > 0\" class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Option Sub Category</label>\r\n\t\t\t\t<label class=\"label-check\" *ngFor=\"let subCategoryOpt of  subCategoriesOpt; let i = index;\" for=\"{{subCategoryOpt.id}}\"><input type=\"radio\" id=\"{{subCategoryOpt.id}}\" name=\"cate03\" (click)=\"selectedSubCategoryOpt(subCategoryOpt);\"> {{subCategoryOpt.name}}</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group service-selection\" *ngIf=\"Single_Set_Option?.length > 0\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Single Set Option</label>\r\n\t\t\t\t<label class=\"label-check\" *ngFor=\"let option of  Single_Set_Option; let i = index;\"  for=\"{{option.id}}\"><input type=\"radio\" id=\"{{option.id}}\" name=\"cate04\" (click)=\"setServices(option);\"> {{option.name}}</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group service-selection\" *ngIf=\"Services?.length > 0\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Multiple Option selection of Upper Repair</label>\r\n\t\t\t\t<label class=\"label-check\" *ngFor=\"let service of  Services; let i = index;\"><input type=\"checkbox\" [(ngModel)]=\"service.isChecked\" name=\"{{service.name}}\" (change)=\"onCheckboxChange(service.isChecked, service.id)\" checked=\"false\"> {{service.name}}</label>\r\n\t\t\t</div>\r\n\t\t\t<!-- \r\n\t\t\t<div class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Multiple Option selection of Upper Repair</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Repaint</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Re-Dye</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Reglue</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Stitching</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Patching</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Refinish</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Remove Crensing</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Other</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Multiple Option selection of Interior Repair</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> In Sole Replacement</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Lining Repair</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Other</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Multiple Option selection of Sole Repair</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Heel Repair</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Deoxidation(Remove Yellow)</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Reglue</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Stitching</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> SDE Replacement</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Mid-sole Replacement</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Other</label>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group service-selection\">\r\n\t\t\t\t<label class=\"w-100\">Restoration Multiple Option selection of Cleaning</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Deep Cleaning</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Spot Cleaning</label>\r\n\t\t\t\t<label class=\"label-check\"><input type=\"checkbox\"> Other</label>\r\n\t\t\t</div> -->\r\n\t\t\t<div class=\"form-group login-button\">\r\n\t\t\t\t<button type=\"submit\" [disabled]=\"submit_btn == false\" (click)=\"submit();\" class=\"btn btn-primary btn-sm\">Submit</button>\r\n\t\t\t</div>\r\n\t\t</form>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/artist-services.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/artist-services/artist-services.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/list/artist-services-list.component.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/artist-services/list/artist-services-list.component.html ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cast-page\">\r\n\t<div class=\"about-bg\"><span>Services List</span></div>\r\n</div>\r\n<div class=\"profile-review service\">\r\n\t<p class=\"rating-stars\"></p>\r\n\t<div class=\"servicerequest\">\r\n\t\t<a href=\"javascript:void(0)\"  (click)=\"Artist_Add_Service()\">Add Services</a>\r\n\t</div>\r\n</div>\r\n\r\n<div class=\"row service\">\r\n\t<div class=\"col-sm-12\" *ngIf=\"record == true\">\r\n\t\t<div class=\"jumbotron jumbotron-fluid\">\r\n\t\t\t<div class=\"container\">\r\n\t\t\t  <h1 class=\"record\">No Record Found</h1>\r\n\t\t\t</div>\r\n\t\t  </div>\r\n\t</div>\r\n\t<div class=\"col-sm-12\" *ngIf=\"services?.length > 0\">\r\n\t\t<table class=\"table table-bordered\">\r\n\t\t\t\t<thead>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<th> S.No</th>\r\n\t\t\t\t\t\t<th> Service Name</th>\r\n\t\t\t\t\t\t<th> Number of Days</th>\r\n\t\t\t\t\t\t<th> Description</th>\r\n\t\t\t\t\t\t<th> Action</th>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</thead>\r\n\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr *ngFor=\"let service of  services; let i = index;\">\r\n\t\t\t\t\t\t<td>{{i+1}}</td>\r\n\t\t\t\t\t\t<td>{{service.service_name}}</td>\r\n\t\t\t\t\t\t<td>{{service.number_of_days}}</td>\r\n\t\t\t\t\t\t<td>{{service.description}}</td>\r\n\t\t\t\t\t\t<td><a href=\"javascript:void(0)\" (click)=\"Delete_service(service)\" title=\"Delete Service\" ><i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n\t\t\t\t\t\t</a></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</tbody>\r\n\t\t</table>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/artist.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/explore/explore.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/explore/explore.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main>\r\n\t<div class=\"cast-page\">\r\n\t\t<div class=\"about-bg\"><span>Explore Artist</span></div>\r\n\t</div>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"shoes-shoper\">\r\n\t\t\t<div class=\"explore-user-profile\">\r\n\t\t\t\t<div class=\"explore-filter\">\r\n\t\t\t\t\t<!-- <p class=\"price-range-head\">Price Range</p>\r\n\t\t\t\t\t<div class=\"price-range\">\r\n\t\t\t\t\t\t<div id=\"slider-range\"></div>\r\n\t\t\t\t\t\t<div class=\"slider-labels\">\r\n\t\t\t\t\t\t\t<div class=\"caption\">\r\n\t\t\t\t\t\t\t\t<span id=\"slider-range-value1\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"text-right caption\">\r\n\t\t\t\t\t\t\t\t<span id=\"slider-range-value2\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<form>\r\n\t\t\t\t\t\t\t<input type=\"hidden\" name=\"min-value\" value=\"\">\r\n\t\t\t\t\t\t\t<input type=\"hidden\" name=\"max-value\" value=\"\">\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t\t\r\n\t\t\t\t\t<!-- <div class=\"unregister-explore\">\r\n\t\t\t\t\t\t<a href=\"javascript:void()0\" id=\"restoration-tab\" class=\"active-service\">Restoration</a>\r\n\t\t\t\t\t\t<a href=\"javascript:void()0\" id=\"sneakerart-tab\">Sneaker Art</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"cate-show\">\r\n\t\t\t\t\t\t<div class=\"explore-categories restoration-tab\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li>Traditional Cobbler Restoration</li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Dye Shoes</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Stretch Shoes</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Replace Heels</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Add Protective Rubber</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Shoe Shine</a></li>\r\n\t\t\t\t\t\t\t\t<li>Sneaker Restoration</li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Deep Clean</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Repaint</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">ReGlue</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Dye Shoes</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Deosidation/Unyellow</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Sole Swap</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Full Restoration</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"explore-categories sneakerart-tab hide\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Custom Colorway</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">Custom Characters</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t\t<div class=\"explore-categories explore-tags\">\r\n\t\t\t\t\t\t<ul><li>Services</li></ul>\r\n\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t<div class=\"explore-categories services\">\r\n\t\t\t\t\t\t<div *ngFor=\"let service of  artist_services; let i = index;\" class=\"col-sm-12 form-check form-check-inline\">\r\n\t\t\t\t\t\t\t<input class=\"form-check-input\" [(ngModel)]=\"service.isChecked\" type=\"checkbox\" id=\"{{service.id}}\" (change)=\"onCheckboxChange(service.isChecked, service.id)\" checked=\"false\"/>\r\n\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"{{service.id}}\">{{service.name}}</label>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<!-- <ul>\r\n\t\t\t\t\t\t\t<li *ngFor=\"let service of  artist_services; let i = index;\">\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\">{{service.name}}</a></li>\r\n\t\t\t\t\t\t</ul> -->\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"explore-details\">\r\n\t\t\t\t\t<!-- <div class=\"explore-search\">\r\n\t\t\t\t\t\tSEARCH FILTER\r\n\t\t\t\t\t\t<input type=\"search\" name=\"search\" placeholder=\"Search\">\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t<li *ngFor=\"let artist of  aritsts_lists; let i = index;\">\r\n\t\t\t\t\t\t\t<div class=\"listing-details\">\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" (click)=\"SetArtistId(artist.id)\">\r\n\t\t\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/../../../../assets/images/img_aboutfix08.jpg\"></figure>\r\n\t\t\t\t\t\t\t\t\t<div class=\"listing-name\">{{artist.first_name}}</div>\r\n\t\t\t\t\t\t\t\t\t<!-- <div class=\"listing-price\">$<span>1020</span></div> -->\r\n\t\t\t\t\t\t\t\t\t<!-- <div class=\"listing-username\">{{artist.service_description}}</div> -->\r\n\t\t\t\t\t\t\t\t\t<!-- <div class=\"listing-rating\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t\t\t\t</div> -->\r\n\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t<!-- <div class=\"editupdatlinks\">\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-fire\"></i></a>\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>\r\n\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-user\"></i></a>\r\n\t\t\t\t\t\t\t\t</div> -->\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</main>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/list/list.component.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/list/list.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\t\t\t<div class=\"cast-page\">\r\n\t\t\t\t<div class=\"about-bg\"><span>Service Provider</span></div>\r\n\t\t\t</div>\r\n\t\t\t<div id=\"main\">\r\n\t\t\t\t<div class=\"listing-outer row\">\r\n\t\t\t\t\t<div class=\"col-md-5 full-listing-details\">\r\n\t\t\t\t\t\t<h3>Service Provider Details</h3>\r\n\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t<label>Service Provider name</label>\r\n\t\t\t\t\t\t\t<p>{{this.artist_details.first_name}}</p>\r\n\t\t\t\t\t\t\t<label>Service Provider Email</label>\r\n\t\t\t\t\t\t\t<p>\t{{this.artist_details.email}}</p>\r\n\t\t\t\t\t\t\t<!-- <label>Price</label>\r\n\t\t\t\t\t\t\t<p>$<span>100000</span></p>\r\n\t\t\t\t\t\t\t<label>Listing Rating</label>\r\n\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t\t</p> -->\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-md-7 remove-pr\">\r\n\t\t\t\t\t\t<div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\r\n\t\t\t\t\t\t\t<ul class=\"carousel-indicators\">\r\n\t\t\t\t\t\t\t\t<li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\r\n\t\t\t\t\t\t\t\t<li data-target=\"#demo\" data-slide-to=\"1\"></li>\r\n\t\t\t\t\t\t\t\t<li data-target=\"#demo\" data-slide-to=\"2\"></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t<div class=\"carousel-inner\">\r\n\t\t\t\t\t\t\t\t<div class=\"carousel-item active\">\r\n\t\t\t\t\t\t\t\t\t<img src=\"../../../../assets/images/jordanshoes00.jpg\" alt=\"Los Angeles\" width=\"1100\" height=\"500\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"carousel-item\">\r\n\t\t\t\t\t\t\t\t\t<img src=\"../../../../assets/images/jordanshoes02.jpg\" alt=\"Chicago\" width=\"1100\" height=\"500\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"carousel-item\">\r\n\t\t\t\t\t\t\t\t\t<img src=\"../../../../assets/images/jordanshoes03.jpg\" alt=\"New York\" width=\"1100\" height=\"500\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<!-- <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\r\n\t\t\t\t\t\t\t\t<span class=\"carousel-control-prev-icon\"></span>\r\n\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t<a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\r\n\t\t\t\t\t\t\t\t<span class=\"carousel-control-next-icon\"></span>\r\n\t\t\t\t\t\t\t</a> -->\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-md-12 full-listing-details remove-pr\">\r\n\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t<label>Description</label>\r\n\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-md-12 full-listing-details remove-pr\">\r\n\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t<label>Services</label>\r\n\t\t\t\t\t\t\t<p *ngFor=\"let service of  artist_services; let i = index;\">\r\n\t\t\t\t\t\t\t\t{{service.service_name}}\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-md-12 remove-pr continue-button\">\r\n\t\t\t\t\t\t<!-- <a href=\"artist/service/request\">SERVICE REQUEST</a> -->\r\n\t\t\t\t\t\t<a href=\"javascript:void(0)\" (click)=\"Chat_request()\">Chat</a>\r\n\t\t\t\t\t\t<a href=\"javascript:void(0)\" (click)=\"Apply_Service_request()\">SERVICE REQUEST</a>\r\n\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/payment-method/payment-method.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/payment-method/payment-method.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main>\r\n  <div class=\"cast-page\">\r\n    <div class=\"about-bg\"><span>Payment Method</span></div>\r\n  </div>\r\n  <div id=\"main\">\r\n    <div class=\"shoes-shoper\">\r\n      <ul class=\"about-shoe-servicec-list\">\r\n        <li>\r\n          <div class=\"about-shoe-text\">\r\n            <h2 class=\"h2-base-heading\"><p>Payment Method Mode</p></h2>\r\n            \r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</main>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/profile/profile.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/profile/profile.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button type=\"button\" class=\"btn btn-info btn-lg\" id=\"profile_btn\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"display :none\">Profile Modal</button>\r\n<div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\r\n\t<div class=\"modal-dialog\">\r\n\t\t<div class=\"modal-content\">\r\n\t\t\t<div class=\"modal-body\">\r\n\t\t\t\t<div class=\"login_form\">\r\n\t\t\t\t\t<div class=\"login_form_set\">\r\n\t\t\t\t\t\t<div class=\"login-logo\"><h2>Profile</h2></div>\r\n\t\t\t\t\t\t<form name=\"profile\" [formGroup]=\"formGroup\" (ngSubmit)=\"UpdateUser()\" autocomplete=\"off\">\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>First Name</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"first_name\" autocomplete=\"off\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Last Name</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"last_name\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Email</label>\r\n\t\t\t\t\t\t\t\t<input type=\"email\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"email\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group formTag\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Mobile Number</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"number form-control\" placeholder=\"\" formControlName=\"mobile_number\" required>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group formTag\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Age</label>\r\n\t\t\t\t\t\t\t\t<input type=\"number\" class=\"number form-control\" placeholder=\"\" formControlName=\"age\" required>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div  class=\"form-group gender\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label class=\"w-100\">Gender</label>\r\n\t\t\t\t\t\t\t\t<label class=\"gender-check\" for=\"male\"><input type=\"radio\" id=\"male\" formControlName=\"gender\" value=\"male\"> Male</label>\r\n\t\t\t\t\t\t\t\t<label class=\"gender-check\" for=\"female\"><input type=\"radio\" id=\"female\" formControlName=\"gender\" value=\"female\"> Female</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Business Name</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"business_name\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Business Socials</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"business_socials\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Address</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"address\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>visual proof of quality work</label>\r\n\t\t\t\t\t\t\t\t<input type=\"file\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"proof_of_work\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"description form-group form-group-100\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>description of services</label>\r\n\t\t\t\t\t\t\t\t<textarea name=\"\" class=\"form-control\" formControlName=\"service_description\" id=\"\" cols=\"3\"></textarea>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group login-button\">\r\n\t\t\t\t\t\t\t\t<button type=\"button\" data-dismiss=\"modal\">Close</button>\r\n\t\t\t\t\t\t\t\t<button type=\"submit\">Submit</button>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<!-- <div class=\"modal-footer\">\r\n\t\t\t</div> -->\r\n\t\t</div>\r\n\t\t\r\n\t</div>\r\n</div>\r\n<main>\r\n\t<div class=\"cast-page\">\r\n\t\t<div class=\"about-bg\"><span>Artist Profile</span></div>\r\n\t</div>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"shoes-shoper\">\r\n\t\t\t<ul class=\"about-shoe-servicec-list\">\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t<h2 class=\"h2-base-heading\"><p>Profile Description</p></h2>\r\n\t\t\t\t\t\t<div class=\"profile-descrition\">\r\n\t\t\t\t\t\t\t<figure class=\"view-browse-image\">\r\n\t\t\t\t\t\t\t\t<img class=\"view-image\" src=\"../../../../assets/../../../../assets/images/img_user.png\" alt=\"img\">\r\n\t\t\t\t\t\t\t</figure>\r\n\t\t\t\t\t\t\t<div class=\"profile-view\">\r\n\t\t\t\t\t\t\t\t<div class=\"detailed-profile\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<label>{{user.first_name}}</label>\r\n\t\t\t\t\t\t\t\t\t\t<p>Admin</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<label>Description of service</label>\r\n\t\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<label>Description of service</label>\r\n\t\t\t\t\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"profile-review\">\r\n\t\t\t\t\t\t\t\t\t<p class=\"rating-stars\">\r\n\t\t\t\t\t\t\t\t\t\t<span>Rating</span><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t\t<div class=\"servicerequest\">\r\n\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" (click)=\"Artist_Service_List()\" >Services</a>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"servicerequest\">\r\n\t\t\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"  (click)=\"Artist_Service_request()\">Service Request</a>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix08.jpg\"></figure>\r\n\t\t\t\t\t\t\t<div class=\"editupdatlinks\">\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-fire\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-user\"></i></a>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix08.jpg\"></figure>\r\n\t\t\t\t\t\t\t<div class=\"editupdatlinks\">\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-fire\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-user\"></i></a>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix08.jpg\"></figure>\r\n\t\t\t\t\t\t\t<div class=\"editupdatlinks\">\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-fire\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-comment\"></i></a>\r\n\t\t\t\t\t\t\t\t<a href=\"javascript:void(0)\"><i class=\"fa fa-user\"></i></a>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n</main>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/service-request/service-request.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/artist/service-request/service-request.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main>\r\n\t<div class=\"cast-page\">\r\n\t\t<div class=\"about-bg\"><span>Service Requests</span></div>\r\n\t</div>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"shoes-shoper\">\r\n\t\t\t<ul class=\"about-shoe-servicec-list\">\r\n\t\t\t\t<li *ngFor=\"let requests of  artist_services_Requests; let i = index;\">\r\n\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t<figure class=\"browse-image\">\r\n\t\t\t\t\t\t\t<img class=\"view-image\" src=\"../../../../assets/images/img_user.png\" alt=\"img\">\r\n\t\t\t\t\t\t</figure>\r\n\t\t\t\t\t\t<div class=\"information-form\">\r\n\t\t\t\t\t\t\t<div class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<label>From</label>\r\n\t\t\t\t\t\t\t\t<p class=\"filled-value\">{{requests.user_info.first_name}}</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group w-100\">\r\n\t\t\t\t\t\t\t\t<label>Description of what is needed done to shoe. What is wrong with the shoe?</label>\r\n\t\t\t\t\t\t\t\t<p class=\"filled-value\">{{requests.description}}</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group w-100\">\r\n\t\t\t\t\t\t\t\t<label>Upload clear images of shoe that need work</label>\r\n\t\t\t\t\t\t\t\t<div class=\"uploaded-image-viewer\">\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/img_user.png\"></p>\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/img_user.png\"></p>\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/img_user.png\"></p>\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/img_user.png\"></p>\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/img_user.png\"></p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"continue-button\"><button *ngIf=\"requests.status == 'pending'\" type=\"submit\" (click)=\"OpenTotPage(requests)\">accept</button><button *ngIf=\"requests.status == 'pending'\" type=\"submit\" (click)=\"Reject_Request(requests)\">Reject</button> <button type=\"submit\" class=\"cancel-button\" *ngIf=\"requests.status == 'cancelled'\" disabled>Cancelled</button>\r\n\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"cancel-button\" *ngIf=\"requests.status == 'approved'\" disabled>Approved</button><button type=\"submit\" class=\"cancel-button\" *ngIf=\"requests.status == 'rejected'\" disabled>Rejected</button></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n</main>\r\n<button type=\"button\" class=\"btn btn-info btn-lg\" id=\"profile_btn\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"display :none\">Profile Modal</button>\r\n<div class=\"modal fade\" id=\"myModal\" role=\"dialog\">\r\n\t<div class=\"modal-dialog\">\r\n\t\t<div class=\"modal-content\">\r\n\t\t\t<div class=\"modal-body\">\r\n\t\t\t\t<div class=\"login_form\">\r\n\t\t\t\t\t<div class=\"login_form_set\">\r\n\t\t\t\t\t\t<div class=\"login-logo\"><h2>Profile</h2></div>\r\n\t\t\t\t\t\t<form name=\"profile\" [formGroup]=\"formGroup\" (ngSubmit)=\"Accept_Request()\" autocomplete=\"off\">\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Price</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"price\" autocomplete=\"off\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Turn arrount time</label>\r\n\t\t\t\t\t\t\t\t<input type=\"time\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"turn_around_time\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group\" [formGroup]=\"formGroup\">\r\n\t\t\t\t\t\t\t\t<label>Description</label>\r\n\t\t\t\t\t\t\t\t<textarea type=\"text\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"description\"></textarea>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"form-group login-button\">\r\n\t\t\t\t\t\t\t\t<button type=\"button\" data-dismiss=\"modal\" id=\"close_btn\" >Close</button>\r\n\t\t\t\t\t\t\t\t<button type=\"submit\">Submit</button>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<!-- <div class=\"modal-footer\">\r\n\t\t\t</div> -->\r\n\t\t</div>\r\n\t\t\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./src/app/@auth/artist/artist-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/@auth/artist/artist-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: ArtistRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistRoutingModule", function() { return ArtistRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _artist_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./artist.component */ "./src/app/@auth/artist/artist.component.ts");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/@auth/artist/profile/profile.component.ts");
/* harmony import */ var _explore_explore_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./explore/explore.component */ "./src/app/@auth/artist/explore/explore.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list/list.component */ "./src/app/@auth/artist/list/list.component.ts");
/* harmony import */ var _payment_method_payment_method_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./payment-method/payment-method.component */ "./src/app/@auth/artist/payment-method/payment-method.component.ts");
/* harmony import */ var _service_request_service_request_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./service-request/service-request.component */ "./src/app/@auth/artist/service-request/service-request.component.ts");









var routes = [{
        path: '',
        component: _artist_component__WEBPACK_IMPORTED_MODULE_3__["ArtistComponent"],
        children: [
            {
                path: 'profile',
                component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_4__["ProfileComponent"]
            },
            {
                path: 'explore',
                component: _explore_explore_component__WEBPACK_IMPORTED_MODULE_5__["NgxArtistExploreComponent"]
            }, {
                path: 'list',
                component: _list_list_component__WEBPACK_IMPORTED_MODULE_6__["NgxArtistListComponent"]
            }, {
                path: 'service/request',
                component: _service_request_service_request_component__WEBPACK_IMPORTED_MODULE_8__["NgxArtistServiceRequestComponent"]
            }, {
                path: 'payment',
                component: _payment_method_payment_method_component__WEBPACK_IMPORTED_MODULE_7__["NgxArtistPaymentMethodComponent"]
            },
            {
                path: 'services',
                loadChildren: function () { return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./artist-services/artist-services.module */ "./src/app/@auth/artist/artist-services/artist-services.module.ts"))
                    .then(function (m) { return m.ArtistServicesModule; }); },
            },
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full',
            },
            {
                path: '',
                redirectTo: 'profile',
            },
        ],
    }];
var ArtistRoutingModule = /** @class */ (function () {
    function ArtistRoutingModule() {
    }
    ArtistRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], ArtistRoutingModule);
    return ArtistRoutingModule;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login_form {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  min-height: 100vh;\n  padding: 50px 15px;\n  overflow-y: auto;\n  position: relative;\n}\n\n.login-logo {\n  text-align: center;\n  margin: 0px 0 20px;\n}\n\n.login-logo h2 {\n  font-family: Noto Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 22px;\n  line-height: 32px;\n  color: #2B3B54;\n  text-align: center;\n}\n\n.login-logo h2 span {\n  text-transform: uppercase;\n}\n\n.login_form_set {\n  /*background: #fff;*/\n  background: rgba(255, 255, 255, 0.95);\n  border-radius: 10px 10px 0px 0px;\n  width: 550px;\n  box-sizing: border-box;\n  padding: 20px 15px;\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  box-shadow: 0 0 20px -19px #000;\n}\n\n.login_form_set form {\n  width: 100%;\n  max-width: 100%;\n  margin: 0 auto;\n}\n\n.login_form_set form {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.login_form_set form .form-group {\n  margin: 0 0 12px;\n  padding: 0 5px;\n  position: relative;\n  display: block;\n  width: 100%;\n}\n\n.login_form_set form .form-group label {\n  margin: 0;\n  font-weight: bold;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  font-size: 14px;\n}\n\n.login_form_set form .form-group label.label-check {\n  display: inline-flex;\n  align-items: center;\n  margin: 0 12px 0 0;\n  font-size: 12px;\n  font-weight: normal;\n  cursor: pointer;\n}\n\n.login_form_set form .form-group label.label-check input {\n  margin: 0 6px 0 0;\n  width: 10px;\n  height: 10px;\n}\n\n.btn:focus, .btn:hover {\n  background-color: #0069d9 !important;\n}\n\n.login-button {\n  width: 100% !important;\n  text-align: center;\n  padding: 0 2%;\n  box-sizing: border-box;\n  margin: 10px 0 0 0 !important;\n}\n\n.login_form_set form .form-group button {\n  /*background: #008cff;*/\n  border-radius: 6px;\n  color: #fff;\n  border: none;\n  width: 160px;\n  padding: 4px 10px 7px;\n  box-sizing: border-box;\n}\n\n.login_form_set form .form-group button:focus {\n  outline: none;\n  border: none;\n}\n\n/* Responsive =================================================================*/\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2FydGlzdC1zZXJ2aWNlcy9hZGQtdXBkYXRlLXNlcnZpY2VzL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxhcnRpc3RcXGFydGlzdC1zZXJ2aWNlc1xcYWRkLXVwZGF0ZS1zZXJ2aWNlc1xcYWRkLXVwZGF0ZS1zZXJ2aWNlcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2FydGlzdC1zZXJ2aWNlcy9hZGQtdXBkYXRlLXNlcnZpY2VzL2FkZC11cGRhdGUtc2VydmljZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNBRjs7QURFQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7QUNDRjs7QURDQTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQ0VGOztBREFBO0VBQ0UseUJBQUE7QUNHRjs7QUREQTtFQUNFLG9CQUFBO0VBQ0EscUNBQUE7RUFDQSxnQ0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw4QkFBQTtFQUNBLCtCQUFBO0FDSUY7O0FERkE7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNLRjs7QURIQTtFQUNFLGFBQUE7RUFDQSxlQUFBO0FDTUY7O0FESkE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FDT0Y7O0FETEE7RUFDRSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQ1FGOztBRE5BO0VBQ0Usb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ1NGOztBRFBBO0VBQ0UsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1VGOztBRFJBO0VBQ0Usb0NBQUE7QUNXRjs7QURUQTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtBQ1lGOztBRFZBO0VBQ0UsdUJBQUE7RUFFQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUNZRjs7QURWQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0FDYUY7O0FEWEEsZ0ZBQUEiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9hcnRpc3QvYXJ0aXN0LXNlcnZpY2VzL2FkZC11cGRhdGUtc2VydmljZXMvYWRkLXVwZGF0ZS1zZXJ2aWNlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbl9mb3JtIHtcclxuICAvLyBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvaW1nX2JnMDEuanBnJykgbm8tcmVwZWF0IHNjcm9sbCBjZW50ZXIgY2VudGVyIC8gY292ZXI7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWluLWhlaWdodDogMTAwdmg7XHJcbiAgcGFkZGluZzogNTBweCAxNXB4O1xyXG4gIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5sb2dpbi1sb2dvIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwcHggMCAyMHB4O1xyXG59XHJcbi5sb2dpbi1sb2dvIGgyIHtcclxuICBmb250LWZhbWlseTogTm90byBTYW5zO1xyXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDIycHg7XHJcbiAgbGluZS1oZWlnaHQ6IDMycHg7XHJcbiAgY29sb3I6ICMyQjNCNTQ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5sb2dpbi1sb2dvIGgyIHNwYW4ge1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuLmxvZ2luX2Zvcm1fc2V0IHtcclxuICAvKmJhY2tncm91bmQ6ICNmZmY7Ki9cclxuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOTUpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4O1xyXG4gIHdpZHRoOiA1NTBweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIHBhZGRpbmc6IDIwcHggMTVweDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBib3gtc2hhZG93OiAwIDAgMjBweCAtMTlweCAjMDAwO1xyXG59XHJcbi5sb2dpbl9mb3JtX3NldCBmb3JtIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbn1cclxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG59XHJcbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIHtcclxuICBtYXJnaW46IDAgMCAxMnB4O1xyXG4gIHBhZGRpbmc6IDAgNXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbCB7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgbGFiZWwubGFiZWwtY2hlY2sge1xyXG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwIDEycHggMCAwO1xyXG4gIGZvbnQtc2l6ZTogMTJweDtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbC5sYWJlbC1jaGVjayBpbnB1dCB7XHJcbiAgbWFyZ2luOiAwIDZweCAwIDA7XHJcbiAgd2lkdGg6IDEwcHg7XHJcbiAgaGVpZ2h0OiAxMHB4O1xyXG59XHJcbi5idG46Zm9jdXMsIC5idG46aG92ZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDY5ZDkgIWltcG9ydGFudDtcclxufVxyXG4ubG9naW4tYnV0dG9uIHtcclxuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nOiAwIDIlO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgbWFyZ2luOiAxMHB4IDAgMCAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgYnV0dG9uIHtcclxuICAvKmJhY2tncm91bmQ6ICMwMDhjZmY7Ki9cclxuICAvLyBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgd2lkdGg6IDE2MHB4O1xyXG4gIHBhZGRpbmc6IDRweCAxMHB4IDdweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGJ1dHRvbjpmb2N1cyB7XHJcbiAgb3V0bGluZTogbm9uZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbn1cclxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU4MHB4KXtcclxuXHJcbn0iLCIubG9naW5fZm9ybSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtaW4taGVpZ2h0OiAxMDB2aDtcbiAgcGFkZGluZzogNTBweCAxNXB4O1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5sb2dpbi1sb2dvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDBweCAwIDIwcHg7XG59XG5cbi5sb2dpbi1sb2dvIGgyIHtcbiAgZm9udC1mYW1pbHk6IE5vdG8gU2FucztcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBsaW5lLWhlaWdodDogMzJweDtcbiAgY29sb3I6ICMyQjNCNTQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ2luLWxvZ28gaDIgc3BhbiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5sb2dpbl9mb3JtX3NldCB7XG4gIC8qYmFja2dyb3VuZDogI2ZmZjsqL1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOTUpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDtcbiAgd2lkdGg6IDU1MHB4O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBwYWRkaW5nOiAyMHB4IDE1cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3gtc2hhZG93OiAwIDAgMjBweCAtMTlweCAjMDAwO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAge1xuICBtYXJnaW46IDAgMCAxMnB4O1xuICBwYWRkaW5nOiAwIDVweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGxhYmVsIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbC5sYWJlbC1jaGVjayB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDAgMTJweCAwIDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbC5sYWJlbC1jaGVjayBpbnB1dCB7XG4gIG1hcmdpbjogMCA2cHggMCAwO1xuICB3aWR0aDogMTBweDtcbiAgaGVpZ2h0OiAxMHB4O1xufVxuXG4uYnRuOmZvY3VzLCAuYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwNjlkOSAhaW1wb3J0YW50O1xufVxuXG4ubG9naW4tYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAwIDIlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBtYXJnaW46IDEwcHggMCAwIDAgIWltcG9ydGFudDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgYnV0dG9uIHtcbiAgLypiYWNrZ3JvdW5kOiAjMDA4Y2ZmOyovXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbiAgd2lkdGg6IDE2MHB4O1xuICBwYWRkaW5nOiA0cHggMTBweCA3cHg7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGJ1dHRvbjpmb2N1cyB7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovIl19 */"

/***/ }),

/***/ "./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ArtistServicesAddUpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistServicesAddUpdateComponent", function() { return ArtistServicesAddUpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/modules/index-all.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../@core/backend/common/services/categories.service */ "./src/app/@core/backend/common/services/categories.service.ts");






var ArtistServicesAddUpdateComponent = /** @class */ (function () {
    function ArtistServicesAddUpdateComponent(router, toastr, categoriesService) {
        this.router = router;
        this.toastr = toastr;
        this.categoriesService = categoriesService;
        this.submit_btn = false;
        var user = JSON.parse(localStorage.getItem("user"));
        this.artist_id = user.id;
        var data = [];
        this.GetCategoriesDetails(data);
    }
    ;
    ArtistServicesAddUpdateComponent.prototype.ngOnInit = function () {
        if (!this.categoriesService.service_id) {
        }
        else {
            this.GetServiceDetails();
        }
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.GetCategoriesDetails = function (data) {
        var _this = this;
        this.categoriesService.GetCategoriesDetails(data).subscribe(function (result) {
            if (result.status) {
                _this.categories = result.data.categories;
            }
        });
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.selectedCategory = function (category) {
        this.subCategories = category.children;
        this.subCategoriesOpt = [];
        this.Single_Set_Option = [];
        this.Services = [];
        this.artist_service_ids = [];
        this.submit_btn = false;
        this.category_id = '';
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.selectedSubCategory = function (subCategory) {
        this.subCategoriesOpt = subCategory.children;
        this.Single_Set_Option = [];
        this.Services = [];
        this.artist_service_ids = [];
        this.submit_btn = false;
        this.category_id = '';
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.selectedSubCategoryOpt = function (option) {
        this.Services = [];
        this.category_id = '';
        this.artist_service_ids = [];
        if (option.type == 'custom') {
            this.category_id = option.id;
            this.artist_service_ids.push(option.id);
            this.submit_btn = true;
        }
        else {
            this.submit_btn = false;
            this.Single_Set_Option = option.children;
        }
        this.filter_services = [];
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.setServices = function (option) {
        this.category_id = option.id;
        this.artist_service_ids = [];
        this.Services = option.services;
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.onCheckboxChange = function (status, value) {
        if (status) {
            this.artist_service_ids.push(value);
        }
        else {
            var index = this.artist_service_ids.indexOf(value);
            if (index > -1) {
                this.artist_service_ids.splice(index, 1);
            }
        }
        if (this.artist_service_ids.length > 0) {
            this.submit_btn = true;
        }
        else {
            this.submit_btn = false;
        }
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.submit = function () {
        var _this = this;
        var count = 0;
        this.artist_service_ids.forEach(function (id) {
            var data = {
                "artist_service": {
                    "service_id": id,
                    "category_id": _this.category_id,
                    "description": "",
                    "artist_id": _this.artist_id,
                    "price": 100,
                    "number_of_days": 3,
                    "proof_of_work": ""
                }
            };
            _this.categoriesService.addService(data).subscribe(function (result) {
                count++;
                if (count >= _this.artist_service_ids.length) {
                    _this.toastr.success("Service Saved !!", result.message);
                    _this.router.navigate(['artist/services/list']);
                }
            });
        });
    };
    ;
    ArtistServicesAddUpdateComponent.prototype.GetServiceDetails = function () {
        var id = this.categoriesService.service_id;
        var data = [];
        this.categoriesService.GetCategoriesDetails(data).subscribe(function (result) {
            var categories = result.data.categories;
            var find_obj = underscore__WEBPACK_IMPORTED_MODULE_3__["findWhere"](categories, { id: id });
        });
    };
    ArtistServicesAddUpdateComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"] },
        { type: _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__["CategoriesService"] }
    ]; };
    ArtistServicesAddUpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-services-add-update',
            template: __webpack_require__(/*! raw-loader!./add-update-services.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.html"),
            styles: [__webpack_require__(/*! ./add-update-services.component.scss */ "./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"], _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__["CategoriesService"]])
    ], ArtistServicesAddUpdateComponent);
    return ArtistServicesAddUpdateComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist-services/artist-services.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/artist-services.component.ts ***!
  \***************************************************************************/
/*! exports provided: ArtistServicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistServicesComponent", function() { return ArtistServicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ArtistServicesComponent = /** @class */ (function () {
    function ArtistServicesComponent() {
    }
    ArtistServicesComponent.prototype.ngOnInit = function () {
    };
    ArtistServicesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-services',
            template: __webpack_require__(/*! raw-loader!./artist-services.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/artist-services.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ArtistServicesComponent);
    return ArtistServicesComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist-services/artist-services.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/artist-services.module.ts ***!
  \************************************************************************/
/*! exports provided: ArtistServicesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistServicesModule", function() { return ArtistServicesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm5/index.js");
/* harmony import */ var _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @nebular/eva-icons */ "./node_modules/@nebular/eva-icons/fesm5/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var ngx_chips__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-chips */ "./node_modules/ngx-chips/fesm5/ngx-chips.js");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _artist_services_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./artist-services.component */ "./src/app/@auth/artist/artist-services/artist-services.component.ts");
/* harmony import */ var _artist_services_routing__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./artist-services.routing */ "./src/app/@auth/artist/artist-services/artist-services.routing.ts");
/* harmony import */ var _list_artist_services_list_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./list/artist-services-list.component */ "./src/app/@auth/artist/artist-services/list/artist-services-list.component.ts");
/* harmony import */ var _add_update_services_add_update_services_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./add-update-services/add-update-services.component */ "./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.ts");
/* harmony import */ var material_dynamic_table__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! material-dynamic-table */ "./node_modules/material-dynamic-table/fesm5/material-dynamic-table.js");















var ArtistServicesModule = /** @class */ (function () {
    function ArtistServicesModule() {
    }
    ArtistServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbSpinnerModule"],
                ngx_chips__WEBPACK_IMPORTED_MODULE_6__["TagInputModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbCardModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbTreeGridModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbIconModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbInputModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbSearchModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbSelectModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbButtonModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbBadgeModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbTabsetModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbLayoutModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_7__["NgMultiSelectDropDownModule"].forRoot(),
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbDatepickerModule"].forRoot(),
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                _nebular_eva_icons__WEBPACK_IMPORTED_MODULE_3__["NbEvaIconsModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbSelectModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_2__["NbTooltipModule"],
                _artist_services_routing__WEBPACK_IMPORTED_MODULE_10__["ArtistServicesRoutingModule"],
                material_dynamic_table__WEBPACK_IMPORTED_MODULE_13__["DynamicTableModule"]
            ],
            declarations: [
                _artist_services_component__WEBPACK_IMPORTED_MODULE_9__["ArtistServicesComponent"],
                _list_artist_services_list_component__WEBPACK_IMPORTED_MODULE_11__["ArtistServicesListComponent"],
                _add_update_services_add_update_services_component__WEBPACK_IMPORTED_MODULE_12__["ArtistServicesAddUpdateComponent"]
            ],
            entryComponents: [],
        })
    ], ArtistServicesModule);
    return ArtistServicesModule;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist-services/artist-services.routing.ts":
/*!*************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/artist-services.routing.ts ***!
  \*************************************************************************/
/*! exports provided: ArtistServicesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistServicesRoutingModule", function() { return ArtistServicesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _artist_services_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./artist-services.component */ "./src/app/@auth/artist/artist-services/artist-services.component.ts");
/* harmony import */ var _list_artist_services_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./list/artist-services-list.component */ "./src/app/@auth/artist/artist-services/list/artist-services-list.component.ts");
/* harmony import */ var _add_update_services_add_update_services_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-update-services/add-update-services.component */ "./src/app/@auth/artist/artist-services/add-update-services/add-update-services.component.ts");







var routes = [{
        path: '',
        component: _artist_services_component__WEBPACK_IMPORTED_MODULE_4__["ArtistServicesComponent"],
        children: [
            {
                path: 'list',
                component: _list_artist_services_list_component__WEBPACK_IMPORTED_MODULE_5__["ArtistServicesListComponent"]
            },
            {
                path: 'add/update',
                component: _add_update_services_add_update_services_component__WEBPACK_IMPORTED_MODULE_6__["ArtistServicesAddUpdateComponent"]
            },
        ]
    }];
var ArtistServicesRoutingModule = /** @class */ (function () {
    function ArtistServicesRoutingModule() {
    }
    ArtistServicesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes), _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], ArtistServicesRoutingModule);
    return ArtistServicesRoutingModule;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist-services/list/artist-services-list.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/list/artist-services-list.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login_form {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  overflow-y: auto;\n  position: relative;\n}\n\n.login-logo {\n  text-align: center;\n  margin: 0px 0 10px;\n}\n\ninput[type=file] {\n  padding: 0;\n  border: none;\n  height: 32px;\n  margin: 0;\n}\n\n.login-logo h2 {\n  font-family: Noto Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 22px;\n  line-height: 32px;\n  color: #2B3B54;\n  text-align: center;\n}\n\n.login-logo h2 span {\n  text-transform: uppercase;\n}\n\n.login_form_set {\n  /*background: #fff;*/\n  background: rgba(255, 255, 255, 0.95);\n  border-radius: 10px 10px 0px 0px;\n  width: 100%;\n  box-sizing: border-box;\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  box-shadow: 0 0 20px -19px #000;\n}\n\n.login_form_set form {\n  width: 100%;\n  max-width: 100%;\n  margin: 0 auto;\n}\n\n.login_form_set form {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.login_form_set form .form-group {\n  width: 46%;\n  margin: 0 2% 12px;\n  position: relative;\n}\n\n.login_form_set form .form-group label {\n  margin: 0;\n  font-weight: bold;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  font-size: 11px;\n}\n\n.login_form_set form .form-group input[type=text],\n.login_form_set form .form-group input[type=email],\n.login_form_set form .form-group input[type=number] {\n  height: 32px;\n  line-height: 32px;\n}\n\n.login_form_set form .form-group input[type=file] {\n  margin: 4px 0 0 0;\n}\n\n.errmsg {\n  position: absolute;\n  color: red;\n  left: 0;\n  bottom: -18px;\n  font-size: 11px;\n  font-weight: bold;\n  transition: all 3s;\n}\n\n.gender {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.gender .gender-check {\n  cursor: pointer;\n  display: flex;\n  align-items: center;\n  height: 15px;\n  margin: 0 25px 0 0 !important;\n}\n\n.gender .gender-check input {\n  margin: 0 5pX 0 0;\n}\n\n.login-button {\n  width: 100% !important;\n  text-align: right;\n  padding: 0 2%;\n  box-sizing: border-box;\n  margin: 10px 0 0 0 !important;\n}\n\n.login_form_set form .form-group button {\n  /*background: #008cff;*/\n  background: #000;\n  border-radius: 6px;\n  color: #fff;\n  border: none;\n  width: 160px;\n  height: 32px;\n  line-height: 32px;\n  margin: 0 10px 0 0;\n}\n\n.form-group-100 {\n  width: 98% !important;\n}\n\n/* Responsive =================================================================*/\n\n@media screen and (max-width: 580px) {\n  .login_form {\n    padding: 50px 35px;\n  }\n\n  .login_form_set, .login_form_set form .form-group button {\n    width: 100%;\n    background: rgba(255, 255, 255, 0.75);\n  }\n\n  .login_form_set form {\n    flex-direction: column;\n  }\n\n  .login_form_set form .form-group {\n    width: 96%;\n  }\n\n  .gender .gender-check {\n    margin: 6px 25px 0 0 !important;\n  }\n}\n\n.record {\n  text-align: center;\n}\n\n.service {\n  margin: 10px;\n}\n\n.header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.about-shoe-servicec-list li {\n  width: 100%;\n  margin: 0 0 20px;\n  padding: 15px 15px 0;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.profile-descrition {\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n}\n\n.about-shoe-servicec-list li .profile-descrition figure {\n  width: 200px;\n  height: 200px;\n  overflow: hidden;\n  border-radius: 50%;\n  padding: 5px;\n  border: 5px solid #fff;\n  background: #fff;\n  margin: 0 30px 20px 0;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.about-shoe-servicec-list li .profile-descrition figure img {\n  width: 200px;\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.profile-view {\n  width: calc(100% - 230px);\n}\n\n.profile-review {\n  display: flex;\n  justify-content: space-between;\n  align-content: center;\n  margin: 18px 0 18px;\n}\n\n.rating-stars {\n  display: flex;\n  align-items: center;\n}\n\n.rating-stars span {\n  text-transform: uppercase;\n  font-weight: bold;\n  font-size: 20px;\n  margin: 0 10px 0 0;\n}\n\n.rating-stars .fa {\n  font-size: 20px;\n  margin: 0 3px 0 0;\n}\n\n.servicerequest {\n  background: #000;\n  color: #fff;\n  font-weight: normal;\n  height: 30px;\n  line-height: 28px;\n  text-transform: uppercase;\n  font-size: 13px;\n  padding: 0 15px;\n  border: none;\n  border-radius: 0;\n}\n\n.about-shoe-servicec-list li ul {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -1%;\n}\n\n.about-shoe-servicec-list li ul li {\n  width: 31.33%;\n  margin: 0 1% 15px;\n}\n\n.about-shoe-servicec-list li ul li figure {\n  height: 165px;\n}\n\n.about-shoe-servicec-list li ul li figure img {\n  width: 100%;\n  height: 165px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.editupdatlinks {\n  margin: 10px 0 12px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.editupdatlinks a .fa {\n  display: block;\n  color: #000;\n  font-size: 20px;\n}\n\n/* Responsive =========================*/\n\n@media screen and (max-width: 991px) {\n  .profile-descrition {\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n  }\n\n  .profile-view {\n    width: 100%;\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .about-shoe-servicec-list li ul li {\n    width: 48%;\n  }\n}\n\n@media screen and (max-width: 640px) {\n  .about-shoe-servicec-list li ul li {\n    width: 100%;\n    margin-left: 0;\n    margin-right: 0;\n  }\n}\n\n@media screen and (max-width: 480px) {\n  .profile-review {\n    flex-direction: column;\n  }\n\n  .servicerequest {\n    text-align: center;\n    margin: 10px 0 0 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2FydGlzdC1zZXJ2aWNlcy9saXN0L0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxhcnRpc3RcXGFydGlzdC1zZXJ2aWNlc1xcbGlzdFxcYXJ0aXN0LXNlcnZpY2VzLWxpc3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL0BhdXRoL2FydGlzdC9hcnRpc3Qtc2VydmljZXMvbGlzdC9hcnRpc3Qtc2VydmljZXMtbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFHQSxnQkFBQTtFQUNBLGtCQUFBO0FDRko7O0FESUU7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDREo7O0FER0U7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FDQUo7O0FERUU7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDRTtFQUNFLHlCQUFBO0FDRUo7O0FEQUU7RUFDRSxvQkFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFFQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLDhCQUFBO0VBQ0EsK0JBQUE7QUNFSjs7QURBRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ0dKOztBRERFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7QUNJSjs7QURGRTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDS0o7O0FESEU7RUFDRSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQ01KOztBREpFOzs7RUFHRSxZQUFBO0VBQ0EsaUJBQUE7QUNPSjs7QURMRTtFQUNFLGlCQUFBO0FDUUo7O0FETkU7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxPQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDU0o7O0FEUEU7RUFDRSxhQUFBO0VBQ0EsZUFBQTtBQ1VKOztBRFJFO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtBQ1dKOztBRFRFO0VBQ0UsaUJBQUE7QUNZSjs7QURWRTtFQUNFLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtBQ2FKOztBRFhFO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNjSjs7QURaRTtFQUNFLHFCQUFBO0FDZUo7O0FEYkUsZ0ZBQUE7O0FBQ0E7RUFDRTtJQUNFLGtCQUFBO0VDZ0JKOztFRGRFO0lBQ0UsV0FBQTtJQUNBLHFDQUFBO0VDaUJKOztFRGZFO0lBQ0Usc0JBQUE7RUNrQko7O0VEaEJFO0lBQ0UsVUFBQTtFQ21CSjs7RURqQkU7SUFDRSwrQkFBQTtFQ29CSjtBQUNGOztBRGpCRTtFQUNFLGtCQUFBO0FDbUJKOztBRGhCRTtFQUNFLFlBQUE7QUNtQko7O0FEaEJFO0VBQ0UsOEJBQUE7QUNtQko7O0FEakJFO0VBQ0Usa0JBQUE7QUNvQko7O0FEbEJFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7QUNxQko7O0FEbkJFO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0hBQUE7QUNzQko7O0FEcEJFO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsOEJBQUE7QUN1Qko7O0FEckJFO0VBQ0UsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLGdIQUFBO0FDd0JKOztBRHRCRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ3lCSjs7QUR2QkU7RUFDRSx5QkFBQTtBQzBCSjs7QUR4QkU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDMkJKOztBRHpCRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQzRCSjs7QUQxQkU7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDNkJKOztBRDNCRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzhCSjs7QUQ1QkU7RUFDRSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUMrQko7O0FEN0JFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0FDZ0NKOztBRDlCRTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtBQ2lDSjs7QUQvQkU7RUFDRSxhQUFBO0FDa0NKOztBRGhDRTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ21DSjs7QURqQ0U7RUFDRSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDb0NKOztBRGxDRTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQ3FDSjs7QURuQ0Usd0NBQUE7O0FBQ0E7RUFDQTtJQUNFLHNCQUFBO0lBQ0EsdUJBQUE7SUFDQSxtQkFBQTtFQ3NDRjs7RURwQ0E7SUFDRSxXQUFBO0VDdUNGO0FBQ0Y7O0FEckNFO0VBQ0E7SUFDSSxVQUFBO0VDdUNKO0FBQ0Y7O0FEckNFO0VBQ0E7SUFDSSxXQUFBO0lBQ0EsY0FBQTtJQUNBLGVBQUE7RUN1Q0o7QUFDRjs7QURyQ0U7RUFDQTtJQUNFLHNCQUFBO0VDdUNGOztFRHJDQTtJQUNFLGtCQUFBO0lBQ0Esa0JBQUE7RUN3Q0Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2FydGlzdC9hcnRpc3Qtc2VydmljZXMvbGlzdC9hcnRpc3Qtc2VydmljZXMtbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbl9mb3JtIHtcclxuICAgIC8vIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9pbWdfYmcwMS5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLy8gbWluLWhlaWdodDogMTAwdmg7XHJcbiAgICAvLyBwYWRkaW5nOiAwcHggMTVweDtcclxuICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5sb2dpbi1sb2dvIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMHB4IDAgMTBweDtcclxuICB9XHJcbiAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLmxvZ2luLWxvZ28gaDIge1xyXG4gICAgZm9udC1mYW1pbHk6IE5vdG8gU2FucztcclxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XHJcbiAgICBjb2xvcjogIzJCM0I1NDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLmxvZ2luLWxvZ28gaDIgc3BhbiB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQge1xyXG4gICAgLypiYWNrZ3JvdW5kOiAjZmZmOyovXHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOTUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgICAvLyBwYWRkaW5nOiAwcHggMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDIwcHggLTE5cHggIzAwMDtcclxuICB9XHJcbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICB9XHJcbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICB9XHJcbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAge1xyXG4gICAgd2lkdGg6IDQ2JTtcclxuICAgIG1hcmdpbjogMCAyJSAxMnB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPVwidGV4dFwiXSxcclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPVwiZW1haWxcIl0sXHJcbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT1cIm51bWJlclwiXSB7XHJcbiAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICBsaW5lLWhlaWdodDogMzJweDtcclxuICB9XHJcbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT1cImZpbGVcIl0ge1xyXG4gICAgbWFyZ2luOiA0cHggMCAwIDA7XHJcbiAgfVxyXG4gIC5lcnJtc2cge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICBib3R0b206IC0xOHB4O1xyXG4gICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgM3M7XHJcbiAgfVxyXG4gIC5nZW5kZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICB9XHJcbiAgLmdlbmRlciAuZ2VuZGVyLWNoZWNrIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgbWFyZ2luOiAwIDI1cHggMCAwICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5nZW5kZXIgLmdlbmRlci1jaGVjayBpbnB1dCB7XHJcbiAgICBtYXJnaW46IDAgNXBYIDAgMDtcclxuICB9XHJcbiAgLmxvZ2luLWJ1dHRvbiB7XHJcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBwYWRkaW5nOiAwIDIlO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIG1hcmdpbjogMTBweCAwIDAgMCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBidXR0b24ge1xyXG4gICAgLypiYWNrZ3JvdW5kOiAjMDA4Y2ZmOyovXHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB3aWR0aDogMTYwcHg7XHJcbiAgICBoZWlnaHQ6IDMycHg7XHJcbiAgICBsaW5lLWhlaWdodDogMzJweDtcclxuICAgIG1hcmdpbjogMCAxMHB4IDAgMDtcclxuICB9XHJcbiAgLmZvcm0tZ3JvdXAtMTAwIHtcclxuICAgIHdpZHRoOiA5OCUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTgwcHgpe1xyXG4gICAgLmxvZ2luX2Zvcm0ge1xyXG4gICAgICBwYWRkaW5nOiA1MHB4IDM1cHg7XHJcbiAgICB9XHJcbiAgICAubG9naW5fZm9ybV9zZXQsIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGJ1dHRvbiB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuNzUpO1xyXG4gICAgfVxyXG4gICAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0ge1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgfVxyXG4gICAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAge1xyXG4gICAgICB3aWR0aDogOTYlO1xyXG4gICAgfVxyXG4gICAgLmdlbmRlciAuZ2VuZGVyLWNoZWNrIHtcclxuICAgICAgbWFyZ2luOiA2cHggMjVweCAwIDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5yZWNvcmQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLnNlcnZpY2Uge1xyXG4gICAgbWFyZ2luOjEwcHg7XHJcbiAgfVxyXG5cclxuICAuaGVhZGVyLWluaXRpYWwge1xyXG4gICAgYmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjgpO1xyXG4gIH1cclxuICAjbWFpbiB7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDE1cHg7XHJcbiAgfVxyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3Qge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICB9XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMCAwIDIwcHg7XHJcbiAgICBwYWRkaW5nOiAxNXB4IDE1cHggMDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwwLDAsMC4wNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsLjIpXHJcbiAgfVxyXG4gIC5wcm9maWxlLWRlc2NyaXRpb24ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5wcm9maWxlLWRlc2NyaXRpb24gZmlndXJlIHtcclxuICAgIHdpZHRoOiAyMDBweDtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgI2ZmZjtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBtYXJnaW46IDAgMzBweCAyMHB4IDA7XHJcbiAgICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsLjIpXHJcbiAgfVxyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLnByb2ZpbGUtZGVzY3JpdGlvbiBmaWd1cmUgaW1nIHtcclxuICAgIHdpZHRoOiAyMDBweDtcclxuICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICB9XHJcbiAgLnByb2ZpbGUtdmlldyB7XHJcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMjMwcHgpO1xyXG4gIH1cclxuICAucHJvZmlsZS1yZXZpZXcge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMThweCAwIDE4cHg7XHJcbiAgfVxyXG4gIC5yYXRpbmctc3RhcnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5yYXRpbmctc3RhcnMgc3BhbiB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBtYXJnaW46IDAgMTBweCAwIDA7XHJcbiAgfVxyXG4gIC5yYXRpbmctc3RhcnMgLmZhIHtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIG1hcmdpbjogMCAzcHggMCAwO1xyXG4gIH1cclxuICAuc2VydmljZXJlcXVlc3Qge1xyXG4gICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyOHB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHBhZGRpbmc6IDAgMTVweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgfVxyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIG1hcmdpbjogMCAtMSU7XHJcbiAgfVxyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwgbGkge1xyXG4gICAgd2lkdGg6IDMxLjMzJTtcclxuICAgIG1hcmdpbjogMCAxJSAxNXB4O1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIGxpIGZpZ3VyZSB7XHJcbiAgICBoZWlnaHQ6IDE2NXB4O1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIGxpIGZpZ3VyZSBpbWcge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDE2NXB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgfVxyXG4gIC5lZGl0dXBkYXRsaW5rcyB7XHJcbiAgICBtYXJnaW46IDEwcHggMCAxMnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG4gIC5lZGl0dXBkYXRsaW5rcyBhIC5mYSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gIH1cclxuICAvKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09PT09PT0qL1xyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KXtcclxuICAucHJvZmlsZS1kZXNjcml0aW9uIHtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5wcm9maWxlLXZpZXcge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIH1cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XHJcbiAgICAgIHdpZHRoOiA0OCU7XHJcbiAgfVxyXG4gIH1cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2NDBweCl7XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XHJcbiAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBtYXJnaW4tbGVmdDogMDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gIH1cclxuICB9XHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDgwcHgpe1xyXG4gIC5wcm9maWxlLXJldmlldyB7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIH1cclxuICAuc2VydmljZXJlcXVlc3Qge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAxMHB4IDAgMCAwO1xyXG4gIH1cclxuICB9IiwiLmxvZ2luX2Zvcm0ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubG9naW4tbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwcHggMCAxMHB4O1xufVxuXG5pbnB1dFt0eXBlPWZpbGVdIHtcbiAgcGFkZGluZzogMDtcbiAgYm9yZGVyOiBub25lO1xuICBoZWlnaHQ6IDMycHg7XG4gIG1hcmdpbjogMDtcbn1cblxuLmxvZ2luLWxvZ28gaDIge1xuICBmb250LWZhbWlseTogTm90byBTYW5zO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDIycHg7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICBjb2xvcjogIzJCM0I1NDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubG9naW4tbG9nbyBoMiBzcGFuIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IHtcbiAgLypiYWNrZ3JvdW5kOiAjZmZmOyovXG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC45NSk7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGJveC1zaGFkb3c6IDAgMCAyMHB4IC0xOXB4ICMwMDA7XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCB7XG4gIHdpZHRoOiA0NiU7XG4gIG1hcmdpbjogMCAyJSAxMnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGxhYmVsIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMXB4O1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPXRleHRdLFxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT1lbWFpbF0sXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPW51bWJlcl0ge1xuICBoZWlnaHQ6IDMycHg7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPWZpbGVdIHtcbiAgbWFyZ2luOiA0cHggMCAwIDA7XG59XG5cbi5lcnJtc2cge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiByZWQ7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogLTE4cHg7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRyYW5zaXRpb246IGFsbCAzcztcbn1cblxuLmdlbmRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmdlbmRlciAuZ2VuZGVyLWNoZWNrIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDE1cHg7XG4gIG1hcmdpbjogMCAyNXB4IDAgMCAhaW1wb3J0YW50O1xufVxuXG4uZ2VuZGVyIC5nZW5kZXItY2hlY2sgaW5wdXQge1xuICBtYXJnaW46IDAgNXBYIDAgMDtcbn1cblxuLmxvZ2luLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwYWRkaW5nOiAwIDIlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBtYXJnaW46IDEwcHggMCAwIDAgIWltcG9ydGFudDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgYnV0dG9uIHtcbiAgLypiYWNrZ3JvdW5kOiAjMDA4Y2ZmOyovXG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbiAgd2lkdGg6IDE2MHB4O1xuICBoZWlnaHQ6IDMycHg7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICBtYXJnaW46IDAgMTBweCAwIDA7XG59XG5cbi5mb3JtLWdyb3VwLTEwMCB7XG4gIHdpZHRoOiA5OCUgIWltcG9ydGFudDtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1ODBweCkge1xuICAubG9naW5fZm9ybSB7XG4gICAgcGFkZGluZzogNTBweCAzNXB4O1xuICB9XG5cbiAgLmxvZ2luX2Zvcm1fc2V0LCAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBidXR0b24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43NSk7XG4gIH1cblxuICAubG9naW5fZm9ybV9zZXQgZm9ybSB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIHtcbiAgICB3aWR0aDogOTYlO1xuICB9XG5cbiAgLmdlbmRlciAuZ2VuZGVyLWNoZWNrIHtcbiAgICBtYXJnaW46IDZweCAyNXB4IDAgMCAhaW1wb3J0YW50O1xuICB9XG59XG4ucmVjb3JkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc2VydmljZSB7XG4gIG1hcmdpbjogMTBweDtcbn1cblxuLmhlYWRlci1pbml0aWFsIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjgpO1xufVxuXG4jbWFpbiB7XG4gIHBhZGRpbmc6IDQwcHggMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDAgMCAyMHB4O1xuICBwYWRkaW5nOiAxNXB4IDE1cHggMDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpO1xufVxuXG4ucHJvZmlsZS1kZXNjcml0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAucHJvZmlsZS1kZXNjcml0aW9uIGZpZ3VyZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbjogMCAzMHB4IDIwcHggMDtcbiAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMiksIDAuNXJlbSAwLjFyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKTtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAucHJvZmlsZS1kZXNjcml0aW9uIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4ucHJvZmlsZS12aWV3IHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDIzMHB4KTtcbn1cblxuLnByb2ZpbGUtcmV2aWV3IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbjogMThweCAwIDE4cHg7XG59XG5cbi5yYXRpbmctc3RhcnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4ucmF0aW5nLXN0YXJzIHNwYW4ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweCAwIDA7XG59XG5cbi5yYXRpbmctc3RhcnMgLmZhIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW46IDAgM3B4IDAgMDtcbn1cblxuLnNlcnZpY2VyZXF1ZXN0IHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGhlaWdodDogMzBweDtcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgcGFkZGluZzogMCAxNXB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIG1hcmdpbjogMCAtMSU7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwgbGkge1xuICB3aWR0aDogMzEuMzMlO1xuICBtYXJnaW46IDAgMSUgMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSBmaWd1cmUge1xuICBoZWlnaHQ6IDE2NXB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIGxpIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxNjVweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5lZGl0dXBkYXRsaW5rcyB7XG4gIG1hcmdpbjogMTBweCAwIDEycHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmVkaXR1cGRhdGxpbmtzIGEgLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAucHJvZmlsZS1kZXNjcml0aW9uIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cblxuICAucHJvZmlsZS12aWV3IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XG4gICAgd2lkdGg6IDQ4JTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjQwcHgpIHtcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0ODBweCkge1xuICAucHJvZmlsZS1yZXZpZXcge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAuc2VydmljZXJlcXVlc3Qge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDEwcHggMCAwIDA7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/artist/artist-services/list/artist-services-list.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/@auth/artist/artist-services/list/artist-services-list.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ArtistServicesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistServicesListComponent", function() { return ArtistServicesListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../@core/backend/common/services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../@core/backend/common/services/categories.service */ "./src/app/@core/backend/common/services/categories.service.ts");






var ArtistServicesListComponent = /** @class */ (function () {
    function ArtistServicesListComponent(artistService, router, categoriesService, toastrService) {
        this.artistService = artistService;
        this.router = router;
        this.categoriesService = categoriesService;
        this.toastrService = toastrService;
        this.record = false;
        this.user = JSON.parse(localStorage.getItem("user"));
    }
    ArtistServicesListComponent.prototype.ngOnInit = function () {
        this.GetArtistServicesById();
    };
    ArtistServicesListComponent.prototype.GetArtistServicesById = function () {
        var _this = this;
        var id = this.user.id;
        this.artistService.GetArtistServicesById(id).subscribe(function (result) {
            if (result.status) {
                _this.services = result.data;
                if (result.data.length > 0) {
                    _this.record = false;
                }
                else {
                    _this.record = true;
                }
            }
        });
    };
    ;
    ArtistServicesListComponent.prototype.Artist_Add_Service = function () {
        this.router.navigate(['artist/services/add/update']);
    };
    ArtistServicesListComponent.prototype.Delete_service = function (service) {
        var _this = this;
        this.categoriesService.deleteService(service.id).subscribe(function (result) {
            if (result.status) {
                _this.toastrService.success("Service Deleted !!", result.message);
                _this.GetArtistServicesById();
            }
        });
    };
    ArtistServicesListComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_2__["ArtistService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__["CategoriesService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] }
    ]; };
    ArtistServicesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-services-list',
            template: __webpack_require__(/*! raw-loader!./artist-services-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist-services/list/artist-services-list.component.html"),
            styles: [__webpack_require__(/*! ./artist-services-list.component.scss */ "./src/app/@auth/artist/artist-services/list/artist-services-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_2__["ArtistService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _core_backend_common_services_categories_service__WEBPACK_IMPORTED_MODULE_5__["CategoriesService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], ArtistServicesListComponent);
    return ArtistServicesListComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist.component.scss":
/*!****************************************************!*\
  !*** ./src/app/@auth/artist/artist.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2FydGlzdC9hcnRpc3QuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/@auth/artist/artist.component.ts":
/*!**************************************************!*\
  !*** ./src/app/@auth/artist/artist.component.ts ***!
  \**************************************************/
/*! exports provided: ArtistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistComponent", function() { return ArtistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");



var ArtistComponent = /** @class */ (function () {
    function ArtistComponent(utilService) {
        this.utilService = utilService;
        this.alive = true;
        this.loading = true;
        this.initMenu();
    }
    ArtistComponent.prototype.initMenu = function () {
    };
    ArtistComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    ArtistComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"] }
    ]; };
    ArtistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist',
            template: __webpack_require__(/*! raw-loader!./artist.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/artist.component.html"),
            styles: [__webpack_require__(/*! ./artist.component.scss */ "./src/app/@auth/artist/artist.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilService"]])
    ], ArtistComponent);
    return ArtistComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/artist.module.ts":
/*!***********************************************!*\
  !*** ./src/app/@auth/artist/artist.module.ts ***!
  \***********************************************/
/*! exports provided: ArtistModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistModule", function() { return ArtistModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _artist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./artist.component */ "./src/app/@auth/artist/artist.component.ts");
/* harmony import */ var _artist_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./artist-routing.module */ "./src/app/@auth/artist/artist-routing.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular2-multiselect-dropdown */ "./node_modules/angular2-multiselect-dropdown/fesm5/angular2-multiselect-dropdown.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./profile/profile.component */ "./src/app/@auth/artist/profile/profile.component.ts");
/* harmony import */ var _explore_explore_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./explore/explore.component */ "./src/app/@auth/artist/explore/explore.component.ts");
/* harmony import */ var _list_list_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./list/list.component */ "./src/app/@auth/artist/list/list.component.ts");
/* harmony import */ var _service_request_service_request_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./service-request/service-request.component */ "./src/app/@auth/artist/service-request/service-request.component.ts");
/* harmony import */ var _payment_method_payment_method_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./payment-method/payment-method.component */ "./src/app/@auth/artist/payment-method/payment-method.component.ts");
/* harmony import */ var _artist_services_artist_services_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./artist-services/artist-services.module */ "./src/app/@auth/artist/artist-services/artist-services.module.ts");











// components imports






var PAGES_COMPONENTS = [
    _artist_component__WEBPACK_IMPORTED_MODULE_2__["ArtistComponent"],
    _profile_profile_component__WEBPACK_IMPORTED_MODULE_11__["ProfileComponent"],
    _explore_explore_component__WEBPACK_IMPORTED_MODULE_12__["NgxArtistExploreComponent"],
    _list_list_component__WEBPACK_IMPORTED_MODULE_13__["NgxArtistListComponent"],
    _service_request_service_request_component__WEBPACK_IMPORTED_MODULE_14__["NgxArtistServiceRequestComponent"],
    _payment_method_payment_method_component__WEBPACK_IMPORTED_MODULE_15__["NgxArtistPaymentMethodComponent"],
    _service_request_service_request_component__WEBPACK_IMPORTED_MODULE_14__["NgxArtistServiceRequestComponent"]
];
var ArtistModule = /** @class */ (function () {
    function ArtistModule() {
    }
    ArtistModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_8__["CommonModule"],
                angular2_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_9__["AngularMultiSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSelectModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_6__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTabsModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_5__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatTooltipModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["Ng2SmartTableModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _artist_routing_module__WEBPACK_IMPORTED_MODULE_3__["ArtistRoutingModule"],
                _artist_services_artist_services_module__WEBPACK_IMPORTED_MODULE_16__["ArtistServicesModule"]
            ],
            declarations: PAGES_COMPONENTS.slice(),
            providers: [],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], ArtistModule);
    return ArtistModule;
}());



/***/ }),

/***/ "./src/app/@auth/artist/explore/explore.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/@auth/artist/explore/explore.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.explore-user-profile {\n  display: flex;\n  align-items: stretch;\n}\n\n.explore-user-profile .explore-filter {\n  width: 250px;\n  border: 1px solid #eaeaea;\n}\n\n.explore-user-profile .explore-filter .explore-search, .explore-user-profile .explore-details .explore-search {\n  padding: 10px;\n  background: #eaeaea;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.explore-user-profile .explore-filter .explore-search input, .explore-user-profile .explore-details .explore-search input {\n  width: 200px;\n  background: #fff;\n  border: none;\n  height: 35px;\n  line-height: 35px;\n  padding: 0 10px;\n  box-sizing: border-box;\n}\n\n.explore-user-profile .explore-filter .unregister-explore {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.explore-user-profile .explore-filter .unregister-explore a {\n  width: 50%;\n  box-sizing: border-box;\n  padding: 10px;\n  color: #000;\n  background: #fff;\n  text-align: center;\n  text-transform: uppercase;\n  font-size: 13px;\n  border: 1px solid #000;\n  transition: all 0.3s;\n}\n\n.active-service, .explore-user-profile .explore-filter .unregister-explore a:hover {\n  color: #fff !important;\n  background: #000 !important;\n}\n\n.explore-user-profile .explore-filter .unregister-explore a:hover {\n  background: #fff;\n  color: #000;\n}\n\n.explore-user-profile .explore-filter .price-range {\n  padding: 25px 21px 20px 17px;\n}\n\n.slider-labels {\n  margin-top: 15px;\n  display: flex;\n  font-size: 11px;\n  font-weight: bolder;\n  letter-spacing: 1px;\n  align-items: center;\n  justify-content: space-between;\n}\n\n#slider-range-value1 {\n  margin: 0 0 0 -6px;\n}\n\n#slider-range-value2 {\n  margin: 0 -12px 0 0;\n}\n\n.noUi-target, .noUi-target * {\n  -webkit-touch-callout: none;\n  -webkit-user-select: none;\n  touch-action: none;\n  -ms-user-select: none;\n  -moz-user-select: none;\n  user-select: none;\n  box-sizing: border-box;\n}\n\n.noUi-target {\n  position: relative;\n  direction: ltr;\n}\n\n.noUi-base {\n  width: 100%;\n  height: 100%;\n  position: relative;\n  z-index: 1;\n}\n\n.noUi-origin {\n  position: absolute;\n  right: 0;\n  top: 0;\n  left: 0;\n  bottom: 0;\n}\n\n.noUi-handle {\n  position: relative;\n  z-index: 1;\n}\n\n.noUi-stacking .noUi-handle {\n  z-index: 10;\n}\n\n.noUi-state-tap .noUi-origin {\n  transition: left 0.3s, top 0.3s;\n}\n\n.noUi-state-drag * {\n  cursor: inherit !important;\n}\n\n.noUi-base, .noUi-handle {\n  -webkit-transform: translate3d(0, 0, 0);\n  transform: translate3d(0, 0, 0);\n}\n\n.noUi-horizontal {\n  height: 4px;\n}\n\n.noUi-horizontal .noUi-handle {\n  width: 18px;\n  height: 18px;\n  border-radius: 50%;\n  left: -7px;\n  top: -7px;\n  background-color: #000;\n}\n\n.noUi-background {\n  background: #D6D7D9;\n}\n\n.noUi-connect {\n  background: #000;\n  transition: background 450ms;\n}\n\n.noUi-origin {\n  border-radius: 2px;\n}\n\n.noUi-target {\n  border-radius: 2px;\n}\n\n.noUi-draggable {\n  cursor: w-resize;\n}\n\n.noUi-vertical .noUi-draggable {\n  cursor: n-resize;\n}\n\n.noUi-handle {\n  cursor: default;\n  box-sizing: content-box !important;\n}\n\n.noUi-handle:active {\n  border: 8px solid #000;\n  border: 8px solid rgba(0, 0, 0, 0.2);\n  -webkit-background-clip: padding-box;\n  background-clip: padding-box;\n  left: -14px;\n  top: -14px;\n}\n\n[disabled].noUi-connect, [disabled] .noUi-connect {\n  background: #B8B8B8;\n}\n\n[disabled].noUi-origin, [disabled] .noUi-handle {\n  cursor: not-allowed;\n}\n\n.services {\n  padding: 0px 20;\n}\n\n.form-check-inline {\n  margin: 0px 10px !important;\n}\n\n.explore-user-profile .explore-filter .explore-categories ul {\n  padding: 10px 0 10px 0px;\n  min-height: 0;\n  height: 260px;\n  overflow-y: auto;\n}\n\n.explore-user-profile .explore-filter .explore-categories ul li {\n  padding: 0 0 0 30px;\n  margin: 0 0 10px;\n  font-weight: bold;\n}\n\n.price-range-head {\n  list-style-type: none;\n  padding: 10px;\n  background: #000;\n  color: #fff;\n}\n\n.explore-user-profile .explore-filter .explore-categories ul li a {\n  display: block;\n  font-weight: normal;\n  color: gray;\n  position: relative;\n  text-transform: capitalize;\n}\n\n.explore-user-profile .explore-filter .explore-categories ul li a::before {\n  position: absolute;\n  content: \"\";\n  left: -20px;\n  top: 10px;\n  width: 10px;\n  height: 1px;\n  background: #000;\n}\n\n.explore-user-profile .explore-filter .explore-categories ul li a:hover {\n  color: #000;\n}\n\n.explore-tags ul {\n  height: auto !important;\n}\n\n.explore-user-profile .explore-details {\n  width: calc(100% - 250px);\n  padding: 0 0 0 20px;\n}\n\n.explore-details ul {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -1%;\n  align-items: stretch;\n}\n\n.explore-details ul li {\n  width: 31.33%;\n  margin: 0 1% 15px;\n}\n\n.explore-details ul li .listing-details {\n  border: 1px solid #eaeaea;\n}\n\n.explore-details ul li .listing-details a {\n  padding: 10px 10px 0;\n}\n\n.explore-details ul li a figure {\n  height: 180px;\n  margin: 0 0 5px 0;\n}\n\n.explore-details ul li a figure img {\n  width: 100%;\n  height: 180px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.editupdatlinks {\n  margin: 0 0 12px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.editupdatlinks a .fa {\n  display: block;\n  color: #000;\n  font-size: 20px;\n}\n\n.listing-details a {\n  display: flex;\n  flex-wrap: wrap;\n  color: #000;\n}\n\n.listing-name, .listing-username {\n  width: calc(100% - 90px);\n  font-weight: bolder;\n  font-size: 15px;\n  line-height: 25px;\n}\n\n.listing-price, .listing-rating {\n  width: 90px;\n  line-height: 25px;\n  text-align: right;\n}\n\n.hide {\n  display: none;\n}\n\n/*responsive*/\n\n@media screen and (max-width: 991px) {\n  .explore-details ul li {\n    width: 48%;\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .explore-details ul li {\n    width: 98%;\n  }\n}\n\n@media screen and (max-width: 640px) {\n  .explore-user-profile {\n    flex-direction: column;\n  }\n\n  .explore-user-profile .explore-filter {\n    width: 100%;\n  }\n\n  .explore-user-profile .explore-details {\n    width: 100%;\n    padding: 0;\n  }\n\n  .explore-details ul li {\n    width: 98%;\n  }\n\n  .explore-tags ul {\n    margin: 0;\n    padding: 0 !important;\n    height: auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2V4cGxvcmUvQzpcXFVzZXJzXFxERUxMXFxEZXNrdG9wXFxmaXhteWtpeC9zcmNcXGFwcFxcQGF1dGhcXGFydGlzdFxcZXhwbG9yZVxcZXhwbG9yZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2V4cGxvcmUvZXhwbG9yZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLDhCQUFBO0FDQ0Q7O0FEQ0E7RUFDRSxrQkFBQTtBQ0VGOztBREFBO0VBQ0MsYUFBQTtFQUNBLG9CQUFBO0FDR0Q7O0FEREE7RUFDQyxZQUFBO0VBQ0EseUJBQUE7QUNJRDs7QURGQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDS0Q7O0FESEE7RUFDQyxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0FDTUQ7O0FESkE7RUFDQyxhQUFBO0VBQ0EsZUFBQTtBQ09EOztBRExBO0VBQ0MsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQ1FEOztBRE5BO0VBQ0Msc0JBQUE7RUFDQSwyQkFBQTtBQ1NEOztBRFBBO0VBQ0MsZ0JBQUE7RUFDQSxXQUFBO0FDVUQ7O0FEUkE7RUFDQyw0QkFBQTtBQ1dEOztBRFRBO0VBQ0UsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDWUY7O0FEVkE7RUFDQyxrQkFBQTtBQ2FEOztBRFhBO0VBQ0MsbUJBQUE7QUNjRDs7QURaQTtFQUNFLDJCQUFBO0VBQ0EseUJBQUE7RUFFQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUVBLHNCQUFBO0FDZUY7O0FEYkE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7QUNnQkY7O0FEZEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQ2lCRjs7QURmQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtBQ2tCRjs7QURoQkE7RUFDRSxrQkFBQTtFQUNBLFVBQUE7QUNtQkY7O0FEakJBO0VBQ0UsV0FBQTtBQ29CRjs7QURsQkE7RUFFRSwrQkFBQTtBQ3FCRjs7QURsQkE7RUFDRSwwQkFBQTtBQ3FCRjs7QURuQkE7RUFDRSx1Q0FBQTtFQUNBLCtCQUFBO0FDc0JGOztBRHBCQTtFQUNFLFdBQUE7QUN1QkY7O0FEckJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0Esc0JBQUE7QUN3QkY7O0FEdEJBO0VBQ0UsbUJBQUE7QUN5QkY7O0FEdkJBO0VBQ0UsZ0JBQUE7RUFFQSw0QkFBQTtBQzBCRjs7QUR4QkE7RUFDRSxrQkFBQTtBQzJCRjs7QUR6QkE7RUFDRSxrQkFBQTtBQzRCRjs7QUR4QkE7RUFDRSxnQkFBQTtBQzJCRjs7QUR6QkE7RUFDRSxnQkFBQTtBQzRCRjs7QUQxQkE7RUFDRSxlQUFBO0VBR0Esa0NBQUE7QUM2QkY7O0FEM0JBO0VBQ0Usc0JBQUE7RUFDQSxvQ0FBQTtFQUNBLG9DQUFBO0VBQ0EsNEJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQzhCRjs7QUQ1QkE7RUFDRSxtQkFBQTtBQytCRjs7QUQ3QkE7RUFDRSxtQkFBQTtBQ2dDRjs7QUQ3QkE7RUFDQyxlQUFBO0FDZ0NEOztBRDdCQTtFQUNDLDJCQUFBO0FDZ0NEOztBRDdCQTtFQUNDLHdCQUFBO0VBQ0EsYUFBQTtFQUNDLGFBQUE7RUFDQSxnQkFBQTtBQ2dDRjs7QUQ5QkE7RUFDQyxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNpQ0Q7O0FEL0JBO0VBQ0MscUJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDa0NEOztBRGhDQTtFQUNDLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0FDbUNEOztBRGpDQTtFQUNDLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ29DRDs7QURsQ0E7RUFDQyxXQUFBO0FDcUNEOztBRG5DQTtFQUNDLHVCQUFBO0FDc0NEOztBRHBDQTtFQUNDLHlCQUFBO0VBQ0EsbUJBQUE7QUN1Q0Q7O0FEckNBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7QUN3Q0Q7O0FEdENBO0VBQ0MsYUFBQTtFQUNBLGlCQUFBO0FDeUNEOztBRHZDQTtFQUNDLHlCQUFBO0FDMENEOztBRHhDQTtFQUNDLG9CQUFBO0FDMkNEOztBRHpDQTtFQUNDLGFBQUE7RUFDQSxpQkFBQTtBQzRDRDs7QUQxQ0E7RUFDQyxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUM2Q0Q7O0FEM0NBO0VBQ0MsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQzhDRDs7QUQ1Q0E7RUFDQyxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUMrQ0Q7O0FEN0NBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FDZ0REOztBRDlDQTtFQUNDLHdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNpREQ7O0FEL0NBO0VBQ0MsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNrREQ7O0FEaERBO0VBQ0MsYUFBQTtBQ21ERDs7QURqREEsYUFBQTs7QUFDQTtFQUNDO0lBQ0MsVUFBQTtFQ29EQTtBQUNGOztBRGxEQTtFQUNDO0lBQ0MsVUFBQTtFQ29EQTtBQUNGOztBRGxEQTtFQUNDO0lBQ0Msc0JBQUE7RUNvREE7O0VEbEREO0lBQ0MsV0FBQTtFQ3FEQTs7RURuREQ7SUFDQyxXQUFBO0lBQ0EsVUFBQTtFQ3NEQTs7RURwREQ7SUFDQyxVQUFBO0VDdURBOztFRHJERDtJQUNDLFNBQUE7SUFDQSxxQkFBQTtJQUNBLFlBQUE7RUN3REE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2FydGlzdC9leHBsb3JlL2V4cGxvcmUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWluaXRpYWwge1xyXG5cdGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxufVxyXG4jbWFpbiB7XHJcbiAgcGFkZGluZzogNDBweCAxNXB4O1xyXG59XHJcbi5leHBsb3JlLXVzZXItcHJvZmlsZSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogc3RyZXRjaDtcclxufVxyXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIHtcclxuXHR3aWR0aDogMjUwcHg7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgI2VhZWFlYTtcclxufVxyXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5leHBsb3JlLXNlYXJjaCwgLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWRldGFpbHMgLmV4cGxvcmUtc2VhcmNoIHtcclxuXHRwYWRkaW5nOiAxMHB4O1xyXG5cdGJhY2tncm91bmQ6ICNlYWVhZWE7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5leHBsb3JlLXNlYXJjaCBpbnB1dCwgLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWRldGFpbHMgLmV4cGxvcmUtc2VhcmNoIGlucHV0IHtcclxuXHR3aWR0aDogMjAwcHg7XHJcblx0YmFja2dyb3VuZDogI2ZmZjtcclxuXHRib3JkZXI6IG5vbmU7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdGxpbmUtaGVpZ2h0OiAzNXB4O1xyXG5cdHBhZGRpbmc6IDAgMTBweDtcclxuXHRib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLnVucmVnaXN0ZXItZXhwbG9yZSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAudW5yZWdpc3Rlci1leHBsb3JlIGEge1xyXG5cdHdpZHRoOiA1MCU7XHJcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHRwYWRkaW5nOiAxMHB4O1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XHJcblx0dHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbn1cclxuLmFjdGl2ZS1zZXJ2aWNlLCAuZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC51bnJlZ2lzdGVyLWV4cGxvcmUgYTpob3ZlciB7XHJcblx0Y29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAudW5yZWdpc3Rlci1leHBsb3JlIGE6aG92ZXIge1xyXG5cdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0Y29sb3I6ICMwMDA7XHJcbn1cclxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAucHJpY2UtcmFuZ2Uge1xyXG5cdHBhZGRpbmc6IDI1cHggMjFweCAyMHB4IDE3cHg7XHJcbn1cclxuLnNsaWRlci1sYWJlbHMge1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmb250LXNpemU6IDExcHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbiNzbGlkZXItcmFuZ2UtdmFsdWUxIHtcclxuXHRtYXJnaW46IDAgMCAwIC02cHg7XHJcbn1cclxuI3NsaWRlci1yYW5nZS12YWx1ZTIge1xyXG5cdG1hcmdpbjogMCAtMTJweCAwIDA7XHJcbn1cclxuLm5vVWktdGFyZ2V0LC5ub1VpLXRhcmdldCAqIHtcclxuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XHJcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuICAtbXMtdG91Y2gtYWN0aW9uOiBub25lO1xyXG4gIHRvdWNoLWFjdGlvbjogbm9uZTtcclxuICAtbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuICB1c2VyLXNlbGVjdDogbm9uZTtcclxuICAtbW96LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxufVxyXG4ubm9VaS10YXJnZXQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXJlY3Rpb246IGx0cjtcclxufVxyXG4ubm9VaS1iYXNlIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuLm5vVWktb3JpZ2luIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgYm90dG9tOiAwO1xyXG59XHJcbi5ub1VpLWhhbmRsZSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHotaW5kZXg6IDE7XHJcbn1cclxuLm5vVWktc3RhY2tpbmcgLm5vVWktaGFuZGxlIHtcclxuICB6LWluZGV4OiAxMDtcclxufVxyXG4ubm9VaS1zdGF0ZS10YXAgLm5vVWktb3JpZ2luIHtcclxuICAtd2Via2l0LXRyYW5zaXRpb246IGxlZnQgMC4zcyx0b3AgLjNzO1xyXG4gIHRyYW5zaXRpb246IGxlZnQgMC4zcyx0b3AgLjNzO1xyXG59XHJcblxyXG4ubm9VaS1zdGF0ZS1kcmFnICoge1xyXG4gIGN1cnNvcjogaW5oZXJpdCAhaW1wb3J0YW50O1xyXG59XHJcbi5ub1VpLWJhc2UsLm5vVWktaGFuZGxlIHtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwwLDApO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwwLDApO1xyXG59XHJcbi5ub1VpLWhvcml6b250YWwge1xyXG4gIGhlaWdodDogNHB4O1xyXG59XHJcbi5ub1VpLWhvcml6b250YWwgLm5vVWktaGFuZGxlIHtcclxuICB3aWR0aDogMThweDtcclxuICBoZWlnaHQ6IDE4cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIGxlZnQ6IC03cHg7XHJcbiAgdG9wOiAtN3B4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbn1cclxuLm5vVWktYmFja2dyb3VuZCB7XHJcbiAgYmFja2dyb3VuZDogI0Q2RDdEOTtcclxufVxyXG4ubm9VaS1jb25uZWN0IHtcclxuICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYmFja2dyb3VuZCA0NTBtcztcclxuICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIDQ1MG1zO1xyXG59XHJcbi5ub1VpLW9yaWdpbiB7XHJcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG59XHJcbi5ub1VpLXRhcmdldCB7XHJcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG59XHJcbi5ub1VpLXRhcmdldC5ub1VpLWNvbm5lY3Qge1xyXG59XHJcbi5ub1VpLWRyYWdnYWJsZSB7XHJcbiAgY3Vyc29yOiB3LXJlc2l6ZTtcclxufVxyXG4ubm9VaS12ZXJ0aWNhbCAubm9VaS1kcmFnZ2FibGUge1xyXG4gIGN1cnNvcjogbi1yZXNpemU7XHJcbn1cclxuLm5vVWktaGFuZGxlIHtcclxuICBjdXJzb3I6IGRlZmF1bHQ7XHJcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveCAhaW1wb3J0YW50O1xyXG4gIC1tb3otYm94LXNpemluZzogY29udGVudC1ib3ggIWltcG9ydGFudDtcclxuICBib3gtc2l6aW5nOiBjb250ZW50LWJveCAhaW1wb3J0YW50O1xyXG59XHJcbi5ub1VpLWhhbmRsZTphY3RpdmUge1xyXG4gIGJvcmRlcjogOHB4IHNvbGlkICMwMDA7XHJcbiAgYm9yZGVyOiA4cHggc29saWQgcmdiYSgwLDAsMCwwLjIpO1xyXG4gIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcclxuICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xyXG4gIGxlZnQ6IC0xNHB4O1xyXG4gIHRvcDogLTE0cHg7XHJcbn1cclxuW2Rpc2FibGVkXS5ub1VpLWNvbm5lY3QsW2Rpc2FibGVkXSAubm9VaS1jb25uZWN0IHtcclxuICBiYWNrZ3JvdW5kOiAjQjhCOEI4O1xyXG59XHJcbltkaXNhYmxlZF0ubm9VaS1vcmlnaW4sW2Rpc2FibGVkXSAubm9VaS1oYW5kbGUge1xyXG4gIGN1cnNvcjogbm90LWFsbG93ZWQ7XHJcbn1cclxuXHJcbi5zZXJ2aWNlcyB7XHJcblx0cGFkZGluZzowcHggMjA7XHJcbn1cclxuXHJcbi5mb3JtLWNoZWNrLWlubGluZSB7XHJcblx0bWFyZ2luOjBweCAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCB7XHJcblx0cGFkZGluZzogMTBweCAwIDEwcHggMHB4O1xyXG5cdG1pbi1oZWlnaHQ6IDA7XHJcbiAgaGVpZ2h0OiAyNjBweDtcclxuICBvdmVyZmxvdy15OiBhdXRvO1xyXG59XHJcbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCBsaSB7XHJcblx0cGFkZGluZzogMCAwIDAgMzBweDtcclxuXHRtYXJnaW46IDAgMCAxMHB4O1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5wcmljZS1yYW5nZS1oZWFkIHtcclxuXHRsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcblx0cGFkZGluZzogMTBweDtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG5cdGNvbG9yOiAjZmZmO1xyXG59XHJcbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCBsaSBhIHtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRmb250LXdlaWdodDogbm9ybWFsO1xyXG5cdGNvbG9yOiBncmF5O1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5leHBsb3JlLWNhdGVnb3JpZXMgdWwgbGkgYTo6YmVmb3JlIHtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0Y29udGVudDogJyc7XHJcblx0bGVmdDogLTIwcHg7XHJcblx0dG9wOiAxMHB4O1xyXG5cdHdpZHRoOiAxMHB4O1xyXG5cdGhlaWdodDogMXB4O1xyXG5cdGJhY2tncm91bmQ6ICMwMDA7XHJcbn1cclxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAuZXhwbG9yZS1jYXRlZ29yaWVzIHVsIGxpIGE6aG92ZXIge1xyXG5cdGNvbG9yOiAjMDAwO1xyXG59XHJcbi5leHBsb3JlLXRhZ3MgdWx7XHJcblx0aGVpZ2h0OmF1dG8gIWltcG9ydGFudDtcclxufVxyXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZGV0YWlscyB7XHJcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDI1MHB4KTtcclxuXHRwYWRkaW5nOiAwIDAgMCAyMHB4O1xyXG59XHJcbi5leHBsb3JlLWRldGFpbHMgdWwge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdG1hcmdpbjogMCAtMSU7XHJcblx0YWxpZ24taXRlbXM6IHN0cmV0Y2g7XHJcbn1cclxuLmV4cGxvcmUtZGV0YWlscyB1bCBsaSB7XHJcblx0d2lkdGg6IDMxLjMzJTtcclxuXHRtYXJnaW46IDAgMSUgMTVweDtcclxufVxyXG4uZXhwbG9yZS1kZXRhaWxzIHVsIGxpIC5saXN0aW5nLWRldGFpbHMge1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkICNlYWVhZWE7XHJcbn1cclxuLmV4cGxvcmUtZGV0YWlscyB1bCBsaSAubGlzdGluZy1kZXRhaWxzIGEge1xyXG5cdHBhZGRpbmc6IDEwcHggMTBweCAwO1xyXG59XHJcbi5leHBsb3JlLWRldGFpbHMgdWwgbGkgYSBmaWd1cmUge1xyXG5cdGhlaWdodDogMTgwcHg7XHJcblx0bWFyZ2luOiAwIDAgNXB4IDA7XHJcbn1cclxuLmV4cGxvcmUtZGV0YWlscyB1bCBsaSBhIGZpZ3VyZSBpbWcge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGhlaWdodDogMTgwcHg7XHJcblx0b2JqZWN0LWZpdDogY292ZXI7XHJcbn1cclxuLmVkaXR1cGRhdGxpbmtzIHtcclxuXHRtYXJnaW46IDAgMCAxMnB4O1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLmVkaXR1cGRhdGxpbmtzIGEgLmZhIHtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRjb2xvcjogIzAwMDtcclxuXHRmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLmxpc3RpbmctZGV0YWlscyBhIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtd3JhcDogd3JhcDtcclxuXHRjb2xvcjogIzAwMDtcclxufVxyXG4ubGlzdGluZy1uYW1lLCAubGlzdGluZy11c2VybmFtZSB7XHJcblx0d2lkdGg6IGNhbGMoMTAwJSAtIDkwcHgpO1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcblx0Zm9udC1zaXplOiAxNXB4O1xyXG5cdGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG59XHJcbi5saXN0aW5nLXByaWNlLCAubGlzdGluZy1yYXRpbmcge1xyXG5cdHdpZHRoOiA5MHB4O1xyXG5cdGxpbmUtaGVpZ2h0OiAyNXB4O1xyXG5cdHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5oaWRlIHtcclxuXHRkaXNwbGF5OiBub25lO1xyXG59XHJcbi8qcmVzcG9uc2l2ZSovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KXtcclxuXHQuZXhwbG9yZS1kZXRhaWxzIHVsIGxpIHtcclxuXHRcdHdpZHRoOiA0OCU7XHJcblx0fVx0XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG5cdC5leHBsb3JlLWRldGFpbHMgdWwgbGkge1xyXG5cdFx0d2lkdGg6IDk4JTtcclxuXHR9XHRcclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2NDBweCl7XHJcblx0LmV4cGxvcmUtdXNlci1wcm9maWxlIHtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0fVxyXG5cdC5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIge1xyXG5cdFx0d2lkdGg6IDEwMCVcclxuXHR9XHJcblx0LmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWRldGFpbHMge1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRwYWRkaW5nOiAwO1xyXG5cdH1cclxuXHQuZXhwbG9yZS1kZXRhaWxzIHVsIGxpIHtcclxuXHRcdHdpZHRoOiA5OCU7XHJcblx0fVxyXG5cdC5leHBsb3JlLXRhZ3MgdWwge1xyXG5cdFx0bWFyZ2luOiAwO1xyXG5cdFx0cGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG5cdFx0aGVpZ2h0OiAgYXV0bztcclxuXHR9XHJcbn0iLCIuaGVhZGVyLWluaXRpYWwge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG59XG5cbiNtYWluIHtcbiAgcGFkZGluZzogNDBweCAxNXB4O1xufVxuXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogc3RyZXRjaDtcbn1cblxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciB7XG4gIHdpZHRoOiAyNTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2VhZWFlYTtcbn1cblxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAuZXhwbG9yZS1zZWFyY2gsIC5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1kZXRhaWxzIC5leHBsb3JlLXNlYXJjaCB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQ6ICNlYWVhZWE7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAuZXhwbG9yZS1zZWFyY2ggaW5wdXQsIC5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1kZXRhaWxzIC5leHBsb3JlLXNlYXJjaCBpbnB1dCB7XG4gIHdpZHRoOiAyMDBweDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYm9yZGVyOiBub25lO1xuICBoZWlnaHQ6IDM1cHg7XG4gIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICBwYWRkaW5nOiAwIDEwcHg7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLnVucmVnaXN0ZXItZXhwbG9yZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWZpbHRlciAudW5yZWdpc3Rlci1leHBsb3JlIGEge1xuICB3aWR0aDogNTAlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBwYWRkaW5nOiAxMHB4O1xuICBjb2xvcjogIzAwMDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEzcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjNzO1xufVxuXG4uYWN0aXZlLXNlcnZpY2UsIC5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLnVucmVnaXN0ZXItZXhwbG9yZSBhOmhvdmVyIHtcbiAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogIzAwMCAhaW1wb3J0YW50O1xufVxuXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC51bnJlZ2lzdGVyLWV4cGxvcmUgYTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjMDAwO1xufVxuXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5wcmljZS1yYW5nZSB7XG4gIHBhZGRpbmc6IDI1cHggMjFweCAyMHB4IDE3cHg7XG59XG5cbi5zbGlkZXItbGFiZWxzIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbiNzbGlkZXItcmFuZ2UtdmFsdWUxIHtcbiAgbWFyZ2luOiAwIDAgMCAtNnB4O1xufVxuXG4jc2xpZGVyLXJhbmdlLXZhbHVlMiB7XG4gIG1hcmdpbjogMCAtMTJweCAwIDA7XG59XG5cbi5ub1VpLXRhcmdldCwgLm5vVWktdGFyZ2V0ICoge1xuICAtd2Via2l0LXRvdWNoLWNhbGxvdXQ6IG5vbmU7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC1tcy10b3VjaC1hY3Rpb246IG5vbmU7XG4gIHRvdWNoLWFjdGlvbjogbm9uZTtcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xuICAtbW96LXVzZXItc2VsZWN0OiBub25lO1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuXG4ubm9VaS10YXJnZXQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpcmVjdGlvbjogbHRyO1xufVxuXG4ubm9VaS1iYXNlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xufVxuXG4ubm9VaS1vcmlnaW4ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbn1cblxuLm5vVWktaGFuZGxlIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB6LWluZGV4OiAxO1xufVxuXG4ubm9VaS1zdGFja2luZyAubm9VaS1oYW5kbGUge1xuICB6LWluZGV4OiAxMDtcbn1cblxuLm5vVWktc3RhdGUtdGFwIC5ub1VpLW9yaWdpbiB7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogbGVmdCAwLjNzLCB0b3AgMC4zcztcbiAgdHJhbnNpdGlvbjogbGVmdCAwLjNzLCB0b3AgMC4zcztcbn1cblxuLm5vVWktc3RhdGUtZHJhZyAqIHtcbiAgY3Vyc29yOiBpbmhlcml0ICFpbXBvcnRhbnQ7XG59XG5cbi5ub1VpLWJhc2UsIC5ub1VpLWhhbmRsZSB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcbn1cblxuLm5vVWktaG9yaXpvbnRhbCB7XG4gIGhlaWdodDogNHB4O1xufVxuXG4ubm9VaS1ob3Jpem9udGFsIC5ub1VpLWhhbmRsZSB7XG4gIHdpZHRoOiAxOHB4O1xuICBoZWlnaHQ6IDE4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgbGVmdDogLTdweDtcbiAgdG9wOiAtN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xufVxuXG4ubm9VaS1iYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZDogI0Q2RDdEOTtcbn1cblxuLm5vVWktY29ubmVjdCB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogYmFja2dyb3VuZCA0NTBtcztcbiAgdHJhbnNpdGlvbjogYmFja2dyb3VuZCA0NTBtcztcbn1cblxuLm5vVWktb3JpZ2luIHtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xufVxuXG4ubm9VaS10YXJnZXQge1xuICBib3JkZXItcmFkaXVzOiAycHg7XG59XG5cbi5ub1VpLWRyYWdnYWJsZSB7XG4gIGN1cnNvcjogdy1yZXNpemU7XG59XG5cbi5ub1VpLXZlcnRpY2FsIC5ub1VpLWRyYWdnYWJsZSB7XG4gIGN1cnNvcjogbi1yZXNpemU7XG59XG5cbi5ub1VpLWhhbmRsZSB7XG4gIGN1cnNvcjogZGVmYXVsdDtcbiAgLXdlYmtpdC1ib3gtc2l6aW5nOiBjb250ZW50LWJveCAhaW1wb3J0YW50O1xuICAtbW96LWJveC1zaXppbmc6IGNvbnRlbnQtYm94ICFpbXBvcnRhbnQ7XG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94ICFpbXBvcnRhbnQ7XG59XG5cbi5ub1VpLWhhbmRsZTphY3RpdmUge1xuICBib3JkZXI6IDhweCBzb2xpZCAjMDAwO1xuICBib3JkZXI6IDhweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcbiAgYmFja2dyb3VuZC1jbGlwOiBwYWRkaW5nLWJveDtcbiAgbGVmdDogLTE0cHg7XG4gIHRvcDogLTE0cHg7XG59XG5cbltkaXNhYmxlZF0ubm9VaS1jb25uZWN0LCBbZGlzYWJsZWRdIC5ub1VpLWNvbm5lY3Qge1xuICBiYWNrZ3JvdW5kOiAjQjhCOEI4O1xufVxuXG5bZGlzYWJsZWRdLm5vVWktb3JpZ2luLCBbZGlzYWJsZWRdIC5ub1VpLWhhbmRsZSB7XG4gIGN1cnNvcjogbm90LWFsbG93ZWQ7XG59XG5cbi5zZXJ2aWNlcyB7XG4gIHBhZGRpbmc6IDBweCAyMDtcbn1cblxuLmZvcm0tY2hlY2staW5saW5lIHtcbiAgbWFyZ2luOiAwcHggMTBweCAhaW1wb3J0YW50O1xufVxuXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5leHBsb3JlLWNhdGVnb3JpZXMgdWwge1xuICBwYWRkaW5nOiAxMHB4IDAgMTBweCAwcHg7XG4gIG1pbi1oZWlnaHQ6IDA7XG4gIGhlaWdodDogMjYwcHg7XG4gIG92ZXJmbG93LXk6IGF1dG87XG59XG5cbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCBsaSB7XG4gIHBhZGRpbmc6IDAgMCAwIDMwcHg7XG4gIG1hcmdpbjogMCAwIDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ucHJpY2UtcmFuZ2UtaGVhZCB7XG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCBsaSBhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiBncmF5O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZXhwbG9yZS11c2VyLXByb2ZpbGUgLmV4cGxvcmUtZmlsdGVyIC5leHBsb3JlLWNhdGVnb3JpZXMgdWwgbGkgYTo6YmVmb3JlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb250ZW50OiBcIlwiO1xuICBsZWZ0OiAtMjBweDtcbiAgdG9wOiAxMHB4O1xuICB3aWR0aDogMTBweDtcbiAgaGVpZ2h0OiAxcHg7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIgLmV4cGxvcmUtY2F0ZWdvcmllcyB1bCBsaSBhOmhvdmVyIHtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi5leHBsb3JlLXRhZ3MgdWwge1xuICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbn1cblxuLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWRldGFpbHMge1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjUwcHgpO1xuICBwYWRkaW5nOiAwIDAgMCAyMHB4O1xufVxuXG4uZXhwbG9yZS1kZXRhaWxzIHVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBtYXJnaW46IDAgLTElO1xuICBhbGlnbi1pdGVtczogc3RyZXRjaDtcbn1cblxuLmV4cGxvcmUtZGV0YWlscyB1bCBsaSB7XG4gIHdpZHRoOiAzMS4zMyU7XG4gIG1hcmdpbjogMCAxJSAxNXB4O1xufVxuXG4uZXhwbG9yZS1kZXRhaWxzIHVsIGxpIC5saXN0aW5nLWRldGFpbHMge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWFlYWVhO1xufVxuXG4uZXhwbG9yZS1kZXRhaWxzIHVsIGxpIC5saXN0aW5nLWRldGFpbHMgYSB7XG4gIHBhZGRpbmc6IDEwcHggMTBweCAwO1xufVxuXG4uZXhwbG9yZS1kZXRhaWxzIHVsIGxpIGEgZmlndXJlIHtcbiAgaGVpZ2h0OiAxODBweDtcbiAgbWFyZ2luOiAwIDAgNXB4IDA7XG59XG5cbi5leHBsb3JlLWRldGFpbHMgdWwgbGkgYSBmaWd1cmUgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTgwcHg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4uZWRpdHVwZGF0bGlua3Mge1xuICBtYXJnaW46IDAgMCAxMnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbi5lZGl0dXBkYXRsaW5rcyBhIC5mYSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4ubGlzdGluZy1kZXRhaWxzIGEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIGNvbG9yOiAjMDAwO1xufVxuXG4ubGlzdGluZy1uYW1lLCAubGlzdGluZy11c2VybmFtZSB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA5MHB4KTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogMjVweDtcbn1cblxuLmxpc3RpbmctcHJpY2UsIC5saXN0aW5nLXJhdGluZyB7XG4gIHdpZHRoOiA5MHB4O1xuICBsaW5lLWhlaWdodDogMjVweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5oaWRlIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLypyZXNwb25zaXZlKi9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gIC5leHBsb3JlLWRldGFpbHMgdWwgbGkge1xuICAgIHdpZHRoOiA0OCU7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5leHBsb3JlLWRldGFpbHMgdWwgbGkge1xuICAgIHdpZHRoOiA5OCU7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDY0MHB4KSB7XG4gIC5leHBsb3JlLXVzZXItcHJvZmlsZSB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5leHBsb3JlLXVzZXItcHJvZmlsZSAuZXhwbG9yZS1maWx0ZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLmV4cGxvcmUtdXNlci1wcm9maWxlIC5leHBsb3JlLWRldGFpbHMge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDA7XG4gIH1cblxuICAuZXhwbG9yZS1kZXRhaWxzIHVsIGxpIHtcbiAgICB3aWR0aDogOTglO1xuICB9XG5cbiAgLmV4cGxvcmUtdGFncyB1bCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/artist/explore/explore.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/@auth/artist/explore/explore.component.ts ***!
  \***********************************************************/
/*! exports provided: NgxArtistExploreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxArtistExploreComponent", function() { return NgxArtistExploreComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");





var NgxArtistExploreComponent = /** @class */ (function () {
    function NgxArtistExploreComponent(fb, artistService, ref, router) {
        this.fb = fb;
        this.artistService = artistService;
        this.ref = ref;
        this.router = router;
        this.filter_services = [];
        this.formGroup = this.fb.group({
            minvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            maxvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    NgxArtistExploreComponent.prototype.ngOnInit = function () {
        this.GetServiceDetails();
        this.GetArtistDetails();
    };
    NgxArtistExploreComponent.prototype.GetServiceDetails = function () {
        var _this = this;
        this.artistService.GetServiceDetails().subscribe(function (result) {
            if (result.status == true) {
                _this.artist_services = result.data.services;
                _this.ref.markForCheck();
            }
        });
    };
    NgxArtistExploreComponent.prototype.GetArtistDetails = function () {
        var _this = this;
        this.artistService.GetArtistDetails().subscribe(function (result) {
            if (result.status) {
                _this.aritsts_lists = result.data;
                _this.ref.markForCheck();
            }
        });
    };
    NgxArtistExploreComponent.prototype.onCheckboxChange = function (status, value) {
        if (status) {
            this.filter_services.push(value);
        }
        else {
            var index = this.filter_services.indexOf(value);
            if (index > -1) {
                console.log('splice', value);
                this.filter_services.splice(index, 1);
            }
        }
        if (this.filter_services.length > 0) {
            this.GetAristsByServiceIds(this.filter_services);
        }
        else {
            this.GetArtistDetails();
        }
    };
    NgxArtistExploreComponent.prototype.SetArtistId = function (id) {
        this.artistService.artist_id = id;
        localStorage.setItem('artist_id', id);
        this.router.navigate(['artist/list']);
    };
    NgxArtistExploreComponent.prototype.GetAristsByServiceIds = function (ids) {
        var _this = this;
        this.artistService.GetAristsByServiceIds(ids).subscribe(function (result) {
            if (result.status) {
                _this.aritsts_lists = result.data;
                _this.ref.markForCheck();
            }
        });
    };
    NgxArtistExploreComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__["ArtistService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    NgxArtistExploreComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-exolore',
            template: __webpack_require__(/*! raw-loader!./explore.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/explore/explore.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./explore.component.scss */ "./src/app/@auth/artist/explore/explore.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__["ArtistService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], NgxArtistExploreComponent);
    return NgxArtistExploreComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/list/list.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/@auth/artist/list/list.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.listing-outer {\n  display: flex;\n  align-items: stretch;\n  flex-wrap: wrap;\n}\n\n.full-listing-details {\n  padding: 0;\n  box-shadow: 0rem 0rem 0.9375rem rgba(177, 177, 177, 0.3), 0rem 0rem 0.9375rem rgba(177, 177, 177, 0.3);\n  margin: 0 0 15px;\n}\n\n.full-listing-details h3 {\n  background: #000;\n  padding: 10px;\n  color: #fff;\n  text-transform: uppercase;\n}\n\n.full-listing-details .form-group {\n  padding: 10px 15px;\n  border-radius: 4px;\n  margin: 0;\n}\n\n.full-listing-details .form-group label {\n  text-transform: uppercase;\n  font-weight: bolder;\n  letter-spacing: 1px;\n  font-size: 15px;\n}\n\n.full-listing-details .form-group p {\n  margin: 2px 0 6px;\n  padding: 0 0 6px;\n  color: gray;\n}\n\n.full-listing-details.col-md-5 .form-group p {\n  border-bottom: 1px solid #eaeaea;\n}\n\n#myCarousel {\n  margin: 0 0 15px;\n}\n\n.carousel-inner .item img {\n  height: auto;\n}\n\n.remove-pr {\n  padding-right: 0;\n}\n\n.continue-button {\n  text-align: right;\n}\n\n.continue-button a {\n  background: #000;\n  color: #fff;\n  font-weight: normal;\n  height: 30px;\n  line-height: 29px;\n  text-transform: uppercase;\n  font-size: 13px;\n  padding: 0 15px;\n  border: none;\n  cursor: pointer;\n  border-radius: 0;\n}\n\n@media screen and (max-width: 991px) {\n  .full-listing-details {\n    padding: 0 15px;\n    box-shadow: none;\n    margin: 0;\n    width: 100%;\n  }\n\n  .remove-pr {\n    padding-right: 15px;\n    width: 100%;\n  }\n\n  .continue-button a {\n    display: block;\n    width: 100%;\n    text-align: center;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2xpc3QvQzpcXFVzZXJzXFxERUxMXFxEZXNrdG9wXFxmaXhteWtpeC9zcmNcXGFwcFxcQGF1dGhcXGFydGlzdFxcbGlzdFxcbGlzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L2xpc3QvbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLDhCQUFBO0FDQ0Q7O0FEQ0E7RUFDRSxrQkFBQTtBQ0VGOztBREFBO0VBQ0MsYUFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtBQ0dEOztBRERBO0VBQ0MsVUFBQTtFQUNBLHNHQUFBO0VBQ0EsZ0JBQUE7QUNJRDs7QURGQTtFQUNDLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtBQ0tEOztBREhBO0VBQ0Msa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUNNRDs7QURKQTtFQUNDLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNPRDs7QURMQTtFQUNDLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDUUQ7O0FETkE7RUFDQyxnQ0FBQTtBQ1NEOztBRFBBO0VBQ0MsZ0JBQUE7QUNVRDs7QURSQTtFQUNDLFlBQUE7QUNXRDs7QURUQTtFQUNDLGdCQUFBO0FDWUQ7O0FEVkE7RUFDSSxpQkFBQTtBQ2FKOztBRFhBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ2NKOztBRFpBO0VBQ0E7SUFDSSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxTQUFBO0lBQ0EsV0FBQTtFQ2VGOztFRGJGO0lBQ0ksbUJBQUE7SUFDQSxXQUFBO0VDZ0JGOztFRGRGO0lBQ0MsY0FBQTtJQUNBLFdBQUE7SUFDQSxrQkFBQTtFQ2lCQztBQUNGIiwiZmlsZSI6InNyYy9hcHAvQGF1dGgvYXJ0aXN0L2xpc3QvbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItaW5pdGlhbCB7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjgpO1xyXG59XHJcbiNtYWluIHtcclxuICBwYWRkaW5nOiA0MHB4IDE1cHg7XHJcbn1cclxuLmxpc3Rpbmctb3V0ZXIge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0YWxpZ24taXRlbXM6IHN0cmV0Y2g7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG59XHJcbi5mdWxsLWxpc3RpbmctZGV0YWlscyB7XHJcblx0cGFkZGluZzogMDtcclxuXHRib3gtc2hhZG93OiAwcmVtIDByZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwuMyksIDByZW0gMHJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4zKTtcclxuXHRtYXJnaW46IDAgMCAxNXB4O1xyXG59XHJcbi5mdWxsLWxpc3RpbmctZGV0YWlscyBoMyB7XHJcblx0YmFja2dyb3VuZDogIzAwMDtcclxuXHRwYWRkaW5nOiAxMHB4O1xyXG5cdGNvbG9yOiAjZmZmO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuLmZ1bGwtbGlzdGluZy1kZXRhaWxzIC5mb3JtLWdyb3VwIHtcclxuXHRwYWRkaW5nOiAxMHB4IDE1cHg7XHJcblx0Ym9yZGVyLXJhZGl1czogNHB4O1xyXG5cdG1hcmdpbjogMDtcclxufVxyXG4uZnVsbC1saXN0aW5nLWRldGFpbHMgLmZvcm0tZ3JvdXAgbGFiZWwge1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuXHRsZXR0ZXItc3BhY2luZzogMXB4O1xyXG5cdGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uZnVsbC1saXN0aW5nLWRldGFpbHMgLmZvcm0tZ3JvdXAgcCB7XHJcblx0bWFyZ2luOiAycHggMCA2cHg7XHJcblx0cGFkZGluZzogMCAwIDZweDtcclxuXHRjb2xvcjogZ3JheTtcclxufVxyXG4uZnVsbC1saXN0aW5nLWRldGFpbHMuY29sLW1kLTUgLmZvcm0tZ3JvdXAgcCB7XHJcblx0Ym9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XHJcbn1cclxuI215Q2Fyb3VzZWwge1xyXG5cdG1hcmdpbjogMCAwIDE1cHg7XHJcbn1cclxuLmNhcm91c2VsLWlubmVyIC5pdGVtIGltZyB7XHJcblx0aGVpZ2h0OiBhdXRvO1xyXG59XHJcbi5yZW1vdmUtcHIge1xyXG5cdHBhZGRpbmctcmlnaHQ6IDA7XHJcbn1cclxuLmNvbnRpbnVlLWJ1dHRvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uY29udGludWUtYnV0dG9uIGEge1xyXG4gICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyOXB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHBhZGRpbmc6IDAgMTVweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpe1xyXG4uZnVsbC1saXN0aW5nLWRldGFpbHMge1xyXG4gICAgcGFkZGluZzogMCAxNXB4O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5yZW1vdmUtcHIge1xyXG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5jb250aW51ZS1idXR0b24gYXtcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHR3aWR0aDogMTAwJTtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxufSIsIi5oZWFkZXItaW5pdGlhbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC44KTtcbn1cblxuI21haW4ge1xuICBwYWRkaW5nOiA0MHB4IDE1cHg7XG59XG5cbi5saXN0aW5nLW91dGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmZ1bGwtbGlzdGluZy1kZXRhaWxzIHtcbiAgcGFkZGluZzogMDtcbiAgYm94LXNoYWRvdzogMHJlbSAwcmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMyksIDByZW0gMHJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjMpO1xuICBtYXJnaW46IDAgMCAxNXB4O1xufVxuXG4uZnVsbC1saXN0aW5nLWRldGFpbHMgaDMge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBwYWRkaW5nOiAxMHB4O1xuICBjb2xvcjogI2ZmZjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmZ1bGwtbGlzdGluZy1kZXRhaWxzIC5mb3JtLWdyb3VwIHtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIG1hcmdpbjogMDtcbn1cblxuLmZ1bGwtbGlzdGluZy1kZXRhaWxzIC5mb3JtLWdyb3VwIGxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uZnVsbC1saXN0aW5nLWRldGFpbHMgLmZvcm0tZ3JvdXAgcCB7XG4gIG1hcmdpbjogMnB4IDAgNnB4O1xuICBwYWRkaW5nOiAwIDAgNnB4O1xuICBjb2xvcjogZ3JheTtcbn1cblxuLmZ1bGwtbGlzdGluZy1kZXRhaWxzLmNvbC1tZC01IC5mb3JtLWdyb3VwIHAge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZWFlYTtcbn1cblxuI215Q2Fyb3VzZWwge1xuICBtYXJnaW46IDAgMCAxNXB4O1xufVxuXG4uY2Fyb3VzZWwtaW5uZXIgLml0ZW0gaW1nIHtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4ucmVtb3ZlLXByIHtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cblxuLmNvbnRpbnVlLWJ1dHRvbiB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uY29udGludWUtYnV0dG9uIGEge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBsaW5lLWhlaWdodDogMjlweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGJvcmRlcjogbm9uZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAuZnVsbC1saXN0aW5nLWRldGFpbHMge1xuICAgIHBhZGRpbmc6IDAgMTVweDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIG1hcmdpbjogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5yZW1vdmUtcHIge1xuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cblxuICAuY29udGludWUtYnV0dG9uIGEge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/@auth/artist/list/list.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/@auth/artist/list/list.component.ts ***!
  \*****************************************************/
/*! exports provided: NgxArtistListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxArtistListComponent", function() { return NgxArtistListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../@core/backend/common/services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/modules/index-all.js");






var NgxArtistListComponent = /** @class */ (function () {
    function NgxArtistListComponent(fb, artistService, ref, router) {
        this.fb = fb;
        this.artistService = artistService;
        this.ref = ref;
        this.router = router;
        this.formGroup = this.fb.group({
            minvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            maxvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
        this.artist_details = {
            first_name: ''
        };
        this.id = this.artistService.artist_id;
        if (!this.id) {
            this.id = localStorage.getItem('artist_id');
        }
        this.GetArtistById(this.id);
    }
    NgxArtistListComponent.prototype.ngOnInit = function () { };
    NgxArtistListComponent.prototype.GetArtistById = function (id) {
        var _this = this;
        this.artistService.GetArtistDetailsById(id).subscribe(function (result) {
            if (result.status) {
                var find_artist = underscore__WEBPACK_IMPORTED_MODULE_5__["findWhere"](result.data, { id: parseInt(id) });
                if (find_artist) {
                    _this.artist_details = find_artist;
                    _this.artist_services = find_artist.artist_services;
                    _this.ref.markForCheck();
                }
            }
        });
    };
    ;
    NgxArtistListComponent.prototype.Apply_Service_request = function () {
        this.artistService.artist_details = this.artist_details;
        console.log('I am hre in');
        this.router.navigate(['customer/send/request']);
    };
    NgxArtistListComponent.prototype.Chat_request = function () {
        this.router.navigate(['users/chat']);
    };
    NgxArtistListComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_3__["ArtistService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    NgxArtistListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-list',
            template: __webpack_require__(/*! raw-loader!./list.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/list/list.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./list.component.scss */ "./src/app/@auth/artist/list/list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_3__["ArtistService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], NgxArtistListComponent);
    return NgxArtistListComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/payment-method/payment-method.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/@auth/artist/payment-method/payment-method.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.about-shoe-servicec-list li {\n  width: 98%;\n  margin: 0 1% 20px;\n  padding: 15px 15px 0;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.li-50 {\n  width: 48% !important;\n  margin: 0 1% 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3BheW1lbnQtbWV0aG9kL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxhcnRpc3RcXHBheW1lbnQtbWV0aG9kXFxwYXltZW50LW1ldGhvZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3BheW1lbnQtbWV0aG9kL3BheW1lbnQtbWV0aG9kLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsOEJBQUE7QUNDRDs7QURDQTtFQUNFLGtCQUFBO0FDRUY7O0FEQUE7RUFDQyxhQUFBO0VBQ0EsZUFBQTtBQ0dEOztBRERBO0VBQ0MsVUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0hBQUE7QUNJRDs7QURGQTtFQUNDLHFCQUFBO0VBQ0EsaUJBQUE7QUNLRCIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2FydGlzdC9wYXltZW50LW1ldGhvZC9wYXltZW50LW1ldGhvZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItaW5pdGlhbCB7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjgpO1xyXG59XHJcbiNtYWluIHtcclxuICBwYWRkaW5nOiA0MHB4IDE1cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcblx0d2lkdGg6IDk4JTtcclxuXHRtYXJnaW46IDAgMSUgMjBweDtcclxuXHRwYWRkaW5nOiAxNXB4IDE1cHggMDtcclxuXHRib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsMCwwLDAuMDUpO1xyXG5cdGJvcmRlci1yYWRpdXM6IDJweDtcclxuXHRib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsLjIpXHJcbn1cclxuLmxpLTUwIHtcclxuXHR3aWR0aDogNDglICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwIDElIDIwcHg7XHJcbn0iLCIuaGVhZGVyLWluaXRpYWwge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG59XG5cbiNtYWluIHtcbiAgcGFkZGluZzogNDBweCAxNXB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcbiAgd2lkdGg6IDk4JTtcbiAgbWFyZ2luOiAwIDElIDIwcHg7XG4gIHBhZGRpbmc6IDE1cHggMTVweCAwO1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIGJveC1zaGFkb3c6IC0wLjVyZW0gLTAuNXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMik7XG59XG5cbi5saS01MCB7XG4gIHdpZHRoOiA0OCUgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwIDElIDIwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/@auth/artist/payment-method/payment-method.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/@auth/artist/payment-method/payment-method.component.ts ***!
  \*************************************************************************/
/*! exports provided: NgxArtistPaymentMethodComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxArtistPaymentMethodComponent", function() { return NgxArtistPaymentMethodComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var NgxArtistPaymentMethodComponent = /** @class */ (function () {
    function NgxArtistPaymentMethodComponent(fb) {
        this.fb = fb;
        this.formGroup = this.fb.group({
            minvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            maxvalue: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    NgxArtistPaymentMethodComponent.prototype.ngOnInit = function () { };
    NgxArtistPaymentMethodComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
    ]; };
    NgxArtistPaymentMethodComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-payment-method',
            template: __webpack_require__(/*! raw-loader!./payment-method.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/payment-method/payment-method.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./payment-method.component.scss */ "./src/app/@auth/artist/payment-method/payment-method.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], NgxArtistPaymentMethodComponent);
    return NgxArtistPaymentMethodComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/profile/profile.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/@auth/artist/profile/profile.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login_form {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  justify-content: center;\n  overflow-y: auto;\n  position: relative;\n}\n\n.login-logo {\n  text-align: center;\n  margin: 0px 0 10px;\n}\n\ninput[type=file] {\n  padding: 0;\n  border: none;\n  height: 32px;\n  margin: 0;\n}\n\n.login-logo h2 {\n  font-family: Noto Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 22px;\n  line-height: 32px;\n  color: #2B3B54;\n  text-align: center;\n}\n\n.login-logo h2 span {\n  text-transform: uppercase;\n}\n\n.login_form_set {\n  /*background: #fff;*/\n  background: rgba(255, 255, 255, 0.95);\n  border-radius: 10px 10px 0px 0px;\n  width: 100%;\n  box-sizing: border-box;\n  position: relative;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  box-shadow: 0 0 20px -19px #000;\n}\n\n.login_form_set form {\n  width: 100%;\n  max-width: 100%;\n  margin: 0 auto;\n}\n\n.login_form_set form {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.login_form_set form .form-group {\n  width: 46%;\n  margin: 0 2% 12px;\n  position: relative;\n}\n\n.login_form_set form .form-group label {\n  margin: 0;\n  font-weight: bold;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  font-size: 11px;\n}\n\n.login_form_set form .form-group input[type=text],\n.login_form_set form .form-group input[type=email],\n.login_form_set form .form-group input[type=number] {\n  height: 32px;\n  line-height: 32px;\n}\n\n.login_form_set form .form-group input[type=file] {\n  margin: 4px 0 0 0;\n}\n\n.errmsg {\n  position: absolute;\n  color: red;\n  left: 0;\n  bottom: -18px;\n  font-size: 11px;\n  font-weight: bold;\n  transition: all 3s;\n}\n\n.gender {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.gender .gender-check {\n  cursor: pointer;\n  display: flex;\n  align-items: center;\n  height: 15px;\n  margin: 0 25px 0 0 !important;\n}\n\n.gender .gender-check input {\n  margin: 0 5pX 0 0;\n}\n\n.login-button {\n  width: 100% !important;\n  text-align: right;\n  padding: 0 2%;\n  box-sizing: border-box;\n  margin: 10px 0 0 0 !important;\n}\n\n.login_form_set form .form-group button {\n  /*background: #008cff;*/\n  background: #000;\n  border-radius: 6px;\n  color: #fff;\n  border: none;\n  width: 160px;\n  height: 32px;\n  line-height: 32px;\n  margin: 0 10px 0 0;\n}\n\n.form-group-100 {\n  width: 98% !important;\n}\n\n/* Responsive =================================================================*/\n\n@media screen and (max-width: 580px) {\n  .login_form {\n    padding: 50px 35px;\n  }\n\n  .login_form_set, .login_form_set form .form-group button {\n    width: 100%;\n    background: rgba(255, 255, 255, 0.75);\n  }\n\n  .login_form_set form {\n    flex-direction: column;\n  }\n\n  .login_form_set form .form-group {\n    width: 96%;\n  }\n\n  .gender .gender-check {\n    margin: 6px 25px 0 0 !important;\n  }\n}\n\n.header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.about-shoe-servicec-list li {\n  width: 100%;\n  margin: 0 0 20px;\n  padding: 15px 15px 0;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.profile-descrition {\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n}\n\n.about-shoe-servicec-list li .profile-descrition figure {\n  width: 200px;\n  height: 200px;\n  overflow: hidden;\n  border-radius: 50%;\n  padding: 5px;\n  border: 5px solid #fff;\n  background: #fff;\n  margin: 0 30px 20px 0;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.about-shoe-servicec-list li .profile-descrition figure img {\n  width: 200px;\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.profile-view {\n  width: calc(100% - 230px);\n}\n\n.profile-review {\n  display: flex;\n  justify-content: space-between;\n  align-content: center;\n  margin: 0 0 18px;\n}\n\n.rating-stars {\n  display: flex;\n  align-items: center;\n}\n\n.rating-stars span {\n  text-transform: uppercase;\n  font-weight: bold;\n  font-size: 20px;\n  margin: 0 10px 0 0;\n}\n\n.rating-stars .fa {\n  font-size: 20px;\n  margin: 0 3px 0 0;\n}\n\n.servicerequest {\n  background: #000;\n  color: #fff;\n  font-weight: normal;\n  height: 30px;\n  line-height: 28px;\n  text-transform: uppercase;\n  font-size: 13px;\n  padding: 0 15px;\n  border: none;\n  border-radius: 0;\n}\n\n.about-shoe-servicec-list li ul {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -1%;\n}\n\n.about-shoe-servicec-list li ul li {\n  width: 31.33%;\n  margin: 0 1% 15px;\n}\n\n.about-shoe-servicec-list li ul li figure {\n  height: 165px;\n}\n\n.about-shoe-servicec-list li ul li figure img {\n  width: 100%;\n  height: 165px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.editupdatlinks {\n  margin: 10px 0 12px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.editupdatlinks a .fa {\n  display: block;\n  color: #000;\n  font-size: 20px;\n}\n\n/* Responsive =========================*/\n\n@media screen and (max-width: 991px) {\n  .profile-descrition {\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n  }\n\n  .profile-view {\n    width: 100%;\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .about-shoe-servicec-list li ul li {\n    width: 48%;\n  }\n}\n\n@media screen and (max-width: 640px) {\n  .about-shoe-servicec-list li ul li {\n    width: 100%;\n    margin-left: 0;\n    margin-right: 0;\n  }\n}\n\n@media screen and (max-width: 480px) {\n  .profile-review {\n    flex-direction: column;\n  }\n\n  .servicerequest {\n    text-align: center;\n    margin: 10px 0 0 0;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3Byb2ZpbGUvQzpcXFVzZXJzXFxERUxMXFxEZXNrdG9wXFxmaXhteWtpeC9zcmNcXGFwcFxcQGF1dGhcXGFydGlzdFxccHJvZmlsZVxccHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFHQSxnQkFBQTtFQUNBLGtCQUFBO0FDRko7O0FESUU7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDREo7O0FER0U7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0FDQUo7O0FERUU7RUFDRSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDRTtFQUNFLHlCQUFBO0FDRUo7O0FEQUU7RUFDRSxvQkFBQTtFQUNBLHFDQUFBO0VBQ0EsZ0NBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFFQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLDhCQUFBO0VBQ0EsK0JBQUE7QUNFSjs7QURBRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ0dKOztBRERFO0VBQ0UsYUFBQTtFQUNBLGVBQUE7QUNJSjs7QURGRTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDS0o7O0FESEU7RUFDRSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQ01KOztBREpFOzs7RUFHRSxZQUFBO0VBQ0EsaUJBQUE7QUNPSjs7QURMRTtFQUNFLGlCQUFBO0FDUUo7O0FETkU7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxPQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDU0o7O0FEUEU7RUFDRSxhQUFBO0VBQ0EsZUFBQTtBQ1VKOztBRFJFO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtBQ1dKOztBRFRFO0VBQ0UsaUJBQUE7QUNZSjs7QURWRTtFQUNFLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtBQ2FKOztBRFhFO0VBQ0UsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUNjSjs7QURaRTtFQUNFLHFCQUFBO0FDZUo7O0FEYkUsZ0ZBQUE7O0FBQ0E7RUFDRTtJQUNFLGtCQUFBO0VDZ0JKOztFRGRFO0lBQ0UsV0FBQTtJQUNBLHFDQUFBO0VDaUJKOztFRGZFO0lBQ0Usc0JBQUE7RUNrQko7O0VEaEJFO0lBQ0UsVUFBQTtFQ21CSjs7RURqQkU7SUFDRSwrQkFBQTtFQ29CSjtBQUNGOztBRFJFO0VBQ0UsOEJBQUE7QUNVSjs7QURSRTtFQUNFLGtCQUFBO0FDV0o7O0FEVEU7RUFDRSxhQUFBO0VBQ0EsZUFBQTtBQ1lKOztBRFZFO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0hBQUE7QUNhSjs7QURYRTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLDhCQUFBO0FDY0o7O0FEWkU7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0hBQUE7QUNlSjs7QURiRTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ2dCSjs7QURkRTtFQUNFLHlCQUFBO0FDaUJKOztBRGZFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtBQ2tCSjs7QURoQkU7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNtQko7O0FEakJFO0VBQ0UseUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ29CSjs7QURsQkU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUNxQko7O0FEbkJFO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDc0JKOztBRHBCRTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQ3VCSjs7QURyQkU7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7QUN3Qko7O0FEdEJFO0VBQ0UsYUFBQTtBQ3lCSjs7QUR2QkU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUMwQko7O0FEeEJFO0VBQ0UsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQzJCSjs7QUR6QkU7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUM0Qko7O0FEMUJFLHdDQUFBOztBQUNBO0VBQ0E7SUFDRSxzQkFBQTtJQUNBLHVCQUFBO0lBQ0EsbUJBQUE7RUM2QkY7O0VEM0JBO0lBQ0UsV0FBQTtFQzhCRjtBQUNGOztBRDVCRTtFQUNBO0lBQ0ksVUFBQTtFQzhCSjtBQUNGOztBRDVCRTtFQUNBO0lBQ0ksV0FBQTtJQUNBLGNBQUE7SUFDQSxlQUFBO0VDOEJKO0FBQ0Y7O0FENUJFO0VBQ0E7SUFDRSxzQkFBQTtFQzhCRjs7RUQ1QkE7SUFDRSxrQkFBQTtJQUNBLGtCQUFBO0VDK0JGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9hcnRpc3QvcHJvZmlsZS9wcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luX2Zvcm0ge1xyXG4gICAgLy8gYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2ltZ19iZzAxLmpwZycpIG5vLXJlcGVhdCBzY3JvbGwgY2VudGVyIGNlbnRlciAvIGNvdmVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAvLyBtaW4taGVpZ2h0OiAxMDB2aDtcclxuICAgIC8vIHBhZGRpbmc6IDBweCAxNXB4O1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcbiAgLmxvZ2luLWxvZ28ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAwcHggMCAxMHB4O1xyXG4gIH1cclxuICBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgaGVpZ2h0OiAzMnB4O1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICAubG9naW4tbG9nbyBoMiB7XHJcbiAgICBmb250LWZhbWlseTogTm90byBTYW5zO1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICBsaW5lLWhlaWdodDogMzJweDtcclxuICAgIGNvbG9yOiAjMkIzQjU0O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAubG9naW4tbG9nbyBoMiBzcGFuIHtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgfVxyXG4gIC5sb2dpbl9mb3JtX3NldCB7XHJcbiAgICAvKmJhY2tncm91bmQ6ICNmZmY7Ki9cclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC45NSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgIC8vIHBhZGRpbmc6IDBweCAxNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMjBweCAtMTlweCAjMDAwO1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCB7XHJcbiAgICB3aWR0aDogNDYlO1xyXG4gICAgbWFyZ2luOiAwIDIlIDEycHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgfVxyXG4gIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgfVxyXG4gIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGlucHV0W3R5cGU9XCJ0ZXh0XCJdLFxyXG4gIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGlucHV0W3R5cGU9XCJlbWFpbFwiXSxcclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPVwibnVtYmVyXCJdIHtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xyXG4gIH1cclxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBpbnB1dFt0eXBlPVwiZmlsZVwiXSB7XHJcbiAgICBtYXJnaW46IDRweCAwIDAgMDtcclxuICB9XHJcbiAgLmVycm1zZyB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgbGVmdDogMDtcclxuICAgIGJvdHRvbTogLTE4cHg7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAzcztcclxuICB9XHJcbiAgLmdlbmRlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gIH1cclxuICAuZ2VuZGVyIC5nZW5kZXItY2hlY2sge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICBtYXJnaW46IDAgMjVweCAwIDAgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmdlbmRlciAuZ2VuZGVyLWNoZWNrIGlucHV0IHtcclxuICAgIG1hcmdpbjogMCA1cFggMCAwO1xyXG4gIH1cclxuICAubG9naW4tYnV0dG9uIHtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIHBhZGRpbmc6IDAgMiU7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gICAgbWFyZ2luOiAxMHB4IDAgMCAwICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGJ1dHRvbiB7XHJcbiAgICAvKmJhY2tncm91bmQ6ICMwMDhjZmY7Ki9cclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHdpZHRoOiAxNjBweDtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xyXG4gICAgbWFyZ2luOiAwIDEwcHggMCAwO1xyXG4gIH1cclxuICAuZm9ybS1ncm91cC0xMDAge1xyXG4gICAgd2lkdGg6IDk4JSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAvKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1ODBweCl7XHJcbiAgICAubG9naW5fZm9ybSB7XHJcbiAgICAgIHBhZGRpbmc6IDUwcHggMzVweDtcclxuICAgIH1cclxuICAgIC5sb2dpbl9mb3JtX3NldCwgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgYnV0dG9uIHtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC43NSk7XHJcbiAgICB9XHJcbiAgICAubG9naW5fZm9ybV9zZXQgZm9ybSB7XHJcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcbiAgICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCB7XHJcbiAgICAgIHdpZHRoOiA5NiU7XHJcbiAgICB9XHJcbiAgICAuZ2VuZGVyIC5nZW5kZXItY2hlY2sge1xyXG4gICAgICBtYXJnaW46IDZweCAyNXB4IDAgMCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgLmhlYWRlci1pbml0aWFsIHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxuICB9XHJcbiAgI21haW4ge1xyXG4gICAgcGFkZGluZzogNDBweCAxNXB4O1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgfVxyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xyXG4gICAgcGFkZGluZzogMTVweCAxNXB4IDA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsMCwwLDAuMDUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4yKVxyXG4gIH1cclxuICAucHJvZmlsZS1kZXNjcml0aW9uIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICB9XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAucHJvZmlsZS1kZXNjcml0aW9uIGZpZ3VyZSB7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgbWFyZ2luOiAwIDMwcHggMjBweCAwO1xyXG4gICAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4yKVxyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5wcm9maWxlLWRlc2NyaXRpb24gZmlndXJlIGltZyB7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgfVxyXG4gIC5wcm9maWxlLXZpZXcge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDIzMHB4KTtcclxuICB9XHJcbiAgLnByb2ZpbGUtcmV2aWV3IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDAgMCAxOHB4O1xyXG4gIH1cclxuICAucmF0aW5nLXN0YXJzIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAucmF0aW5nLXN0YXJzIHNwYW4ge1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgbWFyZ2luOiAwIDEwcHggMCAwO1xyXG4gIH1cclxuICAucmF0aW5nLXN0YXJzIC5mYSB7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBtYXJnaW46IDAgM3B4IDAgMDtcclxuICB9XHJcbiAgLnNlcnZpY2VyZXF1ZXN0IHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjhweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBwYWRkaW5nOiAwIDE1cHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBtYXJnaW46IDAgLTElO1xyXG4gIH1cclxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIGxpIHtcclxuICAgIHdpZHRoOiAzMS4zMyU7XHJcbiAgICBtYXJnaW46IDAgMSUgMTVweDtcclxuICB9XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSBmaWd1cmUge1xyXG4gICAgaGVpZ2h0OiAxNjVweDtcclxuICB9XHJcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSBmaWd1cmUgaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxNjVweDtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gIH1cclxuICAuZWRpdHVwZGF0bGlua3Mge1xyXG4gICAgbWFyZ2luOiAxMHB4IDAgMTJweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIH1cclxuICAuZWRpdHVwZGF0bGlua3MgYSAuZmEge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICB9XHJcbiAgLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcbiAgLnByb2ZpbGUtZGVzY3JpdGlvbiB7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAucHJvZmlsZS12aWV3IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICB9XHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwgbGkge1xyXG4gICAgICB3aWR0aDogNDglO1xyXG4gIH1cclxuICB9XHJcbiAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjQwcHgpe1xyXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwgbGkge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDA7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMDtcclxuICB9XHJcbiAgfVxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KXtcclxuICAucHJvZmlsZS1yZXZpZXcge1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICB9XHJcbiAgLnNlcnZpY2VyZXF1ZXN0IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMTBweCAwIDAgMDtcclxuICB9XHJcbiAgfSIsIi5sb2dpbl9mb3JtIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmxvZ2luLWxvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMHB4IDAgMTBweDtcbn1cblxuaW5wdXRbdHlwZT1maWxlXSB7XG4gIHBhZGRpbmc6IDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgaGVpZ2h0OiAzMnB4O1xuICBtYXJnaW46IDA7XG59XG5cbi5sb2dpbi1sb2dvIGgyIHtcbiAgZm9udC1mYW1pbHk6IE5vdG8gU2FucztcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBsaW5lLWhlaWdodDogMzJweDtcbiAgY29sb3I6ICMyQjNCNTQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ2luLWxvZ28gaDIgc3BhbiB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5sb2dpbl9mb3JtX3NldCB7XG4gIC8qYmFja2dyb3VuZDogI2ZmZjsqL1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOTUpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMHB4IDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3gtc2hhZG93OiAwIDAgMjBweCAtMTlweCAjMDAwO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAge1xuICB3aWR0aDogNDYlO1xuICBtYXJnaW46IDAgMiUgMTJweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCBsYWJlbCB7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT10ZXh0XSxcbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGlucHV0W3R5cGU9ZW1haWxdLFxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT1udW1iZXJdIHtcbiAgaGVpZ2h0OiAzMnB4O1xuICBsaW5lLWhlaWdodDogMzJweDtcbn1cblxuLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgaW5wdXRbdHlwZT1maWxlXSB7XG4gIG1hcmdpbjogNHB4IDAgMCAwO1xufVxuXG4uZXJybXNnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogcmVkO1xuICBsZWZ0OiAwO1xuICBib3R0b206IC0xOHB4O1xuICBmb250LXNpemU6IDExcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0cmFuc2l0aW9uOiBhbGwgM3M7XG59XG5cbi5nZW5kZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG5cbi5nZW5kZXIgLmdlbmRlci1jaGVjayB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxNXB4O1xuICBtYXJnaW46IDAgMjVweCAwIDAgIWltcG9ydGFudDtcbn1cblxuLmdlbmRlciAuZ2VuZGVyLWNoZWNrIGlucHV0IHtcbiAgbWFyZ2luOiAwIDVwWCAwIDA7XG59XG5cbi5sb2dpbi1idXR0b24ge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcGFkZGluZzogMCAyJTtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgbWFyZ2luOiAxMHB4IDAgMCAwICFpbXBvcnRhbnQ7XG59XG5cbi5sb2dpbl9mb3JtX3NldCBmb3JtIC5mb3JtLWdyb3VwIGJ1dHRvbiB7XG4gIC8qYmFja2dyb3VuZDogIzAwOGNmZjsqL1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXI6IG5vbmU7XG4gIHdpZHRoOiAxNjBweDtcbiAgaGVpZ2h0OiAzMnB4O1xuICBsaW5lLWhlaWdodDogMzJweDtcbiAgbWFyZ2luOiAwIDEwcHggMCAwO1xufVxuXG4uZm9ybS1ncm91cC0xMDAge1xuICB3aWR0aDogOTglICFpbXBvcnRhbnQ7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTgwcHgpIHtcbiAgLmxvZ2luX2Zvcm0ge1xuICAgIHBhZGRpbmc6IDUwcHggMzVweDtcbiAgfVxuXG4gIC5sb2dpbl9mb3JtX3NldCwgLmxvZ2luX2Zvcm1fc2V0IGZvcm0gLmZvcm0tZ3JvdXAgYnV0dG9uIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNzUpO1xuICB9XG5cbiAgLmxvZ2luX2Zvcm1fc2V0IGZvcm0ge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAubG9naW5fZm9ybV9zZXQgZm9ybSAuZm9ybS1ncm91cCB7XG4gICAgd2lkdGg6IDk2JTtcbiAgfVxuXG4gIC5nZW5kZXIgLmdlbmRlci1jaGVjayB7XG4gICAgbWFyZ2luOiA2cHggMjVweCAwIDAgIWltcG9ydGFudDtcbiAgfVxufVxuLmhlYWRlci1pbml0aWFsIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjgpO1xufVxuXG4jbWFpbiB7XG4gIHBhZGRpbmc6IDQwcHggMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDAgMCAyMHB4O1xuICBwYWRkaW5nOiAxNXB4IDE1cHggMDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpO1xufVxuXG4ucHJvZmlsZS1kZXNjcml0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAucHJvZmlsZS1kZXNjcml0aW9uIGZpZ3VyZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbjogMCAzMHB4IDIwcHggMDtcbiAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMiksIDAuNXJlbSAwLjFyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKTtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAucHJvZmlsZS1kZXNjcml0aW9uIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMjAwcHg7XG4gIGhlaWdodDogMjAwcHg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4ucHJvZmlsZS12aWV3IHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDIzMHB4KTtcbn1cblxuLnByb2ZpbGUtcmV2aWV3IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbjogMCAwIDE4cHg7XG59XG5cbi5yYXRpbmctc3RhcnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4ucmF0aW5nLXN0YXJzIHNwYW4ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW46IDAgMTBweCAwIDA7XG59XG5cbi5yYXRpbmctc3RhcnMgLmZhIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW46IDAgM3B4IDAgMDtcbn1cblxuLnNlcnZpY2VyZXF1ZXN0IHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGhlaWdodDogMzBweDtcbiAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgcGFkZGluZzogMCAxNXB4O1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIG1hcmdpbjogMCAtMSU7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgdWwgbGkge1xuICB3aWR0aDogMzEuMzMlO1xuICBtYXJnaW46IDAgMSUgMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSBmaWd1cmUge1xuICBoZWlnaHQ6IDE2NXB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHVsIGxpIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxNjVweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5lZGl0dXBkYXRsaW5rcyB7XG4gIG1hcmdpbjogMTBweCAwIDEycHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLmVkaXR1cGRhdGxpbmtzIGEgLmZhIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAucHJvZmlsZS1kZXNjcml0aW9uIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIH1cblxuICAucHJvZmlsZS12aWV3IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XG4gICAgd2lkdGg6IDQ4JTtcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjQwcHgpIHtcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB1bCBsaSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0ODBweCkge1xuICAucHJvZmlsZS1yZXZpZXcge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAuc2VydmljZXJlcXVlc3Qge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDEwcHggMCAwIDA7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/artist/profile/profile.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/@auth/artist/profile/profile.component.ts ***!
  \***********************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../@core/backend/common/services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(userService, fb, toasterService, artistService, router) {
        this.userService = userService;
        this.fb = fb;
        this.toasterService = toasterService;
        this.artistService = artistService;
        this.router = router;
        this.role = localStorage.getItem("role");
        this.user = JSON.parse(localStorage.getItem("user"));
        if (this.user) {
            this.formGroup = this.fb.group({
                first_name: [this.user.first_name, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                last_name: [this.user.last_name, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                email: [this.user.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                mobile_number: [this.user.mobile_number, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                age: [this.user.age, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                gender: [this.user.gender, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                business_name: [this.user.business_name, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                business_socials: [this.user.business_socials, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                address: [this.user.address, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                service_description: [this.user.service_description, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                proof_of_work: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
        }
        else {
            this.formGroup = this.fb.group({
                first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                mobile_number: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                age: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                gender: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                business_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                business_socials: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                service_description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                proof_of_work: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
        }
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent.prototype.ngAfterViewInit = function () {
        this.GetArtistDetailsById();
        // $('#profile_btn').click();
    };
    ProfileComponent.prototype.GetArtistDetailsById = function () {
        var _this = this;
        var id = this.user.id;
        this.artistService.GetArtistDetailsById(id).subscribe(function (result) {
            if (result.status) {
                _this.user = result.data[0];
                if (!result.data[0].business_name) {
                }
                else {
                    console.log('I am here in');
                    $('#profile_btn').click();
                }
            }
        });
    };
    ;
    ProfileComponent.prototype.UpdateUser = function () {
        var _this = this;
        var artist_detail = {};
        artist_detail = {
            artist_detail: this.formGroup.value
        };
        this.userService.updateUser(artist_detail).subscribe(function (result) {
            if (result.success) {
                _this.toasterService.success("User saved !!", result.message);
            }
        });
    };
    ;
    ProfileComponent.prototype.Artist_Service_request = function () {
        this.router.navigate(['artist/service/request']);
    };
    ;
    ProfileComponent.prototype.Artist_Service_List = function () {
        this.router.navigate(['artist/services/list']);
    };
    ;
    ProfileComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"] },
        { type: _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__["ArtistService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
    ]; };
    ProfileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-profile',
            template: __webpack_require__(/*! raw-loader!./profile.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.scss */ "./src/app/@auth/artist/profile/profile.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"], _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_4__["ArtistService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/@auth/artist/service-request/service-request.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/@auth/artist/service-request/service-request.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.about-shoe-servicec-list li {\n  width: 98%;\n  margin: 0 1% 20px;\n  padding: 15px 15px 0;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.li-50 {\n  width: 48% !important;\n  margin: 0 1% 20px;\n}\n\n.about-shoe-servicec-list li figure {\n  width: 120px;\n  height: 120px;\n  overflow: hidden;\n  border-radius: 50%;\n  padding: 5px;\n  border: 5px solid #fff;\n  background: #fff;\n  margin: 20px auto 30px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.about-shoe-servicec-list li figure img {\n  width: 120px;\n  height: 120px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.about-shoe-servicec-list li .about-shoe-text .h2-base-heading {\n  margin: 0 0 12px;\n}\n\n.information-form {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -1%;\n}\n\n.information-form .form-group {\n  width: 48%;\n  margin: 0 1% 15px;\n}\n\n.filled-value {\n  background: rgba(0, 0, 0, 0.05);\n  border-radius: 5px;\n  padding: 5px 10px;\n  margin: 3px 0 0 -1px;\n}\n\n.browse-image {\n  position: relative;\n}\n\n.browse-image input {\n  position: absolute;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  z-index: 9;\n  cursor: pointer;\n}\n\n.flex-direction-column {\n  flex-direction: column;\n  width: 100%;\n  justify-content: flex-start !important;\n  align-items: flex-start !important;\n}\n\n.flex-direction-column small {\n  font-size: 13px;\n  margin: 6px 0 0 0;\n}\n\n.continue-button {\n  text-align: right;\n  margin: 0 0 10px;\n  width: 100%;\n  padding: 0 1%;\n}\n\n.continue-button button {\n  font-size: 16px;\n  background: #000;\n  color: #fff;\n  font-weight: normal;\n  height: 30px;\n  line-height: 27px;\n  text-transform: uppercase;\n  font-size: 13px;\n  padding: 0 15px;\n  border: none;\n  margin: 0 0 0 10px;\n  border-radius: 0;\n}\n\n.uploaded-image-viewer {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 0 -1% 0;\n}\n\n.uploaded-image-viewer p {\n  width: 100px;\n  height: 100px;\n  margin: 1%;\n  box-shadow: 0 0 5px -3px #000;\n}\n\n.uploaded-image-viewer p img {\n  width: 100px;\n  height: 100px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.cancel-button {\n  background: #e8dfdf !important;\n  color: #000 !important;\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  .about-shoe-servicec-list li {\n    width: 100%;\n    margin: 0 0 20px;\n  }\n\n  .li-50 {\n    width: 100% !important;\n    margin: 0 0 20px;\n  }\n\n  .information-form .form-group {\n    width: 48%;\n    margin: 0 1% 15px;\n  }\n\n  .area-height textarea {\n    height: auto !important;\n  }\n\n  .flex-direction-column {\n    justify-content: center !important;\n    align-items: center !important;\n  }\n\n  .uploaded-image-viewer p {\n    width: 23%;\n    height: auto;\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .uploaded-image-viewer p {\n    width: 48%;\n  }\n\n  .uploaded-image-viewer p img {\n    height: auto;\n  }\n}\n\n@media screen and (max-width: 640px) {\n  .information-form .form-group {\n    width: 100%;\n    margin: 0 0 15px;\n  }\n}\n\n@media screen and (max-width: 480px) {\n  .uploaded-image-viewer p {\n    width: 98%;\n    height: auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3NlcnZpY2UtcmVxdWVzdC9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L3NyY1xcYXBwXFxAYXV0aFxcYXJ0aXN0XFxzZXJ2aWNlLXJlcXVlc3RcXHNlcnZpY2UtcmVxdWVzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvYXJ0aXN0L3NlcnZpY2UtcmVxdWVzdC9zZXJ2aWNlLXJlcXVlc3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyw4QkFBQTtBQ0NEOztBRENBO0VBQ0Usa0JBQUE7QUNFRjs7QURBQTtFQUNDLGFBQUE7RUFDQSxlQUFBO0FDR0Q7O0FEREE7RUFDQyxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxnSEFBQTtBQ0lEOztBREZBO0VBQ0MscUJBQUE7RUFDQSxpQkFBQTtBQ0tEOztBREhBO0VBQ0MsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdIQUFBO0FDTUQ7O0FESkE7RUFDQyxZQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUNPRDs7QURMQTtFQUNDLGdCQUFBO0FDUUQ7O0FETkE7RUFDQyxhQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7QUNTRDs7QURQQTtFQUNDLFVBQUE7RUFDQSxpQkFBQTtBQ1VEOztBRFJBO0VBQ0MsK0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNXRDs7QURUQTtFQUNDLGtCQUFBO0FDWUQ7O0FEVkE7RUFDQyxrQkFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNhRDs7QURYQTtFQUNDLHNCQUFBO0VBQ0EsV0FBQTtFQUNHLHNDQUFBO0VBQ0Esa0NBQUE7QUNjSjs7QURaQTtFQUNDLGVBQUE7RUFDQSxpQkFBQTtBQ2VEOztBRGJBO0VBQ0MsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FDZ0JEOztBRGRBO0VBQ0MsZUFBQTtFQUNHLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDaUJKOztBRGZBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDa0JEOztBRGhCQTtFQUNDLFlBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLDZCQUFBO0FDbUJEOztBRGpCQTtFQUNDLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ29CRDs7QURqQkE7RUFDQyw4QkFBQTtFQUNBLHNCQUFBO0FDb0JEOztBRGxCQSxrQ0FBQTs7QUFDQTtFQUNBO0lBQ0MsV0FBQTtJQUNBLGdCQUFBO0VDcUJDOztFRG5CRjtJQUNDLHNCQUFBO0lBQ0EsZ0JBQUE7RUNzQkM7O0VEcEJGO0lBQ0MsVUFBQTtJQUNBLGlCQUFBO0VDdUJDOztFRHJCRjtJQUNJLHVCQUFBO0VDd0JGOztFRHRCRjtJQUNDLGtDQUFBO0lBQ0EsOEJBQUE7RUN5QkM7O0VEdkJGO0lBQ0MsVUFBQTtJQUNBLFlBQUE7RUMwQkM7QUFDRjs7QUR4QkE7RUFDQTtJQUNDLFVBQUE7RUMwQkM7O0VEeEJGO0lBQ0MsWUFBQTtFQzJCQztBQUNGOztBRHpCQTtFQUNBO0lBQ0MsV0FBQTtJQUNBLGdCQUFBO0VDMkJDO0FBQ0Y7O0FEekJBO0VBQ0E7SUFDQyxVQUFBO0lBQ0EsWUFBQTtFQzJCQztBQUNGIiwiZmlsZSI6InNyYy9hcHAvQGF1dGgvYXJ0aXN0L3NlcnZpY2UtcmVxdWVzdC9zZXJ2aWNlLXJlcXVlc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWluaXRpYWwge1xyXG5cdGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxufVxyXG4jbWFpbiB7XHJcbiAgcGFkZGluZzogNDBweCAxNXB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3Qge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG5cdHdpZHRoOiA5OCU7XHJcblx0bWFyZ2luOiAwIDElIDIwcHg7XHJcblx0cGFkZGluZzogMTVweCAxNXB4IDA7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgcmdiYSgwLDAsMCwwLjA1KTtcclxuXHRib3JkZXItcmFkaXVzOiAycHg7XHJcblx0Ym94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4yKVxyXG59XHJcbi5saS01MCB7XHJcblx0d2lkdGg6IDQ4JSAhaW1wb3J0YW50O1xyXG5cdG1hcmdpbjogMCAxJSAyMHB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcclxuXHR3aWR0aDogMTIwcHg7XHJcblx0aGVpZ2h0OiAxMjBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRwYWRkaW5nOiA1cHg7XHJcblx0Ym9yZGVyOiA1cHggc29saWQgI2ZmZjtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdG1hcmdpbjogMjBweCBhdXRvIDMwcHg7XHJcblx0Ym94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4yKVxyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIGltZyB7XHJcblx0d2lkdGg6IDEyMHB4O1xyXG5cdGhlaWdodDogMTIwcHg7XHJcblx0b2JqZWN0LWZpdDogY292ZXI7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IC5oMi1iYXNlLWhlYWRpbmcge1xyXG5cdG1hcmdpbjogMCAwIDEycHg7XHJcbn1cclxuLmluZm9ybWF0aW9uLWZvcm0ge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdG1hcmdpbjogMCAtMSU7XHJcbn1cclxuLmluZm9ybWF0aW9uLWZvcm0gLmZvcm0tZ3JvdXAge1xyXG5cdHdpZHRoOiA0OCU7XHJcblx0bWFyZ2luOiAwIDElIDE1cHg7XHJcbn1cclxuLmZpbGxlZC12YWx1ZSB7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjA1KTtcclxuXHRib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0cGFkZGluZzogNXB4IDEwcHg7XHJcblx0bWFyZ2luOiAzcHggMCAwIC0xcHg7XHJcbn1cclxuLmJyb3dzZS1pbWFnZSB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5icm93c2UtaW1hZ2UgaW5wdXQge1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRsZWZ0OiAwO1xyXG5cdHRvcDogMDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0b3BhY2l0eTogMDtcclxuXHR6LWluZGV4OiA5O1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4uZmxleC1kaXJlY3Rpb24tY29sdW1uIHtcclxuXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0ICFpbXBvcnRhbnQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydCAhaW1wb3J0YW50O1x0XHJcbn1cclxuLmZsZXgtZGlyZWN0aW9uLWNvbHVtbiBzbWFsbCB7XHJcblx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdG1hcmdpbjogNnB4IDAgMCAwO1xyXG59XHJcbi5jb250aW51ZS1idXR0b24ge1xyXG5cdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdG1hcmdpbjogMCAwIDEwcHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0cGFkZGluZzogMCAxJTtcclxufVxyXG4uY29udGludWUtYnV0dG9uIGJ1dHRvbiB7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG4gICAgYmFja2dyb3VuZDogIzAwMDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyN3B4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIHBhZGRpbmc6IDAgMTVweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG1hcmdpbjogMCAwIDAgMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbn1cclxuLnVwbG9hZGVkLWltYWdlLXZpZXdlciB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0bWFyZ2luOiAwIC0xJSAwO1xyXG59XHJcbi51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XHJcblx0d2lkdGg6IDEwMHB4O1xyXG5cdGhlaWdodDogMTAwcHg7XHJcblx0bWFyZ2luOiAxJTtcclxuXHRib3gtc2hhZG93OiAwIDAgNXB4IC0zcHggIzAwMDtcclxufVxyXG4udXBsb2FkZWQtaW1hZ2Utdmlld2VyIHAgaW1nIHtcclxuXHR3aWR0aDogMTAwcHg7XHJcblx0aGVpZ2h0OiAxMDBweDtcclxuXHRvYmplY3QtZml0OiBjb3ZlcjtcclxufVxyXG5cclxuLmNhbmNlbC1idXR0b24ge1xyXG5cdGJhY2tncm91bmQ6I2U4ZGZkZiAhaW1wb3J0YW50O1xyXG5cdGNvbG9yOiMwMDAgIWltcG9ydGFudDtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdG1hcmdpbjogMCAwIDIwcHg7XHJcbn1cclxuLmxpLTUwIHtcclxuXHR3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG5cdG1hcmdpbjogMCAwIDIwcHg7XHJcbn1cclxuLmluZm9ybWF0aW9uLWZvcm0gLmZvcm0tZ3JvdXAge1xyXG5cdHdpZHRoOiA0OCU7XHJcblx0bWFyZ2luOiAwIDElIDE1cHg7XHJcbn1cclxuLmFyZWEtaGVpZ2h0IHRleHRhcmVhIHtcclxuICAgIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xyXG59XHJcbi5mbGV4LWRpcmVjdGlvbi1jb2x1bW4ge1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcbi51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XHJcblx0d2lkdGg6IDIzJTtcclxuXHRoZWlnaHQ6IGF1dG87XHJcbn1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcbi51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XHJcblx0d2lkdGg6IDQ4JTtcclxufVxyXG4udXBsb2FkZWQtaW1hZ2Utdmlld2VyIHAgaW1nIHtcclxuXHRoZWlnaHQ6IGF1dG87XHJcbn1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2NDBweCl7XHJcbi5pbmZvcm1hdGlvbi1mb3JtIC5mb3JtLWdyb3VwIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRtYXJnaW46IDAgMCAxNXB4O1xyXG59XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDgwcHgpe1xyXG4udXBsb2FkZWQtaW1hZ2Utdmlld2VyIHAge1xyXG5cdHdpZHRoOiA5OCU7XHJcblx0aGVpZ2h0OiBhdXRvO1xyXG59XHJcblxyXG59IiwiLmhlYWRlci1pbml0aWFsIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjgpO1xufVxuXG4jbWFpbiB7XG4gIHBhZGRpbmc6IDQwcHggMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gIHdpZHRoOiA5OCU7XG4gIG1hcmdpbjogMCAxJSAyMHB4O1xuICBwYWRkaW5nOiAxNXB4IDE1cHggMDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpO1xufVxuXG4ubGktNTAge1xuICB3aWR0aDogNDglICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMCAxJSAyMHB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbjogMjBweCBhdXRvIDMwcHg7XG4gIGJveC1zaGFkb3c6IC0wLjVyZW0gLTAuNXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMik7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIGltZyB7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaDItYmFzZS1oZWFkaW5nIHtcbiAgbWFyZ2luOiAwIDAgMTJweDtcbn1cblxuLmluZm9ybWF0aW9uLWZvcm0ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIG1hcmdpbjogMCAtMSU7XG59XG5cbi5pbmZvcm1hdGlvbi1mb3JtIC5mb3JtLWdyb3VwIHtcbiAgd2lkdGg6IDQ4JTtcbiAgbWFyZ2luOiAwIDElIDE1cHg7XG59XG5cbi5maWxsZWQtdmFsdWUge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMDUpO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBtYXJnaW46IDNweCAwIDAgLTFweDtcbn1cblxuLmJyb3dzZS1pbWFnZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmJyb3dzZS1pbWFnZSBpbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiA5O1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5mbGV4LWRpcmVjdGlvbi1jb2x1bW4ge1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogMTAwJTtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0ICFpbXBvcnRhbnQ7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0ICFpbXBvcnRhbnQ7XG59XG5cbi5mbGV4LWRpcmVjdGlvbi1jb2x1bW4gc21hbGwge1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbjogNnB4IDAgMCAwO1xufVxuXG4uY29udGludWUtYnV0dG9uIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbjogMCAwIDEwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwIDElO1xufVxuXG4uY29udGludWUtYnV0dG9uIGJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGhlaWdodDogMzBweDtcbiAgbGluZS1oZWlnaHQ6IDI3cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgcGFkZGluZzogMCAxNXB4O1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbjogMCAwIDAgMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cblxuLnVwbG9hZGVkLWltYWdlLXZpZXdlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgbWFyZ2luOiAwIC0xJSAwO1xufVxuXG4udXBsb2FkZWQtaW1hZ2Utdmlld2VyIHAge1xuICB3aWR0aDogMTAwcHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIG1hcmdpbjogMSU7XG4gIGJveC1zaGFkb3c6IDAgMCA1cHggLTNweCAjMDAwO1xufVxuXG4udXBsb2FkZWQtaW1hZ2Utdmlld2VyIHAgaW1nIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLmNhbmNlbC1idXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjZThkZmRmICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDAwICFpbXBvcnRhbnQ7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xuICB9XG5cbiAgLmxpLTUwIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMCAwIDIwcHg7XG4gIH1cblxuICAuaW5mb3JtYXRpb24tZm9ybSAuZm9ybS1ncm91cCB7XG4gICAgd2lkdGg6IDQ4JTtcbiAgICBtYXJnaW46IDAgMSUgMTVweDtcbiAgfVxuXG4gIC5hcmVhLWhlaWdodCB0ZXh0YXJlYSB7XG4gICAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XG4gIH1cblxuICAuZmxleC1kaXJlY3Rpb24tY29sdW1uIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlciAhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgfVxuXG4gIC51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XG4gICAgd2lkdGg6IDIzJTtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XG4gICAgd2lkdGg6IDQ4JTtcbiAgfVxuXG4gIC51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCBpbWcge1xuICAgIGhlaWdodDogYXV0bztcbiAgfVxufVxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjQwcHgpIHtcbiAgLmluZm9ybWF0aW9uLWZvcm0gLmZvcm0tZ3JvdXAge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogMCAwIDE1cHg7XG4gIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XG4gIC51cGxvYWRlZC1pbWFnZS12aWV3ZXIgcCB7XG4gICAgd2lkdGg6IDk4JTtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/artist/service-request/service-request.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/@auth/artist/service-request/service-request.component.ts ***!
  \***************************************************************************/
/*! exports provided: NgxArtistServiceRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxArtistServiceRequestComponent", function() { return NgxArtistServiceRequestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");
/* harmony import */ var _core_backend_common_services_service_request_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../@core/backend/common/services/service_request.service */ "./src/app/@core/backend/common/services/service_request.service.ts");







var NgxArtistServiceRequestComponent = /** @class */ (function () {
    function NgxArtistServiceRequestComponent(fb, artistService, ref, serviceRequestService, toasterService, router) {
        this.fb = fb;
        this.artistService = artistService;
        this.ref = ref;
        this.serviceRequestService = serviceRequestService;
        this.toasterService = toasterService;
        this.router = router;
        this.formGroup = this.fb.group({
            price: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            turn_around_time: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    NgxArtistServiceRequestComponent.prototype.ngOnInit = function () {
        this.GetServiceRequestDetails();
    };
    NgxArtistServiceRequestComponent.prototype.GetServiceRequestDetails = function () {
        var _this = this;
        this.serviceRequestService.GetServicRequesteDetails().subscribe(function (result) {
            if (result.status) {
                _this.artist_services_Requests = result.data.service_requests;
                _this.ref.markForCheck();
            }
        });
    };
    NgxArtistServiceRequestComponent.prototype.Accept_Request = function () {
        var _this = this;
        var request = this.selected_request;
        request.price = this.formGroup.value.price;
        request.artist_description = this.formGroup.value.description;
        request.turn_around_time = this.formGroup.value.turn_around_time;
        request.status = 'approved';
        this.serviceRequestService.AcceptServicRequest(request).subscribe(function (result) {
            if (result.status) {
                _this.toasterService.success('Request Accepted !!', result.message);
                $('#close_btn').click();
                _this.GetServiceRequestDetails();
            }
            else {
                _this.toasterService.error('Request Not Accepted !!', result.message);
            }
        });
    };
    ;
    NgxArtistServiceRequestComponent.prototype.OpenTotPage = function (request) {
        this.selected_request = request;
        $('#profile_btn').click();
    };
    NgxArtistServiceRequestComponent.prototype.Reject_Request = function (request) {
        var _this = this;
        request.status = 'rejected';
        this.serviceRequestService.RejectServicRequest(request).subscribe(function (result) {
            if (result.status) {
                _this.toasterService.success('Request Rejected !!', result.message);
                _this.GetServiceRequestDetails();
            }
            else {
                _this.toasterService.error('Request Not Rejected !!', result.message);
            }
        });
    };
    NgxArtistServiceRequestComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_5__["ArtistService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _core_backend_common_services_service_request_service__WEBPACK_IMPORTED_MODULE_6__["ServiceRequestService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    NgxArtistServiceRequestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-artist-service-request',
            template: __webpack_require__(/*! raw-loader!./service-request.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/artist/service-request/service-request.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./service-request.component.scss */ "./src/app/@auth/artist/service-request/service-request.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _core_backend_common_services_artists_services__WEBPACK_IMPORTED_MODULE_5__["ArtistService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _core_backend_common_services_service_request_service__WEBPACK_IMPORTED_MODULE_6__["ServiceRequestService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], NgxArtistServiceRequestComponent);
    return NgxArtistServiceRequestComponent;
}());



/***/ })

}]);
//# sourceMappingURL=artist-artist-module.js.map