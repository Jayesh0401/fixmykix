(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/@auth/auth.guard.ts":
/*!*************************************!*\
  !*** ./src/app/@auth/auth.guard.ts ***!
  \*************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canLoad = function (route) {
        var userId = localStorage.getItem('userid');
        if (userId) {
            return true;
        }
        else {
            this.router.navigate(['']);
            return false;
        }
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/@auth/isAuth.guard.ts":
/*!***************************************!*\
  !*** ./src/app/@auth/isAuth.guard.ts ***!
  \***************************************/
/*! exports provided: IsAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsAuthGuard", function() { return IsAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");





var IsAuthGuard = /** @class */ (function () {
    function IsAuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    IsAuthGuard.prototype.canLoad = function (route) {
        var userId = localStorage.getItem('userid');
        if (userId) {
            this.router.navigate(['user/profile']);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(false);
        }
        else {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(true);
        }
    };
    IsAuthGuard.ctorParameters = function () { return [
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    IsAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], IsAuthGuard);
    return IsAuthGuard;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/address.api.ts":
/*!*********************************************************!*\
  !*** ./src/app/@core/backend/common/api/address.api.ts ***!
  \*********************************************************/
/*! exports provided: AddressApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressApi", function() { return AddressApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");







var AddressApi = /** @class */ (function () {
    function AddressApi(api, http, apiService) {
        this.api = api;
        this.http = http;
        this.apiService = apiService;
        this.url = apiService.url;
    }
    AddressApi.prototype.addAddress = function (data) {
        var url = this.url + 'v1/addresses';
        var token = localStorage.getItem('token');
        return this.http.post(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }),
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.prototype.getAddress = function () {
        var url = this.url + 'v1/addresses';
        var token = localStorage.getItem('token');
        return this.http.get(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }),
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.prototype.ValidateAddress = function (data) {
        var url = this.url + 'v1/united_parcel_services/validate';
        var token = localStorage.getItem('token');
        return this.http.post(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }),
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.prototype.getAddressById = function (id) {
        var url = this.url + 'v1/orders/' + id + '/address';
        var token = localStorage.getItem('token');
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params = params.append("address_id", id);
        return this.http.get(url, { params: params,
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.prototype.saveAddressToOrder = function (id, address) {
        var url = this.url + 'v1/orders/' + id + '/address';
        var token = localStorage.getItem('token');
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        console.log(address);
        params = params.append("address_id", address.id);
        return this.http.get(url, { params: params, headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.prototype.service_demand = function (id) {
        var url = this.url + 'v1/orders/' + id + '/service_demand';
        var token = localStorage.getItem('token');
        return this.http.post(url, {}, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    AddressApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] }
    ]; };
    AddressApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]])
    ], AddressApi);
    return AddressApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/artists.api.ts":
/*!*********************************************************!*\
  !*** ./src/app/@core/backend/common/api/artists.api.ts ***!
  \*********************************************************/
/*! exports provided: ArtistApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistApi", function() { return ArtistApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");







var ArtistApi = /** @class */ (function () {
    function ArtistApi(api, http, apiService) {
        this.api = api;
        this.http = http;
        this.apiService = apiService;
        this.apiController = 'users';
        this.url = apiService.url;
    }
    ArtistApi.prototype.GetServicRequesteDetails = function () {
        var url = this.url + 'v1/service_requests';
        var token = localStorage.getItem('token');
        return this.http.get(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }),
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ArtistApi.prototype.GetServiceDetails = function () {
        var url = this.url + 'v1/services?query';
        var token = localStorage.getItem('token');
        return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ArtistApi.prototype.GetArtistDetailsById = function (id) {
        var url = this.url + 'v1/artists';
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params = params.append("artist_ids", id);
        return this.http.get(url, { params: params }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ArtistApi.prototype.GetArtistDetails = function () {
        var url = this.url + 'v1/artists';
        console.log(url, '---------url');
        return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ArtistApi.prototype.GetAristsByServiceIds = function (data) {
        var url = this.url + 'v1/artists?';
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        data.forEach(function (id) {
            params = params.append("service_ids", id);
        });
        return this.http.get(url, { params: params }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ArtistApi.prototype.GetArtistServicesById = function (id) {
        var url = this.url + 'v1/artist_services/';
        var token = localStorage.getItem('token');
        return this.http.get(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ArtistApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] }
    ]; };
    ArtistApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]])
    ], ArtistApi);
    return ArtistApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/categories.api.ts":
/*!************************************************************!*\
  !*** ./src/app/@core/backend/common/api/categories.api.ts ***!
  \************************************************************/
/*! exports provided: CategoriesApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesApi", function() { return CategoriesApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");







var CategoriesApi = /** @class */ (function () {
    function CategoriesApi(api, http, apiService) {
        this.api = api;
        this.http = http;
        this.apiService = apiService;
        this.apiController = 'users';
        this.url = apiService.url;
    }
    CategoriesApi.prototype.GetCategoriesDetails = function (data) {
        var url = this.url + 'v1/categories';
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        var token = localStorage.getItem('token');
        data.forEach(function (id) {
            params = params.append("categories_ids", id);
        });
        return this.http.get(url, { params: params }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    CategoriesApi.prototype.addService = function (data) {
        var url = this.url + 'v1/artist_services';
        var token = localStorage.getItem('token');
        return this.http.post(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    CategoriesApi.prototype.GetServiceDetails = function (id) {
        var url = this.url + 'v1/artist_services/' + id;
        var token = localStorage.getItem('token');
        return this.http.get(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    CategoriesApi.prototype.deleteService = function (id) {
        var url = this.url + 'v1/artist_services/' + id;
        var token = localStorage.getItem('token');
        return this.http.delete(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'X-User-Token': token }) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            return result;
        }));
    };
    ;
    CategoriesApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] }
    ]; };
    CategoriesApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]])
    ], CategoriesApi);
    return CategoriesApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/countries.api.ts":
/*!***********************************************************!*\
  !*** ./src/app/@core/backend/common/api/countries.api.ts ***!
  \***********************************************************/
/*! exports provided: CountriesApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountriesApi", function() { return CountriesApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var CountriesApi = /** @class */ (function () {
    function CountriesApi(api) {
        this.api = api;
        this.apiController = 'countries';
    }
    CountriesApi.prototype.list = function () {
        return this.api.get(this.apiController);
    };
    CountriesApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] }
    ]; };
    CountriesApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]])
    ], CountriesApi);
    return CountriesApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/http.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/backend/common/api/http.service.ts ***!
  \**********************************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var HttpService = /** @class */ (function () {
    function HttpService(http) {
        this.http = http;
    }
    Object.defineProperty(HttpService.prototype, "apiUrl", {
        get: function () {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        },
        enumerable: true,
        configurable: true
    });
    HttpService.prototype.getServerDataSource = function (uri) {
        return new ng2_smart_table__WEBPACK_IMPORTED_MODULE_4__["ServerDataSource"](this.http, {
            endPoint: uri,
            totalKey: 'totalCount',
            dataKey: 'items',
            pagerPageKey: 'pageNumber',
            pagerLimitKey: 'pageSize',
            filterFieldKey: 'filterBy#field#',
            sortFieldKey: 'sortBy',
            sortDirKey: 'orderBy',
        });
    };
    HttpService.prototype.get = function (endpoint, options) {
        return this.http.get(this.apiUrl + "/" + endpoint, options);
    };
    HttpService.prototype.post = function (endpoint, data, options) {
        return this.http.post(this.apiUrl + "/" + endpoint, data, options);
    };
    HttpService.prototype.put = function (endpoint, data, options) {
        return this.http.put(this.apiUrl + "/" + endpoint, data, options);
    };
    HttpService.prototype.delete = function (endpoint, options) {
        return this.http.delete(this.apiUrl + "/" + endpoint, options);
    };
    HttpService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    HttpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HttpService);
    return HttpService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/service_request.api.ts":
/*!*****************************************************************!*\
  !*** ./src/app/@core/backend/common/api/service_request.api.ts ***!
  \*****************************************************************/
/*! exports provided: ServiceRequestApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceRequestApi", function() { return ServiceRequestApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");





var ServiceRequestApi = /** @class */ (function () {
    function ServiceRequestApi(api, apiService) {
        this.api = api;
        this.apiService = apiService;
        this.url = apiService.url;
    }
    ServiceRequestApi.prototype.GetServicRequesteDetails = function () {
        var url = this.url + 'v1/service_requests';
        var token = localStorage.getItem('token');
        console.log(token, '-------token---');
        return this.api.get(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ServiceRequestApi.prototype.SaveServicRequesteDetails = function (data) {
        var url = this.url + 'v1/service_requests';
        var token = localStorage.getItem('token');
        return this.api.post(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ServiceRequestApi.prototype.AcceptServicRequest = function (details) {
        var url = this.url + 'v1/service_requests/' + details.id + '/accept';
        var token = localStorage.getItem('token');
        var data = {
            service_request: details
        };
        return this.api.put(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ServiceRequestApi.prototype.RejectServicRequest = function (details) {
        var url = this.url + 'v1/service_requests/' + details.id + '/reject';
        var token = localStorage.getItem('token');
        var data = {
            service_requests: details
        };
        return this.api.put(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ServiceRequestApi.prototype.CancelServicRequest = function (details) {
        var url = this.url + 'v1/service_requests/' + details.id + '/cancel';
        var token = localStorage.getItem('token');
        var data = {
            service_requests: details
        };
        return this.api.put(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ServiceRequestApi.prototype.AddToCart = function (data) {
        var url = this.url + 'v1/cart_items/add_to_cart';
        console.log(data, '--------url', typeof data);
        var params = {
            service_ids: data
        };
        // data.forEach(id => {
        //   params = params.append("service_ids",id);
        // });
        var token = localStorage.getItem('token');
        return this.api.post(url, params, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (result) {
            return result;
        }));
    };
    ;
    ServiceRequestApi.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] }
    ]; };
    ServiceRequestApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]])
    ], ServiceRequestApi);
    return ServiceRequestApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/settings.api.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/backend/common/api/settings.api.ts ***!
  \**********************************************************/
/*! exports provided: SettingsApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsApi", function() { return SettingsApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var SettingsApi = /** @class */ (function () {
    function SettingsApi(api) {
        this.api = api;
        this.apiController = 'settings';
    }
    SettingsApi.prototype.getCurrent = function () {
        return this.api.get(this.apiController + "/current");
    };
    SettingsApi.prototype.updateCurrent = function (item) {
        return this.api.put(this.apiController + "/current", item);
    };
    SettingsApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"] }
    ]; };
    SettingsApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"]])
    ], SettingsApi);
    return SettingsApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/users.api.ts":
/*!*******************************************************!*\
  !*** ./src/app/@core/backend/common/api/users.api.ts ***!
  \*******************************************************/
/*! exports provided: UsersApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersApi", function() { return UsersApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");







var UsersApi = /** @class */ (function () {
    function UsersApi(api, http, apiService) {
        this.api = api;
        this.http = http;
        this.apiService = apiService;
        this.apiController = 'users';
        this.url = apiService.url;
    }
    Object.defineProperty(UsersApi.prototype, "usersDataSource", {
        get: function () {
            return this.api.getServerDataSource(this.api.apiUrl + "/" + this.apiController);
        },
        enumerable: true,
        configurable: true
    });
    UsersApi.prototype.list = function (pageNumber, pageSize) {
        var _this = this;
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('pageNumber', "" + pageNumber)
            .set('pageSize', "" + pageSize);
        return this.api.get(this.apiController, { params: params })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) { return data.map(function (item) {
            var picture = _this.api.apiUrl + "/" + _this.apiController + "/" + item.id + "/photo";
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, item, { picture: picture });
        }); }));
    };
    UsersApi.prototype.getCurrent = function () {
        var _this = this;
        return this.api.get(this.apiController + "/current")
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
            var picture = _this.api.apiUrl + "/" + _this.apiController + "/" + data.id + "/photo";
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, data, { picture: picture });
        }));
    };
    UsersApi.prototype.get = function (id) {
        var _this = this;
        return this.api.get(this.apiController + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
            var picture = _this.api.apiUrl + "/" + _this.apiController + "/" + data.id + "/photo";
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, data, { picture: picture });
        }));
    };
    UsersApi.prototype.delete = function (id) {
        return this.api.delete(this.apiController + "/" + id);
    };
    UsersApi.prototype.add = function (user) {
        var resp = {};
        var url = this.url + 'v1/users/sign_up';
        return this.http.post(url, user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (result) {
            resp = result;
            return resp;
        }));
    };
    UsersApi.prototype.updateCurrent = function (item) {
        return this.api.put(this.apiController + "/current", item);
    };
    UsersApi.prototype.update = function (item) {
        return this.api.put(this.apiController + "/" + item.id, item);
    };
    UsersApi.prototype.getUserRoles = function () {
        return this.http.get("/api/v1/user/userRoles", {});
    };
    UsersApi.prototype.getUsersList = function () {
        return this.http.get("/api/v1/user/getUsers", {})
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (user) {
            return user;
        }));
    };
    UsersApi.prototype.getUserById = function (item) {
        return this.http.get("/api/v1/user/edit?id=" + item).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (user) {
            console.log("getUserById", user);
            return user;
        }));
    };
    UsersApi.prototype.updateUser = function (userModel) {
        var userid = JSON.parse(localStorage.getItem("userid"));
        var token = localStorage.getItem('token');
        var url = this.url + 'v1/artist_details';
        return this.http.post(url, userModel, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('X-User-Token', token) }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (user) {
            return user;
        }));
    };
    ;
    UsersApi.prototype.deleteUser = function (id, status) {
        var data = {
            id: id,
            status: status
        };
        return this.http.post("api/v1/user/delete/", data).pipe(function (resp) {
            return resp;
        });
    };
    ;
    UsersApi.prototype.logout = function () {
        var token = localStorage.getItem('token');
        var url = 'https://fixmykix-backend.herokuapp.com/v1/users/logout';
        return this.http.delete(url, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('X-User-Token', token) }).pipe(function (resp) {
            return resp;
        });
    };
    ;
    UsersApi.ctorParameters = function () { return [
        { type: _http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"] }
    ]; };
    UsersApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_http_service__WEBPACK_IMPORTED_MODULE_3__["HttpService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]])
    ], UsersApi);
    return UsersApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/api/utils.api.ts":
/*!*******************************************************!*\
  !*** ./src/app/@core/backend/common/api/utils.api.ts ***!
  \*******************************************************/
/*! exports provided: UtilsApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtilsApi", function() { return UtilsApi; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var UtilsApi = /** @class */ (function () {
    function UtilsApi(api) {
        this.api = api;
        // private readonly apiController: string = '/api/v1/datatable/getCatalogData';
        this.dummy = 'api/v1/datatable/getCatalogData';
    }
    UtilsApi.prototype.getRole = function () {
        return this.api.get(this.dummy);
    };
    UtilsApi.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    UtilsApi = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UtilsApi);
    return UtilsApi;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/common-backend.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/@core/backend/common/common-backend.module.ts ***!
  \***************************************************************/
/*! exports provided: CommonBackendModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonBackendModule", function() { return CommonBackendModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _interfaces_common_users__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/common/users */ "./src/app/@core/interfaces/common/users.ts");
/* harmony import */ var _services_users_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var _api_users_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./api/users.api */ "./src/app/@core/backend/common/api/users.api.ts");
/* harmony import */ var _api_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./api/http.service */ "./src/app/@core/backend/common/api/http.service.ts");
/* harmony import */ var _interfaces_common_countries__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interfaces/common/countries */ "./src/app/@core/interfaces/common/countries.ts");
/* harmony import */ var _services_countries_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/countries.service */ "./src/app/@core/backend/common/services/countries.service.ts");
/* harmony import */ var _api_countries_api__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./api/countries.api */ "./src/app/@core/backend/common/api/countries.api.ts");
/* harmony import */ var _api_settings_api__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./api/settings.api */ "./src/app/@core/backend/common/api/settings.api.ts");
/* harmony import */ var _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../interfaces/common/settings */ "./src/app/@core/interfaces/common/settings.ts");
/* harmony import */ var _services_settings_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/settings.service */ "./src/app/@core/backend/common/services/settings.service.ts");
/* harmony import */ var _services_upload_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/upload.service */ "./src/app/@core/backend/common/services/upload.service.ts");
/* harmony import */ var _services_search_filter_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/search-filter.service */ "./src/app/@core/backend/common/services/search-filter.service.ts");
/* harmony import */ var _api_artists_api__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./api/artists.api */ "./src/app/@core/backend/common/api/artists.api.ts");
/* harmony import */ var _services_api_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/api.service */ "./src/app/@core/backend/common/services/api.service.ts");
/* harmony import */ var _services_artists_services__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/artists.services */ "./src/app/@core/backend/common/services/artists.services.ts");
/* harmony import */ var _services_service_request_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/service_request.service */ "./src/app/@core/backend/common/services/service_request.service.ts");
/* harmony import */ var _api_service_request_api__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./api/service_request.api */ "./src/app/@core/backend/common/api/service_request.api.ts");
/* harmony import */ var _services_chat_services__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/chat.services */ "./src/app/@core/backend/common/services/chat.services.ts");
/* harmony import */ var _services_categories_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/categories.service */ "./src/app/@core/backend/common/services/categories.service.ts");
/* harmony import */ var _api_categories_api__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./api/categories.api */ "./src/app/@core/backend/common/api/categories.api.ts");
/* harmony import */ var _services_address_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./services/address.service */ "./src/app/@core/backend/common/services/address.service.ts");
/* harmony import */ var _api_address_api__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./api/address.api */ "./src/app/@core/backend/common/api/address.api.ts");

























var API = [_api_users_api__WEBPACK_IMPORTED_MODULE_5__["UsersApi"], _api_countries_api__WEBPACK_IMPORTED_MODULE_9__["CountriesApi"], _api_settings_api__WEBPACK_IMPORTED_MODULE_10__["SettingsApi"], _api_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"], _services_upload_service__WEBPACK_IMPORTED_MODULE_13__["UploadService"], _services_search_filter_service__WEBPACK_IMPORTED_MODULE_14__["SearchFilterService"], _services_api_service__WEBPACK_IMPORTED_MODULE_16__["ApiService"], _api_artists_api__WEBPACK_IMPORTED_MODULE_15__["ArtistApi"], _api_service_request_api__WEBPACK_IMPORTED_MODULE_19__["ServiceRequestApi"], _api_categories_api__WEBPACK_IMPORTED_MODULE_22__["CategoriesApi"], _api_address_api__WEBPACK_IMPORTED_MODULE_24__["AddressApi"]];
var SERVICES = [
    { provide: _interfaces_common_users__WEBPACK_IMPORTED_MODULE_3__["UserData"], useClass: _services_users_service__WEBPACK_IMPORTED_MODULE_4__["UsersService"] },
    { provide: _interfaces_common_countries__WEBPACK_IMPORTED_MODULE_7__["CountryData"], useClass: _services_countries_service__WEBPACK_IMPORTED_MODULE_8__["CountriesService"] },
    { provide: _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_11__["SettingsData"], useClass: _services_settings_service__WEBPACK_IMPORTED_MODULE_12__["SettingsService"] },
    _services_chat_services__WEBPACK_IMPORTED_MODULE_20__["ChatService"],
    _services_categories_service__WEBPACK_IMPORTED_MODULE_21__["CategoriesService"],
    _services_address_service__WEBPACK_IMPORTED_MODULE_23__["AddressService"]
];
var CommonBackendModule = /** @class */ (function () {
    function CommonBackendModule() {
    }
    CommonBackendModule_1 = CommonBackendModule;
    CommonBackendModule.forRoot = function () {
        return {
            ngModule: CommonBackendModule_1,
            providers: API.concat(SERVICES, [
                _services_artists_services__WEBPACK_IMPORTED_MODULE_17__["ArtistService"],
                _services_service_request_service__WEBPACK_IMPORTED_MODULE_18__["ServiceRequestService"]
            ]),
        };
    };
    var CommonBackendModule_1;
    CommonBackendModule = CommonBackendModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
        })
    ], CommonBackendModule);
    return CommonBackendModule;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/address.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/@core/backend/common/services/address.service.ts ***!
  \******************************************************************/
/*! exports provided: AddressService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressService", function() { return AddressService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_address_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/address.api */ "./src/app/@core/backend/common/api/address.api.ts");



var AddressService = /** @class */ (function () {
    function AddressService(api) {
        this.api = api;
    }
    ;
    AddressService.prototype.addAddress = function (data) {
        return this.api.addAddress(data);
    };
    ;
    AddressService.prototype.getAddress = function () {
        return this.api.getAddress();
    };
    ;
    AddressService.prototype.ValidateAddress = function (data) {
        return this.api.ValidateAddress(data);
    };
    AddressService.prototype.getAddressById = function (id) {
        return this.api.getAddressById(id);
    };
    ;
    AddressService.prototype.saveAddressToOrder = function (id, address) {
        return this.api.saveAddressToOrder(id, address);
    };
    ;
    AddressService.prototype.service_demand = function (id) {
        return this.api.service_demand(id);
    };
    ;
    AddressService.ctorParameters = function () { return [
        { type: _api_address_api__WEBPACK_IMPORTED_MODULE_2__["AddressApi"] }
    ]; };
    AddressService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_address_api__WEBPACK_IMPORTED_MODULE_2__["AddressApi"]])
    ], AddressService);
    return AddressService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/api.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/@core/backend/common/services/api.service.ts ***!
  \**************************************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
var ApiService = /** @class */ (function () {
    function ApiService() {
        this.url = 'https://fixmykix-backend.herokuapp.com//';
    }
    return ApiService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/artists.services.ts":
/*!*******************************************************************!*\
  !*** ./src/app/@core/backend/common/services/artists.services.ts ***!
  \*******************************************************************/
/*! exports provided: ArtistService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArtistService", function() { return ArtistService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_artists_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/artists.api */ "./src/app/@core/backend/common/api/artists.api.ts");



var ArtistService = /** @class */ (function () {
    function ArtistService(api) {
        this.api = api;
        // super();
    }
    ArtistService.prototype.GetServicRequesteDetails = function () {
        console.log('Service Called');
        return this.api.GetServicRequesteDetails();
    };
    ArtistService.prototype.GetServiceDetails = function () {
        return this.api.GetServiceDetails();
    };
    ArtistService.prototype.GetArtistDetailsById = function (id) {
        return this.api.GetArtistDetailsById(id);
    };
    ArtistService.prototype.GetArtistDetails = function () {
        return this.api.GetArtistDetails();
    };
    ;
    ArtistService.prototype.GetAristsByServiceIds = function (data) {
        return this.api.GetAristsByServiceIds(data);
    };
    ;
    ArtistService.prototype.GetArtistServicesById = function (id) {
        return this.api.GetArtistServicesById(id);
    };
    ;
    ArtistService.ctorParameters = function () { return [
        { type: _api_artists_api__WEBPACK_IMPORTED_MODULE_2__["ArtistApi"] }
    ]; };
    ArtistService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_artists_api__WEBPACK_IMPORTED_MODULE_2__["ArtistApi"]])
    ], ArtistService);
    return ArtistService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/authentication.service.ts":
/*!*************************************************************************!*\
  !*** ./src/app/@core/backend/common/services/authentication.service.ts ***!
  \*************************************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./api.service */ "./src/app/@core/backend/common/services/api.service.ts");







var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(router, http, apiService) {
        this.router = router;
        this.http = http;
        this.apiService = apiService;
        this.url = apiService.url;
    }
    AuthenticationService.prototype.login = function (user_details) {
        var login_url = this.url + '/v1/users/sign_in';
        return this.http.post(login_url, user_details)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (user) {
            return user;
        }));
    };
    AuthenticationService.prototype.isSession = function () {
        var user = localStorage.getItem('user');
        this.router.navigate(['/auth/login']);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])(false);
    };
    AuthenticationService.prototype.isLogOut = function () {
        var _this = this;
        var url = this.url + '/v1/users/logout';
        return this.http.get("/api/v1/user/logout", {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (resp) {
            if (resp.success) {
                localStorage.removeItem('token');
                localStorage.clear();
                _this.router.navigate(['/auth/login']);
                return false;
            }
            else {
                return true;
            }
        }));
    };
    AuthenticationService.prototype.register = function (user) {
        var _this = this;
        return this.http.post("/api/v1/user/add", user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (resp) {
            if (resp.success) {
                localStorage.clear();
                resp.redirect = '/auth/login';
                _this.router.navigate(['/auth/login']);
                return false;
            }
            else {
                return true;
            }
        }));
    };
    AuthenticationService.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] }
    ]; };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/categories.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/@core/backend/common/services/categories.service.ts ***!
  \*********************************************************************/
/*! exports provided: CategoriesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesService", function() { return CategoriesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_categories_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/categories.api */ "./src/app/@core/backend/common/api/categories.api.ts");



var CategoriesService = /** @class */ (function () {
    function CategoriesService(api) {
        this.api = api;
    }
    CategoriesService.prototype.GetCategoriesDetails = function (data) {
        return this.api.GetCategoriesDetails(data);
    };
    ;
    CategoriesService.prototype.addService = function (data) {
        return this.api.addService(data);
    };
    ;
    CategoriesService.prototype.GetServiceDetails = function (id) {
        return this.api.GetServiceDetails(id);
    };
    ;
    CategoriesService.prototype.deleteService = function (id) {
        return this.api.deleteService(id);
    };
    ;
    CategoriesService.ctorParameters = function () { return [
        { type: _api_categories_api__WEBPACK_IMPORTED_MODULE_2__["CategoriesApi"] }
    ]; };
    CategoriesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_categories_api__WEBPACK_IMPORTED_MODULE_2__["CategoriesApi"]])
    ], CategoriesService);
    return CategoriesService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/chat.services.ts":
/*!****************************************************************!*\
  !*** ./src/app/@core/backend/common/services/chat.services.ts ***!
  \****************************************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/fesm5/ngx-socket-io.js");
/* harmony import */ var rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/Observable */ "./node_modules/rxjs-compat/_esm5/Observable.js");




var ChatService = /** @class */ (function () {
    function ChatService(socket) {
        this.socket = socket;
    }
    ChatService.prototype.sendMessage = function (msg) {
        this.socket.emit("message", msg);
    };
    ChatService.prototype.getMessage = function () {
        var _this = this;
        return rxjs_Observable__WEBPACK_IMPORTED_MODULE_3__["Observable"].create(function (observer) {
            _this.socket.on('messages', function (message) {
                observer.next(message);
                return message;
            });
        });
    };
    ChatService.ctorParameters = function () { return [
        { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_2__["Socket"] }
    ]; };
    ChatService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [ngx_socket_io__WEBPACK_IMPORTED_MODULE_2__["Socket"]])
    ], ChatService);
    return ChatService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/countries.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/@core/backend/common/services/countries.service.ts ***!
  \********************************************************************/
/*! exports provided: CountriesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountriesService", function() { return CountriesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_countries_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/countries.api */ "./src/app/@core/backend/common/api/countries.api.ts");
/* harmony import */ var _interfaces_common_countries__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../interfaces/common/countries */ "./src/app/@core/interfaces/common/countries.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var CountriesService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CountriesService, _super);
    function CountriesService(api) {
        var _this = _super.call(this) || this;
        _this.api = api;
        return _this;
    }
    CountriesService.prototype.list = function () {
        return this.api.list();
    };
    CountriesService.ctorParameters = function () { return [
        { type: _api_countries_api__WEBPACK_IMPORTED_MODULE_2__["CountriesApi"] }
    ]; };
    CountriesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_countries_api__WEBPACK_IMPORTED_MODULE_2__["CountriesApi"]])
    ], CountriesService);
    return CountriesService;
}(_interfaces_common_countries__WEBPACK_IMPORTED_MODULE_3__["CountryData"]));



/***/ }),

/***/ "./src/app/@core/backend/common/services/search-filter.service.ts":
/*!************************************************************************!*\
  !*** ./src/app/@core/backend/common/services/search-filter.service.ts ***!
  \************************************************************************/
/*! exports provided: SearchFilterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchFilterService", function() { return SearchFilterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var SearchFilterService = /** @class */ (function () {
    function SearchFilterService(api) {
        this.api = api;
        this.criteria = {
            "tags": [],
            "catalog_departments": [],
            "assets_types": [],
            "incident_date": "",
            "catalog_location": [],
            "catalog_classification": "",
            "src_system": [],
            "catalog_type": ""
        };
    }
    SearchFilterService.prototype.getCriteria = function () {
        return this.criteria;
    };
    SearchFilterService.prototype.searchFilter = function () {
        var url = "api/v1/catalogs/searchCatalog";
        console.log(this.criteria, '----this.criteria---');
        return this.api.post(url, this.criteria).pipe(function (resp) {
            return resp;
        });
    };
    SearchFilterService.prototype.setTags = function (tags) {
        this.criteria.tags = tags;
    };
    ;
    SearchFilterService.prototype.setDepartment = function (depts) {
        this.criteria.catalog_departments = depts;
    };
    ;
    SearchFilterService.prototype.setAssetTypes = function (assets_types) {
        this.criteria.assets_types = assets_types;
    };
    ;
    SearchFilterService.prototype.setIncident_date = function (incident_date) {
        debugger;
        this.criteria.incident_date = incident_date;
        console.log("setIncident_date", this.criteria.incident_date);
    };
    ;
    SearchFilterService.prototype.setCatalogLocation = function (catalog_location) {
        this.criteria.catalog_location = catalog_location;
    };
    ;
    SearchFilterService.prototype.setCatalog_type = function (catalog_type) {
        this.criteria.catalog_type = catalog_type;
    };
    ;
    SearchFilterService.prototype.setClassification = function (catalog_classification) {
        this.criteria.catalog_classification = catalog_classification;
    };
    SearchFilterService.prototype.setSourceSystem = function (catalog_source) {
        this.criteria.src_system = catalog_source;
        // console.log("Source System", this.criteria.src_system)
    };
    SearchFilterService.prototype.resetSearch = function () {
        this.setTags([]);
        this.setDepartment([]);
        this.setAssetTypes([]);
        this.setIncident_date('');
        this.setCatalogLocation([]);
        this.setCatalog_type('');
        this.setClassification('');
        this.setSourceSystem('');
    };
    SearchFilterService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    SearchFilterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], SearchFilterService);
    return SearchFilterService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/service_request.service.ts":
/*!**************************************************************************!*\
  !*** ./src/app/@core/backend/common/services/service_request.service.ts ***!
  \**************************************************************************/
/*! exports provided: ServiceRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceRequestService", function() { return ServiceRequestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_service_request_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/service_request.api */ "./src/app/@core/backend/common/api/service_request.api.ts");



var ServiceRequestService = /** @class */ (function () {
    function ServiceRequestService(api) {
        this.api = api;
        // super();
    }
    ServiceRequestService.prototype.GetServicRequesteDetails = function () {
        return this.api.GetServicRequesteDetails();
    };
    ServiceRequestService.prototype.SaveServicRequesteDetails = function (details) {
        return this.api.SaveServicRequesteDetails(details);
    };
    ServiceRequestService.prototype.AcceptServicRequest = function (details) {
        return this.api.AcceptServicRequest(details);
    };
    ServiceRequestService.prototype.RejectServicRequest = function (details) {
        return this.api.RejectServicRequest(details);
    };
    ServiceRequestService.prototype.CancelServicRequest = function (details) {
        return this.api.CancelServicRequest(details);
    };
    ServiceRequestService.prototype.AddToCart = function (ids) {
        return this.api.AddToCart(ids);
    };
    ServiceRequestService.ctorParameters = function () { return [
        { type: _api_service_request_api__WEBPACK_IMPORTED_MODULE_2__["ServiceRequestApi"] }
    ]; };
    ServiceRequestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_service_request_api__WEBPACK_IMPORTED_MODULE_2__["ServiceRequestApi"]])
    ], ServiceRequestService);
    return ServiceRequestService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/settings.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/@core/backend/common/services/settings.service.ts ***!
  \*******************************************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_settings_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/settings.api */ "./src/app/@core/backend/common/api/settings.api.ts");
/* harmony import */ var _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../interfaces/common/settings */ "./src/app/@core/interfaces/common/settings.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var SettingsService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SettingsService, _super);
    function SettingsService(api) {
        var _this = _super.call(this) || this;
        _this.api = api;
        return _this;
    }
    SettingsService.prototype.getCurrentSetting = function () {
        return this.api.getCurrent();
    };
    SettingsService.prototype.updateCurrent = function (setting) {
        return this.api.updateCurrent(setting);
    };
    SettingsService.ctorParameters = function () { return [
        { type: _api_settings_api__WEBPACK_IMPORTED_MODULE_2__["SettingsApi"] }
    ]; };
    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_settings_api__WEBPACK_IMPORTED_MODULE_2__["SettingsApi"]])
    ], SettingsService);
    return SettingsService;
}(_interfaces_common_settings__WEBPACK_IMPORTED_MODULE_3__["SettingsData"]));



/***/ }),

/***/ "./src/app/@core/backend/common/services/upload.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/@core/backend/common/services/upload.service.ts ***!
  \*****************************************************************/
/*! exports provided: UploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadService", function() { return UploadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var UploadService = /** @class */ (function () {
    function UploadService(httpClient) {
        this.httpClient = httpClient;
    }
    // public upload(data: any): Observable<any> {
    //   let uploadURL = 'api/v1/fileUpload/upload';
    //   return this.httpClient.post<any>(uploadURL, data, {
    //     reportProgress: true,
    //     observe: 'events'
    //   }).pipe(map((event) => {
    //     switch (event.type) {
    //       case HttpEventType.Response:
    //         return event.body;
    //       default:
    //         return `Unhandled event: ${event.type}`;
    //     }
    //   })
    //   );
    // }
    UploadService.prototype.upload = function (formData) {
        var uploadURL = 'api/v1/fileUpload/upload';
        return this.httpClient.post(uploadURL, formData).pipe(function (resp) {
            return resp;
        });
    };
    UploadService.prototype.deleteById = function (new_obj) {
        var url = 'api/v1/utils/deletebyId';
        return this.httpClient.post(url, new_obj).pipe(function (resp) {
            return resp;
        });
    };
    UploadService.prototype.filedeleteById = function (id) {
        var url = 'api/v1/fileUpload/deleteFile';
        return this.httpClient.post(url, id).pipe(function (resp) {
            return resp;
        });
    };
    UploadService.prototype.convertFileToBase64 = function (filesArr) {
        var uploadURL = 'api/v1/utils/convertToBase64';
        return this.httpClient.post(uploadURL, filesArr).pipe(function (resp) {
            return resp;
        });
    };
    UploadService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    UploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "./src/app/@core/backend/common/services/users.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/@core/backend/common/services/users.service.ts ***!
  \****************************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _api_users_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/users.api */ "./src/app/@core/backend/common/api/users.api.ts");
/* harmony import */ var src_app_core_interfaces_common_usermodel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/@core/interfaces/common/usermodel */ "./src/app/@core/interfaces/common/usermodel.ts");





var UsersService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UsersService, _super);
    function UsersService(api, http) {
        var _this = 
        // this.userModel = this.api.userModel;
        _super.call(this) || this;
        _this.api = api;
        _this.http = http;
        _this.userModel = new src_app_core_interfaces_common_usermodel__WEBPACK_IMPORTED_MODULE_4__["UserModel"]();
        return _this;
    }
    UsersService.prototype.resetUserModel = function () {
        this.userModel = new src_app_core_interfaces_common_usermodel__WEBPACK_IMPORTED_MODULE_4__["UserModel"]();
    };
    UsersService.prototype.getModel = function () {
        return this.userModel;
    };
    Object.defineProperty(UsersService.prototype, "gridDataSource", {
        get: function () {
            return this.api.usersDataSource;
        },
        enumerable: true,
        configurable: true
    });
    UsersService.prototype.list = function (pageNumber, pageSize) {
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return this.api.list(pageNumber, pageSize);
    };
    UsersService.prototype.get = function (id) {
        return this.api.getUserById(id);
    };
    UsersService.prototype.create = function (user) {
        return this.api.add(user);
    };
    UsersService.prototype.update = function (user) {
        return this.api.update(user);
    };
    UsersService.prototype.updateCurrent = function (user) {
        return this.api.updateCurrent(user);
    };
    UsersService.prototype.delete = function (id, status) {
        return this.api.deleteUser(id, status);
    };
    UsersService.prototype.getUserRoles = function () {
        return this.api.getUserRoles();
    };
    UsersService.prototype.getUsersList = function () {
        return this.api.getUsersList();
    };
    UsersService.prototype.updateUser = function (user) {
        return this.api.updateUser(user);
    };
    UsersService.prototype.logout = function () {
        return this.api.logout();
    };
    UsersService.ctorParameters = function () { return [
        { type: _api_users_api__WEBPACK_IMPORTED_MODULE_3__["UsersApi"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_users_api__WEBPACK_IMPORTED_MODULE_3__["UsersApi"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UsersService);
    return UsersService;
}(src_app_core_interfaces_common_usermodel__WEBPACK_IMPORTED_MODULE_4__["UserModel"]));



/***/ }),

/***/ "./src/app/@core/backend/common/services/utils.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/@core/backend/common/services/utils.service.ts ***!
  \****************************************************************/
/*! exports provided: UtilService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtilService", function() { return UtilService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _api_utils_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/utils.api */ "./src/app/@core/backend/common/api/utils.api.ts");
/* harmony import */ var _interfaces_common_catalog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../interfaces/common/catalog */ "./src/app/@core/interfaces/common/catalog.ts");
/* harmony import */ var _interfaces_common_users__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../interfaces/common/users */ "./src/app/@core/interfaces/common/users.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var UtilService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UtilService, _super);
    function UtilService(api, usersService) {
        var _this = _super.call(this) || this;
        _this.api = api;
        _this.usersService = usersService;
        _this.usersList = [];
        _this.usersRoles = [];
        _this.current_user = {};
        if (_this.usersList.length == 0) {
            _this.usersListApi();
        }
        if (_this.usersRoles.length == 0) {
            _this.UsersRolesApi();
        }
        return _this;
    }
    UtilService.prototype.usersListApi = function () {
    };
    UtilService.prototype.UsersRolesApi = function () {
    };
    UtilService.prototype.getUserRoles = function () {
        return this.usersRoles;
    };
    UtilService.prototype.getUsersList = function () {
        return this.usersList;
    };
    UtilService.prototype.setCurrentUser = function (user) {
        this.current_user = user;
    };
    ;
    UtilService.prototype.getCurrentUser = function () {
        return this.current_user;
    };
    UtilService.prototype.getRole = function () {
        return this.api.getRole();
    };
    UtilService.ctorParameters = function () { return [
        { type: _api_utils_api__WEBPACK_IMPORTED_MODULE_2__["UtilsApi"] },
        { type: _interfaces_common_users__WEBPACK_IMPORTED_MODULE_4__["UserData"] }
    ]; };
    UtilService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_utils_api__WEBPACK_IMPORTED_MODULE_2__["UtilsApi"], _interfaces_common_users__WEBPACK_IMPORTED_MODULE_4__["UserData"]])
    ], UtilService);
    return UtilService;
}(_interfaces_common_catalog__WEBPACK_IMPORTED_MODULE_3__["Catalog"]));



/***/ }),

/***/ "./src/app/@core/core.module.ts":
/*!**************************************!*\
  !*** ./src/app/@core/core.module.ts ***!
  \**************************************/
/*! exports provided: NB_CORE_PROVIDERS, CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NB_CORE_PROVIDERS", function() { return NB_CORE_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _module_import_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./module-import-guard */ "./src/app/@core/module-import-guard.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils */ "./src/app/@core/utils/index.ts");
/* harmony import */ var _backend_common_common_backend_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./backend/common/common-backend.module */ "./src/app/@core/backend/common/common-backend.module.ts");
/* harmony import */ var _mock_common_common_mock_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mock/common/common-mock.module */ "./src/app/@core/mock/common/common-mock.module.ts");
/* harmony import */ var _mock_iot_iot_mock_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./mock/iot/iot-mock.module */ "./src/app/@core/mock/iot/iot-mock.module.ts");
/* harmony import */ var _stores_user_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./stores/user.store */ "./src/app/@core/stores/user.store.ts");
/* harmony import */ var _backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./backend/common/services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var _backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");
/* harmony import */ var _backend_common_services_settings_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./backend/common/services/settings.service */ "./src/app/@core/backend/common/services/settings.service.ts");
/* harmony import */ var _backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _backend_common_api_utils_api__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./backend/common/api/utils.api */ "./src/app/@core/backend/common/api/utils.api.ts");














var NB_CORE_PROVIDERS = _mock_common_common_mock_module__WEBPACK_IMPORTED_MODULE_6__["CommonMockModule"].forRoot().providers.concat(_backend_common_common_backend_module__WEBPACK_IMPORTED_MODULE_5__["CommonBackendModule"].forRoot().providers, _mock_iot_iot_mock_module__WEBPACK_IMPORTED_MODULE_7__["IotMockModule"].forRoot().providers, [
    _utils__WEBPACK_IMPORTED_MODULE_4__["AnalyticsService"],
    _utils__WEBPACK_IMPORTED_MODULE_4__["LayoutService"],
    _utils__WEBPACK_IMPORTED_MODULE_4__["PlayerService"],
    _utils__WEBPACK_IMPORTED_MODULE_4__["StateService"],
]);
var CoreModule = /** @class */ (function () {
    function CoreModule(parentModule) {
        Object(_module_import_guard__WEBPACK_IMPORTED_MODULE_3__["throwIfAlreadyLoaded"])(parentModule, 'CoreModule');
    }
    CoreModule_1 = CoreModule;
    CoreModule.forRoot = function () {
        return {
            ngModule: CoreModule_1,
            providers: NB_CORE_PROVIDERS.concat([
                _stores_user_store__WEBPACK_IMPORTED_MODULE_8__["UserStore"],
                _backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_9__["UsersService"],
                _backend_common_services_settings_service__WEBPACK_IMPORTED_MODULE_11__["SettingsService"],
                _backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"]
            ]),
        };
    };
    var CoreModule_1;
    CoreModule.ctorParameters = function () { return [
        { type: CoreModule, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["SkipSelf"] }] }
    ]; };
    CoreModule = CoreModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            ],
            exports: [],
            declarations: [],
            providers: [
                _backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_12__["AuthenticationService"],
                _backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_10__["UtilService"],
                _backend_common_api_utils_api__WEBPACK_IMPORTED_MODULE_13__["UtilsApi"],
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["SkipSelf"])()),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [CoreModule])
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/catalog.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/interfaces/common/catalog.ts ***!
  \****************************************************/
/*! exports provided: Catalog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Catalog", function() { return Catalog; });
var Catalog = /** @class */ (function () {
    function Catalog() {
        this.catalog_id = null;
        this.catalog_name = '';
        this.catalog_info = '';
        this.catalog_department = null;
        this.catalog_classification = '';
        this.srcsystem = '';
        this.catalog_location = '';
        this.incident_id = '';
        this.incident_name = '';
        this.incident_date = '';
        this.owners = [];
        this.path = [];
        this.tags = [];
        this.comments = '';
        this.created_at = '';
        this.updated_at = '';
        this.created_by = '';
        this.updated_by = '';
        this.catalog_xaxis = '';
        this.catalog_yaxis = '';
    }
    return Catalog;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/countries.ts":
/*!******************************************************!*\
  !*** ./src/app/@core/interfaces/common/countries.ts ***!
  \******************************************************/
/*! exports provided: CountryData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryData", function() { return CountryData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var CountryData = /** @class */ (function () {
    function CountryData() {
    }
    return CountryData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/settings.ts":
/*!*****************************************************!*\
  !*** ./src/app/@core/interfaces/common/settings.ts ***!
  \*****************************************************/
/*! exports provided: SettingsData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsData", function() { return SettingsData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var SettingsData = /** @class */ (function () {
    function SettingsData() {
    }
    return SettingsData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/smart-table.ts":
/*!********************************************************!*\
  !*** ./src/app/@core/interfaces/common/smart-table.ts ***!
  \********************************************************/
/*! exports provided: SmartTableData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableData", function() { return SmartTableData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var SmartTableData = /** @class */ (function () {
    function SmartTableData() {
    }
    return SmartTableData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/usermodel.ts":
/*!******************************************************!*\
  !*** ./src/app/@core/interfaces/common/usermodel.ts ***!
  \******************************************************/
/*! exports provided: UserModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModel", function() { return UserModel; });
var UserModel = /** @class */ (function () {
    function UserModel() {
        this.id = null;
        this.role = '';
        this.name = '';
        this.email = '';
        this.mno = null;
        this.groups = '';
        this.created_by = '';
        this.updated_by = '';
    }
    return UserModel;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/common/users.ts":
/*!**************************************************!*\
  !*** ./src/app/@core/interfaces/common/users.ts ***!
  \**************************************************/
/*! exports provided: UserData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserData", function() { return UserData; });
/* harmony import */ var _usermodel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./usermodel */ "./src/app/@core/interfaces/common/usermodel.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

usermodel: _usermodel__WEBPACK_IMPORTED_MODULE_0__["UserModel"];
var UserData = /** @class */ (function () {
    function UserData() {
    }
    return UserData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/devices.ts":
/*!*************************************************!*\
  !*** ./src/app/@core/interfaces/iot/devices.ts ***!
  \*************************************************/
/*! exports provided: DevicesData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesData", function() { return DevicesData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var DevicesData = /** @class */ (function () {
    function DevicesData() {
    }
    return DevicesData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/electricity.ts":
/*!*****************************************************!*\
  !*** ./src/app/@core/interfaces/iot/electricity.ts ***!
  \*****************************************************/
/*! exports provided: ElectricityData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElectricityData", function() { return ElectricityData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var ElectricityData = /** @class */ (function () {
    function ElectricityData() {
    }
    return ElectricityData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/phone.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/interfaces/iot/phone.ts ***!
  \***********************************************/
/*! exports provided: PhoneData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneData", function() { return PhoneData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var PhoneData = /** @class */ (function () {
    function PhoneData() {
    }
    return PhoneData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/security-cameras.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/interfaces/iot/security-cameras.ts ***!
  \**********************************************************/
/*! exports provided: SecurityCamerasData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCamerasData", function() { return SecurityCamerasData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var SecurityCamerasData = /** @class */ (function () {
    function SecurityCamerasData() {
    }
    return SecurityCamerasData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/solar.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/interfaces/iot/solar.ts ***!
  \***********************************************/
/*! exports provided: SolarData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolarData", function() { return SolarData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var SolarData = /** @class */ (function () {
    function SolarData() {
    }
    return SolarData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/temperature-humidity.ts":
/*!**************************************************************!*\
  !*** ./src/app/@core/interfaces/iot/temperature-humidity.ts ***!
  \**************************************************************/
/*! exports provided: TemperatureHumidityData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemperatureHumidityData", function() { return TemperatureHumidityData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var TemperatureHumidityData = /** @class */ (function () {
    function TemperatureHumidityData() {
    }
    return TemperatureHumidityData;
}());



/***/ }),

/***/ "./src/app/@core/interfaces/iot/traffic-chart.ts":
/*!*******************************************************!*\
  !*** ./src/app/@core/interfaces/iot/traffic-chart.ts ***!
  \*******************************************************/
/*! exports provided: TrafficChartData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficChartData", function() { return TrafficChartData; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var TrafficChartData = /** @class */ (function () {
    function TrafficChartData() {
    }
    return TrafficChartData;
}());



/***/ }),

/***/ "./src/app/@core/mock/common/common-mock.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/@core/mock/common/common-mock.module.ts ***!
  \*********************************************************/
/*! exports provided: CommonMockModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonMockModule", function() { return CommonMockModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _interfaces_common_users__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/common/users */ "./src/app/@core/interfaces/common/users.ts");
/* harmony import */ var _interfaces_common_smart_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../interfaces/common/smart-table */ "./src/app/@core/interfaces/common/smart-table.ts");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./users.service */ "./src/app/@core/mock/common/users.service.ts");
/* harmony import */ var _smart_table_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./smart-table.service */ "./src/app/@core/mock/common/smart-table.service.ts");
/* harmony import */ var _periods_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./periods.service */ "./src/app/@core/mock/common/periods.service.ts");
/* harmony import */ var _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../interfaces/common/settings */ "./src/app/@core/interfaces/common/settings.ts");
/* harmony import */ var _settings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./settings.service */ "./src/app/@core/mock/common/settings.service.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */










var SERVICES = [
    { provide: _interfaces_common_users__WEBPACK_IMPORTED_MODULE_3__["UserData"], useClass: _users_service__WEBPACK_IMPORTED_MODULE_5__["UsersService"] },
    { provide: _interfaces_common_smart_table__WEBPACK_IMPORTED_MODULE_4__["SmartTableData"], useClass: _smart_table_service__WEBPACK_IMPORTED_MODULE_6__["SmartTableService"] },
    { provide: _periods_service__WEBPACK_IMPORTED_MODULE_7__["PeriodsService"], useClass: _periods_service__WEBPACK_IMPORTED_MODULE_7__["PeriodsService"] },
    { provide: _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_8__["SettingsData"], useClass: _settings_service__WEBPACK_IMPORTED_MODULE_9__["SettingsService"] },
];
var CommonMockModule = /** @class */ (function () {
    function CommonMockModule() {
    }
    CommonMockModule_1 = CommonMockModule;
    CommonMockModule.forRoot = function () {
        return {
            ngModule: CommonMockModule_1,
            providers: SERVICES.slice(),
        };
    };
    var CommonMockModule_1;
    CommonMockModule = CommonMockModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
        })
    ], CommonMockModule);
    return CommonMockModule;
}());



/***/ }),

/***/ "./src/app/@core/mock/common/periods.service.ts":
/*!******************************************************!*\
  !*** ./src/app/@core/mock/common/periods.service.ts ***!
  \******************************************************/
/*! exports provided: PeriodsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PeriodsService", function() { return PeriodsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */


var PeriodsService = /** @class */ (function () {
    function PeriodsService() {
    }
    PeriodsService.prototype.getYears = function () {
        return [
            '2010', '2011', '2012',
            '2013', '2014', '2015',
            '2016', '2017', '2018',
        ];
    };
    PeriodsService.prototype.getMonths = function () {
        return [
            'Jan', 'Feb', 'Mar',
            'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep',
            'Oct', 'Nov', 'Dec',
        ];
    };
    PeriodsService.prototype.getWeeks = function () {
        return [
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
            'Sun',
        ];
    };
    PeriodsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], PeriodsService);
    return PeriodsService;
}());



/***/ }),

/***/ "./src/app/@core/mock/common/settings.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/@core/mock/common/settings.service.ts ***!
  \*******************************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/common/settings */ "./src/app/@core/interfaces/common/settings.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var SettingsService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SettingsService, _super);
    function SettingsService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SettingsService.prototype.getCurrentSetting = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])({
            themeName: 'cosmic',
        });
    };
    SettingsService.prototype.updateCurrent = function (setting) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])({
            themeName: 'corporate',
        });
    };
    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], SettingsService);
    return SettingsService;
}(_interfaces_common_settings__WEBPACK_IMPORTED_MODULE_3__["SettingsData"]));



/***/ }),

/***/ "./src/app/@core/mock/common/smart-table.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/@core/mock/common/smart-table.service.ts ***!
  \**********************************************************/
/*! exports provided: SmartTableService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableService", function() { return SmartTableService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _interfaces_common_smart_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../interfaces/common/smart-table */ "./src/app/@core/interfaces/common/smart-table.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var SmartTableService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SmartTableService, _super);
    function SmartTableService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data = [{
                id: 1,
                firstName: 'Mark',
                lastName: 'Otto',
                login: '@mdo',
                email: 'mdo@gmail.com',
                age: '28',
            }, {
                id: 2,
                firstName: 'Jacob',
                lastName: 'Thornton',
                login: '@fat',
                email: 'fat@yandex.ru',
                age: '45',
            }, {
                id: 3,
                firstName: 'Larry',
                lastName: 'Bird',
                login: '@twitter',
                email: 'twitter@outlook.com',
                age: '18',
            }, {
                id: 4,
                firstName: 'John',
                lastName: 'Snow',
                login: '@snow',
                email: 'snow@gmail.com',
                age: '20',
            }, {
                id: 5,
                firstName: 'Jack',
                lastName: 'Sparrow',
                login: '@jack',
                email: 'jack@yandex.ru',
                age: '30',
            }, {
                id: 6,
                firstName: 'Ann',
                lastName: 'Smith',
                login: '@ann',
                email: 'ann@gmail.com',
                age: '21',
            }, {
                id: 7,
                firstName: 'Barbara',
                lastName: 'Black',
                login: '@barbara',
                email: 'barbara@yandex.ru',
                age: '43',
            }, {
                id: 8,
                firstName: 'Sevan',
                lastName: 'Bagrat',
                login: '@sevan',
                email: 'sevan@outlook.com',
                age: '13',
            }, {
                id: 9,
                firstName: 'Ruben',
                lastName: 'Vardan',
                login: '@ruben',
                email: 'ruben@gmail.com',
                age: '22',
            }, {
                id: 10,
                firstName: 'Karen',
                lastName: 'Sevan',
                login: '@karen',
                email: 'karen@yandex.ru',
                age: '33',
            }, {
                id: 11,
                firstName: 'Mark',
                lastName: 'Otto',
                login: '@mark',
                email: 'mark@gmail.com',
                age: '38',
            }, {
                id: 12,
                firstName: 'Jacob',
                lastName: 'Thornton',
                login: '@jacob',
                email: 'jacob@yandex.ru',
                age: '48',
            }, {
                id: 13,
                firstName: 'Haik',
                lastName: 'Hakob',
                login: '@haik',
                email: 'haik@outlook.com',
                age: '48',
            }, {
                id: 14,
                firstName: 'Garegin',
                lastName: 'Jirair',
                login: '@garegin',
                email: 'garegin@gmail.com',
                age: '40',
            }, {
                id: 15,
                firstName: 'Krikor',
                lastName: 'Bedros',
                login: '@krikor',
                email: 'krikor@yandex.ru',
                age: '32',
            }, {
                'id': 16,
                'firstName': 'Francisca',
                'lastName': 'Brady',
                'login': '@Gibson',
                'email': 'franciscagibson@comtours.com',
                'age': 11,
            }, {
                'id': 17,
                'firstName': 'Tillman',
                'lastName': 'Figueroa',
                'login': '@Snow',
                'email': 'tillmansnow@comtours.com',
                'age': 34,
            }, {
                'id': 18,
                'firstName': 'Jimenez',
                'lastName': 'Morris',
                'login': '@Bryant',
                'email': 'jimenezbryant@comtours.com',
                'age': 45,
            }, {
                'id': 19,
                'firstName': 'Sandoval',
                'lastName': 'Jacobson',
                'login': '@Mcbride',
                'email': 'sandovalmcbride@comtours.com',
                'age': 32,
            }, {
                'id': 20,
                'firstName': 'Griffin',
                'lastName': 'Torres',
                'login': '@Charles',
                'email': 'griffincharles@comtours.com',
                'age': 19,
            }, {
                'id': 21,
                'firstName': 'Cora',
                'lastName': 'Parker',
                'login': '@Caldwell',
                'email': 'coracaldwell@comtours.com',
                'age': 27,
            }, {
                'id': 22,
                'firstName': 'Cindy',
                'lastName': 'Bond',
                'login': '@Velez',
                'email': 'cindyvelez@comtours.com',
                'age': 24,
            }, {
                'id': 23,
                'firstName': 'Frieda',
                'lastName': 'Tyson',
                'login': '@Craig',
                'email': 'friedacraig@comtours.com',
                'age': 45,
            }, {
                'id': 24,
                'firstName': 'Cote',
                'lastName': 'Holcomb',
                'login': '@Rowe',
                'email': 'coterowe@comtours.com',
                'age': 20,
            }, {
                'id': 25,
                'firstName': 'Trujillo',
                'lastName': 'Mejia',
                'login': '@Valenzuela',
                'email': 'trujillovalenzuela@comtours.com',
                'age': 16,
            }, {
                'id': 26,
                'firstName': 'Pruitt',
                'lastName': 'Shepard',
                'login': '@Sloan',
                'email': 'pruittsloan@comtours.com',
                'age': 44,
            }, {
                'id': 27,
                'firstName': 'Sutton',
                'lastName': 'Ortega',
                'login': '@Black',
                'email': 'suttonblack@comtours.com',
                'age': 42,
            }, {
                'id': 28,
                'firstName': 'Marion',
                'lastName': 'Heath',
                'login': '@Espinoza',
                'email': 'marionespinoza@comtours.com',
                'age': 47,
            }, {
                'id': 29,
                'firstName': 'Newman',
                'lastName': 'Hicks',
                'login': '@Keith',
                'email': 'newmankeith@comtours.com',
                'age': 15,
            }, {
                'id': 30,
                'firstName': 'Boyle',
                'lastName': 'Larson',
                'login': '@Summers',
                'email': 'boylesummers@comtours.com',
                'age': 32,
            }, {
                'id': 31,
                'firstName': 'Haynes',
                'lastName': 'Vinson',
                'login': '@Mckenzie',
                'email': 'haynesmckenzie@comtours.com',
                'age': 15,
            }, {
                'id': 32,
                'firstName': 'Miller',
                'lastName': 'Acosta',
                'login': '@Young',
                'email': 'milleryoung@comtours.com',
                'age': 55,
            }, {
                'id': 33,
                'firstName': 'Johnston',
                'lastName': 'Brown',
                'login': '@Knight',
                'email': 'johnstonknight@comtours.com',
                'age': 29,
            }, {
                'id': 34,
                'firstName': 'Lena',
                'lastName': 'Pitts',
                'login': '@Forbes',
                'email': 'lenaforbes@comtours.com',
                'age': 25,
            }, {
                'id': 35,
                'firstName': 'Terrie',
                'lastName': 'Kennedy',
                'login': '@Branch',
                'email': 'terriebranch@comtours.com',
                'age': 37,
            }, {
                'id': 36,
                'firstName': 'Louise',
                'lastName': 'Aguirre',
                'login': '@Kirby',
                'email': 'louisekirby@comtours.com',
                'age': 44,
            }, {
                'id': 37,
                'firstName': 'David',
                'lastName': 'Patton',
                'login': '@Sanders',
                'email': 'davidsanders@comtours.com',
                'age': 26,
            }, {
                'id': 38,
                'firstName': 'Holden',
                'lastName': 'Barlow',
                'login': '@Mckinney',
                'email': 'holdenmckinney@comtours.com',
                'age': 11,
            }, {
                'id': 39,
                'firstName': 'Baker',
                'lastName': 'Rivera',
                'login': '@Montoya',
                'email': 'bakermontoya@comtours.com',
                'age': 47,
            }, {
                'id': 40,
                'firstName': 'Belinda',
                'lastName': 'Lloyd',
                'login': '@Calderon',
                'email': 'belindacalderon@comtours.com',
                'age': 21,
            }, {
                'id': 41,
                'firstName': 'Pearson',
                'lastName': 'Patrick',
                'login': '@Clements',
                'email': 'pearsonclements@comtours.com',
                'age': 42,
            }, {
                'id': 42,
                'firstName': 'Alyce',
                'lastName': 'Mckee',
                'login': '@Daugherty',
                'email': 'alycedaugherty@comtours.com',
                'age': 55,
            }, {
                'id': 43,
                'firstName': 'Valencia',
                'lastName': 'Spence',
                'login': '@Olsen',
                'email': 'valenciaolsen@comtours.com',
                'age': 20,
            }, {
                'id': 44,
                'firstName': 'Leach',
                'lastName': 'Holcomb',
                'login': '@Humphrey',
                'email': 'leachhumphrey@comtours.com',
                'age': 28,
            }, {
                'id': 45,
                'firstName': 'Moss',
                'lastName': 'Baxter',
                'login': '@Fitzpatrick',
                'email': 'mossfitzpatrick@comtours.com',
                'age': 51,
            }, {
                'id': 46,
                'firstName': 'Jeanne',
                'lastName': 'Cooke',
                'login': '@Ward',
                'email': 'jeanneward@comtours.com',
                'age': 59,
            }, {
                'id': 47,
                'firstName': 'Wilma',
                'lastName': 'Briggs',
                'login': '@Kidd',
                'email': 'wilmakidd@comtours.com',
                'age': 53,
            }, {
                'id': 48,
                'firstName': 'Beatrice',
                'lastName': 'Perry',
                'login': '@Gilbert',
                'email': 'beatricegilbert@comtours.com',
                'age': 39,
            }, {
                'id': 49,
                'firstName': 'Whitaker',
                'lastName': 'Hyde',
                'login': '@Mcdonald',
                'email': 'whitakermcdonald@comtours.com',
                'age': 35,
            }, {
                'id': 50,
                'firstName': 'Rebekah',
                'lastName': 'Duran',
                'login': '@Gross',
                'email': 'rebekahgross@comtours.com',
                'age': 40,
            }, {
                'id': 51,
                'firstName': 'Earline',
                'lastName': 'Mayer',
                'login': '@Woodward',
                'email': 'earlinewoodward@comtours.com',
                'age': 52,
            }, {
                'id': 52,
                'firstName': 'Moran',
                'lastName': 'Baxter',
                'login': '@Johns',
                'email': 'moranjohns@comtours.com',
                'age': 20,
            }, {
                'id': 53,
                'firstName': 'Nanette',
                'lastName': 'Hubbard',
                'login': '@Cooke',
                'email': 'nanettecooke@comtours.com',
                'age': 55,
            }, {
                'id': 54,
                'firstName': 'Dalton',
                'lastName': 'Walker',
                'login': '@Hendricks',
                'email': 'daltonhendricks@comtours.com',
                'age': 25,
            }, {
                'id': 55,
                'firstName': 'Bennett',
                'lastName': 'Blake',
                'login': '@Pena',
                'email': 'bennettpena@comtours.com',
                'age': 13,
            }, {
                'id': 56,
                'firstName': 'Kellie',
                'lastName': 'Horton',
                'login': '@Weiss',
                'email': 'kellieweiss@comtours.com',
                'age': 48,
            }, {
                'id': 57,
                'firstName': 'Hobbs',
                'lastName': 'Talley',
                'login': '@Sanford',
                'email': 'hobbssanford@comtours.com',
                'age': 28,
            }, {
                'id': 58,
                'firstName': 'Mcguire',
                'lastName': 'Donaldson',
                'login': '@Roman',
                'email': 'mcguireroman@comtours.com',
                'age': 38,
            }, {
                'id': 59,
                'firstName': 'Rodriquez',
                'lastName': 'Saunders',
                'login': '@Harper',
                'email': 'rodriquezharper@comtours.com',
                'age': 20,
            }, {
                'id': 60,
                'firstName': 'Lou',
                'lastName': 'Conner',
                'login': '@Sanchez',
                'email': 'lousanchez@comtours.com',
                'age': 16,
            }];
        return _this;
    }
    SmartTableService.prototype.getData = function () {
        return this.data;
    };
    SmartTableService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], SmartTableService);
    return SmartTableService;
}(_interfaces_common_smart_table__WEBPACK_IMPORTED_MODULE_2__["SmartTableData"]));



/***/ }),

/***/ "./src/app/@core/mock/common/users.service.ts":
/*!****************************************************!*\
  !*** ./src/app/@core/mock/common/users.service.ts ***!
  \****************************************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var UsersService = /** @class */ (function () {
    function UsersService() {
        this.data = [
            {
                id: 1,
                role: 'user',
                firstName: 'Mark',
                lastName: 'Walmart',
                login: '@mdo',
                email: 'mdo@gmail.com',
                age: 0,
                picture: '',
                address: {
                    street: 'Wall St.',
                    city: 'New York',
                    zipCode: '10005',
                },
                settings: {
                    themeName: 'cosmic',
                },
            },
            {
                id: 2,
                role: 'user',
                firstName: 'Jacob',
                lastName: 'Cuba',
                login: '@mdo',
                email: 'mdo@gmail.com',
                age: 0,
                picture: '',
                address: {
                    street: 'Wall St.',
                    city: 'New York',
                    zipCode: '10005',
                },
                settings: {
                    themeName: 'cosmic',
                },
            },
            {
                id: 3,
                role: 'user',
                firstName: 'Larry',
                lastName: 'Page',
                login: '@twitter',
                email: 'twitter@outlook.com',
                age: 0,
                picture: '',
                address: {
                    street: 'Wall St.',
                    city: 'New York',
                    zipCode: '10005',
                },
                settings: {
                    themeName: 'cosmic',
                },
            },
            {
                id: 4,
                role: 'user',
                firstName: 'John',
                lastName: 'Snow',
                login: '@snow',
                email: 'snow@gmail.com',
                age: 0,
                picture: '',
                address: {
                    street: 'Wall St.',
                    city: 'New York',
                    zipCode: '10005',
                },
                settings: {
                    themeName: 'cosmic',
                },
            }
        ];
    }
    Object.defineProperty(UsersService.prototype, "gridDataSource", {
        get: function () {
            return new ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"](this.data);
        },
        enumerable: true,
        configurable: true
    });
    UsersService.prototype.getCurrentUser = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.data[0]);
    };
    UsersService.prototype.list = function (pageNumber, pageSize) {
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.data);
    };
    UsersService.prototype.get = function (id) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.data.find(function (x) { return x.id === id; }));
    };
    UsersService.prototype.updateCurrent = function (user) {
        this.data[0] = user;
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.data[0]);
    };
    UsersService.prototype.update = function (user) {
        var i = this.data.indexOf(this.data.find(function (x) { return x.id === user.id; }));
        if (i >= 0) {
            this.data[i] = user;
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(user);
    };
    UsersService.prototype.create = function (user) {
        user.id = Math.max.apply(Math, this.data.map(function (x) { return x.id; })) + 1;
        this.data.push(user);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(user);
    };
    UsersService.prototype.delete = function (id) {
        this.data = this.data.filter(function (x) { return x.id !== id; }).slice();
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])();
    };
    UsersService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/app/@core/mock/iot/devices.service.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/mock/iot/devices.service.ts ***!
  \***************************************************/
/*! exports provided: DevicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicesService", function() { return DevicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_devices__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/devices */ "./src/app/@core/interfaces/iot/devices.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var DevicesService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](DevicesService, _super);
    function DevicesService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data = [
            {
                id: 1,
                name: 'Light',
                isOn: true,
                type: 'Light',
                settings: {
                    iconClass: 'nb-lightbulb',
                    type: 'primary',
                },
            }, {
                id: 2,
                name: 'Roller Shades',
                isOn: true,
                type: 'RollerShades',
                settings: {
                    iconClass: 'nb-roller-shades',
                    type: 'success',
                },
            }, {
                id: 3,
                name: 'Wireless Audio',
                isOn: true,
                type: 'WirelessAudio',
                settings: {
                    iconClass: 'nb-audio',
                    type: 'info',
                },
            }, {
                id: 4,
                name: 'Coffee Maker',
                isOn: true,
                type: 'CoffeeMaker',
                settings: {
                    iconClass: 'nb-coffee-maker',
                    type: 'warning',
                },
            },
        ];
        return _this;
    }
    DevicesService.prototype.list = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.data);
    };
    DevicesService.prototype.edit = function (device) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(device);
    };
    DevicesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], DevicesService);
    return DevicesService;
}(_interfaces_iot_devices__WEBPACK_IMPORTED_MODULE_3__["DevicesData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/electricity.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/@core/mock/iot/electricity.service.ts ***!
  \*******************************************************/
/*! exports provided: ElectricityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ElectricityService", function() { return ElectricityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_electricity__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/electricity */ "./src/app/@core/interfaces/iot/electricity.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var ElectricityService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ElectricityService, _super);
    function ElectricityService() {
        var _this = _super.call(this) || this;
        _this.listData = [
            {
                title: '2015',
                months: [
                    { month: 'Jan', delta: '0.97', down: true, kWatts: '816', cost: '97' },
                    { month: 'Feb', delta: '1.83', down: true, kWatts: '806', cost: '95' },
                    { month: 'Mar', delta: '0.64', down: true, kWatts: '803', cost: '94' },
                    { month: 'Apr', delta: '2.17', down: false, kWatts: '818', cost: '98' },
                    { month: 'May', delta: '1.32', down: true, kWatts: '809', cost: '96' },
                    { month: 'Jun', delta: '0.05', down: true, kWatts: '808', cost: '96' },
                    { month: 'Jul', delta: '1.39', down: false, kWatts: '815', cost: '97' },
                    { month: 'Aug', delta: '0.73', down: true, kWatts: '807', cost: '95' },
                    { month: 'Sept', delta: '2.61', down: true, kWatts: '792', cost: '92' },
                    { month: 'Oct', delta: '0.16', down: true, kWatts: '791', cost: '92' },
                    { month: 'Nov', delta: '1.71', down: true, kWatts: '786', cost: '89' },
                    { month: 'Dec', delta: '0.37', down: false, kWatts: '789', cost: '91' },
                ],
            },
            {
                title: '2016',
                active: true,
                months: [
                    { month: 'Jan', delta: '1.56', down: true, kWatts: '789', cost: '91' },
                    { month: 'Feb', delta: '0.33', down: false, kWatts: '791', cost: '92' },
                    { month: 'Mar', delta: '0.62', down: true, kWatts: '790', cost: '92' },
                    { month: 'Apr', delta: '1.93', down: true, kWatts: '783', cost: '87' },
                    { month: 'May', delta: '2.52', down: true, kWatts: '771', cost: '83' },
                    { month: 'Jun', delta: '0.39', down: false, kWatts: '774', cost: '85' },
                    { month: 'Jul', delta: '1.61', down: true, kWatts: '767', cost: '81' },
                    { month: 'Aug', delta: '1.41', down: true, kWatts: '759', cost: '76' },
                    { month: 'Sept', delta: '1.03', down: true, kWatts: '752', cost: '74' },
                    { month: 'Oct', delta: '2.94', down: false, kWatts: '769', cost: '82' },
                    { month: 'Nov', delta: '0.26', down: true, kWatts: '767', cost: '81' },
                    { month: 'Dec', delta: '1.62', down: true, kWatts: '760', cost: '76' },
                ],
            },
            {
                title: '2017',
                months: [
                    { month: 'Jan', delta: '1.34', down: false, kWatts: '789', cost: '91' },
                    { month: 'Feb', delta: '0.95', down: false, kWatts: '793', cost: '93' },
                    { month: 'Mar', delta: '0.25', down: true, kWatts: '791', cost: '92' },
                    { month: 'Apr', delta: '1.72', down: false, kWatts: '797', cost: '95' },
                    { month: 'May', delta: '2.62', down: true, kWatts: '786', cost: '90' },
                    { month: 'Jun', delta: '0.72', down: false, kWatts: '789', cost: '91' },
                    { month: 'Jul', delta: '0.78', down: true, kWatts: '784', cost: '89' },
                    { month: 'Aug', delta: '0.36', down: true, kWatts: '782', cost: '88' },
                    { month: 'Sept', delta: '0.55', down: false, kWatts: '787', cost: '90' },
                    { month: 'Oct', delta: '1.81', down: true, kWatts: '779', cost: '86' },
                    { month: 'Nov', delta: '1.12', down: true, kWatts: '774', cost: '84' },
                    { month: 'Dec', delta: '0.52', down: false, kWatts: '776', cost: '95' },
                ],
            },
        ];
        _this.chartData = {
            chart: {
                axisXLabels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'],
                linesData: [[490, 505, 550, 800, 870, 720, 130, 635, 660, 380, 340]],
                chartLabel: undefined,
                legend: [],
            },
            consumed: {
                title: 'kWh',
                value: 816,
            },
            spent: {
                title: 'USD',
                value: 291,
            },
        };
        return _this;
    }
    ElectricityService.prototype.getListData = function (yearsCount) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.listData);
    };
    ElectricityService.prototype.getChartData = function (period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.chartData);
    };
    ElectricityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ElectricityService);
    return ElectricityService;
}(_interfaces_iot_electricity__WEBPACK_IMPORTED_MODULE_3__["ElectricityData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/iot-mock.module.ts":
/*!***************************************************!*\
  !*** ./src/app/@core/mock/iot/iot-mock.module.ts ***!
  \***************************************************/
/*! exports provided: IotMockModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IotMockModule", function() { return IotMockModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _common_common_mock_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/common-mock.module */ "./src/app/@core/mock/common/common-mock.module.ts");
/* harmony import */ var _interfaces_iot_devices__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../interfaces/iot/devices */ "./src/app/@core/interfaces/iot/devices.ts");
/* harmony import */ var _interfaces_iot_phone__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../interfaces/iot/phone */ "./src/app/@core/interfaces/iot/phone.ts");
/* harmony import */ var _interfaces_iot_electricity__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../interfaces/iot/electricity */ "./src/app/@core/interfaces/iot/electricity.ts");
/* harmony import */ var _interfaces_iot_solar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interfaces/iot/solar */ "./src/app/@core/interfaces/iot/solar.ts");
/* harmony import */ var _interfaces_iot_temperature_humidity__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../interfaces/iot/temperature-humidity */ "./src/app/@core/interfaces/iot/temperature-humidity.ts");
/* harmony import */ var _interfaces_iot_traffic_chart__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../interfaces/iot/traffic-chart */ "./src/app/@core/interfaces/iot/traffic-chart.ts");
/* harmony import */ var _interfaces_iot_security_cameras__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../interfaces/iot/security-cameras */ "./src/app/@core/interfaces/iot/security-cameras.ts");
/* harmony import */ var _devices_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./devices.service */ "./src/app/@core/mock/iot/devices.service.ts");
/* harmony import */ var _electricity_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./electricity.service */ "./src/app/@core/mock/iot/electricity.service.ts");
/* harmony import */ var _phone_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./phone.service */ "./src/app/@core/mock/iot/phone.service.ts");
/* harmony import */ var _solar_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./solar.service */ "./src/app/@core/mock/iot/solar.service.ts");
/* harmony import */ var _temperature_humidity_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./temperature-humidity.service */ "./src/app/@core/mock/iot/temperature-humidity.service.ts");
/* harmony import */ var _traffic_chart_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./traffic-chart.service */ "./src/app/@core/mock/iot/traffic-chart.service.ts");
/* harmony import */ var _security_cameras_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./security-cameras.service */ "./src/app/@core/mock/iot/security-cameras.service.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */


















var SERVICES = [
    { provide: _interfaces_iot_devices__WEBPACK_IMPORTED_MODULE_4__["DevicesData"], useClass: _devices_service__WEBPACK_IMPORTED_MODULE_11__["DevicesService"] },
    { provide: _interfaces_iot_electricity__WEBPACK_IMPORTED_MODULE_6__["ElectricityData"], useClass: _electricity_service__WEBPACK_IMPORTED_MODULE_12__["ElectricityService"] },
    { provide: _interfaces_iot_phone__WEBPACK_IMPORTED_MODULE_5__["PhoneData"], useClass: _phone_service__WEBPACK_IMPORTED_MODULE_13__["PhoneService"] },
    { provide: _interfaces_iot_solar__WEBPACK_IMPORTED_MODULE_7__["SolarData"], useClass: _solar_service__WEBPACK_IMPORTED_MODULE_14__["SolarService"] },
    { provide: _interfaces_iot_temperature_humidity__WEBPACK_IMPORTED_MODULE_8__["TemperatureHumidityData"], useClass: _temperature_humidity_service__WEBPACK_IMPORTED_MODULE_15__["TemperatureHumidityService"] },
    { provide: _interfaces_iot_traffic_chart__WEBPACK_IMPORTED_MODULE_9__["TrafficChartData"], useClass: _traffic_chart_service__WEBPACK_IMPORTED_MODULE_16__["TrafficChartService"] },
    { provide: _interfaces_iot_security_cameras__WEBPACK_IMPORTED_MODULE_10__["SecurityCamerasData"], useClass: _security_cameras_service__WEBPACK_IMPORTED_MODULE_17__["SecurityCamerasService"] },
];
var IotMockModule = /** @class */ (function () {
    function IotMockModule() {
    }
    IotMockModule_1 = IotMockModule;
    IotMockModule.forRoot = function () {
        return {
            ngModule: IotMockModule_1,
            providers: SERVICES.slice(),
        };
    };
    var IotMockModule_1;
    IotMockModule = IotMockModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _common_common_mock_module__WEBPACK_IMPORTED_MODULE_3__["CommonMockModule"]],
            exports: [_common_common_mock_module__WEBPACK_IMPORTED_MODULE_3__["CommonMockModule"]],
        })
    ], IotMockModule);
    return IotMockModule;
}());



/***/ }),

/***/ "./src/app/@core/mock/iot/phone.service.ts":
/*!*************************************************!*\
  !*** ./src/app/@core/mock/iot/phone.service.ts ***!
  \*************************************************/
/*! exports provided: PhoneService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhoneService", function() { return PhoneService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _interfaces_iot_phone__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/phone */ "./src/app/@core/interfaces/iot/phone.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var PhoneService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PhoneService, _super);
    function PhoneService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.time = new Date;
        _this.types = {
            mobile: 'mobile',
            home: 'home',
            work: 'work',
        };
        _this.contacts = [
            { name: 'Nick Jones', picture: 'assets/images/nick.png', type: _this.types.mobile },
            { name: 'Eva Moor', picture: 'assets/images/eva.png', type: _this.types.home },
            { name: 'Jack Williams', picture: 'assets/images/jack.png', type: _this.types.mobile },
            { name: 'Lee Wong', picture: 'assets/images/lee.png', type: _this.types.mobile },
            { name: 'Alan Thompson', picture: 'assets/images/alan.png', type: _this.types.home },
            { name: 'Kate Martinez', picture: 'assets/images/kate.png', type: _this.types.work },
        ];
        _this.recentUsers = [
            {
                name: _this.contacts[0].name,
                picture: _this.contacts[0].picture,
                type: _this.contacts[0].type,
                time: _this.time.setHours(21, 12),
            },
            {
                name: _this.contacts[2].name,
                picture: _this.contacts[2].picture,
                type: _this.contacts[2].type,
                time: _this.time.setHours(20, 13),
            },
            {
                name: _this.contacts[1].name,
                picture: _this.contacts[1].picture,
                type: _this.contacts[1].type,
                time: _this.time.setHours(18, 10),
            },
            {
                name: _this.contacts[0].name,
                picture: _this.contacts[0].picture,
                type: _this.contacts[0].type,
                time: _this.time.setHours(15, 45),
            },
            {
                name: _this.contacts[4].name,
                picture: _this.contacts[4].picture,
                type: _this.contacts[4].type,
                time: _this.time.setHours(14, 33),
            },
            {
                name: _this.contacts[4].name,
                picture: _this.contacts[4].picture,
                type: _this.contacts[4].type,
                time: _this.time.setHours(14, 29),
            },
        ];
        return _this;
    }
    PhoneService.prototype.getContacts = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.contacts);
    };
    PhoneService.prototype.getRecentUsers = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(this.recentUsers);
    };
    PhoneService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
    ], PhoneService);
    return PhoneService;
}(_interfaces_iot_phone__WEBPACK_IMPORTED_MODULE_3__["PhoneData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/security-cameras.service.ts":
/*!************************************************************!*\
  !*** ./src/app/@core/mock/iot/security-cameras.service.ts ***!
  \************************************************************/
/*! exports provided: SecurityCamerasService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityCamerasService", function() { return SecurityCamerasService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_security_cameras__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/security-cameras */ "./src/app/@core/interfaces/iot/security-cameras.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var SecurityCamerasService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SecurityCamerasService, _super);
    function SecurityCamerasService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.cameras = [
            {
                title: 'Camera #1',
                source: 'assets/images/camera1.jpg',
            },
            {
                title: 'Camera #2',
                source: 'assets/images/camera2.jpg',
            },
            {
                title: 'Camera #3',
                source: 'assets/images/camera3.jpg',
            },
            {
                title: 'Camera #4',
                source: 'assets/images/camera4.jpg',
            },
        ];
        return _this;
    }
    SecurityCamerasService.prototype.getCamerasData = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.cameras);
    };
    SecurityCamerasService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], SecurityCamerasService);
    return SecurityCamerasService;
}(_interfaces_iot_security_cameras__WEBPACK_IMPORTED_MODULE_3__["SecurityCamerasData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/solar.service.ts":
/*!*************************************************!*\
  !*** ./src/app/@core/mock/iot/solar.service.ts ***!
  \*************************************************/
/*! exports provided: SolarService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SolarService", function() { return SolarService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_solar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/solar */ "./src/app/@core/interfaces/iot/solar.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var SolarService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SolarService, _super);
    function SolarService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.value = {
            totalValue: 8421,
            solarValue: 6421,
            percent: 42,
            unitOfMeasure: 'kWh',
        };
        return _this;
    }
    SolarService.prototype.getSolarData = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.value);
    };
    SolarService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], SolarService);
    return SolarService;
}(_interfaces_iot_solar__WEBPACK_IMPORTED_MODULE_3__["SolarData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/temperature-humidity.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/@core/mock/iot/temperature-humidity.service.ts ***!
  \****************************************************************/
/*! exports provided: TemperatureHumidityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TemperatureHumidityService", function() { return TemperatureHumidityService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_temperature_humidity__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/temperature-humidity */ "./src/app/@core/interfaces/iot/temperature-humidity.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var TemperatureHumidityService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TemperatureHumidityService, _super);
    function TemperatureHumidityService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.temperatureDevice = {
            id: 5,
            name: 'Air Conditioner',
            isOn: true,
            type: 'Conditioner',
            parameters: [
                {
                    id: 1,
                    name: 'Temperature',
                    value: 24,
                    min: 12,
                    max: 30,
                },
                {
                    id: 2,
                    name: 'Mode',
                    value: 'Cooling',
                },
            ],
        };
        _this.humidityDevice = {
            id: 6,
            name: 'Humidifier',
            isOn: true,
            type: 'Humidifier',
            parameters: [
                {
                    id: 2,
                    name: 'Humidity',
                    value: 87,
                    min: 0,
                    max: 100,
                },
                {
                    id: 4,
                    name: 'Mode',
                    value: 'Circulation',
                },
            ],
        };
        return _this;
    }
    TemperatureHumidityService.prototype.getTemperatureDevice = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.temperatureDevice);
    };
    TemperatureHumidityService.prototype.getHumidityDevice = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.humidityDevice);
    };
    TemperatureHumidityService.prototype.setTemperatureDevice = function (device) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(device);
    };
    TemperatureHumidityService.prototype.setHumidityDevice = function (device) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(device);
    };
    TemperatureHumidityService.prototype.setTemperatureParameter = function (parameter) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(parameter);
    };
    TemperatureHumidityService.prototype.setHumidityParameter = function (parameter) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(parameter);
    };
    TemperatureHumidityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], TemperatureHumidityService);
    return TemperatureHumidityService;
}(_interfaces_iot_temperature_humidity__WEBPACK_IMPORTED_MODULE_3__["TemperatureHumidityData"]));



/***/ }),

/***/ "./src/app/@core/mock/iot/traffic-chart.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/@core/mock/iot/traffic-chart.service.ts ***!
  \*********************************************************/
/*! exports provided: TrafficChartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrafficChartService", function() { return TrafficChartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _interfaces_iot_traffic_chart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../interfaces/iot/traffic-chart */ "./src/app/@core/interfaces/iot/traffic-chart.ts");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var TrafficChartService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](TrafficChartService, _super);
    function TrafficChartService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.data = [
            300, 520, 435, 530,
            730, 620, 660, 860,
        ];
        return _this;
    }
    TrafficChartService.prototype.getTrafficChartData = function (period) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.data);
    };
    TrafficChartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], TrafficChartService);
    return TrafficChartService;
}(_interfaces_iot_traffic_chart__WEBPACK_IMPORTED_MODULE_3__["TrafficChartData"]));



/***/ }),

/***/ "./src/app/@core/module-import-guard.ts":
/*!**********************************************!*\
  !*** ./src/app/@core/module-import-guard.ts ***!
  \**********************************************/
/*! exports provided: throwIfAlreadyLoaded */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throwIfAlreadyLoaded", function() { return throwIfAlreadyLoaded; });
function throwIfAlreadyLoaded(parentModule, moduleName) {
    if (parentModule) {
        throw new Error(moduleName + " has already been loaded. Import Core modules in the AppModule only.");
    }
}


/***/ }),

/***/ "./src/app/@core/stores/user.store.ts":
/*!********************************************!*\
  !*** ./src/app/@core/stores/user.store.ts ***!
  \********************************************/
/*! exports provided: UserStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserStore", function() { return UserStore; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../interfaces/common/settings */ "./src/app/@core/interfaces/common/settings.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var UserStore = /** @class */ (function () {
    function UserStore(settingsService) {
        this.settingsService = settingsService;
    }
    UserStore.prototype.getUser = function () {
        return this.user;
    };
    UserStore.prototype.setUser = function (paramUser) {
        this.user = paramUser;
    };
    UserStore.prototype.setSetting = function (themeName) {
        if (this.user) {
            if (this.user.settings) {
                this.user.settings.themeName = themeName;
            }
            else {
                this.user.settings = { themeName: themeName };
            }
            return this.settingsService.updateCurrent(this.user.settings);
        }
        else {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])({ themeName: themeName });
        }
    };
    UserStore.ctorParameters = function () { return [
        { type: _interfaces_common_settings__WEBPACK_IMPORTED_MODULE_2__["SettingsData"] }
    ]; };
    UserStore = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_interfaces_common_settings__WEBPACK_IMPORTED_MODULE_2__["SettingsData"]])
    ], UserStore);
    return UserStore;
}());



/***/ }),

/***/ "./src/app/@core/utils/analytics.service.ts":
/*!**************************************************!*\
  !*** ./src/app/@core/utils/analytics.service.ts ***!
  \**************************************************/
/*! exports provided: AnalyticsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticsService", function() { return AnalyticsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var AnalyticsService = /** @class */ (function () {
    function AnalyticsService(location, router) {
        this.location = location;
        this.router = router;
        this.enabled = false;
    }
    AnalyticsService.prototype.trackPageViews = function () {
        var _this = this;
        if (this.enabled) {
            this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; }))
                .subscribe(function () {
                ga('send', { hitType: 'pageview', page: _this.location.path() });
            });
        }
    };
    AnalyticsService.prototype.trackEvent = function (eventName) {
        if (this.enabled) {
            ga('send', 'event', eventName);
        }
    };
    AnalyticsService.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AnalyticsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AnalyticsService);
    return AnalyticsService;
}());



/***/ }),

/***/ "./src/app/@core/utils/index.ts":
/*!**************************************!*\
  !*** ./src/app/@core/utils/index.ts ***!
  \**************************************/
/*! exports provided: LayoutService, AnalyticsService, PlayerService, StateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layout_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout.service */ "./src/app/@core/utils/layout.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return _layout_service__WEBPACK_IMPORTED_MODULE_0__["LayoutService"]; });

/* harmony import */ var _analytics_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./analytics.service */ "./src/app/@core/utils/analytics.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AnalyticsService", function() { return _analytics_service__WEBPACK_IMPORTED_MODULE_1__["AnalyticsService"]; });

/* harmony import */ var _player_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./player.service */ "./src/app/@core/utils/player.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlayerService", function() { return _player_service__WEBPACK_IMPORTED_MODULE_2__["PlayerService"]; });

/* harmony import */ var _state_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./state.service */ "./src/app/@core/utils/state.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StateService", function() { return _state_service__WEBPACK_IMPORTED_MODULE_3__["StateService"]; });

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */







/***/ }),

/***/ "./src/app/@core/utils/layout.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/utils/layout.service.ts ***!
  \***********************************************/
/*! exports provided: LayoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return LayoutService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */




var LayoutService = /** @class */ (function () {
    function LayoutService() {
        this.layoutSize$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    LayoutService.prototype.changeLayoutSize = function () {
        this.layoutSize$.next();
    };
    LayoutService.prototype.onChangeLayoutSize = function () {
        return this.layoutSize$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["share"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(1));
    };
    LayoutService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], LayoutService);
    return LayoutService;
}());



/***/ }),

/***/ "./src/app/@core/utils/player.service.ts":
/*!***********************************************!*\
  !*** ./src/app/@core/utils/player.service.ts ***!
  \***********************************************/
/*! exports provided: Track, PlayerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Track", function() { return Track; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerService", function() { return PlayerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */


var Track = /** @class */ (function () {
    function Track() {
    }
    return Track;
}());

var PlayerService = /** @class */ (function () {
    function PlayerService() {
        this.playlist = [
            {
                name: 'Don\'t Wanna Fight',
                artist: 'Alabama Shakes',
                url: 'https://p.scdn.co/mp3-preview/6156cdbca425a894972c02fca9d76c0b70e001af',
                cover: 'assets/images/cover1.jpg',
            },
            {
                name: 'Harder',
                artist: 'Daft Punk',
                url: 'https://p.scdn.co/mp3-preview/92a04c7c0e96bf93a1b1b1cae7dfff1921969a7b',
                cover: 'assets/images/cover2.jpg',
            },
            {
                name: 'Come Together',
                artist: 'Beatles',
                url: 'https://p.scdn.co/mp3-preview/83090a4db6899eaca689ae35f69126dbe65d94c9',
                cover: 'assets/images/cover3.jpg',
            },
        ];
    }
    PlayerService.prototype.random = function () {
        this.current = Math.floor(Math.random() * this.playlist.length);
        return this.playlist[this.current];
    };
    PlayerService.prototype.next = function () {
        return this.getNextTrack();
    };
    PlayerService.prototype.prev = function () {
        return this.getPrevTrack();
    };
    PlayerService.prototype.getNextTrack = function () {
        if (this.current === this.playlist.length - 1) {
            this.current = 0;
        }
        else {
            this.current++;
        }
        return this.playlist[this.current];
    };
    PlayerService.prototype.getPrevTrack = function () {
        if (this.current === 0) {
            this.current = this.playlist.length - 1;
        }
        else {
            this.current--;
        }
        return this.playlist[this.current];
    };
    PlayerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], PlayerService);
    return PlayerService;
}());



/***/ }),

/***/ "./src/app/@core/utils/state.service.ts":
/*!**********************************************!*\
  !*** ./src/app/@core/utils/state.service.ts ***!
  \**********************************************/
/*! exports provided: StateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateService", function() { return StateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm5/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var StateService = /** @class */ (function () {
    function StateService(directionService) {
        var _this = this;
        this.layouts = [
            {
                name: 'One Column',
                icon: 'nb-layout-default',
                id: 'one-column',
                selected: true,
            },
            {
                name: 'Two Column',
                icon: 'nb-layout-two-column',
                id: 'two-column',
            },
            {
                name: 'Center Column',
                icon: 'nb-layout-centre',
                id: 'center-column',
            },
        ];
        this.sidebars = [
            {
                name: 'Sidebar at layout start',
                icon: 'nb-layout-sidebar-left',
                id: 'start',
                selected: true,
            },
            {
                name: 'Sidebar at layout end',
                icon: 'nb-layout-sidebar-right',
                id: 'end',
            },
        ];
        this.layoutState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.layouts[0]);
        this.sidebarState$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.sidebars[0]);
        this.alive = true;
        directionService.onDirectionChange()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeWhile"])(function () { return _this.alive; }))
            .subscribe(function (direction) { return _this.updateSidebarIcons(direction); });
        this.updateSidebarIcons(directionService.getDirection());
    }
    StateService.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    StateService.prototype.updateSidebarIcons = function (direction) {
        var _a = this.sidebars, startSidebar = _a[0], endSidebar = _a[1];
        var isLtr = direction === _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbLayoutDirection"].LTR;
        var startIconClass = isLtr ? 'nb-layout-sidebar-left' : 'nb-layout-sidebar-right';
        var endIconClass = isLtr ? 'nb-layout-sidebar-right' : 'nb-layout-sidebar-left';
        startSidebar.icon = startIconClass;
        endSidebar.icon = endIconClass;
    };
    StateService.prototype.setLayoutState = function (state) {
        this.layoutState$.next(state);
    };
    StateService.prototype.getLayoutStates = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.layouts);
    };
    StateService.prototype.onLayoutState = function () {
        return this.layoutState$.asObservable();
    };
    StateService.prototype.setSidebarState = function (state) {
        this.sidebarState$.next(state);
    };
    StateService.prototype.getSidebarStates = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.sidebars);
    };
    StateService.prototype.onSidebarState = function () {
        return this.sidebarState$.asObservable();
    };
    StateService.ctorParameters = function () { return [
        { type: _nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbLayoutDirectionService"] }
    ]; };
    StateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_nebular_theme__WEBPACK_IMPORTED_MODULE_4__["NbLayoutDirectionService"]])
    ], StateService);
    return StateService;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");



var routes = [
    {
        path: '',
        loadChildren: function () { return __webpack_require__.e(/*! import() | auth-auth-module */ "auth-auth-module").then(__webpack_require__.bind(null, /*! ./@auth/auth.module */ "./src/app/@auth/auth.module.ts"))
            .then(function (m) { return m.AuthModule; }); },
    },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', redirectTo: '' },
];
var config = {
    useHash: false,
};
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, config)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-header {\n  flex: 1;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXFVzZXJzXFxERUxMXFxEZXNrdG9wXFxmaXhteWtpeC9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxPQUFBO0VBQ0EsZ0JBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImFwcC1oZWFkZXIge1xyXG4gIGZsZXg6IDE7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4iLCJhcHAtaGVhZGVyIHtcbiAgZmxleDogMTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");





var AppComponent = /** @class */ (function () {
    function AppComponent(router, utilsService) {
        var _this = this;
        this.router = router;
        this.utilsService = utilsService;
        this.loading = true;
        this.defaultLayout = {
            header: true,
            headerFixed: false,
            sidebar: true,
            sidebarFixed: false,
            paddings: {
                paddingTop: 16,
                paddingRight: 16,
                paddingBottom: 16,
                paddingLeft: 16
            }
        };
        this.layout$ = this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function () {
            var route = _this.router.routerState.root;
            while (route.firstChild) {
                route = route.firstChild;
            }
            return route.snapshot.data['layout'] || _this.defaultLayout;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])());
        this.padding$ = this.layout$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (layout) {
            var p = layout.paddings;
            return p.paddingTop + "px " + p.paddingRight + "px " + p.paddingBottom + "px " + p.paddingLeft + "px";
        }));
        setTimeout(function () { return _this.loading = false; }, 1000);
    }
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_4__["UtilService"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].Default,
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_4__["UtilService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./@core/core.module */ "./src/app/@core/core.module.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./@auth/auth.guard */ "./src/app/@auth/auth.guard.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _auth_isAuth_guard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./@auth/isAuth.guard */ "./src/app/@auth/isAuth.guard.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/fesm5/ngx-socket-io.js");

















var config = { url: 'http://localhost:8000', options: {} };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"],
            ],
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["NoopAnimationsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_12__["SharedModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"].forRoot(),
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__["FlexLayoutModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrModule"].forRoot(),
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
                ngx_socket_io__WEBPACK_IMPORTED_MODULE_15__["SocketIoModule"].forRoot(config),
            ],
            exports: [
                ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrModule"]
            ],
            providers: [
                _auth_auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"],
                _auth_isAuth_guard__WEBPACK_IMPORTED_MODULE_13__["IsAuthGuard"],
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_10__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nebular_theme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @nebular/theme */ "./node_modules/@nebular/theme/fesm5/index.js");
/* harmony import */ var _smart_table_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./smart-table.component */ "./src/app/shared/smart-table.component.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _uibakery_kit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @uibakery/kit */ "./node_modules/@uibakery/kit/fesm5/uibakery-kit.js");







var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_smart_table_component__WEBPACK_IMPORTED_MODULE_4__["SmartTableComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbCardModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbAccordionModule"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbSelectModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbButtonModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbIconModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbCheckboxModule"],
                _uibakery_kit__WEBPACK_IMPORTED_MODULE_6__["BkMapModule"],
            ],
            exports: [_nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbCardModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbAccordionModule"], _smart_table_component__WEBPACK_IMPORTED_MODULE_4__["SmartTableComponent"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_5__["Ng2SmartTableModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbSelectModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbButtonModule"],
                _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbIconModule"], _nebular_theme__WEBPACK_IMPORTED_MODULE_3__["NbCheckboxModule"], _uibakery_kit__WEBPACK_IMPORTED_MODULE_6__["BkMapModule"]],
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/shared/smart-table.component.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/smart-table.component.ts ***!
  \*************************************************/
/*! exports provided: SmartTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmartTableComponent", function() { return SmartTableComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SmartTableComponent = /** @class */ (function () {
    function SmartTableComponent(cd) {
        this.cd = cd;
    }
    SmartTableComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        /**
        * There is an issue with change detection in ng2-smart-table's.
        * We need to trigger it manually for the correct first time render.
        */
        Promise.resolve().then(function () { return _this.cd.detectChanges(); });
    };
    SmartTableComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SmartTableComponent.prototype, "settings", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SmartTableComponent.prototype, "source", void 0);
    SmartTableComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-smart-table',
            template: "\n    <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\n  ",
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], SmartTableComponent);
    return SmartTableComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    apiUrl: 'localhost:8000',
    testUser: {
        // tslint:disable
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiQFVzZXIiLCJyb2xlIjoidXNlciIsIm5iZiI6MTU2NDA2MTQ1NywiZXhwIjoxNTk1NjgzODU3LCJpc3MiOiJpc3N1ZXJfc2FtcGxlIiwiYXVkIjoiYXVkaWVuY2Vfc2FtcGxlIn0.xAAbQIOsw3ZXlIxDFnv5NynZy7OfzrvrJYWsy2NEBbA',
        // tslint:enable
        email: 'user@user.user',
    },
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\DELL\Desktop\fixmykix\src\main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map