(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"],{

/***/ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js ***!
  \*******************************************************************************/
/*! exports provided: MultiSelectComponent, NgMultiSelectDropDownModule, ɵa, ɵb, ɵc */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MultiSelectComponent", function() { return MultiSelectComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgMultiSelectDropDownModule", function() { return NgMultiSelectDropDownModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return DROPDOWN_CONTROL_VALUE_ACCESSOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return ListFilterPipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return ClickOutsideDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");





var ListItem = /** @class */ (function () {
    function ListItem(source) {
        if (typeof source === 'string' || typeof source === 'number') {
            this.id = this.text = source;
            this.isDisabled = false;
        }
        if (typeof source === 'object') {
            this.id = source.id;
            this.text = source.text;
            this.isDisabled = source.isDisabled;
        }
    }
    return ListItem;
}());

var ListFilterPipe = /** @class */ (function () {
    function ListFilterPipe() {
    }
    ListFilterPipe.prototype.transform = function (items, filter) {
        var _this = this;
        if (!items || !filter) {
            return items;
        }
        return items.filter(function (item) { return _this.applyFilter(item, filter); });
    };
    ListFilterPipe.prototype.applyFilter = function (item, filter) {
        if (typeof item.text === 'string' && typeof filter.text === 'string') {
            return !(filter.text && item.text && item.text.toLowerCase().indexOf(filter.text.toLowerCase()) === -1);
        }
        else {
            return !(filter.text && item.text && item.text.toString().toLowerCase().indexOf(filter.text.toString().toLowerCase()) === -1);
        }
    };
    ListFilterPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'multiSelectFilter',
            pure: false
        })
    ], ListFilterPipe);
    return ListFilterPipe;
}());

var DROPDOWN_CONTROL_VALUE_ACCESSOR = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return MultiSelectComponent; }),
    multi: true
};
var noop = function () { };
var ɵ0 = noop;
var MultiSelectComponent = /** @class */ (function () {
    function MultiSelectComponent(listFilterPipe) {
        this.listFilterPipe = listFilterPipe;
        this._data = [];
        this.selectedItems = [];
        this.isDropdownOpen = true;
        this._placeholder = "Select";
        this._sourceDataType = null; // to keep note of the source data type. could be array of string/number/object
        this._sourceDataFields = []; // store source data fields names
        this.filter = new ListItem(this.data);
        this.defaultSettings = {
            singleSelection: false,
            idField: "id",
            textField: "text",
            disabledField: "isDisabled",
            enableCheckAll: true,
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            allowSearchFilter: false,
            limitSelection: -1,
            clearSearchFilter: true,
            maxHeight: 197,
            itemsShowLimit: 999999999999,
            searchPlaceholderText: "Search",
            noDataAvailablePlaceholderText: "No data available",
            closeDropDownOnSelection: false,
            showSelectedItemsAtTop: false,
            defaultOpen: false,
            allowRemoteDataSearch: false
        };
        this.disabled = false;
        this.onFilterChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onDropDownClose = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onDeSelect = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onSelectAll = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onDeSelectAll = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onTouchedCallback = noop;
        this.onChangeCallback = noop;
    }
    Object.defineProperty(MultiSelectComponent.prototype, "placeholder", {
        set: function (value) {
            if (value) {
                this._placeholder = value;
            }
            else {
                this._placeholder = "Select";
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this._settings = Object.assign(this.defaultSettings, value);
            }
            else {
                this._settings = Object.assign(this.defaultSettings);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MultiSelectComponent.prototype, "data", {
        set: function (value) {
            var _this = this;
            if (!value) {
                this._data = [];
            }
            else {
                var firstItem = value[0];
                this._sourceDataType = typeof firstItem;
                this._sourceDataFields = this.getFields(firstItem);
                this._data = value.map(function (item) {
                    return typeof item === "string" || typeof item === "number"
                        ? new ListItem(item)
                        : new ListItem({
                            id: item[_this._settings.idField],
                            text: item[_this._settings.textField],
                            isDisabled: item[_this._settings.disabledField]
                        });
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    MultiSelectComponent.prototype.onFilterTextChange = function ($event) {
        this.onFilterChange.emit($event);
    };
    MultiSelectComponent.prototype.onItemClick = function ($event, item) {
        if (this.disabled || item.isDisabled) {
            return false;
        }
        var found = this.isSelected(item);
        var allowAdd = this._settings.limitSelection === -1 || (this._settings.limitSelection > 0 && this.selectedItems.length < this._settings.limitSelection);
        if (!found) {
            if (allowAdd) {
                this.addSelected(item);
            }
        }
        else {
            this.removeSelected(item);
        }
        if (this._settings.singleSelection && this._settings.closeDropDownOnSelection) {
            this.closeDropdown();
        }
    };
    MultiSelectComponent.prototype.writeValue = function (value) {
        var _this = this;
        if (value !== undefined && value !== null && value.length > 0) {
            if (this._settings.singleSelection) {
                try {
                    if (value.length >= 1) {
                        var firstItem = value[0];
                        this.selectedItems = [
                            typeof firstItem === "string" || typeof firstItem === "number"
                                ? new ListItem(firstItem)
                                : new ListItem({
                                    id: firstItem[this._settings.idField],
                                    text: firstItem[this._settings.textField],
                                    isDisabled: firstItem[this._settings.disabledField]
                                })
                        ];
                    }
                }
                catch (e) {
                    // console.error(e.body.msg);
                }
            }
            else {
                var _data = value.map(function (item) {
                    return typeof item === "string" || typeof item === "number"
                        ? new ListItem(item)
                        : new ListItem({
                            id: item[_this._settings.idField],
                            text: item[_this._settings.textField],
                            isDisabled: item[_this._settings.disabledField]
                        });
                });
                if (this._settings.limitSelection > 0) {
                    this.selectedItems = _data.splice(0, this._settings.limitSelection);
                }
                else {
                    this.selectedItems = _data;
                }
            }
        }
        else {
            this.selectedItems = [];
        }
        this.onChangeCallback(value);
    };
    // From ControlValueAccessor interface
    MultiSelectComponent.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    // From ControlValueAccessor interface
    MultiSelectComponent.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    // Set touched on blur
    MultiSelectComponent.prototype.onTouched = function () {
        this.closeDropdown();
        this.onTouchedCallback();
    };
    MultiSelectComponent.prototype.trackByFn = function (index, item) {
        return item.id;
    };
    MultiSelectComponent.prototype.isSelected = function (clickedItem) {
        var found = false;
        this.selectedItems.forEach(function (item) {
            if (clickedItem.id === item.id) {
                found = true;
            }
        });
        return found;
    };
    MultiSelectComponent.prototype.isLimitSelectionReached = function () {
        return this._settings.limitSelection === this.selectedItems.length;
    };
    MultiSelectComponent.prototype.isAllItemsSelected = function () {
        // get disabld item count
        var filteredItems = this.listFilterPipe.transform(this._data, this.filter);
        var itemDisabledCount = filteredItems.filter(function (item) { return item.isDisabled; }).length;
        // take disabled items into consideration when checking
        if ((!this.data || this.data.length === 0) && this._settings.allowRemoteDataSearch) {
            return false;
        }
        return filteredItems.length === this.selectedItems.length + itemDisabledCount;
    };
    MultiSelectComponent.prototype.showButton = function () {
        if (!this._settings.singleSelection) {
            if (this._settings.limitSelection > 0) {
                return false;
            }
            // this._settings.enableCheckAll = this._settings.limitSelection === -1 ? true : false;
            return true; // !this._settings.singleSelection && this._settings.enableCheckAll && this._data.length > 0;
        }
        else {
            // should be disabled in single selection mode
            return false;
        }
    };
    MultiSelectComponent.prototype.itemShowRemaining = function () {
        return this.selectedItems.length - this._settings.itemsShowLimit;
    };
    MultiSelectComponent.prototype.addSelected = function (item) {
        if (this._settings.singleSelection) {
            this.selectedItems = [];
            this.selectedItems.push(item);
        }
        else {
            this.selectedItems.push(item);
        }
        this.onChangeCallback(this.emittedValue(this.selectedItems));
        this.onSelect.emit(this.emittedValue(item));
    };
    MultiSelectComponent.prototype.removeSelected = function (itemSel) {
        var _this = this;
        this.selectedItems.forEach(function (item) {
            if (itemSel.id === item.id) {
                _this.selectedItems.splice(_this.selectedItems.indexOf(item), 1);
            }
        });
        this.onChangeCallback(this.emittedValue(this.selectedItems));
        this.onDeSelect.emit(this.emittedValue(itemSel));
    };
    MultiSelectComponent.prototype.emittedValue = function (val) {
        var _this = this;
        var selected = [];
        if (Array.isArray(val)) {
            val.map(function (item) {
                selected.push(_this.objectify(item));
            });
        }
        else {
            if (val) {
                return this.objectify(val);
            }
        }
        return selected;
    };
    MultiSelectComponent.prototype.objectify = function (val) {
        if (this._sourceDataType === 'object') {
            var obj = {};
            obj[this._settings.idField] = val.id;
            obj[this._settings.textField] = val.text;
            if (this._sourceDataFields.includes(this._settings.disabledField)) {
                obj[this._settings.disabledField] = val.isDisabled;
            }
            return obj;
        }
        if (this._sourceDataType === 'number') {
            return Number(val.id);
        }
        else {
            return val.text;
        }
    };
    MultiSelectComponent.prototype.toggleDropdown = function (evt) {
        evt.preventDefault();
        if (this.disabled && this._settings.singleSelection) {
            return;
        }
        this._settings.defaultOpen = !this._settings.defaultOpen;
        if (!this._settings.defaultOpen) {
            this.onDropDownClose.emit();
        }
    };
    MultiSelectComponent.prototype.closeDropdown = function () {
        this._settings.defaultOpen = false;
        // clear search text
        if (this._settings.clearSearchFilter) {
            this.filter.text = "";
        }
        this.onDropDownClose.emit();
    };
    MultiSelectComponent.prototype.toggleSelectAll = function () {
        if (this.disabled) {
            return false;
        }
        if (!this.isAllItemsSelected()) {
            // filter out disabled item first before slicing
            this.selectedItems = this.listFilterPipe.transform(this._data, this.filter).filter(function (item) { return !item.isDisabled; }).slice();
            this.onSelectAll.emit(this.emittedValue(this.selectedItems));
        }
        else {
            this.selectedItems = [];
            this.onDeSelectAll.emit(this.emittedValue(this.selectedItems));
        }
        this.onChangeCallback(this.emittedValue(this.selectedItems));
    };
    MultiSelectComponent.prototype.getFields = function (inputData) {
        var fields = [];
        if (typeof inputData !== "object") {
            return fields;
        }
        // tslint:disable-next-line:forin
        for (var prop in inputData) {
            fields.push(prop);
        }
        return fields;
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [String])
    ], MultiSelectComponent.prototype, "placeholder", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], MultiSelectComponent.prototype, "disabled", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Object])
    ], MultiSelectComponent.prototype, "settings", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Array),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Array])
    ], MultiSelectComponent.prototype, "data", null);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onFilterChange"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onFilterChange", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onDropDownClose"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onDropDownClose", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onSelect"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onSelect", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onDeSelect"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onDeSelect", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onSelectAll"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onSelectAll", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])("onDeSelectAll"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MultiSelectComponent.prototype, "onDeSelectAll", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("blur"),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", []),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)
    ], MultiSelectComponent.prototype, "onTouched", null);
    MultiSelectComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "ng-multiselect-dropdown",
            template: "<div tabindex=\"0\" (blur)=\"onTouched()\" class=\"multiselect-dropdown\" (clickOutside)=\"closeDropdown()\">\n  <div [class.disabled]=\"disabled\">\n    <span tabindex=\"-1\" class=\"dropdown-btn\" (click)=\"toggleDropdown($event)\">\n      <span *ngIf=\"selectedItems.length == 0\">{{_placeholder}}</span>\n      <span class=\"selected-item\" *ngFor=\"let item of selectedItems;trackBy: trackByFn;let k = index\" [hidden]=\"k > _settings.itemsShowLimit-1\">\n        {{item.text}}\n        <a style=\"padding-top:2px;padding-left:2px;color:white\" (click)=\"onItemClick($event,item)\">x</a>\n      </span>\n      <span [ngClass]=\"{ 'dropdown-multiselect--active': _settings.defaultOpen }\" style=\"float:right !important;padding-right:4px\">\n        <span style=\"padding-right: 6px;\" *ngIf=\"itemShowRemaining()>0\">+{{itemShowRemaining()}}</span>\n        <span class=\"dropdown-multiselect__caret\"></span>\n      </span>\n    </span>\n  </div>\n  <div class=\"dropdown-list\" [hidden]=\"!_settings.defaultOpen\">\n    <ul class=\"item1\">\n      <li (click)=\"toggleSelectAll()\" *ngIf=\"(_data.length > 0 || _settings.allowRemoteDataSearch) && !_settings.singleSelection && _settings.enableCheckAll && _settings.limitSelection===-1\" class=\"multiselect-item-checkbox\" style=\"border-bottom: 1px solid #ccc;padding:10px\">\n        <input type=\"checkbox\" aria-label=\"multiselect-select-all\" [checked]=\"isAllItemsSelected()\" [disabled]=\"disabled || isLimitSelectionReached()\" />\n        <div>{{!isAllItemsSelected() ? _settings.selectAllText : _settings.unSelectAllText}}</div>\n      </li>\n      <li class=\"filter-textbox\" *ngIf=\"(_data.length>0 || _settings.allowRemoteDataSearch) && _settings.allowSearchFilter\">\n        <input type=\"text\" aria-label=\"multiselect-search\" [readOnly]=\"disabled\" [placeholder]=\"_settings.searchPlaceholderText\" [(ngModel)]=\"filter.text\" (ngModelChange)=\"onFilterTextChange($event)\">\n      </li>\n    </ul>\n    <ul class=\"item2\" [style.maxHeight]=\"_settings.maxHeight+'px'\">\n      <li *ngFor=\"let item of _data | multiSelectFilter:filter; let i = index;\" (click)=\"onItemClick($event,item)\" class=\"multiselect-item-checkbox\">\n        <input type=\"checkbox\" [attr.aria-label]=\"item.text\" [checked]=\"isSelected(item)\" [disabled]=\"disabled || (isLimitSelectionReached() && !isSelected(item)) || item.isDisabled\" />\n        <div>{{item.text}}</div>\n      </li>\n      <li class='no-data' *ngIf=\"_data.length == 0 && !_settings.allowRemoteDataSearch\">\n        <h5>{{_settings.noDataAvailablePlaceholderText}}</h5>\n      </li>\n    </ul>\n  </div>\n</div>\n",
            providers: [DROPDOWN_CONTROL_VALUE_ACCESSOR],
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [".multiselect-dropdown{position:relative;width:100%;font-size:inherit;font-family:inherit}.multiselect-dropdown .dropdown-btn{display:inline-block;border:1px solid #adadad;width:100%;padding:6px 12px;margin-bottom:0;font-weight:400;line-height:1.52857143;text-align:left;vertical-align:middle;cursor:pointer;background-image:none;border-radius:4px}.multiselect-dropdown .dropdown-btn .selected-item{border:1px solid #337ab7;margin-right:4px;background:#337ab7;padding:0 5px;color:#fff;border-radius:2px;float:left}.multiselect-dropdown .dropdown-btn .selected-item a{text-decoration:none}.multiselect-dropdown .dropdown-btn .selected-item:hover{box-shadow:1px 1px #959595}.multiselect-dropdown .dropdown-btn .dropdown-multiselect__caret{line-height:16px;display:block;position:absolute;box-sizing:border-box;width:40px;height:38px;right:1px;top:0;padding:4px 8px;margin:0;text-decoration:none;text-align:center;cursor:pointer;transition:transform .2s}.multiselect-dropdown .dropdown-btn .dropdown-multiselect__caret:before{position:relative;right:0;top:65%;color:#999;margin-top:4px;border-style:solid;border-width:8px 8px 0;border-color:#999 transparent;content:\"\"}.multiselect-dropdown .dropdown-btn .dropdown-multiselect--active .dropdown-multiselect__caret{transform:rotateZ(180deg)}.multiselect-dropdown .disabled>span{background-color:#eceeef}.dropdown-list{position:absolute;padding-top:6px;width:100%;z-index:9999;border:1px solid #ccc;border-radius:3px;background:#fff;margin-top:10px;box-shadow:0 1px 5px #959595}.dropdown-list ul{padding:0;list-style:none;overflow:auto;margin:0}.dropdown-list li{padding:6px 10px;cursor:pointer;text-align:left}.dropdown-list .filter-textbox{border-bottom:1px solid #ccc;position:relative;padding:10px}.dropdown-list .filter-textbox input{border:0;width:100%;padding:0 0 0 26px}.dropdown-list .filter-textbox input:focus{outline:0}.multiselect-item-checkbox:hover{background-color:#e4e3e3}.multiselect-item-checkbox input[type=checkbox]{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.multiselect-item-checkbox input[type=checkbox]:focus+div:before,.multiselect-item-checkbox input[type=checkbox]:hover+div:before{border-color:#337ab7;background-color:#f2f2f2}.multiselect-item-checkbox input[type=checkbox]:active+div:before{transition-duration:0s}.multiselect-item-checkbox input[type=checkbox]+div{position:relative;padding-left:2em;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;margin:0;color:#000}.multiselect-item-checkbox input[type=checkbox]+div:before{box-sizing:content-box;content:\"\";color:#337ab7;position:absolute;top:50%;left:0;width:14px;height:14px;margin-top:-9px;border:2px solid #337ab7;text-align:center;transition:.4s}.multiselect-item-checkbox input[type=checkbox]+div:after{box-sizing:content-box;content:\"\";position:absolute;transform:scale(0);transform-origin:50%;transition:transform .2s ease-out;background-color:transparent;top:50%;left:4px;width:8px;height:3px;margin-top:-4px;border-style:solid;border-color:#fff;border-width:0 0 3px 3px;-o-border-image:none;border-image:none;transform:rotate(-45deg) scale(0)}.multiselect-item-checkbox input[type=checkbox]:disabled+div:before{border-color:#ccc}.multiselect-item-checkbox input[type=checkbox]:disabled:focus+div:before .multiselect-item-checkbox input[type=checkbox]:disabled:hover+div:before{background-color:inherit}.multiselect-item-checkbox input[type=checkbox]:disabled:checked+div:before{background-color:#ccc}.multiselect-item-checkbox input[type=checkbox]:checked+div:after{content:\"\";transition:transform .2s ease-out;transform:rotate(-45deg) scale(1)}.multiselect-item-checkbox input[type=checkbox]:checked+div:before{-webkit-animation:.2s ease-in borderscale;animation:.2s ease-in borderscale;background:#337ab7}@-webkit-keyframes borderscale{50%{box-shadow:0 0 0 2px #337ab7}}@keyframes borderscale{50%{box-shadow:0 0 0 2px #337ab7}}"]
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ListFilterPipe])
    ], MultiSelectComponent);
    return MultiSelectComponent;
}());

var ClickOutsideDirective = /** @class */ (function () {
    function ClickOutsideDirective(_elementRef) {
        this._elementRef = _elementRef;
        this.clickOutside = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ClickOutsideDirective.prototype.onClick = function (event, targetElement) {
        if (!targetElement) {
            return;
        }
        var clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.clickOutside.emit(event);
        }
    };
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)
    ], ClickOutsideDirective.prototype, "clickOutside", void 0);
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('document:click', ['$event', '$event.target']),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [MouseEvent, HTMLElement]),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)
    ], ClickOutsideDirective.prototype, "onClick", null);
    ClickOutsideDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[clickOutside]'
        }),
        Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]])
    ], ClickOutsideDirective);
    return ClickOutsideDirective;
}());

var NgMultiSelectDropDownModule = /** @class */ (function () {
    function NgMultiSelectDropDownModule() {
    }
    NgMultiSelectDropDownModule_1 = NgMultiSelectDropDownModule;
    NgMultiSelectDropDownModule.forRoot = function () {
        return {
            ngModule: NgMultiSelectDropDownModule_1
        };
    };
    var NgMultiSelectDropDownModule_1;
    NgMultiSelectDropDownModule = NgMultiSelectDropDownModule_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]],
            declarations: [MultiSelectComponent, ClickOutsideDirective, ListFilterPipe],
            providers: [ListFilterPipe],
            exports: [MultiSelectComponent]
        })
    ], NgMultiSelectDropDownModule);
    return NgMultiSelectDropDownModule;
}());

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=ng-multiselect-dropdown.js.map


/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/about-us/about-us.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/about-us/about-us.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"cast-page\">\r\n\t<div class=\"about-bg\">About fixmtkix</div>\r\n</div>\r\n<div id=\"main\">\r\n\t\t<div class=\"shoes-shoper\">\r\n\t\t\t\t<ul class=\"about-shoe-servicec-list\">\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoe.png\"> What Is Shoe Restoration?</p>\r\n\t\t\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t\t\t<p class=\"img-right\">Shoe Restoration is the process of renewing a beat pair of shoes. Why is Shoe Restoration important? At a time when fast fashion is the leading contributor to landfill waste it’s our responsibility to ask how to extend the life cycle of these goods. Luckily, the answer exists: Restoration! Fixmykix’s mission is to create a platform that makes these services accessible globally.</p>\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-right\"><a href=\"shoe/restoration\">Read More</a></div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service08.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoe.png\"> What is Shoe Art or Shoe Redesign?</p>\r\n\t\t\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t\t\t<p class=\"img-right\">Shoe Art here at Fixmykix is looked at as a creative form of restoring a new value into the shoe that was not there before. To use a unique innovative perspective to craft a new work of art from an existing shoe silhouette. Shoe art can be whatever you can envision. All you need to do is let us know what you want in a Service Request Form!</p>\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-right\"><a href=\"shoe/art\">Read More</a></div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service08.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoe.png\"> What is FixmyKix?</p>\r\n\t\t\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t\t\t<p class=\"img-right\">FixmyKix is...</p>\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"text-right\"><a href=\"what/is/fixmykix\">Read More</a></div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service08.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t</ul>\r\n\t\t</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/auth.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/auth.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<router-outlet></router-outlet>\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/contact-us/contact-us.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/contact-us/contact-us.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid container_contact\">\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12\">\r\n        <div class=\"contact_text \">Contact Us</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-6 col-lg-offset-1  col-md-8 col-sm-6 col-xs-12 contact-address-container\">\r\n        <div class=\"address_title\">FixMyKix</div>\r\n        <div class=\"address\">Address<br>\r\n        </div>\r\n        <div class=\"contact_email\">\r\n          <img class=\"svg_contact\" src=\"/img/mail.svg\">\r\n          <a href=\"mailto:info@acasa.co\">info@fixmykix.com</a> \r\n        </div>\r\n        <div class=\"contact_mobile\">\r\n          <img class=\"svg_contact\" src=\"/img/phone-call.svg\">\r\n          <a href=\"tel:+91 77982 - 68680\">+91 12345 67890</a>\r\n        </div>\r\n        <!-- <div class=\"contact_mobile\">\r\n          <img class=\"svg_contact\" src=\"/img/phone-call.svg\">\r\n          <a href=\"tel:+91 77982 - 68680\">+91 77982 64640</a>\r\n        </div>\r\n        <div class=\"contact_mobile\">\r\n          <img class=\"svg_contact\" src=\"/img/phone-call.svg\">\r\n          <a href=\"tel:+91 77982 - 68680\">+91 77982 68680</a>\r\n        </div> -->\r\n      </div>\r\n      <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 contact-form-container\">\r\n        <div class=\"form_title\">send us a message</div>\r\n        <form name=\"ContactForm\" [formGroup]=\"form\">\r\n          <div class=\"form-group\">\r\n            <input class=\"form-control\" name=\"name\" type=\"text\" placeholder=\"Your Name\" id=\"example-text-input\">\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <input type=\"email\" name=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\" placeholder=\"Your Email\">\r\n          </div>\r\n          <div class=\"form-group\">\r\n            <textarea class=\"form-control\" name=\"message\" placeholder=\"Your Message\" id=\"exampleTextarea\" rows=\"3\"></textarea>\r\n          </div>\r\n          <div>\r\n            <button class=\"btn btn-default contct_submit\" type=\"submit\">Send Message</button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/login/login.component.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/login/login.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"login_form\">\r\n  <div class=\"login_form_set\">\r\n    <div class=\"login-logo\"><h2>Login</h2></div>\r\n    <form name=\"loginForm\" [formGroup]=\"formGroup\" (ngSubmit)=\"login()\">\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <label>Email</label>\r\n        <input type=\"email\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"email\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n          <label>Password</label>\r\n          <input class=\"form-control\" formControlName=\"password\" type=\"password\" placeholder=\"\" id=\"password-input\" autocomplete=\"off\">\r\n        </div>\r\n      <div class=\"form-group login-button\">\r\n        <button type=\"submit\">Submit</button>\r\n      </div>\r\n    </form>\r\n  </div>\r\n</div> -->\r\n\r\n<div class=\"login-page\">\r\n    <div class=\"center-form\">\r\n      <h3 class=\"text-center\">LOG IN</h3>\r\n      <form name=\"loginForm\" [formGroup]=\"formGroup\" (ngSubmit)=\"login()\">\r\n        <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n            <label>Email</label>\r\n          <input type=\"email\" class=\"form-control\" placeholder=\"\" autocomplete=\"off\" formControlName=\"email\">\r\n          <!-- <input type=\"text\" class=\"form-control\" placeholder=\"@Username\" required> -->\r\n        </div>\r\n        <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n            <label>Password</label>\r\n            <input class=\"form-control\" formControlName=\"password\" type=\"password\" placeholder=\"\" id=\"password-input\" autocomplete=\"off\">\r\n          <!-- <input type=\"Password\" class=\"form-control\" placeholder=\"Password\" required> -->\r\n        </div>\r\n        <div class=\"form-group login-button\">\r\n          <button type=\"submit\">Login</button>\r\n        </div>\r\n        <div class=\"backlinks\">Dont have an account? <a href=\"create/account\">Create account</a></div>\r\n      </form>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/main/main.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/main/main.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <main>\r\n\t\t<div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\r\n\t\t\t\t<ol class=\"carousel-indicators\">\r\n\t\t\t\t\t<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>\r\n\t\t\t\t\t<li data-target=\"#myCarousel\" data-slide-to=\"1\"></li>\r\n\t\t\t\t\t<li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\r\n\t\t\t\t</ol>\r\n\t\t\t\t<div class=\"carousel-inner\">\r\n\t\t\t\t\t<div class=\"carousel-item active\">\r\n\t\t\t\t\t\t<img src=\"../../../../assets//jordanshoes00.jpg\" alt=\"alt\">\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"carousel-item\">\r\n\t\t\t\t\t\t<img src=\"../../../../assets/images/jordanshoes02.jpg\" alt=\"alt\">\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"carousel-item\">\r\n\t\t\t\t\t\t<img src=\"../../../../assets/images/jordanshoes03.jpg\" alt=\"alt\">\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\r\n\t\t\t\t\t<span class=\"glyphicon glyphicon-chevron-left\"></span>\r\n\t\t\t\t\t<span class=\"sr-only\">Previous</span>\r\n\t\t\t\t</a>\r\n\t\t\t\t<a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\r\n\t\t\t\t\t<span class=\"glyphicon glyphicon-chevron-right\"></span>\r\n\t\t\t\t\t<span class=\"sr-only\">Next</span>\r\n\t\t\t\t</a>\r\n\t\t\t</div>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"shoes-shoper\">\r\n\t\t\t<div class=\"center-text\"><h4>FIXMYKIX is an online mobile marketplace for shoe restoration and shoe art.<br>In a world stunned by fast fashion its time to start interacting with alternatives to throwing shoes in our landfills.</h4></div>\r\n\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoe.png\"> REQUEST OF SERVICE</p>\r\n\t\t\t\t<a class=\"wp-block-button__link has-text-color has-background-color has-background has-foreground-dark-background-color\" href=\"https://forms.gle/FDn2mVtWjjrivXx6A\" target=\"_blank\" rel=\"noreferrer noopener\">SERVICE REQUEST FORM</a>\r\n\t\t\t</h2>\r\n\t\t\t<ul class=\"shoe-servicec-list\">\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<h3 class=\"h3-base-heading\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"> Deep Cleaning</h3>\r\n\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service01.jpg\" width=\"100%\"></figure>\r\n\t\t\t\t\t<div class=\"special-text\">\r\n\t\t\t\t\t\t<h3>dizzzykicks</h3>\r\n\t\t\t\t\t\t<p>Sneaker Customs|Restorations<br>It’s Dizzzy, with 3 Z’s<br>Sneaker CreativeM<br>Bayarea (SF) Native<br>Email and DMs open, so slide (professionally)<br><a href=\"https://www.instagram.com/dizzzykicks/\"><i class=\"fa fa-instagram\"></i> @dizzzykicks</a></p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<h3 class=\"h3-base-heading mt-80\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\">Pokemon Customs</h3>\r\n\t\t\t\t\t<p><img src=\"../../../../assets/images/img_service02.jpg\" width=\"300px\"><span>anbu_customs</span>@2_talll<br>Bay Area, CA<br>I do more than just shoes<br>DM me for any inquiries or customs</p>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<h3 class=\"h3-base-heading mt-80 justify-content-end\">Basic/Spot Cleaning <img style=\"margin: 0 0 0 6px;\" src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"></h3>\r\n\t\t\t\t\t<p class=\"img-right text-right\"><img src=\"../../../../assets/images/img_service04.jpg\" width=\"300px\"> Understanding a spot is key to successfully removing it. Knowing what caused the spot and getting to it quickly can help immensely in getting it out.<br><br>Most spots are caused by a spill of some kind, tracking soils in from somewhere (usually outside), or from an accident (like vomiting).</p>\r\n\t\t\t\t\t<p class=\"img-right text-right\">Following these recommended steps will help get spots out, even if you don't know what caused the spot in the first place</p>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li>\r\n\t\t\t\t\t<h3 class=\"h3-base-heading\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"> De-Oxidation</h3>\r\n\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service03.jpg\"></figure>\r\n\t\t\t\t\t<div class=\"special-text special-text-white\">\r\n\t\t\t\t\t\t<h3>prstg_shop</h3>\r\n\t\t\t\t\t\t<p>EST2013<br>1201 Lincoln Ave Alameda CA<br>510-227-5386<br>Teamprstg@gmail.com<br>Tuesday-Friday 12-3pm<br>CLOSED Sat - Mon<br>Buy<br>/Sell/Trade/Resto<br><a href=\"https://www.instagram.com/prstg_shop/\"><i class=\"fa fa-instagram\"></i>  @prstg_shop</a><br><a style=\"margin-top: 0;\" href=\"http://www.prstgshop.com/\"><i class=\"fa fa-globe\"></i> www.prstgshop.com</a></p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li class=\"one-third\">\r\n\t\t\t\t\t<h3 class=\"h3-base-heading\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"> Reglue</h3>\r\n\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service05.jpg\"></figure>\r\n\t\t\t\t\t<div class=\"special-text\">\r\n\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li class=\"one-third\">\r\n\t\t\t\t\t<h3 class=\"h3-base-heading\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"> Repaint</h3>\r\n\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service06.jpg\"></figure>\r\n\t\t\t\t\t<div class=\"special-text\">\r\n\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t\t<li class=\"one-third\">\r\n\t\t\t\t\t<h3 class=\"h3-base-heading\"><img src=\"../../../../assets/images/icons/icon_service.png\" width=\"34px\"> Shoe Art</h3>\r\n\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_service07.jpg\"></figure>\r\n\t\t\t\t\t<div class=\"special-text\">\r\n\t\t\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</li>\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n</main> -->\r\n\r\n\r\n<!-- \r\n<main>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"row big-banner\">\r\n\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t<img src=\"../../../../assets/images/banner-shoe.jpg\" alt=\"images\">\r\n\t\t\t\t<p class=\"text-center text-transform\">by: NIKOSWOOSH</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-6 text-center\">\r\n\t\t\t\t<h1>CREATE JUST FOT YOU</h1>\r\n\t\t\t\t<h4>YOUR FLAVEOR-YOUR SWAG</h4>\r\n\t\t\t\t<p>CUSTOMSNEAKERS BY INDEOENDENT ARTISTS</p>\r\n\t\t\t\t<a href=\"javascript:void(0)\">SHOP CUSTOMS</a>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading\">Custom Artwork Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading\">Repair & Restoration Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<div class=\"bg-image-text\">\r\n\t\t\t\t\t\t<h1>cobbler <span>noun</span></h1>\r\n\t\t\t\t\t\t<h2>cob-bler l \\ 'ka bler</h2>\r\n\t\t\t\t\t\t<h3>1 : amender or maker of shoes and afion of other  leather goods</h3>\r\n\t\t\t\t\t\t<h3>2 arachaic : a clumsy workman</h3>\r\n\t\t\t\t\t\t<h3>3 : a tall iced drink consisting usually of wine, rum, or whisky and sugar garnished with mint or a slice of lemon or ornage</h3>\r\n\t\t\t\t\t\t<h3>4 : a deep-dish fruit dessert with a thick top crust</h3>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">Repair & Restoration Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\" (click)=\"explore_artists()\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">born_originals</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> BERLIN, GERMANY</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\" (click)=\"explore_artists()\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">sneakeaze</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> provence-alpes-cote d'azur france</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\" (click)=\"explore_artists()\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">tony</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> hamerkaz, israel</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\" (click)=\"explore_artists()\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">kustomb</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> california, nmited states</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">behind the scenes</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/ViQH9A9SWOA\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/nJ2TNbnF-ZE\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/pD1tK-TigS8\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/YRFRfoLFT-Q\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<a href=\"\" class=\"see-more\">see more of \"the work\"</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">latest articles</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">netflix and skill - <span>btbro mahn</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">love your shoes - <span>by my gosh</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">i didn't have to kill fido afterall - <span>by jamie lee</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<a href=\"\" class=\"see-more\">see more of \"the work\"</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"hash-tag\"><span># FIXMYKIX</span></div>\r\n\t</div>\r\n</main> -->\r\n\r\n\r\n<main>\r\n\t<div id=\"main\">\r\n\t\t<div class=\"row big-banner\">\r\n\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t<img src=\"../../../../assets/images/banner-shoe.jpg\" alt=\"images\">\r\n\t\t\t\t<p class=\"text-center text-transform\">by: NIKOSWOOSH</p>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"col-md-6 text-center\">\r\n\t\t\t\t<h1>CREATE JUST FOT YOU</h1>\r\n\t\t\t\t<h4>YOUR FLAVOR-YOUR SWAG</h4>\r\n\t\t\t\t<p>CUSTOMSNEAKERS BY INDEOENDENT ARTISTS</p>\r\n\t\t\t\t<a href=\"javascript:void(0)\">SHOP CUSTOMS</a>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading\">Custom Artwork Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/artwork03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading\">Repair & Restoration Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/service_repair03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<div class=\"bg-image-text\">\r\n\t\t\t\t\t\t<h1>cobbler <span>noun</span></h1>\r\n\t\t\t\t\t\t<h2>cob-bler l \\ 'ka bler</h2>\r\n\t\t\t\t\t\t<h3>1 : amender or maker of shoes and afion of other  leather goods</h3>\r\n\t\t\t\t\t\t<h3>2 arachaic : a clumsy workman</h3>\r\n\t\t\t\t\t\t<h3>3 : a tall iced drink consisting usually of wine, rum, or whisky and sugar garnished with mint or a slice of lemon or ornage</h3>\r\n\t\t\t\t\t\t<h3>4 : a deep-dish fruit dessert with a thick top crust</h3>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">Repair & Restoration Services</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">born_originals</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> BERLIN, GERMANY</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">sneakeaze</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> provence-alpes-cote d'azur france</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">tony</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> hamerkaz, israel</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/custom_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<a href=\"javascript:void:void(0)\">kustomb</a>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\"><img src=\"../../../../assets/images/icon_location.png\"> california, nmited states</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services custom-services-providers\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">Service Providers</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">BERLIN, GERMANY</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">provence-alpes-cote d'azur france</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">hamerkaz, israel</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">california, nmited states</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">california, nmited states</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-2\">\r\n\t\t\t\t\t<div class=\"outer-border\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<p class=\"brand-name-loaction\" style=\"margin: 12px 0 0 0;\"><a href=\"javascript:void:void(0)\">california, nmited states</a></p>\r\n\t\t\t\t\t\t<div class=\"star-ratings\">\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star\"></i>\r\n\t\t\t\t\t\t\t<i class=\"fa fa-star-o\"></i>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">behind the scenes</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/ViQH9A9SWOA\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/nJ2TNbnF-ZE\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/pD1tK-TigS8\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-3\">\r\n\t\t\t\t\t<iframe src=\"https://www.youtube.com/embed/YRFRfoLFT-Q\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<a href=\"\" class=\"see-more\">see more of \"the work\"</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"custom-services\">\t\r\n\t\t\t<div class=\"row\">\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<h2 class=\"center-heading01\">latest articles</h2>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art01.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">netflix and skill - <span>btbro mahn</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art02.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">love your shoes - <span>by my gosh</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t<a href=\"javascript:void:void(0)\">\r\n\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/latest_art03.jpg\" alt=\"img\" width=\"100%\"></figure>\r\n\t\t\t\t\t\t<h3 class=\"heading-article\">i didn't have to kill fido afterall - <span>by jamie lee</span></h3>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"col-md-12\">\r\n\t\t\t\t\t<a href=\"\" class=\"see-more\">see more of \"the work\"</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"hash-tag\"><span># FIXMYKIX</span></div>\r\n\t</div>\r\n</main>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/register/register.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/register/register.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "Register"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/shoeart/shoeart.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/shoeart/shoeart.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\t\t\t<div class=\"cast-page\">\r\n\t\t\t\t<div class=\"about-bg\"><span>Shoe Art</span></div>\r\n\t\t\t</div>\r\n\t\t\t<div id=\"main\">\r\n\t\t\t\t<div class=\"shoes-shoper\">\r\n\t\t\t\t\t<ul class=\"about-shoe-servicec-list\">\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> Shoe Art is...</p>\r\n\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t<p class=\"img-right\">Short description of Shoe Restoration…</p>\r\n\t\t\t\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix05.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> History of Shoe Art</p>\r\n\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix06.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li>\r\n\t\t\t\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> Importance of creativity and expression</p>\r\n\t\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix07.jpg\" alt=\"img\"></figure>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t</div>\r\n\t\t\t</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/shoerestoration/shoerestoration.component.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/shoerestoration/shoerestoration.component.html ***!
  \***********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cast-page\">\r\n\t<div class=\"about-bg\"><span>Shoe Restoration</span></div>\r\n</div>\r\n<div id=\"main\">\r\n\t<div class=\"shoes-shoper\">\r\n\t\t<ul class=\"about-shoe-servicec-list\">\r\n\t\t\t<li>\r\n\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> What Is Shoe Restoration?</p>\r\n\t\t\t\t\t</h2>\r\n\t\t\t\t\t<p class=\"img-right\">Short description of Shoe Restoration…</p>\r\n\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix02.jpg\" alt=\"img\"></figure>\r\n\t\t\t</li>\r\n\t\t\t<li>\r\n\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> Shoe waste & How it affects the environment...…</p>\r\n\t\t\t\t\t</h2>\r\n\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix03.jpg\" alt=\"img\"></figure>\r\n\t\t\t</li>\r\n\t\t\t<li>\r\n\t\t\t\t<div class=\"about-shoe-text\">\r\n\t\t\t\t\t<h2 class=\"h2-base-heading\">\r\n\t\t\t\t\t\t<p><img src=\"../../../../assets/images/icons/icon_shoedetails.png\"> History of Shoe Restoration…..</p>\r\n\t\t\t\t\t</h2>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<figure><img src=\"../../../../assets/images/img_aboutfix04.jpg\" alt=\"img\"></figure>\r\n\t\t\t</li>\r\n\t\t</ul>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/sign-up/sign-up.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/sign-up/sign-up.component.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"signin-page\">\r\n  <div class=\"center-form\">\r\n    <h3 class=\"text-center\">SIGN UP</h3>\r\n    <form name=\"SignUpForm\" [formGroup]=\"formGroup\" (ngSubmit)=\"onSubmit()\" autocomplete=\"off\">\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"First Name\" formControlName=\"first_name\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"last Name\" formControlName=\"last_name\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email\" formControlName=\"email\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"Password\" class=\"form-control\" placeholder=\"Password\" formControlName=\"password\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"Password\" class=\"form-control\" placeholder=\"Confirm Password\" formControlName=\"cpassword\">\r\n      </div>\r\n      <div class=\"form-group\" [formGroup]=\"formGroup\">\r\n        <input type=\"text\" class=\"form-control\" placeholder=\"Zip Code\" formControlName=\"zipcode\">\r\n      </div>\r\n      <div class=\"form-group check-user\" [formGroup]=\"formGroup\">\r\n          <label for=\"head\" class=\"gender-check\">\r\n          <input type=\"radio\" id=\"head\" value=\"0\" formControlName=\"role\">\r\n          Customer</label>\r\n          <label for=\"Artist\" class=\"gender-check\">\r\n          <input type=\"radio\" id=\"Artist\" value=\"1\" formControlName=\"role\">\r\n          Service Provider</label>\r\n      </div>\r\n      <div class=\"form-group login-button\">\r\n        <button type=\"submit\">Submit</button>\r\n      </div>\r\n      <div class=\"backlinks\">Already have an account <a href=\"login\"> LOGIN</a></div>\r\n    </form>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\t\t\t<div class=\"cast-page\">\r\n                    <div class=\"about-bg\">What is fixmtkix</div>\r\n                </div>\r\n                <div id=\"main\">\r\n                    <div class=\"shoes-shoper\">\r\n                        <div class=\"center-text-heading\">TEAM BIOS</div>\r\n                        <ul class=\"about-shoe-servicec-list\">\r\n                            <li>\r\n                                <figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\"></figure>\r\n                                <div class=\"about-shoe-text\">\r\n                                    <h2 class=\"h2-base-heading\"><p>What Is Shoe Restoration?</p></h2>\r\n                                    <p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                                </div>\r\n                            </li>\r\n                            <li>\r\n                                <figure><img src=\"../../../../assets/images/img_user.png\" alt=\"img\"></figure>\r\n                                <div class=\"about-shoe-text\">\r\n                                    <h2 class=\"h2-base-heading\"><p>What is Shoe Art or Shoe Redesign?</p></h2>\r\n                                    <p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                                </div>\r\n                            </li>\r\n                            <li class=\"w-100\">\r\n                                <div class=\"about-shoe-text\">\r\n                                    <h2 class=\"h2-base-heading\">\r\n                                        <p>The Story of FixmyKix?</p>\r\n                                    </h2>\r\n                                    <p class=\"img-right\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n                                </div>\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/footer.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/footer.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <footer>\r\n  <div id=\"footer\">\r\n    <div class=\"inner-footer\">\r\n      <div class=\"footbox foot-box-01\">\r\n        <h3 class=\"logo\">FIXMYKIX INC</h3>\r\n        <p><i class=\"fa fa-map-marker\"></i> 2934 TELEGRAPH AVE OAKLAND, CA, 94609</p>\r\n        <p><i class=\"fa fa-globe\"></i> JAMESG@FIXMYKIX.COM</p>\r\n        <p><i class=\"fa fa-phone\"></i> (510) 689-4282</p>\r\n      </div>\r\n      <div class=\"footbox foot-box-02\">\r\n        <h3 class=\"logo\">Social links</h3>\r\n        <ul>\r\n          <li><a href=\"https://www.facebook.com/FixmyKixx/\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>\r\n          <li><a href=\"https://www.linkedin.com/in/james-green-9a9243158\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>\r\n          <li><a href=\"https://www.instagram.com/fixmykixx/?hl=en\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"footbox foot-box-03\">\r\n        <small>&copy; Copyright FIXMYKIX INC – All rights reserved</small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</footer> -->\r\n\r\n\r\n<footer>\r\n  <div id=\"footer\">\r\n    <div class=\"row inner-footer\">\r\n      <div class=\"col-md-3\">\r\n        <h2>our services</h2>\r\n        <ul>\r\n          <li><a href=\"#\">Women's Shoes/Boots</a></li>\r\n          <li><a href=\"#\">Men's Shoes/Boots</a></li>\r\n          <li><a href=\"#\">Handbags</a></li>\r\n          <li><a href=\"#\">Wallets</a></li>\r\n          <li><a href=\"#\">Belts</a></li>\r\n          <li><a href=\"#\">Garments</a></li>\r\n        </ul>\r\n        <div class=\"text-center\"><a href=\"#\" class=\"order-a-repair\">order a repair</a></div>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <h2>Company Links</h2>\r\n        <ul>\r\n          <li><a href=\"#\">about</a></li>\r\n          <li><a href=\"#\">news & press</a></li>\r\n          <li><a href=\"#\">testimonials</a></li>\r\n          <li><a href=\"#\">privacy policy</a></li>\r\n          <li><a href=\"#\">tearms of use</a></li>\r\n          <li><a href=\"#\">repair policies</a></li>\r\n          <li><a href=\"#\">refund policy</a></li>\r\n          <li><a href=\"#\">sitemap</a></li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <h2>My Account</h2>\r\n        <ul>\r\n          <li><a href=\"#\">MY account</a></li>\r\n          <li><a href=\"#\">Corporate amount</a></li>\r\n          <li><a href=\"#\">customer service</a></li>\r\n          <li><a href=\"#\">order a repair</a></li>\r\n          <li><a href=\"#\">how it works</a></li>\r\n          <li><a href=\"#\">cart</a></li>\r\n          <li><a href=\"#\">log in</a></li>\r\n        </ul>\r\n      </div>\r\n      <div class=\"col-md-3\">\r\n        <h2>connect</h2>\r\n        <ul>\r\n          <li><a href=\"javascript:void(0)\"><i class=\"fa fa-map-marker\" style=\"color: #e8b13d; font-size: 26px;\"></i><br>2934 TELEGRAPH AVE OAKLAND, CA, 94609</a></li>\r\n          <li><a href=\"tel:5106894282\"><i class=\"fa fa-phone\" style=\"color: #e8b13d; font-size: 26px;\"></i><br>510-689-4282</a></li>\r\n          <li><a href=\"mainto:jamesg@fixmykix.com\"><i class=\"fa fa-envelope\" style=\"color: #e8b13d; font-size: 26px;\"></i><br>JAMESG@FIXMYKIX.COM</a></li>\r\n        </ul>\r\n        <ul class=\"social-icons\">\r\n          <li><a href=\"#\"><img src=\"../../assets/images/facebook.png\" alt=\"img\"></a></li>\r\n          <li><a href=\"#\"><img src=\"../../assets/images/instagram.png\" alt=\"img\"></a></li>\r\n          <li><a href=\"#\"><img src=\"../../assets/images/twitter.png\" alt=\"img\"></a></li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"copyright\">Copyright FIXMYKIX INC – All rights reserved</div>\r\n</footer>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/layout/header.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/layout/header.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\r\n\t<div id=\"header\">\r\n\t\t<div class=\"inner-header\">\r\n\t\t\t<div class=\"inner-header-flex\">\r\n\t\t\t\t<div class=\"logo\">FIXMYKIX</div>\r\n\t\t\t\t<div class=\"navigation-menu\">\r\n\t\t\t\t\t<ul class=\"pc-menu\">\r\n\t\t\t\t\t\t<li><a href=\"/\">home</a></li>\r\n\t\t\t\t\t\t<li *ngIf=\"role != 1\"><a href=\"artist/explore\">Explore</a></li>\r\n\t\t\t\t\t\t<li><a href=\"about-us\">about fixmykix</a></li>\r\n\t\t\t\t\t\t<li *ngIf=\"isLoggedIn == false\"><a href=\"login\">Login</a></li>\r\n\t\t\t\t\t\t<li class=\"dropdown\" *ngIf=\"isLoggedIn == true\">\r\n\t\t\t\t\t\t\t<button id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-user\"></i></button>\r\n\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\r\n\t\t\t\t\t\t\t\t<a class=\"dropdown-item\" *ngIf=\"role == '0'\" href=\"javascript:void(0)\" (click)=\"myRequest()\"><i class=\"fa fa-list\"></i> My Reqeust</a>\r\n\t\t\t\t\t\t\t  <a class=\"dropdown-item\" href=\"javascript:void(0)\" (click)=\"profileRoute()\"><i class=\"fa fa-user\"></i> Profile</a>\r\n\t\t\t\t\t\t\t  <a class=\"dropdown-item\" href=\"javascript:void(0)\" (click)=\"logout()\"><i class=\"fa fa-sign-out\"></i> Logout</a>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t  </li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t\t<p class=\"menu-open\"><a href=\"javascript:void(0)\">MENU</a></p>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</header>\r\n\r\n\r\n\r\n<!-- <header>\r\n\t<div id=\"header\">\r\n\t\t<div class=\"inner-header\">\r\n\t\t\t<div class=\"header-top\">\r\n\t\t\t\t<div class=\"logo-bar\">\r\n\t\t\t\t\t<p class=\"custom-movement\">THE CUSTOM MOVEMENT</p>\r\n\t\t\t\t\t<a href=\"javascript:void(0)\" class=\"fmk-logo\">FIXMYKIX</a>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"top-menu\">\r\n\t\t\t\t\t<ul class=\"top-menu-list\">\r\n\t\t\t\t\t\t<li class=\"dropdown\">\r\n\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" class=\"nav-links dropdown-toggle\" data-toggle=\"dropdown\">SHOP <span class=\"caret\"></span></a>\r\n\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">SHOP 01</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li class=\"dropdown\">\r\n\t\t\t\t\t\t\t<a href=\"javascript:void(0)\" class=\"nav-links dropdown-toggle\" data-toggle=\"dropdown\">COLLECTIONS <span class=\"caret\"></span></a>\r\n\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:void(0)\">COLLECTIONS 01</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t<li><a class=\"nav-links\" href=\"/\">FAVOURITES</a></li>\r\n\t\t\t\t\t\t<li><a class=\"nav-links\" href=\"artist/explore\">EXPLORE</a></li>\r\n\t\t\t\t\t\t<li *ngIf=\"isLoggedIn == false\"><a class=\"nav-links\" href=\"login\">LOGIN / SIGNUP</a></li>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<li class=\"dropdown\" *ngIf=\"isLoggedIn == true\">\r\n\t\t\t\t\t\t\t<button id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"fa fa-user\"></i></button>\r\n\t\t\t\t\t\t\t<div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\r\n\t\t\t\t\t\t\t\t<a class=\"dropdown-item\" *ngIf=\"role == '0'\" href=\"javascript:void(0)\" (click)=\"myRequest()\"><i class=\"fa fa-list\"></i> My Reqeust</a>\r\n\t\t\t\t\t\t\t  <a class=\"dropdown-item\" href=\"javascript:void(0)\" (click)=\"profileRoute()\"><i class=\"fa fa-user\"></i> Profile</a>\r\n\t\t\t\t\t\t\t  <a class=\"dropdown-item\" href=\"javascript:void(0)\" (click)=\"logout()\"><i class=\"fa fa-sign-out\"></i> Logout</a>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t  </li>\r\n\t\t\t\t\t\t<li class=\"dropdown\">\r\n\t\t\t\t\t\t\t<a class=\"nav-links dropdown-toggle search-menu\" href=\"javascript:void(0)\" data-toggle=\"dropdown\"><img src=\"../../assets/images/icon_search.png\"></a>\r\n\t\t\t\t\t\t\t<ul class=\"dropdown-menu search-input\">\r\n\t\t\t\t\t\t\t\t<li><input type=\"search\" name=\"search\" placeholder=\"Search\"></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</li>\r\n\t\t\t\t\t</ul>\r\n\t\t\t\t</div>\r\n\t\t\t\t<p class=\"bar-menu\"><a href=\"javascript:void(0)\"><img src=\"images/icon_menu.png\"></a></p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</header> -->"

/***/ }),

/***/ "./src/app/@auth/auth-routing.module.ts":
/*!**********************************************!*\
  !*** ./src/app/@auth/auth-routing.module.ts ***!
  \**********************************************/
/*! exports provided: AuthRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function() { return AuthRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _components_main_main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/main/main.component */ "./src/app/@auth/components/main/main.component.ts");
/* harmony import */ var _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/contact-us/contact-us.component */ "./src/app/@auth/components/contact-us/contact-us.component.ts");
/* harmony import */ var _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/sign-up/sign-up.component */ "./src/app/@auth/components/sign-up/sign-up.component.ts");
/* harmony import */ var _components_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/about-us/about-us.component */ "./src/app/@auth/components/about-us/about-us.component.ts");
/* harmony import */ var _components_shoerestoration_shoerestoration_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/shoerestoration/shoerestoration.component */ "./src/app/@auth/components/shoerestoration/shoerestoration.component.ts");
/* harmony import */ var _components_shoeart_shoeart_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/shoeart/shoeart.component */ "./src/app/@auth/components/shoeart/shoeart.component.ts");
/* harmony import */ var _components_what_is_fixmykix_what_is_fixmykix_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/what-is-fixmykix/what-is-fixmykix.component */ "./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components */ "./src/app/@auth/components/index.ts");











var routes = [{
        path: '',
        component: _components__WEBPACK_IMPORTED_MODULE_10__["NgxAuthComponent"],
        children: [
            {
                path: '',
                component: _components_main_main_component__WEBPACK_IMPORTED_MODULE_3__["NgxMainComponent"]
            },
            {
                path: 'login',
                component: _components__WEBPACK_IMPORTED_MODULE_10__["NgxLoginComponent"]
            },
            {
                path: 'register',
                component: _components__WEBPACK_IMPORTED_MODULE_10__["NgxRegisterComponent"]
            },
            {
                path: 'contact-us',
                component: _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_4__["NgxContactUsComponent"]
            },
            {
                path: 'create/account',
                component: _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_5__["NgxSignUpComponent"]
            },
            {
                path: 'about-us',
                component: _components_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_6__["NgxAboutUsComponent"]
            }, {
                path: 'shoe/restoration',
                component: _components_shoerestoration_shoerestoration_component__WEBPACK_IMPORTED_MODULE_7__["NgxShoeRestorationComponent"]
            }, {
                path: 'shoe/art',
                component: _components_shoeart_shoeart_component__WEBPACK_IMPORTED_MODULE_8__["NgxShoeArtComponent"]
            }, {
                path: 'what/is/fixmykix',
                component: _components_what_is_fixmykix_what_is_fixmykix_component__WEBPACK_IMPORTED_MODULE_9__["NgxFixmyKixComponent"]
            }, {
                path: 'customer',
                loadChildren: function () { return Promise.all(/*! import() | customer-customer-module */[__webpack_require__.e("default~artist-artist-module~customer-customer-module"), __webpack_require__.e("customer-customer-module")]).then(__webpack_require__.bind(null, /*! ./customer/customer.module */ "./src/app/@auth/customer/customer.module.ts"))
                    .then(function (m) { return m.CustomerModule; }); },
            }, {
                path: 'artist',
                loadChildren: function () { return Promise.all(/*! import() | artist-artist-module */[__webpack_require__.e("default~artist-artist-module~customer-customer-module"), __webpack_require__.e("artist-artist-module")]).then(__webpack_require__.bind(null, /*! ./artist/artist.module */ "./src/app/@auth/artist/artist.module.ts"))
                    .then(function (m) { return m.ArtistModule; }); },
            }, {
                path: 'users',
                loadChildren: function () { return __webpack_require__.e(/*! import() | chat-chat-module */ "chat-chat-module").then(__webpack_require__.bind(null, /*! ./chat/chat.module */ "./src/app/@auth/chat/chat.module.ts"))
                    .then(function (m) { return m.ChatModule; }); },
            }
        ],
    }];
var AuthRoutingModule = /** @class */ (function () {
    function AuthRoutingModule() {
    }
    AuthRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], AuthRoutingModule);
    return AuthRoutingModule;
}());



/***/ }),

/***/ "./src/app/@auth/auth.interceptor.ts":
/*!*******************************************!*\
  !*** ./src/app/@auth/auth.interceptor.ts ***!
  \*******************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(router) {
        this.router = router;
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        var _this = this;
        return next.handle(req)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            if (error.status === 401) {
                _this.router.navigate(['auth/login']);
            }
            // TODO: handle 403 error ?
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
        }));
    };
    AuthInterceptor.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AuthInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/@auth/auth.module.ts":
/*!**************************************!*\
  !*** ./src/app/@auth/auth.module.ts ***!
  \**************************************/
/*! exports provided: filterInterceptorRequest, AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterInterceptorRequest", function() { return filterInterceptorRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _auth_interceptor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.interceptor */ "./src/app/@auth/auth.interceptor.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.guard */ "./src/app/@auth/auth.guard.ts");
/* harmony import */ var _auth_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth.pipe */ "./src/app/@auth/auth.pipe.ts");
/* harmony import */ var ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-multiselect-dropdown */ "./node_modules/ng-multiselect-dropdown/fesm5/ng-multiselect-dropdown.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _components_main_main_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/main/main.component */ "./src/app/@auth/components/main/main.component.ts");
/* harmony import */ var _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/contact-us/contact-us.component */ "./src/app/@auth/components/contact-us/contact-us.component.ts");
/* harmony import */ var _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/sign-up/sign-up.component */ "./src/app/@auth/components/sign-up/sign-up.component.ts");
/* harmony import */ var _components_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/about-us/about-us.component */ "./src/app/@auth/components/about-us/about-us.component.ts");
/* harmony import */ var _components_shoerestoration_shoerestoration_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/shoerestoration/shoerestoration.component */ "./src/app/@auth/components/shoerestoration/shoerestoration.component.ts");
/* harmony import */ var _components_shoeart_shoeart_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/shoeart/shoeart.component */ "./src/app/@auth/components/shoeart/shoeart.component.ts");
/* harmony import */ var _components_what_is_fixmykix_what_is_fixmykix_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/what-is-fixmykix/what-is-fixmykix.component */ "./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.ts");
/* harmony import */ var _layout_header_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../layout/header.component */ "./src/app/layout/header.component.ts");
/* harmony import */ var _layout_footer_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../layout/footer.component */ "./src/app/layout/footer.component.ts");
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components */ "./src/app/@auth/components/index.ts");
/* harmony import */ var _auth_routing_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./auth-routing.module */ "./src/app/@auth/auth-routing.module.ts");
/* harmony import */ var _isAuth_guard__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./isAuth.guard */ "./src/app/@auth/isAuth.guard.ts");






















var GUARDS = [_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]];
var PIPES = [_auth_pipe__WEBPACK_IMPORTED_MODULE_7__["AuthPipe"]];
var COMPONENTS = [
    _components__WEBPACK_IMPORTED_MODULE_19__["NgxLoginComponent"],
    _components__WEBPACK_IMPORTED_MODULE_19__["NgxAuthComponent"],
    _components__WEBPACK_IMPORTED_MODULE_19__["NgxRegisterComponent"],
    _components__WEBPACK_IMPORTED_MODULE_19__["NgxAuthBlockComponent"],
    _components_main_main_component__WEBPACK_IMPORTED_MODULE_10__["NgxMainComponent"],
    _components_contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_11__["NgxContactUsComponent"],
    _components_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_12__["NgxSignUpComponent"],
    _layout_header_component__WEBPACK_IMPORTED_MODULE_17__["HeaderComponent"],
    _layout_footer_component__WEBPACK_IMPORTED_MODULE_18__["FooterComponent"],
    _components_about_us_about_us_component__WEBPACK_IMPORTED_MODULE_13__["NgxAboutUsComponent"],
    _components_shoerestoration_shoerestoration_component__WEBPACK_IMPORTED_MODULE_14__["NgxShoeRestorationComponent"],
    _components_shoeart_shoeart_component__WEBPACK_IMPORTED_MODULE_15__["NgxShoeArtComponent"],
    _components_what_is_fixmykix_what_is_fixmykix_component__WEBPACK_IMPORTED_MODULE_16__["NgxFixmyKixComponent"]
];
function filterInterceptorRequest(req) {
    return ['/auth/login', '/auth/sign-up']
        .some(function (url) { return req.url.includes(url); });
}
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule_1 = AuthModule;
    AuthModule.forRoot = function () {
        return {
            ngModule: AuthModule_1,
            providers: [
                { useValue: filterInterceptorRequest },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"], useClass: _auth_interceptor__WEBPACK_IMPORTED_MODULE_5__["AuthInterceptor"], multi: true }
            ].concat(GUARDS)
        };
    };
    var AuthModule_1;
    AuthModule = AuthModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: PIPES.concat(COMPONENTS),
            imports: [
                _auth_routing_module__WEBPACK_IMPORTED_MODULE_20__["AuthRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                ng_multiselect_dropdown__WEBPACK_IMPORTED_MODULE_8__["NgMultiSelectDropDownModule"].forRoot()
            ],
            exports: PIPES.slice(),
            providers: [
                _isAuth_guard__WEBPACK_IMPORTED_MODULE_21__["IsAuthGuard"],
                _auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/@auth/auth.pipe.ts":
/*!************************************!*\
  !*** ./src/app/@auth/auth.pipe.ts ***!
  \************************************/
/*! exports provided: AuthPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthPipe", function() { return AuthPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */



var AuthPipe = /** @class */ (function () {
    function AuthPipe() {
    }
    AuthPipe.prototype.transform = function (url) {
        if (!url) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(url);
        }
    };
    AuthPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'ngxAuthToken' }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AuthPipe);
    return AuthPipe;
}());



/***/ }),

/***/ "./src/app/@auth/components/about-us/about-us.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/@auth/components/about-us/about-us.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n.about-bg {\n  background: #000 url('jordanshoesbanner.png') no-repeat fixed top -150px center/100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 32px;\n  line-height: 36px;\n  color: #fff;\n  letter-spacing: 2px;\n  font-weight: bold;\n  text-transform: uppercase;\n  padding: 150px 15px 50px;\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list li {\n  padding: 15px;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem #fff, 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.6);\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n}\n\n.about-shoe-servicec-list li + li {\n  margin: 20px 0 0 0;\n}\n\n.about-shoe-servicec-list li .about-shoe-text {\n  width: calc(100% - 415px);\n}\n\n.about-shoe-servicec-list li .about-shoe-text .h2-base-heading, .about-shoe-servicec-list li .about-shoe-text .img-right {\n  margin: 0 0 12px;\n}\n\n.about-shoe-servicec-list li .text-right a {\n  display: inline-block;\n  transition: all 0.5s;\n  color: #fff;\n  background: #000;\n  padding: 4px 12px;\n  border: 2px solid #000;\n}\n\n.about-shoe-servicec-list li .text-right a:hover {\n  background: #fff;\n  color: #000;\n  padding: 4px 18px;\n}\n\n.about-shoe-servicec-list li figure {\n  width: 415px;\n  height: 250px;\n  overflow: hidden;\n  padding: 0 0 0 15px;\n}\n\n.about-shoe-servicec-list li figure img {\n  width: 100%;\n  height: 250px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 4px;\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  .about-shoe-servicec-list li {\n    flex-direction: column;\n  }\n\n  .about-shoe-text {\n    width: 100% !important;\n    margin: 0 0 15px;\n    text-align: center;\n  }\n\n  .about-shoe-text .text-right {\n    text-align: center;\n  }\n\n  .about-shoe-servicec-list li figure {\n    width: 100%;\n    max-width: 400px;\n    padding: 0;\n    margin: 0 auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9hYm91dC11cy9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L3NyY1xcYXBwXFxAYXV0aFxcY29tcG9uZW50c1xcYWJvdXQtdXNcXGFib3V0LXVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2Fib3V0LXVzL2Fib3V0LXVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsOEJBQUE7QUNDRDs7QURDQTtFQUNDLG9GQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7QUNFRDs7QURBQTtFQUNFLGtCQUFBO0FDR0Y7O0FEREE7RUFDQyxhQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLDRGQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsOEJBQUE7QUNJRDs7QURGQTtFQUNDLGtCQUFBO0FDS0Q7O0FESEE7RUFDQyx5QkFBQTtBQ01EOztBREpBO0VBQ0MsZ0JBQUE7QUNPRDs7QURMQTtFQUNDLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0FDUUY7O0FETkE7RUFDQyxnQkFBQTtFQUNBLFdBQUE7RUFDQyxpQkFBQTtBQ1NGOztBRFBBO0VBQ0MsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDVUQ7O0FEUkE7RUFDQyxXQUFBO0VBQ0EsYUFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7RUFDQSxrQkFBQTtBQ1dEOztBRFRBLGtDQUFBOztBQUNBO0VBQ0M7SUFDQyxzQkFBQTtFQ1lBOztFRFZEO0lBQ0Msc0JBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VDYUE7O0VEWEQ7SUFDQyxrQkFBQTtFQ2NBOztFRFpEO0lBQ0MsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsVUFBQTtJQUNBLGNBQUE7RUNlQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9hYm91dC11cy9hYm91dC11cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItaW5pdGlhbCB7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjgpO1xyXG59XHJcbi5hYm91dC1iZyB7XHJcblx0YmFja2dyb3VuZDogIzAwMCB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIucG5nJykgbm8tcmVwZWF0IGZpeGVkIHRvcCAtMTUwcHggY2VudGVyIC8gMTAwJSA7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGZvbnQtc2l6ZTogMzJweDtcclxuXHRsaW5lLWhlaWdodDogMzZweDtcclxuXHRjb2xvcjogI2ZmZjtcclxuXHRsZXR0ZXItc3BhY2luZzogMnB4O1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0cGFkZGluZzogMTUwcHggMTVweCA1MHB4O1xyXG59XHJcbiNtYWluIHtcclxuICBwYWRkaW5nOiA0MHB4IDE1cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcblx0cGFkZGluZzogMTVweDtcclxuXHRib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsMCwwLDAuMDUpO1xyXG5cdGJvcmRlci1yYWRpdXM6IDJweDtcclxuXHRib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtICNmZmYsIDAuNXJlbSAwLjEwMHJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC42KTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpK2xpIHtcclxuXHRtYXJnaW46IDIwcHggMCAwIDA7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IHtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDE1cHgpO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaDItYmFzZS1oZWFkaW5nLCAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmltZy1yaWdodCB7XHJcblx0bWFyZ2luOiAwIDAgMTJweDtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC50ZXh0LXJpZ2h0IGEge1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHR0cmFuc2l0aW9uOiBhbGwgMC41cztcclxuICBjb2xvcjogI2ZmZjtcclxuICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gIHBhZGRpbmc6IDRweCAxMnB4O1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMDA7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAudGV4dC1yaWdodCBhOmhvdmVyIHtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG4gIHBhZGRpbmc6IDRweCAxOHB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcclxuXHR3aWR0aDogNDE1cHg7XHJcblx0aGVpZ2h0OiAyNTBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHBhZGRpbmc6IDAgMCAwIDE1cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUgaW1nIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRoZWlnaHQ6IDI1MHB4O1xyXG5cdG9iamVjdC1maXQ6IGNvdmVyO1xyXG5cdGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcblx0LmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdH1cclxuXHQuYWJvdXQtc2hvZS10ZXh0IHtcclxuXHRcdHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcblx0XHRtYXJnaW46IDAgMCAxNXB4O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdH1cclxuXHQuYWJvdXQtc2hvZS10ZXh0IC50ZXh0LXJpZ2h0IHtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHR9XHJcblx0LmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRtYXgtd2lkdGg6IDQwMHB4O1xyXG5cdFx0cGFkZGluZzogMDtcclxuXHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdH1cclxufSIsIi5oZWFkZXItaW5pdGlhbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC44KTtcbn1cblxuLmFib3V0LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMCB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLnBuZ1wiKSBuby1yZXBlYXQgZml4ZWQgdG9wIC0xNTBweCBjZW50ZXIvMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMzJweDtcbiAgbGluZS1oZWlnaHQ6IDM2cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcGFkZGluZzogMTUwcHggMTVweCA1MHB4O1xufVxuXG4jbWFpbiB7XG4gIHBhZGRpbmc6IDQwcHggMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSAjZmZmLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuNik7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgKyBsaSB7XG4gIG1hcmdpbjogMjBweCAwIDAgMDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDQxNXB4KTtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IC5oMi1iYXNlLWhlYWRpbmcsIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaW1nLXJpZ2h0IHtcbiAgbWFyZ2luOiAwIDAgMTJweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAudGV4dC1yaWdodCBhIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIHBhZGRpbmc6IDRweCAxMnB4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjMDAwO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC50ZXh0LXJpZ2h0IGE6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBjb2xvcjogIzAwMDtcbiAgcGFkZGluZzogNHB4IDE4cHg7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcbiAgd2lkdGg6IDQxNXB4O1xuICBoZWlnaHQ6IDI1MHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwYWRkaW5nOiAwIDAgMCAxNXB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgb2JqZWN0LWZpdDogY292ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09Ki9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAuYWJvdXQtc2hvZS10ZXh0IHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMCAwIDE1cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmFib3V0LXNob2UtdGV4dCAudGV4dC1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG5cbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1heC13aWR0aDogNDAwcHg7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/@auth/components/about-us/about-us.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/@auth/components/about-us/about-us.component.ts ***!
  \*****************************************************************/
/*! exports provided: NgxAboutUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxAboutUsComponent", function() { return NgxAboutUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NgxAboutUsComponent = /** @class */ (function () {
    function NgxAboutUsComponent() {
    }
    NgxAboutUsComponent.prototype.ngOnInit = function () { };
    NgxAboutUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-about-us',
            template: __webpack_require__(/*! raw-loader!./about-us.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/about-us/about-us.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./about-us.component.scss */ "./src/app/@auth/components/about-us/about-us.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NgxAboutUsComponent);
    return NgxAboutUsComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/auth-block/auth-block.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/@auth/components/auth-block/auth-block.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*!\n * Copyright (c) Akveo 2019. All Rights Reserved.\n * Licensed under the Single Application / Multi Application License.\n * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.\n */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9hdXRoLWJsb2NrL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFxhdXRoLWJsb2NrXFxhdXRoLWJsb2NrLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0VBQUEiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2F1dGgtYmxvY2svYXV0aC1ibG9jay5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBDb3B5cmlnaHQgKGMpIEFrdmVvIDIwMTkuIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBTaW5nbGUgQXBwbGljYXRpb24gLyBNdWx0aSBBcHBsaWNhdGlvbiBMaWNlbnNlLlxyXG4gKiBTZWUgTElDRU5TRV9TSU5HTEVfQVBQIC8gTElDRU5TRV9NVUxUSV9BUFAgaW4gdGhlICdkb2NzJyBmb2xkZXIgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24gb24gdHlwZSBvZiBwdXJjaGFzZWQgbGljZW5zZS5cclxuICovXHJcblxyXG4vLyA6aG9zdCB7XHJcbi8vICAgZGlzcGxheTogYmxvY2s7XHJcbi8vICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgbWF4LXdpZHRoOiAzNXJlbTtcclxuXHJcbi8vICAgOjpuZy1kZWVwIHtcclxuLy8gICAgIGZvcm0ge1xyXG4vLyAgICAgICB3aWR0aDogMTAwJTtcclxuLy8gICAgIH1cclxuXHJcbi8vICAgICAubGFiZWwge1xyXG4vLyAgICAgICBkaXNwbGF5OiBibG9jaztcclxuLy8gICAgICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG4vLyAgICAgfVxyXG5cclxuLy8gICAgIC5mb3Jnb3QtcGFzc3dvcmQge1xyXG4vLyAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbi8vICAgICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcclxuLy8gICAgIH1cclxuXHJcbi8vICAgICAuY2FwdGlvbiB7XHJcbi8vICAgICAgIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuLy8gICAgIH1cclxuXHJcbi8vICAgICAuYWxlcnQge1xyXG4vLyAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgLnRpdGxlIHtcclxuLy8gICAgICAgbWFyZ2luLXRvcDogMDtcclxuLy8gICAgICAgbWFyZ2luLWJvdHRvbTogMC43NXJlbTtcclxuLy8gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgfVxyXG5cclxuLy8gICAgIC5zdWItdGl0bGUge1xyXG4vLyAgICAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4vLyAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgLmZvcm0tY29udHJvbC1ncm91cCB7XHJcbi8vICAgICAgIG1hcmdpbi1ib3R0b206IDJyZW07XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgLmZvcm0tY29udHJvbC1ncm91cC5hY2NlcHQtZ3JvdXAge1xyXG4vLyAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbi8vICAgICAgIG1hcmdpbjogMnJlbSAwO1xyXG4vLyAgICAgfVxyXG5cclxuLy8gICAgIC5sYWJlbC13aXRoLWxpbmsge1xyXG4vLyAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgLmxpbmtzIHtcclxuLy8gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgICAgICBtYXJnaW4tdG9wOiAxLjc1cmVtO1xyXG5cclxuLy8gICAgICAgLnNvY2lhbHMge1xyXG4vLyAgICAgICAgIG1hcmdpbi10b3A6IDEuNXJlbTtcclxuLy8gICAgICAgfVxyXG5cclxuLy8gICAgICAgLnNvY2lhbHMgYSB7XHJcbi8vICAgICAgICAgbWFyZ2luOiAwIDFyZW07XHJcbi8vICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4vLyAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcblxyXG4vLyAgICAgICAgICYud2l0aC1pY29uIHtcclxuLy8gICAgICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcclxuLy8gICAgICAgICB9XHJcbi8vICAgICAgIH1cclxuLy8gICAgIH1cclxuXHJcbi8vICAgICAuYW5vdGhlci1hY3Rpb24ge1xyXG4vLyAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4vLyAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgLnNpZ24taW4tb3ItdXAge1xyXG4vLyAgICAgICBtYXJnaW4tdG9wOiAycmVtO1xyXG4vLyAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbi8vICAgICB9XHJcblxyXG4vLyAgICAgbmItYWxlcnQge1xyXG4vLyAgICAgICAuYWxlcnQtdGl0bGUsXHJcbi8vICAgICAgIC5hbGVydC1tZXNzYWdlIHtcclxuLy8gICAgICAgICBtYXJnaW46IDAgMCAwLjVyZW07XHJcbi8vICAgICAgIH1cclxuLy8gICAgICAgLmFsZXJ0LW1lc3NhZ2UtbGlzdCB7XHJcbi8vICAgICAgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4vLyAgICAgICAgIHBhZGRpbmc6IDA7XHJcbi8vICAgICAgICAgbWFyZ2luOiAwO1xyXG4vLyAgICAgICB9XHJcbi8vICAgICB9XHJcbi8vICAgfVxyXG4vLyB9XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/@auth/components/auth-block/auth-block.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/@auth/components/auth-block/auth-block.component.ts ***!
  \*********************************************************************/
/*! exports provided: NgxAuthBlockComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxAuthBlockComponent", function() { return NgxAuthBlockComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */


var NgxAuthBlockComponent = /** @class */ (function () {
    function NgxAuthBlockComponent() {
    }
    NgxAuthBlockComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-auth-block',
            template: "\n    <ng-content></ng-content>\n  ",
            styles: [__webpack_require__(/*! ./auth-block.component.scss */ "./src/app/@auth/components/auth-block/auth-block.component.scss")]
        })
    ], NgxAuthBlockComponent);
    return NgxAuthBlockComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/auth.component.scss":
/*!******************************************************!*\
  !*** ./src/app/@auth/components/auth.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*!\n * Copyright (c) Akveo 2019. All Rights Reserved.\n * Licensed under the Single Application / Multi Application License.\n * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.\n */\n/*!\n * @license\n * Copyright Akveo. All Rights Reserved.\n * Licensed under the MIT License. See License.txt in the project root for license information.\n */\n:host nb-card {\n  margin: 0;\n  height: calc(100vh - 2 * 2.5rem);\n}\n:host .navigation .link {\n  display: inline-block;\n  text-decoration: none;\n}\n:host .navigation .link nb-icon {\n  font-size: 2rem;\n  vertical-align: middle;\n}\n:host .links nb-icon {\n  font-size: 2.5rem;\n}\n:host nb-card-body {\n  display: flex;\n  width: 100%;\n}\n:host nb-auth-block {\n  margin: auto;\n}\n@media (max-width: 767.98px) {\n  :host nb-card {\n    border-radius: 0;\n    height: 100vh;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L3NyY1xcYXBwXFxAYXV0aFxcY29tcG9uZW50c1xcYXV0aC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L25vZGVfbW9kdWxlc1xcQG5lYnVsYXJcXHRoZW1lXFxzdHlsZXNcXGdsb2JhbFxcX2JyZWFrcG9pbnRzLnNjc3MiLCJzcmMvYXBwL0BhdXRoL2NvbXBvbmVudHMvYXV0aC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztFQUFBO0FDQUE7Ozs7RUFBQTtBRFVFO0VBQ0UsU0FBQTtFQUNBLGdDQUFBO0FFQ0o7QUZFRTtFQUNFLHFCQUFBO0VBQ0EscUJBQUE7QUVBSjtBRkVJO0VBQ0UsZUFBQTtFQUNBLHNCQUFBO0FFQU47QUZJRTtFQUNFLGlCQUFBO0FFRko7QUZLRTtFQUNFLGFBQUE7RUFDQSxXQUFBO0FFSEo7QUZNRTtFQUNFLFlBQUE7QUVKSjtBRHlCSTtFRGpCQTtJQUNFLGdCQUFBO0lBQ0EsYUFBQTtFRUxKO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2F1dGguY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQ29weXJpZ2h0IChjKSBBa3ZlbyAyMDE5LiBBbGwgUmlnaHRzIFJlc2VydmVkLlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgU2luZ2xlIEFwcGxpY2F0aW9uIC8gTXVsdGkgQXBwbGljYXRpb24gTGljZW5zZS5cclxuICogU2VlIExJQ0VOU0VfU0lOR0xFX0FQUCAvIExJQ0VOU0VfTVVMVElfQVBQIGluIHRoZSAnZG9jcycgZm9sZGVyIGZvciBsaWNlbnNlIGluZm9ybWF0aW9uIG9uIHR5cGUgb2YgcHVyY2hhc2VkIGxpY2Vuc2UuXHJcbiAqL1xyXG5cclxuQGltcG9ydCAnfkBuZWJ1bGFyL3RoZW1lL3N0eWxlcy9nbG9iYWwvYnJlYWtwb2ludHMnO1xyXG5cclxuOmhvc3Qge1xyXG4gICRhdXRoLWxheW91dC1wYWRkaW5nOiAyLjVyZW07XHJcbiAgbmItY2FyZCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAyICogI3skYXV0aC1sYXlvdXQtcGFkZGluZ30pO1xyXG4gIH1cclxuXHJcbiAgLm5hdmlnYXRpb24gLmxpbmsge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5cclxuICAgIG5iLWljb24ge1xyXG4gICAgICBmb250LXNpemU6IDJyZW07XHJcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAubGlua3MgbmItaWNvbiB7XHJcbiAgICBmb250LXNpemU6IDIuNXJlbTtcclxuICB9XHJcblxyXG4gIG5iLWNhcmQtYm9keSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICBuYi1hdXRoLWJsb2NrIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICB9XHJcblxyXG4gIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bihzbSkge1xyXG4gICAgbmItY2FyZCB7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyA6Om5nLWRlZXAge1xyXG4gIC8vICAgbmItbGF5b3V0IC5sYXlvdXQgLmxheW91dC1jb250YWluZXIgLmNvbnRlbnQgLmNvbHVtbnMgbmItbGF5b3V0LWNvbHVtbiB7XHJcbiAgLy8gICAgIHBhZGRpbmc6ICRhdXRoLWxheW91dC1wYWRkaW5nO1xyXG5cclxuICAvLyAgICAgQGluY2x1ZGUgbWVkaWEtYnJlYWtwb2ludC1kb3duKHNtKSB7XHJcbiAgLy8gICAgICAgcGFkZGluZzogMDtcclxuICAvLyAgICAgfVxyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxufVxyXG4iLCIvKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgQWt2ZW8uIEFsbCBSaWdodHMgUmVzZXJ2ZWQuXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIExpY2Vuc2UuIFNlZSBMaWNlbnNlLnR4dCBpbiB0aGUgcHJvamVjdCByb290IGZvciBsaWNlbnNlIGluZm9ybWF0aW9uLlxuICovXG5cbiRncmlkLWNvbHVtbnM6IDEyICFkZWZhdWx0O1xuJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2U6IDI0cHggIWRlZmF1bHQ7XG4kZ3JpZC1ndXR0ZXItd2lkdGhzOiAoXG4gIHhzOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgc206ICRncmlkLWd1dHRlci13aWR0aC1iYXNlLFxuICBtZDogJGdyaWQtZ3V0dGVyLXdpZHRoLWJhc2UsXG4gIGxnOiAkZ3JpZC1ndXR0ZXItd2lkdGgtYmFzZSxcbiAgeGw6ICRncmlkLWd1dHRlci13aWR0aC1iYXNlXG4pICFkZWZhdWx0O1xuXG5cbiRncmlkLWJyZWFrcG9pbnRzOiAoXG4gIHhzOiAwLFxuICBpczogNDAwcHgsXG4gIHNtOiA1NzZweCxcbiAgbWQ6IDc2OHB4LFxuICBsZzogOTkycHgsXG4gIHhsOiAxMjAwcHgsXG4gIHh4bDogMTQwMHB4LFxuICB4eHhsOiAxNjAwcHhcbik7XG5cbiRjb250YWluZXItbWF4LXdpZHRoczogKFxuICBpczogMzgwcHgsXG4gIHNtOiA1NDBweCxcbiAgbWQ6IDcyMHB4LFxuICBsZzogOTYwcHgsXG4gIHhsOiAxMTQwcHgsXG4gIHh4bDogMTMyMHB4LFxuICB4eHhsOiAxNTAwcHhcbik7XG5cbkBmdW5jdGlvbiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBtYXAtZ2V0KCRicmVha3BvaW50cywgJG5hbWUpO1xuICBAcmV0dXJuIGlmKCRtaW4gIT0gMCwgJG1pbiwgbnVsbCk7XG59XG5cbkBmdW5jdGlvbiBicmVha3BvaW50LW5leHQoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMsICRicmVha3BvaW50LW5hbWVzOiBtYXAta2V5cygkYnJlYWtwb2ludHMpKSB7XG4gICRuOiBpbmRleCgkYnJlYWtwb2ludC1uYW1lcywgJG5hbWUpO1xuICBAcmV0dXJuIGlmKCRuIDwgbGVuZ3RoKCRicmVha3BvaW50LW5hbWVzKSwgbnRoKCRicmVha3BvaW50LW5hbWVzLCAkbiArIDEpLCBudWxsKTtcbn1cblxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbWF4KCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRuZXh0OiBicmVha3BvaW50LW5leHQoJG5hbWUsICRicmVha3BvaW50cyk7XG4gIEByZXR1cm4gaWYoJG5leHQsIGJyZWFrcG9pbnQtbWluKCRuZXh0LCAkYnJlYWtwb2ludHMpIC0gMC4wMnB4LCBudWxsKTtcbn1cblxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWF4OiBicmVha3BvaW50LW1heCgkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQGlmICRtYXgge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAkbWF4KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbkBtaXhpbiBtZWRpYS1icmVha3BvaW50LXVwKCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRtaW46IGJyZWFrcG9pbnQtbWluKCRuYW1lLCAkYnJlYWtwb2ludHMpO1xuICBAaWYgJG1pbiB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICRtaW4pIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSB7XG4gICAgQGNvbnRlbnQ7XG4gIH1cbn1cbiIsIi8qIVxuICogQ29weXJpZ2h0IChjKSBBa3ZlbyAyMDE5LiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIFNpbmdsZSBBcHBsaWNhdGlvbiAvIE11bHRpIEFwcGxpY2F0aW9uIExpY2Vuc2UuXG4gKiBTZWUgTElDRU5TRV9TSU5HTEVfQVBQIC8gTElDRU5TRV9NVUxUSV9BUFAgaW4gdGhlICdkb2NzJyBmb2xkZXIgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24gb24gdHlwZSBvZiBwdXJjaGFzZWQgbGljZW5zZS5cbiAqL1xuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IEFrdmVvLiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlLiBTZWUgTGljZW5zZS50eHQgaW4gdGhlIHByb2plY3Qgcm9vdCBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbi5cbiAqL1xuOmhvc3QgbmItY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gMiAqIDIuNXJlbSk7XG59XG46aG9zdCAubmF2aWdhdGlvbiAubGluayB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuOmhvc3QgLm5hdmlnYXRpb24gLmxpbmsgbmItaWNvbiB7XG4gIGZvbnQtc2l6ZTogMnJlbTtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cbjpob3N0IC5saW5rcyBuYi1pY29uIHtcbiAgZm9udC1zaXplOiAyLjVyZW07XG59XG46aG9zdCBuYi1jYXJkLWJvZHkge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTAwJTtcbn1cbjpob3N0IG5iLWF1dGgtYmxvY2sge1xuICBtYXJnaW46IGF1dG87XG59XG5AbWVkaWEgKG1heC13aWR0aDogNzY3Ljk4cHgpIHtcbiAgOmhvc3QgbmItY2FyZCB7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/@auth/components/auth.component.ts":
/*!****************************************************!*\
  !*** ./src/app/@auth/components/auth.component.ts ***!
  \****************************************************/
/*! exports provided: NgxAuthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxAuthComponent", function() { return NgxAuthComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */


// <nb-layout>
// <nb-layout-column>
//   <nb-card>
//     <nb-card-header>
//       <nav class="navigation">
//         <a href="#" (click)="back()" class="link back-link" aria-label="Back">
//           <nb-icon icon="arrow-back"></nb-icon>
//         </a>
//       </nav>
//     </nb-card-header>
//     <nb-card-body>
//       <nb-auth-block>
//       </nb-auth-block>
//     </nb-card-body>
//   </nb-card>
// </nb-layout-column>
// </nb-layout>
var NgxAuthComponent = /** @class */ (function () {
    // showcase of how to use the onAuthenticationChange method
    function NgxAuthComponent(location) {
        this.location = location;
        this.alive = true;
        this.authenticated = false;
        this.token = '';
        // this.subscription = auth.onAuthenticationChange()
        //   .pipe(takeWhile(() => this.alive))
        //   .subscribe((authenticated: boolean) => {
        //     this.authenticated = authenticated;
        //   });
    }
    NgxAuthComponent.prototype.back = function () {
        this.location.back();
        return false;
    };
    NgxAuthComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NgxAuthComponent.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"] }
    ]; };
    NgxAuthComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-auth',
            template: __webpack_require__(/*! raw-loader!./auth.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/auth.component.html"),
            styles: [__webpack_require__(/*! ./auth.component.scss */ "./src/app/@auth/components/auth.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], NgxAuthComponent);
    return NgxAuthComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/constants.ts":
/*!***********************************************!*\
  !*** ./src/app/@auth/components/constants.ts ***!
  \***********************************************/
/*! exports provided: EMAIL_PATTERN, NUMBERS_PATTERN, DOUBLE_PATTERN */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EMAIL_PATTERN", function() { return EMAIL_PATTERN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NUMBERS_PATTERN", function() { return NUMBERS_PATTERN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOUBLE_PATTERN", function() { return DOUBLE_PATTERN; });
/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
var EMAIL_PATTERN = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
var NUMBERS_PATTERN = '^-?[0-9]+$';
var DOUBLE_PATTERN = '[+-]?([0-9]*[.])?[0-9]+';


/***/ }),

/***/ "./src/app/@auth/components/contact-us/contact-us.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/@auth/components/contact-us/contact-us.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container_contact {\n  width: 60%;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.contact-form-container {\n  padding-bottom: 25px;\n}\n\n.contact_text {\n  font-size: 50px;\n  padding-top: 50px;\n}\n\n.company_name {\n  font-size: 28px;\n  font-weight: bold;\n  padding-top: 25px;\n}\n\n.address_title {\n  font-size: 22px;\n  font-weight: bold;\n  padding-top: 20px;\n}\n\n.address {\n  font-size: 16px;\n  padding-top: 20px;\n  text-transform: capitalize;\n  font-weight: 500;\n  padding-bottom: 40px;\n  line-height: 170%;\n}\n\n.form_title {\n  font-size: 19px;\n  padding-top: 25px;\n  text-transform: uppercase;\n  font-weight: bold;\n}\n\n.contct_submit {\n  color: white !important;\n  background-color: #23b8b5 !important;\n  margin-bottom: 40px;\n}\n\n.contct_submit:hover {\n  color: white !important;\n  background-color: #106866 !important;\n  margin-bottom: 40px;\n}\n\n.contct_submit:active {\n  color: white !important;\n  background-color: #23b8b5 !important;\n  margin-bottom: 40px;\n}\n\n.form-group {\n  margin-top: 15px;\n  margin-bottom: 15px;\n  /*border: 1px solid black;*/\n}\n\n.svg_contact {\n  width: 20px;\n}\n\n.contact_email,\n.contact_mobile {\n  margin-top: 10px;\n}\n\n.contact_email > a {\n  color: black;\n  font-size: 16px;\n  font-weight: 500;\n  margin-left: 15px;\n}\n\n.contact_email > a:hover {\n  color: #23b8b5;\n  font-size: 16px;\n}\n\n.contact_mobile > a {\n  color: black;\n  font-size: 16px;\n  font-weight: 500;\n  margin-left: 8px;\n}\n\n.contact_mobile > a:first-of-type {\n  margin-left: 15px;\n}\n\n.contact_mobile > a:hover {\n  color: #23b8b5;\n  font-size: 16px;\n}\n\n.contact-form-container input[type=text],\n.contact-form-container input[type=email],\ntextarea {\n  font-weight: 500;\n}\n\n.contact-form-container input[type=text]:focus,\n.contact-form-container input[type=email]:focus,\ntextarea:focus {\n  border-bottom: 2px #23b8b5 solid;\n  font-weight: 500;\n  outline: 0;\n  background: none;\n}\n\n@media (max-width: 500px) {\n  .container_contact {\n    width: 80%;\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .company_name {\n    font-size: 23px;\n  }\n}\n\n@media (min-width: 500px) and (max-width: 1366px) {\n  .container_contact {\n    width: 80%;\n    margin-left: auto;\n    margin-right: auto;\n  }\n}\n\n@media screen and (max-width: 768px) {\n  .contact-form-container {\n    padding-top: 30px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9jb250YWN0LXVzL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFxjb250YWN0LXVzXFxjb250YWN0LXVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2NvbnRhY3QtdXMvY29udGFjdC11cy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDQ0Q7O0FEQ0E7RUFDQyxvQkFBQTtBQ0VEOztBREFBO0VBQ0ksZUFBQTtFQUNILGlCQUFBO0FDR0Q7O0FEREE7RUFDQyxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ0lEOztBRERBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNJRDs7QURGQTtFQUNDLGVBQUE7RUFDQSxpQkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FDS0Q7O0FESEE7RUFDQyxlQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0FDTUQ7O0FESkE7RUFDRSx1QkFBQTtFQUNBLG9DQUFBO0VBQ0EsbUJBQUE7QUNPRjs7QURKQTtFQUNFLHVCQUFBO0VBQ0Esb0NBQUE7RUFDQSxtQkFBQTtBQ09GOztBRExBO0VBQ0UsdUJBQUE7RUFDQSxvQ0FBQTtFQUNBLG1CQUFBO0FDUUY7O0FETkE7RUFDQyxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7QUNTRDs7QURQQTtFQUNDLFdBQUE7QUNVRDs7QURSQTs7RUFFQyxnQkFBQTtBQ1dEOztBRFRBO0VBQ0MsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDWUQ7O0FEVkE7RUFDQyxjQUFBO0VBQ0EsZUFBQTtBQ2FEOztBRFhBO0VBQ0MsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDY0Q7O0FEWkE7RUFDQyxpQkFBQTtBQ2VEOztBRGJBO0VBQ0MsY0FBQTtFQUNBLGVBQUE7QUNnQkQ7O0FEZEE7OztFQUdDLGdCQUFBO0FDaUJEOztBRGZBOzs7RUFHQyxnQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FDa0JEOztBRGZBO0VBQ0M7SUFDQyxVQUFBO0lBQ0EsaUJBQUE7SUFDRCxrQkFBQTtFQ2tCQzs7RURoQkQ7SUFDQyxlQUFBO0VDbUJBO0FBQ0Y7O0FEakJBO0VBQ0M7SUFDQyxVQUFBO0lBQ0EsaUJBQUE7SUFDQyxrQkFBQTtFQ21CRDtBQUNGOztBRGpCQTtFQUNDO0lBQ0MsaUJBQUE7RUNtQkE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2NvbXBvbmVudHMvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcl9jb250YWN0IHtcclxuXHR3aWR0aDogNjAlO1xyXG5cdG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG5cdG1hcmdpbi1yaWdodDogYXV0bztcclxufVxyXG4uY29udGFjdC1mb3JtLWNvbnRhaW5lciB7XHJcblx0cGFkZGluZy1ib3R0b206IDI1cHg7XHRcclxufVxyXG4uY29udGFjdF90ZXh0IHtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuXHRwYWRkaW5nLXRvcDogNTBweDtcclxufVxyXG4uY29tcGFueV9uYW1lIHtcclxuXHRmb250LXNpemU6IDI4cHg7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0cGFkZGluZy10b3A6IDI1cHg7XHJcbn1cclxuXHJcbi5hZGRyZXNzX3RpdGxlIHtcclxuXHRmb250LXNpemU6IDIycHg7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0cGFkZGluZy10b3A6IDIwcHg7XHJcbn1cclxuLmFkZHJlc3Mge1xyXG5cdGZvbnQtc2l6ZTogMTZweDtcclxuXHRwYWRkaW5nLXRvcDogMjBweDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuXHRmb250LXdlaWdodDogNTAwO1xyXG5cdHBhZGRpbmctYm90dG9tOiA0MHB4O1xyXG5cdGxpbmUtaGVpZ2h0OiAxNzAlO1xyXG59XHJcbi5mb3JtX3RpdGxlIHtcclxuXHRmb250LXNpemU6IDE5cHg7XHJcblx0cGFkZGluZy10b3A6IDI1cHg7XHJcblx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uY29udGN0X3N1Ym1pdHtcclxuXHQgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcblx0IGJhY2tncm91bmQtY29sb3I6ICMyM2I4YjUgIWltcG9ydGFudDtcclxuXHQgbWFyZ2luLWJvdHRvbTogNDBweDtcclxuXHJcbn1cclxuLmNvbnRjdF9zdWJtaXQ6aG92ZXJ7XHJcblx0IGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG5cdCBiYWNrZ3JvdW5kLWNvbG9yOiAjMTA2ODY2ICFpbXBvcnRhbnQ7XHJcblx0IG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbn1cclxuLmNvbnRjdF9zdWJtaXQ6YWN0aXZle1xyXG5cdCBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuXHQgYmFja2dyb3VuZC1jb2xvcjogIzIzYjhiNSAhaW1wb3J0YW50O1xyXG5cdCBtYXJnaW4tYm90dG9tOiA0MHB4O1xyXG59XHJcbi5mb3JtLWdyb3VwIHtcclxuXHRtYXJnaW4tdG9wOiAxNXB4O1xyXG5cdG1hcmdpbi1ib3R0b206IDE1cHg7XHJcblx0Lypib3JkZXI6IDFweCBzb2xpZCBibGFjazsqL1xyXG59XHJcbi5zdmdfY29udGFjdCB7XHJcblx0d2lkdGg6IDIwcHg7XHJcbn1cclxuLmNvbnRhY3RfZW1haWwsIFxyXG4uY29udGFjdF9tb2JpbGV7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxufVxyXG4uY29udGFjdF9lbWFpbD5hIHtcclxuXHRjb2xvcjogYmxhY2s7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0bWFyZ2luLWxlZnQ6IDE1cHg7XHJcbn1cclxuLmNvbnRhY3RfZW1haWw+YTpob3ZlciB7XHJcblx0Y29sb3I6ICMyM2I4YjU7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG59XHJcbi5jb250YWN0X21vYmlsZT5hIHtcclxuXHRjb2xvcjogYmxhY2s7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxufVxyXG4uY29udGFjdF9tb2JpbGU+YTpmaXJzdC1vZi10eXBlIHtcclxuXHRtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG4uY29udGFjdF9tb2JpbGU+YTpob3ZlciB7XHJcblx0Y29sb3I6ICMyM2I4YjU7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG59XHJcbi5jb250YWN0LWZvcm0tY29udGFpbmVyIGlucHV0W3R5cGU9XCJ0ZXh0XCJdLFxyXG4uY29udGFjdC1mb3JtLWNvbnRhaW5lciBpbnB1dFt0eXBlPVwiZW1haWxcIl0sXHJcbnRleHRhcmVhIHtcclxuXHRmb250LXdlaWdodDogNTAwO1xyXG59XHJcbi5jb250YWN0LWZvcm0tY29udGFpbmVyIGlucHV0W3R5cGU9XCJ0ZXh0XCJdOmZvY3VzLFxyXG4uY29udGFjdC1mb3JtLWNvbnRhaW5lciBpbnB1dFt0eXBlPVwiZW1haWxcIl06Zm9jdXMsXHJcbnRleHRhcmVhOmZvY3VzIHtcclxuXHRib3JkZXItYm90dG9tOiAycHggIzIzYjhiNSBzb2xpZDtcclxuXHRmb250LXdlaWdodDogNTAwO1xyXG5cdG91dGxpbmU6IDA7XHJcblx0YmFja2dyb3VuZDogbm9uZTtcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDUwMHB4KSB7XHJcblx0LmNvbnRhaW5lcl9jb250YWN0IHtcclxuXHRcdHdpZHRoOiA4MCU7XHJcblx0XHRtYXJnaW4tbGVmdDogYXV0bztcclxuXHRtYXJnaW4tcmlnaHQ6IGF1dG87XHJcblx0fVxyXG5cdC5jb21wYW55X25hbWUge1xyXG5cdFx0Zm9udC1zaXplOiAyM3B4O1xyXG5cdH1cclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDo1MDBweCkgYW5kIChtYXgtd2lkdGg6IDEzNjZweCkge1xyXG5cdC5jb250YWluZXJfY29udGFjdCB7XHJcblx0XHR3aWR0aDogODAlO1xyXG5cdFx0bWFyZ2luLWxlZnQ6IGF1dG87XHJcblx0ICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcblx0fVxyXG59XHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcblx0LmNvbnRhY3QtZm9ybS1jb250YWluZXIge1xyXG5cdFx0cGFkZGluZy10b3A6IDMwcHg7XHJcblx0fVxyXG59IiwiLmNvbnRhaW5lcl9jb250YWN0IHtcbiAgd2lkdGg6IDYwJTtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLmNvbnRhY3QtZm9ybS1jb250YWluZXIge1xuICBwYWRkaW5nLWJvdHRvbTogMjVweDtcbn1cblxuLmNvbnRhY3RfdGV4dCB7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgcGFkZGluZy10b3A6IDUwcHg7XG59XG5cbi5jb21wYW55X25hbWUge1xuICBmb250LXNpemU6IDI4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLXRvcDogMjVweDtcbn1cblxuLmFkZHJlc3NfdGl0bGUge1xuICBmb250LXNpemU6IDIycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLXRvcDogMjBweDtcbn1cblxuLmFkZHJlc3Mge1xuICBmb250LXNpemU6IDE2cHg7XG4gIHBhZGRpbmctdG9wOiAyMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgcGFkZGluZy1ib3R0b206IDQwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxNzAlO1xufVxuXG4uZm9ybV90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTlweDtcbiAgcGFkZGluZy10b3A6IDI1cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uY29udGN0X3N1Ym1pdCB7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjNiOGI1ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XG59XG5cbi5jb250Y3Rfc3VibWl0OmhvdmVyIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxMDY4NjYgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cblxuLmNvbnRjdF9zdWJtaXQ6YWN0aXZlIHtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyM2I4YjUgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogNDBweDtcbn1cblxuLmZvcm0tZ3JvdXAge1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICAvKmJvcmRlcjogMXB4IHNvbGlkIGJsYWNrOyovXG59XG5cbi5zdmdfY29udGFjdCB7XG4gIHdpZHRoOiAyMHB4O1xufVxuXG4uY29udGFjdF9lbWFpbCxcbi5jb250YWN0X21vYmlsZSB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG5cbi5jb250YWN0X2VtYWlsID4gYSB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW4tbGVmdDogMTVweDtcbn1cblxuLmNvbnRhY3RfZW1haWwgPiBhOmhvdmVyIHtcbiAgY29sb3I6ICMyM2I4YjU7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLmNvbnRhY3RfbW9iaWxlID4gYSB7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uY29udGFjdF9tb2JpbGUgPiBhOmZpcnN0LW9mLXR5cGUge1xuICBtYXJnaW4tbGVmdDogMTVweDtcbn1cblxuLmNvbnRhY3RfbW9iaWxlID4gYTpob3ZlciB7XG4gIGNvbG9yOiAjMjNiOGI1O1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5jb250YWN0LWZvcm0tY29udGFpbmVyIGlucHV0W3R5cGU9dGV4dF0sXG4uY29udGFjdC1mb3JtLWNvbnRhaW5lciBpbnB1dFt0eXBlPWVtYWlsXSxcbnRleHRhcmVhIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmNvbnRhY3QtZm9ybS1jb250YWluZXIgaW5wdXRbdHlwZT10ZXh0XTpmb2N1cyxcbi5jb250YWN0LWZvcm0tY29udGFpbmVyIGlucHV0W3R5cGU9ZW1haWxdOmZvY3VzLFxudGV4dGFyZWE6Zm9jdXMge1xuICBib3JkZXItYm90dG9tOiAycHggIzIzYjhiNSBzb2xpZDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgb3V0bGluZTogMDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDUwMHB4KSB7XG4gIC5jb250YWluZXJfY29udGFjdCB7XG4gICAgd2lkdGg6IDgwJTtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIH1cblxuICAuY29tcGFueV9uYW1lIHtcbiAgICBmb250LXNpemU6IDIzcHg7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA1MDBweCkgYW5kIChtYXgtd2lkdGg6IDEzNjZweCkge1xuICAuY29udGFpbmVyX2NvbnRhY3Qge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuY29udGFjdC1mb3JtLWNvbnRhaW5lciB7XG4gICAgcGFkZGluZy10b3A6IDMwcHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/components/contact-us/contact-us.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/@auth/components/contact-us/contact-us.component.ts ***!
  \*********************************************************************/
/*! exports provided: NgxContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxContactUsComponent", function() { return NgxContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");






var NgxContactUsComponent = /** @class */ (function () {
    function NgxContactUsComponent(utilsService, cd, fb, router, authenticationService) {
        this.utilsService = utilsService;
        this.cd = cd;
        this.fb = fb;
        this.router = router;
        this.authenticationService = authenticationService;
    }
    NgxContactUsComponent.prototype.ngOnInit = function () { };
    NgxContactUsComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] }
    ]; };
    NgxContactUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-contact-us',
            template: __webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/contact-us/contact-us.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./contact-us.component.scss */ "./src/app/@auth/components/contact-us/contact-us.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], NgxContactUsComponent);
    return NgxContactUsComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/index.ts":
/*!*******************************************!*\
  !*** ./src/app/@auth/components/index.ts ***!
  \*******************************************/
/*! exports provided: NgxLoginComponent, NgxAuthBlockComponent, NgxRegisterComponent, NgxAuthComponent, EMAIL_PATTERN, NUMBERS_PATTERN, DOUBLE_PATTERN */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login/login.component */ "./src/app/@auth/components/login/login.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NgxLoginComponent", function() { return _login_login_component__WEBPACK_IMPORTED_MODULE_0__["NgxLoginComponent"]; });

/* harmony import */ var _auth_block_auth_block_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-block/auth-block.component */ "./src/app/@auth/components/auth-block/auth-block.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NgxAuthBlockComponent", function() { return _auth_block_auth_block_component__WEBPACK_IMPORTED_MODULE_1__["NgxAuthBlockComponent"]; });

/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register/register.component */ "./src/app/@auth/components/register/register.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NgxRegisterComponent", function() { return _register_register_component__WEBPACK_IMPORTED_MODULE_2__["NgxRegisterComponent"]; });

/* harmony import */ var _auth_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.component */ "./src/app/@auth/components/auth.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NgxAuthComponent", function() { return _auth_component__WEBPACK_IMPORTED_MODULE_3__["NgxAuthComponent"]; });

/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./constants */ "./src/app/@auth/components/constants.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EMAIL_PATTERN", function() { return _constants__WEBPACK_IMPORTED_MODULE_4__["EMAIL_PATTERN"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NUMBERS_PATTERN", function() { return _constants__WEBPACK_IMPORTED_MODULE_4__["NUMBERS_PATTERN"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DOUBLE_PATTERN", function() { return _constants__WEBPACK_IMPORTED_MODULE_4__["DOUBLE_PATTERN"]; });

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */







/***/ }),

/***/ "./src/app/@auth/components/login/login.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/@auth/components/login/login.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n.login-page {\n  background: #000 url('jordanshoesbanner.png') no-repeat fixed center center/cover;\n  width: 100%;\n  height: 100vh;\n  box-sizing: border-box;\n  padding: 75px 30px 30px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.center-form {\n  padding: 20px 15px;\n  background: rgba(255, 255, 255, 0.84);\n  border-radius: 4px;\n  width: 400px;\n}\n\n.center-form form {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 20px 0 0 0;\n}\n\n.center-form form .form-group {\n  width: 100%;\n  margin: 0 0% 20px;\n}\n\n.check-user {\n  width: 100% !important;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.check-user label {\n  display: flex;\n  align-items: center;\n  margin: 0 4% 0 0;\n  font-size: 16px;\n  letter-spacing: 1px;\n}\n\n.check-user label input {\n  margin: 2px 6px 0 0;\n}\n\n.login-button {\n  width: 100% !important;\n  text-align: center;\n}\n\n.login-button button {\n  padding: 0 15px;\n  height: 34px;\n  background: #000;\n  color: #fff;\n  border: none;\n  border-radius: 0;\n  line-height: 33px;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  cursor: pointer;\n  width: 120px;\n}\n\n.backlinks {\n  text-align: center;\n  width: 100% !important;\n}\n\n.backlinks a {\n  color: #0f5fde;\n  font-size: 12px;\n  text-transform: uppercase;\n  font-weight: bold;\n}\n\n/* Responsive ====================*/\n\n@media screen and (max-width: 768px) {\n  .login-page {\n    min-height: 100vh;\n    height: 100%;\n  }\n}\n\n@media screen and (max-width: 500px) {\n  .center-form {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9sb2dpbi9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L3NyY1xcYXBwXFxAYXV0aFxcY29tcG9uZW50c1xcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsOEJBQUE7QUNDRDs7QURDQTtFQUNDLGlGQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNFRDs7QURBQTtFQUNDLGtCQUFBO0VBQ0EscUNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNHRDs7QUREQTtFQUNDLGFBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNJRDs7QURGQTtFQUNDLFdBQUE7RUFDQSxpQkFBQTtBQ0tEOztBREhBO0VBQ0Msc0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQ01EOztBREpBO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNPRDs7QURMQTtFQUNDLG1CQUFBO0FDUUQ7O0FETkE7RUFDQyxzQkFBQTtFQUNBLGtCQUFBO0FDU0Q7O0FEUEE7RUFDQyxlQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDVUQ7O0FEUkE7RUFDQyxrQkFBQTtFQUNBLHNCQUFBO0FDV0Q7O0FEVEE7RUFDQyxjQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7QUNZRDs7QURWQSxtQ0FBQTs7QUFDQTtFQUNBO0lBQ0MsaUJBQUE7SUFDQSxZQUFBO0VDYUM7QUFDRjs7QURYQTtFQUNBO0lBQ0MsV0FBQTtFQ2FDO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlci1pbml0aWFsIHtcclxuXHRiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuOCk7XHJcbn1cclxuLmxvZ2luLXBhZ2Uge1xyXG5cdGJhY2tncm91bmQ6ICMwMDAgdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLnBuZycpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXIgY2VudGVyIC8gY292ZXIgO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGhlaWdodDogMTAwdmg7XHJcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHRwYWRkaW5nOiA3NXB4IDMwcHggMzBweDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLmNlbnRlci1mb3JtIHtcclxuXHRwYWRkaW5nOiAyMHB4IDE1cHg7XHJcblx0YmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjg0KTtcclxuXHRib3JkZXItcmFkaXVzOiA0cHg7XHJcblx0d2lkdGg6IDQwMHB4O1xyXG59XHJcbi5jZW50ZXItZm9ybSBmb3JtIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtd3JhcDogd3JhcDtcclxuXHRtYXJnaW46IDIwcHggMCAwIDA7XHJcbn1cclxuLmNlbnRlci1mb3JtIGZvcm0gLmZvcm0tZ3JvdXAge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdG1hcmdpbjogMCAwJSAyMHB4O1xyXG59XHJcbi5jaGVjay11c2VyIHtcclxuXHR3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uY2hlY2stdXNlciBsYWJlbCB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdG1hcmdpbjogMCA0JSAwIDA7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbn1cclxuLmNoZWNrLXVzZXIgbGFiZWwgaW5wdXQge1xyXG5cdG1hcmdpbjogMnB4IDZweCAwIDA7XHJcbn1cclxuLmxvZ2luLWJ1dHRvbiB7XHJcblx0d2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmxvZ2luLWJ1dHRvbiBidXR0b24ge1xyXG5cdHBhZGRpbmc6IDAgMTVweDtcclxuXHRoZWlnaHQ6IDM0cHg7XHJcblx0YmFja2dyb3VuZDogIzAwMDtcclxuXHRjb2xvcjogI2ZmZjtcclxuXHRib3JkZXI6IG5vbmU7XHJcblx0Ym9yZGVyLXJhZGl1czogMDtcclxuXHRsaW5lLWhlaWdodDogMzNweDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG5cdGxldHRlci1zcGFjaW5nOiAxcHg7XHJcblx0Y3Vyc29yOiBwb2ludGVyO1xyXG5cdHdpZHRoOiAxMjBweDtcclxufVxyXG4uYmFja2xpbmtzIHtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0d2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxufVxyXG4uYmFja2xpbmtzIGEge1xyXG5cdGNvbG9yOiAjMGY1ZmRlO1xyXG5cdGZvbnQtc2l6ZTogMTJweDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcbi5sb2dpbi1wYWdlIHtcclxuXHRtaW4taGVpZ2h0OiAxMDB2aDtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcbn1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCl7XHJcbi5jZW50ZXItZm9ybSB7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn1cclxufSIsIi5oZWFkZXItaW5pdGlhbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC44KTtcbn1cblxuLmxvZ2luLXBhZ2Uge1xuICBiYWNrZ3JvdW5kOiAjMDAwIHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIucG5nXCIpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXIgY2VudGVyL2NvdmVyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcGFkZGluZzogNzVweCAzMHB4IDMwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uY2VudGVyLWZvcm0ge1xuICBwYWRkaW5nOiAyMHB4IDE1cHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44NCk7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgd2lkdGg6IDQwMHB4O1xufVxuXG4uY2VudGVyLWZvcm0gZm9ybSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgbWFyZ2luOiAyMHB4IDAgMCAwO1xufVxuXG4uY2VudGVyLWZvcm0gZm9ybSAuZm9ybS1ncm91cCB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDAgMCUgMjBweDtcbn1cblxuLmNoZWNrLXVzZXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmNoZWNrLXVzZXIgbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDAgNCUgMCAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5cbi5jaGVjay11c2VyIGxhYmVsIGlucHV0IHtcbiAgbWFyZ2luOiAycHggNnB4IDAgMDtcbn1cblxuLmxvZ2luLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ2luLWJ1dHRvbiBidXR0b24ge1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGhlaWdodDogMzRweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgbGluZS1oZWlnaHQ6IDMzcHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgd2lkdGg6IDEyMHB4O1xufVxuXG4uYmFja2xpbmtzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4uYmFja2xpbmtzIGEge1xuICBjb2xvcjogIzBmNWZkZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAubG9naW4tcGFnZSB7XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xuICAuY2VudGVyLWZvcm0ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/@auth/components/login/login.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/@auth/components/login/login.component.ts ***!
  \***********************************************************/
/*! exports provided: NgxLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxLoginComponent", function() { return NgxLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");
/* harmony import */ var _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../@core/backend/common/services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var NgxLoginComponent = /** @class */ (function () {
    function NgxLoginComponent(utilsService, cd, fb, router, authenticationService, UsersService, toasterService) {
        this.utilsService = utilsService;
        this.cd = cd;
        this.fb = fb;
        this.router = router;
        this.authenticationService = authenticationService;
        this.UsersService = UsersService;
        this.toasterService = toasterService;
        this.formGroup = this.fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    NgxLoginComponent.prototype.ngOnInit = function () { };
    NgxLoginComponent.prototype.login = function () {
        var _this = this;
        var user_details = { user: this.formGroup.value };
        this.authenticationService.login(user_details).subscribe(function (result) {
            if (result.status) {
                localStorage.setItem('token', result.data.registration_token);
                _this.toasterService.success("Login successfully !!", result.message);
                localStorage.setItem('userid', result.data.id);
                localStorage.setItem('user', JSON.stringify(result.data));
                localStorage.setItem('role', result.data.role);
                window.location.href = "";
                // this.router.navigate(['']);
            }
            else {
                _this.toasterService.error("Login unsuccessful !!", result.message);
            }
        });
    };
    ;
    NgxLoginComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] }
    ]; };
    NgxLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/login/login.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/@auth/components/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], NgxLoginComponent);
    return NgxLoginComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/main/main.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/@auth/components/main/main.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".big-banner {\n  display: flex;\n  align-items: center;\n  height: 100vh;\n  box-sizing: border-box;\n  padding-top: 100px;\n}\n\n.big-banner .col-md-6 {\n  text-transform: uppercase;\n}\n\n.big-banner .col-md-6 h1 {\n  font-weight: bold;\n  margin: 0 0 3px;\n  font-size: 45px;\n}\n\n.big-banner .col-md-6 h4 {\n  font-size: 22px;\n  font-family: \"Comfortaa\", cursive;\n}\n\n.big-banner .col-md-6 p {\n  margin: 5px 0 18px;\n  font-size: 12px;\n  font-family: \"Comfortaa\", cursive;\n  letter-spacing: 0.5px;\n  color: #000;\n  font-weight: bold;\n}\n\n.big-banner .col-md-6 a {\n  display: inline-block;\n  padding: 7px 10px 6px;\n  border: 1px solid #000;\n  background: #000;\n  color: #fff;\n  font-size: 10px;\n  font-family: \"Comfortaa\", cursive;\n  letter-spacing: 0.5px;\n}\n\n.custom-services {\n  margin: 0 0 50px;\n}\n\n.custom-services .col-md-4 a {\n  box-shadow: 0 0 10px -9px #666;\n  display: block;\n}\n\n.custom-services .col-md-4 a:hover {\n  box-shadow: 0 0 10px -7px #666;\n}\n\n.custom-services .col-md-4 figure img, .custom-services .col-md-3 figure img {\n  transition: all 1s;\n  -o-object-fit: contain;\n     object-fit: contain;\n  width: 100%;\n  height: 240px;\n}\n\n.custom-services .col-md-4 a:hover figure img, .custom-services .col-md-3 a:hover figure img, .custom-services .col-md-3:hover figure img {\n  -webkit-transform: scale(1.2);\n          transform: scale(1.2);\n}\n\n.custom-services .col-md-3, .custom-services .col-md-2 {\n  text-align: center;\n}\n\n.custom-services .col-md-3 a, .custom-services .col-md-2 a {\n  display: inline-block;\n  margin: 12px 0;\n  font-family: auto;\n  color: #000;\n}\n\n.custom-services .col-md-3 a:hover, .custom-services .col-md-2 a:hover {\n  text-decoration: underline;\n}\n\n.custom-services .col-md-3 p, .custom-services .col-md-2 p {\n  font-weight: bold;\n  color: #000;\n  text-transform: uppercase;\n  display: flex;\n  align-items: flex-start;\n  justify-content: center;\n}\n\n.custom-services .col-md-3 p img, .custom-services .col-md-2 p img {\n  margin: 0 6px 0 0;\n  width: 20px;\n}\n\n.custom-services .col-md-3 iframe {\n  width: 100%;\n  border: none;\n  height: 180px;\n  background: #000;\n}\n\n.see-more {\n  border: 1px solid #000;\n  padding: 10px 15px;\n  color: #000;\n  text-transform: uppercase;\n  display: inline-block;\n  margin: 30px 0 0 0;\n  font-size: 20px;\n}\n\n.see-more:hover {\n  background: #000;\n  color: #fff;\n}\n\n.bg-image-text {\n  background: url('bg_text.jpg') no-repeat scroll top center/cover;\n  padding: 50px 20px;\n  color: #fff;\n  font-weight: bold;\n  font-family: auto;\n}\n\n.bg-image-text h1 {\n  font-size: 45px;\n}\n\n.bg-image-text h2 {\n  margin: 20px 0;\n  font-size: 32px;\n}\n\n.bg-image-text h3 {\n  margin: 5px 0;\n  font-size: 26px;\n}\n\n.hash-tag {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  padding: 50px 20px;\n  height: 400px;\n  margin: 0 0 40px;\n  font-family: \"Comfortaa\", cursive;\n  text-transform: uppercase;\n  /*background: #242420;*/\n  background: url('hash-tag.jpg') no-repeat scroll center center/cover;\n}\n\n.hash-tag span {\n  display: inline-block;\n  padding: 15px 5px 5px;\n  color: #fff;\n  background: #000;\n  font-size: 90px;\n  line-height: 90px;\n}\n\n.brand-name-loaction a {\n  font-family: \"Montserrat\", sans-serif !important;\n  font-size: 13px;\n  height: 20px;\n  line-height: 20px;\n  color: #242020;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  margin: 0 0 !important;\n}\n\n.custom-services.custom-services-providers .col-md-2 figure img {\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9tYWluL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFxtYWluXFxtYWluLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL21haW4vbWFpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFrVEE7RUFDQyxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQ2pURDs7QURtVEE7RUFDQyx5QkFBQTtBQ2hURDs7QURrVEE7RUFDQyxpQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDL1NEOztBRGlUQTtFQUNDLGVBQUE7RUFDQSxpQ0FBQTtBQzlTRDs7QURnVEE7RUFDRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FDN1NGOztBRCtTQTtFQUNDLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQyxpQ0FBQTtFQUNBLHFCQUFBO0FDNVNGOztBRDhTQTtFQUNDLGdCQUFBO0FDM1NEOztBRDZTQTtFQUNDLDhCQUFBO0VBQ0EsY0FBQTtBQzFTRDs7QUQ0U0E7RUFDQyw4QkFBQTtBQ3pTRDs7QUQyU0E7RUFDQyxrQkFBQTtFQUNBLHNCQUFBO0tBQUEsbUJBQUE7RUFDRyxXQUFBO0VBQ0EsYUFBQTtBQ3hTSjs7QUQwU0E7RUFDQyw2QkFBQTtVQUFBLHFCQUFBO0FDdlNEOztBRHlTQTtFQUNDLGtCQUFBO0FDdFNEOztBRHdTQTtFQUNDLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQ3JTRDs7QUR1U0E7RUFDQywwQkFBQTtBQ3BTRDs7QURzU0E7RUFDQyxpQkFBQTtFQUNBLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0FDblNEOztBRHFTQTtFQUNDLGlCQUFBO0VBQ0EsV0FBQTtBQ2xTRDs7QURvU0E7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtBQ2pTRDs7QURtU0E7RUFDQyxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNoU0Q7O0FEa1NBO0VBQ0MsZ0JBQUE7RUFDQSxXQUFBO0FDL1JEOztBRGlTQTtFQUNDLGdFQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQzlSRDs7QURnU0E7RUFDQyxlQUFBO0FDN1JEOztBRCtSQTtFQUNDLGNBQUE7RUFDQSxlQUFBO0FDNVJEOztBRDhSQTtFQUNDLGFBQUE7RUFDQSxlQUFBO0FDM1JEOztBRDZSQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQyxpQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxvRUFBQTtBQzFSRjs7QUQ0UkE7RUFDQyxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDelJEOztBRDJSQTtFQUNDLGdEQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQ3hSRDs7QUQwUkE7RUFDQyxrQkFBQTtBQ3ZSRCIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2NvbXBvbmVudHMvbWFpbi9tYWluLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gI3NsaWRlciAuY2Fyb3VzZWwtaXRlbSB7XHJcbi8vICAgaGVpZ2h0OiA1MDBweDtcclxuLy8gfVxyXG5cclxuLy8gLmNhcm91c2VsLWNhcHRpb24ge1xyXG4vLyAgIGZvbnQtc2l6ZTogMThweDtcclxuLy8gfVxyXG5cclxuLy8gLmNhcm91c2VsLWl0ZW0+aW1nIHtcclxuLy8gICB3aWR0aDogMTAwJVxyXG4vLyB9XHJcblxyXG4vLyAuc2hvZXMtc2hvcGVyIC5zaG9lLXNlcnZpY2VjLWxpc3Qge1xyXG4vLyAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgZmxleC13cmFwOiB3cmFwO1xyXG4vLyAgIC8qbWFyZ2luOiAwIC0yJTsqL1xyXG4vLyB9XHJcbi8vIC5zaG9lcy1zaG9wZXIgLnNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcbi8vICAgd2lkdGg6IDQ2JTtcclxuLy8gICBtYXJnaW46IDAgMiUgNDBweDtcclxuLy8gICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbi8vICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuLy8gICAvKm92ZXJmbG93OiBoaWRkZW47Ki9cclxuLy8gICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vIH1cclxuLy8gLm9uZS10aGlyZCB7XHJcbi8vICAgd2lkdGg6IDI5LjMzJSAhaW1wb3J0YW50O1xyXG4vLyAgIG1hcmdpbjogMCAyJSA2MHB4ICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGgxIHtcclxuLy8gICBtYXJnaW46IDUwcHggMCAxNXB4O1xyXG4vLyB9XHJcbi8vIC5zaG9lcy1zaG9wZXIgLnNob2Utc2VydmljZWMtbGlzdCBsaSBwIHtcclxuLy8gICBvdmVyZmxvdzogaGlkZGVuO1xyXG4vLyAgIGZvbnQtc2l6ZTogMTRweDtcclxuLy8gfVxyXG4vLyAuc2hvZXMtc2hvcGVyIC5zaG9lLXNlcnZpY2VjLWxpc3QgbGkgcCBhIHtcclxuLy8gICBmb250LXdlaWdodDogYm9sZDtcclxuLy8gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbi8vICAgY29sb3I6ICNmZTQ5NjA7XHJcbi8vICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuLy8gICBtYXJnaW46IDVweCAwIDAgMDtcclxuLy8gICBmb250LXNpemU6IDEzcHg7XHJcbi8vICAgbGluZS1oZWlnaHQ6IDE2cHg7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHAgYSAuZmEge1xyXG4vLyAgIGZsb2F0OiBsZWZ0O1xyXG4vLyAgIG1hcmdpbjogMi4ycHggMHB4IDAgMDtcclxuLy8gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHAgaW1nIHtcclxuLy8gICBmbG9hdDogbGVmdDtcclxuLy8gICBtYXJnaW46IDAgMTVweCAxMHB4IDA7XHJcbi8vICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbi8vICAgaGVpZ2h0OiAyNDBweDtcclxuLy8gfVxyXG4vLyAuc2hvZXMtc2hvcGVyIC5zaG9lLXNlcnZpY2VjLWxpc3QgbGkgcC5pbWctcmlnaHQgaW1nIHtcclxuLy8gICBmbG9hdDogcmlnaHQ7XHJcbi8vICAgbWFyZ2luOiAwIDAgMTBweCAxNXB4O1xyXG4vLyB9XHJcbi8vIC5zcGVjaWFsLXRleHQge1xyXG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDAuOTApO1xyXG4vLyAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbi8vICAgYm94LXNoYWRvdzogMCAwIDEwcHggLTZweCAjMDAwO1xyXG4vLyAgIHJpZ2h0OiA1cHg7XHJcbi8vICAgYm90dG9tOiAtODBweDtcclxuLy8gICBjb2xvcjogI2ZmZjtcclxuLy8gICB3aWR0aDogMTcwcHg7XHJcbi8vICAgcGFkZGluZzogMTBweCAxMHB4IDNweDtcclxuLy8gICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4vLyAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4vLyB9XHJcbi8vIC5vbmUtdGhpcmQgLnNwZWNpYWwtdGV4dCB7XHJcbi8vICAgd2lkdGg6IDgwJSAhaW1wb3J0YW50O1xyXG4vLyAgIGJvdHRvbTogLTQ2cHggIWltcG9ydGFudDtcclxuLy8gICByaWdodDogMTAlO1xyXG4vLyB9XHJcbi8vIC5zcGVjaWFsLXRleHQgaDMsIC5zaG9lcy1zaG9wZXIgLnNob2Utc2VydmljZWMtbGlzdCBsaSBwIHNwYW4ge1xyXG4vLyAgIG1hcmdpbjogMCAwIDZweDtcclxuLy8gICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4vLyAgIGRpc3BsYXk6IGJsb2NrO1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyAgIGZvbnQtc2l6ZTogMTdweDtcclxuLy8gfVxyXG4vLyAuc2hvZXMtc2hvcGVyIC5zaG9lLXNlcnZpY2VjLWxpc3QgbGkgcCAuZmEge1xyXG4vLyAgIHdpZHRoOiAyMHB4O1xyXG4vLyAgIG1hcmdpbjogOHB4IDA7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC5zcGVjaWFsLXRleHQtd2hpdGUge1xyXG4vLyAgIGJvdHRvbTogYXV0bztcclxuLy8gICB0b3A6IC01MHB4O1xyXG4vLyAgIGNvbG9yOiAjMDAwO1xyXG4vLyAgIGJveC1zaGFkb3c6IDAgMCAxMHB4IC01cHggIzAwMDtcclxuLy8gICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDEpO1xyXG4vLyB9XHJcbi8vIC5zcGVjaWFsLXRleHQgcCB7XHJcbi8vICAgbWFyZ2luLWJvdHRvbTogNXB4ICFpbXBvcnRhbnQ7XHJcbi8vICAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLy8gLmNlbnRlci10ZXh0IHtcclxuLy8gICBwYWRkaW5nOiAyMHB4IDAgNjBweDtcclxuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmNlbnRlci10ZXh0IGg0IHtcclxuLy8gICBtYXgtd2lkdGg6IDY4MHB4O1xyXG4vLyAgIG1hcmdpbjogMCBhdXRvO1xyXG4vLyAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyAgIGxldHRlci1zcGFjaW5nOiAwLjVweDtcclxuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmp1c3RpZnktY29udGVudC1lbmQge1xyXG4vLyAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbi8vIH1cclxuLy8gLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09PT09PT09Ki9cclxuLy8gQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpe1xyXG4vLyAuY2VudGVyLXRleHQge1xyXG4vLyAgIHBhZGRpbmc6IDIwcHggMDtcclxuLy8gICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4vLyB9XHJcbi8vIC5jZW50ZXItdGV4dCBoNCB7XHJcbi8vICAgZm9udC1zaXplOiAxNnB4O1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbi8vICAgbGluZS1oZWlnaHQ6IDIycHg7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IHtcclxuLy8gICBtYXJnaW46IDA7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpLCAub25lLXRoaXJkIHtcclxuLy8gICB3aWR0aDogOTYlICFpbXBvcnRhbnQ7XHJcbi8vICAgbWFyZ2luOiAwIDIlIDI1cHggIWltcG9ydGFudDtcclxuLy8gfVxyXG4vLyAub25lLXRoaXJkIHtcclxuLy8gICBtYXJnaW46IDAgMiUgMjVweCAhaW1wb3J0YW50O1xyXG4vLyB9XHJcbi8vIC5oMy1iYXNlLWhlYWRpbmcge1xyXG4vLyAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC5zaG9lcy1zaG9wZXIgLnNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xyXG4vLyAgIHdpZHRoOiA1MDBweDtcclxuLy8gICBtYXJnaW46IDAgYXV0bztcclxuLy8gfVxyXG4vLyAuc3BlY2lhbC10ZXh0IHtcclxuLy8gICBwb3NpdGlvbjogc3RhdGljO1xyXG4vLyAgIG1heC13aWR0aDogNDUwcHg7XHJcbi8vICAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xyXG4vLyAgIG1hcmdpbjogLTMwcHggYXV0byAwO1xyXG4vLyAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gfVxyXG4vLyAuc2hvZXMtc2hvcGVyIC5zaG9lLXNlcnZpY2VjLWxpc3QgbGkgcCB7XHJcbi8vICAgbWF4LXdpZHRoOiA1MDBweDtcclxuLy8gICBtYXJnaW46IDAgYXV0bztcclxuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHAgaW1nIHtcclxuLy8gICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4vLyAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgIG1hcmdpbjogMCAwIDE1cHggMDtcclxuLy8gfVxyXG4vLyB9XHJcbi8vIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU1MHB4KXtcclxuLy8gLnNob2VzLXNob3BlciAuc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XHJcbi8vICAgd2lkdGg6IDEwMCU7XHJcbi8vIH1cclxuLy8gfVxyXG5cclxuLy8gLmJpZy1iYW5uZXIge1xyXG4vLyBcdGRpc3BsYXk6IGZsZXg7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gXHRoZWlnaHQ6IDEwMHZoO1xyXG4vLyBcdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbi8vIFx0cGFkZGluZy10b3A6IDEwMHB4O1xyXG4vLyB9XHJcbi8vIC5iaWctYmFubmVyIC5jb2wtbWQtNiB7XHJcbi8vIFx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuLy8gfVxyXG4vLyAuYmlnLWJhbm5lciAuY29sLW1kLTYgaDEge1xyXG4vLyBcdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4vLyBcdG1hcmdpbjogMCAwIDNweDtcclxuLy8gXHRmb250LXNpemU6IDQ1cHg7XHJcbi8vIH1cclxuLy8gLmJpZy1iYW5uZXIgLmNvbC1tZC02IGg0IHtcclxuLy8gXHRmb250LXNpemU6IDIycHg7XHJcbi8vIFx0Zm9udC1mYW1pbHk6ICdDb21mb3J0YWEnLCBjdXJzaXZlO1xyXG4vLyB9XHJcbi8vIC5iaWctYmFubmVyIC5jb2wtbWQtNiBwIHtcclxuLy8gICBtYXJnaW46IDVweCAwIDE4cHg7XHJcbi8vICAgZm9udC1zaXplOiAxMnB4O1xyXG4vLyAgIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuLy8gICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbi8vICAgY29sb3I6ICMwMDA7XHJcbi8vICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vIH1cclxuLy8gLmJpZy1iYW5uZXIgLmNvbC1tZC02IGEge1xyXG4vLyBcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gXHRwYWRkaW5nOiA3cHggMTBweCA2cHg7XHJcbi8vIFx0Ym9yZGVyOiAxcHggc29saWQgIzAwMDtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG4vLyBcdGNvbG9yOiAjZmZmO1xyXG4vLyBcdGZvbnQtc2l6ZTogMTBweDtcclxuLy8gICBmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbi8vICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xyXG4vLyB9XHJcbi8vIC5jdXN0b20tc2VydmljZXMge1xyXG4vLyBcdG1hcmdpbjogMCAwIDUwcHg7XHJcbi8vIH1cclxuLy8gLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTQgYSB7XHJcbi8vIFx0Ym94LXNoYWRvdzogMCAwIDEwcHggLTlweCAjNjY2O1xyXG4vLyBcdGRpc3BsYXk6IGJsb2NrO1xyXG4vLyB9XHJcbi8vIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC00IGE6aG92ZXIge1xyXG4vLyBcdGJveC1zaGFkb3c6IDAgMCAxMHB4IC03cHggIzY2NjtcclxuLy8gfVxyXG4vLyAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtNCBmaWd1cmUgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBmaWd1cmUgaW1nIHtcclxuLy8gXHR0cmFuc2l0aW9uOiBhbGwgMXM7XHJcbi8vIH1cclxuLy8gLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTQgYTpob3ZlciBmaWd1cmUgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBhOmhvdmVyIGZpZ3VyZSBpbWcsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zOmhvdmVyIGZpZ3VyZSBpbWcge1xyXG4vLyBcdHRyYW5zZm9ybTogc2NhbGUoMS4yKTtcclxuLy8gfVxyXG4vLyAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyB7XHJcbi8vIFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIGEge1xyXG4vLyBcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gXHRtYXJnaW46IDEycHggMDtcclxuLy8gXHRmb250LWZhbWlseTogYXV0bztcclxuLy8gXHRjb2xvcjogIzAwMDtcclxuLy8gfVxyXG4vLyAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBhOmhvdmVyIHtcclxuLy8gXHR0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuLy8gfVxyXG4vLyAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBwIHtcclxuLy8gXHRmb250LXdlaWdodDogYm9sZDtcclxuLy8gXHRjb2xvcjogIzAwMDtcclxuLy8gXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4vLyBcdGRpc3BsYXk6IGZsZXg7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbi8vIFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgcCBpbWcge1xyXG4vLyBcdG1hcmdpbjogMCA2cHggMCAwO1xyXG4vLyBcdHdpZHRoOiAyMHB4O1xyXG4vLyB9XHJcbi8vIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIGlmcmFtZSB7XHJcbi8vIFx0d2lkdGg6IDEwMCU7XHJcbi8vIFx0Ym9yZGVyOiBub25lO1xyXG4vLyBcdGhlaWdodDogMTgwcHg7XHJcbi8vIFx0YmFja2dyb3VuZDogIzAwMDtcclxuLy8gfVxyXG4vLyAuc2VlLW1vcmUge1xyXG4vLyBcdGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XHJcbi8vIFx0cGFkZGluZzogMTBweCAxNXB4O1xyXG4vLyBcdGNvbG9yOiAjMDAwO1xyXG4vLyBcdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbi8vIFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4vLyBcdG1hcmdpbjogMzBweCAwIDAgMDtcclxuLy8gXHRmb250LXNpemU6IDIwcHg7XHJcbi8vIH1cclxuLy8gLnNlZS1tb3JlOmhvdmVyIHtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG4vLyBcdGNvbG9yOiAjZmZmO1xyXG4vLyB9XHJcbi8vIC5iZy1pbWFnZS10ZXh0IHtcclxuLy8gXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvYmdfdGV4dC5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIHRvcCBjZW50ZXIgLyBjb3ZlcjtcclxuLy8gXHRwYWRkaW5nOiA1MHB4IDIwcHg7XHJcbi8vIFx0Y29sb3I6ICNmZmY7XHJcbi8vIFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vIFx0Zm9udC1mYW1pbHk6IGF1dG87XHJcbi8vIH1cclxuLy8gLmJnLWltYWdlLXRleHQgaDEge1xyXG4vLyBcdGZvbnQtc2l6ZTogNDVweDtcclxuLy8gfVxyXG4vLyAuYmctaW1hZ2UtdGV4dCBoMiB7XHJcbi8vIFx0bWFyZ2luOiAyMHB4IDA7XHJcbi8vIFx0Zm9udC1zaXplOiAzMnB4O1xyXG4vLyB9XHJcbi8vIC5iZy1pbWFnZS10ZXh0IGgzIHtcclxuLy8gXHRtYXJnaW46IDVweCAwO1xyXG4vLyBcdGZvbnQtc2l6ZTogMjZweDtcclxuLy8gfVxyXG4vLyAuaGFzaC10YWcge1xyXG4vLyBcdGRpc3BsYXk6IGZsZXg7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gXHRwYWRkaW5nOiA1MHB4IDIwcHg7XHJcbi8vIFx0aGVpZ2h0OiA0MDBweDtcclxuLy8gXHRtYXJnaW46IDAgMCA0MHB4O1xyXG4vLyAgIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuLy8gICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4vLyAgIC8qYmFja2dyb3VuZDogIzI0MjQyMDsqL1xyXG4vLyAgIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9oYXNoLXRhZy5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcclxuLy8gfVxyXG4vLyAuaGFzaC10YWcgc3BhbiB7XHJcbi8vIFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4vLyBcdHBhZGRpbmc6IDE1cHggNXB4IDVweDtcclxuLy8gXHRjb2xvcjogI2ZmZjtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG4vLyBcdGZvbnQtc2l6ZTogOTBweDtcclxuLy8gXHRsaW5lLWhlaWdodDogOTBweDtcclxuLy8gfVxyXG5cclxuXHJcblxyXG4uYmlnLWJhbm5lciB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGhlaWdodDogMTAwdmg7XHJcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHRwYWRkaW5nLXRvcDogMTAwcHg7XHJcbn1cclxuLmJpZy1iYW5uZXIgLmNvbC1tZC02IHtcclxuXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcbi5iaWctYmFubmVyIC5jb2wtbWQtNiBoMSB7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0bWFyZ2luOiAwIDAgM3B4O1xyXG5cdGZvbnQtc2l6ZTogNDVweDtcclxufVxyXG4uYmlnLWJhbm5lciAuY29sLW1kLTYgaDQge1xyXG5cdGZvbnQtc2l6ZTogMjJweDtcclxuXHRmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbn1cclxuLmJpZy1iYW5uZXIgLmNvbC1tZC02IHAge1xyXG4gIG1hcmdpbjogNXB4IDAgMThweDtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgZm9udC1mYW1pbHk6ICdDb21mb3J0YWEnLCBjdXJzaXZlO1xyXG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcclxuICBjb2xvcjogIzAwMDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uYmlnLWJhbm5lciAuY29sLW1kLTYgYSB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHBhZGRpbmc6IDdweCAxMHB4IDZweDtcclxuXHRib3JkZXI6IDFweCBzb2xpZCAjMDAwO1xyXG5cdGJhY2tncm91bmQ6ICMwMDA7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0Zm9udC1zaXplOiAxMHB4O1xyXG4gIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbn1cclxuLmN1c3RvbS1zZXJ2aWNlcyB7XHJcblx0bWFyZ2luOiAwIDAgNTBweDtcclxufVxyXG4uY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtNCBhIHtcclxuXHRib3gtc2hhZG93OiAwIDAgMTBweCAtOXB4ICM2NjY7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTQgYTpob3ZlciB7XHJcblx0Ym94LXNoYWRvdzogMCAwIDEwcHggLTdweCAjNjY2O1xyXG59XHJcbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC00IGZpZ3VyZSBpbWcsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIGZpZ3VyZSBpbWcge1xyXG5cdHRyYW5zaXRpb246IGFsbCAxcztcclxuXHRvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDI0MHB4O1xyXG59XHJcbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC00IGE6aG92ZXIgZmlndXJlIGltZywgLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgYTpob3ZlciBmaWd1cmUgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMzpob3ZlciBmaWd1cmUgaW1nIHtcclxuXHR0cmFuc2Zvcm06IHNjYWxlKDEuMik7XHJcbn1cclxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0yIHtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgYSwgLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTIgYSB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdG1hcmdpbjogMTJweCAwO1xyXG5cdGZvbnQtZmFtaWx5OiBhdXRvO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG59XHJcbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIGE6aG92ZXIsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0yIGE6aG92ZXIge1xyXG5cdHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIHAsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0yIHAge1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4uY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBwIGltZywgLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTIgcCBpbWcge1xyXG5cdG1hcmdpbjogMCA2cHggMCAwO1xyXG5cdHdpZHRoOiAyMHB4O1xyXG59XHJcbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIGlmcmFtZSB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0Ym9yZGVyOiBub25lO1xyXG5cdGhlaWdodDogMTgwcHg7XHJcblx0YmFja2dyb3VuZDogIzAwMDtcclxufVxyXG4uc2VlLW1vcmUge1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XHJcblx0cGFkZGluZzogMTBweCAxNXB4O1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdG1hcmdpbjogMzBweCAwIDAgMDtcclxuXHRmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLnNlZS1tb3JlOmhvdmVyIHtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG5cdGNvbG9yOiAjZmZmO1xyXG59XHJcbi5iZy1pbWFnZS10ZXh0IHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvYmdfdGV4dC5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIHRvcCBjZW50ZXIgLyBjb3ZlcjtcclxuXHRwYWRkaW5nOiA1MHB4IDIwcHg7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0Zm9udC1mYW1pbHk6IGF1dG87XHJcbn1cclxuLmJnLWltYWdlLXRleHQgaDEge1xyXG5cdGZvbnQtc2l6ZTogNDVweDtcclxufVxyXG4uYmctaW1hZ2UtdGV4dCBoMiB7XHJcblx0bWFyZ2luOiAyMHB4IDA7XHJcblx0Zm9udC1zaXplOiAzMnB4O1xyXG59XHJcbi5iZy1pbWFnZS10ZXh0IGgzIHtcclxuXHRtYXJnaW46IDVweCAwO1xyXG5cdGZvbnQtc2l6ZTogMjZweDtcclxufVxyXG4uaGFzaC10YWcge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRwYWRkaW5nOiA1MHB4IDIwcHg7XHJcblx0aGVpZ2h0OiA0MDBweDtcclxuXHRtYXJnaW46IDAgMCA0MHB4O1xyXG4gIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIC8qYmFja2dyb3VuZDogIzI0MjQyMDsqL1xyXG4gIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9oYXNoLXRhZy5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIGNlbnRlciBjZW50ZXIgLyBjb3ZlcjtcclxufVxyXG4uaGFzaC10YWcgc3BhbiB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHBhZGRpbmc6IDE1cHggNXB4IDVweDtcclxuXHRjb2xvcjogI2ZmZjtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG5cdGZvbnQtc2l6ZTogOTBweDtcclxuXHRsaW5lLWhlaWdodDogOTBweDtcclxufVxyXG4uYnJhbmQtbmFtZS1sb2FjdGlvbiBhIHtcclxuXHRmb250LWZhbWlseTogJ01vbnRzZXJyYXQnLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XHJcblx0Zm9udC1zaXplOiAxM3B4O1xyXG5cdGhlaWdodDogMjBweDtcclxuXHRsaW5lLWhlaWdodDogMjBweDtcclxuXHRjb2xvcjogIzI0MjAyMDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG5cdHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcblx0bWFyZ2luOiAwIDAgIWltcG9ydGFudDtcclxufVxyXG4uY3VzdG9tLXNlcnZpY2VzLmN1c3RvbS1zZXJ2aWNlcy1wcm92aWRlcnMgLmNvbC1tZC0yIGZpZ3VyZSBpbWcge1xyXG5cdGJvcmRlci1yYWRpdXM6IDUwJTtcclxufSIsIi5iaWctYmFubmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcGFkZGluZy10b3A6IDEwMHB4O1xufVxuXG4uYmlnLWJhbm5lciAuY29sLW1kLTYge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uYmlnLWJhbm5lciAuY29sLW1kLTYgaDEge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luOiAwIDAgM3B4O1xuICBmb250LXNpemU6IDQ1cHg7XG59XG5cbi5iaWctYmFubmVyIC5jb2wtbWQtNiBoNCB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgZm9udC1mYW1pbHk6IFwiQ29tZm9ydGFhXCIsIGN1cnNpdmU7XG59XG5cbi5iaWctYmFubmVyIC5jb2wtbWQtNiBwIHtcbiAgbWFyZ2luOiA1cHggMCAxOHB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtZmFtaWx5OiBcIkNvbWZvcnRhYVwiLCBjdXJzaXZlO1xuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmJpZy1iYW5uZXIgLmNvbC1tZC02IGEge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDdweCAxMHB4IDZweDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC1mYW1pbHk6IFwiQ29tZm9ydGFhXCIsIGN1cnNpdmU7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcyB7XG4gIG1hcmdpbjogMCAwIDUwcHg7XG59XG5cbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC00IGEge1xuICBib3gtc2hhZG93OiAwIDAgMTBweCAtOXB4ICM2NjY7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuXG4uY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtNCBhOmhvdmVyIHtcbiAgYm94LXNoYWRvdzogMCAwIDEwcHggLTdweCAjNjY2O1xufVxuXG4uY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtNCBmaWd1cmUgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBmaWd1cmUgaW1nIHtcbiAgdHJhbnNpdGlvbjogYWxsIDFzO1xuICBvYmplY3QtZml0OiBjb250YWluO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyNDBweDtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTQgYTpob3ZlciBmaWd1cmUgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBhOmhvdmVyIGZpZ3VyZSBpbWcsIC5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zOmhvdmVyIGZpZ3VyZSBpbWcge1xuICB0cmFuc2Zvcm06IHNjYWxlKDEuMik7XG59XG5cbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgYSwgLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTIgYSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAxMnB4IDA7XG4gIGZvbnQtZmFtaWx5OiBhdXRvO1xuICBjb2xvcjogIzAwMDtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgYTpob3ZlciwgLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTIgYTpob3ZlciB7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG4uY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMyBwLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMiBwIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMDAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5jdXN0b20tc2VydmljZXMgLmNvbC1tZC0zIHAgaW1nLCAuY3VzdG9tLXNlcnZpY2VzIC5jb2wtbWQtMiBwIGltZyB7XG4gIG1hcmdpbjogMCA2cHggMCAwO1xuICB3aWR0aDogMjBweDtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcyAuY29sLW1kLTMgaWZyYW1lIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgaGVpZ2h0OiAxODBweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnNlZS1tb3JlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICBjb2xvcjogIzAwMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW46IDMwcHggMCAwIDA7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLnNlZS1tb3JlOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5iZy1pbWFnZS10ZXh0IHtcbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9iZ190ZXh0LmpwZ1wiKSBuby1yZXBlYXQgc2Nyb2xsIHRvcCBjZW50ZXIvY292ZXI7XG4gIHBhZGRpbmc6IDUwcHggMjBweDtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LWZhbWlseTogYXV0bztcbn1cblxuLmJnLWltYWdlLXRleHQgaDEge1xuICBmb250LXNpemU6IDQ1cHg7XG59XG5cbi5iZy1pbWFnZS10ZXh0IGgyIHtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtc2l6ZTogMzJweDtcbn1cblxuLmJnLWltYWdlLXRleHQgaDMge1xuICBtYXJnaW46IDVweCAwO1xuICBmb250LXNpemU6IDI2cHg7XG59XG5cbi5oYXNoLXRhZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBwYWRkaW5nOiA1MHB4IDIwcHg7XG4gIGhlaWdodDogNDAwcHg7XG4gIG1hcmdpbjogMCAwIDQwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIkNvbWZvcnRhYVwiLCBjdXJzaXZlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAvKmJhY2tncm91bmQ6ICMyNDI0MjA7Ki9cbiAgYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9oYXNoLXRhZy5qcGdcIikgbm8tcmVwZWF0IHNjcm9sbCBjZW50ZXIgY2VudGVyL2NvdmVyO1xufVxuXG4uaGFzaC10YWcgc3BhbiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMTVweCA1cHggNXB4O1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgZm9udC1zaXplOiA5MHB4O1xuICBsaW5lLWhlaWdodDogOTBweDtcbn1cblxuLmJyYW5kLW5hbWUtbG9hY3Rpb24gYSB7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXRcIiwgc2Fucy1zZXJpZiAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGhlaWdodDogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGNvbG9yOiAjMjQyMDIwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgbWFyZ2luOiAwIDAgIWltcG9ydGFudDtcbn1cblxuLmN1c3RvbS1zZXJ2aWNlcy5jdXN0b20tc2VydmljZXMtcHJvdmlkZXJzIC5jb2wtbWQtMiBmaWd1cmUgaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/@auth/components/main/main.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/@auth/components/main/main.component.ts ***!
  \*********************************************************/
/*! exports provided: NgxMainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxMainComponent", function() { return NgxMainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");






var NgxMainComponent = /** @class */ (function () {
    function NgxMainComponent(utilsService, cd, fb, router, authenticationService) {
        this.utilsService = utilsService;
        this.cd = cd;
        this.fb = fb;
        this.router = router;
        this.authenticationService = authenticationService;
    }
    NgxMainComponent.prototype.ngOnInit = function () { };
    NgxMainComponent.prototype.explore_artists = function () {
        this.router.navigate(['artist/explore']);
    };
    NgxMainComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] }
    ]; };
    NgxMainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-main',
            template: __webpack_require__(/*! raw-loader!./main.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/main/main.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./main.component.scss */ "./src/app/@auth/components/main/main.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], NgxMainComponent);
    return NgxMainComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/register/register.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/@auth/components/register/register.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*!\n * Copyright (c) Akveo 2019. All Rights Reserved.\n * Licensed under the Single Application / Multi Application License.\n * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.\n */\n:host .title {\n  margin-bottom: 2rem;\n}\n.common-input {\n  margin: 10px 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9yZWdpc3Rlci9DOlxcVXNlcnNcXERFTExcXERlc2t0b3BcXGZpeG15a2l4L3NyY1xcYXBwXFxAYXV0aFxcY29tcG9uZW50c1xccmVnaXN0ZXJcXHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7O0VBQUE7QUFNRTtFQUNFLG1CQUFBO0FDQUo7QURJQTtFQUNFLGdCQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIENvcHlyaWdodCAoYykgQWt2ZW8gMjAxOS4gQWxsIFJpZ2h0cyBSZXNlcnZlZC5cclxuICogTGljZW5zZWQgdW5kZXIgdGhlIFNpbmdsZSBBcHBsaWNhdGlvbiAvIE11bHRpIEFwcGxpY2F0aW9uIExpY2Vuc2UuXHJcbiAqIFNlZSBMSUNFTlNFX1NJTkdMRV9BUFAgLyBMSUNFTlNFX01VTFRJX0FQUCBpbiB0aGUgJ2RvY3MnIGZvbGRlciBmb3IgbGljZW5zZSBpbmZvcm1hdGlvbiBvbiB0eXBlIG9mIHB1cmNoYXNlZCBsaWNlbnNlLlxyXG4gKi9cclxuOmhvc3Qge1xyXG4gIC50aXRsZSB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG4gIH1cclxufVxyXG5cclxuLmNvbW1vbi1pbnB1dCB7XHJcbiAgbWFyZ2luIDoxMHB4IDBweDtcclxufSIsIi8qIVxuICogQ29weXJpZ2h0IChjKSBBa3ZlbyAyMDE5LiBBbGwgUmlnaHRzIFJlc2VydmVkLlxuICogTGljZW5zZWQgdW5kZXIgdGhlIFNpbmdsZSBBcHBsaWNhdGlvbiAvIE11bHRpIEFwcGxpY2F0aW9uIExpY2Vuc2UuXG4gKiBTZWUgTElDRU5TRV9TSU5HTEVfQVBQIC8gTElDRU5TRV9NVUxUSV9BUFAgaW4gdGhlICdkb2NzJyBmb2xkZXIgZm9yIGxpY2Vuc2UgaW5mb3JtYXRpb24gb24gdHlwZSBvZiBwdXJjaGFzZWQgbGljZW5zZS5cbiAqL1xuOmhvc3QgLnRpdGxlIHtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cblxuLmNvbW1vbi1pbnB1dCB7XG4gIG1hcmdpbjogMTBweCAwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/@auth/components/register/register.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/@auth/components/register/register.component.ts ***!
  \*****************************************************************/
/*! exports provided: NgxRegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxRegisterComponent", function() { return NgxRegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../constants */ "./src/app/@auth/components/constants.ts");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */





var NgxRegisterComponent = /** @class */ (function () {
    function NgxRegisterComponent(authService, cd, fb, router) {
        this.authService = authService;
        this.cd = cd;
        this.fb = fb;
        this.router = router;
        this.minLength = this.getConfigValue('forms.validation.password.minLength');
        this.maxLength = this.getConfigValue('forms.validation.password.maxLength');
        this.isFullNameRequired = this.getConfigValue('forms.validation.fullName.required');
        this.isEmailRequired = this.getConfigValue('forms.validation.email.required');
        this.isPasswordRequired = this.getConfigValue('forms.validation.password.required');
        this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
        this.showMessages = this.getConfigValue('forms.register.showMessages');
        this.strategy = this.getConfigValue('forms.register.strategy');
        this.submitted = false;
        this.errors = [];
        this.messages = [];
        this.user = {};
    }
    Object.defineProperty(NgxRegisterComponent.prototype, "fullName", {
        get: function () { return this.registerForm.get('fullName'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxRegisterComponent.prototype, "email", {
        get: function () { return this.registerForm.get('email'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxRegisterComponent.prototype, "password", {
        get: function () { return this.registerForm.get('password'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxRegisterComponent.prototype, "confirmPassword", {
        get: function () { return this.registerForm.get('confirmPassword'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxRegisterComponent.prototype, "terms", {
        get: function () { return this.registerForm.get('terms'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxRegisterComponent.prototype, "mno", {
        get: function () { return this.registerForm.get('mno'); },
        enumerable: true,
        configurable: true
    });
    NgxRegisterComponent.prototype.ngOnInit = function () {
        var fullNameValidators = [];
        // this.isFullNameRequired && fullNameValidators.push(Validators.required);
        var emailValidators = [
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(_constants__WEBPACK_IMPORTED_MODULE_4__["EMAIL_PATTERN"]),
        ];
        // this.isEmailRequired && emailValidators.push(Validators.required);
        var passwordValidators = [
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10),
        ];
        var mobileNumberValidators = [
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(10),
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(10),
        ];
        // this.isPasswordRequired && passwordValidators.push(Validators.required);
        this.registerForm = this.fb.group({
            fullName: this.fb.control('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: this.fb.control('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            password: this.fb.control('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            confirmPassword: this.fb.control('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            mno: this.fb.control('', []),
        });
    };
    NgxRegisterComponent.prototype.register = function () {
        var _this = this;
        this.user = this.registerForm.value;
        this.errors = this.messages = [];
        this.submitted = true;
        var obj = {
            "email": "",
            "name": "",
            "mno": "",
            "password": "",
            "role": "3"
        };
        obj.name = this.user.fullName;
        obj.email = this.user.email;
        obj.mno = this.user.mno;
        obj.password = btoa(this.user.password);
        this.authService.register(obj).subscribe(function (result) {
            _this.submitted = false;
            if (result.success) {
                _this.messages = result.message;
            }
            else {
                _this.errors = result.error;
            }
            var redirect = result.redirect;
            if (redirect) {
                setTimeout(function () {
                    return _this.router.navigateByUrl(redirect);
                }, _this.redirectDelay);
            }
            _this.cd.detectChanges();
        });
    };
    NgxRegisterComponent.prototype.getConfigValue = function (key) {
        return '';
    };
    NgxRegisterComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    NgxRegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-register',
            template: __webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/register/register.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/@auth/components/register/register.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], NgxRegisterComponent);
    return NgxRegisterComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/shoeart/shoeart.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/@auth/components/shoeart/shoeart.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".about-bg {\n  background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top -100px center/100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 32px;\n  line-height: 36px;\n  color: #000;\n  letter-spacing: 2px;\n  font-weight: bold;\n  text-transform: uppercase;\n  padding: 150px 15px 50px;\n}\n\n.about-bg span {\n  padding: 5px 15px;\n  background: rgba(255, 255, 255, 0.8);\n  text-align: center;\n}\n\n@media screen and (max-width: 768px) {\n  .about-bg {\n    background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top 30px center/cover;\n  }\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  #main {\n    padding: 10px 10px 0 !important;\n  }\n\n  .h2-base-heading {\n    flex-direction: column;\n    font-size: 18px;\n  }\n\n  .h2-base-heading p {\n    margin: 0 0 10px;\n    flex-direction: column;\n  }\n\n  .h2-base-heading p img {\n    margin: 0 0 6px;\n  }\n\n  .mt-80 {\n    margin-top: 0 !important;\n  }\n\n  .about-bg {\n    background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top -70px center/cover;\n    font-size: 18px;\n    line-height: 24px;\n    padding: 100px 15px 50px;\n  }\n}\n\n.header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list li {\n  padding: 15px;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n}\n\n.about-shoe-servicec-list li + li {\n  margin: 20px 0 0 0;\n}\n\n.about-shoe-servicec-list li .about-shoe-text {\n  width: calc(100% - 415px);\n}\n\n.about-shoe-servicec-list li .about-shoe-text .h2-base-heading, .about-shoe-servicec-list li .about-shoe-text .img-right {\n  margin: 0 0 12px;\n}\n\n.about-shoe-servicec-list li .text-right a {\n  display: inline-block;\n  transition: all 0.5s;\n  color: #fff;\n  background: #000;\n  padding: 4px 12px;\n  border: 2px solid #000;\n}\n\n.about-shoe-servicec-list li .text-right a:hover {\n  background: #fff;\n  color: #000;\n  padding: 4px 18px;\n}\n\n.about-shoe-servicec-list li figure {\n  width: 415px;\n  height: 250px;\n  overflow: hidden;\n  padding: 0 0 0 15px;\n}\n\n.about-shoe-servicec-list li figure img {\n  width: 100%;\n  height: 250px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 4px;\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  .about-shoe-servicec-list li {\n    flex-direction: column;\n  }\n\n  .about-shoe-text {\n    width: 100% !important;\n    margin: 0 0 15px;\n    text-align: center;\n  }\n\n  .about-shoe-text .text-right {\n    text-align: center;\n  }\n\n  .about-shoe-servicec-list li figure {\n    width: 100%;\n    max-width: 400px;\n    padding: 0;\n    margin: 0 auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9zaG9lYXJ0L0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFxzaG9lYXJ0XFxzaG9lYXJ0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3Nob2VhcnQvc2hvZWFydC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLG9GQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0Esd0JBQUE7QUNDRDs7QURDRTtFQUNELGlCQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtBQ0VEOztBREVBO0VBQ0M7SUFDRSxtRkFBQTtFQ0NEO0FBQ0Y7O0FER0Esa0NBQUE7O0FBQ0E7RUFDQztJQUNFLCtCQUFBO0VDREQ7O0VER0Q7SUFDRSxzQkFBQTtJQUNBLGVBQUE7RUNBRDs7RURFRDtJQUNFLGdCQUFBO0lBQ0Esc0JBQUE7RUNDRDs7RURDRDtJQUNFLGVBQUE7RUNFRDs7RURBRDtJQUNFLHdCQUFBO0VDR0Q7O0VEREQ7SUFDRSxvRkFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUNBLHdCQUFBO0VDSUQ7QUFDRjs7QURBRTtFQUNELDhCQUFBO0FDRUQ7O0FEQUE7RUFDRSxrQkFBQTtBQ0dGOztBRERBO0VBQ0MsYUFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxnSEFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLDhCQUFBO0FDSUQ7O0FERkE7RUFDQyxrQkFBQTtBQ0tEOztBREhBO0VBQ0MseUJBQUE7QUNNRDs7QURKQTtFQUNDLGdCQUFBO0FDT0Q7O0FETEE7RUFDQyxxQkFBQTtFQUNBLG9CQUFBO0VBQ0MsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtBQ1FGOztBRE5BO0VBQ0MsZ0JBQUE7RUFDQSxXQUFBO0VBQ0MsaUJBQUE7QUNTRjs7QURQQTtFQUNDLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ1VEOztBRFJBO0VBQ0MsV0FBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0VBQ0Esa0JBQUE7QUNXRDs7QURUQSxrQ0FBQTs7QUFDQTtFQUNDO0lBQ0Msc0JBQUE7RUNZQTs7RURWRDtJQUNDLHNCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQ2FBOztFRFhEO0lBQ0Msa0JBQUE7RUNjQTs7RURaRDtJQUNDLFdBQUE7SUFDQSxnQkFBQTtJQUNBLFVBQUE7SUFDQSxjQUFBO0VDZUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL0BhdXRoL2NvbXBvbmVudHMvc2hvZWFydC9zaG9lYXJ0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFib3V0LWJnIHtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwIHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9qb3JkYW5zaG9lc2Jhbm5lci5qcGcnKSBuby1yZXBlYXQgZml4ZWQgdG9wIC0xMDBweCBjZW50ZXIgLyAxMDAlIDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0Zm9udC1zaXplOiAzMnB4O1xyXG5cdGxpbmUtaGVpZ2h0OiAzNnB4O1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdGxldHRlci1zcGFjaW5nOiAycHg7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcblx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRwYWRkaW5nOiAxNTBweCAxNXB4IDUwcHg7XHJcbiAgfVxyXG4gIC5hYm91dC1iZyBzcGFuIHtcclxuXHRwYWRkaW5nOiA1cHggMTVweDtcclxuXHRiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDAuOCk7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcblx0LmFib3V0LWJnIHtcclxuXHQgIGJhY2tncm91bmQ6ICMwMDAgdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLmpwZycpIG5vLXJlcGVhdCBmaXhlZCB0b3AgMzBweCBjZW50ZXIgLyBjb3ZlcjtcclxuXHR9XHJcbiAgfVxyXG5cclxuXHJcbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KXtcclxuXHQjbWFpbiB7XHJcblx0ICBwYWRkaW5nOiAxMHB4IDEwcHggMCAhaW1wb3J0YW50O1xyXG5cdH1cclxuXHQuaDItYmFzZS1oZWFkaW5nIHtcclxuXHQgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0ICBmb250LXNpemU6IDE4cHg7XHJcblx0fVxyXG5cdC5oMi1iYXNlLWhlYWRpbmcgcCB7XHJcblx0ICBtYXJnaW46IDAgMCAxMHB4O1xyXG5cdCAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuXHR9XHJcblx0LmgyLWJhc2UtaGVhZGluZyBwIGltZyB7XHJcblx0ICBtYXJnaW46IDAgMCA2cHg7XHJcblx0fVxyXG5cdC5tdC04MCB7XHJcblx0ICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XHJcblx0fVxyXG5cdC5hYm91dC1iZyB7XHJcblx0ICBiYWNrZ3JvdW5kOiAjMDAwIHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9qb3JkYW5zaG9lc2Jhbm5lci5qcGcnKSBuby1yZXBlYXQgZml4ZWQgdG9wIC03MHB4IGNlbnRlciAvIGNvdmVyO1xyXG5cdCAgZm9udC1zaXplOiAxOHB4O1xyXG5cdCAgbGluZS1oZWlnaHQ6IDI0cHg7XHJcblx0ICBwYWRkaW5nOiAxMDBweCAxNXB4IDUwcHg7XHJcblx0fVxyXG4gIH1cclxuXHJcblxyXG4gIC5oZWFkZXItaW5pdGlhbCB7XHJcblx0YmFja2dyb3VuZDogcmdiYSgwLDAsMCwwLjgpO1xyXG59XHJcbiNtYWluIHtcclxuICBwYWRkaW5nOiA0MHB4IDE1cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcblx0cGFkZGluZzogMTVweDtcclxuXHRib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsMCwwLDAuMDUpO1xyXG5cdGJvcmRlci1yYWRpdXM6IDJweDtcclxuXHRib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwwLjIpLCAwLjVyZW0gMC4xcmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsLjIpO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0YWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcblx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkrbGkge1xyXG5cdG1hcmdpbjogMjBweCAwIDAgMDtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQge1xyXG5cdHdpZHRoOiBjYWxjKDEwMCUgLSA0MTVweCk7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IC5oMi1iYXNlLWhlYWRpbmcsIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaW1nLXJpZ2h0IHtcclxuXHRtYXJnaW46IDAgMCAxMnB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLnRleHQtcmlnaHQgYSB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHRyYW5zaXRpb246IGFsbCAwLjVzO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgcGFkZGluZzogNHB4IDEycHg7XHJcbiAgYm9yZGVyOiAycHggc29saWQgIzAwMDtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC50ZXh0LXJpZ2h0IGE6aG92ZXIge1xyXG5cdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0Y29sb3I6ICMwMDA7XHJcbiAgcGFkZGluZzogNHB4IDE4cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xyXG5cdHdpZHRoOiA0MTVweDtcclxuXHRoZWlnaHQ6IDI1MHB4O1xyXG5cdG92ZXJmbG93OiBoaWRkZW47XHJcblx0cGFkZGluZzogMCAwIDAgMTVweDtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSBpbWcge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGhlaWdodDogMjUwcHg7XHJcblx0b2JqZWN0LWZpdDogY292ZXI7XHJcblx0Ym9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KXtcclxuXHQuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0fVxyXG5cdC5hYm91dC1zaG9lLXRleHQge1xyXG5cdFx0d2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuXHRcdG1hcmdpbjogMCAwIDE1cHg7XHJcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0fVxyXG5cdC5hYm91dC1zaG9lLXRleHQgLnRleHQtcmlnaHQge1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdH1cclxuXHQuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdG1heC13aWR0aDogNDAwcHg7XHJcblx0XHRwYWRkaW5nOiAwO1xyXG5cdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0fVxyXG59IiwiLmFib3V0LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMCB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgdG9wIC0xMDBweCBjZW50ZXIvMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMzJweDtcbiAgbGluZS1oZWlnaHQ6IDM2cHg7XG4gIGNvbG9yOiAjMDAwO1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgcGFkZGluZzogMTUwcHggMTVweCA1MHB4O1xufVxuXG4uYWJvdXQtYmcgc3BhbiB7XG4gIHBhZGRpbmc6IDVweCAxNXB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgLmFib3V0LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwIHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCB0b3AgMzBweCBjZW50ZXIvY292ZXI7XG4gIH1cbn1cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAjbWFpbiB7XG4gICAgcGFkZGluZzogMTBweCAxMHB4IDAgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5oMi1iYXNlLWhlYWRpbmcge1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICB9XG5cbiAgLmgyLWJhc2UtaGVhZGluZyBwIHtcbiAgICBtYXJnaW46IDAgMCAxMHB4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAuaDItYmFzZS1oZWFkaW5nIHAgaW1nIHtcbiAgICBtYXJnaW46IDAgMCA2cHg7XG4gIH1cblxuICAubXQtODAge1xuICAgIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5hYm91dC1iZyB7XG4gICAgYmFja2dyb3VuZDogIzAwMCB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLmpwZ1wiKSBuby1yZXBlYXQgZml4ZWQgdG9wIC03MHB4IGNlbnRlci9jb3ZlcjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XG4gICAgcGFkZGluZzogMTAwcHggMTVweCA1MHB4O1xuICB9XG59XG4uaGVhZGVyLWluaXRpYWwge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG59XG5cbiNtYWluIHtcbiAgcGFkZGluZzogNDBweCAxNXB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcbiAgcGFkZGluZzogMTVweDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA1KTtcbiAgYm9yZGVyLXJhZGl1czogMnB4O1xuICBib3gtc2hhZG93OiAtMC41cmVtIC0wLjVyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjIpO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpICsgbGkge1xuICBtYXJnaW46IDIwcHggMCAwIDA7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MTVweCk7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaDItYmFzZS1oZWFkaW5nLCAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmltZy1yaWdodCB7XG4gIG1hcmdpbjogMCAwIDEycHg7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLnRleHQtcmlnaHQgYSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXM7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBwYWRkaW5nOiA0cHggMTJweDtcbiAgYm9yZGVyOiAycHggc29saWQgIzAwMDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAudGV4dC1yaWdodCBhOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6ICMwMDA7XG4gIHBhZGRpbmc6IDRweCAxOHB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XG4gIHdpZHRoOiA0MTVweDtcbiAgaGVpZ2h0OiAyNTBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgcGFkZGluZzogMCAwIDAgMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjUwcHg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB9XG5cbiAgLmFib3V0LXNob2UtdGV4dCB7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXJnaW46IDAgMCAxNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5hYm91dC1zaG9lLXRleHQgLnRleHQtcmlnaHQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDQwMHB4O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/components/shoeart/shoeart.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/@auth/components/shoeart/shoeart.component.ts ***!
  \***************************************************************/
/*! exports provided: NgxShoeArtComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxShoeArtComponent", function() { return NgxShoeArtComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NgxShoeArtComponent = /** @class */ (function () {
    function NgxShoeArtComponent() {
    }
    NgxShoeArtComponent.prototype.ngOnInit = function () { };
    NgxShoeArtComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-shoe-art',
            template: __webpack_require__(/*! raw-loader!./shoeart.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/shoeart/shoeart.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./shoeart.component.scss */ "./src/app/@auth/components/shoeart/shoeart.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NgxShoeArtComponent);
    return NgxShoeArtComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/shoerestoration/shoerestoration.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/@auth/components/shoerestoration/shoerestoration.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list li {\n  padding: 15px;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n  box-shadow: -0.5rem -0.5rem 0.9375rem rgba(177, 177, 177, 0.2), 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.2);\n}\n\n.about-shoe-servicec-list li + li {\n  margin: 20px 0 0 0;\n}\n\n.about-shoe-servicec-list li .about-shoe-text {\n  width: calc(100% - 415px);\n}\n\n.about-shoe-servicec-list li .about-shoe-text .h2-base-heading, .about-shoe-servicec-list li .about-shoe-text .img-right {\n  margin: 0 0 12px;\n}\n\n.about-shoe-servicec-list li .text-right a {\n  display: inline-block;\n  transition: all 0.5s;\n  color: #fff;\n  background: #000;\n  padding: 4px 12px;\n  border: 2px solid #000;\n}\n\n.about-shoe-servicec-list li .text-right a:hover {\n  background: #fff;\n  color: #000;\n  padding: 4px 18px;\n}\n\n.about-shoe-servicec-list li figure {\n  width: 415px;\n  height: 250px;\n  overflow: hidden;\n  padding: 0 0 0 15px;\n}\n\n.about-shoe-servicec-list li figure img {\n  width: 100%;\n  height: 250px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 4px;\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  .about-shoe-servicec-list li {\n    flex-direction: column;\n  }\n\n  .about-shoe-text {\n    width: 100% !important;\n    margin: 0 0 15px;\n    text-align: center;\n  }\n\n  .about-shoe-text .text-right {\n    text-align: center;\n  }\n\n  .about-shoe-servicec-list li figure {\n    width: 100%;\n    max-width: 400px;\n    padding: 0;\n    margin: 0 auto;\n  }\n}\n\n.about-bg {\n  background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top -100px center/100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 32px;\n  line-height: 36px;\n  color: #000;\n  letter-spacing: 2px;\n  font-weight: bold;\n  text-transform: uppercase;\n  padding: 150px 15px 50px;\n}\n\n.about-bg span {\n  padding: 5px 15px;\n  background: rgba(255, 255, 255, 0.8);\n  text-align: center;\n}\n\n@media screen and (max-width: 768px) {\n  .about-bg {\n    background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top 30px center/cover;\n  }\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  #main {\n    padding: 10px 10px 0 !important;\n  }\n\n  .h2-base-heading {\n    flex-direction: column;\n    font-size: 18px;\n  }\n\n  .h2-base-heading p {\n    margin: 0 0 10px;\n    flex-direction: column;\n  }\n\n  .h2-base-heading p img {\n    margin: 0 0 6px;\n  }\n\n  .mt-80 {\n    margin-top: 0 !important;\n  }\n\n  .about-bg {\n    background: #000 url('jordanshoesbanner.jpg') no-repeat fixed top -70px center/cover;\n    font-size: 18px;\n    line-height: 24px;\n    padding: 100px 15px 50px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9zaG9lcmVzdG9yYXRpb24vQzpcXFVzZXJzXFxERUxMXFxEZXNrdG9wXFxmaXhteWtpeC9zcmNcXGFwcFxcQGF1dGhcXGNvbXBvbmVudHNcXHNob2VyZXN0b3JhdGlvblxcc2hvZXJlc3RvcmF0aW9uLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3Nob2VyZXN0b3JhdGlvbi9zaG9lcmVzdG9yYXRpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyw4QkFBQTtBQ0NEOztBRENBO0VBQ0Usa0JBQUE7QUNFRjs7QURBQTtFQUNDLGFBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsOEJBQUE7RUFDQSxnSEFBQTtBQ0dEOztBREFBO0VBQ0Msa0JBQUE7QUNHRDs7QUREQTtFQUNDLHlCQUFBO0FDSUQ7O0FERkE7RUFDQyxnQkFBQTtBQ0tEOztBREhBO0VBQ0MscUJBQUE7RUFDQSxvQkFBQTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUNNRjs7QURKQTtFQUNDLGdCQUFBO0VBQ0EsV0FBQTtFQUNDLGlCQUFBO0FDT0Y7O0FETEE7RUFDQyxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNRRDs7QUROQTtFQUNDLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLGtCQUFBO0FDU0Q7O0FEUEEsa0NBQUE7O0FBQ0E7RUFDQztJQUNDLHNCQUFBO0VDVUE7O0VEUkQ7SUFDQyxzQkFBQTtJQUNBLGdCQUFBO0lBQ0Esa0JBQUE7RUNXQTs7RURURDtJQUNDLGtCQUFBO0VDWUE7O0VEVkQ7SUFDQyxXQUFBO0lBQ0EsZ0JBQUE7SUFDQSxVQUFBO0lBQ0EsY0FBQTtFQ2FBO0FBQ0Y7O0FEVEE7RUFDQyxvRkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0FDV0Q7O0FEVEU7RUFDRCxpQkFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7QUNZRDs7QURSQTtFQUNDO0lBQ0UsbUZBQUE7RUNXRDtBQUNGOztBRFBBLGtDQUFBOztBQUNBO0VBQ0M7SUFDRSwrQkFBQTtFQ1NEOztFRFBEO0lBQ0Usc0JBQUE7SUFDQSxlQUFBO0VDVUQ7O0VEUkQ7SUFDRSxnQkFBQTtJQUNBLHNCQUFBO0VDV0Q7O0VEVEQ7SUFDRSxlQUFBO0VDWUQ7O0VEVkQ7SUFDRSx3QkFBQTtFQ2FEOztFRFhEO0lBQ0Usb0ZBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSx3QkFBQTtFQ2NEO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3Nob2VyZXN0b3JhdGlvbi9zaG9lcmVzdG9yYXRpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWluaXRpYWwge1xyXG5cdGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxufVxyXG4jbWFpbiB7XHJcbiAgcGFkZGluZzogNDBweCAxNXB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG5cdHBhZGRpbmc6IDE1cHg7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgcmdiYSgwLDAsMCwwLjA1KTtcclxuXHRib3JkZXItcmFkaXVzOiAycHg7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0Ym94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsMC4yKSwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gaHNsYSgwLDAlLDY5LjQlLC4yKTtcclxuXHRcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpK2xpIHtcclxuXHRtYXJnaW46IDIwcHggMCAwIDA7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IHtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gNDE1cHgpO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLmFib3V0LXNob2UtdGV4dCAuaDItYmFzZS1oZWFkaW5nLCAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmltZy1yaWdodCB7XHJcblx0bWFyZ2luOiAwIDAgMTJweDtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC50ZXh0LXJpZ2h0IGEge1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHR0cmFuc2l0aW9uOiBhbGwgMC41cztcclxuICBjb2xvcjogI2ZmZjtcclxuICBiYWNrZ3JvdW5kOiAjMDAwO1xyXG4gIHBhZGRpbmc6IDRweCAxMnB4O1xyXG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMDA7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAudGV4dC1yaWdodCBhOmhvdmVyIHtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG4gIHBhZGRpbmc6IDRweCAxOHB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcclxuXHR3aWR0aDogNDE1cHg7XHJcblx0aGVpZ2h0OiAyNTBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHBhZGRpbmc6IDAgMCAwIDE1cHg7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUgaW1nIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRoZWlnaHQ6IDI1MHB4O1xyXG5cdG9iamVjdC1maXQ6IGNvdmVyO1xyXG5cdGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcblx0LmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdH1cclxuXHQuYWJvdXQtc2hvZS10ZXh0IHtcclxuXHRcdHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcblx0XHRtYXJnaW46IDAgMCAxNXB4O1xyXG5cdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdH1cclxuXHQuYWJvdXQtc2hvZS10ZXh0IC50ZXh0LXJpZ2h0IHtcclxuXHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHR9XHJcblx0LmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRtYXgtd2lkdGg6IDQwMHB4O1xyXG5cdFx0cGFkZGluZzogMDtcclxuXHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdH1cclxufVxyXG5cclxuXHJcbi5hYm91dC1iZyB7XHJcblx0YmFja2dyb3VuZDogIzAwMCB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIuanBnJykgbm8tcmVwZWF0IGZpeGVkIHRvcCAtMTAwcHggY2VudGVyIC8gMTAwJSA7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGZvbnQtc2l6ZTogMzJweDtcclxuXHRsaW5lLWhlaWdodDogMzZweDtcclxuXHRjb2xvcjogIzAwMDtcclxuXHRsZXR0ZXItc3BhY2luZzogMnB4O1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0cGFkZGluZzogMTUwcHggMTVweCA1MHB4O1xyXG4gIH1cclxuICAuYWJvdXQtYmcgc3BhbiB7XHJcblx0cGFkZGluZzogNXB4IDE1cHg7XHJcblx0YmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjgpO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcblxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG5cdC5hYm91dC1iZyB7XHJcblx0ICBiYWNrZ3JvdW5kOiAjMDAwIHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9qb3JkYW5zaG9lc2Jhbm5lci5qcGcnKSBuby1yZXBlYXQgZml4ZWQgdG9wIDMwcHggY2VudGVyIC8gY292ZXI7XHJcblx0fVxyXG4gIH1cclxuXHJcblxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcblx0I21haW4ge1xyXG5cdCAgcGFkZGluZzogMTBweCAxMHB4IDAgIWltcG9ydGFudDtcclxuXHR9XHJcblx0LmgyLWJhc2UtaGVhZGluZyB7XHJcblx0ICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdCAgZm9udC1zaXplOiAxOHB4O1xyXG5cdH1cclxuXHQuaDItYmFzZS1oZWFkaW5nIHAge1xyXG5cdCAgbWFyZ2luOiAwIDAgMTBweDtcclxuXHQgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0fVxyXG5cdC5oMi1iYXNlLWhlYWRpbmcgcCBpbWcge1xyXG5cdCAgbWFyZ2luOiAwIDAgNnB4O1xyXG5cdH1cclxuXHQubXQtODAge1xyXG5cdCAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xyXG5cdH1cclxuXHQuYWJvdXQtYmcge1xyXG5cdCAgYmFja2dyb3VuZDogIzAwMCB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIuanBnJykgbm8tcmVwZWF0IGZpeGVkIHRvcCAtNzBweCBjZW50ZXIgLyBjb3ZlcjtcclxuXHQgIGZvbnQtc2l6ZTogMThweDtcclxuXHQgIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG5cdCAgcGFkZGluZzogMTAwcHggMTVweCA1MHB4O1xyXG5cdH1cclxuICB9IiwiLmhlYWRlci1pbml0aWFsIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjgpO1xufVxuXG4jbWFpbiB7XG4gIHBhZGRpbmc6IDQwcHggMTVweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYm94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSByZ2JhKDE3NywgMTc3LCAxNzcsIDAuMiksIDAuNXJlbSAwLjFyZW0gMC45Mzc1cmVtIHJnYmEoMTc3LCAxNzcsIDE3NywgMC4yKTtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSArIGxpIHtcbiAgbWFyZ2luOiAyMHB4IDAgMCAwO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQge1xuICB3aWR0aDogY2FsYygxMDAlIC0gNDE1cHgpO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmgyLWJhc2UtaGVhZGluZywgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSAuYWJvdXQtc2hvZS10ZXh0IC5pbWctcmlnaHQge1xuICBtYXJnaW46IDAgMCAxMnB4O1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC50ZXh0LXJpZ2h0IGEge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzO1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgcGFkZGluZzogNHB4IDEycHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMDA7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgLnRleHQtcmlnaHQgYTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjMDAwO1xuICBwYWRkaW5nOiA0cHggMThweDtcbn1cblxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUge1xuICB3aWR0aDogNDE1cHg7XG4gIGhlaWdodDogMjUwcHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHBhZGRpbmc6IDAgMCAwIDE1cHg7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDI1MHB4O1xuICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkxcHgpIHtcbiAgLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5hYm91dC1zaG9lLXRleHQge1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAwIDAgMTVweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuYWJvdXQtc2hvZS10ZXh0IC50ZXh0LXJpZ2h0IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWF4LXdpZHRoOiA0MDBweDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG59XG4uYWJvdXQtYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwIHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCB0b3AgLTEwMHB4IGNlbnRlci8xMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAzMnB4O1xuICBsaW5lLWhlaWdodDogMzZweDtcbiAgY29sb3I6ICMwMDA7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nOiAxNTBweCAxNXB4IDUwcHg7XG59XG5cbi5hYm91dC1iZyBzcGFuIHtcbiAgcGFkZGluZzogNXB4IDE1cHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xuICAuYWJvdXQtYmcge1xuICAgIGJhY2tncm91bmQ6ICMwMDAgdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9qb3JkYW5zaG9lc2Jhbm5lci5qcGdcIikgbm8tcmVwZWF0IGZpeGVkIHRvcCAzMHB4IGNlbnRlci9jb3ZlcjtcbiAgfVxufVxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09Ki9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICNtYWluIHtcbiAgICBwYWRkaW5nOiAxMHB4IDEwcHggMCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmgyLWJhc2UtaGVhZGluZyB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gIH1cblxuICAuaDItYmFzZS1oZWFkaW5nIHAge1xuICAgIG1hcmdpbjogMCAwIDEwcHg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5oMi1iYXNlLWhlYWRpbmcgcCBpbWcge1xuICAgIG1hcmdpbjogMCAwIDZweDtcbiAgfVxuXG4gIC5tdC04MCB7XG4gICAgbWFyZ2luLXRvcDogMCAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmFib3V0LWJnIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwIHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXNiYW5uZXIuanBnXCIpIG5vLXJlcGVhdCBmaXhlZCB0b3AgLTcwcHggY2VudGVyL2NvdmVyO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICBwYWRkaW5nOiAxMDBweCAxNXB4IDUwcHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/@auth/components/shoerestoration/shoerestoration.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/@auth/components/shoerestoration/shoerestoration.component.ts ***!
  \*******************************************************************************/
/*! exports provided: NgxShoeRestorationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxShoeRestorationComponent", function() { return NgxShoeRestorationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NgxShoeRestorationComponent = /** @class */ (function () {
    function NgxShoeRestorationComponent() {
    }
    NgxShoeRestorationComponent.prototype.ngOnInit = function () { };
    NgxShoeRestorationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-shoe-restoration',
            template: __webpack_require__(/*! raw-loader!./shoerestoration.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/shoerestoration/shoerestoration.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./shoerestoration.component.scss */ "./src/app/@auth/components/shoerestoration/shoerestoration.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NgxShoeRestorationComponent);
    return NgxShoeRestorationComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/sign-up/sign-up.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/@auth/components/sign-up/sign-up.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n.signin-page {\n  background: #000 url('jordanshoesbanner.png') no-repeat fixed center center/cover;\n  width: 100%;\n  height: 100vh;\n  box-sizing: border-box;\n  padding: 75px 30px 30px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.center-form {\n  padding: 20px 15px;\n  background: rgba(255, 255, 255, 0.84);\n  border-radius: 4px;\n  width: 600px;\n}\n\n.center-form form {\n  display: flex;\n  flex-wrap: wrap;\n  margin: 20px 0 0 0;\n}\n\n.center-form form .form-group {\n  width: 48%;\n  margin: 0 1% 20px;\n}\n\n.check-user {\n  width: 100% !important;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.check-user label {\n  display: flex;\n  align-items: center;\n  margin: 0 4% 0 0;\n  font-size: 16px;\n  letter-spacing: 1px;\n}\n\n.check-user label input {\n  margin: 2px 6px 0 0;\n}\n\n.login-button {\n  width: 100% !important;\n  text-align: center;\n}\n\n.login-button button {\n  padding: 0 15px;\n  height: 34px;\n  background: #000;\n  color: #fff;\n  border: none;\n  border-radius: 0;\n  line-height: 33px;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  cursor: pointer;\n  width: 120px;\n}\n\n.backlinks {\n  text-align: center;\n  width: 100% !important;\n}\n\n.backlinks a {\n  color: #0f5fde;\n  font-size: 12px;\n  text-transform: uppercase;\n  font-weight: bold;\n}\n\ntextarea {\n  height: 34px !important;\n  resize: none;\n}\n\n/* Responsive ====================*/\n\n@media screen and (max-width: 768px) {\n  .signin-page {\n    min-height: 100vh;\n    height: 100%;\n  }\n}\n\n@media screen and (max-width: 500px) {\n  .center-form form {\n    flex-direction: column;\n  }\n\n  .center-form form .form-group {\n    width: 100%;\n    margin: 0 0 20px;\n  }\n\n  .center-form {\n    width: 100%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9zaWduLXVwL0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFxzaWduLXVwXFxzaWduLXVwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3NpZ24tdXAvc2lnbi11cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLDhCQUFBO0FDQ0Q7O0FEQ0E7RUFDQyxpRkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDRUQ7O0FEQUE7RUFDQyxrQkFBQTtFQUNBLHFDQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDR0Q7O0FEREE7RUFDQyxhQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDSUQ7O0FERkE7RUFDQyxVQUFBO0VBQ0EsaUJBQUE7QUNLRDs7QURIQTtFQUNDLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUNNRDs7QURKQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDT0Q7O0FETEE7RUFDQyxtQkFBQTtBQ1FEOztBRE5BO0VBQ0Msc0JBQUE7RUFDQSxrQkFBQTtBQ1NEOztBRFBBO0VBQ0MsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ1VEOztBRFJBO0VBQ0Msa0JBQUE7RUFDQSxzQkFBQTtBQ1dEOztBRFRBO0VBQ0MsY0FBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0FDWUQ7O0FEVkE7RUFDQyx1QkFBQTtFQUNBLFlBQUE7QUNhRDs7QURYQSxtQ0FBQTs7QUFDQTtFQUNBO0lBQ0MsaUJBQUE7SUFDQSxZQUFBO0VDY0M7QUFDRjs7QURaQTtFQUNBO0lBQ0Msc0JBQUE7RUNjQzs7RURaRjtJQUNJLFdBQUE7SUFDQSxnQkFBQTtFQ2VGOztFRGJGO0lBQ0MsV0FBQTtFQ2dCQztBQUNGIiwiZmlsZSI6InNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy9zaWduLXVwL3NpZ24tdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWluaXRpYWwge1xyXG5cdGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxufVxyXG4uc2lnbmluLXBhZ2Uge1xyXG5cdGJhY2tncm91bmQ6ICMwMDAgdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLnBuZycpIG5vLXJlcGVhdCBmaXhlZCBjZW50ZXIgY2VudGVyIC8gY292ZXIgO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGhlaWdodDogMTAwdmg7XHJcblx0Ym94LXNpemluZzogYm9yZGVyLWJveDtcclxuXHRwYWRkaW5nOiA3NXB4IDMwcHggMzBweDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLmNlbnRlci1mb3JtIHtcclxuXHRwYWRkaW5nOiAyMHB4IDE1cHg7XHJcblx0YmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjg0KTtcclxuXHRib3JkZXItcmFkaXVzOiA0cHg7XHJcblx0d2lkdGg6IDYwMHB4O1xyXG59XHJcbi5jZW50ZXItZm9ybSBmb3JtIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtd3JhcDogd3JhcDtcclxuXHRtYXJnaW46IDIwcHggMCAwIDA7XHJcbn1cclxuLmNlbnRlci1mb3JtIGZvcm0gLmZvcm0tZ3JvdXAge1xyXG5cdHdpZHRoOiA0OCU7XHJcblx0bWFyZ2luOiAwIDElIDIwcHg7XHJcbn1cclxuLmNoZWNrLXVzZXIge1xyXG5cdHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi5jaGVjay11c2VyIGxhYmVsIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0bWFyZ2luOiAwIDQlIDAgMDtcclxuXHRmb250LXNpemU6IDE2cHg7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDFweDtcclxufVxyXG4uY2hlY2stdXNlciBsYWJlbCBpbnB1dCB7XHJcblx0bWFyZ2luOiAycHggNnB4IDAgMDtcclxufVxyXG4ubG9naW4tYnV0dG9uIHtcclxuXHR3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ubG9naW4tYnV0dG9uIGJ1dHRvbiB7XHJcblx0cGFkZGluZzogMCAxNXB4O1xyXG5cdGhlaWdodDogMzRweDtcclxuXHRiYWNrZ3JvdW5kOiAjMDAwO1xyXG5cdGNvbG9yOiAjZmZmO1xyXG5cdGJvcmRlcjogbm9uZTtcclxuXHRib3JkZXItcmFkaXVzOiAwO1xyXG5cdGxpbmUtaGVpZ2h0OiAzM3B4O1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDFweDtcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcblx0d2lkdGg6IDEyMHB4O1xyXG59XHJcbi5iYWNrbGlua3Mge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHR3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG59XHJcbi5iYWNrbGlua3MgYSB7XHJcblx0Y29sb3I6ICMwZjVmZGU7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxudGV4dGFyZWEge1xyXG5cdGhlaWdodDogMzRweCAhaW1wb3J0YW50O1xyXG5cdHJlc2l6ZTogbm9uZTtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09Ki9cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG4uc2lnbmluLXBhZ2Uge1xyXG5cdG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG5cdGhlaWdodDogMTAwJTtcclxufVxyXG59XHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDUwMHB4KXtcclxuLmNlbnRlci1mb3JtIGZvcm0ge1xyXG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuLmNlbnRlci1mb3JtIGZvcm0gLmZvcm0tZ3JvdXAge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xyXG59XHJcbi5jZW50ZXItZm9ybSB7XHJcblx0d2lkdGg6IDEwMCU7XHJcbn1cclxufSIsIi5oZWFkZXItaW5pdGlhbCB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC44KTtcbn1cblxuLnNpZ25pbi1wYWdlIHtcbiAgYmFja2dyb3VuZDogIzAwMCB1cmwoXCIuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLnBuZ1wiKSBuby1yZXBlYXQgZml4ZWQgY2VudGVyIGNlbnRlci9jb3ZlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIHBhZGRpbmc6IDc1cHggMzBweCAzMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmNlbnRlci1mb3JtIHtcbiAgcGFkZGluZzogMjBweCAxNXB4O1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuODQpO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIHdpZHRoOiA2MDBweDtcbn1cblxuLmNlbnRlci1mb3JtIGZvcm0ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIG1hcmdpbjogMjBweCAwIDAgMDtcbn1cblxuLmNlbnRlci1mb3JtIGZvcm0gLmZvcm0tZ3JvdXAge1xuICB3aWR0aDogNDglO1xuICBtYXJnaW46IDAgMSUgMjBweDtcbn1cblxuLmNoZWNrLXVzZXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmNoZWNrLXVzZXIgbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDAgNCUgMCAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5cbi5jaGVjay11c2VyIGxhYmVsIGlucHV0IHtcbiAgbWFyZ2luOiAycHggNnB4IDAgMDtcbn1cblxuLmxvZ2luLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ2luLWJ1dHRvbiBidXR0b24ge1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGhlaWdodDogMzRweDtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgbGluZS1oZWlnaHQ6IDMzcHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgd2lkdGg6IDEyMHB4O1xufVxuXG4uYmFja2xpbmtzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4uYmFja2xpbmtzIGEge1xuICBjb2xvcjogIzBmNWZkZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxudGV4dGFyZWEge1xuICBoZWlnaHQ6IDM0cHggIWltcG9ydGFudDtcbiAgcmVzaXplOiBub25lO1xufVxuXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09Ki9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5zaWduaW4tcGFnZSB7XG4gICAgbWluLWhlaWdodDogMTAwdmg7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1MDBweCkge1xuICAuY2VudGVyLWZvcm0gZm9ybSB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgfVxuXG4gIC5jZW50ZXItZm9ybSBmb3JtIC5mb3JtLWdyb3VwIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xuICB9XG5cbiAgLmNlbnRlci1mb3JtIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/@auth/components/sign-up/sign-up.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/@auth/components/sign-up/sign-up.component.ts ***!
  \***************************************************************/
/*! exports provided: NgxSignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxSignUpComponent", function() { return NgxSignUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../@core/backend/common/services/utils.service */ "./src/app/@core/backend/common/services/utils.service.ts");
/* harmony import */ var _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../@core/backend/common/services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");








var NgxSignUpComponent = /** @class */ (function () {
    function NgxSignUpComponent(utilsService, cd, fb, router, authenticationService, UsersService, toasterService) {
        this.utilsService = utilsService;
        this.cd = cd;
        this.fb = fb;
        this.router = router;
        this.authenticationService = authenticationService;
        this.UsersService = UsersService;
        this.toasterService = toasterService;
        this.formGroup = this.fb.group({
            first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            cpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            role: ['0', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            zipcode: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
    }
    NgxSignUpComponent.prototype.ngOnInit = function () { };
    NgxSignUpComponent.prototype.onSubmit = function () {
        var _this = this;
        var user_details = {
            user: this.formGroup.value
        };
        this.UsersService.create(user_details).subscribe(function (res) {
            if (res.status) {
                _this.toasterService.success("User Registered successfully !!", res.message);
                _this.login();
            }
            else {
                _this.toasterService.error("User Not Registered", res.message);
            }
        });
    };
    NgxSignUpComponent.prototype.login = function () {
        var _this = this;
        var user_details = { user: this.formGroup.value };
        this.authenticationService.login(user_details).subscribe(function (result) {
            if (result.status) {
                localStorage.setItem('token', result.data.registration_token);
                localStorage.setItem('userid', result.data.id);
                localStorage.setItem('user', JSON.stringify(result.data));
                localStorage.setItem('role', result.data.role);
                _this.router.navigate(['/user/profile']);
            }
            else {
                _this.toasterService.error("Login unsuccessful !!", result.message);
            }
        });
    };
    ;
    NgxSignUpComponent.prototype.changeUserRole = function (role) {
        this.role = role;
    };
    NgxSignUpComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"] }
    ]; };
    NgxSignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-contact-us',
            template: __webpack_require__(/*! raw-loader!./sign-up.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/sign-up/sign-up.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./sign-up.component.scss */ "./src/app/@auth/components/sign-up/sign-up.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_6__["UsersService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrService"]])
    ], NgxSignUpComponent);
    return NgxSignUpComponent;
}());



/***/ }),

/***/ "./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-initial {\n  background: rgba(0, 0, 0, 0.8);\n}\n\n.about-bg {\n  background: #000 url('jordanshoesbanner.png') no-repeat fixed top -150px center/100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 32px;\n  line-height: 36px;\n  color: #fff;\n  letter-spacing: 2px;\n  font-weight: bold;\n  text-transform: uppercase;\n  padding: 150px 15px 50px;\n}\n\n#main {\n  padding: 40px 15px;\n}\n\n.about-shoe-servicec-list {\n  display: flex;\n  flex-wrap: wrap;\n}\n\n.about-shoe-servicec-list li {\n  width: 47%;\n  margin: 0 1.5% 20px;\n  padding: 15px;\n  border: 1px solid rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem #fff, 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.6);\n}\n\n.about-shoe-servicec-list li figure {\n  width: 120px;\n  height: 120px;\n  overflow: hidden;\n  border-radius: 50%;\n  padding: 5px;\n  border: 5px solid #fff;\n  background: #fff;\n  margin: 20px auto 30px;\n  box-shadow: -0.5rem -0.5rem 0.9375rem #fff, 0.5rem 0.1rem 0.9375rem rgba(177, 177, 177, 0.6);\n}\n\n.about-shoe-servicec-list li figure img {\n  width: 120px;\n  height: 120px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.about-shoe-servicec-list li .about-shoe-text .h2-base-heading {\n  margin: 0 0 12px;\n}\n\n/* Responsive ===================*/\n\n@media screen and (max-width: 991px) {\n  .about-shoe-servicec-list li {\n    width: 100%;\n    margin: 0 0 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy93aGF0LWlzLWZpeG15a2l4L0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXEBhdXRoXFxjb21wb25lbnRzXFx3aGF0LWlzLWZpeG15a2l4XFx3aGF0LWlzLWZpeG15a2l4LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9AYXV0aC9jb21wb25lbnRzL3doYXQtaXMtZml4bXlraXgvd2hhdC1pcy1maXhteWtpeC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLDhCQUFBO0FDQ0Q7O0FEQ0E7RUFDQyxvRkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLHdCQUFBO0FDRUQ7O0FEQUE7RUFDRSxrQkFBQTtBQ0dGOztBRERBO0VBQ0MsYUFBQTtFQUNBLGVBQUE7QUNJRDs7QURGQTtFQUNDLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxxQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsNEZBQUE7QUNLRDs7QURIQTtFQUNDLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSw0RkFBQTtBQ01EOztBREpBO0VBQ0MsWUFBQTtFQUNBLGFBQUE7RUFDQSxvQkFBQTtLQUFBLGlCQUFBO0FDT0Q7O0FETEE7RUFDQyxnQkFBQTtBQ1FEOztBRE5BLGtDQUFBOztBQUNBO0VBQ0E7SUFDSSxXQUFBO0lBQ0EsZ0JBQUE7RUNTRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvQGF1dGgvY29tcG9uZW50cy93aGF0LWlzLWZpeG15a2l4L3doYXQtaXMtZml4bXlraXguY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLWluaXRpYWwge1xyXG5cdGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44KTtcclxufVxyXG4uYWJvdXQtYmcge1xyXG5cdGJhY2tncm91bmQ6ICMwMDAgdXJsKCcuLi8uLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2pvcmRhbnNob2VzYmFubmVyLnBuZycpIG5vLXJlcGVhdCBmaXhlZCB0b3AgLTE1MHB4IGNlbnRlciAvIDEwMCUgO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRmb250LXNpemU6IDMycHg7XHJcblx0bGluZS1oZWlnaHQ6IDM2cHg7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0bGV0dGVyLXNwYWNpbmc6IDJweDtcclxuXHRmb250LXdlaWdodDogYm9sZDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG5cdHBhZGRpbmc6IDE1MHB4IDE1cHggNTBweDtcclxufVxyXG4jbWFpbiB7XHJcbiAgcGFkZGluZzogNDBweCAxNXB4O1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3Qge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0ZmxleC13cmFwOiB3cmFwO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG5cdHdpZHRoOiA0NyU7XHJcblx0bWFyZ2luOiAwIDEuNSUgMjBweDtcclxuXHRwYWRkaW5nOiAxNXB4O1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwwLDAsMC4wNSk7XHJcblx0Ym9yZGVyLXJhZGl1czogMnB4O1xyXG5cdGJveC1zaGFkb3c6IC0wLjVyZW0gLTAuNXJlbSAwLjkzNzVyZW0gI2ZmZiwgMC41cmVtIDAuMTAwcmVtIDAuOTM3NXJlbSBoc2xhKDAsMCUsNjkuNCUsLjYpO1xyXG59XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkgZmlndXJlIHtcclxuXHR3aWR0aDogMTIwcHg7XHJcblx0aGVpZ2h0OiAxMjBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRwYWRkaW5nOiA1cHg7XHJcblx0Ym9yZGVyOiA1cHggc29saWQgI2ZmZjtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdG1hcmdpbjogMjBweCBhdXRvIDMwcHg7XHJcblx0Ym94LXNoYWRvdzogLTAuNXJlbSAtMC41cmVtIDAuOTM3NXJlbSAjZmZmLCAwLjVyZW0gMC4xMDByZW0gMC45Mzc1cmVtIGhzbGEoMCwwJSw2OS40JSwuNik7XHJcbn1cclxuLmFib3V0LXNob2Utc2VydmljZWMtbGlzdCBsaSBmaWd1cmUgaW1nIHtcclxuXHR3aWR0aDogMTIwcHg7XHJcblx0aGVpZ2h0OiAxMjBweDtcclxuXHRvYmplY3QtZml0OiBjb3ZlcjtcclxufVxyXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmgyLWJhc2UtaGVhZGluZyB7XHJcblx0bWFyZ2luOiAwIDAgMTJweDtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xyXG59XHJcbn0iLCIuaGVhZGVyLWluaXRpYWwge1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG59XG5cbi5hYm91dC1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDAgdXJsKFwiLi4vLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9qb3JkYW5zaG9lc2Jhbm5lci5wbmdcIikgbm8tcmVwZWF0IGZpeGVkIHRvcCAtMTUwcHggY2VudGVyLzEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmb250LXNpemU6IDMycHg7XG4gIGxpbmUtaGVpZ2h0OiAzNnB4O1xuICBjb2xvcjogI2ZmZjtcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmc6IDE1MHB4IDE1cHggNTBweDtcbn1cblxuI21haW4ge1xuICBwYWRkaW5nOiA0MHB4IDE1cHg7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG5cbi5hYm91dC1zaG9lLXNlcnZpY2VjLWxpc3QgbGkge1xuICB3aWR0aDogNDclO1xuICBtYXJnaW46IDAgMS41JSAyMHB4O1xuICBwYWRkaW5nOiAxNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMDUpO1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIGJveC1zaGFkb3c6IC0wLjVyZW0gLTAuNXJlbSAwLjkzNzVyZW0gI2ZmZiwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjYpO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSB7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbjogMjBweCBhdXRvIDMwcHg7XG4gIGJveC1zaGFkb3c6IC0wLjVyZW0gLTAuNXJlbSAwLjkzNzVyZW0gI2ZmZiwgMC41cmVtIDAuMXJlbSAwLjkzNzVyZW0gcmdiYSgxNzcsIDE3NywgMTc3LCAwLjYpO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIGZpZ3VyZSBpbWcge1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogMTIwcHg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4uYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIC5hYm91dC1zaG9lLXRleHQgLmgyLWJhc2UtaGVhZGluZyB7XG4gIG1hcmdpbjogMCAwIDEycHg7XG59XG5cbi8qIFJlc3BvbnNpdmUgPT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAuYWJvdXQtc2hvZS1zZXJ2aWNlYy1saXN0IGxpIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IDAgMCAyMHB4O1xuICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.ts ***!
  \*********************************************************************************/
/*! exports provided: NgxFixmyKixComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxFixmyKixComponent", function() { return NgxFixmyKixComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NgxFixmyKixComponent = /** @class */ (function () {
    function NgxFixmyKixComponent() {
    }
    NgxFixmyKixComponent.prototype.ngOnInit = function () { };
    NgxFixmyKixComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ngx-what-is-fixmykix',
            template: __webpack_require__(/*! raw-loader!./what-is-fixmykix.component.html */ "./node_modules/raw-loader/index.js!./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            styles: [__webpack_require__(/*! ./what-is-fixmykix.component.scss */ "./src/app/@auth/components/what-is-fixmykix/what-is-fixmykix.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NgxFixmyKixComponent);
    return NgxFixmyKixComponent;
}());



/***/ }),

/***/ "./src/app/layout/footer.component.scss":
/*!**********************************************!*\
  !*** ./src/app/layout/footer.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "footer {\n  background: #515256;\n}\n\n#footer {\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 20px 0 40px;\n}\n\n.inner-footer {\n  margin: 0;\n}\n\n.inner-footer .col-md-3 h2 {\n  text-transform: uppercase;\n  font-family: \"Comfortaa\", cursive;\n  letter-spacing: -2px;\n  text-align: center;\n  color: #fff;\n  padding: 0 10px 10px;\n  border-bottom: 1px solid #fff;\n  font-weight: normal;\n}\n\n.inner-footer .col-md-3 ul {\n  margin: 20px 0 0 0;\n  text-align: center;\n}\n\n.inner-footer .col-md-3 ul li a {\n  text-transform: capitalize;\n  text-transform: uppercase;\n  font-family: \"Comfortaa\", cursive;\n  letter-spacing: -1px;\n  font-size: 20px;\n  color: #fff;\n  margin: 0 0 15px;\n}\n\n.order-a-repair {\n  background: #e8b13d;\n  color: #000;\n  font-family: \"Comfortaa\", cursive;\n  text-transform: uppercase;\n  padding: 20px 25px 16px;\n  display: inline-block;\n  letter-spacing: 0.5px;\n  font-weight: bold;\n  font-size: 16px;\n  margin: 0 auto;\n}\n\n.social-icons {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.social-icons li a {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  text-align: center;\n  background: #e8b13d;\n  margin: 0 10px !important;\n  width: 40px;\n  height: 40px;\n  box-sizing: border-box;\n  padding: 10px;\n}\n\n.copyright {\n  font-size: 12px;\n  background: black;\n  color: #fff;\n  padding: 10px 15px;\n  text-align: center;\n}\n\n/* Responsive ======================*/\n\n@media screen and (max-width: 768px) {\n  #footer {\n    padding-bottom: 20px;\n  }\n\n  .inner-footer .col-md-3 h2 {\n    font-size: 24px;\n    border-top: 1px solid #fff;\n    padding: 10px;\n  }\n\n  .inner-footer .col-md-3 ul li a {\n    font-size: 15px;\n    margin: 0 0 10px;\n  }\n\n  .order-a-repair {\n    padding: 14px 15px 10px;\n    margin: 0 0 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXGxheW91dFxcZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQW9KQTtFQUNDLG1CQUFBO0FDbkpEOztBRHFKQTtFQUNDLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FDbEpEOztBRG9KQTtFQUNDLFNBQUE7QUNqSkQ7O0FEbUpBO0VBQ0UseUJBQUE7RUFDQSxpQ0FBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0Esb0JBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FDaEpGOztBRGtKQTtFQUNDLGtCQUFBO0VBQ0Esa0JBQUE7QUMvSUQ7O0FEaUpBO0VBQ0MsMEJBQUE7RUFDQyx5QkFBQTtFQUNBLGlDQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDOUlGOztBRGdKQTtFQUNDLG1CQUFBO0VBQ0EsV0FBQTtFQUNDLGlDQUFBO0VBQ0EseUJBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDN0lGOztBRCtJQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDNUlEOztBRDhJQTtFQUNDLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7QUMzSUQ7O0FENklBO0VBQ0MsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUMxSUQ7O0FENElBLHFDQUFBOztBQUNBO0VBQ0E7SUFDQyxvQkFBQTtFQ3pJQzs7RUQySUY7SUFDQyxlQUFBO0lBQ0EsMEJBQUE7SUFDQyxhQUFBO0VDeElBOztFRDBJRjtJQUNDLGVBQUE7SUFDQSxnQkFBQTtFQ3ZJQzs7RUR5SUY7SUFDQyx1QkFBQTtJQUNBLGdCQUFBO0VDdElDO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gI2Zvb3RlciB7XHJcbi8vICAgYmFja2dyb3VuZDogIzAwMCB1cmwoJy4uLy4uL2Fzc2V0cy9pbWFnZXMvam9yZGFuc2hvZXMxNC5qcGcnKSBuby1yZXBlYXQgc2Nyb2xsIHJpZ2h0IDA7XHJcbi8vICAgcGFkZGluZzogMjBweCAxMDBweCAwcHg7XHJcbi8vICAgY29sb3I6ICNmZmY7XHJcbi8vIH1cclxuLy8gLmlubmVyLWZvb3RlciB7XHJcbi8vICAgZGlzcGxheTogZmxleDtcclxuLy8gICBmbGV4LXdyYXA6IHdyYXA7XHJcbi8vIH1cclxuLy8gLmZvb3Rib3gge1xyXG4vLyAgIHBhZGRpbmc6IDIwcHg7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuLy8gICB3aWR0aDogNDAlO1xyXG4vLyAgIG1hcmdpbjogMCAzLjMzJSAyNXB4O1xyXG4vLyB9XHJcbi8vIC5mb290Ym94IGgzIHtcclxuLy8gICBtYXJnaW46IDAgMCAxNXB4O1xyXG4vLyB9XHJcbi8vIC5mb290Ym94IHAge1xyXG4vLyAgIG1hcmdpbjogMCAwIDEwcHg7XHJcbi8vIH1cclxuLy8gLmZvb3QtYm94LTAxIHAgLmZhIHtcclxuLy8gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbi8vICAgd2lkdGg6IDIwcHg7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC5mb290LWJveC0wMyB7XHJcbi8vICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgbWFyZ2luLWJvdHRvbTogMDtcclxuLy8gfVxyXG4vLyAvKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09PT0qL1xyXG4vLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCl7XHJcbi8vICNmb290ZXIge1xyXG4vLyAgIHBhZGRpbmc6IDE1cHg7XHJcbi8vIH1cclxuLy8gLmZvb3Rib3gge1xyXG4vLyAgIHBhZGRpbmc6IDIwcHggMCAxMHB4O1xyXG4vLyAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgIG1hcmdpbjogMDtcclxuLy8gfVxyXG4vLyAuZm9vdGJveCBoMyB7XHJcbi8vICAgZm9udC1zaXplOiAyMHB4O1xyXG4vLyAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG4vLyAgIG1hcmdpbjogMCAwIDEwcHg7XHJcbi8vIH1cclxuLy8gfVxyXG4vLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA1NTBweCl7XHJcbi8vICNmb290ZXIge1xyXG4vLyAgIHBhZGRpbmc6IDE1cHggMTVweCAwO1xyXG4vLyB9XHJcbi8vIC5mb290Ym94IHVsIHtcclxuLy8gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gfVxyXG4vLyAuZm9vdGJveCB7XHJcbi8vICAgcGFkZGluZzogMTVweDtcclxuLy8gICB3aWR0aDogMTAwJTtcclxuLy8gICBtYXJnaW46IDAgMCAxNXB4O1xyXG4vLyAgIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMC44Nik7XHJcbi8vICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyNTUsMjU1LDI1NSwwLjEyKTtcclxuLy8gICBib3gtc2hhZG93OiAwIDJweCAxMHB4IC04cHggI2ZmZjtcclxuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gfVxyXG5cclxuLy8gZm9vdGVyIHtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjNTE1MjU2O1xyXG4vLyB9XHJcbi8vICNmb290ZXIge1xyXG4vLyBcdG1heC13aWR0aDogMTIwMHB4O1xyXG4vLyBcdG1hcmdpbjogMCBhdXRvO1xyXG4vLyBcdHBhZGRpbmc6IDIwcHggMCA0MHB4O1xyXG4vLyB9XHJcbi8vIC5pbm5lci1mb290ZXIge1xyXG4vLyBcdG1hcmdpbjogMDtcclxuLy8gfVxyXG4vLyAuaW5uZXItZm9vdGVyIC5jb2wtbWQtMyBoMiB7XHJcbi8vICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuLy8gICBmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbi8vICAgbGV0dGVyLXNwYWNpbmc6IC0ycHg7XHJcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4vLyAgIGNvbG9yOiAjZmZmO1xyXG4vLyAgIHBhZGRpbmc6IDAgMTBweCAxMHB4O1xyXG4vLyAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmO1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbi8vIH1cclxuLy8gLmlubmVyLWZvb3RlciAuY29sLW1kLTMgdWwge1xyXG4vLyBcdG1hcmdpbjogMjBweCAwIDAgMDtcclxuLy8gXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmlubmVyLWZvb3RlciAuY29sLW1kLTMgdWwgbGkgYSB7XHJcbi8vIFx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbi8vICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuLy8gICBmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbi8vICAgbGV0dGVyLXNwYWNpbmc6IC0xcHg7XHJcbi8vICAgZm9udC1zaXplOiAyMHB4O1xyXG4vLyAgIGNvbG9yOiAjZmZmO1xyXG4vLyAgIG1hcmdpbjogMCAwIDE1cHg7XHJcbi8vIH1cclxuLy8gLm9yZGVyLWEtcmVwYWlyIHtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjZThiMTNkO1xyXG4vLyBcdGNvbG9yOiAjMDAwO1xyXG4vLyAgIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuLy8gICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4vLyAgIHBhZGRpbmc6IDIwcHggMjVweCAxNnB4O1xyXG4vLyAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbi8vICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vICAgZm9udC1zaXplOiAxNnB4O1xyXG4vLyAgIG1hcmdpbjogMCBhdXRvXHJcbi8vIH1cclxuLy8gLnNvY2lhbC1pY29ucyB7XHJcbi8vIFx0ZGlzcGxheTogZmxleDtcclxuLy8gXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG4vLyBcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC5zb2NpYWwtaWNvbnMgbGkgYSB7XHJcbi8vIFx0ZGlzcGxheTogZmxleDtcclxuLy8gXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG4vLyBcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4vLyBcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjZThiMTNkO1xyXG4vLyBcdG1hcmdpbjogMCAxMHB4ICFpbXBvcnRhbnQ7XHJcbi8vIFx0d2lkdGg6IDQwcHg7XHJcbi8vIFx0aGVpZ2h0OiA0MHB4O1xyXG4vLyBcdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbi8vIFx0cGFkZGluZzogMTBweDtcclxuLy8gfVxyXG5cclxuLy8gLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09Ki9cclxuLy8gQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpe1xyXG4vLyAjZm9vdGVyIHtcclxuLy8gXHRwYWRkaW5nLWJvdHRvbTogMjBweDtcdFxyXG4vLyB9XHJcbi8vIC5pbm5lci1mb290ZXIgLmNvbC1tZC0zIGgyIHtcclxuLy8gXHRmb250LXNpemU6IDI0cHg7XHJcbi8vIFx0Ym9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmY7XHJcbi8vICAgcGFkZGluZzogMTBweDtcclxuLy8gfVxyXG4vLyAuaW5uZXItZm9vdGVyIC5jb2wtbWQtMyB1bCBsaSBhIHtcclxuLy8gXHRmb250LXNpemU6IDE1cHg7XHJcbi8vIFx0bWFyZ2luOiAwIDAgMTBweDtcclxuLy8gfVxyXG4vLyAub3JkZXItYS1yZXBhaXIge1xyXG4vLyBcdHBhZGRpbmc6IDE0cHggMTVweCAxMHB4O1xyXG4vLyBcdG1hcmdpbjogMCAwIDIwcHg7XHJcbi8vIH1cclxuLy8gfVxyXG5cclxuZm9vdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiAjNTE1MjU2O1xyXG59XHJcbiNmb290ZXIge1xyXG5cdG1heC13aWR0aDogMTIwMHB4O1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHBhZGRpbmc6IDIwcHggMCA0MHB4O1xyXG59XHJcbi5pbm5lci1mb290ZXIge1xyXG5cdG1hcmdpbjogMDtcclxufVxyXG4uaW5uZXItZm9vdGVyIC5jb2wtbWQtMyBoMiB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IC0ycHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIHBhZGRpbmc6IDAgMTBweCAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbn1cclxuLmlubmVyLWZvb3RlciAuY29sLW1kLTMgdWwge1xyXG5cdG1hcmdpbjogMjBweCAwIDAgMDtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmlubmVyLWZvb3RlciAuY29sLW1kLTMgdWwgbGkgYSB7XHJcblx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IC0xcHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbjogMCAwIDE1cHg7XHJcbn1cclxuLm9yZGVyLWEtcmVwYWlyIHtcclxuXHRiYWNrZ3JvdW5kOiAjZThiMTNkO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtZmFtaWx5OiAnQ29tZm9ydGFhJywgY3Vyc2l2ZTtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIHBhZGRpbmc6IDIwcHggMjVweCAxNnB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIG1hcmdpbjogMCBhdXRvXHJcbn1cclxuLnNvY2lhbC1pY29ucyB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi5zb2NpYWwtaWNvbnMgbGkgYSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRiYWNrZ3JvdW5kOiAjZThiMTNkO1xyXG5cdG1hcmdpbjogMCAxMHB4ICFpbXBvcnRhbnQ7XHJcblx0d2lkdGg6IDQwcHg7XHJcblx0aGVpZ2h0OiA0MHB4O1xyXG5cdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcblx0cGFkZGluZzogMTBweDtcclxufVxyXG4uY29weXJpZ2h0IHtcclxuXHRmb250LXNpemU6IDEycHg7XHJcblx0YmFja2dyb3VuZDogYmxhY2s7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0cGFkZGluZzogMTBweCAxNXB4O1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4vKiBSZXNwb25zaXZlID09PT09PT09PT09PT09PT09PT09PT0qL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCl7XHJcbiNmb290ZXIge1xyXG5cdHBhZGRpbmctYm90dG9tOiAyMHB4O1x0XHJcbn1cclxuLmlubmVyLWZvb3RlciAuY29sLW1kLTMgaDIge1xyXG5cdGZvbnQtc2l6ZTogMjRweDtcclxuXHRib3JkZXItdG9wOiAxcHggc29saWQgI2ZmZjtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5pbm5lci1mb290ZXIgLmNvbC1tZC0zIHVsIGxpIGEge1xyXG5cdGZvbnQtc2l6ZTogMTVweDtcclxuXHRtYXJnaW46IDAgMCAxMHB4O1xyXG59XHJcbi5vcmRlci1hLXJlcGFpciB7XHJcblx0cGFkZGluZzogMTRweCAxNXB4IDEwcHg7XHJcblx0bWFyZ2luOiAwIDAgMjBweDtcclxufVxyXG59IiwiZm9vdGVyIHtcbiAgYmFja2dyb3VuZDogIzUxNTI1Njtcbn1cblxuI2Zvb3RlciB7XG4gIG1heC13aWR0aDogMTIwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMjBweCAwIDQwcHg7XG59XG5cbi5pbm5lci1mb290ZXIge1xuICBtYXJnaW46IDA7XG59XG5cbi5pbm5lci1mb290ZXIgLmNvbC1tZC0zIGgyIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1mYW1pbHk6IFwiQ29tZm9ydGFhXCIsIGN1cnNpdmU7XG4gIGxldHRlci1zcGFjaW5nOiAtMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAwIDEwcHggMTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbi5pbm5lci1mb290ZXIgLmNvbC1tZC0zIHVsIHtcbiAgbWFyZ2luOiAyMHB4IDAgMCAwO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pbm5lci1mb290ZXIgLmNvbC1tZC0zIHVsIGxpIGEge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1mYW1pbHk6IFwiQ29tZm9ydGFhXCIsIGN1cnNpdmU7XG4gIGxldHRlci1zcGFjaW5nOiAtMXB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjZmZmO1xuICBtYXJnaW46IDAgMCAxNXB4O1xufVxuXG4ub3JkZXItYS1yZXBhaXIge1xuICBiYWNrZ3JvdW5kOiAjZThiMTNkO1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1mYW1pbHk6IFwiQ29tZm9ydGFhXCIsIGN1cnNpdmU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHBhZGRpbmc6IDIwcHggMjVweCAxNnB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxldHRlci1zcGFjaW5nOiAwLjVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5zb2NpYWwtaWNvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLnNvY2lhbC1pY29ucyBsaSBhIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2U4YjEzZDtcbiAgbWFyZ2luOiAwIDEwcHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNvcHlyaWdodCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PT09Ki9cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gICNmb290ZXIge1xuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICB9XG5cbiAgLmlubmVyLWZvb3RlciAuY29sLW1kLTMgaDIge1xuICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2ZmZjtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgLmlubmVyLWZvb3RlciAuY29sLW1kLTMgdWwgbGkgYSB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIG1hcmdpbjogMCAwIDEwcHg7XG4gIH1cblxuICAub3JkZXItYS1yZXBhaXIge1xuICAgIHBhZGRpbmc6IDE0cHggMTVweCAxMHB4O1xuICAgIG1hcmdpbjogMCAwIDIwcHg7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/layout/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(authService) {
        this.authService = authService;
    }
    FooterComponent.prototype.logout = function () {
        this.authService.isLogOut().subscribe(function (res) {
            // console.log("res", res);
        });
    };
    FooterComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] }
    ]; };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layout/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/layout/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inner-header {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.inner-header-flex {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  top: 0;\n  z-index: 999;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  box-sizing: border-box;\n  padding: 10px 80px;\n  transition: all 0.8s;\n  background: rgba(255, 255, 255, 0.7);\n}\n\n.darkHeader {\n  background: #fff !important;\n  padding: 6px 80px;\n  box-shadow: 0 2px 6px -4px #000;\n  border-bottom: 1px solid #eaeaea;\n}\n\n.logo {\n  color: #000000;\n  font-weight: bold;\n  text-transform: uppercase;\n  font-size: 26px;\n  line-height: 26px;\n  letter-spacing: 1px;\n}\n\n.navigation-menu ul.pc-menu, .footbox ul {\n  display: flex;\n  align-items: center;\n}\n\n.navigation-menu ul.pc-menu li {\n  position: relative;\n}\n\n.navigation-menu ul.pc-menu li + li, .footbox ul li + li {\n  margin: 0 0 0 25px;\n}\n\n.navigation-menu ul.pc-menu li a, .footbox ul li a {\n  color: #000;\n  font-size: 15px;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  border-bottom: 2px solid transparent;\n  padding: 10px 0;\n}\n\n.navigation-menu ul.pc-menu li a:hover, .footbox ul li a :hover {\n  border-bottom: 2px solid #fff;\n}\n\n#dropdownMenuButton {\n  background: none;\n  padding: 0 6px;\n  color: #000;\n  border-radius: 0;\n  border: none;\n  font-size: 20px;\n}\n\n.dropdown-menu {\n  min-width: 143px;\n  padding: 0;\n  left: auto !important;\n  right: -50px;\n  border-radius: 0;\n  margin: 0;\n}\n\n.dropdown-menu a {\n  display: block;\n  color: #000 !important;\n  margin: 0;\n  padding: 7px 10px !important;\n  font-size: 13px !important;\n  border-bottom: 1px solid #eaeaea !important;\n  transition: all 0.3s;\n}\n\n.typed_wrap {\n  padding: 0 25px;\n  text-align: center;\n  max-width: 991px;\n}\n\n.shoes-tag {\n  text-align: center;\n  padding: 10px 20px;\n  color: #fff;\n  text-align: center;\n  margin: 0 auto;\n  background: none;\n  border-radius: 10px;\n  font-size: 24px;\n  line-height: 32px;\n}\n\n.shoes-tag span {\n  line-height: 32px;\n  display: inline-block;\n}\n\n.typed_wrap h1 {\n  display: inline;\n}\n\n.typed::after {\n  content: \"|\";\n  display: inline;\n  -webkit-animation: blink 0.7s infinite;\n  animation: blink 0.7s infinite;\n}\n\n.typed-cursor {\n  opacity: 0;\n  display: none;\n}\n\n.open > .dropdown-menu {\n  display: block;\n}\n\n@keyframes blink {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n\n@-webkit-keyframes blink {\n  0% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n\n.carousel-inner .item img {\n  width: 100%;\n  height: 100vh;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.menu-open {\n  display: none;\n}\n\n/* Responsive ====================*/\n\n@media screen and (max-width: 991px) {\n  .inner-header-flex {\n    padding: 9px 15px 12px;\n    background: black;\n  }\n\n  .menu-open {\n    display: block;\n  }\n\n  .menu-open a {\n    display: block;\n    color: #000;\n    padding: 1px 6px;\n    border-radius: 1px;\n    background: #fff;\n  }\n\n  .navigation-menu ul.pc-menu {\n    display: none;\n    position: absolute;\n    left: 0;\n    top: 100%;\n    width: 100%;\n    background: #fff;\n    border-top: 1px solid rgba(255, 255, 255, 0.02);\n    border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n    flex-direction: column;\n  }\n\n  .navigation-menu ul.pc-menu li {\n    width: 100%;\n  }\n\n  .navigation-menu ul.pc-menu li + li {\n    margin: 0;\n    border-top: 1px solid rgba(0, 0, 0, 0.07);\n  }\n\n  .navigation-menu ul.pc-menu li a {\n    display: block;\n    color: #000;\n    padding: 7px 15px;\n    transition: all 0.5s;\n  }\n\n  .navigation-menu ul.pc-menu li a:hover {\n    background: #000;\n    color: #fff;\n    padding: 7px 15px 7px 25px;\n  }\n\n  .hover-dropdown {\n    position: static;\n    background: none;\n    border-radius: 0;\n    width: 100%;\n  }\n\n  .hover-dropdown li a {\n    padding: 5px 10px 5px 25px !important;\n    display: block;\n    text-align: left;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L0M6XFxVc2Vyc1xcREVMTFxcRGVza3RvcFxcZml4bXlraXgvc3JjXFxhcHBcXGxheW91dFxcaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUNDRjs7QURDQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxvQ0FBQTtBQ0VGOztBREFBO0VBQ0UsMkJBQUE7RUFDQSxpQkFBQTtFQUNBLCtCQUFBO0VBQ0EsZ0NBQUE7QUNHRjs7QUREQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNJRjs7QURGQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ0tGOztBREhBO0VBQ0Usa0JBQUE7QUNNRjs7QURKQTtFQUNFLGtCQUFBO0FDT0Y7O0FETEE7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxvQ0FBQTtFQUNBLGVBQUE7QUNRRjs7QUROQTtFQUNFLDZCQUFBO0FDU0Y7O0FETkE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ1NGOztBRFBBO0VBQ0MsZ0JBQUE7RUFDRyxVQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDVUo7O0FEUkE7RUFDRSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxTQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQkFBQTtFQUNBLDJDQUFBO0VBRUEsb0JBQUE7QUNVRjs7QURSQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDV0Y7O0FEVEE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNZRjs7QURWQTtFQUNFLGlCQUFBO0VBQ0EscUJBQUE7QUNhRjs7QURYQTtFQUNFLGVBQUE7QUNjRjs7QURaQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0Esc0NBQUE7RUFFQSw4QkFBQTtBQ2VGOztBRGJBO0VBQ0csVUFBQTtFQUNELGFBQUE7QUNnQkY7O0FEZEE7RUFDRSxjQUFBO0FDaUJGOztBRGZBO0VBQ0U7SUFBSyxVQUFBO0VDbUJMO0VEbEJBO0lBQU0sVUFBQTtFQ3FCTjtFRHBCQTtJQUFPLFVBQUE7RUN1QlA7QUFDRjs7QUR0QkE7RUFDRTtJQUFLLFVBQUE7RUN5Qkw7RUR4QkE7SUFBTSxVQUFBO0VDMkJOO0VEMUJBO0lBQU8sVUFBQTtFQzZCUDtBQUNGOztBRHZCQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtBQ29DRjs7QURsQ0E7RUFDRSxhQUFBO0FDcUNGOztBRG5DQSxtQ0FBQTs7QUFDQTtFQUNBO0lBQ0Usc0JBQUE7SUFDQSxpQkFBQTtFQ3NDQTs7RURwQ0Y7SUFDRSxjQUFBO0VDdUNBOztFRHJDRjtJQUNFLGNBQUE7SUFDQSxXQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0VDd0NBOztFRHRDRjtJQUNFLGFBQUE7SUFDQSxrQkFBQTtJQUNBLE9BQUE7SUFDQSxTQUFBO0lBQ0EsV0FBQTtJQUNBLGdCQUFBO0lBQ0EsK0NBQUE7SUFDQSwyQ0FBQTtJQUNBLHNCQUFBO0VDeUNBOztFRHZDRjtJQUNFLFdBQUE7RUMwQ0E7O0VEeENGO0lBQ0UsU0FBQTtJQUNBLHlDQUFBO0VDMkNBOztFRHpDRjtJQUNFLGNBQUE7SUFDQSxXQUFBO0lBQ0EsaUJBQUE7SUFDQSxvQkFBQTtFQzRDQTs7RUQxQ0Y7SUFDRSxnQkFBQTtJQUNBLFdBQUE7SUFDQSwwQkFBQTtFQzZDQTs7RUQzQ0Y7SUFDRSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxXQUFBO0VDOENBOztFRDVDRjtJQUNFLHFDQUFBO0lBQ0EsY0FBQTtJQUNBLGdCQUFBO0VDK0NBO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmlubmVyLWhlYWRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcbi5pbm5lci1oZWFkZXItZmxleCB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAwO1xyXG4gIHotaW5kZXg6IDk5OTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbiAgcGFkZGluZzogMTBweCA4MHB4O1xyXG4gIHRyYW5zaXRpb246IGFsbCAwLjhzO1xyXG4gIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMC43KTtcclxufVxyXG4uZGFya0hlYWRlciB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDZweCA4MHB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMnB4IDZweCAtNHB4ICMwMDA7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XHJcbn1cclxuLmxvZ28ge1xyXG4gIGNvbG9yOiAjMDAwMDAwO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgZm9udC1zaXplOiAyNnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbn1cclxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51LCAuZm9vdGJveCB1bCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbi5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSBsaSB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSBsaStsaSwgLmZvb3Rib3ggdWwgbGkrbGkge1xyXG4gIG1hcmdpbjogMCAwIDAgMjVweDtcclxufVxyXG4ubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUgbGkgYSwgLmZvb3Rib3ggdWwgbGkgYSB7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBib3JkZXItYm90dG9tOiAycHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgcGFkZGluZzogMTBweCAwO1xyXG59XHJcbi5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSBsaSBhOmhvdmVyLCAuZm9vdGJveCB1bCBsaSBhIDpob3ZlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNmZmY7XHJcbn1cclxuXHJcbiNkcm9wZG93bk1lbnVCdXR0b24ge1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgcGFkZGluZzogMCA2cHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgYm9yZGVyLXJhZGl1czogMDtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG59XHJcbi5kcm9wZG93bi1tZW51IHtcclxuXHRtaW4td2lkdGg6IDE0M3B4O1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGxlZnQ6IGF1dG8gIUlNUE9SVEFOVDtcclxuICAgIHJpZ2h0OiAtNTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuLmRyb3Bkb3duLW1lbnUgYSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZzogN3B4IDEwcHggIWltcG9ydGFudDtcclxuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZWFlYSAhaW1wb3J0YW50O1xyXG4gIC8vIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB0cmFuc2l0aW9uOiBhbGwgMC4zcztcclxufVxyXG4udHlwZWRfd3JhcCB7XHJcbiAgcGFkZGluZzogMCAyNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXgtd2lkdGg6IDk5MXB4O1xyXG59XHJcbi5zaG9lcy10YWcge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nOiAxMHB4IDIwcHg7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBmb250LXNpemU6IDI0cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDMycHg7XHJcbn1cclxuLnNob2VzLXRhZyBzcGFuIHtcclxuICBsaW5lLWhlaWdodDogMzJweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuLnR5cGVkX3dyYXAgaDEge1xyXG4gIGRpc3BsYXk6IGlubGluZTtcclxufVxyXG4udHlwZWQ6OmFmdGVyIHtcclxuICBjb250ZW50OiAnfCc7XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG4gIC13ZWJraXQtYW5pbWF0aW9uOiBibGluayAwLjdzIGluZmluaXRlO1xyXG4gIC1tb3otYW5pbWF0aW9uOiBibGluayAwLjdzIGluZmluaXRlO1xyXG4gIGFuaW1hdGlvbjogYmxpbmsgMC43cyBpbmZpbml0ZTtcclxufVxyXG4udHlwZWQtY3Vyc29ye1xyXG4gICBvcGFjaXR5OiAwO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLm9wZW4+LmRyb3Bkb3duLW1lbnUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbkBrZXlmcmFtZXMgYmxpbmt7XHJcbiAgMCUgeyBvcGFjaXR5OjE7IH1cclxuICA1MCUgeyBvcGFjaXR5OjA7IH1cclxuICAxMDAlIHsgb3BhY2l0eToxOyB9XHJcbn1cclxuQC13ZWJraXQta2V5ZnJhbWVzIGJsaW5re1xyXG4gIDAlIHsgb3BhY2l0eToxOyB9XHJcbiAgNTAlIHsgb3BhY2l0eTowOyB9XHJcbiAgMTAwJSB7IG9wYWNpdHk6MTsgfVxyXG59XHJcbkAtbW96LWtleWZyYW1lcyBibGlua3tcclxuICAwJSB7IG9wYWNpdHk6MTsgfVxyXG4gIDUwJSB7IG9wYWNpdHk6MDsgfVxyXG4gIDEwMCUgeyBvcGFjaXR5OjE7IH1cclxufVxyXG4uY2Fyb3VzZWwtaW5uZXIgLml0ZW0gaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMHZoO1xyXG4gIG9iamVjdC1maXQ6IGNvdmVyO1xyXG59XHJcbi5tZW51LW9wZW4ge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PSovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KSB7XHJcbi5pbm5lci1oZWFkZXItZmxleCB7XHJcbiAgcGFkZGluZzogOXB4IDE1cHggMTJweDtcclxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDEpXHJcbn1cclxuLm1lbnUtb3BlbiB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLm1lbnUtb3BlbiBhIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBjb2xvcjogIzAwMDtcclxuICBwYWRkaW5nOiAxcHggNnB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDFweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG59XHJcbi5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgbGVmdDogMDtcclxuICB0b3A6IDEwMCU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsMjU1LDI1NSwwLjAyKTtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLDAsMCwwLjEpO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4ubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUgbGkrbGkge1xyXG4gIG1hcmdpbjogMDtcclxuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgwLDAsMCwwLjA3KTtcclxufVxyXG4ubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUgbGkgYSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgcGFkZGluZzogN3B4IDE1cHg7XHJcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXM7XHJcbn1cclxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpIGE6aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcGFkZGluZzogN3B4IDE1cHggN3B4IDI1cHg7XHJcbn0gXHJcbi5ob3Zlci1kcm9wZG93biB7XHJcbiAgcG9zaXRpb246IHN0YXRpYztcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmhvdmVyLWRyb3Bkb3duIGxpIGEge1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4IDVweCAyNXB4ICFpbXBvcnRhbnQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG59XHJcblxyXG5cclxuLy8gaGVhZGVyIHtcclxuLy8gXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG4vLyBcdHBvc2l0aW9uOiBmaXhlZDtcclxuLy8gXHRsZWZ0OiAwO1xyXG4vLyBcdHRvcDogMDtcclxuLy8gXHR3aWR0aDogMTAwJTtcclxuLy8gXHR6LWluZGV4OiA5O1xyXG4vLyBcdHBhZGRpbmc6IDAgMCAxMHB4O1xyXG4vLyB9XHJcbi8vIC5maXhlZC1oZWFkZXIge1xyXG4vLyBcdGJveC1zaGFkb3c6IDAgMCAxMHB4IC02cHggIzAwMDtcclxuLy8gXHRwYWRkaW5nOiAxMHB4IDA7XHJcbi8vIH1cclxuLy8gI2hlYWRlciB7XHJcbi8vICAgbWF4LXdpZHRoOiAxMjAwcHg7XHJcbi8vICAgbWFyZ2luOiAwIGF1dG87XHJcbi8vICAgcGFkZGluZzogMCAxNXB4O1xyXG4vLyAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbi8vIH1cclxuLy8gLmhlYWRlci10b3Age1xyXG4vLyBcdGRpc3BsYXk6IGZsZXg7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbi8vIFx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4vLyBcdHRyYW5zaXRpb246IGFsbCAwLjVzO1xyXG4vLyB9XHJcbi8vIC5maXhlZC1oZWFkZXIgLmhlYWRlci10b3Age1xyXG4vLyBcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmN1c3RvbS1tb3ZlbWVudCB7XHJcbi8vIFx0cGFkZGluZzogMTBweCAwO1xyXG4vLyBcdHRyYW5zaXRpb246IGFsbCAwLjVzO1xyXG4vLyB9XHJcbi8vIC5jdXN0b20tbW92ZW1lbnQtaHplcm8ge1xyXG4vLyBcdGhlaWdodDogMDtcclxuLy8gXHRwYWRkaW5nOiAwO1xyXG4vLyBcdG9wYWNpdHk6IDA7XHJcbi8vIH1cclxuLy8gLnRvcC1tZW51IHtcclxuLy8gXHR0cmFuc2l0aW9uOiBhbGwgMC4ycztcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3Qge1xyXG4vLyBcdGRpc3BsYXk6IGZsZXg7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3QgbGkgYSB7XHJcbi8vIFx0cGFkZGluZzogMTBweCAxNXB4O1xyXG4vLyBcdGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4vLyBcdGNvbG9yOiAjNzg3ODc4O1xyXG4vLyBcdHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbi8vIFx0ZGlzcGxheTogZmxleDtcclxuLy8gXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG4vLyBcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4vLyB9XHJcbi8vIC50b3AtbWVudSAudG9wLW1lbnUtbGlzdCBsaSBhIC5jYXJldCB7XHJcbi8vIFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4vLyBcdG1hcmdpbjogMCAwIDAgMTBweDtcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3QgbGkgYTpob3ZlciB7XHJcbi8vIFx0Y29sb3I6ICMwMDA7XHJcbi8vIH1cclxuLy8gLnRvcC1tZW51IC50b3AtbWVudS1saXN0IGxpIGEgaW1nIHtcclxuLy8gXHR3aWR0aDogMjBweDtcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3QgbGkub3BlbiBhIC5jYXJldCB7XHJcbi8vIFx0dHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3QgbGkgdWwge1xyXG4vLyBcdG1pbi13aWR0aDogMTQwcHg7XHJcbi8vIFx0dHJhbnNpdGlvbjogYWxsIDAuM3M7XHJcbi8vIFx0bGVmdDogNTAlICFpbXBvcnRhbnQ7XHJcbi8vIFx0d2lkdGg6IDEwMCU7XHJcbi8vIFx0bWFyZ2luOiAwIDAgMCAtNzBweDtcclxuLy8gXHRwYWRkaW5nOiAwO1xyXG4vLyB9XHJcbi8vIC5sb2dvLWJhciBhIHtcclxuLy8gXHRmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbi8vIFx0YmFja2dyb3VuZDogIzAwMDtcclxuLy8gXHRjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xyXG4vLyBcdGZvbnQtc2l6ZTogNDBweDtcclxuLy8gXHRwYWRkaW5nOiA4cHggNXB4IDRweDtcclxuLy8gXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbi8vIFx0bGluZS1oZWlnaHQ6IDQwcHg7XHJcbi8vIFx0dHJhbnNpdGlvbjogYWxsIDAuNXM7XHJcbi8vIH1cclxuLy8gLmxvZ28tZml4ZWQge1xyXG4vLyAgIGZvbnQtc2l6ZTogMjhweCAhaW1wb3J0YW50O1xyXG4vLyAgIGxpbmUtaGVpZ2h0OiAyOHB4ICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLy8gLnNlYXJjaC1pbnB1dCB7XHJcbi8vIFx0d2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XHJcbi8vICAgcGFkZGluZzogNnB4IDhweCAhaW1wb3J0YW50O1xyXG4vLyAgIGxlZnQ6IGF1dG8gIWltcG9ydGFudDtcclxuLy8gICByaWdodDogMDtcclxuLy8gfVxyXG4vLyAuc2VhcmNoLWlucHV0IGlucHV0IHtcclxuLy8gXHRkaXNwbGF5OiBibG9jaztcclxuLy8gXHR3aWR0aDogMTAwJTtcclxuLy8gXHRmb250LWZhbWlseTogJ0NvbWZvcnRhYScsIGN1cnNpdmU7XHJcbi8vIFx0Ym9yZGVyOiAxcHggc29saWQgI2VhZWFlYTtcclxuLy8gXHRoZWlnaHQ6IDM1cHg7XHJcbi8vIFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG4vLyBcdGxpbmUtaGVpZ2h0OiAzNXB4O1xyXG4vLyBcdHBhZGRpbmc6IDAgMTBweDtcclxuLy8gXHRmb250LXNpemU6IDE0cHg7XHJcbi8vIFx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vIH1cclxuLy8gLmJhci1tZW51IHtcclxuLy8gXHRkaXNwbGF5OiBub25lO1xyXG4vLyB9XHJcbi8vIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MXB4KXtcclxuLy8gaGVhZGVyIHtcclxuLy8gXHRwYWRkaW5nOiAwO1xyXG4vLyB9XHJcbi8vIC5maXhlZC1oZWFkZXIge1xyXG4vLyBcdHBhZGRpbmc6IDA7XHJcbi8vIH1cclxuLy8gI2hlYWRlciB7XHJcbi8vIFx0cGFkZGluZzogMDtcclxuLy8gXHRib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZWFhZTtcclxuLy8gfVxyXG4vLyAuaGVhZGVyLXRvcCB7XHJcbi8vIFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gfVxyXG4vLyAuY3VzdG9tLW1vdmVtZW50IHtcclxuLy8gXHRkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLy8gLmJhci1tZW51IHtcclxuLy8gXHRkaXNwbGF5OiBibG9jaztcclxuLy8gfVxyXG4vLyAuYmFyLW1lbnUgYSB7XHJcbi8vICAgcGFkZGluZzogM3B4O1xyXG4vLyAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4vLyAgIGNvbG9yOiAjNzg3ODc4O1xyXG4vLyAgIGRpc3BsYXk6IGZsZXg7XHJcbi8vICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuLy8gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICB3aWR0aDogNDBweDtcclxuLy8gICBoZWlnaHQ6IDQwcHg7XHJcbi8vICAgbWFyZ2luOiAwIDEycHggMCAwO1xyXG4vLyB9XHJcbi8vIC5iYXItbWVudSBhIGltZyB7XHJcbi8vIFx0d2lkdGg6IDEwMCU7XHJcbi8vIH1cclxuLy8gLnRvcC1tZW51IHtcclxuLy8gXHRkaXNwbGF5OiBub25lO1xyXG4vLyBcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gXHRsZWZ0OiAwO1xyXG4vLyBcdHRvcDogY2FsYygxMDAlIC0gMXB4KTtcclxuLy8gXHR6LWluZGV4OiA5O1xyXG4vLyBcdGJhY2tncm91bmQ6ICNmZmY7XHJcbi8vIFx0d2lkdGg6IDEwMCU7XHJcbi8vIH1cclxuLy8gLnRvcC1tZW51IC50b3AtbWVudS1saXN0IHtcclxuLy8gXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4vLyBcdGJvcmRlci10b3A6IDJweCBzb2xpZCAjZWFlYWVhO1xyXG4vLyB9XHJcbi8vIC50b3AtbWVudSAudG9wLW1lbnUtbGlzdCBsaSB7XHJcbi8vIFx0d2lkdGg6IDEwMCU7XHJcbi8vIH1cclxuLy8gLnRvcC1tZW51IC50b3AtbWVudS1saXN0IGxpIGEge1xyXG4vLyBcdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuLy8gfVxyXG4vLyAudG9wLW1lbnUgLnRvcC1tZW51LWxpc3QgbGkgdWwge1xyXG4vLyBcdHBvc2l0aW9uOiBzdGF0aWM7XHJcbi8vICAgbWFyZ2luOiAwO1xyXG4vLyAgIG1pbi13aWR0aDogMTAwJTtcclxuLy8gfVxyXG4vLyAuc2VhcmNoLW1lbnUge1xyXG4vLyBcdGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxuLy8gfVxyXG4vLyB9IiwiLmlubmVyLWhlYWRlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4uaW5uZXItaGVhZGVyLWZsZXgge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBsZWZ0OiAwO1xuICB0b3A6IDA7XG4gIHotaW5kZXg6IDk5OTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBwYWRkaW5nOiAxMHB4IDgwcHg7XG4gIHRyYW5zaXRpb246IGFsbCAwLjhzO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG59XG5cbi5kYXJrSGVhZGVyIHtcbiAgYmFja2dyb3VuZDogI2ZmZiAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA2cHggODBweDtcbiAgYm94LXNoYWRvdzogMCAycHggNnB4IC00cHggIzAwMDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XG59XG5cbi5sb2dvIHtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDI2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuXG4ubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUsIC5mb290Ym94IHVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUgbGkgKyBsaSwgLmZvb3Rib3ggdWwgbGkgKyBsaSB7XG4gIG1hcmdpbjogMCAwIDAgMjVweDtcbn1cblxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpIGEsIC5mb290Ym94IHVsIGxpIGEge1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBib3JkZXItYm90dG9tOiAycHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIHBhZGRpbmc6IDEwcHggMDtcbn1cblxuLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpIGE6aG92ZXIsIC5mb290Ym94IHVsIGxpIGEgOmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNmZmY7XG59XG5cbiNkcm9wZG93bk1lbnVCdXR0b24ge1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBwYWRkaW5nOiAwIDZweDtcbiAgY29sb3I6ICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlcjogbm9uZTtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuXG4uZHJvcGRvd24tbWVudSB7XG4gIG1pbi13aWR0aDogMTQzcHg7XG4gIHBhZGRpbmc6IDA7XG4gIGxlZnQ6IGF1dG8gIWltcG9ydGFudDtcbiAgcmlnaHQ6IC01MHB4O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBtYXJnaW46IDA7XG59XG5cbi5kcm9wZG93bi1tZW51IGEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiA3cHggMTBweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEzcHggIWltcG9ydGFudDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWEgIWltcG9ydGFudDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuM3M7XG59XG5cbi50eXBlZF93cmFwIHtcbiAgcGFkZGluZzogMCAyNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1heC13aWR0aDogOTkxcHg7XG59XG5cbi5zaG9lcy10YWcge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgbGluZS1oZWlnaHQ6IDMycHg7XG59XG5cbi5zaG9lcy10YWcgc3BhbiB7XG4gIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi50eXBlZF93cmFwIGgxIHtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuXG4udHlwZWQ6OmFmdGVyIHtcbiAgY29udGVudDogXCJ8XCI7XG4gIGRpc3BsYXk6IGlubGluZTtcbiAgLXdlYmtpdC1hbmltYXRpb246IGJsaW5rIDAuN3MgaW5maW5pdGU7XG4gIC1tb3otYW5pbWF0aW9uOiBibGluayAwLjdzIGluZmluaXRlO1xuICBhbmltYXRpb246IGJsaW5rIDAuN3MgaW5maW5pdGU7XG59XG5cbi50eXBlZC1jdXJzb3Ige1xuICBvcGFjaXR5OiAwO1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ub3BlbiA+IC5kcm9wZG93bi1tZW51IHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbkBrZXlmcmFtZXMgYmxpbmsge1xuICAwJSB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICA1MCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgMTAwJSB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuQC13ZWJraXQta2V5ZnJhbWVzIGJsaW5rIHtcbiAgMCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbiAgNTAlIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIDEwMCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbkAtbW96LWtleWZyYW1lcyBibGluayB7XG4gIDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG4gIDUwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICAxMDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG4uY2Fyb3VzZWwtaW5uZXIgLml0ZW0gaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4ubWVudS1vcGVuIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLyogUmVzcG9uc2l2ZSA9PT09PT09PT09PT09PT09PT09PSovXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTFweCkge1xuICAuaW5uZXItaGVhZGVyLWZsZXgge1xuICAgIHBhZGRpbmc6IDlweCAxNXB4IDEycHg7XG4gICAgYmFja2dyb3VuZDogYmxhY2s7XG4gIH1cblxuICAubWVudS1vcGVuIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuXG4gIC5tZW51LW9wZW4gYSB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgY29sb3I6ICMwMDA7XG4gICAgcGFkZGluZzogMXB4IDZweDtcbiAgICBib3JkZXItcmFkaXVzOiAxcHg7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgfVxuXG4gIC5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjAyKTtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIH1cblxuICAubmF2aWdhdGlvbi1tZW51IHVsLnBjLW1lbnUgbGkge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG5cbiAgLm5hdmlnYXRpb24tbWVudSB1bC5wYy1tZW51IGxpICsgbGkge1xuICAgIG1hcmdpbjogMDtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjA3KTtcbiAgfVxuXG4gIC5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSBsaSBhIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBjb2xvcjogIzAwMDtcbiAgICBwYWRkaW5nOiA3cHggMTVweDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbiAgfVxuXG4gIC5uYXZpZ2F0aW9uLW1lbnUgdWwucGMtbWVudSBsaSBhOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjMDAwO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIHBhZGRpbmc6IDdweCAxNXB4IDdweCAyNXB4O1xuICB9XG5cbiAgLmhvdmVyLWRyb3Bkb3duIHtcbiAgICBwb3NpdGlvbjogc3RhdGljO1xuICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxuXG4gIC5ob3Zlci1kcm9wZG93biBsaSBhIHtcbiAgICBwYWRkaW5nOiA1cHggMTBweCA1cHggMjVweCAhaW1wb3J0YW50O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/layout/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../@core/backend/common/services/authentication.service */ "./src/app/@core/backend/common/services/authentication.service.ts");
/* harmony import */ var _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../@core/backend/common/services/users.service */ "./src/app/@core/backend/common/services/users.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(authService, usersService, toastrService, router) {
        this.authService = authService;
        this.usersService = usersService;
        this.toastrService = toastrService;
        this.router = router;
        this.isLoggedIn = false;
        this.user = localStorage.getItem('user');
        this.role = localStorage.getItem('role');
        if (this.user) {
            this.isLoggedIn = true;
        }
        $('.title').hover(function () {
            $('.hover-dropdown').toggleClass('toggled');
        });
    }
    HeaderComponent.prototype.logout = function () {
        var _this = this;
        this.usersService.logout().subscribe(function (result) {
            if (result.status) {
                _this.toastrService.success('Success', result.message);
                localStorage.clear();
                window.location.href = "";
            }
            else {
                _this.toastrService.error('Error', result.message);
            }
        });
    };
    HeaderComponent.prototype.myRequest = function () {
        this.router.navigate(['customer/my/request']);
    };
    HeaderComponent.prototype.profileRoute = function () {
        if (this.role) {
            if (this.role == '1') {
                window.location.href = "artist/profile";
            }
            else {
                window.location.href = "customer/profile";
            }
        }
    };
    HeaderComponent.ctorParameters = function () { return [
        { type: _core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"] },
        { type: _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/layout/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_backend_common_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"], _core_backend_common_services_users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ })

}]);
//# sourceMappingURL=auth-auth-module.js.map