import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbAccordionModule, NbSelectModule, NbButtonModule, NbIconModule, NbCheckboxModule } from '@nebular/theme';
import { SmartTableComponent } from './smart-table.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { BkMapModule } from '@uibakery/kit';

@NgModule({
  declarations: [SmartTableComponent],
  imports: [
    CommonModule,
    NbCardModule,
    NbAccordionModule,
    Ng2SmartTableModule,
    NbSelectModule,
    NbButtonModule,
    NbIconModule,
    NbCheckboxModule,
    BkMapModule,
    
  ],
  exports: [ NbCardModule,
     NbAccordionModule, SmartTableComponent,
      Ng2SmartTableModule, NbSelectModule, NbButtonModule, 
      NbIconModule, NbCheckboxModule, BkMapModule],
})
export class SharedModule { }
