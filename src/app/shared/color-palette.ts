
export const ColorPalette = [
    {
        name:'Blue',
        headerColor: '#3F42B3',
        tagColor: '#5D60C7',
        departmentColor: '#8285D5',
        ownerColor: '#3F42A3',
        id:1
    },
     {
        name:'Green',
        headerColor: '#276E62',
        tagColor: '#369F8D',
        departmentColor: '#8A8A8A',
        ownerColor: '#276E52',
        id:2
    },
     {
        name:'Purple',
        headerColor: '#5E06F9',
        tagColor: '#7E37FB',
        departmentColor: '#9E68FD',
        ownerColor: '#5E06E9',
        id:3
    },
    {
        name:'Brown',
        headerColor: '#000000',
        tagColor: '#321701',
        departmentColor: '#642E02',
        ownerColor: '#000A00',
        id:4
    },

]
