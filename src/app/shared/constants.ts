
export const AppConstants = {
   location  :  [{ 'name': 'Attard' },
   { 'name': 'Balzan' },
   { 'name': 'Birkirkara' },
   { 'name': 'Birżebbuġa' },
   { 'name': 'Ċirkewwa' },
   { 'name': 'Comino' },
   { 'name': 'Dingli' },
   { 'name': 'Fgura' }],
   classification:[{
    'name': 'Confidential'
  }, {
    'name': 'Restricted'
  }, {
    'name': 'Official'
  }, {
    'name': 'Open'
  }],
  srcsystem: [{
      'name': "CPD",
      isSelected: false
    }, {
      'name': "112",
      isSelected: false
    }, {
      'name': "DVI",
      isSelected: false
    }, {
      'name': "Drone Footage",
      isSelected: false
    }, {
      'name': "Hospital",
      isSelected: false
    }]
   
}