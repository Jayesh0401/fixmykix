export class UserModel{
    id: number;
    role: string;
    name: string;
    email: string;
    mno: number;
    groups: string;
    created_by: string;
    updated_by: string;
    constructor() {
        this.id = null;
        this.role = '';
        this.name = '';
        this.email = '';
        this.mno = null;
        this.groups = '';
        this.created_by = '';
        this.updated_by = '';
    }

}