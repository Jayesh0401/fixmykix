export class Catalog {
	catalog_id: number;
	catalog_name: string;
	catalog_info: any;
	catalog_department: any;
	catalog_classification: any;
	srcsystem: string;
	catalog_location: any;
	incident_id: any;
	incident_name: any;
	incident_date: any;
	owners: any;
	path: any[];
	tags: any;
	comments: any;
	created_at: any;
	updated_at: any;
	created_by: any;
	updated_by: any;
	catalog_xaxis: any;
	catalog_yaxis: any;
	constructor() {
		this.catalog_id = null;
		this.catalog_name = '';
		this.catalog_info = '';
		this.catalog_department = null;
		this.catalog_classification = '';
		this.srcsystem = '';
		this.catalog_location = '';
		this.incident_id = '';
		this.incident_name = '';
		this.incident_date = '';
		this.owners = [];
		this.path = [];
		this.tags = [];
		this.comments = '';
		this.created_at = '';
		this.updated_at = '';
		this.created_by = '';
		this.updated_by = ''
		this.catalog_xaxis = '';
		this.catalog_yaxis = '';
	}
}