import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { UserModel } from '../../../../@core/interfaces/common/usermodel';
import  { ApiService } from '../services/api.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
@Injectable()
export class UsersApi {
  url : any;
  private readonly apiController: string = 'users';

  constructor(private api: HttpService, private http: HttpClient,private apiService: ApiService) {
    this.url = apiService.url;
   }

  get usersDataSource() {
    return this.api.getServerDataSource(`${this.api.apiUrl}/${this.apiController}`);
  }

  list(pageNumber: number = 1, pageSize: number = 10): Observable<any[]> {
    const params = new HttpParams()
      .set('pageNumber', `${pageNumber}`)
      .set('pageSize', `${pageSize}`);

    return this.api.get(this.apiController, { params })
      .pipe(map(data => data.map(item => {
        const picture = `${this.api.apiUrl}/${this.apiController}/${item.id}/photo`;
        return { ...item, picture };
      })));
  }

  getCurrent(): Observable<any> {
    return this.api.get(`${this.apiController}/current`)
      .pipe(map(data => {
        const picture = `${this.api.apiUrl}/${this.apiController}/${data.id}/photo`;
        return { ...data, picture };
      }));
  }

  get(id: number): Observable<any> {
    return this.api.get(`${this.apiController}/${id}`)
      .pipe(map(data => {
        const picture = `${this.api.apiUrl}/${this.apiController}/${data.id}/photo`;
        return { ...data, picture };
      }));
  }

  delete(id: number): Observable<boolean> {
    return this.api.delete(`${this.apiController}/${id}`);
  }

  add(user): Observable<any> {
    let resp = {}
    let url = this.url + 'v1/users/sign_up';
    return this.http.post(url, user).pipe(map(result => {
      resp = result;
      return resp;
    }));
  }

  updateCurrent(item: any): Observable<any> {
    return this.api.put(`${this.apiController}/current`, item);
  }

  update(item: any): Observable<any> {
    return this.api.put(`${this.apiController}/${item.id}`, item);
  }

  getUserRoles(): Observable<any> {
    return this.http.get(`/api/v1/user/userRoles`, {});
  }

  getUsersList(): Observable<any> {
    return this.http.get(`/api/v1/user/getUsers`, {})
      .pipe(map(user => {
        return user;
      }));
  }
  getUserById(item: any): Observable<any> {
    return this.http.get(`/api/v1/user/edit?id=${item}`).pipe(map(user =>{
      console.log("getUserById", user);
      return user;
    }))
  }
  updateUser(userModel: UserModel) :Observable<any> {
    let userid = JSON.parse(localStorage.getItem("userid"));
    let token = localStorage.getItem('token');
    let url = this.url + 'v1/artist_details';
    return this.http.post(url,userModel,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(user=>{
      return user;
    }));
  };

  deleteUser(id: any,status) :Observable<any> {
    let data = {
      id:id,
      status: status
    };
    return this.http.post(`api/v1/user/delete/`,data).pipe((resp: any) => {
      return resp;
    });
  };


  logout() :Observable<any> {
    let token = localStorage.getItem('token');
    let url = 'https://fixmykix-backend.herokuapp.com/v1/users/logout';
    return this.http.delete(url,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe((resp: any) => {
      return resp;
    });
  };
}
