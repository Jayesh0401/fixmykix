import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpService } from './http.service';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import  { ApiService } from '../services/api.service';

@Injectable()
export class ServiceRequestApi {
  url: '';
  constructor(private api: HttpClient,private apiService: ApiService) {
    this.url =  apiService.url;
  }

  GetServicRequesteDetails(): Observable<any> {
    let url = this.url + 'v1/service_requests';
    let token = localStorage.getItem('token');
    console.log(token, '-------token---');
    return this.api.get(url,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  }


  SaveServicRequesteDetails(data): Observable<any> {
    let url = this.url + 'v1/service_requests';
    let token = localStorage.getItem('token');
    return this.api.post(url,data,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  }

  AcceptServicRequest(details): Observable<any> {
    let url = this.url + 'v1/service_requests/'+details.id+'/accept';
    let token = localStorage.getItem('token');

    let data = {
      service_request:details
    };
    return this.api.put(url,data,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  }


  RejectServicRequest(details): Observable<any> {
    let url = this.url + 'v1/service_requests/'+details.id+'/reject';
    let token = localStorage.getItem('token');

    let data = {
      service_requests:details
    };
    return this.api.put(url,data,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  }


  CancelServicRequest(details): Observable<any> {
    let url = this.url + 'v1/service_requests/'+details.id+'/cancel';
    let token = localStorage.getItem('token');
    let data = {
      service_requests:details
    };
    return this.api.put(url,data,{ headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  };

  AddToCart(data):Observable<any>{
    let url = this.url + 'v1/cart_items/add_to_cart';
    console.log(data, '--------url',typeof data);
    let params =  {
      service_ids:data
    };
    // data.forEach(id => {
    //   params = params.append("service_ids",id);
    // });
    let token = localStorage.getItem('token');
    return this.api.post(url, params,{headers: new HttpHeaders().set('X-User-Token', token)}).pipe(map(result => {
      return result;
    }));
  };
}

