/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UtilsApi {
  // private readonly apiController: string = '/api/v1/datatable/getCatalogData';
  dummy: string = 'api/v1/datatable/getCatalogData';
  constructor(private api: HttpClient) { }

  getRole(): Observable<any[]> {
    return this.api.get<any>(this.dummy);
  }
}
