/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpService } from './http.service';
import { AddCatalog } from '../../../interfaces/common/addcatalog';
import { Catalog } from '../../../interfaces/common/catalog';
import { Count } from '../../../interfaces/common/count';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class CatalogApi {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  // private readonly apiController: string = '/api/v1/datatable/getCatalogData';
  dummy: string = 'api/v1/catalogs/getCatalogData';
  departmentapi: string = 'api/v1/departments/getDepartments';
  catalogAPi: string = '';
  addCatalogAPI: string = 'api/v1/catalogs/addCatalog';
  data: any;
  catalogCount: string = 'api/v1/utils/get/total/count';
  constructor(private api: HttpClient) {

  }
  cataloglist(): Observable<any[]> {
    return this.api.get<any>(this.dummy);
  }
  departmentList(): Observable<any[]> {
    return this.api.get<any>(this.departmentapi);
  }
  getCatalogCount(): Observable<any[]> {
    return this.api.get<any>(this.catalogCount);
  }

  updatedCatalogList(catalog: AddCatalog): Observable<any> {
    let updated_url = 'api/v1/catalogs/updateCatalog';
    return this.api.post(updated_url, catalog).pipe(map(result => {
      return result;
    }));
  }

  addCatalog(data:Catalog): Observable<any> {
    let API_URL = `${this.addCatalogAPI}`;
    // console.log("ADD", data);
    return this.api.post(API_URL, data)
      .pipe(
        catchError(this.errorMgmt)
      )
  }

  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
  updateCatalog(id: any, data: AddCatalog) {

  }

  getCatalogById(id: number) {
    return this.api.get<Catalog>('api/v1/catalogs/add' + '/' + id);
  }

  getCatalogDetailsById(id){
    var req_obj = {
      catalog_id:id
    };
    return this.api.post('/api/v1/catalogs/getCatalogById',req_obj).pipe((resp:any) => {
      return resp;
    });
  }

  getFileByCatalogId(id: any): Observable<any> {
    const url = `api/v1/catalogs/getFilesById`;
    var data = {
      id: id
    };
    return this.api.post(url, data).pipe((resp) => {
      return resp;
    });
  }

  getOwnersByCatalogId(id: any): Observable<any> {
    const url = `api/v1/catalogs/getOwnersById`;
    var data = {
      id: id
    };
    return this.api.post(url, data).pipe((resp) => {
      return resp;
    });
  }

  getTagsByCatalogId(id: any): Observable<any> {
    const url = `api/v1/catalogs/getTagsById`;
    var data = {
      id: id
    };
    return this.api.post(url, data).pipe((resp) => {
      return resp;
    });
  }

  getDepartmentsByCatalogId(id: any): Observable<any> {
    const url = `api/v1/catalogs/getDepartmentsById`;
    var data = {
      id: id
    };
    return this.api.post(url, data).pipe((resp) => {
      return resp;
    });
  }

  getTagsInList(): Observable<any> {
    const url = `api/v1/tags/getDistinctTags`;
    var search = {
      search: ""
    }
    return this.api.post(url, search).pipe((resp: any) => {
      return resp;
    });
  }

  deleteCatalogById(id: number): Observable<any> {
    return this.api.delete(`api/v1/catalogs/deleteCatalog/${id}`).pipe((resp: any) => {
      return resp;
    });
  }
}

