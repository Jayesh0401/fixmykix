import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { UserModel } from '../../../../@core/interfaces/common/usermodel';
import  { ApiService } from '../services/api.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
@Injectable()
export class AddressApi {
  url : any;
  constructor(private api: HttpService, private http: HttpClient,private apiService: ApiService) {
    this.url = apiService.url;
   }
   
   addAddress(data): Observable<any> {
    let url = this.url + 'v1/addresses';
    let token = localStorage.getItem('token');
    return this.http.post(url,data ,{ headers: new HttpHeaders({'X-User-Token':token}),
  }).pipe(map(result => {
      return result;
    }));
  };

  getAddress(): Observable<any> {
    let url = this.url + 'v1/addresses';
    let token = localStorage.getItem('token');
    return this.http.get(url,{ headers: new HttpHeaders({'X-User-Token':token}),
  }).pipe(map(result => {
      return result;
    }));
  };

  ValidateAddress(data): Observable<any> {
    let url = this.url + 'v1/united_parcel_services/validate';
    let token = localStorage.getItem('token');
    return this.http.post(url,data,{ headers: new HttpHeaders({'X-User-Token':token}),
  }).pipe(map(result => {
      return result;
    }));
  };

  
  getAddressById(id): Observable<any> {
    let url = this.url + 'v1/orders/'+id+'/address';
    let token = localStorage.getItem('token');
    let params =  new HttpParams();
    params = params.append("address_id",id);
    return this.http.get(url,{params:params,
      headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };

  saveAddressToOrder(id,address): Observable<any> {
    let url = this.url + 'v1/orders/'+id+'/address';
    let token = localStorage.getItem('token');
    let params =  new HttpParams();
    console.log(address);
    params = params.append("address_id",address.id);
    return this.http.get(url,{params:params,headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };

  
  service_demand(id): Observable<any> {
    let url = this.url + 'v1/orders/'+id+'/service_demand';
    let token = localStorage.getItem('token');
    return this.http.post(url,{},{headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };
}
