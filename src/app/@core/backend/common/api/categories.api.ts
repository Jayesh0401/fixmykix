import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import  { ApiService } from '../services/api.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
@Injectable()
export class CategoriesApi {
  url : any;
  private readonly apiController: string = 'users';
  constructor(private api: HttpService, private http: HttpClient,private apiService: ApiService) {
    this.url = apiService.url;
   }

  GetCategoriesDetails(data): Observable<any> {
    let url = this.url + 'v1/categories';
    let params =  new HttpParams();
    let token = localStorage.getItem('token');
    data.forEach(id => {
      params = params.append("categories_ids",id);
    });
    return this.http.get(url, {params: params}).pipe(map(result => {
      return result;
    }));
  };

  addService(data):Observable<any> {
    let url = this.url + 'v1/artist_services';
    let token = localStorage.getItem('token');
    return  this.http.post(url, data, { headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };
  GetServiceDetails(id):Observable<any> {
    let url = this.url + 'v1/artist_services/'+id;
    let token = localStorage.getItem('token');
    return  this.http.get(url,  { headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };

  deleteService(id):Observable<any> {
    let url = this.url + 'v1/artist_services/'+id;
    let token = localStorage.getItem('token');
    return  this.http.delete(url,  { headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));
  };
}
