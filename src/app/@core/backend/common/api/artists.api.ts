import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { UserModel } from '../../../../@core/interfaces/common/usermodel';
import  { ApiService } from '../services/api.service';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
@Injectable()
export class ArtistApi {
  url : any;
  private readonly apiController: string = 'users';

  constructor(private api: HttpService, private http: HttpClient,private apiService: ApiService) {
    this.url = apiService.url;
   }

  
  GetServicRequesteDetails(): Observable<any> {
    let url = this.url + 'v1/service_requests';
    let token = localStorage.getItem('token');
    return this.http.get(url, { headers: new HttpHeaders({'X-User-Token':token}),
  }).pipe(map(result => {
      return result;
    }));
  };

  GetServiceDetails(): Observable<any> {
    let url = this.url + 'v1/services?query';
    let token = localStorage.getItem('token');

    return this.http.get(url).pipe(map(result => {
      return result;
    }));
  };

  GetArtistDetailsById(id):Observable<any> {
    let url = this.url + 'v1/artists';
    let params =  new HttpParams();
    params = params.append("artist_ids",id);
    return this.http.get(url, {params:params}).pipe(map(result => {
      return result;
    }));
  };

  GetArtistDetails():Observable<any>{
    let url = this.url + 'v1/artists';
    console.log(url, '---------url');
    return this.http.get(url).pipe(map(result => {
      return result;
    }));
  };

  GetAristsByServiceIds(data):Observable<any>{
    let url = this.url + 'v1/artists?';
    let params =  new HttpParams();
    data.forEach(id => {
      params = params.append("service_ids",id);
    });
    return this.http.get(url, {params: params}).pipe(map(result => {
      return result;
    }));
  };

  GetArtistServicesById(id) {
    let url = this.url + 'v1/artist_services/';
    let token =  localStorage.getItem('token');
    return this.http.get(url,{ headers: new HttpHeaders({'X-User-Token':token})}).pipe(map(result => {
      return result;
    }));    
  }
}
