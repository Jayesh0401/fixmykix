import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable()
export class SearchFilterApi {

    constructor(private api: HttpService) { }

    searchFilter(): Observable<any> {
        const url = `api/v1/catalogs/searchCatalog`;
        var data = {
            "tags": [],
            "department": [],
            "assets_types": [],
            "incidence_date":"",
            "catalog_location":"",
            "catalog_type":""
        }
        return this.api.post(url, data).pipe((resp) => {
            return resp
        })
    }
}