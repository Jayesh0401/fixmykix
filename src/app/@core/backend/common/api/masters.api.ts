/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpService } from './http.service';
import { AddCatalog } from '../../../interfaces/common/addcatalog';
import { Catalog } from '../../../interfaces/common/catalog';
import { Count } from '../../../interfaces/common/count';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class MastersApi {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private api: HttpClient) {
  }
  
  GetMastersList(data): Observable<any> {
    let url = 'api/v1/masters/get/list';
    return this.api.post(url, data).pipe(map(result => {
      return result;
    }));
  };
  addMaster(data): Observable<any>{
    let url = 'api/v1/masters/add';
    return this.api.post(url,data).pipe(map(result =>{
      return result;
    }))
  }

  updateMaster(data): Observable<any>{
    let url = 'api/v1/masters/update';
    return this.api.post(url,data).pipe(map(result =>{
      return result;
    }));
  }

  GetAllMasterDetails(): Observable<any>{
    let url = 'api/v1/masters/get/all/masters';
    return this.api.post(url,{}).pipe(map(result =>{
      return result;
    }));
  }
}

