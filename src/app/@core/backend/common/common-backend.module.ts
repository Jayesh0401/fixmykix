import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserData } from '../../interfaces/common/users';
import { Catalog } from '../../interfaces/common/catalog';
import { UsersService } from './services/users.service';
import { UsersApi } from './api/users.api';
import { HttpService } from './api/http.service';
import { CountryData } from '../../interfaces/common/countries';
import { CountriesService } from './services/countries.service';
import { CountriesApi } from './api/countries.api';
import { SettingsApi } from './api/settings.api';
import { SettingsData } from '../../interfaces/common/settings';
import { SettingsService } from './services/settings.service';
import { UploadService } from './services/upload.service';
import { SearchFilterService } from './services/search-filter.service';
import { ArtistApi } from './api/artists.api';
import { ApiService } from  './services/api.service';
import { ArtistService } from './services/artists.services';
import { ServiceRequestService } from './services/service_request.service';
import { ServiceRequestApi } from './api/service_request.api';
import { ChatService }  from './services/chat.services';
import { CategoriesService } from './services/categories.service';
import  { CategoriesApi } from './api/categories.api';
import { AddressService } from './services/address.service';
import { AddressApi } from './api/address.api';

const API = [UsersApi, CountriesApi, SettingsApi, HttpService, UploadService, SearchFilterService,ApiService, ArtistApi,ServiceRequestApi, CategoriesApi,AddressApi];

const SERVICES = [
  { provide: UserData, useClass: UsersService },
  { provide: CountryData, useClass: CountriesService },
  { provide: SettingsData, useClass: SettingsService },
  ChatService,
  CategoriesService,
  AddressService
];

@NgModule({
  imports: [CommonModule],
})
export class CommonBackendModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CommonBackendModule,
      providers: [
        ...API,
        ...SERVICES,
        ArtistService,
        ServiceRequestService
      ],
    };
  }
}
