import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable,of as observableOf } from 'rxjs';
import  { ApiService } from './api.service';
@Injectable()
export class AuthenticationService {
  url : any
  constructor(private router: Router, private http: HttpClient,private apiService: ApiService) {
    this.url = apiService.url;
  }
  login(user_details) {
    let login_url =  this.url + '/v1/users/sign_in';
    return this.http.post<any>(login_url, user_details)
      .pipe(map(user => {
        return user;
      }));
  }

  public isSession(): Observable<boolean> {
    let user = localStorage.getItem('user');
    this.router.navigate(['/auth/login']);
    return observableOf(false);
  }

  public isLogOut(): Observable<boolean> {
    let url =  this.url + '/v1/users/logout'
    return this.http.get<any>(`/api/v1/user/logout`, {}).pipe(
      map(resp => {
        if (resp.success) {
          localStorage.removeItem('token');
          localStorage.clear();
          this.router.navigate(['/auth/login']);
          return false;
        } else {
          return true;
        }
      }));
  }

  public register(user: any): Observable<boolean> {
    return this.http.post<any>(`/api/v1/user/add`, user).pipe(
      map(resp => {
        if (resp.success) {
          localStorage.clear();
          resp.redirect = '/auth/login';
          this.router.navigate(['/auth/login']);
          return false;
        } else {
          return true;
        }
      }));
  }
}