import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UsersApi } from '../api/users.api';
import { UserData, User } from '../../../interfaces/common/users';
import { UserModel } from 'src/app/@core/interfaces/common/usermodel';


@Injectable()
export class UsersService extends UserModel {
  userModel: any;
  constructor(private api: UsersApi, private http: HttpClient) {
    // this.userModel = this.api.userModel;
    super();
    this.userModel = new UserModel();
  }
  resetUserModel() {
    this.userModel = new UserModel();
  }

  getModel() {
    return this.userModel;
  }
  get gridDataSource() {
    return this.api.usersDataSource;
  }

  list(pageNumber: number = 1, pageSize: number = 10): Observable<User[]> {
    return this.api.list(pageNumber, pageSize);
  }

  get(id: number): Observable<User> {
    return this.api.getUserById(id);
  }

  create(user: any): Observable<any> {
    return this.api.add(user);
  }

  update(user: any): Observable<User> {
    return this.api.update(user);
  }

  updateCurrent(user: any): Observable<User> {
    return this.api.updateCurrent(user);
  }

  delete(id: number,status:any): Observable<boolean> {
    return this.api.deleteUser(id,status);
  }
  getUserRoles(): Observable<any> {
    return this.api.getUserRoles();
  }
  getUsersList(): Observable<any> {
    return this.api.getUsersList()
  }
  updateUser(user: any): Observable<any> {
    return this.api.updateUser(user);
  }

  logout(): Observable<any> {
    return this.api.logout();
  }
}
