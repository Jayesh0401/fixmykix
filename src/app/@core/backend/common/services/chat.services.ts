import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChatService {
	constructor(private socket: Socket) { }

	sendMessage(msg: string){
		this.socket.emit("message", msg);
	}
	getMessage() {
		return Observable.create((observer) => {
            this.socket.on('messages', (message) => {
				observer.next(message);
				return message;
			});
        });
	}
}