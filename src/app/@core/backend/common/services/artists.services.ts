import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArtistApi } from '../api/artists.api';
@Injectable()
export class ArtistService {
  artist_id:'';
  artist_details:any;
  constructor(private api: ArtistApi) {
    // super();
  }

  GetServicRequesteDetails(): Observable<any> {
    console.log('Service Called');
    return this.api.GetServicRequesteDetails();
  }

  GetServiceDetails() {
    return this.api.GetServiceDetails();
  }

  GetArtistDetailsById(id):Observable<any>{
    return this.api.GetArtistDetailsById(id);
  }

  GetArtistDetails():Observable<any>{
    return this.api.GetArtistDetails();
  };

  GetAristsByServiceIds(data):Observable<any>{
    return this.api.GetAristsByServiceIds(data);
  };

  GetArtistServicesById(id):Observable<any>{
    return this.api.GetArtistServicesById(id);
  };
}
