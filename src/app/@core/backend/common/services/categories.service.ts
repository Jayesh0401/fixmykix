import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoriesApi } from '../api/categories.api';
@Injectable()
export class CategoriesService {
  service_id:'';
  service_details:any;
  constructor(private api: CategoriesApi) {
  }

  GetCategoriesDetails(data): Observable<any> {
    return this.api.GetCategoriesDetails(data);
  };

  addService(data): Observable<any> {
    return this.api.addService(data);
  };

  GetServiceDetails(id):Observable<any> {
    return this.api.GetServiceDetails(id);
  };

  deleteService(id):Observable<any> {
    return this.api.deleteService(id);
  };
}
