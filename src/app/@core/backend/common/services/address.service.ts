import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddressApi } from '../api/address.api';
@Injectable()
export class AddressService {
  order_id:'';
  constructor(private api: AddressApi) {};

  addAddress(data): Observable<any> {
    return this.api.addAddress(data);
  };
  getAddress(): Observable<any> {
    return this.api.getAddress();
  };

  ValidateAddress(data): Observable<any> {
    return this.api.ValidateAddress(data);
  }
  getAddressById(id):Observable<any> {
    return this.api.getAddressById(id);
  };

  saveAddressToOrder(id,address):Observable<any> {
    return this.api.saveAddressToOrder(id,address);
  };
  service_demand(id):Observable<any> {
    return this.api.service_demand(id);
  };
}
