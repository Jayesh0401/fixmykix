/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UtilsApi } from '../api/utils.api';
import { Catalog } from '../../../interfaces/common/catalog';
import { UserData, User } from '../../../interfaces/common/users';
@Injectable()
export class UtilService extends Catalog {
  usersList = [];
  usersRoles = [];
  current_user = {};
  constructor(private api: UtilsApi, private usersService: UserData) {
    super();
    if(this.usersList.length == 0) {
      this.usersListApi();
    }
    if(this.usersRoles.length == 0) {
      this.UsersRolesApi();
    }
  }
  usersListApi() {
  }

  UsersRolesApi() {

  }

  getUserRoles(){
    return this.usersRoles;
  }

  getUsersList(){
    return this.usersList;
  }

  setCurrentUser(user: any) {
    this.current_user = user;
  };

  getCurrentUser(){
    return this.current_user;
  }

  getRole(): Observable<Catalog[]> {
    return this.api.getRole();
  }
}
