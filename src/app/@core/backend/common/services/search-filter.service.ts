import { Injectable } from '@angular/core';
import { SearchFilterApi } from '../api/search-filter.api';
import { Observable } from 'rxjs';
import { HttpClient, HttpEventType } from '@angular/common/http';
@Injectable()
export class SearchFilterService {
	criteria = {
		"tags": [],
		"catalog_departments": [],
		"assets_types": [],
		"incident_date": "",
		"catalog_location": [],
		"catalog_classification": "",
		"src_system": [],
		"catalog_type": ""
	}
	constructor(public api: HttpClient) {
	}
	getCriteria() {
		return this.criteria;
	}
	searchFilter(): Observable<any> {
		const url = `api/v1/catalogs/searchCatalog`;
		console.log(this.criteria, '----this.criteria---');
		return this.api.post(url, this.criteria).pipe((resp) => {
			return resp;
		});
	}

	setTags(tags: any) {
		this.criteria.tags = tags;
	};
	setDepartment(depts: any) {
		this.criteria.catalog_departments = depts;
	};

	setAssetTypes(assets_types: any) {
		this.criteria.assets_types = assets_types;
	};

	setIncident_date(incident_date: any) {
		debugger
		this.criteria.incident_date = incident_date;
		console.log("setIncident_date", this.criteria.incident_date);
		
	};

	setCatalogLocation(catalog_location: any) {
		this.criteria.catalog_location = catalog_location;
	};

	setCatalog_type(catalog_type: any) {
		this.criteria.catalog_type = catalog_type;
	};

	setClassification(catalog_classification: any) {
		this.criteria.catalog_classification = catalog_classification;
	}
	setSourceSystem(catalog_source: any) {
		this.criteria.src_system = catalog_source;
		// console.log("Source System", this.criteria.src_system)
	}

	resetSearch() {
		this.setTags([]);
		this.setDepartment([]);
		this.setAssetTypes([]);
		this.setIncident_date('');
		this.setCatalogLocation([]);
		this.setCatalog_type('');
		this.setClassification('');
		this.setSourceSystem('');
	}
}
