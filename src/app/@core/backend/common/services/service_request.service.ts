import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServiceRequestApi } from '../api/service_request.api';

@Injectable()
export class ServiceRequestService {
  price:''
  constructor( private api : ServiceRequestApi) {
    // super();
  }

  GetServicRequesteDetails(): Observable<any> {
    return this.api.GetServicRequesteDetails();
  }

  SaveServicRequesteDetails(details): Observable<any> {
    return this.api.SaveServicRequesteDetails(details);
  }

  AcceptServicRequest(details): Observable<any> {
    return this.api.AcceptServicRequest(details);
  }

  RejectServicRequest(details): Observable<any> {
    return this.api.RejectServicRequest(details);
  }

  CancelServicRequest(details): Observable<any> {
    return this.api.CancelServicRequest(details);
  }
  AddToCart(ids): Observable<any> {
    return this.api.AddToCart(ids);
  }
}
