/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable()
export class UploadService {
  rowData: any;
  constructor(private httpClient: HttpClient) {
  }
  // public upload(data: any): Observable<any> {
  //   let uploadURL = 'api/v1/fileUpload/upload';
  //   return this.httpClient.post<any>(uploadURL, data, {
  //     reportProgress: true,
  //     observe: 'events'
  //   }).pipe(map((event) => {
  //     switch (event.type) {
  //       case HttpEventType.Response:
  //         return event.body;
  //       default:
  //         return `Unhandled event: ${event.type}`;
  //     }
  //   })
  //   );
  // }
  public upload(formData: any): Observable<any> {
    let uploadURL = 'api/v1/fileUpload/upload';
    return this.httpClient.post<any>(uploadURL, formData).pipe((resp) => {
      return resp;
    });
  }

  public deleteById(new_obj: any): Observable<any> {
    let url = 'api/v1/utils/deletebyId';
    return this.httpClient.post<any>(url, new_obj).pipe((resp)=>{
      return resp
    })
  }

  public filedeleteById(id: any): Observable<any> {
    let url = 'api/v1/fileUpload/deleteFile';
    return this.httpClient.post<any>(url,id).pipe((resp)=>{
      return resp;
    })
  }

  public convertFileToBase64(filesArr: any): Observable<any> {
    let uploadURL = 'api/v1/utils/convertToBase64';
    return this.httpClient.post<any>(uploadURL, filesArr).pipe((resp) => {
      return resp;
    });
  }
}
