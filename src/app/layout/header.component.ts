import { Component } from '@angular/core';
import { AuthenticationService } from '../@core/backend/common/services/authentication.service';
import { UsersService } from '../@core/backend/common/services/users.service';
import { ToastrService }  from 'ngx-toastr';
import { Router } from '@angular/router';

declare var $: any;
@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  isLoggedIn =  false;
  user:any;
  role:any;
  constructor(private authService: AuthenticationService,private usersService: UsersService,private toastrService: ToastrService, private router:  Router) {
    this.user = localStorage.getItem('user');
    this.role =  localStorage.getItem('role');
    if(this.user) {
      this.isLoggedIn =  true;
    }
    $('.title').hover(function() {
      $('.hover-dropdown').toggleClass('toggled');
    }); 
  }
  logout() {
    this.usersService.logout().subscribe((result) => {
      if(result.status) {
        this.toastrService.success('Success',result.message);
        localStorage.clear();
        window.location.href="";
      } else {
        this.toastrService.error('Error',result.message);
      }
    });
  }

  myRequest() {
    this.router.navigate(['customer/my/request']);
  }

  profileRoute() {
    if(this.role) {
      if(this.role == '1') {
        window.location.href="artist/profile"
      } else {
        window.location.href="customer/profile"
      }
    }
  }
}
