import { Component } from '@angular/core';
import { AuthenticationService } from '../@core/backend/common/services/authentication.service';
@Component({
  selector: 'app-footer',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html',
})
export class FooterComponent {
  constructor(private authService: AuthenticationService) {}

  logout() {
    this.authService.isLogOut().subscribe((res:any) => {
      // console.log("res", res);
    });
  }
}
