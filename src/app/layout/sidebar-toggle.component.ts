import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-sidebar-toggle',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./sidebar-toggle.component.scss'],
  templateUrl: `./sidebar-toggle.component.html`,
})
export class SidebarToggleComponent implements OnInit {

  constructor(private sidebarService: NbSidebarService) {
    this.sidebarService.toggle(false);
  }

  ngOnInit() {
  }

  toggle() {
    this.sidebarService.toggle(false);
    return false;
  }
}
