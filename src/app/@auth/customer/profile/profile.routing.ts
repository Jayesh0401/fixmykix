import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import  { ProfileComponent }  from  './profile.component';

const routes: Routes = [{
	path: '',
	component: ProfileComponent,
	// children: [
	// 	{
	// 		path: 'list',
	// 		component: MastersListComponent,
	// 	},
	// 	{
	// 		path: 'add',
	// 		component: MastersAddComponent,
	// 	},
	// 	{
	// 		path: '',
	// 		redirectTo: 'list',
	// 		pathMatch: 'full'
	// 	},
	// ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes), FormsModule],
    exports: [RouterModule],
})
export class ProfileRoutingModule {
}
