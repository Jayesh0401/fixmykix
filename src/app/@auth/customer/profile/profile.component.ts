import { Component } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { ColorPalette } from './../../../shared/color-palette';
import { UsersService } from './../../../@core/backend/common/services/users.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'underscore';
import { $ } from 'jquery';
@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  
})
export class ProfileComponent {
  colorTheme: any;
  formGroup: any;
  colorPaletteProps: any[];
  colorPaletteArr: [];
  arrayOfObj: any;
  selectedOption: any;
  selectedOptionnew: any; 
  user:any;
  role:any;
  constructor( private userService:UsersService,
    private fb: FormBuilder,private toasterService: ToastrService
    ) {
    // $('#profile_btn').click();
    this.role = localStorage.getItem("role");
    this.user = JSON.parse(localStorage.getItem("user"));

    this.formGroup = this.fb.group({
      first_name: ['', Validators.required],
      last_name: ['',Validators.required],
      email:[ '',Validators.required],
      mobile_number:['',Validators.required],
      age:['',Validators.required],
      gender:['',Validators.required]
    });
    // this.formGroup = this.fb.group({
    //   first_name: [this.user.first_name, Validators.required],
    //   last_name: [this.user.last_name,Validators.required],
    //   email: [this.user.email,Validators.required],
    //   mobile_number:[this.user.mobile_number,Validators.required],
    //   age:[this.user.age,Validators.required],
    //   gender:[this.user.gender,Validators.required]
    // });
  }
	ngOnInit() {}

  ngAfterViewInit() {
    // $('#profile_btn').click();
  }

  UpdateUser() {
    let artist_detail ={}
    artist_detail = {
      artist_detail:this.formGroup.value
    };
    this.userService.updateUser(artist_detail).subscribe((result) => {
      if(result.success){
        this.toasterService.success("User saved !!", result.message);
      }
    });
  }
}
