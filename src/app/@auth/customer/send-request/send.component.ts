import { ChangeDetectionStrategy,Component,OnInit ,ChangeDetectorRef} from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddressService } from '../../../@core/backend/common/services/address.service';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
import { ServiceRequestService } from '../../../@core/backend/common/services/service_request.service';
import * as _ from 'underscore';
declare var $: any;

@Component({
  selector: 'ngx-send-request',
  styleUrls: ['./send-request.component.scss'],
  templateUrl: './send-request.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxCustomerSendRequestComponent implements OnInit {
  formGroup: any;
  id:any;
  user:any;
  service_request_form:any;
  artist_details:any;
  artist_services:[];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'service_name',
    enableCheckAll: true,
    selectAllText: 'Select All',
    unSelectAllText: 'De Select All',
    allowSearchFilter: true,
    limitSelection: -1,
    clearSearchFilter: true,
    maxHeight: 197,
    itemsShowLimit: 10,
    searchPlaceholderText: 'Search Service',
    noDataAvailablePlaceholderText: 'No service found',
    closeDropDownOnSelection: false,
    showSelectedItemsAtTop: false,
    defaultOpen: false
  };

  constructor(private fb: FormBuilder, private artistService: ArtistService, private ref: ChangeDetectorRef, private serviceRequestService: ServiceRequestService,private toasterService: ToastrService,private router: Router,private addressService:AddressService) {
    this.user  = localStorage.getItem('user');
    if(this.user) {
      if(typeof this.user == 'string'){
        this.user = JSON.parse(this.user);
      }
      this.id =  this.artistService.artist_id;
      if(!this.id) {
        this.id =  localStorage.getItem('artist_id');
      }
      this.GetArtistById(this.id);
      this.service_request_form = {
        user_name: this.user.first_name,
        artist_name:'',
        description:'',
        artist_service_ids:[]
      }
    } else {
      this.toasterService.error("Error!!", 'User must be logged in');
      this.router.navigate(['login']);
    }
  }

  ngOnInit(): void {}

  SendRequest(formDetails) {
    
    this.serviceRequestService.AddToCart(this.service_request_form.artist_service_ids).subscribe((result) => {
      console.log(result);
      if(result.status) {
        // formDetails.order_id = result.data.id;
        this.AddRequest(result.data.id,formDetails);
      //   this.toasterService.success("Success!!", 'Your Request has been sent to Artist');
      //   // this.router.navigate(['artist/payment']);
      }
    });
  };

  AddRequest(order_id,formDetails) {
    
    formDetails.image = '';
    formDetails.price = '';
    formDetails.artist_service_ids = this.service_request_form.artist_service_ids;
    formDetails.artist_id = this.id;
    formDetails.order_id = order_id;
    let data = {
      service_request:formDetails
    };

    this.serviceRequestService.SaveServicRequesteDetails(data).subscribe((result) => {
      if(result.status) {
        this.toasterService.success("Success!!", 'Your Request has been sent to Artist');
        this.addressService.order_id =  order_id;
        localStorage.setItem('order_id',order_id);
        this.router.navigate(['customer/validate/address']);
      }
    });
  }

  GetArtistById(id){
    this.artistService.GetArtistDetailsById(id).subscribe((result) => {
      if(result.status) {
        let find_artist = _.findWhere(result.data,{id:parseInt(id)});
        if(find_artist){
          this.artist_details = find_artist;
          this.service_request_form.artist_name = this.artist_details.first_name;
          this.artist_services =  find_artist.artist_services;
          this.dropdownList =  find_artist.artist_services;
      		this.ref.markForCheck();
        }
      }
    });
  };

  onItemSelect(item:any){
    this.service_request_form.artist_service_ids.push(item.id);
  };

  OnItemDeSelect(item:any) {
    let index =  this.service_request_form.artist_service_ids.indexOf(item.id);
    if(index > -1) {
      this.service_request_form.artist_service_ids.splice(index, 1);
    }
  };

  onSelectAll(items: any){
    let selected_items = [];
    selected_items = _.pluck(items,'id');
    this.service_request_form.artist_service_ids = selected_items;
    this.service_request_form.artist_service_ids = _.uniq(this.service_request_form.artist_service_ids);
  };

  onDeSelectAll(items: any){
    this.service_request_form.artist_service_ids = [];
  };
}

