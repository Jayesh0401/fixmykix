import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { CustomerComponent } from './customer.component';
import { ArtistRoutingModule } from './customer-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { NgxCustomerSendRequestComponent } from './send-request/send.component';
import { NgxCustomerMyServiceRequestComponent } from './my-request/my-request.component';
import { AddressComponent } from './address/address.component';
import { OrderSummaryComponent } from './order-summay/order-summary.component';

const PAGES_COMPONENTS = [
  CustomerComponent,
  ProfileComponent,
  NgxCustomerSendRequestComponent,
  NgxCustomerMyServiceRequestComponent,
  AddressComponent,
  OrderSummaryComponent
];

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';

@NgModule({
  imports :[
    CommonModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatAutocompleteModule,
    AngularMultiSelectModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    Ng2SmartTableModule,
    FormsModule, 
    ReactiveFormsModule,
    ArtistRoutingModule
  ],
  declarations: [
    PAGES_COMPONENTS
  ],
  providers: [
    CustomerComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class CustomerModule {
}
