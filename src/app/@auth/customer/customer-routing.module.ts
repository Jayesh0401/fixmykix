import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CustomerComponent } from './customer.component';
import { ProfileComponent } from './profile/profile.component';
import { NgxCustomerSendRequestComponent } from './send-request/send.component';
import { NgxCustomerMyServiceRequestComponent } from './my-request/my-request.component';
import { AddressComponent } from './address/address.component';
import { OrderSummaryComponent } from './order-summay/order-summary.component';
const routes: Routes = [{
  path: '',
  component: CustomerComponent,
  children: [
    {
      path:'profile',
      component:ProfileComponent
    },
    {
      path: '',
      redirectTo: 'profile',
      pathMatch: 'full',
    },
    {
      path: '',
      redirectTo: 'profile',
    },{
      path:'send/request',
      component:NgxCustomerSendRequestComponent
    },{
      path:'my/request',
      component:NgxCustomerMyServiceRequestComponent
    },{
      path:'validate/address',
      component:AddressComponent
    },  {
      path:'order-summary',
      component:OrderSummaryComponent
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArtistRoutingModule {
}
