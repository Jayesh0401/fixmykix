import { Component, OnDestroy,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceRequestService } from '../../../@core/backend/common/services/service_request.service';
@Component({
  selector: 'ngx-customer-order-summary',
  templateUrl: `./order-summary.component.html`,
  styleUrls: ['./order-summary.component.scss'],

})
export class OrderSummaryComponent implements OnInit {
  price:any;
  total:any;
  constructor(private serviceRequestService: ServiceRequestService,private router:Router) {
    this.price =  this.serviceRequestService.price;
    this.total =  this.serviceRequestService.price + 200;
    if(!this.price) {
			this.router.navigate(['customer/my/request']);
		}
  }

  ngOnInit(): void {}

  pay() {
    this.router.navigate(['artists/payment']);
  }
}
