import { ChangeDetectionStrategy,Component,OnInit ,ChangeDetectorRef} from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
import { ServiceRequestService } from '../../../@core/backend/common/services/service_request.service';
import * as _ from 'underscore';
declare var $: any;

@Component({
  selector: 'ngx-customer-my-request',
  styleUrls: ['./my-request.component.scss'],
  templateUrl: './my-request.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxCustomerMyServiceRequestComponent implements OnInit {
  artist_services_Requests:[];
  constructor(private fb: FormBuilder, private artistService: ArtistService, private ref: ChangeDetectorRef, private serviceRequestService: ServiceRequestService,private toasterService: ToastrService,private router: Router) {
  }

  ngOnInit(): void {
    this.GetServiceRequestDetails();
  }

  GetServiceRequestDetails(){
    this.serviceRequestService.GetServicRequesteDetails().subscribe((result) => {
      if(result.status){
        this.artist_services_Requests = result.data.service_requests;
    		this.ref.markForCheck();
      }
    });
  }

  Cancel_Request(request) {
    var service_request = {
      description:request.description,
      artist_id:request.artist_id,
      price:request.price,
      image:request.image,
      artist_service_ids:request.artist_service_ids,
      id:request.id,
      status: 'cancelled'
    };
    this.serviceRequestService.CancelServicRequest(service_request).subscribe((result) => {
      if(result.status) {
        this.GetServiceRequestDetails();
      }
    });
  };

  payment_Request(requests)  {
    this.serviceRequestService.price =  requests.price;
    this.router.navigate(['customer/order-summary']);
  };
}

