import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { NbMenuItem } from '@nebular/theme';
@Component({
  selector: 'ngx-customer',
  templateUrl: `./customer.component.html`,
  styleUrls: ['./customer.component.scss'],

})
export class CustomerComponent implements OnDestroy {
  alive: boolean = true;
  loading = true;
  constructor() {
    this.initMenu();
  }
  initMenu() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
