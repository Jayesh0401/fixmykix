import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { AddressService } from '../../../@core/backend/common/services/address.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'ngx-customer-address',
  templateUrl: `./address.component.html`,
  styleUrls: ['./address.component.scss'],

})
export class AddressComponent implements OnInit {
  formGroup: any;
	user:any;
	addRessList:any;
	order_id:any;
  constructor(private fb: FormBuilder,private addressService:AddressService,private toasterService:ToastrService,private router:Router) {
		this.user  = localStorage.getItem('user');
		this.order_id =  this.addressService.order_id;
		if(!this.order_id) {
			this.order_id =  localStorage.getItem('order_id');
		}
    if(this.user) {
		if(typeof this.user == 'string'){
			this.user = JSON.parse(this.user);
		}
	} else {
		this.toasterService.error("Error!!", 'User must be logged in');
		this.router.navigate(['login']);
	}
    this.formGroup = this.fb.group({
			street: ['', Validators.required],
			city: ['',Validators.required],
			state:[ '',Validators.required],
			zip:['',Validators.required]
		});
	}

	ngOnInit() {
		this.GetAddressList();
	}

	GetAddressList() {
		this.addressService.getAddress().subscribe((result) => {
			if(result.status) {
				this.addRessList = result.data;
			}
		});
	}

	submit() {
		let data =  this.formGroup.value;
		this.addressService.addAddress(data).subscribe((result) => {
			if(result.status){
				this.toasterService.success("Address saved !!", result.message);
			}
		});
	};

	onCheckboxChange(address) {
		let data = {
			city:address.city,
			posta_code:address.zip
		};
		this.addressService.ValidateAddress(data).subscribe((result) => {
			if(result.status) {
				this.saveAddressToOrder(this.order_id,address);
			}
		});
	};

	saveAddressToOrder(id,address) {
		this.addressService.saveAddressToOrder(id,address).subscribe((result) => {
			console.log(result, '--------result');
			if(result.status) {
				this.service_demand(id);
			}
		});
	}

	getAddressById(id) {
		this.addressService.getAddressById(id).subscribe((result) => {
			console.log(result, '--------result');
		});
	};

	service_demand(id){
		this.addressService.service_demand(id).subscribe((result) => {
			console.log(result, '--------result');
			if(result.status) {
				this.toasterService.success("Order request !!", result.message);
				this.router.navigate(['']);
			}
		});
	}
}
