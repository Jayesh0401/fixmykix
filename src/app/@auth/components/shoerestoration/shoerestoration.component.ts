import { ChangeDetectionStrategy,Component,OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'ngx-shoe-restoration',
  styleUrls: ['./shoerestoration.component.scss'],
  templateUrl: './shoerestoration.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxShoeRestorationComponent implements OnInit {
  formGroup: any;
  constructor() {}

  ngOnInit(): void {}
}
