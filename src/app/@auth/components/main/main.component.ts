import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../@core/backend/common/services/authentication.service';
import { UtilService } from '../../../@core/backend/common/services/utils.service';
@Component({
  selector: 'ngx-main',
  styleUrls: ['./main.component.scss'],
  templateUrl: './main.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxMainComponent implements OnInit {

  constructor(
    private utilsService: UtilService,
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    protected router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {}

  explore_artists() {
    this.router.navigate(['artist/explore']);
  }
}
