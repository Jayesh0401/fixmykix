import { ChangeDetectionStrategy,Component,OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'ngx-about-us',
  styleUrls: ['./about-us.component.scss'],
  templateUrl: './about-us.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxAboutUsComponent implements OnInit {
  formGroup: any;
  constructor() {}

  ngOnInit(): void {}
}
