/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */
import { Component, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { takeWhile } from 'rxjs/operators';
// <nb-layout>
// <nb-layout-column>
//   <nb-card>
//     <nb-card-header>
//       <nav class="navigation">
//         <a href="#" (click)="back()" class="link back-link" aria-label="Back">
//           <nb-icon icon="arrow-back"></nb-icon>
//         </a>
//       </nav>
//     </nb-card-header>
//     <nb-card-body>
//       <nb-auth-block>
//       </nb-auth-block>
//     </nb-card-body>
//   </nb-card>
// </nb-layout-column>
// </nb-layout>
@Component({
  selector: 'ngx-auth',
  styleUrls: ['./auth.component.scss'],
  templateUrl: './auth.component.html'
})
export class NgxAuthComponent implements OnDestroy {

  private alive = true;

  subscription: any;

  authenticated: boolean = false;
  token: string = '';

  // showcase of how to use the onAuthenticationChange method
  constructor(protected location: Location) {

    // this.subscription = auth.onAuthenticationChange()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((authenticated: boolean) => {
    //     this.authenticated = authenticated;
    //   });
  }

  back() {
    this.location.back();
    return false;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
