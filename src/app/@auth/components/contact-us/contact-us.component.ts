import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../@core/backend/common/services/authentication.service';
import { UtilService } from '../../../@core/backend/common/services/utils.service';
@Component({
  selector: 'ngx-contact-us',
  styleUrls: ['./contact-us.component.scss'],
  templateUrl: './contact-us.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxContactUsComponent implements OnInit {

  constructor(
    private utilsService: UtilService,
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    protected router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {}
}
