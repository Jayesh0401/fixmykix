import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../@core/backend/common/services/authentication.service';
import { UtilService } from '../../../@core/backend/common/services/utils.service';
import { UsersService } from '../../../@core/backend/common/services/users.service';
import { ToastrService } from 'ngx-toastr'

declare var $: any;

@Component({
  selector: 'ngx-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxLoginComponent implements OnInit {
  formGroup: any;
  constructor(
    private utilsService: UtilService,
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    protected router: Router,
    private authenticationService: AuthenticationService,  private UsersService: UsersService,private toasterService: ToastrService) {
      this.formGroup = this.fb.group({
        email: ['',Validators.required],
        password: ['',Validators.required]
     });
    }

  ngOnInit(): void {}
  
  login() {
    let user_details ={ user: this.formGroup.value};
    this.authenticationService.login(user_details).subscribe((result) => {
      if(result.status){
        localStorage.setItem('token',result.data.registration_token);
        this.toasterService.success("Login successfully !!", result.message);
        localStorage.setItem('userid',result.data.id);
        localStorage.setItem('user',JSON.stringify(result.data));
        localStorage.setItem('role',result.data.role);
        window.location.href="";
        // this.router.navigate(['']);
      } else {
        this.toasterService.error("Login unsuccessful !!", result.message);
      }
    });
  };
}
