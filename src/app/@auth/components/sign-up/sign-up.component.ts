import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../@core/backend/common/services/authentication.service';
import { UtilService } from '../../../@core/backend/common/services/utils.service';
import { UsersService } from '../../../@core/backend/common/services/users.service';
import { ToastrService } from 'ngx-toastr';


declare var $: any;

@Component({
  selector: 'ngx-contact-us',
  styleUrls: ['./sign-up.component.scss'],
  templateUrl: './sign-up.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxSignUpComponent implements OnInit {
  formGroup: any;
  role : 0;
  constructor(
    private utilsService: UtilService,
    protected cd: ChangeDetectorRef,
    private fb: FormBuilder,
    protected router: Router,
    private authenticationService: AuthenticationService,  private UsersService: UsersService,private toasterService: ToastrService) {
      this.formGroup = this.fb.group({
        first_name: ['', Validators.required],
        last_name: ['',Validators.required],
        email: ['',Validators.required],
        password: ['',Validators.required],
        cpassword: ['',Validators.required],
        role: ['0',Validators.required],
        zipcode:['',Validators.required]
     });
    }

  ngOnInit(): void {}
  
  onSubmit() {
    let user_details ={
      user:this.formGroup.value
    }
    this.UsersService.create(user_details).subscribe(res=> {
      if(res.status){
        this.toasterService.success("User Registered successfully !!", res.message);
        this.login();
      } else {
        this.toasterService.error("User Not Registered", res.message);
      }
    });
  }

  login() {
    let user_details ={user: this.formGroup.value};
    this.authenticationService.login(user_details).subscribe((result) => {
      if(result.status){
        localStorage.setItem('token',result.data.registration_token);
        localStorage.setItem('userid',result.data.id);
        localStorage.setItem('user',JSON.stringify(result.data));
        localStorage.setItem('role',result.data.role);
        this.router.navigate(['/user/profile']);
      } else {
        this.toasterService.error("Login unsuccessful !!", result.message);
      }
    });
  };
  changeUserRole(role) {
    this.role = role;
  }
}
