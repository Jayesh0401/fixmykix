import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {IsAuthGuard } from './isAuth.guard'
import { NgxMainComponent } from './components/main/main.component';
import  { NgxContactUsComponent } from  './components/contact-us/contact-us.component';
import { NgxSignUpComponent } from './components/sign-up/sign-up.component';
import { NgxAboutUsComponent } from  './components/about-us/about-us.component';
import { NgxShoeRestorationComponent } from './components/shoerestoration/shoerestoration.component';
import { NgxShoeArtComponent } from './components/shoeart/shoeart.component';
import  { NgxFixmyKixComponent } from './components/what-is-fixmykix/what-is-fixmykix.component';
import {
  NgxAuthComponent,
  NgxLoginComponent,
  NgxRegisterComponent,
  
} from './components';

const routes: Routes = [{
  path: '',
  component: NgxAuthComponent,
  children: [
    {
      path: '',
      component: NgxMainComponent
    },
    {
      path: 'login',
      component: NgxLoginComponent
    },
    {
      path: 'register',
      component: NgxRegisterComponent
    },
    {
      path: 'contact-us',
      component: NgxContactUsComponent
    },
    {
      path:'create/account',
      component: NgxSignUpComponent
    },
    {
      path:'about-us',
      component: NgxAboutUsComponent
    },{
      path:'shoe/restoration',
      component: NgxShoeRestorationComponent
    },{
      path:'shoe/art',
      component: NgxShoeArtComponent
    },{
      path:'what/is/fixmykix',
      component: NgxFixmyKixComponent
    },{
      path: 'customer',
      loadChildren: () => import('./customer/customer.module')
        .then(m => m.CustomerModule),
    },{
      path: 'artist',
      loadChildren: () => import('./artist/artist.module')
    .then(m => m.ArtistModule),
    },{
      path: 'users',
      loadChildren: () => import('./chat/chat.module')
    .then(m => m.ChatModule),
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
