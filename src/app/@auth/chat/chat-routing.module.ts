import { RouterModule, Routes } from '@angular/router';
import { ChatComponent } from './chat.component';
import { NgModule } from '@angular/core';const routes: Routes = [{
  path: 'chat',
  component: ChatComponent,
//   children: [
//     {
//       path:'profile',
//       component:ProfileComponent
//     },
//     {
//       path: '',
//       redirectTo: 'profile',
//       pathMatch: 'full',
//     },
//     {
//       path: '',
//       redirectTo: 'profile',
//     },{
//       path:'send/request',
//       component:NgxCustomerSendRequestComponent
//     },{
//       path:'my/request',
//       component:NgxCustomerMyServiceRequestComponent
//     }
//   ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatRoutingModule {
}
