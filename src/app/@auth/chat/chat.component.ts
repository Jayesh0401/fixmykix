import { Component ,OnInit,ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ChatService } from '../../@core/backend/common/services/chat.services';
@Component({
  selector: 'ngx-chat',
  templateUrl: `./chat.component.html`,
  styleUrls: ['./chat.component.scss'],

})
export class ChatComponent implements OnInit {
  message:any;
  messages = [];

	constructor(private chatService: ChatService, private ref: ChangeDetectorRef) {
    this.initMenu();
  }
  initMenu() {
  }

  ngOnInit(): void {
    this.GetMessage();
  }
  
  SendMessage() {
    this.chatService.sendMessage(this.message);
    this.message = '';
    // this.GetMessage();
 }
  GetMessage() {
    this.chatService.getMessage().subscribe((result) => {
      console.log(result, '-------result');
      this.messages.push(result);
      this.ref.markForCheck();
    });

  }
}

