import { Injectable } from '@angular/core';
import { CanActivate,CanLoad ,ActivatedRouteSnapshot, RouterStateSnapshot, Route,Router} from '@angular/router';
import { Observable , of as observableOf } from 'rxjs';
import { tap } from 'rxjs/operators';
import {AuthenticationService } from '../@core/backend/common/services/authentication.service';
@Injectable()
export class IsAuthGuard implements CanLoad {
  constructor(private authService: AuthenticationService, private router: Router) {}
  canLoad (
    route: Route
  ): Observable<boolean> | Promise<boolean> | boolean {
    let userId = localStorage.getItem('userid');
    if(userId){
      this.router.navigate(['user/profile']);
      return observableOf(false);
    } else {
      return observableOf(true);
    }

  }
}
