import { ChangeDetectionStrategy,Component,OnInit,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
declare var $: any;

@Component({
  selector: 'ngx-artist-exolore',
  styleUrls: ['./explore.component.scss'],
  templateUrl: './explore.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxArtistExploreComponent implements OnInit {
  formGroup: any;
  artist_services: any[];
  aritsts_lists: any[];
  filter_services:any[];
  constructor(private fb: FormBuilder,private artistService: ArtistService, private ref: ChangeDetectorRef,private router: Router) {
    this.filter_services = [];
    this.formGroup = this.fb.group({
      minvalue: ['',Validators.required],
      maxvalue: ['',Validators.required]
   });
  }

  ngOnInit(): void {
    this.GetServiceDetails();
    this.GetArtistDetails();
  }

  GetServiceDetails() {
    this.artistService.GetServiceDetails().subscribe((result) => {
      if(result.status == true) {
        this.artist_services =  result.data.services;
    		this.ref.markForCheck();
      }
    });
  }

  GetArtistDetails() {
    this.artistService.GetArtistDetails().subscribe((result) => {
      if(result.status) {
        this.aritsts_lists = result.data;
    		this.ref.markForCheck();
      }
    });
  }

  onCheckboxChange(status, value) {
    if (status) {
      this.filter_services.push(value);
    } else {
      let index = this.filter_services.indexOf(value);

      if (index > -1) {
        console.log('splice',value);
        this.filter_services.splice(index, 1);
      }
    }

    if(this.filter_services.length > 0) {
      this.GetAristsByServiceIds(this.filter_services);
    } else { 
     this.GetArtistDetails(); 
    }
  }

  SetArtistId(id){
    this.artistService.artist_id = id;
    localStorage.setItem('artist_id',id);
    this.router.navigate(['artist/list']);
  }

  GetAristsByServiceIds(ids) {
    this.artistService.GetAristsByServiceIds(ids).subscribe((result) => {
      if(result.status) {
        this.aritsts_lists = result.data;
    		this.ref.markForCheck();
      }
    });
  }
}
