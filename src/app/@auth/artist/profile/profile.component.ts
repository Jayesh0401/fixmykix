import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { ColorPalette } from './../../../shared/color-palette';
import { UsersService } from './../../../@core/backend/common/services/users.service';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'underscore';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'ngx-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  
})
export class ProfileComponent implements OnInit {
  colorTheme: any;
  formGroup: any;
  colorPaletteProps: any[];
  colorPaletteArr: [];
  arrayOfObj: any;
  selectedOption: any;
  selectedOptionnew: any; 
  user:any;
  role:any;
  constructor( private userService:UsersService,
    private fb: FormBuilder,private toasterService: ToastrService,private artistService : ArtistService,private router: Router
    ) {
    this.role = localStorage.getItem("role");
    this.user = JSON.parse(localStorage.getItem("user"));
    if(this.user) {

      this.formGroup = this.fb.group({
        first_name: [this.user.first_name, Validators.required],
        last_name: [this.user.last_name,Validators.required],
        email: [this.user.email,Validators.required],
        mobile_number:[this.user.mobile_number,Validators.required],
        age:[this.user.age,Validators.required],
        gender:[this.user.gender,Validators.required],
        business_name:[this.user.business_name,Validators.required],
        business_socials:[this.user.business_socials,Validators.required],
        address:[this.user.address,Validators.required],
        service_description:[this.user.service_description,Validators.required],
        proof_of_work:['',Validators.required]
      });
    } else {
      this.formGroup = this.fb.group({
        first_name: ['', Validators.required],
        last_name: ['',Validators.required],
        email: ['',Validators.required],
        mobile_number:['',Validators.required],
        age:['',Validators.required],
        gender:['',Validators.required],
        business_name:['',Validators.required],
        business_socials:['',Validators.required],
        address:['',Validators.required],
        service_description:['',Validators.required],
        proof_of_work:['',Validators.required]
      });
    }
  }
	ngOnInit() {
  }
  ngAfterViewInit() {
    this.GetArtistDetailsById();
    // $('#profile_btn').click();
  }

  GetArtistDetailsById() {
    var id = this.user.id;
    this.artistService.GetArtistDetailsById(id).subscribe((result)=> {
      if(result.status) {
        this.user =  result.data[0];
        if(!result.data[0].business_name){
        } else { 
          console.log('I am here in');
          $('#profile_btn').click();
        }
      }
    });
  };

  UpdateUser() {
    let artist_detail ={}
    artist_detail = {
      artist_detail:this.formGroup.value
    };
    this.userService.updateUser(artist_detail).subscribe((result) => {
      if(result.success){
        this.toasterService.success("User saved !!", result.message);
      }
    });
  };

  Artist_Service_request() {
    this.router.navigate(['artist/service/request']);
  };

  
  Artist_Service_List() {
    this.router.navigate(['artist/services/list']);
  };
}
