import { NgModule } from '@angular/core';
import { NbDatepickerModule,NbTooltipModule, NbLayoutModule, NbSpinnerModule, NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbBadgeModule, NbSelectModule, NbButtonModule } from '@nebular/theme';
import { NbSearchModule ,NbTabsetModule} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile.routing';
@NgModule({
  imports: [
    NbSpinnerModule,
    TagInputModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    Ng2SmartTableModule,
    NbSearchModule,
    NbSelectModule,
    NbButtonModule,
    NbBadgeModule,
    FormsModule,
    NbTabsetModule,
    NbLayoutModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NbDatepickerModule.forRoot(),
    CommonModule,
    NbEvaIconsModule,
    NbSelectModule,
    NbTooltipModule,
    ProfileRoutingModule
  ],
  declarations: [ProfileComponent],
  entryComponents: [],
})
export class ProfileModule { }
