import { NgModule , NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ArtistComponent } from './artist.component';
import { ArtistRoutingModule } from './artist-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';
// components imports
import { ProfileComponent } from './profile/profile.component';
import { NgxArtistExploreComponent } from './explore/explore.component';
import { NgxArtistListComponent } from './list/list.component';
import{ NgxArtistServiceRequestComponent } from './service-request/service-request.component';
import { NgxArtistPaymentMethodComponent } from './payment-method/payment-method.component';
import { ArtistServicesModule } from './artist-services/artist-services.module';

const PAGES_COMPONENTS = [
  ArtistComponent,
  ProfileComponent,
  NgxArtistExploreComponent,
  NgxArtistListComponent,
  NgxArtistServiceRequestComponent,
  NgxArtistPaymentMethodComponent,
  NgxArtistServiceRequestComponent
];

@NgModule({
  imports :[
    CommonModule,
    AngularMultiSelectModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    Ng2SmartTableModule,
    FormsModule, 
    ReactiveFormsModule,
    ArtistRoutingModule,
    ArtistServicesModule
  ],
  declarations: [
    ...PAGES_COMPONENTS
  ],
  providers: [
  ],
  schemas:[NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA], 
})
export class ArtistModule {
}
