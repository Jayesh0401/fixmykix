import { ChangeDetectionStrategy,Component,OnInit ,ChangeDetectorRef} from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
import { Router } from '@angular/router';
import * as _ from 'underscore';
declare var $: any;

@Component({
  selector: 'ngx-artist-list',
  styleUrls: ['./list.component.scss'],
  templateUrl: './list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxArtistListComponent implements OnInit {
  formGroup: any;
  id:any;
  artist_details:any;
  artist_services:[];
  constructor(private fb: FormBuilder, private artistService: ArtistService, private ref: ChangeDetectorRef,private router: Router) {
    this.formGroup = this.fb.group({
      minvalue: ['',Validators.required],
      maxvalue: ['',Validators.required]
   });
    this.artist_details = {
      first_name:''
    };
   this.id =  this.artistService.artist_id;
   if(!this.id) {
     this.id =  localStorage.getItem('artist_id');
   }
   this.GetArtistById(this.id);
  }

  ngOnInit(): void {}

  GetArtistById(id){
    this.artistService.GetArtistDetailsById(id).subscribe((result) => {
      if(result.status) {
        let find_artist = _.findWhere(result.data,{id:parseInt(id)});
        if(find_artist){
          this.artist_details = find_artist;
          this.artist_services =  find_artist.artist_services;
      		this.ref.markForCheck();
        }
      }
    });
  };

  Apply_Service_request() {
    this.artistService.artist_details =  this.artist_details;
    console.log('I am hre in');
    this.router.navigate(['customer/send/request']);
  }

  Chat_request() {
    this.router.navigate(['users/chat']);
  }
}

