import { ChangeDetectionStrategy,Component,OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'ngx-artist-payment-method',
  styleUrls: ['./payment-method.component.scss'],
  templateUrl: './payment-method.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxArtistPaymentMethodComponent implements OnInit {
  formGroup: any;
  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      minvalue: ['',Validators.required],
      maxvalue: ['',Validators.required]
   });
  }

  ngOnInit(): void {}
}
