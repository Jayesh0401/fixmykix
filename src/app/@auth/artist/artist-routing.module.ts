import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ArtistComponent } from './artist.component';
import { ProfileComponent } from './profile/profile.component';
import { NgxArtistExploreComponent } from './explore/explore.component';
import { NgxArtistListComponent } from  './list/list.component';
import { NgxArtistPaymentMethodComponent } from './payment-method/payment-method.component';
import { NgxArtistServiceRequestComponent } from './service-request/service-request.component';
import { ArtistServicesModule } from './artist-services/artist-services.module';
const routes: Routes = [{
  path: '',
  component: ArtistComponent,
  children: [
    {
      path:'profile',
      component:ProfileComponent
    },
    {
      path:'explore',
      component:NgxArtistExploreComponent
    },{
      path:'list',
      component:NgxArtistListComponent
    },{
      path:'service/request',
      component:NgxArtistServiceRequestComponent
    },{
      path:'payment',
      component:NgxArtistPaymentMethodComponent
    },
    {
      path:'services',
      loadChildren: () => import('./artist-services/artist-services.module')
      .then(m => m.ArtistServicesModule),
    },
    {
      path: '',
      redirectTo: 'profile',
      pathMatch: 'full',
    },
    {
      path: '',
      redirectTo: 'profile',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArtistRoutingModule {
}
