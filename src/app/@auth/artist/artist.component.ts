import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { NbMenuItem } from '@nebular/theme';
import { UtilService } from '../../@core/backend/common/services/utils.service';
@Component({
  selector: 'ngx-artist',
  templateUrl: `./artist.component.html`,
  styleUrls: ['./artist.component.scss'],

})
export class ArtistComponent implements OnDestroy {
  alive: boolean = true;
  loading = true;
  constructor(private utilService: UtilService) {
    this.initMenu();
  }
  initMenu() {
  }

  ngOnDestroy(): void {
    this.alive = false;
  }
}
