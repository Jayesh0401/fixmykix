import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import  { ArtistServicesComponent }  from  './artist-services.component';
import { ArtistServicesListComponent } from './list/artist-services-list.component';
import { ArtistServicesAddUpdateComponent } from './add-update-services/add-update-services.component';
const routes: Routes = [{
	path: '',
    component: ArtistServicesComponent,
    children: [
        {
          path:'list',
          component:ArtistServicesListComponent
        },
        {
            path:'add/update',
            component:ArtistServicesAddUpdateComponent
        },
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(routes), FormsModule],
    exports: [RouterModule],
})
export class ArtistServicesRoutingModule {
}
