import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { ColorPalette } from '../../../../shared/color-palette';
import { UsersService } from '../../../../@core/backend/common/services/users.service';
import { ArtistService } from '../../../../@core/backend/common/services/artists.services';
import { ToastrService } from 'ngx-toastr';
import { ColumnConfig } from 'material-dynamic-table';
import * as _ from 'underscore';
import { Router } from '@angular/router';
import  { CategoriesService } from '../../../../@core/backend/common/services/categories.service';
declare var $: any;
@Component({
  selector: 'ngx-artist-services-list',
  templateUrl: './artist-services-list.component.html',
  styleUrls: ['./artist-services-list.component.scss'],
  
})
export class ArtistServicesListComponent implements OnInit {
  user:any;
  services:[];
  record:Boolean = false;
  constructor(private artistService : ArtistService,private router: Router,private categoriesService:CategoriesService,private toastrService:ToastrService) {
    this.user = JSON.parse(localStorage.getItem("user"));
  }
  
	ngOnInit() {
    this.GetArtistServicesById();
  }

  GetArtistServicesById() {
    var id = this.user.id;
    this.artistService.GetArtistServicesById(id).subscribe((result)=> {
      if(result.status) {
        this.services =  result.data;
        if(result.data.length > 0) {
          this.record =  false;
        } else {
          this.record = true;
        }
      }
    });
  };

  Artist_Add_Service() {
    this.router.navigate(['artist/services/add/update']);
  }

  Delete_service(service) {
    this.categoriesService.deleteService(service.id).subscribe((result)=> {
      if(result.status) {
        this.toastrService.success("Service Deleted !!", result.message);
        this.GetArtistServicesById();
      }
    });
  }
  
}
