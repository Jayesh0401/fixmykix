import { NgModule } from '@angular/core';
import { NbDatepickerModule,NbTooltipModule, NbLayoutModule, NbSpinnerModule, NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule, NbBadgeModule, NbSelectModule, NbButtonModule } from '@nebular/theme';
import { NbSearchModule ,NbTabsetModule} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TagInputModule } from 'ngx-chips';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CommonModule } from '@angular/common';
import { ArtistServicesComponent } from './artist-services.component';
import { ArtistServicesRoutingModule } from './artist-services.routing';
import { ArtistServicesListComponent } from './list/artist-services-list.component';
import { ArtistServicesAddUpdateComponent } from './add-update-services/add-update-services.component';
import { DynamicTableModule } from 'material-dynamic-table';
@NgModule({
  imports: [
    NbSpinnerModule,
    TagInputModule,
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    Ng2SmartTableModule,
    NbSearchModule,
    NbSelectModule,
    NbButtonModule,
    NbBadgeModule,
    FormsModule,
    NbTabsetModule,
    NbLayoutModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NbDatepickerModule.forRoot(),
    CommonModule,
    NbEvaIconsModule,
    NbSelectModule,
    NbTooltipModule,
    ArtistServicesRoutingModule,
    DynamicTableModule
  ],
  declarations: [ 
    ArtistServicesComponent,
    ArtistServicesListComponent,
    ArtistServicesAddUpdateComponent
  ],
  entryComponents: [],
})
export class ArtistServicesModule { }
