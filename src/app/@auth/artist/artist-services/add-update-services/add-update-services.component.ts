import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'underscore';
import { Router } from '@angular/router';
import  { CategoriesService } from '../../../../@core/backend/common/services/categories.service';
declare var $: any;
@Component({
  selector: 'ngx-artist-services-add-update',
  templateUrl: './add-update-services.component.html',
  styleUrls: ['./add-update-services.component.scss'],
  
})
export class ArtistServicesAddUpdateComponent implements OnInit {
  categories:[];
  subCategories:[];
  subCategoriesOpt:[];
  Single_Set_Option:[];
  Services:[];
  artist_id:'';
  artist_service_ids:any[];
  filter_services:any;
  submit_btn:Boolean =  false;
  category_id: any;
  constructor(private router:Router, private toastr: ToastrService,private categoriesService:CategoriesService) {
    let user = JSON.parse(localStorage.getItem("user"));
    this.artist_id =  user.id;
    let data = [];
    this.GetCategoriesDetails(data);
  };

	ngOnInit() {
    if(!this.categoriesService.service_id) {
    } else {
      this.GetServiceDetails();
    }
  };

  GetCategoriesDetails(data) {
    this.categoriesService.GetCategoriesDetails(data).subscribe((result)=> {
      if(result.status){
        this.categories =  result.data.categories;
      }
    });
  };

  selectedCategory(category) {
    this.subCategories = category.children;
      this.subCategoriesOpt = [];
      this.Single_Set_Option = [];
      this.Services = [];
      this.artist_service_ids = [];
      this.submit_btn = false;
      this.category_id = '';
  };

  selectedSubCategory(subCategory) {
    this.subCategoriesOpt =  subCategory.children;
    this.Single_Set_Option = [];
    this.Services = [];
    this.artist_service_ids = [];
    this.submit_btn = false;
    this.category_id = '';
  };

  selectedSubCategoryOpt(option){
    this.Services = [];
    this.category_id = '';
    this.artist_service_ids = [];
    if(option.type =='custom') {
      this.category_id = option.id;
      this.artist_service_ids.push(option.id);
      this.submit_btn = true;
    } else {  
      this.submit_btn = false;
      this.Single_Set_Option = option.children;
    }
    this.filter_services = [];
  };

  setServices(option) {
    this.category_id = option.id;
    this.artist_service_ids = [];
    this.Services =  option.services;
  };

  onCheckboxChange(status, value) {
    if (status) {
      this.artist_service_ids.push(value);
    } else {
      let index = this.artist_service_ids.indexOf(value);
      if (index > -1) {
        this.artist_service_ids.splice(index, 1);
      }
    }
    if(this.artist_service_ids.length > 0) {
      this.submit_btn = true;
    } else {
      this.submit_btn = false;
    }
  };

  submit() {
    let count = 0;
    this.artist_service_ids.forEach((id) => {
      let data ={
        "artist_service" : {
          "service_id": id,
          "category_id": this.category_id,
          "description": "",
          "artist_id" : this.artist_id,
          "price": 100,
          "number_of_days" : 3,
        "proof_of_work": ""
        }
      };
      this.categoriesService.addService(data).subscribe((result)=>{
        count++;
        if(count >= this.artist_service_ids.length) {
          this.toastr.success("Service Saved !!", result.message);
          this.router.navigate(['artist/services/list']);
        }
      });
    });

  };

  GetServiceDetails() {
    let id =  this.categoriesService.service_id;
    let data = [];
    this.categoriesService.GetCategoriesDetails(data).subscribe((result)=> {
      let categories = result.data.categories;
      var find_obj = _.findWhere(categories,{id:id});
    });
  }
}
