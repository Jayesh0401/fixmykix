import { ChangeDetectionStrategy,Component,OnInit ,ChangeDetectorRef} from '@angular/core';
import { FormBuilder, FormControl,ReactiveFormsModule,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ArtistService } from '../../../@core/backend/common/services/artists.services';
import { ServiceRequestService } from '../../../@core/backend/common/services/service_request.service';
import * as _ from 'underscore';
declare var $: any;

@Component({
  selector: 'ngx-artist-service-request',
  styleUrls: ['./service-request.component.scss'],
  templateUrl: './service-request.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxArtistServiceRequestComponent implements OnInit {
  artist_services_Requests:[];
  formGroup: any;
  selected_request:any;
  constructor(private fb: FormBuilder, private artistService: ArtistService, private ref: ChangeDetectorRef, private serviceRequestService: ServiceRequestService,private toasterService: ToastrService,private router: Router) {
    
    this.formGroup = this.fb.group({
      price: ['', Validators.required],
      description: ['',Validators.required],
      turn_around_time: ['',Validators.required]
    });
  }

  ngOnInit(): void {
    this.GetServiceRequestDetails();
  }

  GetServiceRequestDetails(){
    this.serviceRequestService.GetServicRequesteDetails().subscribe((result) => {
      if(result.status){
        this.artist_services_Requests = result.data.service_requests;
    		this.ref.markForCheck();
      }
    });
  }

  Accept_Request() {
    let request =  this.selected_request;
    request.price =  this.formGroup.value.price;
    request.artist_description =  this.formGroup.value.description;
    request.turn_around_time =  this.formGroup.value.turn_around_time;
    request.status = 'approved';
    this.serviceRequestService.AcceptServicRequest(request).subscribe((result) => {
      if(result.status){
        this.toasterService.success('Request Accepted !!',result.message);
        $('#close_btn').click();
        this.GetServiceRequestDetails();
      } else {
        this.toasterService.error('Request Not Accepted !!',result.message);
      }
    });
  };

  OpenTotPage(request) {
    this.selected_request = request;
    $('#profile_btn').click();
  }

  Reject_Request(request) {
    request.status = 'rejected';
    this.serviceRequestService.RejectServicRequest(request).subscribe((result) => {
      if(result.status) {
        this.toasterService.success('Request Rejected !!',result.message);
        this.GetServiceRequestDetails();
      }else {
        this.toasterService.error('Request Not Rejected !!',result.message);
      }
    });
  }
}

