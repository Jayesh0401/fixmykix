import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpRequest } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import { AuthGuard } from './auth.guard';
import { AuthPipe } from './auth.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SharedModule } from '../shared/shared.module';
import { NgxMainComponent } from './components/main/main.component';
import { NgxContactUsComponent } from './components/contact-us/contact-us.component';
import  { NgxSignUpComponent } from './components/sign-up/sign-up.component';
import {NgxAboutUsComponent } from './components/about-us/about-us.component';
import { NgxShoeRestorationComponent } from './components/shoerestoration/shoerestoration.component';
import { NgxShoeArtComponent } from './components/shoeart/shoeart.component';
import  { NgxFixmyKixComponent } from './components/what-is-fixmykix/what-is-fixmykix.component';
import { HeaderComponent } from  '../layout/header.component';
import  { FooterComponent } from '../layout/footer.component';
import {
  NgxLoginComponent,
  NgxAuthComponent,
  NgxAuthBlockComponent,
  NgxRegisterComponent,
} from './components';

import { AuthRoutingModule } from './auth-routing.module';
import { authOptions } from './auth.settings';
import { authSettings } from './access.settings';
import { IsAuthGuard } from  './isAuth.guard';
const GUARDS = [AuthGuard];
const PIPES = [AuthPipe];
const COMPONENTS = [
  NgxLoginComponent,
  NgxAuthComponent,
  NgxRegisterComponent,
  NgxAuthBlockComponent,
  NgxMainComponent,
  NgxContactUsComponent,
  NgxSignUpComponent,
  HeaderComponent,
  FooterComponent,
  NgxAboutUsComponent,
  NgxShoeRestorationComponent,
  NgxShoeArtComponent,
  NgxFixmyKixComponent
];

export function filterInterceptorRequest(req: HttpRequest<any>): boolean {
  return ['/auth/login', '/auth/sign-up']
    .some(url => req.url.includes(url));
}

@NgModule({
  declarations: [...PIPES, ...COMPONENTS],
  imports: [
    AuthRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule ,
    NgMultiSelectDropDownModule.forRoot()
  ],
  exports: [...PIPES],
  providers: [
    IsAuthGuard,
    AuthGuard
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: AuthModule,
      providers: [
        { useValue: filterInterceptorRequest },
        { provide: HTTP_INTERCEPTORS, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        ...GUARDS]
    };
  }
}
