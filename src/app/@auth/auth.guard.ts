/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Injectable } from '@angular/core';
import { CanLoad ,ActivatedRouteSnapshot, RouterStateSnapshot, Route,Router} from '@angular/router';
import { Observable,of as observableOf } from 'rxjs';
import { tap } from 'rxjs/operators';
import {AuthenticationService } from '../@core/backend/common/services/authentication.service';
@Injectable()
export class AuthGuard implements CanLoad {
  constructor(private authService: AuthenticationService, private router: Router) {}

  canLoad (
    route: Route
  ): Observable< boolean> | Promise< boolean> | boolean {
    let userId = localStorage.getItem('userid');
    if(userId) {
      return true;
    } else {
      this.router.navigate(['']);
      return false;
    }
  }
}
