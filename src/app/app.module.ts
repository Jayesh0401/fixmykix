import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './@core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule } from  '@angular/material';
import { AuthGuard } from './@auth/auth.guard';
import { FormsModule } from '@angular/forms';
import * as _ from 'underscore';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { IsAuthGuard } from './@auth/isAuth.guard';
import { FlexLayoutModule } from "@angular/flex-layout";
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:8000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    NoopAnimationsModule,
    HttpClientModule,
    CommonModule,
    SharedModule,
    AppRoutingModule,
    CoreModule.forRoot(),
    FlexLayoutModule,
    FormsModule,
    ToastrModule.forRoot(),
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    SocketIoModule.forRoot(config),
  ],
  exports :[
    ToastrModule
  ],
  providers: [
    AuthGuard,
    IsAuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
